﻿namespace CodorniXLogin.Resource
{
	using Newtonsoft.Json;
	public class IpInfo
	{
		[JsonProperty("ip")]
		public string Ip { get; set; }

		[JsonProperty("city")]
		public string City { get; set; }

		[JsonProperty("region")]
		public string Region { get; set; }

		[JsonProperty("country_code")]
		public string Country { get; set; }

		[JsonProperty("latitude")]
		public string Lat { get; set; }

		[JsonProperty("longitude")]
		public string Long { get; set; }

		[JsonProperty("organisation")]
		public string Provider
		{
			get;
			set;
		}

		[JsonProperty("postal")]
		public string Postal { get; set; }
	}
}