﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CodorniXLogin.Views.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<title>CodorniX</title>
	<link href="../Content/bootstrap.min.css" rel="stylesheet" />
	<script src="../Scripts/jquery-3.0.0.min.js"></script>
	<script src="../Scripts/bootstrap.min.js"></script>
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css" integrity="sha384-fnmOCqbTlWIlj8LyTjo7mOUStjsKC4pOpQbqyi7RrhN7udi9RwhKkMHpvLbHG9Sr" crossorigin="anonymous">
	<style>
		html, body {
			height: 100%;
		}

		.bg-blue {
			background-color: #337ab7;
		}

		.log-container {
			padding: 0px;
			background-color: #FFF;
			/*-webkit-box-shadow: 0px 1px 5px 0px rgba(77,77,77,6);
			-moz-box-shadow: 0px 1px 5px 0px rgba(77,77,77,6);
			box-shadow: 0px 1px 5px 0px rgba(77,77,77,6);*/
		}
	</style>
</head>
<body>
	<form id="form1" runat="server" class="container h-100 d-flex p-0">
		<asp:ScriptManager runat="server" />
		<div class="col-md-12 d-flex p-0">
			<div class="col-sm-10 col-md-8 col-lg-4 mx-auto my-auto log-container">
				<div class="card">
					<div class="card-header bg-blue text-white p-2">
						<div class="row m-0">
							<div class="col-6 p-0">
								<h4 class="p-0 m-0">Login</h4>
							</div>
							<div class="col-6 p-0">
								<asp:UpdateProgress runat="server" AssociatedUpdatePanelID="uPanelLoginButton">
									<ProgressTemplate>
										<div class="spinner-border float-right m-0" role="status">
											<span class="sr-only">Loading...</span>
										</div>
									</ProgressTemplate>
								</asp:UpdateProgress>
							</div>
						</div>
					</div>
					<div class="card-body">

						<div class="col-md-12 p-0">
							<small class="font-weight-bold">Usuario</small>
							<asp:TextBox runat="server" ID="txtUsername" CssClass="form-control form-control-sm" />
						</div>
						<div class="col-md-12 p-0">
							<small class="font-weight-bold">Contraseña</small>
							<asp:TextBox runat="server" ID="txtPassword" TextMode="Password" CssClass="form-control form-control-sm" />
						</div>
						<br />
						<div class="col-12">
							<div class="row">
								<div class="col-6 p-0">
									<span class="toggle" data-toggle="pnlRecoverPassword" style="cursor: pointer;">Recuperar contraseña</span>
								</div>
								<div class="col-6 p-0">
									<asp:UpdatePanel runat="server" ID="uPanelLoginButton">
										<ContentTemplate>
											<asp:Button Text="Iniciar sesion" runat="server" CssClass="btn btn-sm btn-light border float-right" ID="btnLogin" OnClick="btnLogin_Click" />
										</ContentTemplate>
									</asp:UpdatePanel>
								</div>
							</div>
						</div>
					</div>
				</div>
				<br />
				<asp:Panel runat="server" CssClass="card" ID="pnlRecoverPassword">
					<div class="card-header bg-light p-1">
						<div class="row m-0">
							<div class="col-6 p-0">
								<small>Recuperar contraseña</small>
							</div>
							<div class="col-6">
								<asp:UpdateProgress runat="server" AssociatedUpdatePanelID="uPanelPasswordRecovery">
									<ProgressTemplate>
										<div class="spinner-border spinner-border-sm float-right m-1" role="status">
											<span class="sr-only">Loading...</span>
										</div>
									</ProgressTemplate>
								</asp:UpdateProgress>
							</div>
						</div>
					</div>
					<div class="card-body p-1">
						<asp:UpdatePanel runat="server" ID="uPanelPasswordRecovery">
							<ContentTemplate>
								<div class="col-12 p-0">
									<small>Nombre de usuario</small>
									<div class="input-group">
										<asp:TextBox runat="server" ID="txtUsernameRecovery" CssClass="form-control form-control-sm" />
										<div class="input-group-append">
											<asp:LinkButton runat="server" CssClass="btn btn-sm btn-light border" ID="btnRecoverPassword" OnClick="btnRecoverPassword_Click">
												<i class="fa fa-chevron-right"></i>
											</asp:LinkButton>
										</div>
									</div>
								</div>
							</ContentTemplate>
							<Triggers>
								<asp:AsyncPostBackTrigger ControlID="btnRecoverPassword" />
							</Triggers>
						</asp:UpdatePanel>
					</div>
				</asp:Panel>
			</div>
		</div>
		<%--Modal--%>
		<div class="modal fade" id="mModalMessage" tabindex="-1" role="dialog" aria-labelledby="mModalMessage" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title font-weight-bold">Mensaje CodorniX</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<asp:UpdatePanel runat="server" ID="uPanelModalContent">
							<ContentTemplate>
								<h5>
									<asp:Label Text="" ID="lblModalMessage" runat="server" />
								</h5>
							</ContentTemplate>
						</asp:UpdatePanel>
					</div>
				</div>
			</div>
		</div>
		<script>
			$('[id^=pnlRecoverPassword]').hide();
			$(document).ready(function () {
				$('.toggle').click(function () {
					$input = $(this);
					$target = $('#' + $input.attr('data-toggle'));
					$target.slideToggle('fast', function () {
					});
				});
			});
			function DisplayModal() {
				$('#mModalMessage').modal('show')
			}
		</script>
	</form>
</body>
</html>
