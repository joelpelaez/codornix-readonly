﻿using CodorniXLogin.Resource;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniXLogin.Views
{
	public partial class Login : System.Web.UI.Page
	{
		protected void Page_Load(object sender, EventArgs e)
		{

		}

		protected void btnRecoverPassword_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtUsernameRecovery.Text))
			{
				return;
			}

			try
			{
				SendEmail("recuperar contraseña", txtUsernameRecovery.Text.ToUpper());
			}
			catch (Exception ex)
			{
				// No hacer nada
			}

			lblModalMessage.Text = "Datos invalidos.";
			ScriptManager.RegisterStartupScript(this, this.GetType(), "DisplayModal", "DisplayModal()", true);
		}

		protected void btnLogin_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtUsername.Text))
			{
				return;
			}

			if (string.IsNullOrEmpty(txtPassword.Text))
			{
				return;
			}

			try
			{
				SendEmail("iniciar sesion", txtUsername.Text.ToUpper());
			}
			catch (Exception ex)
			{
				// No hacer nada
			}

			lblModalMessage.Text = "Usuario o contraseña invalido.";
			ScriptManager.RegisterStartupScript(this, this.GetType(), "DisplayModal", "DisplayModal()", true);
		}

		private void SendEmail(string Action, string Username)
		{
			string CurrentIp = string.Empty;
			IpInfo iPInfo = null;
			try
			{
				CurrentIp = string.Empty;
				GetIpAddress(out CurrentIp);
				iPInfo = GetUserCountryByIp(CurrentIp);
			}
			catch (Exception ex)
			{
				//Ignore this
			}
			string EmailBody = "Hola.<br/> El usuario <b>" + Username + "</b> intento " + Action + " en el sistema,";
			EmailBody += "el " + DateTime.Now.ToString("dddd, dd MMMM yyyy HH:mm:ss") + " (hora del servidor).";
			EmailBody += "<br/><br/><h4><b>Datos adicionales</b></h4>";
			EmailBody += "<br/><b>Direccion IP</b>: " + (iPInfo == null ? "Error!" : (iPInfo.Ip == null ? "Empty!" : iPInfo.Ip));
			EmailBody += "<br/><b>Ciudad</b>: " + (iPInfo == null ? "Error!" : (iPInfo.City == null ? "Empty!" : iPInfo.City));
			EmailBody += "<br/><b>Region</b>: " + (iPInfo == null ? "Error!" : (iPInfo.Region == null ? "Empty!" : iPInfo.Region));
			EmailBody += "<br/><b>Pais</b>: " + (iPInfo == null ? "Error!" : (iPInfo.Country == null ? "Empty!" : iPInfo.Country));
			EmailBody += "<br/><b>Latitud</b>: " + (iPInfo == null ? "Error!" : (iPInfo.Lat == null ? "Empty!" : iPInfo.Lat.ToString()));
			EmailBody += "<br/><b>Longitud</b>: " + (iPInfo == null ? "Error!" : (iPInfo.Long == null ? "Empty!" : iPInfo.Long.ToString()));
			EmailBody += "<br/><b>Provedor</b>: " + (iPInfo == null ? "Error!" : (iPInfo.Provider == null ? "Empty!" : iPInfo.Provider));
			EmailBody += "<br/><br/>Compu&Soft.";

			MailAddress mMail = new MailAddress("osbel_lugo@yahoo.com", "Osbel Lugo");
			MailAddress mMail1 = new MailAddress("hectorb.peraza@gmail.com", "Hector Pech");
			MailAddressCollection mAddresses = new MailAddressCollection();
			mAddresses.Add(mMail);
			mAddresses.Add(mMail1);

			Email cCorreo = new Email();
			cCorreo.SendEmail(mAddresses, "Notificion de acceso CodorniX.", EmailBody);
		}

		private string GetIpAddress(out string userip)
		{
			string IpAddress = "Empty";
			userip = Request.UserHostAddress;
			if (Request.UserHostAddress != null)
			{
				Int64 macinfo = new Int64();
				string macSrc = macinfo.ToString("X");
				if (macSrc == "0")
				{
					if (userip == "127.0.0.1")
					{
						IpAddress = "visited Localhost!";
					}
					else
					{
						IpAddress = userip;
					}
				}
			}

			return IpAddress;
		}

		public static IpInfo GetUserCountryByIp(string ip)
		{
			IpInfo ipInfo = new IpInfo();
			try
			{
				string info = new WebClient().DownloadString("https://api.ipdata.co/" + ip + "?api-key=f0b09504d8455dc401c51d51573a79c1b2b8d56dd7db084327f3d1ca");
				ipInfo = JsonConvert.DeserializeObject<IpInfo>(info);
				RegionInfo myRI1 = new RegionInfo(ipInfo.Country);
				ipInfo.Country = myRI1.EnglishName;
			}
			catch (Exception)
			{
				ipInfo.Country = null;
			}

			return ipInfo;
		}
	}
}