﻿CREATE   FUNCTION [dbo].[fn_ObtenerFechasPeriodicidad](@UidPeriodicidad uniqueidentifier, @DtFechaInicio date, @DtFechaFin date)
RETURNS @res TABLE (DtDate date)

BEGIN
	DECLARE @tmp TABLE (DtDate date);
	DECLARE @DtFecha date,
			@tipo nvarchar(50);
	
	SELECT @tipo = VchTipoFrecuencia FROM TipoFrecuencia tf INNER JOIN Periodicidad p ON p.UidTipoFrecuencia = tf.UidTipoFrecuencia WHERE p.UidPeriodicidad = @UidPeriodicidad
	
	SET @DtFecha = @DtFechaInicio

	IF @tipo = 'Sin periodicidad' OR @UidPeriodicidad IS NULL
	BEGIN
		WHILE @DtFecha <= @DtFechaFin
		BEGIN
			INSERT INTO @tmp VALUES (@DtFecha)
			
			SET @DtFecha = DATEADD(day, 1, @DtFecha)
		END
	END

	ELSE
	BEGIN
		WHILE @DtFecha <= @DtFechaFin 
		BEGIN
			SET @DtFecha = dbo.f_ObtenerPeriodicidad(@UidPeriodicidad, @DtFecha)

			IF @DtFecha <= @DtFechaFin
				INSERT INTO @tmp VALUES (@DtFecha);

		END
	END

	INSERT INTO @res SELECT * FROM @tmp

	RETURN
END