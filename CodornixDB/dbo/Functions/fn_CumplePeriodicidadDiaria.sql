﻿CREATE  FUNCTION [dbo].[fn_CumplePeriodicidadDiaria] (@UidPeriodicidad uniqueidentifier, @DtFecha date)
RETURNS bit
AS
BEGIN

	DECLARE @frecuencia int,
			@diff int,
			@DtFechaInicio date,
			@result bit;



	SELECT @frecuencia = IntFrecuencia, @DtFechaInicio = DtFechaInicio FROM Periodicidad WHERE UidPeriodicidad = @UidPeriodicidad

	IF (@DtFechaInicio > @DtFecha)
		RETURN 0;

	SET @diff = DATEDIFF(day, @DtFechaInicio, @DtFecha) % @frecuencia

	IF @diff = 0
		RETURN 1

	RETURN 0
END