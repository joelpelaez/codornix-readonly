﻿CREATE FUNCTION [dbo].[fn_CumplePeriodicidadMensual] (@UidPeriodicidad uniqueidentifier, @DtFecha date)
RETURNS bit
AS
BEGIN

	DECLARE @diff int,
			@freq int,
			@DtStart date,
			@DtEnd date,
			@IntDiasSemana int,
			@IntDiasMes int,
			@DtTmp date;

	SELECT
		@freq = IntFrecuencia,
		@DtStart = DtFechaInicio
	FROM Periodicidad WHERE UidPeriodicidad = @UidPeriodicidad

	SELECT
		@IntDiasMes = IntDiasMes,
		@IntDiasSemana = IntDiasSemana
	FROM PeriodicidadMensual WHERE UidPeriodicidad = @UidPeriodicidad

	SET @diff = DATEDIFF(month, @DtStart, @DtFecha) % @freq

	IF @diff = 0
	BEGIN
		IF (@IntDiasSemana = 0 OR @IntDiasSemana IS NULL) AND DATEPART(day, @DtFecha) = @IntDiasMes
		BEGIN
			RETURN 1;
		END
		ELSE
		BEGIN
			IF @IntDiasMes = -1
			BEGIN
				SET @DtTmp = EOMONTH(@DtFecha)
				IF @DtFecha < @DtTmp
				BEGIN
					SET @DtEnd = @DtTmp
					SET @DtTmp = DATEADD(day, -7, @DtEnd)
					IF (@DtTmp <= @DtFecha)
						IF (@IntDiasSemana = DATEPART(weekday, @DtFecha))
							RETURN 1
					
				END
			END
			IF (@IntDiasMes * 7) > DATEPART(day, @DtFecha)
			BEGIN
				SET @DtEnd = DATEADD(day, (@IntDiasMes * 7) + 1, EOMONTH(@DtFecha, -1))
				SET @DtTmp = DATEADD(day, -7, @DtEnd)
				IF @DtTmp <= @DtFecha AND @DtEnd >= @DtFecha
					IF (@IntDiasSemana = DATEPART(weekday, @DtFecha))
						RETURN 1;
				
			END
		END
	END

	RETURN 0
END