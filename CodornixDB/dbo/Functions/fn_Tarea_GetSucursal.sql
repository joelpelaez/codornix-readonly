﻿CREATE FUNCTION [dbo].[fn_Tarea_GetSucursal](@UidTarea uniqueidentifier)
RETURNS uniqueidentifier
BEGIN

DECLARE @UidSucursal uniqueidentifier

SELECT @UidSucursal = d.UidSucursal
	FROM Tarea t
	LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
	LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
	LEFT JOIN Area a ON ta.UidArea = a.UidArea
	INNER JOIN Departamento d ON (d.UidDepartamento = dt.UidDepartamento) OR (d.UidDepartamento = a.UidDepartamento)
	WHERE t.UidTarea = @UidTarea

RETURN @UidSucursal

END