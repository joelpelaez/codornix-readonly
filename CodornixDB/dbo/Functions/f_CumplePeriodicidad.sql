﻿CREATE  FUNCTION [dbo].[f_CumplePeriodicidad] (@UidPeriodicidad uniqueidentifier, @DtFecha date)
RETURNS bit
BEGIN
	
	DECLARE @DtFechaInicio date,
			@VchTipoFrecuencia nvarchar(50),
			@DtResult bit = null,
			@DtStart date,
			@DtEnd date;

	SELECT @VchTipoFrecuencia = t.VchTipoFrecuencia
		FROM TipoFrecuencia t
		INNER JOIN Periodicidad p ON t.UidTipoFrecuencia = p.UidTipoFrecuencia 
		WHERE p.UidPeriodicidad = @UidPeriodicidad

	IF @VchTipoFrecuencia = 'Sin periodicidad' OR @UidPeriodicidad IS NULL
	BEGIN
		-- Asume que el rango de fecha fue verificado anteriormente o lo será.
		RETURN 1;
	END

	IF @VchTipoFrecuencia = 'Diaria'
	BEGIN
		SELECT @DtResult = dbo.fn_CumplePeriodicidadDiaria(@UidPeriodicidad, @DtFecha);
	END

	ELSE IF @VchTipoFrecuencia = 'Semanal'
	BEGIN
		SELECT @DtResult = dbo.fn_CumplePeriodicidadSemanal(@UidPeriodicidad, @DtFecha);
	END

	ELSE IF @VchTipoFrecuencia = 'Mensual'
	BEGIN
		SELECT @DtResult = dbo.fn_CumplePeriodicidadMensual(@UidPeriodicidad, @DtFecha);
	END

	RETURN @DtResult;
END