﻿CREATE FUNCTION fn_EsAplazabaleAFecha(@UidDepartamento uniqueidentifier, @DtFecha date, @UidUsuario uniqueidentifier) 
RETURNS bit
AS

BEGIN

DECLARE @found uniqueidentifier;

IF (@UidUsuario IS NULL)
	SELECT @found = UidPeriodo FROM Periodo WHERE UidDepartamento = @UidDepartamento AND DtFechaInicio >= @DtFecha AND DtFechaFin <= @DtFecha
ELSE 
	SELECT @found = UidPeriodo FROM Periodo WHERE UidDepartamento = @UidDepartamento AND DtFechaInicio >= @DtFecha AND DtFechaFin <= @DtFecha AND UidUsuario = @UidUsuario

IF @found IS NOT NULL
	RETURN 1

RETURN 0

END