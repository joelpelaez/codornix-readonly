﻿CREATE PROCEDURE [dbo].[usp_Revision_ListByUser]
	@UidUsuario uniqueidentifier,
	@UidSucursal uniqueidentifier,
	@DtFecha date,
	@VchPeriodos nvarchar(2000) = null,
	@VchNombre nvarchar(50) = null,
	@UidDepartamento uniqueidentifier = null,
	@UidArea uniqueidentifier = null,
	@UidEstado int = null
AS

SET NOCOUNT ON

SELECT
	c.IntFolio,
	r.UidRevision,
	c.UidCumplimiento,
	t.VchNombre AS VchTarea,
	d.VchNombre AS VchDepartamento,
	a.VchNombre AS VchArea,
	c.DtFechaHora AS DtFechaCumplimiento,
	CASE WHEN r.UidRevision IS NULL THEN 'No Revisado' ELSE 'Revisado' END AS VchEstado
FROM Cumplimiento c
INNER JOIN EstadoCumplimiento ec ON c.UidEstadoCumplimiento = ec.UidEstadoCumplimiento
INNER JOIN Tarea t ON c.UidTarea = t.UidTarea
LEFT JOIN Area a ON a.UidArea = c.UidArea
INNER JOIN Departamento d ON (d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = c.UidDepartamento)
INNER JOIN AsignacionSupervision ap ON ap.UidDepartamento = d.UidDepartamento
LEFT JOIN Revision r ON r.UidCumplimiento = c.UidCumplimiento
WHERE
	(@UidEstado = 0 OR @UidEstado IS NULL OR (@UidEstado = 1 AND r.UidRevision IS NULL) OR (@UidEstado = 2 AND r.UidRevision IS NOT NULL)) AND 
	ap.DtFechaInicio <= @DtFecha AND
	ap.DtFechaFin >= @DtFecha AND
	ap.UidUsuario = @UidUsuario AND
	d.UidSucursal = @UidSucursal AND
	CAST(c.DtFechaHora AS DATE) = @DtFecha AND
	ec.VchTipoCumplimiento = 'Completo' AND
	(@VchNombre IS NULL OR t.VchNombre LIKE '%' + @VchNombre + '%') AND
	((@UidDepartamento IS NULL OR @UidDepartamento = '00000000-0000-0000-0000-000000000000') OR
	(d.UidDepartamento = @UidDepartamento AND @UidArea = '00000000-0000-0000-0000-000000000001' AND a.UidArea IS NULL) OR
	(d.UidDepartamento = @UidDepartamento AND a.UidArea IS NOT NULL AND @UidArea = cast(cast(0 as binary) as uniqueidentifier)) OR
	(a.UidArea = @UidArea))