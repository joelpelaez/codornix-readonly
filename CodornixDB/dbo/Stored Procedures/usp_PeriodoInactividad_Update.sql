﻿CREATE PROCEDURE usp_PeriodoInactividad_Update
@UidPeriodoInactividad uniqueidentifier,
@DtFechaInicio date,
@DtFechaFin date,
@VchNotas nvarchar(200)
AS

SET NOCOUNT ON

IF @DtFechaInicio <= GETDATE()
	RETURN

IF @DtFechaFin <= GETDATE() AND @DtFechaFin < @DtFechaInicio
	RETURN

UPDATE PeriodoInactividad SET DtFechaFin = @DtFechaFin, DtFechaInicio = @DtFechaInicio, VchNotas = @VchNotas WHERE UidPeriodoInactividad = @UidPeriodoInactividad