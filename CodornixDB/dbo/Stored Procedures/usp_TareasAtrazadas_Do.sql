﻿CREATE PROCEDURE [dbo].[usp_TareasAtrazadas_Do]
@UidTarea uniqueidentifier,
@UidDepartamento uniqueidentifier = null,
@UidArea uniqueidentifier = null,
@UidCumplimiento uniqueidentifier = null,
@DtFechaHora datetime,
@UidTurno uniqueidentifier = null,
@VchEstado nvarchar(50) = 'Cancelado',
@DtFechaNueva date = null,
@Estado tinyint = null OUTPUT
AS

SET NOCOUNT ON

DECLARE @UidEC uniqueidentifier,
		@UidNO uniqueidentifier,
		@DtSiguiente date,
		@UidPeriodicidad uniqueidentifier,
		@Requerido nvarchar(50),
		@UidCumplimientoNuevo uniqueidentifier,
		@UidAntecesor uniqueidentifier;

SET @Estado = 0

DECLARE @NewDtFechaHora date = DATEADD(day, -1, @DtFechaHora);

SELECT @UidEC = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = @VchEstado;
SELECT @UidNO = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = 'No Realizado';
SELECT @UidAntecesor = UidAntecesorTarea FROM Tarea WHERE UidTarea = @UidTarea;
SELECT @UidPeriodicidad = UidPeriodicidad FROM Tarea WHERE UidTarea = @UidTarea;

SELECT @DtSiguiente = dbo.f_ObtenerPeriodicidad(@UidPeriodicidad, @NewDtFechaHora)

-- Si la fecha siguiente es hoy y se pospone la tarea, se tomará la siguiente fecha
IF @DtSiguiente = @DtFechaHora AND @VchEstado = 'Pospuesto'
	SELECT @DtSiguiente = dbo.f_ObtenerPeriodicidad(@UidPeriodicidad, @DtFechaHora)

SELECT @Requerido = VchTipoTarea FROM TipoTarea tt JOIN Tarea t ON tt.UidTipoTarea = t.UidTipoTarea WHERE t.UidTarea = @UidTarea;

IF (@VchEstado = 'Completo' ) 
BEGIN
	SET @Estado = 2
	RETURN
END

-- Si la fecha de aplazo es mayor a la de la siguiente tarea, se considera
-- como cancelado de forma automática. Pero en este caso debe invalidarse
IF @VchEstado = 'Pospuesto' AND (@DtFechaNueva >= @DtSiguiente)
BEGIN 
	SET @Estado = 1
	RETURN
END

ELSE IF @VchEstado = 'Pospuesto' AND @DtFechaNueva < @DtSiguiente
BEGIN
	SET @DtSiguiente = @DtFechaNueva
END


IF @UidCumplimiento IS NULL
BEGIN
	-- Create the current clumplieminto
	EXECUTE usp_Cumplimiento_Add @UidTarea = @UidTarea, @UidDepartamento = @UidDepartamento,
		@UidArea = @UidArea, @UidUsuario = null, @DtFechaHora = @DtFechaHora,
		@DtFechaProgramada = @DtFechaHora,
		@UidEstadoCumplimiento = @UidEC, @UidTurno = @UidTurno,
		@UidCumplimiento = @UidCumplimiento OUTPUT;

END
ELSE
	UPDATE Cumplimiento
	SET
		UidEstadoCumplimiento = @UidEC,
		DtFechaHora = @DtFechaHora,
		UidTurno = @UidTurno
	WHERE UidCumplimiento = @UidCumplimiento


-- Create the next

-- Si la tarea tiene antecesores, al ser cancelada no genera su siguiente.
IF @UidAntecesor IS NOT NULL AND @VchEstado = 'Cancelado'
	RETURN

IF @DtSiguiente IS NULL
	RETURN

EXECUTE usp_Cumplimiento_Add @UidTarea = @UidTarea, @UidDepartamento = @UidDepartamento, @UidArea = @UidArea,
	@UidUsuario = null, @DtFechaHora = null, @DtFechaProgramada = @DtSiguiente, @UidEstadoCumplimiento = @UidNO,
	@VchObservacion = null, @DcValor1 = null, @DcValor2 = null, @UidOpcion = null, @UrlFoto = null, @UidTurno = @UidTurno,
	@UidCumplimiento = @UidCumplimientoNuevo OUTPUT;

UPDATE Cumplimiento
SET
	UidCumplimientoNuevo = @UidCumplimientoNuevo
WHERE UidCumplimiento = @UidCumplimiento