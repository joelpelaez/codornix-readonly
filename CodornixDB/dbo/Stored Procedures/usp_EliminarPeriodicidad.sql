﻿CREATE PROCEDURE [dbo].[usp_EliminarPeriodicidad] (@UidPeriodicidad uniqueidentifier)
AS
BEGIN
	
	DECLARE @VchTipoFrecuencia nvarchar(50);

	SELECT @VchTipoFrecuencia = t.VchTipoFrecuencia
		FROM TipoFrecuencia t
		INNER JOIN Periodicidad p ON t.UidTipoFrecuencia = p.UidTipoFrecuencia 
		WHERE p.UidPeriodicidad = @UidPeriodicidad

	IF @VchTipoFrecuencia = 'Semanal'
	BEGIN
		DELETE FROM PeriodicidadSemanal WHERE UidPeriodicidad = @UidPeriodicidad;
	END

	ELSE IF @VchTipoFrecuencia = 'Mensual'
	BEGIN
		DELETE FROM PeriodicidadMensual WHERE UidPeriodicidad = @UidPeriodicidad;
	END

	DELETE FROM Periodicidad WHERE UidPeriodicidad = @UidPeriodicidad;
END