﻿CREATE PROCEDURE usp_AuxFolioTarea_Get
@UidSucursal uniqueidentifier,
@IntCount int OUTPUT
AS
BEGIN

	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	SELECT @IntCount = IntCount FROM AuxFolioTarea WHERE UidSucursal = @UidSucursal

	IF (@IntCount = NULL)
	BEGIN
		INSERT INTO AuxFolioTarea (UidSucursal, IntCount) VALUES (@UidSucursal, 1);
		SET @IntCount = 1;
	END
	ELSE
		UPDATE AuxFolioTarea SET IntCount = @IntCount + 1 WHERE UidSucursal = @UidSucursal;

	COMMIT 

END