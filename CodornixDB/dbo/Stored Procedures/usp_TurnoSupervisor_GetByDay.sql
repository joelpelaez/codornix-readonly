﻿CREATE PROCEDURE [dbo].[usp_TurnoSupervisor_GetByDay]
@UidUsuario uniqueidentifier,
@UidSucursal uniqueidentifier,
@DtFecha date
AS

SET NOCOUNT ON

SELECT
	ts.UidTurnoSupervisor,
	u.UidUsuario,
	ts.UidSucursal,
	ISNULL(ts.IntFolio, 0) AS IntFolio,
	ISNULL(et.VchEstadoTurno, 'No Creado') AS VchEstadoTurno,
	u.VchNombre,
	u.VchApellidoPaterno,
	u.VchApellidoMaterno,
	ts.DtFechaInicio,
	ts.DtFechaFin
FROM Usuario u
LEFT JOIN TurnoSupervisor ts ON u.UidUsuario = ts.UidUsuario AND CAST(ts.DtFechaInicio as date) = @DtFecha AND ts.UidSucursal = @UidSucursal
LEFT JOIN EstadoTurno et ON et.UidEstadoTurno = ts.UidEstadoTurno
WHERE 
u.UidUsuario = @UidUsuario