﻿CREATE PROCEDURE usp_Encargado_ModificarNumeroAsignaciones
@UidUsuario uniqueidentifier,
@IntMaxAsignaciones int
AS

SET NOCOUNT ON;

DECLARE @found uniqueidentifier;
SELECT @found = UidUsuario FROM Encargado WHERE UidUsuario = @UidUsuario;

IF @found IS NOT NULL
	UPDATE Encargado SET IntMaxAsignaciones = @IntMaxAsignaciones Where UidUsuario = @UidUsuario;
ELSE
	INSERT INTO Encargado (UidUsuario, IntMaxAsignaciones) VALUES (@UidUsuario, @IntMaxAsignaciones);