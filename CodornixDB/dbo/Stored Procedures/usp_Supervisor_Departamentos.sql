﻿CREATE PROCEDURE usp_Supervisor_Departamentos
@UidUsuario uniqueidentifier,
@DtFecha date
AS

SET NOCOUNT ON

SELECT d.UidDepartamento, d.VchNombre
FROM AsignacionSupervision s 
INNER JOIN Departamento d ON d.UidDepartamento = s.UidDepartamento
WHERE UidUsuario = @UidUsuario AND DtFechaInicio <= @DtFecha AND (DtFechaFin IS NULL OR DtFechaFin >= @DtFecha)