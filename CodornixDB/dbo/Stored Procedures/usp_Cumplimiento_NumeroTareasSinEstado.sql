﻿CREATE   PROCEDURE [dbo].[usp_Cumplimiento_NumeroTareasSinEstado]
	@UidUsuario uniqueidentifier,
	@UidPeriodo uniqueidentifier,
	@DtFecha date
AS

SET NOCOUNT ON

SELECT
	COUNT(t.UidTarea) AS NumTareasSinEstado
FROM Tarea t
INNER JOIN TipoTarea tt ON t.UidTipoTarea = tt.UidTipoTarea
INNER JOIN Estatus es ON t.UidStatus = es.UidStatus 
LEFT JOIN TareaArea ta ON t.UidTarea = ta.UidTarea
LEFT JOIN Area a ON ta.UidArea = a.UidArea
LEFT JOIN DepartamentoTarea dt ON t.UidTarea = dt.UidTarea
INNER JOIN Departamento d ON d.UidDepartamento = a.UidDepartamento OR d.UidDepartamento = dt.UidDepartamento
INNER JOIN Periodo p ON d.UidDepartamento = p.UidDepartamento
INNER JOIN Usuario u ON u.UidUsuario = p.UidUsuario
LEFT JOIN Cumplimiento c ON t.UidTarea = c.UidTarea AND (c.UidDepartamento = dt.UidDepartamento OR c.UidArea = ta.UidArea)
and (c.UidTurno =p.UidTurno OR c.UidTurno IS NULL)
LEFT JOIN EstadoCumplimiento ec ON ec.UidEstadoCumplimiento = c.UidEstadoCumplimiento
WHERE
	p.UidPeriodo = @UidPeriodo AND
	t.BitCaducado = 0 AND
	es.VchStatus = 'Activo' AND
	(ec.UidEstadoCumplimiento IS NULL OR ec.VchTipoCumplimiento = 'No Realizado') AND
	(u.UidUsuario = @UidUsuario) AND
	(p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha) AND
	(c.UidCumplimiento IS NULL OR c.DtFechaProgramada = @DtFecha)