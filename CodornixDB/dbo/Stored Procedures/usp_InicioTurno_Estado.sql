﻿CREATE PROCEDURE usp_InicioTurno_Estado
@UidInicioTurno uniqueidentifier,
@UidEstadoTurno uniqueidentifier
AS

SET NOCOUNT ON

UPDATE InicioTurno SET UidEstadoTurno = @UidEstadoTurno WHERE UidInicioTurno = @UidInicioTurno;