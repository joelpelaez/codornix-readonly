﻿CREATE PROCEDURE [dbo].[usp_Revision_CrearSucesor]
@UidCumplimiento uniqueidentifier,
@UidTareaSucesor uniqueidentifier
AS

BEGIN 

SET NOCOUNT ON

DECLARE @UidTarea uniqueidentifier,
		@DtFechaNueva date,
		@UidArea uniqueidentifier,
		@UidDepartamento uniqueidentifier,
		@IntDias int,
		@DtFechaOriginal date,
		@UidNO uniqueidentifier;


SELECT @DtFechaOriginal = CAST(DtFechaHora as date) FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento

SELECT @UidNO = UidEstadoCumplimiento FROM EstadoCumplimiento WHERE VchTipoCumplimiento = 'No Realizado';

SELECT @UidTarea = UidTarea FROM Cumplimiento WHERE UidCumplimiento = @UidCumplimiento;

SELECT @IntDias = IntDiasDespues FROM Antecesor WHERE UidTarea = @UidTareaSucesor

SELECT @UidArea = ta.UidArea FROM TareaArea ta WHERE ta.UidTarea = @UidTareaSucesor;

	IF @UidArea IS NULL
		SELECT @UidDepartamento = dt.UidDepartamento FROM DepartamentoTarea dt WHERE dt.UidTarea = @UidTarea

	SET @DtFechaNueva = DATEADD(day, @IntDias, @DtFechaOriginal)

	EXECUTE usp_Cumplimiento_Add @UidTarea = @UidTareaSucesor, @UidDepartamento = @UidDepartamento,
		@UidArea = @UidArea, @UidUsuario = null, @DtFechaHora = null, @DtFechaProgramada = @DtFechaNueva,
		@UidEstadoCumplimiento = @UidNO, @VchObservacion = null, @UrlFoto = null,
		@BitValor = null, @DcValor1 = null, @DcValor2 = null, @UidOpcion = null,
		@UidTurno = null, @UidCumplimientoAntecesor = @UidCumplimiento, @UidCumplimiento = null;

END