﻿CREATE PROCEDURE usp_Notificacion_ObtenerCorreos
@UidCumplimiento uniqueidentifier
AS

SET NOCOUNT ON

SELECT
	c.UidCumplimiento,
	u.UidUsuario,
	u.VchUsuario,
	u.VchCorreo,
	u.VchNombre,
	u.VchApellidoPaterno,
	u.VchApellidoMaterno
FROM Cumplimiento c
LEFT JOIN Area a ON c.UidArea = a.UidArea
INNER JOIN Departamento d ON c.UidDepartamento = d.UidDepartamento OR a.UidDepartamento = d.UidDepartamento
INNER JOIN Sucursal s ON s.UidSucursal = d.UidSucursal
-- Busqueda del supervisor
LEFT JOIN AsignacionSupervision spr ON spr.UidDepartamento = d.UidDepartamento 
-- Busqueda del administrador
LEFT JOIN UsuarioPerfilEmpresa upe ON upe.UidEmpresa = s.UidEmpresa
LEFT JOIN Perfil p ON upe.UidPerfil = p.UidPerfil
-- Usuario
INNER JOIN Usuario u On u.UidUsuario = spr.UidUsuario OR u.UidUsuario = upe.UidUsuario

WHERE

c.UidCumplimiento = @UidCumplimiento AND
(
-- Condiciones del supervisor
(spr.DtFechaInicio <= c.DtFechaHora AND spr.DtFechaFin >= c.DtFechaHora)
-- Condiciones del administrador
OR
(p.VchPerfil = 'Administrador')
)