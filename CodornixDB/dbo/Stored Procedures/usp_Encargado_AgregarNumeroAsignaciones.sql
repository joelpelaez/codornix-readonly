﻿CREATE PROCEDURE usp_Encargado_AgregarNumeroAsignaciones
@UidUsuario uniqueidentifier,
@IntMaxAsignaciones int
AS

SET NOCOUNT ON;

INSERT INTO Encargado (UidUsuario, IntMaxAsignaciones) VALUES (@UidUsuario, @IntMaxAsignaciones);