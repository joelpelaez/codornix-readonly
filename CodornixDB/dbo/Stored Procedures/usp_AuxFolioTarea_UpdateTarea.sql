﻿CREATE   PROCEDURE [dbo].[usp_AuxFolioTarea_UpdateTarea]
@UidTarea uniqueidentifier,
@UidSucursal uniqueidentifier = null
AS
BEGIN

	DECLARE @IntCount int;

	IF @UidSucursal IS NULL
		SET @UidSucursal = dbo.fn_Tarea_GetSucursal(@UidTarea);

	BEGIN TRANSACTION
	SET TRANSACTION ISOLATION LEVEL SERIALIZABLE

	SELECT @IntCount = IntCount FROM AuxFolioTarea WHERE UidSucursal = @UidSucursal

	IF @IntCount IS NULL
	BEGIN
		-- El primer elemento será 1 pero se guardará 2 como el siguiente disponible
		INSERT INTO AuxFolioTarea (UidSucursal, IntCount) VALUES (@UidSucursal, 2);
		SET @IntCount = 1;
	END
	ELSE
		UPDATE AuxFolioTarea SET IntCount = @IntCount + 1 WHERE UidSucursal = @UidSucursal;

	COMMIT TRANSACTION

	UPDATE Tarea SET IntFolio = @IntCount WHERE UidTarea = @UidTarea;

END