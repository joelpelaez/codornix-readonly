﻿CREATE PROCEDURE usp_TurnoSupervisor_CambiarFechaFin
@UidTurnoSupervisor uniqueidentifier,
@DtFechaFin datetimeoffset(2)
AS

SET NOCOUNT ON

UPDATE TurnoSupervisor SET DtFechaFin = @DtFechaFin WHERE UidTurnoSupervisor = @UidTurnoSupervisor