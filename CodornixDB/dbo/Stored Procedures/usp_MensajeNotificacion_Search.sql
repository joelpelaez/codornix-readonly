﻿CREATE   PROCEDURE [dbo].[usp_MensajeNotificacion_Search]
@UidSucursal uniqueidentifier,
@VchDepartamentos nvarchar(2000),
@VchDepartamentosBusqueda nvarchar(2000) = null,
@VchEstados nvarchar(2000) = null,
@DtFechaInicio date = null,
@DtFechaFin date = null
AS

BEGIN

SET NOCOUNT ON

SELECT
	mn.UidCumplimiento,
	mn.UidEstadoNotificacion,
	mn.UidMensajeNotificacion,
	t.VchNombre as VchTarea,
	CASE 
		WHEN md.VchTipoMedicion = 'Seleccionable' THEN op.VchOpciones
		WHEN md.VchTipoMedicion = 'Numerico' THEN CAST(c.DcValor1 as nvarchar)
		WHEN md.VchTipoMedicion = 'Verdadero/Falso' AND c.BitValor = 1 THEN 'Sí'
		WHEN md.VchTipoMedicion = 'Verdadero/Falso' AND c.BitValor = 0 THEN 'No'
		ELSE '(Sin valor)'
	END as VchResultado,
	d.VchNombre as VchDepartamento,
	en.VchEstadoNotificacion
FROM MensajeNotificacion mn
INNER JOIN EstadoNotificacion en ON mn.UidEstadoNotificacion = en.UidEstadoNotificacion
INNER JOIN Cumplimiento c ON mn.UidCumplimiento = c.UidCumplimiento
LEFT JOIN Area a ON c.UidArea = a.UidArea
INNER JOIN Departamento d ON (d.UidDepartamento = c.UidDepartamento OR d.UidDepartamento = a.UidDepartamento)
INNER JOIN Tarea t ON c.UidTarea = t.UidTarea
INNER JOIN Medicion md ON t.UidTipoMedicion = md.UidTipoMedicion
LEFT JOIN Opciones op ON op.UidOpciones = c.UidOpcion
WHERE
	d.UidSucursal = @UidSucursal AND
	(@DtFechaInicio IS NULL OR CAST(c.DtFechaHora as date) >= @DtFechaInicio) AND
	(@DtFechaFin IS NULL OR CAST(c.DtFechaHora as date) <= @DtFechaFin) AND
	(d.UidDepartamento IN (SELECT * FROM CSVtoTable(@VchDepartamentos, ','))) AND
	(@VchDepartamentosBusqueda IS NULL OR d.UidDepartamento IN (SELECT * FROM CSVtoTable(@VchDepartamentosBusqueda, ','))) AND
	(@VchEstados IS NULL OR en.UidEstadoNotificacion IN (SELECT * FROM CSVtoTable(@VchEstados, ',')))

END