﻿CREATE   PROCEDURE [dbo].[usp_InicioSesion_Frontend]
@UidUsuario uniqueidentifier,
@UidSucursal uniqueidentifier,
@DtFecha date
AS
BEGIN

DECLARE @result int = null;

SELECT @result = 1 FROM Periodo p
INNER JOIN Departamento d  ON p.UidDepartamento = d.UidDepartamento
WHERE [dbo].[fn_RevisarInactividad](p.UidPeriodo, @DtFecha) = 0 AND p.UidUsuario = @UidUsuario AND p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha AND d.UidSucursal = @UidSucursal

IF @result = 1
BEGIN
	SELECT @result
	RETURN
END

SELECT 1 FROM AsignacionSupervision p
INNER JOIN Departamento d  ON p.UidDepartamento = d.UidDepartamento
WHERE p.UidUsuario = @UidUsuario AND p.DtFechaInicio <= @DtFecha AND p.DtFechaFin >= @DtFecha AND d.UidSucursal = @UidSucursal

END