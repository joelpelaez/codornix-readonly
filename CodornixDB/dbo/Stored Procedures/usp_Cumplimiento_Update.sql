﻿CREATE PROCEDURE [dbo].[usp_Cumplimiento_Update]
@UidCumplimiento uniqueidentifier,
@DtFechaHora datetimeoffset(2) = null,
@VchObservacion nvarchar(200) = null,
@UrlFoto nvarchar(50) = null,
@BitValor bit = null,
@DcValor1 decimal(18, 4) = null,
@DcValor2 decimal(18, 4) = null,
@UidOpcion uniqueidentifier = null
AS

SET NOCOUNT ON

UPDATE Cumplimiento SET DtFechaHora = @DtFechaHora, VchObservacion = @VchObservacion,
	UrlFoto = @UrlFoto, BitValor = @BitValor, DcValor1 = @DcValor1, DcValor2 = @DcValor2, 
	UidOpcion = @UidOpcion