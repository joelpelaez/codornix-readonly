﻿CREATE PROCEDURE [dbo].[usp_PeriodoInactividad_Remove]
@UidPeriodoInactividad uniqueidentifier,
@DtFecha date
AS

BEGIN

SET NOCOUNT ON

DECLARE @DtFechaInicio date,
		@DtFechaFin date,
		@UidPeriodicidad uniqueidentifier;

SELECT @DtFechaInicio = DtFechaInicio, @DtFechaFin = DtFechaFin, @UidPeriodicidad = UidPeriodicidad FROM PeriodoInactividad WHERE UidPeriodoInactividad = @UidPeriodoInactividad;

-- Si el periodo de inactividad no ha empezado, se eliminará completamente.
IF @DtFechaInicio > @DtFecha
BEGIN
	
	DELETE FROM PeriodoInactividad WHERE UidPeriodoInactividad = @UidPeriodoInactividad
	EXECUTE usp_EliminarPeriodicidad @UidPeriodicidad = null;
END
-- Si el periodo de inactvidad no ha acabado, solo se recortaran los dias que no han pasado.
ELSE IF @DtFechaFin > @DtFecha
	UPDATE PeriodoInactividad SET DtFechaFin = @DtFechaFin WHERE UidPeriodoInactividad = @UidPeriodoInactividad

-- En caso de que es un periodo pasado no se puede eliminar ni modificar (fines historicos).

END