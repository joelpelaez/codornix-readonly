﻿CREATE PROCEDURE [dbo].[usp_AsignarPeriodoInactividad_Search]
@UidSucursal uniqueidentifier,
@UidDepartamentos nvarchar(2000) = null,
@DtFechaInicio date,
@DtFechaFin date
AS

BEGIN

SET NOCOUNT ON

SELECT
	pp.UidPeriodoInactividad,
	p.UidPeriodo,
	p.UidDepartamento,
	pp.DtFechaInicio,
	pp.DtFechaFin,
	d.VchNombre AS VchDepartamento,
	dbo.fn_ObtenerNumeroFechasPeriodicidad(pp.UidPeriodicidad, CASE WHEN pp.DtFechaInicio > @DtFechaInicio THEN pp.DtFechaInicio ELSE @DtFechaInicio END, CASE WHEN pp.DtFechaFin < @DtFechaFin THEN pp.DtFechaFin ELSE @DtFechaFin END) AS IntDiasInactividad 
FROM PeriodoInactividad AS pp
INNER JOIN Periodo AS p ON pp.UidPeriodo = p.UidPeriodo
INNER JOIN Departamento AS d ON p.UidDepartamento = d.UidDepartamento
WHERE
	d.UidSucursal = @UidSucursal AND
	pp.DtFechaInicio >= @DtFechaInicio AND
	pp.DtFechaFin <= @DtFechaFin AND
	(@UidDepartamentos IS NULL OR d.UidDepartamento IN (SELECT * FROM CSVtoTable(@UidDepartamentos, ','))) 

END