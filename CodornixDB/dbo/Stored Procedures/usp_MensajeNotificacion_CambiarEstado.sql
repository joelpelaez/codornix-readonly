﻿CREATE PROCEDURE usp_MensajeNotificacion_CambiarEstado
@UidMensajeNotificacion uniqueidentifier,
@VchEstado nvarchar(40)
AS
BEGIN

SET NOCOUNT ON

DECLARE @UidEstadoNotificacion uniqueidentifier;

SELECT @UidEstadoNotificacion = UidEstadoNotificacion FROM EstadoNotificacion WHERE VchEstadoNotificacion = @VchEstado;

UPDATE MensajeNotificacion SET UidEstadoNotificacion = @UidEstadoNotificacion WHERE UidMensajeNotificacion = @UidMensajeNotificacion

END