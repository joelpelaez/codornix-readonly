﻿CREATE PROCEDURE usp_EstadoTurno_Find
@UidEstadoTurno uniqueidentifier
AS

SET NOCOUNT ON

SELECT * FROM EstadoTurno WHERE UidEstadoTurno = @UidEstadoTurno;