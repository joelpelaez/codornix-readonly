﻿CREATE TABLE [dbo].[PeriodoInactividad] (
    [UidPeriodoInactividad] UNIQUEIDENTIFIER NOT NULL,
    [UidPeriodo]            UNIQUEIDENTIFIER NOT NULL,
    [UidTipoInactividad]    UNIQUEIDENTIFIER NOT NULL,
    [UidPeriodicidad]       UNIQUEIDENTIFIER NULL,
    [DtFechaInicio]         DATE             NOT NULL,
    [DtFechaFin]            DATE             NOT NULL,
    [VchNotas]              NVARCHAR (200)   NULL,
    CONSTRAINT [PK_PeriodoInactividad] PRIMARY KEY CLUSTERED ([UidPeriodoInactividad] ASC),
    CONSTRAINT [FK_PeriodoInactividad_Periodicidad] FOREIGN KEY ([UidPeriodicidad]) REFERENCES [dbo].[Periodicidad] ([UidPeriodicidad]),
    CONSTRAINT [FK_PeriodoInactividad_Periodo] FOREIGN KEY ([UidPeriodo]) REFERENCES [dbo].[Periodo] ([UidPeriodo]),
    CONSTRAINT [FK_PeriodoInactividad_TipoInactividad] FOREIGN KEY ([UidTipoInactividad]) REFERENCES [dbo].[TipoInactividad] ([UidTipoInactividad])
);




GO
CREATE NONCLUSTERED INDEX [IX_PeriodoInactividad_TipoInactividad]
    ON [dbo].[PeriodoInactividad]([UidTipoInactividad] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PeriodoInactividad_Periodicidad]
    ON [dbo].[PeriodoInactividad]([UidPeriodicidad] ASC);


GO
CREATE NONCLUSTERED INDEX [IX_PeriodoInactividad_Periodo]
    ON [dbo].[PeriodoInactividad]([UidPeriodo] ASC);

