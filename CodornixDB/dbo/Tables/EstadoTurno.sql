﻿CREATE TABLE [dbo].[EstadoTurno] (
    [UidEstadoTurno] UNIQUEIDENTIFIER NOT NULL,
    [VchEstadoTurno] NVARCHAR (50)    NOT NULL,
    CONSTRAINT [PK_EstadoTurno] PRIMARY KEY CLUSTERED ([UidEstadoTurno] ASC)
);

