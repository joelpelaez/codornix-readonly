﻿CREATE TABLE [dbo].[AuxFolioTarea] (
    [UidSucursal] UNIQUEIDENTIFIER NOT NULL,
    [IntCount]    INT              NOT NULL,
    CONSTRAINT [PK_AuxFolioTarea] PRIMARY KEY CLUSTERED ([UidSucursal] ASC),
    CONSTRAINT [FK_AuxFolioTarea_Sucursal] FOREIGN KEY ([UidSucursal]) REFERENCES [dbo].[Sucursal] ([UidSucursal])
);

