﻿CREATE TABLE [dbo].[Notificacion] (
    [UidNotificacion]      UNIQUEIDENTIFIER NOT NULL,
    [UidTarea]             UNIQUEIDENTIFIER NOT NULL,
    [BlValor]              BIT              NULL,
    [DcMenorQue]           DECIMAL (18, 4)  NULL,
    [DcMayorQue]           DECIMAL (18, 4)  NULL,
    [BitMenorIgual]        BIT              NULL,
    [BitMayorIgual]        BIT              NULL,
    [VchOpciones]          NVARCHAR (2000)  NULL,
    [IntMaxNotificaciones] INT              NULL,
    CONSTRAINT [PK_Notificacion] PRIMARY KEY CLUSTERED ([UidNotificacion] ASC),
    CONSTRAINT [FK_Notificacion_Tarea] FOREIGN KEY ([UidTarea]) REFERENCES [dbo].[Tarea] ([UidTarea])
);



