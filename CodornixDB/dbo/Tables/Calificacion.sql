﻿CREATE TABLE [dbo].[Calificacion] (
    [UidCalificacion] UNIQUEIDENTIFIER NOT NULL,
    [VchCalificacion] NVARCHAR (20)    NOT NULL,
    [IntOrden]        INT              NOT NULL,
    CONSTRAINT [PK_Calificacion] PRIMARY KEY CLUSTERED ([UidCalificacion] ASC)
);

