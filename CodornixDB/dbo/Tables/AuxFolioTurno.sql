﻿CREATE TABLE [dbo].[AuxFolioTurno] (
    [UidSucursal] UNIQUEIDENTIFIER NOT NULL,
    [IntCount]    INT              NOT NULL,
    CONSTRAINT [PK_AuxFolioTurno] PRIMARY KEY CLUSTERED ([UidSucursal] ASC),
    CONSTRAINT [FK_AuxFolioTurno_Sucursal] FOREIGN KEY ([UidSucursal]) REFERENCES [dbo].[Sucursal] ([UidSucursal])
);

