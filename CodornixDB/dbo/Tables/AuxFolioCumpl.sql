﻿CREATE TABLE [dbo].[AuxFolioCumpl] (
    [UidTarea] UNIQUEIDENTIFIER NOT NULL,
    [IntCount] INT              NOT NULL,
    CONSTRAINT [PK_AuxFolioCumpl] PRIMARY KEY CLUSTERED ([UidTarea] ASC),
    CONSTRAINT [FK_AuxFolioCumpl_Tarea] FOREIGN KEY ([UidTarea]) REFERENCES [dbo].[Tarea] ([UidTarea])
);

