﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using CodorniX.Modelo;
using System.Globalization;

namespace CodorniX.Vista
{
	public partial class HistoricoTareas : System.Web.UI.Page
	{
		private Sesion SesionActual
		{
			get => (Sesion)Session["Sesion"];
		}
		/// <summary>
		/// Lista de tareas obtenidas de acuerdo a la busqueda realizada
		/// </summary>
		private List<Tarea> LsTareas
		{
			get
			{
				return (List<Tarea>)ViewState["Tareas"];
			}
			set
			{
				ViewState["Tareas"] = value;
			}
		}
		private List<Modelo.Cumplimiento> LsCumplimientos
		{
			get
			{
				return (List<Modelo.Cumplimiento>)ViewState["Cumplimientos"];
			}
			set
			{
				ViewState["Cumplimientos"] = value;
			}
		}
		/// <summary>
		/// Direccion de ordenamiento de la lista de tareas
		/// </summary>
		private string SortDirection
		{
			get
			{
				return (string)ViewState["Sort"];
			}
			set
			{
				ViewState["Sort"] = value;
			}
		}
		private Guid UidTareaSeleccionada
		{
			get
			{
				return (Guid)ViewState["UidTarea"];
			}
			set
			{
				ViewState["UidTarea"] = value;
			}
		}
		VMHistoricoTarea VmHistorico = new VMHistoricoTarea();

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				pnlAlertBusqueda.Visible = false;
				pnlAlertGeneral.Visible = false;
				pnlFiltros.Visible = true;
				pnlListaTareas.Visible = false;
				pnlDetallesCumplimiento.Visible = false;

				UidTareaSeleccionada = Guid.Empty;
				SortDirection = "ASC";

				gvCumplimientos.DataSource = null;
				gvCumplimientos.DataBind();

				VmHistorico.GetEstadosCumplimiento();
				ddlFiltroEstadoCumplimiento.DataSource = VmHistorico.LsEstadosCumplimiento;
				ddlFiltroEstadoCumplimiento.DataTextField = "StrEstadoCumplimiento";
				ddlFiltroEstadoCumplimiento.DataValueField = "UidEstadoCumplimiento";
				ddlFiltroEstadoCumplimiento.DataBind();

				btnLimpiarFiltrosCumplimiento.CssClass = "btn btn-sm btn-default disabled";
				btnBuscarCumplimiento.CssClass = "btn btn-sm btn-default disabled";

				txtFiltroFolioCumplimiento.Enabled = false;
				txtFiltroFechaInicioCumplimiento.Enabled = false;
				txtFiltroFechaFinCumplimiento.Enabled = false;
				ddlFiltroEstadoCumplimiento.Enabled = false;

			}
		}

		#region Methods
		#region Left
		#region Buttons
		protected void btnOcultarFiltros_Click(object sender, EventArgs e)
		{
			if (lblOcultarFiltros.Text.Equals("Mostrar"))
			{
				lblOcultarFiltros.Text = "Ocultar";
				pnlFiltros.Visible = false;
				pnlListaTareas.Visible = true;

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
			}
			else
			{
				lblOcultarFiltros.Text = "Mostrar";
				pnlFiltros.Visible = true;
				pnlListaTareas.Visible = false;
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";
			}
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroFolio.Text = string.Empty;
			txtFiltroDescripcion.Text = string.Empty;
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertBusqueda.Visible)
					pnlAlertBusqueda.Visible = false;

				int? FolioTarea = null;
				string Nombre = txtFiltroDescripcion.Text.Trim() == string.Empty ? string.Empty : txtFiltroDescripcion.Text.Trim();
				if (txtFiltroFolio.Text != string.Empty)
				{
					int Aux = 0;
					if (int.TryParse(txtFiltroFolio.Text.Trim(), out Aux))
					{
						FolioTarea = Aux;
					}
					else
					{
						pnlAlertBusqueda.Visible = true;
						lblErrorBusqueda.Text = "El folio ingresado es invalido";
						return;
					}
				}

				VmHistorico.GetTareasDepartamento(SesionActual.UidDepartamentos, FolioTarea, Nombre);
				this.LsTareas = VmHistorico.LsTareas;
				gvTareasDepartamento.DataSource = VmHistorico.LsTareas;
				gvTareasDepartamento.DataBind();

				pnlFiltros.Visible = false;
				pnlListaTareas.Visible = true;

				lblOcultarFiltros.Text = "Ocultar";
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
			}
			catch (Exception ex)
			{
				pnlAlertBusqueda.Visible = true;
				lblErrorBusqueda.Text = "Error al realizar busqueda: " + ex.Message;
			}
		}
		protected void CloseAlertDialogSearch(object sender, EventArgs e)
		{
			pnlAlertBusqueda.Visible = false;
		}

		protected void btnCerrarDetalleCumplimiento_Click(object sender, EventArgs e)
		{
			pnlBotonesIzquierda.Visible = true;
			pnlDetallesCumplimiento.Visible = false;

			if (lblOcultarFiltros.Text.Equals("Mostrar"))
			{
				pnlFiltros.Visible = true;
				pnlListaTareas.Visible = false;
			}
			else
			{
				pnlFiltros.Visible = false;
				pnlListaTareas.Visible = true;
			}
		}
		#endregion
		#region GridView
		protected void gvTareasDepartamento_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareasDepartamento, "Select$" + e.Row.RowIndex);

				if (e.Row.Cells[3].Text.Equals("Requerida"))
				{
					e.Row.CssClass = "font-bold";
				}
			}
		}
		protected void gvTareasDepartamento_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvTareasDepartamento.SelectedIndex = -1;
			gvTareasDepartamento.DataSource = LsTareas;
			gvTareasDepartamento.PageIndex = e.NewPageIndex;
			gvTareasDepartamento.DataBind();
		}
		protected void gvTareasDepartamento_Sorting(object sender, GridViewSortEventArgs e)
		{
			string SortExpression = e.SortExpression;
			switch (SortExpression)
			{
				case "Folio":
					if (SortDirection.Equals("ASC"))
					{
						LsTareas = LsTareas.OrderBy(t => t.IntFolio).ToList();
					}
					else if (SortDirection.Equals("DESC"))
					{
						LsTareas = LsTareas.OrderByDescending(t => t.IntFolio).ToList();
					}
					break;
				case "Nombre":
					if (SortDirection.Equals("ASC"))
					{
						LsTareas = LsTareas.OrderBy(t => t.StrNombre).ToList();
					}
					else if (SortDirection.Equals("DESC"))
					{
						LsTareas = LsTareas.OrderByDescending(t => t.StrNombre).ToList();
					}
					break;
				case "Tipo":
					if (SortDirection.Equals("ASC"))
					{
						LsTareas = LsTareas.OrderBy(t => t.StrTipoTarea).ToList();
					}
					else if (SortDirection.Equals("DESC"))
					{
						LsTareas = LsTareas.OrderByDescending(t => t.StrTipoTarea).ToList();
					}
					break;
				default:
					break;
			}
			if (SortDirection.Equals("ASC"))
				SortDirection = "DESC";
			else
				SortDirection = "ASC";

			if (this.UidTareaSeleccionada != Guid.Empty)
			{
				
			}

			gvTareasDepartamento.DataSource = LsTareas;
			gvTareasDepartamento.DataBind();
		}
		protected void gvTareasDepartamento_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidTarea = new Guid(gvTareasDepartamento.SelectedDataKey.Value.ToString());
				Guid UidDepartamento = new Guid(gvTareasDepartamento.SelectedDataKey.Values[1].ToString());
				Guid UidArea = new Guid(gvTareasDepartamento.SelectedDataKey.Values[2].ToString());
				Guid UidPeriodicidad = new Guid(gvTareasDepartamento.SelectedDataKey.Values[3].ToString());

				UidTareaSeleccionada = UidTarea;

				VmHistorico.GetTarea(UidTarea);
				VmHistorico.GetDepartamento(UidDepartamento);
				VmHistorico.GetArea(UidArea);
				VmHistorico.GetPeriodicidad(UidPeriodicidad);
				VmHistorico.GetTipoFrecuenciaByTarea(UidTarea);

				lblTituloPanelDerecho.Text = "Historico Tarea: " + VmHistorico._Tarea.IntFolio.ToString("0000");
				lblNombreTarea.Text = VmHistorico._Tarea.StrNombre;
				lblDescripcion.Text = VmHistorico._Tarea.StrDescripcion == string.Empty ? "(sin descripción)" : VmHistorico._Tarea.StrDescripcion;
				lblTipoTarea.Text = VmHistorico._Tarea.StrTipoTarea;
				lblDepartamento.Text = VmHistorico._Departamento.StrNombre;
				lblPeriodicidad.Text = VmHistorico._Frecuencia.StrTipoFrecuencia;
				lblArea.Text = VmHistorico._Area == null ? "(global)" : VmHistorico._Area.StrNombre;

				VmHistorico.GetCumplimientoTarea(UidTarea, SesionActual.uidUsuario, null, null, null, Guid.Empty);
				this.LsCumplimientos = VmHistorico.LsCumplimientosTarea;
				gvCumplimientos.SelectedIndex = -1;
				gvCumplimientos.DataSource = VmHistorico.LsCumplimientosTarea;
				gvCumplimientos.DataBind();

				btnLimpiarFiltrosCumplimiento.CssClass = "btn btn-sm btn-default";
				btnBuscarCumplimiento.CssClass = "btn btn-sm btn-default";

				txtFiltroFolioCumplimiento.Enabled = true;
				txtFiltroFechaInicioCumplimiento.Enabled = true;
				txtFiltroFechaFinCumplimiento.Enabled = true;
				ddlFiltroEstadoCumplimiento.Enabled = true;

				gvCumplimientos.SelectedIndex = -1;
			}
			catch (Exception)
			{
				pnlAlertBusqueda.Visible = true;
				lblErrorBusqueda.Text = "Error al obtener datos";
				throw;
			}
		}
		#endregion

		#endregion

		#region Right
		#region Buttons
		protected void btnOcultarFiltrosCumplimiento_Click(object sender, EventArgs e)
		{
			if (lblMostrarFiltrosCumplimiento.Text.Equals("Ocultar"))
			{
				lblMostrarFiltrosCumplimiento.Text = "Mostrar";
				pnlFiltrosCumplimiento.Visible = false;

				btnLimpiarFiltrosCumplimiento.CssClass = "btn btn-sm btn-default disabled";
				btnBuscarCumplimiento.CssClass = "btn btn-sm btn-default disabled";
			}
			else
			{
				lblMostrarFiltrosCumplimiento.Text = "Ocultar";
				pnlFiltrosCumplimiento.Visible = true;

				if (this.UidTareaSeleccionada != Guid.Empty)
				{
					btnLimpiarFiltrosCumplimiento.CssClass = "btn btn-sm btn-default";
					btnBuscarCumplimiento.CssClass = "btn btn-sm btn-default";
				}
			}
		}
		protected void btnLimpiarFiltrosCumplimiento_Click(object sender, EventArgs e)
		{
			txtFiltroFolioCumplimiento.Text = string.Empty;
			txtFiltroFechaInicioCumplimiento.Text = string.Empty;
			txtFiltroFechaFinCumplimiento.Text = string.Empty;
			ddlFiltroEstadoCumplimiento.SelectedIndex = 0;
		}
		protected void btnBuscarCumplimiento_Click(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertGeneral.Visible)
					pnlAlertGeneral.Visible = false;

				int? FolioCumplimiento = null;
				DateTime? DtFechaInicio = null;
				DateTime? DtFechaFin = null;
				Guid UidEstado = new Guid(ddlFiltroEstadoCumplimiento.SelectedValue.ToString());

				if (txtFiltroFolioCumplimiento.Text.Trim() != string.Empty)
				{
					int Aux = 0;
					if (int.TryParse(txtFiltroFolioCumplimiento.Text.Trim(), out Aux))
					{
						FolioCumplimiento = Aux;
					}
					else
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Ingrese un folio numerico valido.";
					}
				}

				if (txtFiltroFechaInicioCumplimiento.Text.Trim() != string.Empty)
				{
					try
					{
						DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicioCumplimiento.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					catch (Exception)
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Formato de fecha de inicio invalida.";
						return;
					}
				}

				if (txtFiltroFechaFinCumplimiento.Text.Trim() != string.Empty)
				{
					try
					{
						DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFinCumplimiento.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					catch (Exception)
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Formato de fecha fin invalida.";
						return;
					}
				}

				VmHistorico.GetCumplimientoTarea(this.UidTareaSeleccionada, SesionActual.uidUsuario, FolioCumplimiento, DtFechaInicio, DtFechaFin, UidEstado);
				this.LsCumplimientos = VmHistorico.LsCumplimientosTarea;
				gvCumplimientos.DataSource = VmHistorico.LsCumplimientosTarea;
				gvCumplimientos.DataBind();
			}
			catch (Exception ex)
			{
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Error: " + ex.Message;
			}
		}

		protected void CloseAlertDialogGeneral(object sender, EventArgs e)
		{
			pnlAlertGeneral.Visible = false;
		}
		#endregion

		#region GridView
		protected void gvCumplimientos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvCumplimientos, "Select$" + e.Row.RowIndex);
				if (e.Row.Cells[5].Text.Equals("True"))
				{
					e.Row.Cells[3].CssClass = "txt-warning";
				}
			}
		}
		protected void gvCumplimientos_Sorting(object sender, GridViewSortEventArgs e)
		{
			string SortExpression = e.SortExpression;
			switch (SortExpression)
			{
				case "FolioT":
					if (hfSortDirectionGvCumplimientos.Value.Equals("ASC"))
						this.LsCumplimientos = this.LsCumplimientos.OrderBy(c => c.IntFolioTurno).ToList();
					else
						this.LsCumplimientos = this.LsCumplimientos.OrderByDescending(c => c.IntFolioTurno).ToList();
					break;
				case "FolioC":
					if (hfSortDirectionGvCumplimientos.Value.Equals("ASC"))
						this.LsCumplimientos = this.LsCumplimientos.OrderBy(c => c.IntFolio).ToList();
					else
						this.LsCumplimientos = this.LsCumplimientos.OrderByDescending(c => c.IntFolio).ToList();
					break;
				case "Fecha":
					if (hfSortDirectionGvCumplimientos.Value.Equals("ASC"))
						this.LsCumplimientos = this.LsCumplimientos.OrderBy(c => c.DtFechaHora).ToList();
					else
						this.LsCumplimientos = this.LsCumplimientos.OrderByDescending(c => c.DtFechaHora).ToList();
					break;
				case "Estado":
					if (hfSortDirectionGvCumplimientos.Value.Equals("ASC"))
						this.LsCumplimientos = this.LsCumplimientos.OrderBy(c => c.StrEstadoCumplimiento).ToList();
					else
						this.LsCumplimientos = this.LsCumplimientos.OrderByDescending(c => c.StrEstadoCumplimiento).ToList();
					break;
				default:
					break;
			}
			if (hfSortDirectionGvCumplimientos.Value.Equals("ASC"))
				hfSortDirectionGvCumplimientos.Value = "DESC";
			else
				hfSortDirectionGvCumplimientos.Value = "ASC";

			gvCumplimientos.SelectedIndex = -1;
			gvCumplimientos.DataSource = this.LsCumplimientos;
			gvCumplimientos.DataBind();
		}
		protected void gvCumplimientos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidCumplimiento = new Guid(gvCumplimientos.SelectedDataKey.Value.ToString());
				lblTareaAtrasada.Visible = false;

				VmHistorico.GetCumplimientoById(UidCumplimiento);
				if (VmHistorico._Cumplimiento.BlAtrasada.Value)
				{
					lblTareaAtrasada.Visible = true;
					txtFechaCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaAtrasada == null ? "(Sin fecha)" : VmHistorico._Cumplimiento.DtFechaAtrasada.Value.ToString("dd/MM/yyyy");
					txtHoraCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaAtrasada == null ? "(Sin hora)" : VmHistorico._Cumplimiento.DtFechaAtrasada.Value.ToString("hh:mm tt");
				}
				else
				{
					txtFechaCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaHora == null ? "(Sin fecha)" : VmHistorico._Cumplimiento.DtFechaHora.Value.ToString("dd/MM/yyyy");
					txtHoraCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaHora == null ? "(Sin hora)" : VmHistorico._Cumplimiento.DtFechaHora.Value.ToString("hh:mm tt");
				}

				txtObservaciones.Text = VmHistorico._Cumplimiento.StrObservacion == string.Empty ? "(Sin observaciones)" : VmHistorico._Cumplimiento.StrObservacion;
				VmHistorico.GetTarea(VmHistorico._Cumplimiento.UidTarea);
				pnlCumplimientoSeleccion.Visible = false;
				pnlCumplimientoValor.Visible = false;
				pnlCumplimientoVerdaderoFalso.Visible = false;

				string Tipo = VmHistorico._Tarea.StrTipoMedicion;
				rbNo.Checked = false;
				rbYes.Checked = false;
				txtValorIngresado.Text = string.Empty;
				switch (Tipo)
				{
					case "Verdadero/Falso":
						pnlCumplimientoVerdaderoFalso.Visible = true;
						if (VmHistorico._Cumplimiento.BlValor != null)
						{
							if (VmHistorico._Cumplimiento.BlValor.Value)
								rbYes.Checked = true;
							else
								rbNo.Checked = true;
						}
						break;
					case "Numerico":
						pnlCumplimientoValor.Visible = true;
						if (VmHistorico._Cumplimiento.DcValor1 != null)
							txtValorIngresado.Text = VmHistorico._Cumplimiento.DcValor1.Value.ToString("0.00");
						else
							txtValorIngresado.Text = "0.00";
						break;
					case "Seleccionable":
						pnlCumplimientoSeleccion.Visible = true;
						VmHistorico.GetOpcionesTarea(VmHistorico._Cumplimiento.UidTarea);
						ddlOpcionSeleccionada.DataSource = VmHistorico.LsOpcionesTarea;
						ddlOpcionSeleccionada.DataTextField = "StrOpciones";
						ddlOpcionSeleccionada.DataValueField = "UidOpciones";
						ddlOpcionSeleccionada.DataBind();
						if (VmHistorico._Cumplimiento.UidOpcion != null)
						{
							ddlOpcionSeleccionada.SelectedValue = VmHistorico._Cumplimiento.UidOpcion.Value.ToString();
						}
						break;
					default:
						break;
				}
				pnlDetallesCumplimiento.Visible = true;
				pnlAlertBusqueda.Visible = false;
				pnlFiltros.Visible = false;
				pnlListaTareas.Visible = false;
				pnlBotonesIzquierda.Visible = false;
			}
			catch (Exception)
			{
				throw;
			}
		}
		#endregion

		#endregion

		#endregion
	}
}