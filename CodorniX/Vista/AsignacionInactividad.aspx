﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="AsignacionInactividad.aspx.cs" Inherits="CodorniX.Vista.AsignacionInactividad" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Asignación de Inactidad</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-5 col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="text-center">
								Lista de Inactividades
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body panel-pd">
					<div class="col-xs-12 pd-left-right-5">
						<div class="text-right">
							<div class="btn-group">
								<asp:LinkButton ID="btnMostrar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click">
									<span class="glyphicon glyphicon-eye-close"></span>
									<asp:Label Text="Ocultar" runat="server" ID="lblOcultarFiltros" />
								</asp:LinkButton>
								<asp:LinkButton ID="btnLimpiar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
                                    <span class="glyphicon glyphicon-trash"></span>
                                    Limpiar
								</asp:LinkButton>
								<asp:LinkButton ID="btnBuscar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnBuscar_Click">
                                    <span class="glyphicon glyphicon-search"></span>
                                    Buscar
								</asp:LinkButton>
							</div>
						</div>
					</div>
					<asp:Panel runat="server" ID="pnlAlertBusqueda" CssClass="col-md-12 pd-left-right-5">
						<div class="alert alert-danger">
							<asp:Label Text="Error: " runat="server" ID="lblAlertBusqueda" />
						</div>
					</asp:Panel>
					<asp:PlaceHolder ID="panelBusqueda" runat="server">
						<div class="col-xs-6 pd-left-right-5">
							<small>Fecha inicio</small>
							<div class="date input-group start-date">
								<asp:TextBox ID="txtFechaInicio" runat="server" CssClass="form-control" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar" />
								</span>
							</div>
						</div>
						<div class="col-xs-6 pd-left-right-5">
							<small>Fecha fin</small>
							<div class="date input-group end-date">
								<asp:TextBox ID="txtFechaFin" runat="server" CssClass="form-control" />
								<span class="input-group-addon">
									<span class="glyphicon glyphicon-calendar" />
								</span>
							</div>
						</div>
						<div class="col-xs-12 pd-left-right-5">
							<h6>Departamentos</h6>
							<asp:ListBox ID="lbDepartamentos" runat="server" CssClass="form-control" Rows="6" SelectionMode="Multiple" />
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="panelResultados" runat="server">
						<div class="col-xs-12 pd-left-right-5">
							<asp:GridView ID="dgvListaInactividades" runat="server" AutoGenerateColumns="false" CssClass="table table-sm table-bordered table-condensed input-sm" DataKeyNames="UidPeriodoInactividad,DtFechaInicio,DtFechaFin" OnSelectedIndexChanged="dgvListaInactividades_SelectedIndexChanged" OnRowDataBound="dgvListaInactividades_RowDataBound" SelectedRowStyle-BackColor="#dff0d8">
								<EmptyDataTemplate>
									No existen días sin asignar durante el periodo especificado.
								</EmptyDataTemplate>
								<Columns>
									<asp:TemplateField HeaderText="Encargado">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("Encargado") %>' runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
									<asp:BoundField DataField="DtFechaInicio" HeaderText="Fecha Inicio" SortExpression="FechaInicio" DataFormatString="{0:dd/MM/yyyy}" />
									<asp:BoundField DataField="DtFechaFin" HeaderText="Fecha Fin" SortExpression="FechaFin" DataFormatString="{0:dd/MM/yyyy}" />
									<asp:BoundField DataField="IntDiasInactividad" HeaderText="Días inactivos" SortExpression="Dias" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
		<div class="col-md-7 col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Días de inactividad							
				</div>
				<div class="panel-body panel-pd">
					<asp:Panel runat="server" ID="pnlAlertGeneral" CssClass="alert alert-danger">
						<small>
							<asp:Label ID="lblAlertGeneral" runat="server" Text="Error: " />
						</small>
						<asp:LinkButton runat="server" class="close" OnClick="btnCloseAlertDialogGeneral">
							<span aria-hidden="true">&times;</span>
						</asp:LinkButton>
					</asp:Panel>
					<div class="col-xs-6 pd-0">
						<div class="col-md-12 pd-0">
							<div class="col-md-12 pd-left-right-5 text-right">
								<div class="btn-group">
									<asp:LinkButton ID="btnOcultarFiltrosDiasInactividad" runat="server" CssClass="btn btn-sm btn-default" OnClick="OcultarFiltrosDiasInactividad_Click">
										<span class="glyphicon glyphicon-eye-close"></span>
										<asp:Label Text="Ocultar" runat="server" ID="lblOcultarFiltrosDiasInactividad" />
									</asp:LinkButton>
									<asp:LinkButton ID="btnBuscarDiasInactividad" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnBuscarDiasInactividad_Click">
										<span class="glyphicon glyphicon-search"></span>
										Buscar
									</asp:LinkButton>
								</div>
							</div>
							<asp:Panel runat="server" ID="pnlFiltrosDiasInactividad">
								<div class="col-md-6 pd-left-right-5">
									<small>Inicio</small>
									<div class="date start-date input-group">
										<asp:TextBox ID="txtFiltroDiaFechaInicio" runat="server" CssClass="form-control input-sm" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar" />
										</span>
									</div>
								</div>
								<div class="col-md-6 pd-left-right-5">
									<small>Fin</small>
									<div class="date end-date input-group">
										<asp:TextBox ID="txtFiltroDiaFechaFin" runat="server" CssClass="form-control input-sm" />
										<span class="input-group-addon">
											<span class="glyphicon glyphicon-calendar" />
										</span>
									</div>
								</div>
							</asp:Panel>
						</div>
						<div class="col-xs-12 pd-left-right-5">
							<table style="width: 100%;">
								<tr>
									<td>
										<h5><strong>Dias</strong> de inactividad</h5>
									</td>
									<td class="text-right">
										<asp:LinkButton ID="btnQuitarTodos" runat="server" CssClass="btn btn-xs btn-danger" OnClick="btnQuitarTodos_Click">
											<span class=""></span>
											Deseleccionar
										</asp:LinkButton>
									</td>
								</tr>
							</table>
						</div>
						<div class="col-xs-12 pd-left-right-5" style="overflow-y: scroll; height: 340px;">
							<div class="table-responsive">
								<asp:GridView ID="dgvDiasInactividad" runat="server" AutoGenerateColumns="false" CssClass="table table-sm table-bordered table-hover table-condensed input-sm" DataKeyNames="DtFecha" OnSelectedIndexChanged="dgvDiasInactividad_SelectedIndexChanged" OnRowDataBound="dgvDiasInactividad_RowDataBound" OnRowCommand="dgvDiasInactividad_RowCommand">
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No hay días inactivos próximos.
										</div>
									</EmptyDataTemplate>
									<Columns>
										<asp:TemplateField>
											<HeaderTemplate>
												<span>
													<asp:CheckBox ID="cbTodos" runat="server" AutoPostBack="true" OnCheckedChanged="cbTodos_CheckedChanged" /></span>
											</HeaderTemplate>
											<ItemTemplate>
												<span>
													<asp:CheckBox ID="cbSeleccionado" runat="server" AutoPostBack="true" OnCheckedChanged="cbSeleccionado_CheckedChanged" />
												</span>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="DtFecha" HeaderText="Fecha" DataFormatString="{0:dd/MM/yyyy}" />
										<asp:TemplateField HeaderText="Opciones">
											<ItemTemplate>
												<asp:LinkButton ID="QuitarButton" runat="server" CommandName="Quitar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" CssClass="btn btn-danger btn-xs">
													<span class="glyphicon glyphicon-trash"></span>
													Quitar
												</asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField ItemStyle-CssClass="hidden" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" DataField="BlAsignado" HeaderText="Asignado" ReadOnly="true" />
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</div>
					<div class="col-xs-6 pd-0">
						<div class="col-md-12 pd-left-right-5 text-right">
							<div class="btn-group">
								<asp:LinkButton runat="server" ID="btnOcultarFiltrosEncargado" CssClass="btn btn-sm btn-default" OnClick="btnOcultarFiltrosEncargado_Click">
									<span class="glyphicon glyphicon-eye-close"></span>
									<asp:Label ID="lblOcultarFiltrosEncargado" Text="Ocultar" runat="server" />
								</asp:LinkButton>
								<asp:LinkButton runat="server" ID="btnLimpiarFiltrosEncargado" CssClass="btn btn-sm btn-default" OnClick="btnLimpiarFiltrosEncargado_Click">
									<span class="glyphicon glyphicon-trash"></span>
									Limpiar
								</asp:LinkButton>
								<asp:LinkButton ID="btnBuscarEncargado" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnBuscarEncargado_Click">
									<span class="glyphicon glyphicon-search"></span>
									Buscar
								</asp:LinkButton>
							</div>
						</div>
						<asp:Panel runat="server" ID="pnlFiltrosEncargado">
							<div class="col-md-12 pd-left-right-5">
								<small>Nombre del encargado</small>
								<asp:TextBox ID="txtNombreEncargado" runat="server" CssClass="form-control input-sm" />
							</div>
						</asp:Panel>
						<div class="col-xs-12 pd-left-right-5" style="height: 330px; overflow-y: scroll;">
							<h5>
								<strong>Encargados</strong>
								disponibles
							</h5>
							<div class="table-responsive">
								<asp:GridView ID="dgvEncargados" runat="server" DataKeyNames="UidUsuario" AutoGenerateColumns="false" CssClass="table table-sm table-bordered table-condensed input-sm" OnSelectedIndexChanged="dgvEncargados_SelectedIndexChanged" OnRowDataBound="dgvEncargados_RowDataBound" OnRowCommand="dgvEncargados_RowCommand">
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No hay encargados disponibles.
										</div>
									</EmptyDataTemplate>
									<Columns>
										<asp:BoundField DataField="StrNombreCompleto" HeaderText="Nombre Completo" />
										<asp:TemplateField HeaderText="Opciones">
											<ItemTemplate>
												<asp:LinkButton ID="AsignarButton" runat="server" CommandName="Asignar" CommandArgument="<%# ((GridViewRow)Container).RowIndex %>" CssClass="btn btn-success btn-xs" ToolTip="Asignar dias seleccionados">
													<span class="glyphicon glyphicon-ok"></span>
													Asignar
												</asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		//<![CDATA[
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date.start-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				});
			}
			if (!endDateReady) {
				$(".input-group.date.end-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				});
			}
		}

		function setStartDateLimit(date) {
			$(".input-group.date.start-date").datepicker('remove');
			$(".input-group.date.start-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: date,
			}).on('changeDate', function (e) {
				setEndDateLimit(e.format());
			});
			setEndDateLimit(date);
			$(".input-group.date.start-date").datepicker('update', date);
			startDateReady = true;
		}

		function setEndDateLimit(date) {
			$(".input-group.date.end-date").datepicker('remove');
			$(".input-group.date.end-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: date,
			});
			$(".input-group.date.end-date").datepicker('update', date);
			endDateReady = true;
		}

		function pageLoad() {
			enableDatapicker();
			$(".input-group.date.n-n").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es'
			})
			$(".input-group.date.start-inactivity").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es'
			})
			$(".input-group.date.end-inactivity").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es'
			})
		}
        //]]>
	</script>
</asp:Content>
