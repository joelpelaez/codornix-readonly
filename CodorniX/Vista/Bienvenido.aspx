﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Bienvenido.aspx.cs" Inherits="CodorniX.Vista.Bienvenido" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<asp:PlaceHolder runat="server" ID="PanelBusqueda">
				<div class="panel panel-primary">
					<div class="panel-heading text-center">
						<asp:Label Text="Turnos" runat="server" ID="lblTittleLeftPanel" />
					</div>
					<div class="panel-body panel-pd">
						<asp:PlaceHolder ID="PanelBusquedas" runat="server">
							<div class="pull-right">
								<asp:LinkButton ID="btnActualizar" OnClick="btnActualizar_Click" CssClass="btn btn-sm btn-default" runat="server">
									<span class="glyphicon glyphicon-search"></span>
									Buscar
								</asp:LinkButton>
							</div>
							<div class="row">
								<div class="col-md-12">
									<asp:Panel runat="server" ID="pnlAlertBusqueda">
										<div class="alert alert-danger">
											<asp:Label runat="server" ID="lblmensaje" />
										</div>
									</asp:Panel>
								</div>
								<div class="col-md-3 pd-right-5">
									<h6>Folio</h6>
									<asp:TextBox runat="server" ID="txtFolioTurno" CssClass="form-control" />
								</div>
								<div class="col-sm-6 col-md-3 pd-left-right-5">
									<h6>Inicio</h6>
									<div class="input-group date extra">
										<asp:TextBox ID="txtFecha" CssClass="form-control" runat="server" />
										<span class="input-group-addon input-sm ">
											<i class="glyphicon glyphicon-calendar"></i>
										</span>
									</div>
								</div>
								<div class="col-sm-6 col-md-3 pd-left-right-5">
									<h6>Fin</h6>
									<div class="input-group date extra">
										<asp:TextBox ID="txtfecha2" CssClass="form-control" runat="server" />
										<span class="input-group-addon input-sm ">
											<i class="glyphicon glyphicon-calendar"></i>
										</span>
									</div>
								</div>
								<div class="col-md-3 pd-left-5">
									<h6>Estado</h6>
									<asp:DropDownList runat="server" ID="ddlEstadoTurno" CssClass="form-control">
									</asp:DropDownList>
								</div>
							</div>
						</asp:PlaceHolder>
						<asp:Panel runat="server" ID="pnlListaTareas">
							<div class="row">
								<div class="col-md-12 margin-top-10">
									<asp:GridView ID="dvgDepartamentos" runat="server" CssClass="table table-bordered table-sm table-condensed input-sm" OnRowDataBound="dvgDepartamentos_RowDataBound" OnSelectedIndexChanged="dvgDepartamentos_SelectedIndexChanged" AutoGenerateColumns="false" DataKeyNames="UidDepartamento,DtFecha">
										<EmptyDataTemplate>No hay turnos disponibles.</EmptyDataTemplate>
										<Columns>
											<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
											<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." />
											<asp:BoundField DataField="StrTurno" HeaderText="Turno" />
											<asp:BoundField DataField="DtFecha" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha" />
											<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" />
											<asp:BoundField DataField="DtFechaHoraInicio" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Fecha y hora Inicio" />
											<asp:BoundField DataField="DtFechaHoraFin" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Fecha y hora Fin" />
											<asp:BoundField DataField="StrEstadoTurno" HeaderText="Estado" />
										</Columns>
									</asp:GridView>
								</div>
							</div>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlResumenTarea">
							<p class="text-right">
								<asp:LinkButton ID="btnCerrarResumentTarea" Text="text" runat="server" CssClass="btn btn-sm btn-danger" ToolTip="Cerrar" OnClick="btnCerrarResumentTarea_Click">
									<span class="glyphicon glyphicon-remove"></span>
								</asp:LinkButton>
							</p>

							<div class="col-md-12 pd-left-right-5">
								<h6>Tarea.</h6>
								<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailNombreTarea" />
							</div>
							<div class="col-md-6 pd-left-right-5">
								<h6>Area.</h6>
								<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailArea" />
							</div>
							<div class="col-md-6 pd-left-right-5">
								<h6>Departamento.</h6>
								<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailDepartamento" />
							</div>
							<div class="col-md-6 pd-left-right-5">
								<h6>Tipo.</h6>
								<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailTipoTarea" />
							</div>
							<div class="col-md-6 pd-left-right-5">
								<h6>Periodicidad.</h6>
								<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailPeriodicidad" />
							</div>
							<div class="col-md-12 pd-left-right-5">
								<h5 class="font-bold">Valor Ingresado.</h5>
								<asp:Panel runat="server" ID="pnlReviewTaskValue">
									<div class="form-group">
										<div class="input-group">
											<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailValue" />
											<div class="input-group-addon">
												<asp:Label runat="server" ID="lblDetailUnidadMedida" />
											</div>
										</div>
									</div>
								</asp:Panel>
								<asp:Panel runat="server" ID="pnlReviewTaskTrueFalse">
									<label class="radio-inline">
										<asp:RadioButton runat="server" GroupName="rbgSiNo" Enabled="false" ID="rbDetailYes" />
										Sí
									</label>
									<label class="radio-inline">
										<asp:RadioButton runat="server" GroupName="rbgSiNo" Enabled="false" ID="rbDetailNo" />
										No
									</label>
								</asp:Panel>
								<asp:Panel runat="server" ID="pnlReviewTaskSelection">
									<asp:DropDownList runat="server" CssClass="form-control input-sm" Enabled="false" ID="ddlDetailOptions">
										<asp:ListItem Text="text1" />
										<asp:ListItem Text="text2" />
									</asp:DropDownList>
								</asp:Panel>
							</div>
							<div class="col-md-12 pd-left-right-5">
								<label>Observaciones:</label>
								<asp:TextBox TextMode="MultiLine" runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailObservaciones" />
							</div>
						</asp:Panel>
					</div>
				</div>
			</asp:PlaceHolder>
		</div>

		<asp:Label ID="lblnoiniciarturno" runat="server" />

		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Tareas</div>
				<div class="row">
					<div class="col-xs-6 text-left">
						<asp:LinkButton ID="btnReporte" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnReporte_Click" OnClientClick="document.forms[0].target ='_blank';">
                            <span class="glyphicon glyphicon-file"></span>
                            Reporte
						</asp:LinkButton>
					</div>
					<div class="col-xs-6 text-right">
						<div class="btn-group">
							<asp:LinkButton ID="btnIniciarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnIniciarTurno_Click">
                                Inicar turno
							</asp:LinkButton>
							<asp:LinkButton ID="btnCerrarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnCerrarTurno_Click">
                                Cerrar turno
							</asp:LinkButton>
						</div>
					</div>
					<div class="col-md-12">
						<asp:Panel runat="server" ID="pnlErrorGeneral">
							<div class="col-md-12">
								<div class="alert alert-danger">
									<asp:Label Text="Mensaje de error" runat="server" Visible="false" ID="lblErrorGeneral" />
								</div>
							</div>
						</asp:Panel>
						<asp:HiddenField ID="hfUidInicioTurno" runat="server" />
						<asp:HiddenField ID="hfUidPeriodo" runat="server" />
					</div>
				</div>
				<asp:TextBox CssClass="hide" ID="txtUidDepartamento" runat="server"></asp:TextBox>
				<asp:TextBox CssClass="hide" ID="txtUidArea" runat="server"></asp:TextBox>

				<div id="divCerrarturno" runat="server" class="alert alert-default">
					<asp:Label ID="lblAceptarCerrarTurno" runat="server" />

					<asp:LinkButton ID="btnAceptarCerrarTurno" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnAceptarCerrarTurno_Click">
						<asp:Label ID="Label2" CssClass="glyphicon glyphicon-ok" runat="server" />
					</asp:LinkButton>
					<asp:LinkButton ID="btnCancelarCerrarTurno" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnCancelarCerrarTurno_Click">
                        <span class="glyphicon glyphicon-remove"></span>
					</asp:LinkButton>
				</div>
				<asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
					<div class="alert alert-danger">
						<asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">x</asp:LinkButton>
						<strong>Error: </strong>
						<asp:Label ID="lblError" runat="server" />
					</div>
				</asp:PlaceHolder>
				<div class="panel-body" style="padding: 0 10px 5px 10px;">
					<ul class="nav nav-tabs">
						<li class="active" id="activeResumen" runat="server">
							<asp:LinkButton ID="tabResumen" runat="server" Text="Resumen" OnClick="tabResumen_Click" />
						</li>
						<li id="activeCompletadas" runat="server">
							<asp:LinkButton ID="tabCompletadas" runat="server" OnClick="tabCompletadas_Click" ToolTip="Completadas">
								<span class="glyphicon glyphicon-check"></span>
								<span class="">
									<asp:Label runat="server" ID="lblContadorC" Text="0"></asp:Label>
								</span>
							</asp:LinkButton>
						</li>
						<li id="activeNoCompletadas" runat="server">
							<asp:LinkButton ID="tabNoCompletadas" runat="server" OnClick="tabNoCompletadas_Click" ToolTip="No Completadas">
								<span class="glyphicon glyphicon-remove"></span>
								<span class="">
									<asp:Label runat="server" ID="lblContadorNC" Text="0"></asp:Label>
								</span>
							</asp:LinkButton>
						</li>
						<li id="activeRequeridas" runat="server">
							<asp:LinkButton ID="tabRequeridas" runat="server" OnClick="tabRequeridas_Click" ToolTip="Requeridas">
								<span class="glyphicon glyphicon glyphicon-asterisk"></span>
								<span class="">
									<asp:Label runat="server" ID="lblContadorRNC" Text="0"></asp:Label>
								</span>
							</asp:LinkButton>
						</li>
						<li id="activeCanceladas" runat="server">
							<asp:LinkButton ID="tabCanceladas" Text="text" runat="server" ToolTip="Canceladas" OnClick="tabCanceladas_Click">
								<span class="glyphicon glyphicon-ban-circle"></span>
								<span class="span">
									<asp:Label ID="lblContadorCanceladas" Text="0" runat="server" />
								</span>
							</asp:LinkButton>
						</li>
						<li id="activePospuestas" runat="server">
							<asp:LinkButton ID="tabPospuestas" Text="text" runat="server" ToolTip="Pospuestas" OnClick="tabPospuestas_Click">
								<span class="glyphicon glyphicon-share"></span>
								<span class="span">
									<asp:Label ID="lblContadorPospuestas" Text="0" runat="server" />
								</span>
							</asp:LinkButton>
						</li>
					</ul>
					<asp:PlaceHolder ID="PanelResumen" runat="server">
						<div class="col-md-12 margin-top-10">
							<h5>Departamento</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblDepartamento" />
						</div>
						<div class="col-md-4">
							<h5>Turno</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblTurno" />
						</div>
						<div class="col-md-4">
							<h5>Hora Inicio</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraInicio"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Hora Fin</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraFin"></asp:TextBox>
						</div>

						<div class="col-md-4">
							<h5>Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>No Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblNoCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Requeridas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblRequeridasNoCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Canceladas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblCanceladas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Pospuestas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblPospuestas"></asp:TextBox>
						</div>
						<div class="col-md-4">
							<h5>Estado</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblEstadoDelTurno"></asp:TextBox>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelCompletadas" runat="server">
						<h4>Tareas Completadas</h4>
						<asp:HiddenField runat="server" ID="hfSortDirectionTareasCompletadas" Value="ASC" />
						<div class="margin-top-10">
							<asp:GridView ID="DVGTareasCumplidas" DataKeyNames="UidCumplimiento" OnSelectedIndexChanging="DVGTareasCumplidas_SelectedIndexChanging" OnSelectedIndexChanged="DVGTareasCumplidas_SelectedIndexChanged" OnRowDataBound="DVGTareasCumplidas_RowDataBound" AllowSorting="true" OnSorting="DVGTareasCumplidas_Sorting" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" SelectedRowStyle-BackColor="#dff0d8">
								<EmptyDataTemplate>
									<div class="alert alert-info">
										No hay Tareas Cumplidas
									</div>
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelNoCompletadas" runat="server">
						<h4>Tareas no Completadas</h4>
						<asp:HiddenField runat="server" ID="hfSortDirectionTareasNoCompletadas" Value="ASC" />
						<div class="margin-top-10">
							<asp:GridView ID="DvgTareasNoCumplidas" OnRowDataBound="DvgTareasNoCumplidas_RowDataBound" AllowSorting="true" OnSorting="DvgTareasNoCumplidas_Sorting" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>
									<div class="alert alert-info">
										No hay Tareas No Cumplidas
									</div>
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:TemplateField HeaderText="Estado" SortExpression="Estado">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrEstadoCumplimiento") %>' ID="lblEstado" runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelRequeridas" runat="server">
						<h4>Tareas Requeridas</h4>
						<asp:HiddenField runat="server" ID="hfSortDirectionTareasRequeridas" Value="ASC" />
						<div class="margin-top-10">
							<asp:GridView ID="dgvTareasRequeridas" OnRowDataBound="dgvTareasRequeridas_RowDataBound" AllowSorting="true" OnSorting="dgvTareasRequeridas_Sorting" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>
									<div class="alert alert-info">
										No hay Tareas Requeridas
									</div>
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:TemplateField HeaderText="Estado" SortExpression="Estado">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrEstadoCumplimiento") %>' ID="lblEstado" runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="PanelCanceladas">
						<h4>Tareas Canceladas</h4>
						<asp:HiddenField runat="server" ID="hfSortDirectionTareasCanceladas" Value="ASC" />
						<div class="margin-top-10">
							<asp:GridView ID="gvTareasCanceladas" OnRowDataBound="gvTareasCanceladas_RowDataBound" AllowSorting="true" OnSorting="gvTareasCanceladas_Sorting" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
								<EmptyDataTemplate>
									<div class="alert alert-info">
										No hay tareas canceladas
									</div>
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder runat="server" ID="PanelPospuestas">
						<h4>Tareas Pospuestas</h4>
						<asp:HiddenField runat="server" ID="hfSortDirectionTareasPospuestas" Value="ASC" />
						<div class="margin-top-10">
							<asp:GridView ID="gvTareasPospuestas" OnRowDataBound="gvTareasPospuestas_RowDataBound" AllowSorting="true" OnSorting="gvTareasPospuestas_Sorting" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
								<EmptyDataTemplate>
									<div class="alert alert-info">
										No hay tareas pospuestas
									</div>
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>

	<script>
		//<![CDATA[
		function pageLoad() {
			prepareFechaAll();
		}
        //]]>
	</script>
</asp:Content>
