﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class DetalleTareaAntecesor : System.Web.UI.Page
	{
		#region Properties
		private VMDetalleAntecesorTarea VmAntecesor = new VMDetalleAntecesorTarea();
		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}
		private List<TareaAntecesora> LsTareas
		{
			get
			{
				return (List<TareaAntecesora>)ViewState["ListaTareas"];
			}
			set
			{
				ViewState["ListaTareas"] = value;
			}
		}
		private List<TareaAntecesora> LsTareasRelacionadas
		{
			get
			{
				return (List<TareaAntecesora>)ViewState["TareasRelacionadas"];
			}
			set
			{
				ViewState["TareasRelacionadas"] = value;
			}
		}
		private bool IsEditMode
		{
			get
			{
				return (bool)ViewState["EditMode"];
			}
			set
			{
				ViewState["EditMode"] = value;
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!IsPostBack)
			{
				IsEditMode = false;
				HabilitarCamposTarea(false);
				btnOk.Visible = false;
				btnCancel.Visible = false;
				pnlAlertBusqueda.Visible = false;
				pnlErrorGeneral.Visible = false;
				pnlDetallesTarea.Visible = true;
				pnlTareasRelacionadas.Visible = false;

				Guid UidSucursal = SesionActual.uidSucursalActual.Value;
				MostrarFiltros("Mostrar");

				VmAntecesor.GetDepartamentosSucursal(UidSucursal);
				lbFiltroDepartamento.DataSource = VmAntecesor.LsDepartamentos;
				lbFiltroDepartamento.DataTextField = "StrNombre";
				lbFiltroDepartamento.DataValueField = "UidDepartamento";
				lbFiltroDepartamento.DataBind();

				VmAntecesor.GetAreasSucursal(UidSucursal);
				lbFiltroArea.DataSource = VmAntecesor.LsAreas;
				lbFiltroArea.DataTextField = "StrNombre";
				lbFiltroArea.DataValueField = "UidArea";
				lbFiltroArea.DataBind();

				VmAntecesor.GetTiposTarea();
				ddlRequeridaTarea.DataSource = VmAntecesor.LsTipoTarea;
				ddlRequeridaTarea.DataTextField = "StrTipoTarea";
				ddlRequeridaTarea.DataValueField = "UidTipoTarea";
				ddlRequeridaTarea.DataBind();

				VmAntecesor.GetEstadosTarea();
				ddlEstatusTarea.DataSource = VmAntecesor.LsEstatus;
				ddlEstatusTarea.DataTextField = "strStatus";
				ddlEstatusTarea.DataValueField = "UidStatus";
				ddlEstatusTarea.DataBind();

				VmAntecesor.GetMedicion();
				ddlTipoMedicionTarea.DataSource = VmAntecesor.LsMediciones;
				ddlTipoMedicionTarea.DataTextField = "StrTipoMedicion";
				ddlTipoMedicionTarea.DataValueField = "UidTipoMedicion";
				ddlTipoMedicionTarea.DataBind();
			}
		}

		#region Left
		#region Buttons
		protected void btnMostrarFiltros_Click(object sender, EventArgs e)
		{
			MostrarFiltros(lblMostrarFiltros.Text);
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroFolioTarea.Text = string.Empty;
			txtFiltroNombre.Text = string.Empty;
			txtFiltroFechaInicio.Text = string.Empty;
			txtFiltroFechaFin.Text = string.Empty;
			lbFiltroDepartamento.ClearSelection();
			lbFiltroArea.ClearSelection();
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			string Nombre = txtFiltroNombre.Text.Trim();
			int Folio = -1;
			DateTime? DtFechaInicio = null;
			DateTime? DtFechaFin = null;
			Guid UidSucursal = SesionActual.uidSucursalActual.Value;
			string Departamentos = string.Empty;
			string Areas = string.Empty;
			string TipoTarea = ddlFiltroTipoTarea.SelectedValue.ToString();

			if (!string.IsNullOrEmpty(txtFiltroFolioTarea.Text.Trim()))
			{
				int Aux = 0;
				if (int.TryParse(txtFiltroFolioTarea.Text.Trim(), out Aux))
				{
					if (Aux >= 0)
					{
						Folio = Aux;
					}
					else
					{
						DisplayAlertBusqueda("Ingrese un folio mayor a 0.", "warning");
						return;
					}
				}
				else
				{
					DisplayAlertBusqueda("Ingrese un folio numerico.", "danger");
					return;
				}
			}
			if (!string.IsNullOrEmpty(txtFiltroFechaInicio.Text.Trim()))
			{
				try
				{
					DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayAlertBusqueda("Formato de fecha de (inicio) invalido.", "Warning");
					return;
				}
			}
			if (!string.IsNullOrEmpty(txtFiltroFechaFin.Text.Trim()))
			{
				try
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayAlertBusqueda("Formato de fecha (fin) invalido.", "Warning");
					return;
				}
			}

			Departamentos = GetSelectedValuesListBox(lbFiltroDepartamento);

			Areas = GetSelectedValuesListBox(lbFiltroArea);

			try
			{
				VmAntecesor.BusquedaTareas(TipoTarea, Nombre, DtFechaInicio, DtFechaFin, UidSucursal, Departamentos, Areas, Folio);
				this.LsTareas = VmAntecesor.LsTareas;
				gvListaTareas.SelectedIndex = -1;
				gvListaTareas.DataSource = VmAntecesor.LsTareas;
				gvListaTareas.DataBind();
				MostrarFiltros("Ocultar");
			}
			catch (Exception ex)
			{
				DisplayAlertBusqueda("Ocurrio un error al realizar la busqueda: <br/> " + ex.Message, "warning");
			}
		}
		private void MostrarFiltros(string IsOpen)
		{
			if (IsOpen.Equals("Mostrar"))
			{
				pnlFiltros.Visible = true;
				pnlListaTareas.Visible = false;
				lblMostrarFiltros.Text = "Ocultar";
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";
			}
			else
			{
				pnlFiltros.Visible = false;
				pnlListaTareas.Visible = true;
				lblMostrarFiltros.Text = "Mostrar";
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
			}
		}
		#endregion
		#region GridView
		protected void gvListaTareas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvListaTareas, "Select$" + e.Row.RowIndex);

				Label lblName = e.Row.FindControl("lblTaskName") as Label;
				if (lblName.Text.ToString().Length > 30)
				{
					lblName.ToolTip = lblName.Text;
					lblName.Text = lblName.Text.Substring(0, 27) + "...";
				}

				Label lblDepto = e.Row.FindControl("lblTaskDepto") as Label;
				if (lblDepto.Text.Length >= 13)
				{
					lblDepto.ToolTip = lblDepto.Text;
					lblDepto.Text = lblDepto.Text.Substring(0, 10) + "...";
				}

				Label lblArea = e.Row.FindControl("lblTaskArea") as Label;
				if (lblArea.Text.Length >= 13)
				{
					lblArea.ToolTip = lblArea.Text;
					lblArea.Text = lblArea.Text.Substring(0, 10) + "...";
				}

				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					//PERFIL.CssClass = "glyphicon glyphicon-user";
					e.Row.Cells[5].Text = "(Sin Asignar)";
				}
				if (e.Row.Cells[3].Text.Contains(","))
				{
					string split = e.Row.Cells[3].Text.Split(',')[0];
					e.Row.Cells[3].ToolTip = Server.HtmlDecode(e.Row.Cells[3].Text);
					e.Row.Cells[3].Text = split + ", ...";
				}
				if (e.Row.Cells[5].Text.Contains(","))
				{
					string split = e.Row.Cells[5].Text.Split(',')[0];
					e.Row.Cells[5].ToolTip = Server.HtmlDecode(e.Row.Cells[5].Text);
					e.Row.Cells[5].Text = split + ", ...";
				}
			}
		}
		protected void gvListaTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvListaTareas.SelectedIndex = -1;
			gvListaTareas.DataSource = this.LsTareas;
			gvListaTareas.PageIndex = e.NewPageIndex;
			gvListaTareas.DataBind();
		}
		protected void gvListaTareas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (btnCancel.Visible)
				e.Cancel = true;
		}
		protected void gvListaTareas_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertBusqueda.Visible)
					pnlAlertBusqueda.Visible = false;

				//Guid UidAntecesor = new Guid(gvListaTareas.SelectedDataKey.Values[0].ToString());
				Guid UidTarea = new Guid(gvListaTareas.SelectedDataKey.Values[0].ToString());
				Guid UidDepartamento = new Guid(gvListaTareas.SelectedDataKey.Values[1].ToString());
				Guid UidArea = new Guid(gvListaTareas.SelectedDataKey.Values[2].ToString());
				Guid UidPeriodicidad = new Guid(gvListaTareas.SelectedDataKey.Values[3].ToString());
				string Type = gvListaTareas.SelectedDataKey.Values[4].ToString();

				hfUidTarea.Value = UidTarea.ToString();
				hfTipoTarea.Value = Type;
				VmAntecesor.GetTarea(UidTarea);
				//VmAntecesor.GetDepartamento(UidDepartamento);
				//VmAntecesor.GetArea(UidArea);
				VmAntecesor.GetPeriodicidad(VmAntecesor.tTarea.UidPeriodicidad);
				VmAntecesor.GetFrecuenciaPeriodicidad(UidTarea);

				txtNombreTarea.Text = VmAntecesor.tTarea.StrNombre;
				txtDescripcionTarea.Text = VmAntecesor.tTarea.StrDescripcion;
				ddlEstatusTarea.SelectedValue = VmAntecesor.tTarea.UidStatus.ToString();
				ddlTipoMedicionTarea.SelectedValue = VmAntecesor.tTarea.UidMedicion.ToString();

				txtFechaInicioTarea.Text = VmAntecesor.pPeriodicidad.DtFechaInicio.ToString("dd/MM/yyyy");
				txtFechaFinTarea.Text = VmAntecesor.pPeriodicidad.DtFechaFin == null ? "" : VmAntecesor.pPeriodicidad.DtFechaFin.Value.ToString("dd/MM/yyyy");

				IsEditMode = false;
				HabilitarCamposTarea(false);

				if (Type.Equals("S"))
				{
					btnTabTareasRelacionadas.Text = "Antecesoras";
					VmAntecesor.GetTareasAntecesoras(UidTarea);
				}
				else if (Type.Equals("A"))
				{
					btnTabTareasRelacionadas.Text = "Sucesoras";
					VmAntecesor.GetTareasSucesoras(UidTarea);
				}

				this.LsTareasRelacionadas = VmAntecesor.LsTareasRelacionadas;
				dlTareasRelacionadas.DataSource = VmAntecesor.LsTareasRelacionadas;
				dlTareasRelacionadas.DataBind();
			}
			catch (Exception ex)
			{
				DisplayAlertBusqueda("Error: " + ex.Message, "danger");
			}
		}

		#endregion
		#endregion

		#region Right
		#region Buttons
		protected void TabDatosGenerales_Click(object sender, EventArgs e)
		{
			liTabDatosGenerales.Attributes.Add("class", "active");
			liTabTareasRelacionadas.Attributes.Add("class", "");

			pnlDetallesTarea.Visible = true;
			pnlTareasRelacionadas.Visible = false;
		}
		protected void TabTareasRelaciondas_Click(object sender, EventArgs e)
		{
			liTabDatosGenerales.Attributes.Add("class", "");
			liTabTareasRelacionadas.Attributes.Add("class", "active");

			pnlDetallesTarea.Visible = false;
			pnlTareasRelacionadas.Visible = true;
		}

		protected void Edit_Click(object sender, EventArgs e)
		{
			btnOk.Visible = true;
			btnCancel.Visible = true;
			this.IsEditMode = true;

			dlTareasRelacionadas.DataSource = this.LsTareasRelacionadas;
			dlTareasRelacionadas.DataBind();
		}
		protected void btnOk_Click(object sender, EventArgs e)
		{
			if (pnlErrorGeneral.Visible)
				pnlErrorGeneral.Visible = false;

			try
			{
				Guid UidTarea = new Guid(hfUidTarea.Value);

				bool BlError = false;
				TextBox tbDiasDespues;
				CheckBox cbUserNotificicion;
				CheckBox cbMaximo;
				CheckBox cbSiempre;
				TextBox tbRepeticion;
				int IntItemIndex = 0;
				int IntAuxValidate = 0;

				foreach (DataListItem item in dlTareasRelacionadas.Items)
				{
					tbDiasDespues = item.FindControl("tbDiasDespues") as TextBox;
					cbUserNotificicion = item.FindControl("cbUsarNotificacion") as CheckBox;
					cbMaximo = item.FindControl("cbMaximo") as CheckBox;
					cbSiempre = item.FindControl("cbSiembre") as CheckBox;
					tbRepeticion = item.FindControl("tbNumeroVeces") as TextBox;

					if (!int.TryParse(tbDiasDespues.Text.Trim(), out IntAuxValidate))
					{
						DisplayAlertGeneral("Ingrese un valor numerico", "warning");
						tbDiasDespues.BorderColor = System.Drawing.Color.Red;
						BlError = true;
						break;
					}
					else
					{
						if (IntAuxValidate < 0)
						{
							DisplayAlertGeneral("Ingrese un valor numerico mayor o igual a (0)", "warning");
							tbDiasDespues.BorderColor = System.Drawing.Color.Red;
							BlError = true;
							break;
						}
					}

					if (cbUserNotificicion.Checked)
					{
						if (cbMaximo.Checked && cbSiempre.Checked)
						{
							DisplayAlertGeneral("Seleccione solo una opción en <strong>Numero de repeticiones.</strong>.", "warning");
							cbMaximo.BorderColor = System.Drawing.Color.Red;
							cbSiempre.BorderColor = System.Drawing.Color.Red;
							BlError = true;
							break;
						}

						if (cbMaximo.Checked)
						{
							if (!int.TryParse(tbRepeticion.Text.Trim(), out IntAuxValidate))
							{
								DisplayAlertGeneral("Ingrese un valor numerico", "warning");
								tbRepeticion.BorderColor = System.Drawing.Color.Red;
								BlError = true;
								break;
							}
						}
					}

					IntItemIndex = item.ItemIndex;
					this.LsTareasRelacionadas[IntItemIndex].DiasDespues = int.Parse(tbDiasDespues.Text.Trim());
					this.LsTareasRelacionadas[IntItemIndex].BlUsarNotificacion = cbUserNotificicion.Checked;
					if (cbUserNotificicion.Checked)
						this.LsTareasRelacionadas[IntItemIndex].Repeticion = cbMaximo.Checked ? int.Parse(tbRepeticion.Text.Trim()) : (int?)null;
				}

				// Si surge un error se detiene el proceso
				if (BlError)
					return;

				foreach (var antecesor in this.LsTareasRelacionadas)
				{
					VmAntecesor.Actualizar(antecesor.UidAntecesor, antecesor.Repeticion, antecesor.BlUsarNotificacion, antecesor.DiasDespues.Value);
				}

				if (hfTipoTarea.Value.Equals("S"))
				{
					btnTabTareasRelacionadas.Text = "Antecesoras";
					VmAntecesor.GetTareasAntecesoras(UidTarea);
				}
				else if (hfTipoTarea.Value.Equals("A"))
				{
					btnTabTareasRelacionadas.Text = "Sucesoras";
					VmAntecesor.GetTareasSucesoras(UidTarea);
				}

				btnOk.Visible = false;
				btnCancel.Visible = false;
				this.IsEditMode = false;

				this.LsTareasRelacionadas = VmAntecesor.LsTareasRelacionadas;
				dlTareasRelacionadas.DataSource = this.LsTareasRelacionadas;
				dlTareasRelacionadas.DataBind();
			}
			catch (Exception ex)
			{
				DisplayAlertGeneral("Ocurrio un error <br/>" + ex.Message, "danger");
			}
		}
		protected void btnCancel_Click(object sender, EventArgs e)
		{
			btnOk.Visible = false;
			btnCancel.Visible = false;
			this.IsEditMode = false;

			dlTareasRelacionadas.DataSource = this.LsTareasRelacionadas;
			dlTareasRelacionadas.DataBind();
		}
		#endregion
		#region CheckBox
		protected void HabilitarNotificacionTarea_CheckedChanged(object sender, EventArgs e)
		{
			CheckBox cbHabilitarNotificacion = (CheckBox)sender;
			DataListItem Item = (DataListItem)cbHabilitarNotificacion.NamingContainer;
			int Index = Item.ItemIndex;

			if (!this.LsTareasRelacionadas[Index].HasNotificacion)
				cbHabilitarNotificacion.Enabled = false;

			Panel pnlRepeticiones = Item.FindControl("pnlRepeticiones") as Panel;
			pnlRepeticiones.Visible = cbHabilitarNotificacion.Checked;
		}
		#endregion
		#region DataList
		protected void dlTareasRelacionadas_ItemDataBound(Object sender, DataListItemEventArgs e)
		{
			Panel pnlRepeticiones = e.Item.FindControl("pnlRepeticiones") as Panel;
			TextBox tbDiasDepues = e.Item.FindControl("tbDiasDespues") as TextBox;
			TextBox tbNumeroVeces = e.Item.FindControl("tbNumeroVeces") as TextBox;
			CheckBox cbUserNotificacion = e.Item.FindControl("cbUsarNotificacion") as CheckBox;
			CheckBox cbMaximo = e.Item.FindControl("cbMaximo") as CheckBox;
			CheckBox cbSiembre = e.Item.FindControl("cbSiembre") as CheckBox;

			int Index = e.Item.ItemIndex;
			tbDiasDepues.Text = LsTareasRelacionadas[Index].DiasDespues == null ? string.Empty : LsTareasRelacionadas[Index].DiasDespues.ToString();
			tbNumeroVeces.Text = LsTareasRelacionadas[Index].Repeticion == null ? string.Empty : LsTareasRelacionadas[Index].Repeticion.ToString();
			cbUserNotificacion.Checked = LsTareasRelacionadas[Index].BlUsarNotificacion;
			cbMaximo.Checked = LsTareasRelacionadas[Index].Repeticion == null ? false : true;
			cbSiembre.Checked = LsTareasRelacionadas[Index].Repeticion == null ? true : false;

			tbDiasDepues.Enabled = this.IsEditMode;
			tbNumeroVeces.Enabled = this.IsEditMode;
			cbUserNotificacion.Enabled = this.IsEditMode;
			cbMaximo.Enabled = this.IsEditMode;
			cbSiembre.Enabled = this.IsEditMode;

			if (this.IsEditMode)
				cbUserNotificacion.Enabled = (!LsTareasRelacionadas[Index].HasNotificacion && LsTareasRelacionadas[Index].BlUsarNotificacion) ? true : LsTareasRelacionadas[Index].HasNotificacion;

			if (LsTareasRelacionadas[Index].BlUsarNotificacion)
			{
				pnlRepeticiones.Visible = true;
			}
			else
			{
				pnlRepeticiones.Visible = false;
			}
		}
		protected void dlTareasRelacionadas_ItemCommand(object source, DataListCommandEventArgs e)
		{

		}
		#endregion
		#region Fields
		private void HabilitarCamposTarea(bool IsEnabled)
		{
			txtNombreTarea.Enabled = IsEnabled;
			ddlEstatusTarea.Enabled = IsEnabled;
			txtDescripcionTarea.Enabled = IsEnabled;
			txtFechaInicioTarea.Enabled = IsEnabled;
			txtFechaFinTarea.Enabled = IsEnabled;
			ddlRequeridaTarea.Enabled = IsEnabled;
			ddlTipoMedicionTarea.Enabled = IsEnabled;
		}
		#endregion
		#endregion

		#region Complements
		private void DisplayAlertBusqueda(string Message, string Type)
		{
			pnlAlertBusqueda.CssClass = "alert alert-" + Type;
			lblAlertBusqueda.Text = Message;
			pnlAlertBusqueda.Visible = true;
		}
		protected void HideAlertBusqueda(object sender, EventArgs e)
		{
			pnlAlertBusqueda.Visible = false;
		}

		private void DisplayAlertGeneral(string Message, string Type)
		{
			lblErrorGeneral.Text = Message;
			pnlErrorGeneral.CssClass = "alert alert-" + Type;
			pnlErrorGeneral.Visible = true;
		}
		protected void HideAlertGeneral(object sender, EventArgs e)
		{
			pnlErrorGeneral.Visible = false;
		}

		private string GetSelectedValuesListBox(ListBox lbControl)
		{
			string SelectedValues = string.Empty;
			foreach (ListItem item in lbControl.Items)
			{
				if (item.Selected)
				{
					if (SelectedValues == string.Empty)
						SelectedValues = item.Value.ToString();
					else
						SelectedValues = "," + item.Value.ToString();
				}
			}
			return SelectedValues;
		}
		#endregion
	}
}