﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="HistoricoTurnosEncargados.aspx.cs" Inherits="CodorniX.Vista.HistoricoTurnosEncargados" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Historico de turnos de encargados
				</div>
				<div class="panel-body panel-pd">
					<asp:Panel runat="server" ID="pnlBotonesFiltro">
						<div class="col-md-12 pd-left-right-5 text-right">
							<div class="btn-group">
								<asp:LinkButton runat="server" ID="btnOcultarFiltros" CssClass="btn btn-sm btn-default" OnClick="btnOcultarFiltros_Click">
									<span class="glyphicon glyphicon-eye-open"></span>
									<asp:Label Text="Ocultar" runat="server" ID="lblOcultarFiltros" />
								</asp:LinkButton>
								<asp:LinkButton runat="server" ID="btnLimpiarFiltros" CssClass="btn btn-sm btn-default" OnClick="btnLimpiarFiltros_Click">
								<span class="glyphicon glyphicon-trash"></span>
								Limpiar
								</asp:LinkButton>
								<asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-sm btn-default" OnClick="btnBuscar_Click">
								<span class="glyphicon glyphicon-search"></span>
								Buscar
								</asp:LinkButton>
							</div>
						</div>
					</asp:Panel>
					<div class="col-md-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlErrorBusqueda" CssClass="alert alert-danger">
							<asp:Label runat="server" ID="lblErrorBusqueda" Text="Error" />
							<asp:LinkButton runat="server" class="close" OnClick="CloseAlertDialogBusqueda_Click">
								<span aria-hidden="true">&times;</span>
							</asp:LinkButton>
						</asp:Panel>
					</div>
					<asp:Panel runat="server" ID="pnlFiltrosBusqueda">
						<div class="col-md-12 pd-0">
							<div class="col-md-4 pd-left-right-5">
								<small>Folio de turno</small>
								<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFolioTurno" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Fecha Inicio</small>
								<div class="input-group date extra">
									<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFechaInicio" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Fecha Fin</small>
								<div class="input-group date extra">
									<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFechaFin" />
									<span class="input-group-addon">
										<span class="glyphicon glyphicon-calendar"></span>
									</span>
								</div>
							</div>
							<asp:Panel runat="server" ID="pnlFiltrosAdministrador">
								<div class="col-md-4 pd-left-right-5">
									<small>Sucursal</small>
									<asp:DropDownList runat="server" CssClass="form-control" ID="ddlSucursal">
										<asp:ListItem Text="text1" />
										<asp:ListItem Text="text2" />
									</asp:DropDownList>
								</div>
								<div class="col-md-4 pd-left-right-5">
									<small>Encargado</small>
									<asp:DropDownList runat="server" CssClass="form-control" ID="ddlEncargado">
										<asp:ListItem Text="text1" />
										<asp:ListItem Text="text2" />
									</asp:DropDownList>
								</div>
							</asp:Panel>
							<%--<div class="col-md-4 pd-left-right-5">
							<small>Estado de turno</small>
							<asp:DropDownList runat="server" CssClass="form-control">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Departamento</small>
							<asp:DropDownList runat="server" CssClass="form-control">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Encargado</small>
							<asp:ListBox runat="server" CssClass="form-control">
								<asp:ListItem Text="Item1"></asp:ListItem>
							</asp:ListBox>
						</div>--%>
						</div>
					</asp:Panel>

					<asp:Panel runat="server" ID="pnlListaTurnos">
						<div class="col-md-12 pd-left-right-5">
							<asp:HiddenField runat="server" ID="hfSortDirectionGvTurnos" Value="ASC" />
							<asp:GridView runat="server" ID="gvTurnosEncargados" DataKeyNames="UidInicioTurno" AllowSorting="true" AllowPaging="true" OnRowDataBound="gvTurnosEncargados_RowDataBound" OnSelectedIndexChanged="gvTurnosEncargados_SelectedIndexChanged" OnSorting="gvTurnosEncargados_Sorting" OnPageIndexChanging="gvTurnosEncargados_PageIndexChanging" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-hover input-sm" SelectedRowStyle-BackColor="#dff0d8">
								<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
								<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								<EmptyDataTemplate>
									No hay registros de turnos
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField HeaderText="Folio" DataField="Folio" DataFormatString="{0:0000}" SortExpression="Folio" />
									<asp:BoundField HeaderText="Usuario" DataField="Usuario" SortExpression="Usuario" />
									<asp:BoundField HeaderText="Turno" DataField="Turno" SortExpression="Turno" />
									<asp:BoundField HeaderText="F. Inicio" DataField="DtoFechaInicio" DataFormatString="{0:dd/MM/yyyy HH:mm}" SortExpression="FInicio" />
									<asp:BoundField HeaderText="F. Fin" DataField="DtoFechaFin" DataFormatString="{0:dd/MM/yyyy HH:mm}" SortExpression="FFin" />
									<asp:BoundField HeaderText="Departamento" DataField="Departamento" SortExpression="Departamento" />
									<asp:BoundField HeaderText="Estado" DataField="EstadoTurno" SortExpression="Estado" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlDetallesTarea">
						<div class="text-right">
							<asp:LinkButton runat="server" ID="btnCerrarDetalleTarea" CssClass="btn btn-sm btn-danger" OnClick="btnCerrarDetalleTarea_Click">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</div>
						<div class="col-md-12 pd-left-right-5">
							Datos de <strong>cumplimiento.</strong>
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Fecha
								<asp:Label CssClass="glyphicon glyphicon-time text-danger" Text="(Atrasada)" runat="server" ToolTip="La Tarea fue realizada despues del tiempo programado" ID="lblTareaAtrasada" />
							</small>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtFechaCumplimiento" />
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Hora</small>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtHoraCumplimiento" />
						</div>
						<div class="col-md-12 pd-left-right-5 pd-top-5">
							<small>Valor Ingresado</small>
							<asp:Panel runat="server" ID="pnlCumplimientoSeleccion">
								<asp:DropDownList runat="server" CssClass="form-control" ID="ddlOpcionSeleccionada" Enabled="false">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlCumplimientoVerdaderoFalso">
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbYes" Enabled="false" />
									Sí
								</label>
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbNo" Enabled="false" />
									No
								</label>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlCumplimientoValor">
								<div class="input-group">
									<span class="input-group-addon">
										<asp:Label Text="$" runat="server" ID="lblUnidadMedida" />
									</span>
									<asp:TextBox runat="server" CssClass="form-control" ID="txtValorIngresado" Enabled="false" />
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-12 pd-left-right-5 pd-top-5">
							<small>Observaciones</small>
							<asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" CssClass="form-control" Enabled="false" />
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlDetalleRevisión">
						<div class="col-md-12 pd-left-right-5 pd-top-10">
							Datos de <strong>revisión.</strong>
						</div>
						<div class="col-md-8 pd-left-right-5 pd-top-5">
							<small>Supervisado por</small>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtNombreSupervisor" />
						</div>
						<div class="col-md-4 pd-left-right-5 pd-top-5">
							<small>Fecha</small>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtFechaSupervision" />
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Valor de supervisión</small>
							<asp:Panel runat="server" ID="pnlOpcionMultipleSupervision">
								<asp:DropDownList runat="server" CssClass="form-control" ID="ddlOpcionSeleccionadaSupervision" Enabled="false">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlVFSupervision">
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbSiSupervision" Enabled="false" />
									Sí
								</label>
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbNoSupervision" Enabled="false" />
									No
								</label>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlValorSupervision">
								<div class="input-group">
									<span class="input-group-addon">
										<asp:Label Text="$" runat="server" ID="Label1" />
									</span>
									<asp:TextBox runat="server" CssClass="form-control" ID="txtValorSupervision" Enabled="false" />
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Calificacion</small>
							<asp:DropDownList runat="server" ID="ddlCalificaciones" CssClass="form-control" Enabled="false">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Detalles del turno
				</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-left-right-5">
						<div class="btn-group">
							<asp:LinkButton ID="btnPrintReport" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnPrintReport_Click">
								<span class="glyphicon glyphicon-print"></span>
								Reporte
							</asp:LinkButton>
						</div>
					</div>
					<div class="col-md-12 pd-left-right-5">
						<asp:Panel runat="server" CssClass="alert alert-info" ID="pnlAlertGeneral">
							<asp:Label Text="Error: " runat="server" ID="lblAlertGeneral" />
							<asp:LinkButton runat="server" class="close" OnClick="CloseAlertDialogGeneral_Click">
								<span aria-hidden="true">&times;</span>
							</asp:LinkButton>
						</asp:Panel>
					</div>
					<div class="col-md-12 pd-left-right-5">
						<ul class="nav nav-tabs">
							<li role="presentation" class="active" id="liTabResumen" runat="server">
								<asp:LinkButton Text="text" runat="server" OnClick="NavigateResumen_Click">
									Resumen
								</asp:LinkButton>
							</li>
							<li role="presentation" id="liTabCompletadas" runat="server">
								<asp:LinkButton Text="text" runat="server" ToolTip="Completadas" OnClick="NavigateCompletadas_Click">
									<span class="glyphicon glyphicon-check"></span>
									<asp:Label runat="server" Text="0" ID="lblCountCompletadas" />
								</asp:LinkButton>
							</li>
							<li role="presentation" id="liTabNoCompletadas" runat="server">
								<asp:LinkButton Text="text" runat="server" ToolTip="No Completadas" OnClick="NavigateNoCompletadas_Click">
									<span class="glyphicon glyphicon-remove"></span>
									<asp:Label runat="server" Text="0" ID="lblCountNoCompletadas" />
								</asp:LinkButton>
							</li>
							<li role="presentation" id="liTabRequeridas" runat="server">
								<asp:LinkButton Text="text" runat="server" ToolTip="Requeridas" OnClick="NavigateRequeridas_Click">
									<span class="glyphicon glyphicon-asterisk"></span>
									<asp:Label runat="server" Text="0" ID="lblCountRequeridas" />
								</asp:LinkButton>
							</li>
							<li role="presentation" id="liTabCanceladas" runat="server">
								<asp:LinkButton Text="text" runat="server" ToolTip="Canceladas" OnClick="NavigateCanceladas_Click">
									<span class="glyphicon glyphicon-ban-circle"></span>
									<asp:Label runat="server" Text="0" ID="lblCountCanceladas" />
								</asp:LinkButton>
							</li>
							<li role="presentation" id="liTabPospuestas" runat="server">
								<asp:LinkButton Text="text" runat="server" ToolTip="Pospuestas" OnClick="NavigatePospuestas_Click">
									<span class="glyphicon glyphicon-share"></span>
									<asp:Label runat="server" Text="0" ID="lblCountPospuestas" />
								</asp:LinkButton>
							</li>
						</ul>
					</div>
					<div class="col-md-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlResumen">
							<div class="col-md-12 pd-left-right-5">
								<h6>Departamento</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtUsuario" />
							</div>
							<div class="col-md-12 pd-left-right-5">
								<h6>Departamento</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtDepartamento" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Turno</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtTurno" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Hora Inicio</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtFechaInicio" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Hora Fin</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtFechaFin" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Cumplidas</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtNoTareasCumplidas" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>No Cumplidas</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtNoTareasNoCumplidas" />
							</div>
							<%--<div class="col-md-4 pd-left-right-5">
								<h6>Requeridas no cumplidas</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtNoTareasRequeridaNC"/>
							</div>--%>
							<div class="col-md-4 pd-left-right-5">
								<h6>Estado</h6>
								<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtEstado" />
							</div>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlCompletadas">
							<h5>Tareas <strong>Completadas</strong></h5>
							<asp:HiddenField runat="server" ID="hfSortDirectionTC" Value="DESC" />
							<asp:GridView runat="server" ID="gvTareasCompletadas" DataKeyNames="UidCumplimiento" AllowSorting="true" OnRowDataBound="gvTareasCompletadas_RowDataBound" OnSelectedIndexChanged="gvTareasCompletadas_SelectedIndexChanged" OnSorting="gvTareasCompletadas_Sorting" CssClass="table table-bordered table-hover table-condensed input-sm" AutoGenerateColumns="false" SelectedRowStyle-BackColor="#dff0d8">
								<EmptyDataTemplate>
									No hay registros de tareas completadas
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
							</asp:GridView>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlNoCompletadas">
							<h5>Tareas <strong>no Completadas</strong></h5>
							<asp:HiddenField runat="server" ID="hfSortDirectionTNC" Value="DESC" />
							<asp:GridView runat="server" ID="gvTareasNoCompletadas" AllowSorting="true" OnRowDataBound="gvTareasNoCompletadas_RowDataBound" OnSelectedIndexChanging="gvTareasNoCompletadas_SelectedIndexChanging" OnSorting="gvTareasNoCompletadas_Sorting" CssClass="table table-bordered table-hover table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>
									No hay registros de tareas no completadas
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:TemplateField HeaderText="Estado" SortExpression="Estado">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrEstadoCumplimiento") %>' ID="lblEstado" runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
							</asp:GridView>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlRequeridas">
							<h5>Tareas <strong>Requeridas</strong></h5>
							<asp:HiddenField runat="server" ID="hfSortDirectionTR" Value="DESC" />
							<asp:GridView runat="server" ID="gvTareasRequeridas" AllowSorting="true" OnRowDataBound="gvTareasRequeridas_RowDataBound" OnSelectedIndexChanged="gvTareasRequeridas_SelectedIndexChanged" OnSorting="gvTareasRequeridas_Sorting" CssClass="table table-bordered table-hover table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>
									No hay registros de tareas requeridas
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:TemplateField HeaderText="Estado" SortExpression="Estado">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrEstadoCumplimiento") %>' ID="lblEstado" runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlCanceladas">
							<h5>Tareas <strong>Canceladas</strong></h5>
							<asp:HiddenField runat="server" ID="hfSortDirectionTCan" Value="DESC" />
							<asp:GridView runat="server" ID="gvTareasCanceladas" AllowSorting="true" OnRowDataBound="gvTareasCanceladas_RowDataBound" OnSelectedIndexChanged="gvTareasCanceladas_SelectedIndexChanged" OnSorting="gvTareasCanceladas_Sorting" CssClass="table table-bordered table-hover table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>
									No hay registros de tareas canceladas
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
							</asp:GridView>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlPospuestas">
							<h5>Tareas <strong>Pospuestas</strong></h5>
							<asp:HiddenField runat="server" ID="hfSortDirectionTP" Value="DESC" />
							<asp:GridView runat="server" ID="gvTareasPospuestas" AllowSorting="true" OnRowDataBound="gvTareasPospuestas_RowDataBound" OnSelectedIndexChanged="gvTareasPospuestas_SelectedIndexChanged" OnSorting="gvTareasPospuestas_Sorting" CssClass="table table-bordered table-hover table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>
									No hay registros de tareas pospuestas
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
							</asp:GridView>
						</asp:Panel>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>

	<script>
		//<![CDATA[
		function pageLoad() {
			prepareFechaAll();
		}
        //]]>
	</script>
</asp:Content>
