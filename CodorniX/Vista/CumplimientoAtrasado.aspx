﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="CumplimientoAtrasado.aspx.cs" Inherits="CodorniX.Vista.CumplimientoAtrasado" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Cumplimiento Atrasado</title>
	<style>
		.text-overflow {
			display: block;
			overflow: hidden;
			white-space: nowrap;
			text-overflow: ellipsis;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					<asp:Label Text="Turnos" runat="server" ID="lblTituloIzquierda" />
				</div>
				<div class="panel-body panel-pd">
					<asp:Panel runat="server" ID="pnlBotonesFiltros" CssClass="col-md-12 pd-0 text-right">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="btnOcultarFiltros" CssClass="btn btn-sm btn-default" OnClick="btnOcultarFiltros_Click">
								<asp:Label Text="Ocultar" runat="server" ID="lblOcultarFiltros" />
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnLimpiarFiltros" CssClass="btn btn-sm btn-default" OnClick="btnLimpiarFiltros_Click">
								<span class="glyphicon glyphicon-trash"></span>
								Limpiar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnBusqueda" CssClass="btn btn-sm btn-default" OnClick="btnBusqueda_Click">
								<span class="glyphicon glyphicon-search"></span>
								Buscar
							</asp:LinkButton>
						</div>
					</asp:Panel>
					<div class="col-md-12 pd-0">
						<asp:Panel runat="server" ID="pnlMessageLeft">
							<asp:Label Text="Error: " runat="server" ID="lblMessageLeft" />
						</asp:Panel>
					</div>
					<asp:Panel runat="server" ID="pnlFiltros" CssClass="col-md-12 pd-0">
						<div class="col-md-4 pd-left-right-5">
							<small>Folio</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFolio" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Fecha Inicio</small>
							<div class="input-group date start-date">
								<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFechaInicio" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Fecha Fin</small>
							<div class="input-group date start-date">
								<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFechaFin" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Departamentos</small>
							<asp:ListBox runat="server" CssClass="form-control" Rows="3" ID="lbFiltroDepartamento">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:ListBox>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Usuarios</small>
							<asp:DropDownList runat="server" ID="ddlFiltroEncargado" CssClass="form-control input-sm">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlTurnos" CssClass="col-md-12 pd-0">
						<asp:HiddenField runat="server" ID="hfSoftDirectionTurnos" />
						<asp:HiddenField runat="server" ID="hfUidInicioTurno" />
						<asp:GridView
							runat="server"
							ID="gvTurnos"
							DataKeyNames="UidInicioTurno"
							OnRowDataBound="gvTurnos_RowDataBound"
							OnSelectedIndexChanging="gvTurnos_SelectedIndexChanging"
							OnSelectedIndexChanged="gvTurnos_SelectedIndexChanged"
							AllowSorting="true"
							OnSorting="gvTurnos_Sorting"
							AllowPaging="true"
							OnPageIndexChanging="gvTurnos_PageIndexChanging"
							AutoGenerateColumns="false"
							CssClass="table table-bordered table-condensed table-hover input-sm"
							SelectedRowStyle-BackColor="#dff0d8">
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
								<asp:BoundField HeaderText="Folio" DataField="Folio" SortExpression="Folio" />
								<asp:TemplateField HeaderText="Encargado" ControlStyle-Width="50" SortExpression="Encargado">
									<ItemTemplate>
										<asp:Label Text='<%#Eval("Usuario")%>' ToolTip='<%#Eval("Usuario")%>' runat="server" CssClass="text-overflow" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="Departamento" ControlStyle-Width="50" SortExpression="Departamento">
									<ItemTemplate>
										<asp:Label Text='<%#Eval("Departamento")%>' ToolTip='<%#Eval("Departamento")%>' runat="server" CssClass="text-overflow" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField HeaderText="Apertura" DataField="DtoFechaInicio" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="FInicio" />
								<asp:BoundField HeaderText="Cierre" DataField="DtoFechaFin" DataFormatString="{0:dd/MM/yyyy HH:mm:ss}" SortExpression="FFin" />
								<asp:BoundField HeaderText="Turno" DataField="Turno" SortExpression="Turno" />
								<asp:BoundField HeaderText="Estado" DataField="EstadoTurno" SortExpression="Estado" />
							</Columns>
							<EmptyDataTemplate>
								Sin resultados
							</EmptyDataTemplate>
						</asp:GridView>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlCumplimiento" CssClass="col-md-12 pd-0">
						<%--Botones--%>
						<div class="col-lg-12 pd-0">
							<table style="width: 100%;">
								<tr>
									<td>
										<div class="btn-group">
											<asp:LinkButton runat="server" ID="btnCancelar" CssClass="btn btn-sm btn-default" OnClick="btnCancelar_Click">
												<span class="glyphicon glyphicon-remove-circle"></span>
												Cancelar
											</asp:LinkButton>
											<asp:LinkButton runat="server" ID="btnRealizar" CssClass="btn btn-sm btn-default" OnClick="btnRealizar_Click">
												<span class="glyphicon glyphicon-ok-circle"></span>
												Realizar
											</asp:LinkButton>
											<asp:LinkButton runat="server" ID="btnOkRealizar" CssClass="btn btn-sm btn-success" OnClick="btnOkRealizar_Click">
												<span class="glyphicon glyphicon-ok"></span>
											</asp:LinkButton>
											<asp:LinkButton runat="server" ID="btnCancelRealizar" CssClass="btn btn-sm btn-danger" OnClick="btnCancelRealizar_Click">
												<span class="glyphicon glyphicon-remove"></span>
											</asp:LinkButton>

											<asp:LinkButton runat="server" ID="btnOkIncumplimiento" CssClass="btn btn-sm btn-success" OnClick="btnOkIncumplimiento_Click">
												<span class="glyphicon glyphicon-ok"></span>
											</asp:LinkButton>
											<asp:LinkButton runat="server" ID="btnCancelarIncumplimiento" CssClass="btn btn-sm btn-danger" OnClick="btnCancelarIncumplimiento_Click">
												<span class="glyphicon glyphicon-remove"></span>
											</asp:LinkButton>
										</div>
									</td>
									<td class="text-right">
										<div class="btn-group">
											<asp:LinkButton runat="server" ID="btnCerrarCumplimiento" OnClick="btnCerrarCumplimiento_Click" CssClass="btn btn-sm btn-default">
												<span class="glyphicon glyphicon-remove"></span>
												Cerrar
											</asp:LinkButton>
										</div>
									</td>
								</tr>
							</table>
						</div>
						<div class="col-lg-12 pd-left-right-5">
							<small>Tarea</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtTarea" />
						</div>
						<div class="col-lg-12 pd-left-right-5 mg-top-5">
							<small>Departamento</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDepartamento" />
						</div>
						<div class="col-lg-12 pd-left-right-5 mg-top-5">
							<small>Area</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtArea" />
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6 pd-left-right-5 mg-top-5">
							<small>Tipo</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtTipo" />
						</div>
						<div class="col-sm-12 col-md-6 col-lg-6 pd-left-right-5  mg-top-5">
							<small>Periodicidad</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtFrecuenciaPeriodicidad" />
						</div>
						<div class="col-lg-12 pd-left-right-5 mg-top-5">
							Realizado por: 						
						<asp:RadioButton runat="server" CssClass="radio-inline" Text="Supervisor" GroupName="DoneBy" ID="rbDoneBySupervisor" />
							<asp:RadioButton runat="server" CssClass="radio-inline" Text="Encargado" GroupName="DoneBy" ID="rbDoneByEncargado" />
						</div>
						<div class="col-lg-12 pd-left-right-5 mg-top-5">
							<asp:Panel runat="server" ID="pnlYesNo">
								<small>Seleccione una opción</small>
								<br />
								<asp:RadioButton Text="SI" runat="server" CssClass="radio-inline" GroupName="CumplimientoSiNo" ID="rbCumplimientoSi" />
								<asp:RadioButton Text="NO" runat="server" CssClass="radio-inline" GroupName="CumplimientoSiNo" ID="rbCumplimientoNo" />
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlValue">
								<small>Valor</small>
								<div class="input-group">
									<asp:Label Text="$" runat="server" CssClass="input-group-addon" ID="lblUnidadMedida" />
									<asp:TextBox runat="server" CssClass="form-control" ID="txtCumplimientoValor" />
								</div>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlSelection">
								<small>Seleccione una opción</small>
								<asp:DropDownList runat="server" CssClass="form-control" ID="ddlCumplimientoSeleccion">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</asp:Panel>
						</div>
						<div class="col-lg-12 pd-left-right-5">
							<small>Observaciones</small>
							<asp:TextBox runat="server" CssClass="form-control" TextMode="MultiLine" Rows="3" ID="txtObservaciones" />
						</div>
						<div class="col-lg-12 pd-left-right-5">
							<%--<asp:Panel runat="server" ID="pnlPosponer">
								<small>Ingrese fecha</small>
								<span class="input-group date start-date">
									<span class="input-group-addon input-sm">Fecha:
									</span>
									<asp:TextBox CssClass="form-control" runat="server" ID="txtNuevaFecha" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</span>
							</asp:Panel>--%>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tareas no completadas
				</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-0">
						<asp:Panel runat="server" ID="pnlMessageRight">
							<asp:Label Text="Error: " runat="server" ID="lblMessageRight" />
						</asp:Panel>
					</div>
					<div class="col-md-12 pd-0">
						<table style="width: 100%;">
							<tr>
								<td>
									<div class="btn-group">
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnPrintReporteTurno" OnClick="btnPrintReporteTurno_Click">
											<span class="glyphicon glyphicon-print"></span>
											Reporte
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnPrintNuevasTareas" OnClick="btnPrintNuevasTareas_Click">
											<asp:Label Text="<span class='glyphicon glyphicon-print'></span> Tareas nuevas" runat="server" ID="lblTareasNuevas"/>
										</asp:LinkButton>
									</div>
									<asp:HiddenField runat="server" ID="hfUidPeriodoTurno" />
									<asp:Label runat="server" ID="lblHoraInicioTurno" Visible="false" />
									<asp:Label runat="server" ID="lblHoraFinTurno" Visible="false" />
								</td>
								<td class="text-right">
									<div class="btn-group">
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnFiltroCOcultar" OnClick="btnFiltroCOcultar_Click">
											<asp:Label Text="Ocultar" runat="server" ID="lblFiltroCOcultar" />
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnFiltroCLimpiar" OnClick="btnFiltroCLimpiar_Click">
											<span class="glyphicon glyphicon-trash"></span>
											Limpiar
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnFiltroCBuscar" OnClick="btnFiltroCBuscar_Click">
											<span class="glyphicon glyphicon-search"></span>
											Buscar
										</asp:LinkButton>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<asp:Panel runat="server" CssClass="col-md-12 pd-0" ID="pnlFiltrosCumplimiento">
						<div class="col-md-4 pd-left-right-5">
							<small>Folio</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroCFolio" />
						</div>
						<div class="col-md-8 pd-left-right-5">
							<small>Tarea</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroCTarea" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Departamento</small>
							<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlFiltroCDepartamento" AutoPostBack="true" OnSelectedIndexChanged="ddlFiltroCDepartamento_SelectedIndexChanged">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Area</small>
							<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlFiltroCArea">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Tipo de tarea</small>
							<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlFiltroCTipo">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" class="col-md-12 pd-0" ID="pnlListaCumplimientos">
						<p>Tareas <strong>pedientes</strong></p>
						<asp:HiddenField runat="server" ID="hfUidCumplimiento" />
						<asp:HiddenField runat="server" ID="hfUidTarea" />
						<asp:HiddenField runat="server" ID="hfSortCumplimiento" Value="ASC" />
						<asp:GridView runat="server"
							ID="gvCumplimientos"
							DataKeyNames="UidCumplimiento"
							OnRowDataBound="gvCumplimientos_RowDataBound"
							OnSelectedIndexChanging="gvCumplimientos_SelectedIndexChanging"
							OnSelectedIndexChanged="gvCumplimientos_SelectedIndexChanged"
							AllowSorting="true"
							OnSorting="gvCumplimientos_Sorting"
							AllowPaging="true" PageSize="10"
							OnPageIndexChanging="gvCumplimientos_PageIndexChanging"
							AutoGenerateColumns="false"
							CssClass="table table-bordered table-condensed table-hover input-sm"
							SelectedRowStyle-BackColor="#dff0d8">
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
								<asp:BoundField HeaderText="F.C." DataField="IntFolio" SortExpression="FCumplimiento" />
								<asp:BoundField HeaderText="F.T." DataField="IntFolioTarea" SortExpression="FTarea" />
								<asp:BoundField HeaderText="StrTarea" DataField="StrTarea" SortExpression="Tarea" />
								<asp:TemplateField HeaderText="Departamento" ControlStyle-Width="50" SortExpression="Departamento">
									<ItemTemplate>
										<asp:Label Text='<%#Eval("StrDepartamento")%>' ToolTip='<%#Eval("StrDepartamento")%>' runat="server" CssClass="text-overflow" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="Area" ControlStyle-Width="50" SortExpression="Area">
									<ItemTemplate>
										<asp:Label Text='<%#Eval("StrArea")%>' ToolTip='<%#Eval("StrArea")%>' runat="server" CssClass="text-overflow" />
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
							<EmptyDataTemplate>
								Las tareas fueron completadas
							</EmptyDataTemplate>
							<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="5" />
							<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
						</asp:GridView>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<script>
		//<![CDATA[
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date.start-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
		}

		function setStartDateLimit(start, end) {
			$(".input-group.date.start-date").datepicker('remove');
			$(".input-group.date.start-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: start,
				endDate: end
			})
			startDateReady = true;
		}


		function pageLoad() {
			enableDatapicker();
		}
        //]]>
	</script>
</asp:Content>
