﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using CodorniX.Util;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class CumplimientoAtrasado : System.Web.UI.Page
	{
		#region Properties
		private Guid sUidEncargado
		{
			get
			{
				return (Guid)ViewState["sUidEncargado"];
			}
			set
			{
				ViewState["sUidEncargado"] = value;
			}
		}
		private string sUidDepartamentos
		{
			get
			{
				return (string)ViewState["sUidDepartamentos"];
			}
			set
			{
				ViewState["sUidDepartamentos"] = value;
			}
		}
		private int? sFolioTurno
		{
			get
			{
				return (int?)ViewState["sFolioTurno"];
			}
			set
			{
				ViewState["sFolioTurno"] = value;
			}
		}
		private DateTime? sDtFechaInicio
		{
			get
			{
				return (DateTime?)ViewState["sDtFechaInicio"];
			}
			set
			{
				ViewState["sDtFechaInicio"] = value;
			}
		}
		private DateTime? sDtFechaFin
		{
			get
			{
				return (DateTime?)ViewState["sDtFechaFin"];
			}
			set
			{
				ViewState["sDtFechaFin"] = value;
			}
		}

		private Guid UidPeriodoTurno
		{
			get
			{
				return (Guid)ViewState["UidPeriodoTurno"];
			}
			set
			{
				ViewState["UidPeriodoTurno"] = value;
			}
		}
		private Guid UidDepartamentoTurno
		{
			get
			{
				return (Guid)ViewState["UidDeptoTurno"];
			}
			set
			{
				ViewState["UidDeptoTurno"] = value;
			}
		}
		private DateTime DtFechaInicioTurno
		{
			get
			{
				return (DateTime)ViewState["dtFechaTurno"];
			}
			set
			{
				ViewState["dtFechaTurno"] = value;
			}
		}
		private int IntFolioTurno
		{
			get
			{
				return (int)ViewState["IntFolioTurno"];
			}
			set
			{
				ViewState["IntFolioTurno"] = value;
			}
		}

		private int? FCIntFolio
		{
			get
			{
				return (int?)ViewState["FCFolio"];
			}
			set
			{
				ViewState["FCFolio"] = value;
			}
		}
		private string FCStrTarea
		{
			get
			{
				return (string)ViewState["FCTarea"];
			}
			set
			{
				ViewState["FCTarea"] = value;
			}
		}
		private Guid FCUidDepartamento
		{
			get
			{
				return (Guid)ViewState["FCUidDepartamento"];
			}
			set
			{
				ViewState["FCUidDepartamento"] = value;
			}
		}
		private Guid FCUidArea
		{
			get
			{
				return (Guid)ViewState["FCUidArea"];
			}
			set
			{
				ViewState["FCUidArea"] = value;
			}
		}
		private Guid FCUidTipoTarea
		{
			get
			{
				return (Guid)ViewState["FCUidTipo"];
			}
			set
			{
				ViewState["FCUidTipo"] = value;
			}
		}

		private Modelo.Sesion SesionActual => (Modelo.Sesion)Session["Sesion"];

		private VMCumplimientoAtrasado VmCumplimiento = new VMCumplimientoAtrasado();

		private List<Modelo.TurnoIniciado> LsTurnos
		{
			get
			{
				return (List<Modelo.TurnoIniciado>)ViewState["lsTurnos"];
			}
			set
			{
				ViewState["lsTurnos"] = value;
			}
		}
		private List<Modelo.Cumplimiento> LsCumplimientos
		{
			get
			{
				return (List<Modelo.Cumplimiento>)ViewState["lCumplimientos"];
			}
			set
			{
				ViewState["lCumplimientos"] = value;
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{
				Response.Redirect("Login.aspx");
			}

			if (!SesionActual.uidSucursalActual.HasValue)
			{
				Response.Redirect(Modelo.Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				this.LsTurnos = new List<Modelo.TurnoIniciado>();
				this.sUidEncargado = Guid.Empty;
				this.sUidDepartamentos = string.Empty;
				this.sFolioTurno = null;
				this.sDtFechaInicio = null;
				this.sDtFechaFin = null;

				this.FCIntFolio = null;
				this.FCStrTarea = string.Empty;
				this.FCUidDepartamento = Guid.Empty;
				this.FCUidArea = Guid.Empty;
				this.FCUidTipoTarea = Guid.Empty;

				pnlMessageLeft.Visible = false;
				pnlMessageRight.Visible = false;
				pnlCumplimiento.Visible = false;
				HideShowFilters("Mostrar");

				if (SesionActual.Rol.Equals("Supervisor"))
				{
					VmCumplimiento.GetDepartamentosSupervisor(SesionActual.UidDepartamentos);
					VmCumplimiento.GetEncargadosSupervisor();
				}
				else if (SesionActual.Rol.Equals("Administrador"))
				{
					VmCumplimiento.GetDepartamentosAdministrador(SesionActual.uidSucursalActual.Value);
					VmCumplimiento.GetEncargadosAdministrador(SesionActual.uidSucursalActual.Value);
				}

				btnFiltroCLimpiar.CssClass = "btn btn-sm btn-default disabled";
				btnFiltroCBuscar.CssClass = "btn btn-sm btn-default disabled";
				btnPrintReporteTurno.CssClass = "btn btn-sm btn-default disabled";

				EnableSearchFields(false);

				lbFiltroDepartamento.DataSource = VmCumplimiento.LsDepartamentos;
				lbFiltroDepartamento.DataValueField = "UidDepartamento";
				lbFiltroDepartamento.DataTextField = "StrNombre";
				lbFiltroDepartamento.DataBind();

				ddlFiltroCDepartamento.DataSource = VmCumplimiento.LsDepartamentos;
				ddlFiltroCDepartamento.DataValueField = "UidDepartamento";
				ddlFiltroCDepartamento.DataTextField = "StrNombre";
				ddlFiltroCDepartamento.DataBind();

				VmCumplimiento.GetAreasDepartamento(Guid.Empty);
				ddlFiltroCArea.DataSource = VmCumplimiento.LsAreas;
				ddlFiltroCArea.DataValueField = "UidArea";
				ddlFiltroCArea.DataTextField = "StrNombre";
				ddlFiltroCArea.DataBind();

				VmCumplimiento.GetTiposTarea();
				ddlFiltroCTipo.DataSource = VmCumplimiento.LsTiposTarea;
				ddlFiltroCTipo.DataValueField = "UidTipoTarea";
				ddlFiltroCTipo.DataTextField = "StrTipoTarea";
				ddlFiltroCTipo.DataBind();

				ddlFiltroEncargado.DataSource = VmCumplimiento.LsEncargados;
				ddlFiltroEncargado.DataValueField = "UIDUSUARIO";
				ddlFiltroEncargado.DataTextField = "STRNOMBRE";
				ddlFiltroEncargado.DataBind();
			}
		}

		#region Left
		#region Buttons
		protected void btnOcultarFiltros_Click(object sender, EventArgs e)
		{
			HideShowFilters(lblOcultarFiltros.Text);
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			txtFiltroFolio.Text = string.Empty;
			txtFiltroFechaInicio.Text = string.Empty;
			txtFiltroFechaFin.Text = string.Empty;
			lbFiltroDepartamento.ClearSelection();
			ddlFiltroEncargado.SelectedIndex = 0;
		}
		protected void btnBusqueda_Click(object sender, EventArgs e)
		{
			this.sUidEncargado = Guid.Empty;
			this.sUidDepartamentos = string.Empty;
			this.sFolioTurno = null;
			this.sDtFechaInicio = null;
			this.sDtFechaFin = null;

			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			this.sUidEncargado = new Guid(ddlFiltroEncargado.SelectedValue.ToString());

			this.sUidDepartamentos = GetValuesFromListBox(lbFiltroDepartamento);

			if (!string.IsNullOrWhiteSpace(txtFiltroFolio.Text))
			{
				if (IsNumericTextValue(txtFiltroFolio.Text.Trim()))
				{
					this.sFolioTurno = int.Parse(txtFiltroFolio.Text.Trim());
				}
				else
				{
					DisplayMessageLeft("Ingrese un <strong>folio</strong> valido.", "danger");
					return;
				}
			}

			if (!string.IsNullOrWhiteSpace(txtFiltroFechaInicio.Text))
			{
				try
				{
					this.sDtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayMessageLeft("<strong>Fecha de inicio</strong> invalida.", "danger");
					return;
				}
			}

			if (!string.IsNullOrWhiteSpace(txtFiltroFechaFin.Text))
			{
				try
				{
					this.sDtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayMessageLeft("<strong>Fecha fin</strong> invalida.", "danger");
					return;
				}
			}

			if (SesionActual.Rol.Equals("Supervisor"))
			{
				if (this.sUidDepartamentos == string.Empty)
				{
					this.sUidDepartamentos = string.Join(",", SesionActual.UidDepartamentos.ToArray());
				}
			}

			try
			{
				VmCumplimiento.Search(SesionActual.uidEmpresaActual.Value, SesionActual.uidSucursalActual.Value, this.sUidEncargado, this.sUidDepartamentos, this.sFolioTurno, this.sDtFechaInicio, this.sDtFechaFin);
				this.LsTurnos = VmCumplimiento.LsTurnos;
				gvTurnos.DataSource = VmCumplimiento.LsTurnos;
				gvTurnos.DataBind();
				HideShowFilters("Ocultar");
			}
			catch (Exception ex)
			{
				DisplayMessageLeft("Ocurrio un <strong>error</strong> inesperado: </br> " + ex.Message, "danger");
			}
		}
		private void HideShowFilters(string ToDisplay)
		{
			if (ToDisplay.Equals("Mostrar"))
			{
				lblOcultarFiltros.Text = "Ocultar";
				pnlFiltros.Visible = true;
				pnlTurnos.Visible = false;
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBusqueda.CssClass = "btn btn-sm btn-default";

			}
			else
			{
				lblOcultarFiltros.Text = "Mostrar";
				pnlFiltros.Visible = false;
				pnlTurnos.Visible = true;
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBusqueda.CssClass = "btn btn-sm btn-default disabled";
			}
		}

		protected void btnRealizar_Click(object sender, EventArgs e)
		{
			btnCancelar.CssClass = "btn btn-sm btn-default disabled";
			btnRealizar.CssClass = "btn btn-sm btn-default disabled";

			rbDoneBySupervisor.Checked = false;
			rbDoneByEncargado.Checked = true;

			HabilitarCamposCumplimiento(true);

			btnOkRealizar.Visible = true;
			btnOkRealizar.Enabled = true;
			btnCancelRealizar.Visible = true;
		}
		protected void btnOkRealizar_Click(object sender, EventArgs e)
		{
			btnOkRealizar.Enabled = false;
			HabilitarCamposCumplimiento(false);
			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			VmCumplimiento.GetTarea(new Guid(hfUidTarea.Value));
			bool? blValor = null;
			decimal? DcValor1 = null;
			decimal? DcValor2 = null;
			Guid UidOpcion = Guid.Empty;

			string StrTipoMedicion = VmCumplimiento.tTarea.StrTipoMedicion;
			switch (StrTipoMedicion)
			{
				case "Verdadero/Falso":
					if (!rbCumplimientoNo.Checked && !rbCumplimientoSi.Checked)
					{
						DisplayMessageLeft("Seleccione una opcion <strong>(SI)</strong> o <strong>(NO)</strong>.", "warning");
						return;
					}
					if (rbCumplimientoNo.Checked && rbCumplimientoSi.Checked)
					{
						DisplayMessageLeft("Seleccione solo una opcion <strong>(SI)</strong> o <strong>(NO)</strong>.", "warning");
						return;
					}
					blValor = rbCumplimientoNo.Checked ? false : true;
					break;
				case "Numerico":
					decimal Aux = 0;
					if (string.IsNullOrWhiteSpace(txtCumplimientoValor.Text.Trim()))
					{
						DisplayMessageLeft("Ingrese un <strong>valor</strong>.", "warning");
						return;
					}
					if (decimal.TryParse(txtCumplimientoValor.Text.Trim(), out Aux))
					{
						DcValor1 = decimal.Parse(txtCumplimientoValor.Text.Trim());
					}
					else
					{
						DisplayMessageLeft("Ingrese un valor <strong>númerico</strong>.", "warning");
						return;
					}
					break;
				case "Seleccionable":
					try
					{
						UidOpcion = new Guid(ddlCumplimientoSeleccion.SelectedValue.ToString());
					}
					catch (Exception)
					{
						DisplayMessageLeft("Seleccione una opción <strong>valida</strong>.", "warning");
						return;
					}
					break;
				default:
					break;
			}

			try
			{
				VmCumplimiento.GetTurno(new Guid(hfUidInicioTurno.Value));
				Guid UidPeriodo = VmCumplimiento.tiTurno.UidPeriodo;
				VmCumplimiento.GetPeriodo(UidPeriodo);

				Guid UidCumplimiento = new Guid(hfUidCumplimiento.Value);
				string EstadoCumplimiento = VmCumplimiento.CumplimientoCompleto;
				string StrObservaciones = txtObservaciones.Text.Trim();

				Guid UidTurno = VmCumplimiento.pPeriodo.UidTurno;
				Guid UidCreador = SesionActual.uidUsuario;
				string StrRolCreador = SesionActual.Rol;
				Guid UidTipoMedicion = VmCumplimiento.tTarea.UidMedicion;
				bool BlAtrasado = true;
				DateTimeOffset DtCumplimiento = SesionActual.GetDateTimeOffset();
				Guid UidUsuario = VmCumplimiento.pPeriodo.UidUsuario;

				if (SesionActual.Rol.Equals("Administrador"))
				{
					if (rbDoneBySupervisor.Checked)
					{
						Guid UidDepartamento = VmCumplimiento.pPeriodo.UidDepartamento;
						VmCumplimiento.GetSupervisorDepartamento(UidDepartamento, DtCumplimiento.Date);
						UidUsuario = VmCumplimiento.uUsuario.UIDUSUARIO;
					}
				}
				else if (SesionActual.Rol.Equals("Supervisor"))
				{
					if (rbDoneBySupervisor.Checked)
					{
						UidUsuario = SesionActual.uidUsuario;
					}
				}


				if (VmCumplimiento.SetCumplimiento(EstadoCumplimiento, UidCumplimiento, UidUsuario, StrObservaciones, blValor, DcValor1, DcValor2, UidOpcion, UidTurno, UidCreador, StrRolCreador, UidTipoMedicion, BlAtrasado, DtCumplimiento))
				{
					DisplayMessageLeft("<strong>Guardado</strong> correctamente.", "success");
					btnOkIncumplimiento.Visible = false;
					btnCancelarIncumplimiento.Visible = false;
					txtObservaciones.Enabled = false;

					bool BlNotificacionCreada = VmCumplimiento.SetNotification(UidCumplimiento, StrTipoMedicion);

					VmCumplimiento.CrearSucesor(UidCumplimiento, new Guid(hfUidTarea.Value), BlNotificacionCreada, VmCumplimiento.tiTurno.Folio);

					VmCumplimiento.SendNotificacion(UidCumplimiento);
				}
				else
				{
					DisplayMessageLeft("Ocurrio un error al <strong>realizar cumplimiento</strong> de la tarea", "danger");
				}
			}
			catch (Exception ex)
			{
				throw;
			}

			VmCumplimiento.GetTareasNoCumplidas(this.UidDepartamentoTurno, this.IntFolioTurno, this.FCIntFolio, this.FCStrTarea, this.FCUidDepartamento, this.FCUidArea, this.FCUidTipoTarea);
			this.LsCumplimientos = VmCumplimiento.LsTareasNoCompletadas;
			gvCumplimientos.SelectedIndex = -1;
			gvCumplimientos.DataSource = this.LsCumplimientos;
			gvCumplimientos.DataBind();

			btnOkRealizar.Visible = false;
			btnCancelRealizar.Visible = false;
		}
		protected void btnCancelRealizar_Click(object sender, EventArgs e)
		{
			btnCancelar.CssClass = "btn btn-sm btn-default";
			btnRealizar.CssClass = "btn btn-sm btn-default";

			HabilitarCamposCumplimiento(false);

			btnOkRealizar.Visible = false;
			btnCancelRealizar.Visible = false;
		}

		protected void btnCancelar_Click(object sender, EventArgs e)
		{
			btnCancelar.CssClass = "btn btn-sm btn-default disabled";
			btnRealizar.CssClass = "btn btn-sm btn-default disabled";

			txtObservaciones.Enabled = true;

			rbDoneBySupervisor.Checked = false;
			rbDoneByEncargado.Checked = true;

			btnOkIncumplimiento.Enabled = true;
			btnOkIncumplimiento.Visible = true;
			btnCancelarIncumplimiento.Visible = true;
		}
		protected void btnOkIncumplimiento_Click(object sender, EventArgs e)
		{
			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			if (string.IsNullOrWhiteSpace(txtObservaciones.Text))
			{
				DisplayMessageLeft("Es requerido agregar observaciones.", "warning");
				return;
			}
			btnOkIncumplimiento.Enabled = false;
			try
			{
				VmCumplimiento.GetTarea(new Guid(hfUidTarea.Value));
				VmCumplimiento.GetTurno(new Guid(hfUidInicioTurno.Value));
				Guid UidPeriodo = VmCumplimiento.tiTurno.UidPeriodo;
				VmCumplimiento.GetPeriodo(UidPeriodo);


				Guid UidCumplimiento = new Guid(hfUidCumplimiento.Value);
				string EstadoCumplimiento = VmCumplimiento.CumplimientoCancelado;
				string StrObservaciones = txtObservaciones.Text.Trim();
				bool? blValor = null;
				decimal? DcValor1 = null;
				decimal? DcValor2 = null;
				Guid UidOpcion = Guid.Empty;
				Guid UidTurno = VmCumplimiento.pPeriodo.UidTurno;
				Guid UidCreador = SesionActual.uidUsuario;
				string StrRolCreador = SesionActual.Rol;
				Guid UidTipoMedicion = VmCumplimiento.tTarea.UidMedicion;
				bool BlAtrasado = true;
				DateTimeOffset DtCumplimiento = SesionActual.GetDateTimeOffset();
				Guid UidUsuario = VmCumplimiento.pPeriodo.UidUsuario;

				if (SesionActual.Rol.Equals("Administrador"))
				{
					if (rbDoneBySupervisor.Checked)
					{
						Guid UidDepartamento = VmCumplimiento.pPeriodo.UidDepartamento;
						VmCumplimiento.GetSupervisorDepartamento(UidDepartamento, DtCumplimiento.Date);
						UidUsuario = VmCumplimiento.uUsuario.UIDUSUARIO;
					}
				}
				else if (SesionActual.Rol.Equals("Supervisor"))
				{
					if (rbDoneBySupervisor.Checked)
					{
						UidUsuario = SesionActual.uidUsuario;
					}
				}

				if (VmCumplimiento.SetCumplimiento(EstadoCumplimiento, UidCumplimiento, UidUsuario, StrObservaciones, blValor, DcValor1, DcValor2, UidOpcion, UidTurno, UidCreador, StrRolCreador, UidTipoMedicion, BlAtrasado, DtCumplimiento))
				{
					DisplayMessageLeft("<strong>Guardado</strong> correctamente.", "success");
					btnOkIncumplimiento.Visible = false;
					btnCancelarIncumplimiento.Visible = false;
					txtObservaciones.Enabled = false;

					btnOkIncumplimiento.Visible = false;
					btnCancelarIncumplimiento.Visible = false;
				}
				else
				{
					btnOkIncumplimiento.Enabled = true;
					btnOkIncumplimiento.Visible = true;
					btnCancelarIncumplimiento.Visible = true;
					DisplayMessageLeft("Ocurrio un error al <strong>cancelar</strong> la tarea", "danger");
				}
			}
			catch (Exception ex)
			{
				DisplayMessageLeft("Ocurrio un <strong>error</strong> inesperado: </br>" + ex.Message, "warning");
			}

			VmCumplimiento.GetTareasNoCumplidas(this.UidDepartamentoTurno, this.IntFolioTurno, this.FCIntFolio, this.FCStrTarea, this.FCUidDepartamento, this.FCUidArea, this.FCUidTipoTarea);
			this.LsCumplimientos = VmCumplimiento.LsTareasNoCompletadas;
			gvCumplimientos.SelectedIndex = -1;
			gvCumplimientos.DataSource = this.LsCumplimientos;
			gvCumplimientos.DataBind();
		}
		protected void btnCancelarIncumplimiento_Click(object sender, EventArgs e)
		{
			btnCancelar.CssClass = "btn btn-sm btn-default";
			btnRealizar.CssClass = "btn btn-sm btn-default";

			txtObservaciones.Enabled = false;

			btnOkIncumplimiento.Visible = false;
			btnCancelarIncumplimiento.Visible = false;
		}

		protected void btnCerrarCumplimiento_Click(object sender, EventArgs e)
		{
			pnlCumplimiento.Visible = false;
			pnlBotonesFiltros.Visible = true;

			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			if (lblOcultarFiltros.Text.Equals("Mostrar"))
				HideShowFilters("Ocultar");
			else
				HideShowFilters("Mostrar");
		}
		#endregion

		#region GridView
		protected void gvTurnos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTurnos, "Select$" + e.Row.RowIndex);

				if (hfUidInicioTurno.Value != string.Empty)
				{
					if (gvTurnos.DataKeys[e.Row.RowIndex].Value.ToString().Equals(hfUidInicioTurno.Value))
						e.Row.RowState = DataControlRowState.Selected;
				}
			}
		}
		protected void gvTurnos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{

		}
		protected void gvTurnos_Sorting(object sender, GridViewSortEventArgs e)
		{
			VmCumplimiento.LsTurnos = this.LsTurnos;
			VmCumplimiento.SortList(hfSoftDirectionTurnos.Value, e.SortExpression);
			this.LsTurnos = VmCumplimiento.LsTurnos;

			if (hfSoftDirectionTurnos.Value.Equals("ASC"))
				hfSoftDirectionTurnos.Value = "DESC";
			else
				hfSoftDirectionTurnos.Value = "ASC";

			gvTurnos.SelectedIndex = -1;
			gvTurnos.DataSource = this.LsTurnos;
			gvTurnos.DataBind();
		}
		protected void gvTurnos_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvTurnos.SelectedIndex = -1;
			gvTurnos.DataSource = this.LsTurnos;
			gvTurnos.PageIndex = e.NewPageIndex;
			gvTurnos.DataBind();
		}
		protected void gvTurnos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlMessageRight.Visible)
					pnlMessageRight.Visible = false;

				Guid UidInicioTurno = new Guid(gvTurnos.SelectedDataKey.Value.ToString());
				hfUidInicioTurno.Value = UidInicioTurno.ToString();

				VmCumplimiento.GetTurno(UidInicioTurno);
				int FolioTurno = VmCumplimiento.tiTurno.Folio;
				Guid UidDepartamento = VmCumplimiento.tiTurno.UidDepartamento;

				this.DtFechaInicioTurno = VmCumplimiento.tiTurno.DtoFechaInicio.Value.DateTime;
				this.UidDepartamentoTurno = UidDepartamento;
				hfUidPeriodoTurno.Value = VmCumplimiento.tiTurno.UidPeriodo.ToString();
				lblHoraInicioTurno.Text = VmCumplimiento.tiTurno.DtoFechaInicio.Value.ToString("t");
				lblHoraFinTurno.Text = VmCumplimiento.tiTurno.DtoFechaFin.Value.ToString("t");
				this.IntFolioTurno = FolioTurno;

				MostrarFiltroCumplimiento("Ocultar");
				btnFiltroCLimpiar.CssClass = "btn btn-sm btn-default";
				btnFiltroCBuscar.CssClass = "btn btn-sm btn-default";
				EnableSearchFields(true);
				btnPrintReporteTurno.CssClass = "btn btn-sm btn-default";
				VmCumplimiento.GetTareasNoCumplidas(UidDepartamento, FolioTurno, null, string.Empty, Guid.Empty, Guid.Empty, Guid.Empty);
				this.LsCumplimientos = VmCumplimiento.LsTareasNoCompletadas;
				gvCumplimientos.SelectedIndex = -1;
				gvCumplimientos.DataSource = this.LsCumplimientos;
				gvCumplimientos.DataBind();

				this.UidPeriodoTurno = VmCumplimiento.tiTurno.UidPeriodo;
				VmCumplimiento.GetUltimoTurnoAbiertoPeriodo(VmCumplimiento.tiTurno.UidPeriodo, VmCumplimiento.tiTurno.Folio);
				lblTareasNuevas.Text = "<span class='glyphicon glyphicon-print'></span> Tareas nuevas";
				if (VmCumplimiento.tiTurnoAbierto == null)
				{
					btnPrintNuevasTareas.Enabled = false;
					btnPrintNuevasTareas.CssClass = "btn btn-sm btn-default disabled";
				}
				else
				{
					int TotalNuevosCumplimientos = VmCumplimiento.GetTotalCumplimientosNuevosTurno(this.UidPeriodoTurno, VmCumplimiento.tiTurnoAbierto.DtoFechaInicio.Value.DateTime, this.IntFolioTurno);
					if (TotalNuevosCumplimientos > 0)
					{
						lblTareasNuevas.Text = "<span class='glyphicon glyphicon-print'></span> Tareas nuevas <strong>" + TotalNuevosCumplimientos + "</strong>";
						btnPrintNuevasTareas.Enabled = true;
						btnPrintNuevasTareas.CssClass = "btn btn-sm btn-default";
					}
					else
					{
						btnPrintNuevasTareas.Enabled = false;
						btnPrintNuevasTareas.CssClass = "btn btn-sm btn-default disabled";
					}
				}

			}
			catch (Exception ex)
			{
				throw;
			}
		}
		#endregion
		#endregion

		#region Right
		#region Buttons
		protected void btnFiltroCLimpiar_Click(object sender, EventArgs e)
		{
			txtFiltroCFolio.Text = string.Empty;
			txtFiltroCTarea.Text = string.Empty;
			ddlFiltroCDepartamento.SelectedIndex = 0;
			ddlFiltroCArea.SelectedIndex = 0;
			ddlFiltroCTipo.SelectedIndex = 0;
		}
		protected void btnFiltroCOcultar_Click(object sender, EventArgs e)
		{
			MostrarFiltroCumplimiento(lblFiltroCOcultar.Text);
		}
		protected void btnFiltroCBuscar_Click(object sender, EventArgs e)
		{
			if (!string.IsNullOrWhiteSpace(txtFiltroCFolio.Text))
			{
				int Aux = 0;
				if (!int.TryParse(txtFiltroCFolio.Text, out Aux))
				{
					DisplayMessageRight("Ingrese un folio <strong>númerico</strong>.", "danger");
					return;
				}
			}

			this.FCIntFolio = string.IsNullOrWhiteSpace(txtFiltroCFolio.Text) ? (int?)null : int.Parse(txtFiltroCFolio.Text.Trim());
			this.FCStrTarea = txtFiltroCTarea.Text.Trim();
			this.FCUidDepartamento = new Guid(ddlFiltroCDepartamento.SelectedValue.ToString());
			this.FCUidArea = new Guid(ddlFiltroCArea.SelectedValue.ToString());
			this.FCUidTipoTarea = new Guid(ddlFiltroCTipo.SelectedValue.ToString());


			VmCumplimiento.GetTareasNoCumplidas(this.UidDepartamentoTurno, this.IntFolioTurno, this.FCIntFolio, this.FCStrTarea, this.FCUidDepartamento, this.FCUidArea, this.FCUidTipoTarea);
			this.LsCumplimientos = VmCumplimiento.LsTareasNoCompletadas;
			gvCumplimientos.SelectedIndex = -1;
			gvCumplimientos.DataSource = this.LsCumplimientos;
			gvCumplimientos.DataBind();

			MostrarFiltroCumplimiento("Ocultar");
		}
		private void MostrarFiltroCumplimiento(string ToShow)
		{
			if (ToShow.Equals("Mostrar"))
			{
				lblFiltroCOcultar.Text = "Ocultar";
				pnlFiltrosCumplimiento.Visible = true;
				pnlListaCumplimientos.Visible = false;
			}
			else
			{
				lblFiltroCOcultar.Text = "Mostrar";
				pnlFiltrosCumplimiento.Visible = false;
				pnlListaCumplimientos.Visible = true;
			}
		}
		#endregion

		#region Field
		private void EnableSearchFields(bool IsEnabled)
		{
			txtFiltroCFolio.Enabled = IsEnabled;
			txtFiltroCTarea.Enabled = IsEnabled;
			ddlFiltroCDepartamento.Enabled = IsEnabled;
			ddlFiltroCArea.Enabled = IsEnabled;
			ddlFiltroCTipo.Enabled = IsEnabled;
		}
		#endregion

		#region GridView
		protected void gvCumplimientos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvCumplimientos, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvCumplimientos_Sorting(object sender, GridViewSortEventArgs e)
		{
			try
			{
				if (pnlMessageRight.Visible)
					pnlMessageRight.Visible = false;

				gvCumplimientos.SelectedIndex = -1;
				VmCumplimiento.LsTareasNoCompletadas = this.LsCumplimientos;
				VmCumplimiento.SortListCumplimiento(hfSortCumplimiento.Value, e.SortExpression);
				this.LsCumplimientos = VmCumplimiento.LsTareasNoCompletadas;
				gvCumplimientos.DataSource = this.LsCumplimientos;
				gvCumplimientos.DataBind();

				if (hfSortCumplimiento.Value.Equals("ASC"))
					hfSortCumplimiento.Value = "DESC";
				else
					hfSortCumplimiento.Value = "ASC";
			}
			catch (Exception ex)
			{
				DisplayMessageRight("Ocurrio un error al ordenar: </br>" + ex.Message, "danger");
			}
		}
		protected void gvCumplimientos_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			try
			{
				if (pnlMessageRight.Visible)
					pnlMessageRight.Visible = false;

				gvCumplimientos.SelectedIndex = -1;
				gvCumplimientos.PageIndex = e.NewPageIndex;
				gvCumplimientos.DataSource = this.LsCumplimientos;
				gvCumplimientos.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageRight("Ocurrio un error al ordenar: </br>" + ex.Message, "danger");
			}
		}
		protected void gvCumplimientos_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{

		}
		protected void gvCumplimientos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlMessageRight.Visible)
					pnlMessageRight.Visible = false;

				Guid UidCumplimiento = new Guid(gvCumplimientos.SelectedDataKey.Value.ToString());
				Guid UidTarea;
				Guid UidDepartamento;
				Guid UidArea;
				hfUidCumplimiento.Value = UidCumplimiento.ToString();

				VmCumplimiento.GetCumplimiento(UidCumplimiento);
				UidTarea = VmCumplimiento.cCumplimiento.UidTarea;
				hfUidTarea.Value = UidTarea.ToString();
				UidDepartamento = VmCumplimiento.cCumplimiento.UidDepartamento == null ? Guid.Empty : VmCumplimiento.cCumplimiento.UidDepartamento.Value;
				UidArea = VmCumplimiento.cCumplimiento.UidArea == null ? Guid.Empty : VmCumplimiento.cCumplimiento.UidArea.Value;
				VmCumplimiento.GetTarea(UidTarea);

				if (UidDepartamento != Guid.Empty)
				{
					VmCumplimiento.GetDepartamento(UidDepartamento);
					txtDepartamento.Text = VmCumplimiento.dDepartamento.StrNombre;
				}
				else
				{
					txtDepartamento.Text = "";
				}

				if (UidArea != Guid.Empty)
				{
					VmCumplimiento.GetArea(UidArea);
					txtArea.Text = VmCumplimiento.aArea.StrNombre;
				}
				else
				{
					txtArea.Text = "(GLOBAL)";
				}

				VmCumplimiento.GetFrecuencia(UidTarea);
				txtFrecuenciaPeriodicidad.Text = VmCumplimiento.tfFrecuencia.StrTipoFrecuencia;

				txtTipo.Text = VmCumplimiento.tTarea.StrTipoTarea;

				pnlYesNo.Visible = false;
				pnlValue.Visible = false;
				pnlSelection.Visible = false;
				switch (VmCumplimiento.tTarea.StrTipoMedicion)
				{
					case "Verdadero/Falso":
						pnlYesNo.Visible = true;
						break;
					case "Numerico":
						lblUnidadMedida.Text = VmCumplimiento.tTarea.StrUnidadMedida;
						pnlValue.Visible = true;
						break;
					case "Seleccionable":
						VmCumplimiento.GetOpcionesTarea(UidTarea);
						ddlCumplimientoSeleccion.DataSource = VmCumplimiento.LsOpciones;
						ddlCumplimientoSeleccion.DataTextField = "StrOpciones";
						ddlCumplimientoSeleccion.DataValueField = "UidOpciones";
						ddlCumplimientoSeleccion.DataBind();
						pnlSelection.Visible = true;
						break;
					default:
						break;
				}

				txtTarea.Text = VmCumplimiento.tTarea.StrNombre;
				HabilitarCamposCumplimiento(false);
				LimpiarCamposCumplimiento();

				pnlCumplimiento.Visible = true;
				pnlFiltros.Visible = false;
				pnlTurnos.Visible = false;
				pnlBotonesFiltros.Visible = false;

				btnOkRealizar.Visible = false;
				btnCancelRealizar.Visible = false;

				btnOkIncumplimiento.Visible = false;
				btnCancelarIncumplimiento.Visible = false;

				btnCancelar.CssClass = "btn btn-sm btn-default";
				btnRealizar.CssClass = "btn btn-sm btn-default";

				if (VmCumplimiento.cCumplimiento.StrEstadoCumplimiento.Equals("Completo") ||
					VmCumplimiento.cCumplimiento.StrEstadoCumplimiento.Equals("Cancelado"))
				{
					txtObservaciones.Text = VmCumplimiento.cCumplimiento.StrObservacion;
					btnCancelar.CssClass = "btn btn-sm btn-default disabled";
					btnRealizar.CssClass = "btn btn-sm btn-default disabled";
				}
			}
			catch (Exception ex)
			{
				DisplayMessageRight("Ocurrion un <strong>error</strong>: </br>" + ex.Message, "danger");
			}
		}
		#endregion
		#endregion

		#region Aux
		private void DisplayMessageLeft(string Message, string Type)
		{
			lblMessageLeft.Text = Message;
			pnlMessageLeft.CssClass = "alert alert-" + Type;
			pnlMessageLeft.Visible = true;
		}
		protected void HideMessageLeft(object sender, EventArgs e)
		{
			pnlMessageLeft.Visible = false;
		}

		private void DisplayMessageRight(string Message, string Type)
		{
			lblMessageRight.Text = Message;
			pnlMessageRight.CssClass = "alert alert-" + Type;
			pnlMessageRight.Visible = true;
		}
		protected void HideMessageRight(object sender, EventArgs e)
		{
			pnlMessageRight.Visible = false;
		}

		private bool IsNumericTextValue(string Value)
		{
			int Aux = 0;
			return int.TryParse(Value, out Aux);
		}

		private string GetValuesFromListBox(ListBox lbList)
		{
			string Values = string.Empty;
			foreach (ListItem item in lbList.Items)
			{
				if (item.Selected)
				{
					if (string.IsNullOrEmpty(Values))
						Values = item.Value;
					else
						Values = "," + item.Value;
				}
			}
			return Values;
		}

		private void HabilitarCamposCumplimiento(bool IsEnabled)
		{
			rbCumplimientoSi.Enabled = IsEnabled;
			rbCumplimientoNo.Enabled = IsEnabled;
			txtCumplimientoValor.Enabled = IsEnabled;
			txtObservaciones.Enabled = IsEnabled;
			ddlCumplimientoSeleccion.Enabled = IsEnabled;
		}
		private void LimpiarCamposCumplimiento()
		{
			rbCumplimientoSi.Checked = false;
			rbCumplimientoNo.Checked = true;

			txtCumplimientoValor.Text = string.Empty;
			txtObservaciones.Text = String.Empty;

			if (ddlCumplimientoSeleccion.Items.Count > 0)
				ddlCumplimientoSeleccion.SelectedIndex = 0;
		}


		#endregion

		protected void ddlFiltroCDepartamento_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlMessageRight.Visible)
					pnlMessageRight.Visible = false;

				Guid UidDepartamento = new Guid(ddlFiltroCDepartamento.SelectedValue.ToString());
				VmCumplimiento.GetAreasDepartamento(UidDepartamento);
				ddlFiltroCArea.DataSource = VmCumplimiento.LsAreas;
				ddlFiltroCArea.DataValueField = "UidArea";
				ddlFiltroCArea.DataTextField = "StrNombre";
				ddlFiltroCArea.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageRight("Ocurrio un error al obtener areas del departamento seleccionado.</br>:" + ex.Message, "danger");
			}
		}

		protected void btnPrintReporteTurno_Click(object sender, EventArgs e)
		{
			if (pnlMessageRight.Visible)
				pnlMessageRight.Visible = false;

			if (hfUidPeriodoTurno.Value != string.Empty)
			{
				Session["Periodo"] = new Guid(hfUidPeriodoTurno.Value);
				Session["Fecha"] = this.DtFechaInicioTurno;
				Session["HoraInicio"] = lblHoraInicioTurno.Text;
				Session["HoraFin"] = lblHoraFinTurno.Text.Length == 0 ? "Sin Cerrar Turno" : lblHoraFinTurno.Text;
				Session["Replica"] = "[Duplicado]";
				Session["FolioTurno"] = this.IntFolioTurno;
				ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "open", "window.open('ReporteTareas.aspx', '_blank')", true);
			}
			else
			{
				DisplayMessageRight("Seleccione un turno", "info");
			}
		}

		protected void btnPrintNuevasTareas_Click(object sender, EventArgs e)
		{
			try
			{
				VmCumplimiento.GetUltimoTurnoAbiertoPeriodo(this.UidPeriodoTurno, this.IntFolioTurno);
				if (VmCumplimiento.tiTurnoAbierto != null)
				{
					Session["CAPeriodo"] = this.UidPeriodoTurno;
					Session["CAFecha"] = VmCumplimiento.tiTurnoAbierto.DtoFechaInicio.Value.DateTime;
					Session["CAHoraInicio"] = VmCumplimiento.tiTurnoAbierto.DtoFechaInicio.Value.ToString("t");
					Session["CAHoraFin"] = VmCumplimiento.tiTurnoAbierto.DtoFechaFin == null ? "Turno sin cerrar" : VmCumplimiento.tiTurnoAbierto.DtoFechaFin.Value.ToString("t");
					Session["CAFolioTurnoA"] = this.IntFolioTurno;
					Session["CAFolioTurnoN"] = VmCumplimiento.tiTurnoAbierto.Folio;

					ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
						"window.open('ReporteTareasNuevasTurno.aspx', '_blank')", true);
					return;
				}
				else
				{
					DisplayMessageRight("No hay turnos abiertos.", "danger");
				}
			}
			catch (Exception ex)
			{
				DisplayMessageRight("Ocurrio un error al mostrar las tareas", "danger");
			}
		}
	}
}