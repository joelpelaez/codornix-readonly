﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class Notificaciones : System.Web.UI.Page
	{
		private VMNotificacion VM = new VMNotificacion();

		private List<MensajeNotificacion> VSNotificaciones
		{
			get
			{
				return ViewState["Notificaciones"] as List<MensajeNotificacion>;
			}
			set
			{
				ViewState["Notificaciones"] = value;
			}
		}

		private List<ParametroOrdenamiento> LsOrdenamiento
		{
			get
			{
				return ViewState["POrdenamiento"] as List<ParametroOrdenamiento>;
			}
			set
			{
				ViewState["POrdenamiento"] = value;
			}
		}

		private Sesion SesionActual
		{
			get
			{
				return (Sesion)Session["Sesion"];
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			// No hacer nada si no tiene algun turno iniciado. Master se hace cargo de ello.
			if (SesionActual == null)
				return;

			// No permite acceder en caso de no iniciar una sucursal (solo para administradores).
			if (!SesionActual.uidSucursalActual.HasValue)
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			// Verificar permisos
			if (!Acceso.TieneAccesoAModulo("Cumplimiento", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				btnLeido.Disable();
				RealizarBusqueda();
				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnBuscar.Visible = false;
				VM.ObtenerEstados();
				lbEstados.DataSource = VM.Estados;
				lbEstados.DataValueField = "UidEstadoNotitifacion";
				lbEstados.DataTextField = "StrEstadoNotificacion";
				lbEstados.DataBind();

				pnlAlertDetail.Visible = false;
				pnlAlertBusqueda.Visible = false;

				//VM.CargarParametrosOrdenamiento();
				//gvOrdenamiento.DataSource = VM.LsParametrosOrdenamiento;
				//gvOrdenamiento.DataBind();

				ViewState["POrdenamiento"] = VM.LsParametrosOrdenamiento;
			}
		}

		protected void btnLeido_Click(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertDetail.Visible == true)
					pnlAlertDetail.Visible = false;

				Guid uid = new Guid(fldUidMensajeNotificacion.Value);

				if (btnLeido.Text == "Leido")
				{
					btnLeido.Text = "No leido";
					VM.CambiarEstado(uid, "Leido");
					RealizarBusqueda();
				}
				else
				{
					btnLeido.Text = "Leido";
					VM.CambiarEstado(uid, "No leido");
				}

				Site1 master = (Site1)Page.Master;
				master.TotalNotificacionesNoRevisadas();
			}
			catch (Exception)
			{
				pnlAlertDetail.Visible = true;
				lblErrorDetail.Text = "Ocurrio un error al guardar";
			}
			finally
			{
				RealizarBusqueda();
			}
		}

		//protected void gvOrdenamiento_RowDataBound(object sender, GridViewRowEventArgs e)
		//{
		//	if (e.Row.RowType == DataControlRowType.DataRow)
		//	{
		//		e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvOrdenamiento, "Select$" + e.Row.RowIndex);
		//		LinkButton Change = e.Row.FindControl("btnChangeDirection") as LinkButton;
		//		Label Icon = e.Row.FindControl("lblIcon") as Label;
		//		if (e.Row.Cells[4].Text.Equals("ASC"))
		//		{
		//			Icon.CssClass = "glyphicon glyphicon-chevron-down";
		//			Change.ToolTip = "Descendente";
		//		}
		//		else if (e.Row.Cells[4].Text.Equals("DESC"))
		//		{
		//			Icon.CssClass = "glyphicon glyphicon-chevron-up";
		//			Change.ToolTip = "Ascendente";
		//		}
		//	}
		//}
		//protected void gvOrdenamiento_SelectedIndexChanged(object sender, EventArgs e)
		//{

		//}

		protected void dgvNotificaciones_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertDetail.Visible == true)
					pnlAlertDetail.Visible = false;

				Guid uid = new Guid(dgvNotificaciones.SelectedDataKey.Value.ToString());
				fldUidMensajeNotificacion.Value = uid.ToString();
				VM.ObtenerNotificacion(uid);

				lblTarea.Text = VM.Notificacion.StrTarea;
				lblDepto.Text = VM.Notificacion.StrDepartamento;
				lblArea.Text = VM.Notificacion.StrArea;
				lblResultado.Text = VM.Notificacion.StrResultado;
				lblTipoTarea.Text = VM.Notificacion.TipoTarea;
				lblObservaciones.Text = VM.Notificacion.Observaciones == string.Empty ? "(Sin observaciones)" : VM.Notificacion.Observaciones;

				if (VM.Notificacion.UnidadMedida != string.Empty)
				{
					lblResultado.Text += " " + VM.Notificacion.UnidadMedida;
				}

				string estado = VM.Notificacion.StrEstadoNotificacion;

				if (estado == "Leido")
				{
					btnLeido.Text = "No leido";
				}
				else
				{
					btnLeido.Text = "Leido";
				}

				btnLeido.Enable();
			}
			catch (Exception ex)
			{
				pnlAlertDetail.Visible = true;
				lblErrorDetail.Text = "Error al obtener datos";
			}
			finally
			{
				int pos = -1;
				if (ViewState["NotiPreviousRow"] != null)
				{
					pos = (int)ViewState["NotiPreviousRow"];
					GridViewRow previousRow = dgvNotificaciones.Rows[pos];
					previousRow.RemoveCssClass("success");
				}

				ViewState["NotiPreviousRow"] = dgvNotificaciones.SelectedIndex;
				dgvNotificaciones.SelectedRow.AddCssClass("success");
			}
		}
		protected void dgvNotificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvNotificaciones, "Select$" + e.Row.RowIndex);

				Label icon = e.Row.FindControl("lblCompleto") as Label;


				if (e.Row.Cells[5].Text == "No leido")
				{
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon glyphicon-eye-close");
					icon.ToolTip = "No Leido (No Revisado)";
				}
				else if (e.Row.Cells[5].Text == "Leido")
				{
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-ok");
					icon.ToolTip = "Leido (Revisado)";
				}
			}
		}
		protected void dgvNotificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ViewState["NotiPreviousRow"] = null;
			if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
			{
				string SortExpression = (string)ViewState["SortColumn"];
				SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
				SortNotificaciones(SortExpression, SortDirection, true);
			}
			else
			{
				dgvNotificaciones.DataSource = VSNotificaciones;
			}
			dgvNotificaciones.PageIndex = e.NewPageIndex;
			dgvNotificaciones.DataBind();
		}

		protected void dgvNotificaciones_Sorting(object sender, GridViewSortEventArgs e)
		{
			ViewState["NotiPreviousRow"] = null;
			SortNotificaciones(e.SortExpression, e.SortDirection, false);
			dgvNotificaciones.DataBind();
		}

		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			RealizarBusqueda();
			btnMostrar.Text = "Ocultar";
			panelBusqueda.Visible = false;
			panelResultados.Visible = true;
			btnLimpiar.Visible = false;
			btnBuscar.Visible = false;
		}
		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			lbEstados.ClearSelection();
			txtFechaInicio.Text = "";
			txtFechaFin.Text = "";
		}
		protected void btnMostrar_Click(object sender, EventArgs e)
		{
			if (btnMostrar.Text == "Mostrar")
			{
				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnBuscar.Visible = false;
			}
			else
			{
				btnMostrar.Text = "Mostrar";
				panelBusqueda.Visible = true;
				panelResultados.Visible = false;
				btnLimpiar.Visible = true;
				btnBuscar.Visible = true;
			}
		}

		protected void btnCloseAlert_Click(object sender, EventArgs e)
		{
			pnlAlertDetail.Visible = false;
		}
		protected void btnCloseAlertSearch_Click(object sender, EventArgs e)
		{
			pnlAlertBusqueda.Visible = false;
		}

		private void RealizarBusqueda()
		{
			try
			{
				if (pnlAlertBusqueda.Visible == true)
					pnlAlertBusqueda.Visible = false;

				string deptos = null;
				if (SesionActual.UidDepartamentos.Count > 0)
				{
					deptos = SesionActual.UidDepartamentos[0].ToString();
					for (int i = 1; i < SesionActual.UidDepartamentos.Count; i++)
					{
						deptos += "," + SesionActual.UidDepartamentos[i].ToString();
					}
				}

				string deptosBusqueda = null;

				string estados = null;
				int[] j = lbEstados.GetSelectedIndices();
				if (j.Length > 0)
				{
					estados = lbEstados.Items[j[0]].Value;
					for (int i = 1; i < j.Length; i++)
					{
						estados += "," + lbEstados.Items[j[i]].Value;
					}
				}

				DateTime? fechaInicio = null, fechaFin = null;

				if (!string.IsNullOrWhiteSpace(txtFechaInicio.Text))
				{
					fechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				if (!string.IsNullOrWhiteSpace(txtFechaFin.Text))
				{
					fechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}

				//bool ExpFolioCumplimiento = false;
				//string DirFolioCumplimiento = "ASC";

				//bool ExpFolioTarea = false;
				//string DirFolioTarea = "ASC";

				//bool ExpTarea = false;
				//string DirTarea = "ASC";

				//bool ExpDepartamento = false;
				//string DirDepartamento = "ASC";

				//bool ExpFecha = false;
				//string DirFecha = "ASC";

				//bool ExpHora = false;
				//string DirHora = "ASC";

				//bool ExpEstado = false;
				//string DirEstado = "ASC";

				//foreach (var item in this.LsOrdenamiento)
				//{
				//	if (item.Parametro.Equals("Folio de Cumplimiento"))
				//	{
				//		ExpFolioCumplimiento = item.BlOrdenar;
				//		DirFolioCumplimiento = item.Direccion;
				//	}
				//	else if (item.Parametro.Equals("Folio de Tarea"))
				//	{
				//		ExpFolioTarea = item.BlOrdenar;
				//		DirFolioTarea = item.Direccion;
				//	}
				//	else if (item.Parametro.Equals("Tarea"))
				//	{
				//		ExpTarea = item.BlOrdenar;
				//		DirTarea = item.Direccion;
				//	}
				//	else if (item.Parametro.Equals("Departamento"))
				//	{
				//		ExpDepartamento = item.BlOrdenar;
				//		DirDepartamento = item.Direccion;
				//	}
				//	else if (item.Parametro.Equals("Fecha"))
				//	{
				//		ExpFecha = item.BlOrdenar;
				//		DirFecha = item.Direccion;
				//	}
				//	else if (item.Parametro.Equals("Hora"))
				//	{
				//		ExpHora = item.BlOrdenar;
				//		DirHora = item.Direccion;
				//	}
				//	else if (item.Parametro.Equals("Estado"))
				//	{
				//		ExpEstado = item.BlOrdenar;
				//		DirEstado = item.Direccion;
				//	}
				//}
				VM.ObtenerNotificaciones(SesionActual.uidSucursalActual.Value, deptos, null, estados, fechaInicio, fechaFin);
				VSNotificaciones = VM.Mensajes;
				dgvNotificaciones.DataSource = VSNotificaciones;
				dgvNotificaciones.DataBind();
			}
			catch (Exception)
			{
				pnlAlertBusqueda.Visible = true;
				lblErrorBusqueda.Text = "Error al realizar busqueda, verifique que los datos sean validos.";

				dgvNotificaciones.DataSource = null;
				dgvNotificaciones.DataBind();
			}
		}

		private void SortNotificaciones(string sortExpression, SortDirection sortDirection, bool same = false)
		{
			if (sortExpression == (string)ViewState["SortColumn"] && !same)
			{
				// We are resorting the same column, so flip the sort direction
				sortDirection =
					((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
					SortDirection.Descending : SortDirection.Ascending;
			}

			if (sortExpression.Equals("FolioC"))
			{
				if (sortDirection == SortDirection.Ascending)
				{
					VSNotificaciones = VSNotificaciones.OrderBy(x => x.FolioCumplimiento).ToList();
				}
				else
				{
					VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.FolioCumplimiento).ToList();
				}
			}
			else if (sortExpression.Equals("FolioT"))
			{
				if (sortDirection == SortDirection.Ascending)
				{
					VSNotificaciones = VSNotificaciones.OrderBy(x => x.FolioTarea).ToList();
				}
				else
				{
					VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.FolioTarea).ToList();
				}
			}
			else if (sortExpression == "Tarea")
			{
				if (sortDirection == SortDirection.Ascending)
				{
					VSNotificaciones = VSNotificaciones.OrderBy(x => x.StrTarea).ToList();
				}
				else
				{
					VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.StrTarea).ToList();
				}
			}
			else if (sortExpression == "Departamento")
			{
				if (sortDirection == SortDirection.Ascending)
				{
					VSNotificaciones = VSNotificaciones.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}
			else if (sortExpression == "Area")
			{
				if (sortDirection == SortDirection.Ascending)
				{
					VSNotificaciones = VSNotificaciones.OrderBy(x => x.StrArea).ToList();
				}
				else
				{
					VSNotificaciones = VSNotificaciones.OrderByDescending(x => x.StrArea).ToList();
				}
			}
			else if (sortExpression == "Estado")
			{
				if (sortDirection == SortDirection.Ascending)
				{
					VSNotificaciones = VSNotificaciones.OrderBy((x => x.StrEstadoNotificacion)).ToList();
				}
				else
				{
					VSNotificaciones = VSNotificaciones.OrderByDescending((x => x.StrEstadoNotificacion)).ToList();
				}
			}

			dgvNotificaciones.DataSource = VSNotificaciones;
			ViewState["SortColumn"] = sortExpression;
			ViewState["SortColumnDirection"] = sortDirection;
		}

		//protected void Ordenamiento_CheckedChanged(object sender, EventArgs e)
		//{
		//	try
		//	{
		//		GridViewRow row = (sender as CheckBox).Parent.Parent as GridViewRow;
		//		CheckBox cbRowCheckBox = ((CheckBox)sender) as CheckBox;

		//		Guid Uid = new Guid(gvOrdenamiento.DataKeys[row.RowIndex].Value.ToString());
		//		int Index = this.LsOrdenamiento.FindIndex(o => o.ID == Uid);
		//		if (Index >= 0)
		//		{
		//			LsOrdenamiento[Index].BlOrdenar = cbRowCheckBox.Checked;
		//		}
		//	}
		//	catch (Exception)
		//	{

		//		throw;
		//	}
		//}

		//protected void gvOrdenamiento_RowCommand(object sender, GridViewCommandEventArgs e)
		//{
		//	if (e.CommandName.Equals("CambiarOrden"))
		//	{
		//		Guid Uid = new Guid(e.CommandArgument.ToString());
		//		int Index = this.LsOrdenamiento.FindIndex(o => o.ID == Uid);
		//		if (Index >= 0)
		//		{
		//			if (this.LsOrdenamiento[Index].Direccion.Equals("ASC"))
		//				this.LsOrdenamiento[Index].Direccion = "DESC";
		//			else
		//				this.LsOrdenamiento[Index].Direccion = "ASC";

		//			gvOrdenamiento.DataSource = this.LsOrdenamiento;
		//			gvOrdenamiento.DataBind();
		//		}
		//	}
		//}
	}
}