﻿using CodorniX.Modelo;
using CodorniX.Template;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
    public partial class Departamentos : System.Web.UI.Page
    {
        VMDepartamentos VM = new VMDepartamentos();
        
        private List<Departamento> _Departamentos
        {
            set { ViewState["Departamentos"] = value; }
            get
            {
                if (ViewState["Departamentos"] == null)
                    ViewState["Departamentos"] = new List<Departamento>();

                return (List<Departamento>)ViewState["Departamentos"];
            }
        }

        private List<Departamento> _Areas
        {
            set { ViewState["Areas"] = value; }
            get
            {
                if (ViewState["Areas"] == null)
                    ViewState["Areas"] = new List<Departamento>();

                return (List<Departamento>)ViewState["Areas"];
            }
        }

        private List<Area> NuevasAreas
        {
            set { ViewState["Areas"] = value; }
            get
            {
                if (ViewState["Areas"] == null)
                    ViewState["Areas"] = new List<Area>();

                return (List<Area>)ViewState["Areas"];
            }
        }

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }

        private Guid? DepartamentoActual
        {
            set { ViewState["DeptoActual"] = value; }
            get
            {
                if (ViewState["DeptoActual"] == null)
                    return null;

                return (Guid?)ViewState["DeptoActual"];
            }
        }

        private bool EditMode
        {
            set { ViewState["EditMode"] = value; }
            get
            {
                if (ViewState["EditMode"] == null)
                    return false;

                return (bool)ViewState["EditMode"];
            }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;

            if (!Acceso.TieneAccesoAModulo("Departamentos", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (SesionActual.uidSucursalActual == null)
            {
                Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
                return;
            }

            if (!IsPostBack)
            {
                dgvDepartamentos.DataSource = null;
                dgvDepartamentos.DataBind();

                dgvAreas.DataSource = null;
                dgvAreas.DataBind();

                btnMostrar.Text = "Ocultar";

                VM.ObtenerStatus();
                ddStatus.DataSource = VM.Status;
                ddStatus.DataValueField = "UidStatus";
                ddStatus.DataTextField = "strStatus";
                ddStatus.DataBind();

                ddEstadoArea.DataSource = VM.Status;
                ddEstadoArea.DataValueField = "UidStatus";
                ddEstadoArea.DataTextField = "strStatus";
                ddEstadoArea.DataBind();

                fuImagen.Disable();
                txtNombre.Disable();
                txtDescripcion.Disable();
                ddStatus.Disable();
                panelDepartamentos.Visible = true;

                btnAreaNuevo.Disable();
                btnAreaEditar.Disable();
                txtAreaNombre.Disable();
                ddEstadoArea.Disable();
            }

            Page.Form.Attributes.Add("enctype", "multipart/form-data");
        }

        protected void btnMostrar_Click(object sender, EventArgs e)
        {
            if (btnMostrar.Text == "Ocultar")
            {
                btnMostrar.Text = "Mostrar";

                btnBorrar.Visible = false;
                btnBorrar.Disable();

                btnBuscar.Visible = false;
                btnBuscar.Disable();

                panelBusqueda.Visible = false;
                panelResultados.Visible = true;
            }
            else
            {
                btnMostrar.Text = "Ocultar";

                btnBorrar.Visible = true;
                btnBorrar.Enable();

                btnBuscar.Visible = true;
                btnBuscar.Enable();

                panelBusqueda.Visible = true;
                panelResultados.Visible = false;
            }
        }

        protected void btnBorrar_Click(object sender, EventArgs e)
        {
            busquedaNombre.Text = string.Empty;
            busquedaDescripcion.Text = string.Empty;
        }

        protected void btnBuscar_Click(object sender, EventArgs e)
        {
            panelBusqueda.Visible = false;

            btnMostrar.Text = "Mostrar";

            btnBorrar.Visible = false;
            btnBorrar.Disable();

            btnBuscar.Visible = false;
            btnBuscar.Disable();
            
            panelResultados.Visible = true;
            VM.BusquedaDepartamento(busquedaNombre.Text, busquedaDescripcion.Text, SesionActual.uidSucursalActual.Value);
            dgvDepartamentos.DataSource = VM.Departamentos;
            dgvDepartamentos.DataBind();
            ViewState["DeptoPreviousRow"] = null;
        }

        protected void btnDepartamentos_Click(object sender, EventArgs e)
        {
            title.Text = "Departamentos";
            panelAreas.Visible = false;
            panelDepartamentos.Visible = true;
            tabDepartamentos.AddCssClass("active");
            tabAreas.RemoveCssClass("active");
        }

        protected void btnAreas_Click(object sender, EventArgs e)
        {
            title.Text = "Áreas";
            
            panelAreas.Visible = true;
            panelDepartamentos.Visible = false;
            tabDepartamentos.RemoveCssClass("active");
            tabAreas.AddCssClass("active");

        }

        protected void dgvDepartamentos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvDepartamentos, "Select$" + e.Row.RowIndex);
                
                e.Row.Cells[1].ToolTip = Server.HtmlDecode(e.Row.Cells[2].Text);
            }
        }

        protected void dgvDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(dgvDepartamentos.SelectedDataKey.Value.ToString());
            VM.ObtenerDepartamento(uid);

            DepartamentoActual = uid;
            btnAreas.Enable();
            tabAreas.RemoveCssClass("disabled");

            this.uid.Text = VM.Departamento.UidDepartamento.ToString();
            txtNombre.Text = VM.Departamento.StrNombre;
            txtDescripcion.Text = VM.Departamento.StrDescripcion;
            image.ImageUrl = Page.ResolveUrl("~/Assets/" + VM.Departamento.StrURL);

            fuImagen.Disable();
            fuImagenButton.AddCssClass("disabled");
            txtNombre.Disable();
            txtDescripcion.Disable();
            ddStatus.Disable();
            btnNuevoDatos.Enable();
            btnEditarDatos.Enable();

            dgvAreas.DataSource = null;
            dgvAreas.DataBind();

            int pos = -1;
            if (ViewState["DeptoPreviousRow"] != null)
            {
                pos = (int)ViewState["DeptoPreviousRow"];
                GridViewRow previousRow = dgvDepartamentos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["DeptoPreviousRow"] = dgvDepartamentos.SelectedIndex;
            dgvDepartamentos.SelectedRow.AddCssClass("success");
            ViewState["AreasPreviousRow"] = null;

            VM.ObtenerAreas(uid);
            NuevasAreas = VM.Areas;
            dgvAreas.DataSource = NuevasAreas;
            dgvAreas.DataBind();

            EditMode = false;

            btnAreaNuevo.Disable();
            btnAreaEditar.Disable();
            txtAreaNombre.Disable();
            txtAreaNombre.Text = string.Empty;
            ddEstadoArea.Disable();
            ddEstadoArea.SelectedIndex = 0;
        }

        private void SortDepartamentos(string SortExpression, SortDirection SortDirection, bool same = false)
        {

            if (SortExpression == (string)ViewState["SortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                SortDirection =
                    ((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }

            if (SortExpression == "Nombre")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    _Departamentos = _Departamentos.OrderBy(x => x.StrNombre).ToList();
                }
                else
                {
                    _Departamentos = _Departamentos.OrderByDescending(x => x.StrNombre).ToList();
                }
            }
            else if (SortExpression == "Descripcion")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    _Departamentos = _Departamentos.OrderBy(x => x.StrDescripcion).ToList();
                }
                else
                {
                    _Departamentos = _Departamentos.OrderByDescending(x => x.StrDescripcion).ToList();
                }
            }

            dgvDepartamentos.DataSource = _Departamentos;
            ViewState["SortColumn"] = SortExpression;
            ViewState["SortColumnDirection"] = SortDirection;
        }

        protected void dgvDepartamentos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["DeptoPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["SortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortDepartamentos(SortExpression, SortDirection, true);
            }
            else
            {
                dgvDepartamentos.DataSource = _Departamentos;
            }
            dgvDepartamentos.PageIndex = e.NewPageIndex;
            dgvDepartamentos.DataBind();
        }

        protected void dgvDepartamentos_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["DeptoPreviousRow"] = null;
            SortDepartamentos(e.SortExpression, e.SortDirection, false);
            dgvDepartamentos.DataBind();
        }

        protected void dgvAreas_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvAreas, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvAreas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(dgvAreas.SelectedDataKey.Value.ToString());
            VM.ObtenerArea(uid);

            fldUidArea.Value = VM.Area.UidArea.ToString();
            txtAreaNombre.Text = VM.Area.StrNombre;
            ddEstadoArea.SelectedValue = VM.Area.UidStatus.ToString();

            if (EditMode)
            {
                txtAreaNombre.Disable();
                btnAreaNuevo.Enable();
                btnAreaEditar.Enable();
            }
            else
            {
                txtAreaNombre.Disable();
                btnAreaNuevo.Disable();
                btnAreaEditar.Disable();
            }

            int pos = -1;
            if (ViewState["AreasPreviousRow"] != null)
            {
                pos = (int)ViewState["AreasPreviousRow"];
                GridViewRow previousRow = dgvAreas.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["AreasPreviousRow"] = dgvAreas.SelectedIndex;
            dgvAreas.SelectedRow.AddCssClass("success");
        }

        private void SortAreas(string SortExpression, SortDirection SortDirection, bool same = false)
        {

            if (SortExpression == (string)ViewState["SortColumn"] && !same)
            {
                // We are resorting the same column, so flip the sort direction
                SortDirection =
                    ((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
                    SortDirection.Descending : SortDirection.Ascending;
            }

            if (SortExpression == "Nombre")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    _Areas = _Areas.OrderBy(x => x.StrNombre).ToList();
                }
                else
                {
                    _Areas = _Areas.OrderByDescending(x => x.StrNombre).ToList();
                }
            }
            else if (SortExpression == "Descripcion")
            {
                if (SortDirection == SortDirection.Ascending)
                {
                    _Areas = _Areas.OrderBy(x => x.StrDescripcion).ToList();
                }
                else
                {
                    _Areas = _Areas.OrderByDescending(x => x.StrDescripcion).ToList();
                }
            }

            dgvAreas.DataSource = _Areas;
            ViewState["SortColumn"] = SortExpression;
            ViewState["SortColumnDirection"] = SortDirection;
        }

        protected void dgvAreas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            ViewState["AreasPreviousRow"] = null;
            if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
            {
                string SortExpression = (string)ViewState["SortColumn"];
                SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
                SortDepartamentos(SortExpression, SortDirection, true);
            }
            else
            {
                dgvDepartamentos.DataSource = _Departamentos;
            }
            dgvDepartamentos.PageIndex = e.NewPageIndex;
            dgvDepartamentos.DataBind();
        }

        protected void dgvAreas_Sorting(object sender, GridViewSortEventArgs e)
        {
            ViewState["AreasPreviousRow"] = null;
            SortDepartamentos(e.SortExpression, e.SortDirection, false);
            dgvDepartamentos.DataBind();
        }

        protected void btnNuevoDatos_Click(object sender, EventArgs e)
        {
            EditMode = true;

            uid.Text = string.Empty;
            txtNombre.Text = string.Empty;
            txtDescripcion.Text = string.Empty;
            image.ImageUrl = null;

            btnNuevoDatos.Disable();
            btnEditarDatos.Disable();

            fuImagen.Enable();
            fuImagenButton.RemoveCssClass("disabled");
            txtNombre.Enable();
            txtDescripcion.Enable();
            ddStatus.Enable();

            btnOKDatos.Visible = true;
            btnOKDatos.Enable();

            btnCancelarDatos.Visible = true;
            btnCancelarDatos.Enable();

            btnAreaNuevo.Enable();
            btnAreaEditar.Disable();
            btnAreaOK.Enable();

			this.NuevasAreas = new List<Area>();

            // Cambiar automaticamente a Departamentos en elemnto nuevo.
            btnDepartamentos_Click(sender, e);
        }

        protected void btnEditarDatos_Click(object sender, EventArgs e)
        {
            EditMode = true;

            btnNuevoDatos.Disable();
            btnEditarDatos.Disable();

            fuImagen.Enable();
            fuImagenButton.RemoveCssClass("disabled");
            txtNombre.Enable();
            txtDescripcion.Enable();
            ddStatus.Enable();

            btnOKDatos.Visible = true;
            btnOKDatos.Enable();

            btnCancelarDatos.Visible = true;
            btnCancelarDatos.Enable();
            
            btnAreaNuevo.Enable();
            if (fldUidArea.Value != string.Empty)
                btnAreaEditar.Enable();
        }

        protected void btnOKDatos_Click(object sender, EventArgs e)
        {
            string filename = string.Empty;
            string oldFile = string.Empty;

            frmGrpNombre.RemoveCssClass("has-error");
            lblError.Text = string.Empty;

            if (string.IsNullOrWhiteSpace(txtNombre.Text))
            {
                lblError.Text = "El nombre es obligatorio";
                txtNombre.Focus();
                frmGrpNombre.AddCssClass("has-error");
                return;
            }

            if (fuImagen.HasFile)
            {
                filename = fuImagen.FileName;
            }
            
            Departamento departamento = null;
            if (uid.Text.Length == 0)
            {
                departamento = new Departamento()
                {
                    StrNombre = txtNombre.Text,
                    StrDescripcion = txtDescripcion.Text,
                    StrURL = filename,
                    UidStatus = new Guid(ddStatus.SelectedValue),
                    UidSucursal = SesionActual.uidSucursalActual.Value,
                };
                VM.GuardarDepartamento(departamento);
                filename = departamento.UidDepartamento.ToString() + '_' + departamento.StrURL;

                foreach (Area area in NuevasAreas)
                {
                    area.UidDepartamento = departamento.UidDepartamento;
                    VM.GuardarArea(area);
                }
            }
            else
            {
                VM.ObtenerDepartamento(new Guid(uid.Text));
                departamento = VM.Departamento;
                departamento.StrNombre = txtNombre.Text;
                departamento.StrDescripcion = txtDescripcion.Text;
                if (!string.IsNullOrEmpty(filename))
                {
                    oldFile = departamento.StrURL;
                    filename = departamento.UidDepartamento.ToString() + '_' + filename;
                    departamento.StrURL = filename;
                }
                departamento.UidStatus = new Guid(ddStatus.SelectedValue);
                departamento.UidSucursal = SesionActual.uidSucursalActual.Value;
                VM.GuardarDepartamento(departamento);

                foreach (Area area in NuevasAreas)
                {
                    area.UidDepartamento = departamento.UidDepartamento;
                    VM.GuardarArea(area);
                }
            }

            int pos = -1;
            if (ViewState["DeptoPreviousRow"] != null)
            {
                pos = (int)ViewState["DeptoPreviousRow"];
                GridViewRow previousRow = dgvDepartamentos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
            
            if (fuImagen.HasFile)
            {
                if (oldFile.Length > 0)
                    if (File.Exists("~/Assets/" + Server.MapPath(oldFile)))
                        File.Delete("~/Assets/" + Server.MapPath(oldFile));

                fuImagen.SaveAs(Server.MapPath("~/Assets/" + filename));
            }

            fuImagen.Disable();
            fuImagenButton.AddCssClass("disabled");
            txtNombre.Text = null;
            txtNombre.Disable();
            txtDescripcion.Text = null;
            txtDescripcion.Disable();
            image.ImageUrl = null;
            ddStatus.Disable();
            btnOKDatos.Visible = false;
            btnOKDatos.Disable();
            btnCancelarDatos.Visible = false;
            btnCancelarDatos.Disable();
            btnNuevoDatos.Enable();

            EditMode = false;
            btnBuscar_Click(sender, e);

            btnAreaEditar.Disable();
            btnAreaNuevo.Disable();
            btnAreaOK.Disable().Visible = false;
            btnAreaCancelar.Disable().Visible = false;
        }

        protected void btnCancelarDatos_Click(object sender, EventArgs e)
        {
            frmGrpNombre.RemoveCssClass("has-error");
            lblError.Text = string.Empty;
            fuImagen.Disable();
            fuImagenButton.AddCssClass("disabled");
            txtNombre.Disable();
            txtDescripcion.Disable();
            ddStatus.Disable();
            btnOKDatos.Visible = false;
            btnOKDatos.Disable();
            btnCancelarDatos.Visible = false;
            btnCancelarDatos.Disable();
            btnNuevoDatos.Enable();
            btnEditarDatos.Enable();

            EditMode = false;

            btnAreaEditar.Disable();
            btnAreaNuevo.Disable();
            btnAreaOK.Disable().Visible = false;
            btnAreaCancelar.Disable().Visible = false;
            txtAreaNombre.Text = string.Empty;
        }

        protected void btnAreaNuevo_Click(object sender, EventArgs e)
        {
            txtAreaNombre.Enable();
            ddEstadoArea.Enable();
            btnAreaNuevo.Disable();
            btnAreaEditar.Disable();
            btnAreaOK.Visible = true;
            btnAreaOK.Enable();
            btnAreaCancelar.Visible = true;
            btnAreaCancelar.Enable();
            txtAreaNombre.Text = string.Empty;
            fldUidArea.Value = string.Empty;
        }

        protected void btnAreaEditar_Click(object sender, EventArgs e)
        {
            txtAreaNombre.Enable();
            ddEstadoArea.Enable();
            btnAreaNuevo.Disable();
            btnAreaEditar.Disable();
            btnAreaOK.Visible = true;
            btnAreaOK.Enable();
            btnAreaCancelar.Visible = true;
            btnAreaCancelar.Enable();
        }

        protected void btnAreaOK_Click(object sender, EventArgs e)
        {
            Area area = null;
            if (fldUidArea.Value.Length == 0)
            {
                area = new Area()
                {
                    StrNombre = txtAreaNombre.Text,
                    StrDescripcion = "",
                    StrURL = "",
                    UidStatus = new Guid(ddEstadoArea.SelectedValue)
                    //UidDepartamento = DepartamentoActual.Value,
                };

                NuevasAreas.Add(area);
            }
            else
            {
                VM.ObtenerArea(new Guid(fldUidArea.Value));
                area = VM.Area;
                area.StrNombre = txtAreaNombre.Text;
                area.StrDescripcion = "";
                area.UidDepartamento = DepartamentoActual.Value;
                area.UidStatus = new Guid(ddEstadoArea.SelectedValue);
                NuevasAreas.Remove(NuevasAreas.Where(x => x.UidArea == area.UidArea).FirstOrDefault());
                NuevasAreas.Add(area);
            }


            int pos = -1;
            if (ViewState["AreasPreviousRow"] != null)
            {
                pos = (int)ViewState["AreasPreviousRow"];
                GridViewRow previousRow = dgvAreas.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            fldUidArea.Value = string.Empty;
            btnAreaCancelar.Visible = false;
            btnAreaCancelar.Disable();
            btnAreaOK.Visible = false;
            btnAreaOK.Disable();
            btnAreaNuevo.Enable();
            btnAreaEditar.Disable();

            dgvAreas.DataSource = NuevasAreas;
            dgvAreas.DataBind();

            txtAreaNombre.Disable();
            txtAreaNombre.Text = string.Empty;
            ddEstadoArea.Disable();
            ddEstadoArea.SelectedIndex = 0;
        }

        protected void btnAreaCancelar_Click(object sender, EventArgs e)
        {
            btnAreaCancelar.Visible = false;
            btnAreaOK.Visible = false;
            btnAreaNuevo.Enable();
            btnAreaEditar.Enable();
            txtAreaNombre.Disable();
            ddEstadoArea.Disable();
        }
    }
}