﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="AsignacionDeSupervision.aspx.cs" Inherits="CodorniX.Vista.AsignacionDeSupervision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Asiganciones</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-0">
						<div class="text-right">
							<div class="btn-group">
								<asp:LinkButton ID="btnMostrar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnMostrar_Click">
									Mostrar
								</asp:LinkButton>
								<asp:LinkButton ID="btnBorrar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnBorrar_Click">
									<span class="glyphicon glyphicon-trash"></span>
									Borrar
								</asp:LinkButton>
								<asp:LinkButton ID="btnBuscar" CssClass="btn btn-default btn-sm" runat="server" OnClick="btnBuscar_Click">
									<span class="glyphicon glyphicon-search"></span>
									Buscar
								</asp:LinkButton>
							</div>
						</div>
					</div>
					<asp:PlaceHolder ID="panelBusqueda" runat="server">

						<asp:HiddenField ID="busquedaUidUsuario" runat="server" />
						<div class="col-xs-12 pd-left-right-5">
							<h6>Nombre</h6>
							<asp:TextBox ID="busquedaNombre" runat="server" CssClass="form-control" />
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h6>Fecha Inicio</h6>
							<div class="input-group date n-n">
								<asp:TextBox ID="busquedaFechaInicio" placeholder="Fecha Inicio" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h6>Fecha de finalización</h6>
							<div class="input-group date n-n">
								<asp:TextBox ID="busquedaFechaFin" placeholder="Fecha Fin" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-xs-6 pd-left-right-5">
							<h6>Turno</h6>
							<asp:ListBox ID="lbTurno" runat="server" CssClass="form-control" SelectionMode="Multiple" />
						</div>
						<div class="col-xs-6 pd-left-right-5">
							<h6>Departamentos</h6>
							<asp:ListBox ID="lbDepartamentos" runat="server" CssClass="form-control" SelectionMode="Multiple" />
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="panelResultados" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<asp:GridView ID="dgvPeriodos" runat="server" PageSize="6" AllowSorting="true" AllowPaging="true" CssClass="table table-bordered table-responsive table-condensed input-sm" OnPageIndexChanging="dgvPeriodos_PageIndexChanging" OnSorting="dgvPeriodos_Sorting" OnSelectedIndexChanged="dgvPeriodos_SelectedIndexChanged" OnRowDataBound="dgvPeriodos_RowDataBound" AutoGenerateColumns="false" DataKeyNames="UidAsignacion">
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
									<EmptyDataTemplate>
										No existen asignaciones que coincidan con la búsqueda.
									</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="StrNombreUsuario" HeaderText="Nombre del Encargado" SortExpression="NombreUsuario" />
										<asp:BoundField DataField="StrNombreDepto" HeaderText="Departamento" SortExpression="NombreDepto" />
										<asp:BoundField DataField="StrTurno" HeaderText="Turno" SortExpression="Turno" />
										<asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false" HeaderText="Fecha de inicio" SortExpression="FechaInicio" />
										<asp:BoundField DataField="DtFechaFin" DataFormatString="{0:dd/MM/yyyy}" HtmlEncode="false" HeaderText="Fecha de finalización" SortExpression="FechaFin" />
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Asiganaciones</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-0">
						<div class="text-left">
							<div class="btn-group">
								<asp:LinkButton ID="btnNuevo" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnNuevo_Click">
									<span class="glyphicon glyphicon-file"></span>
									Nuevo
								</asp:LinkButton>
								<asp:LinkButton ID="btnEditar" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnEditar_Click">
									<span class="glyphicon glyphicon-edit"></span>
									Editar
								</asp:LinkButton>
								<asp:LinkButton ID="btnOKDatos" runat="server" CssClass="btn btn-success btn-sm disabled" OnClick="btnOKDatos_Click">
									<span class="glyphicon glyphicon-ok"></span>
								</asp:LinkButton>
								<asp:LinkButton ID="btnCancelarDatos" runat="server" CssClass="btn btn-danger btn-sm disabled" OnClick="btnCancelarDatos_Click">
									<span class="glyphicon glyphicon-remove"></span>
								</asp:LinkButton>
							</div>
						</div>
					</div>
					<asp:HiddenField ID="uidPeriodo" runat="server" />
					<div class="row">
						<div class="col-xs-12">
							<asp:Label ID="lbMensaje" runat="server" />
						</div>
					</div>
					<asp:PlaceHolder ID="userFields" runat="server">
						<div class="col-xs-12 text-right pd-left-right-5">
							<asp:LinkButton ID="btnBuscarUsuario" runat="server" CssClass="btn btn-default btn-sm" OnClick="btnBuscarUsuario_Click">
                                    Buscar Encargado
							</asp:LinkButton>
						</div>

						<asp:HiddenField ID="uidUsuario" runat="server" />
						<div class="col-xs-12 pd-left-right-5">
							<asp:Panel ID="frmGrpEncargado" runat="server" CssClass="form-group">
								<h6>Nombre del Encargado</h6>
								<asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
							</asp:Panel>
						</div>
						<div class="col-xs-6 pd-left-right-5">
							<h6>Departamento</h6>
							<asp:DropDownList ID="ddDepartamentos" runat="server" AutoPostBack="true" CssClass="form-control" OnSelectedIndexChanged="ddDepartamentos_SelectedIndexChanged" />
						</div>
						<div class="col-xs-6 pd-left-right-5">
							<h6>Turno</h6>
							<asp:DropDownList ID="ddTurno" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddTurno_SelectedIndexChanged" />
						</div>
						<div class="col-md-6 pd-left-right-5">
							<asp:Panel ID="frmGrpFechaInicio" runat="server" CssClass="form-group">
								<h6>Fecha Inicio</h6>
								<div class="input-group date start-date">
									<asp:TextBox ID="txtFechaInicio" placeholder="Fecha Inicio" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h6>Feche de finalización</h6>
							<div class="input-group date end-date">
								<asp:TextBox ID="txtFechaFin" placeholder="Fecha Fin" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="userGrid" runat="server">
						<asp:GridView ID="dgvUsuario" runat="server" AllowSorting="true" AllowPaging="true" CssClass="table table-bordered table-responsive table-condensed input-sm" OnSorting="dgvUsuario_Sorting" OnPageIndexChanging="dgvUsuario_PageIndexChanging" OnSelectedIndexChanged="dgvUsuario_SelectedIndexChanged" OnRowDataBound="dgvUsuario_RowDataBound" DataKeyNames="UidUsuario" AutoGenerateColumns="false">
							<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
							<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
							<EmptyDataTemplate>
								No existen encargados que coincidan con la búsqueda.
							</EmptyDataTemplate>
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
								<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
								<asp:BoundField DataField="StrApellidoPaterno" HeaderText="Apellido Paterno" SortExpression="ApellidoPaterno" />
								<asp:BoundField DataField="StrApellidoMaterno" HeaderText="Apellido Materno" SortExpression="ApellidoMaterno" />
							</Columns>
						</asp:GridView>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
	</div>
	<script>
		//<![CDATA[
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date.start-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
			if (!endDateReady) {
				$(".input-group.date.end-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				});
			}
		}

		function setStartDateLimit(date) {
			$(".input-group.date.start-date").datepicker('remove');
			$(".input-group.date.start-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: date,
			}).on('changeDate', function (e) {
				setEndDateLimit(e.format());
			});
			setEndDateLimit(date);
			$(".input-group.date.start-date").datepicker('update', date);
			startDateReady = true;
		}

		function setEndDateLimit(date) {
			$(".input-group.date.end-date").datepicker('remove');
			$(".input-group.date.end-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: date,
			});
			$(".input-group.date.end-date").datepicker('update', date);
			endDateReady = true;
		}

		function pageLoad() {
			enableDatapicker();
			$(".input-group.date.n-n").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es'
			})
		}
        //]]>
	</script>
</asp:Content>
