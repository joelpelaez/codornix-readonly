﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="Site1.Master" CodeBehind="Tareas1.aspx.cs" Inherits="CodorniX.Vista.Tareas1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<!--Panel Izquierdo -->
		<div class="col-md-6 col-xs-12">
			<asp:PlaceHolder ID="PanelBusquedas" runat="server">
				<div class="panel panel-primary">
					<div class="panel-heading text-center">
						Buscar Tareas
					</div>
					<div class="panel-body panel-pd">
						<div class="col-md-12 pd-0">
							<div class="row">
								<div class="col-xs-12 text-right">
									<asp:Label ID="lblMensajeIzquierdo" runat="server" />
									<asp:LinkButton ID="btnMostrar" OnClick="btnMostrar_Click" CssClass="btn btn-sm btn-default" runat="server">
										<asp:Label ID="lblFiltros" class="glyphicon glyphicon-collapse-up" runat="server" />
									</asp:LinkButton><asp:LinkButton ID="btnLimpiar" OnClick="btnLimpiar_Click" CssClass="btn btn-sm btn-default" runat="server">
										<span class="glyphicon glyphicon-trash"></span>
										Limpiar
									</asp:LinkButton><asp:LinkButton ID="btnBuscar" OnClick="btnBuscar_Click" CssClass="btn btn-sm btn-default" runat="server">
										<span class="glyphicon glyphicon-search"></span>
										Buscar
									</asp:LinkButton>
								</div>
							</div>
						</div>
						<asp:Panel ID="PanelFiltros" runat="server">
							<div class="col-md-4 pd-left-right-5">
								<h6>Folio</h6>
								<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFolio" />
							</div>
							<div class="col-md-8 pd-left-right-5">
								<h6>Nombre</h6>
								<asp:TextBox CssClass="form-control" ID="FiltroNombre" runat="server" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Folio de Antecesor</h6>
								<asp:TextBox ID="txtFolioAntecesor" runat="server" CssClass="form-control" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Fecha Inicio</h6>
								<div class="input-group date extra">
									<asp:TextBox ID="filtrofechainicio1" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Fecha Fin</h6>
								<div class="input-group date extra">
									<asp:TextBox ID="filtrofechainicio2" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Departamento</h6>
								<asp:ListBox ID="lblDepartamento" runat="server" SelectionMode="Multiple" CssClass="form-control" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Area</h6>
								<asp:ListBox ID="lbFiltroArea" runat="server" SelectionMode="Multiple" CssClass="form-control" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Unidad Medida</h6>
								<asp:ListBox ID="lblUnidad" runat="server" SelectionMode="Multiple" CssClass="form-control" />
							</div>
						</asp:Panel>
						<div style="padding-top: 15px">
							<asp:GridView ID="DVGTareas" AllowPaging="true" PageSize="13" OnPreRender="DVGTareas_PreRender" OnPageIndexChanging="DVGTareas_PageIndexChanging" OnSelectedIndexChanged="DVGTareas_SelectedIndexChanged" OnSorting="DVGTareas_Sorting" OnRowDataBound="DVGTareas_RowDataBound" AutoGenerateColumns="false" DataKeyNames="UidTarea" CssClass="table table-bordered table-condensed table-striped input-sm" AllowSorting="true" runat="server">
								<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
								<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								<EmptyDataTemplate>
									No hay Datos
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
									<asp:BoundField DataField="IntFolio" HeaderText="Folio" DataFormatString="{0:D4}" SortExpression="Folio" ItemStyle-VerticalAlign="Middle" />
									<asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrNombre") %>' runat="server" ID="lblTaskName" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodicidad" SortExpression="Periodicidad" />
									<asp:TemplateField HeaderText="Departamento" SortExpression="Departamento">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrDepartamento") %>' runat="server" ID="lblTaskDepto" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Area" SortExpression="Area">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrArea") %>' runat="server" ID="lblTaskArea" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Inicio" SortExpression="FechaInicio" />
									<asp:BoundField DataField="BlAutorizado" HeaderText="Autorizado" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
								</Columns>
							</asp:GridView>
						</div>
					</div>
				</div>
			</asp:PlaceHolder>
		</div>

		<!--Panel Derecho -->
		<div class="col-md-6 col-xs-12">
			<div class="panel panel-primary">
				<div class="panel-heading">
					Gestion De Tareas
				</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-0">
						<table style="width: 100%;">
							<tr>
								<td>
									<div class="btn btn-group pd-0">
										<asp:LinkButton ID="btnNuevo" ToolTip="Crear nueva tarea." OnClick="btnNuevo_Click" CssClass="btn btn-sm btn-default" runat="server">
											<span class="glyphicon glyphicon-file"></span>
											Nuevo
										</asp:LinkButton>
										<asp:LinkButton ID="btnEditar" ToolTip="Editar datos de la tarea seleccionada." OnClick="btnEditar_Click" CssClass="btn btn-sm disabled btn-default" runat="server">
											<span class="glyphicon glyphicon-cog"></span>
											Editar
										</asp:LinkButton>
										<asp:LinkButton ID="btnAceptar" OnClick="btnAceptar_Click" CssClass="btn btn-sm btn-success" runat="server">
											<asp:Label ID="lblAccion" CssClass="glyphicon glyphicon-ok" runat="server" />
										</asp:LinkButton>
										<asp:LinkButton ID="btnCancelar" OnClick="btnCancelar_Click" CssClass="btn btn-sm btn-danger" runat="server">
											<span class="glyphicon glyphicon-remove"></span>
										</asp:LinkButton>
									</div>
								</td>
								<td class="text-right">
									<asp:LinkButton ID="btnConfirmarTarea" ToolTip="Confirmar tarea creada por el supervisor" OnClick="btnConfirmarTarea_Click" runat="server" CssClass="btn btn-sm btn-success disabled">
											<span class="glyphicon glyphicon-asterisk"></span>
											Confirmar
									</asp:LinkButton>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-12 pd-left-right-5">
						<asp:Label ID="lblMensaje" runat="server" />
						<asp:Label Style="color: red;" ID="lblErrorDatosGenerales" Text="" runat="server" />
						<asp:TextBox CssClass="hide" ID="txtUidTarea" runat="server" />
						<asp:TextBox ID="txtradio" runat="server" CssClass="hide" />
						<asp:TextBox ID="txtUidTipoFrecuencia" CssClass="hide" runat="server" />
						<asp:TextBox ID="txtUidPeriodicidad" CssClass="hide" runat="server" />
						<asp:TextBox ID="txtIntDiasMes" CssClass="hide" runat="server" />
						<asp:TextBox ID="txtintDiasSemanas" CssClass="hide" runat="server" />
						<asp:TextBox ID="txtintNumero" CssClass="hide" runat="server" />
					</div>
					<ul class="nav nav-tabs">
						<li class="active" id="activeDatosGenerales" runat="server">
							<asp:LinkButton ID="tabDatosGenerales" runat="server" Text="Datos Generales" OnClick="tabDatosGenerales_Click" /></li>
						<li id="activeOpciones" runat="server">
							<asp:LinkButton ID="tabOpciones" runat="server" Text="Seleccion" OnClick="tabOpciones_Click" /></li>
						<li id="activePeriodos" runat="server">
							<asp:LinkButton ID="tabPeriodos" runat="server" Text="Periodos" OnClick="tabPeriodos_Click" /></li>
						<li id="activeDepartamentos" runat="server">
							<asp:LinkButton ID="tabDepartamentos" runat="server" Text="Asignacion" OnClick="tabDepartamentos_Click" /></li>
						<li id="activeAntecesor" runat="server">
							<asp:LinkButton ID="tabAntecesor" runat="server" Text="Antecesor" OnClick="tabAntecesor_Click" /></li>
						<li id="activeNotificacion" runat="server">
							<asp:LinkButton ID="tabNotificacion" runat="server" Text="Notificación" OnClick="tabNotificacion_Click" />
						</li>
					</ul>
					<asp:PlaceHolder ID="PanelDatosGenerales" runat="server">
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpNombre" runat="server" CssClass="form-group">
								<h6>Nombre</h6>
								<asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpFechaInicio" runat="server" CssClass="form-group">
								<h6>Fecha Inicio</h6>
								<div class="input-group date extra">
									<asp:TextBox ID="txtFechainicio" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpFechaFin" runat="server" CssClass="form-group">
								<h6>Fecha Fin</h6>
								<div class="input-group date extra">
									<asp:TextBox ID="txtFechaFin" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpDescripcion" runat="server" CssClass="form-group">
								<h6>Descripcion</h6>
								<asp:TextBox ID="txtDescripcion" runat="server" TextMode="MultiLine" Rows="9" CssClass="form-control" />
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpActivo" runat="server" CssClass="form-group">
								<h6>Estatus</h6>
								<asp:DropDownList ID="DdStatus" CssClass="form-control" runat="server">
								</asp:DropDownList>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="Panel1" runat="server" CssClass="form-group">
								<h6>Tipo Tarea</h6>
								<asp:DropDownList ID="DdTipoTarea" CssClass="form-control" runat="server">
								</asp:DropDownList>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5" style="margin-top: 25px">
							<asp:Panel ID="Panel2" runat="server" CssClass="form-group">
								<label class="checkbox-inline">
									<asp:CheckBox AutoPostBack="true" ID="CbFoto" runat="server" Checked="false" Visible="false" /></label>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpMedicion" runat="server" CssClass="form-group">
								<h6>Tipo de medicion</h6>
								<asp:DropDownList ID="DdMedicion" AutoPostBack="true" OnSelectedIndexChanged="DdMedicion_SelectedIndexChanged" CssClass="form-control" runat="server">
								</asp:DropDownList>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5" style="margin-top: 25px">
							<asp:Panel ID="frmcbhora" runat="server" CssClass="form-group">
								<label class="checkbox-inline">
									<asp:CheckBox AutoPostBack="true" OnCheckedChanged="CbHora_CheckedChanged" ID="CbHora" runat="server" />Habilitar Hora</label>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpUnidad" runat="server" CssClass="form-group">
								<h6 id="nombreunidad" runat="server">Unidad Medida</h6>
								<asp:DropDownList ID="DdUnidadMedida" CssClass="form-control" runat="server">
								</asp:DropDownList>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmHora" runat="server" CssClass="form-group">
								<h6 id="lblHora" runat="server">Hora</h6>
								<div class="input-group clockpicker" data-placement="top" data-align="left" data-autoclose="true">
									<asp:TextBox ID="txtHora" runat="server" CssClass="form-control" />
									<span id="clock" runat="server" class="input-group-addon input-sm">
										<i class="glyphicon glyphicon-time"></i>
									</span>
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmTolerancia" runat="server" CssClass="form-group">
								<h6 id="lblTolerancia" runat="server">Tolerancia</h6>
								<div class="input-group">
									<asp:TextBox ID="txtTolerancia" runat="server" CssClass="form-control" />
									<span id="min" runat="server" class="input-group-addon input-sm"><i>min</i> </span>
								</div>
							</asp:Panel>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelSeleccion" Visible="false" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<div class="btn-group">
									<asp:LinkButton ID="btnAgregarOpcion" runat="server" Enabled="false" CssClass="btn btn-default btn-sm disabled" OnClick="btnAgregarOpcion_Click">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        Nuevo
									</asp:LinkButton>
									<asp:LinkButton ID="btnEditarOpcion" runat="server" CssClass="btn btn-default btn-sm disabled" OnClick="btnEditarOpcion_Click">
                                        <span class="glyphicon glyphicon-edit"></span>
                                        Editar
									</asp:LinkButton>
									<asp:LinkButton ID="btnEliminarOpcion" runat="server" CssClass="btn btn-default btn-sm disabled" OnClick="btnEliminarOpcion_Click">
                                        <span class="glyphicon glyphicon-trash"></span>
                                        Eliminar
									</asp:LinkButton>
									<asp:LinkButton ID="btnAceptarOpcion" runat="server" CssClass="btn btn-sm btn-success" OnClick="btnAceptarOpcion_Click">
                                        <span class="glyphicon glyphicon-ok"></span>
									</asp:LinkButton>
									<asp:LinkButton ID="btnCancelarOpcion" runat="server" CssClass="btn btn-sm btn-danger" OnClick="btnCancelarOpcion_Click">
                                        <span class="glyphicon glyphicon-remove"></span>
									</asp:LinkButton>
								</div>
								<div style="padding-top: 10px;">
									<asp:Label ID="lblAceptarEliminarOpcion" runat="server" />
									<asp:LinkButton ID="btnAceptarEliminarOpcion" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnAceptarEliminarOpcion_Click">
                                        <span class="glyphicon glyphicon-ok"></span>
									</asp:LinkButton>
									<asp:LinkButton ID="btnCancelarEliminarOpcion" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnCancelarEliminarOpcion_Click">
                                        <span class="glyphicon glyphicon-remove"></span>
									</asp:LinkButton>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<asp:Label Style="color: red;" ID="lblErrorOpcion" Text="" runat="server" />
							</div>
						</div>
						<asp:TextBox ID="txtUidOpcion" runat="server" CssClass="hidden disabled" />

						<asp:Panel ID="frmGrpOpcion" runat="server" CssClass="form-group">
							<div class="row" style="padding-top: 10px;">
								<div class="col-xs-8">
									<h6>Opción</h6>
									<asp:TextBox ID="txtOpcion" runat="server" Enabled="false" CssClass="form-control disabled" placeholder="Opcion" />
								</div>
								<div class="col-xs-2">
									<h6>Orden</h6>
									<asp:TextBox ID="txtOrden" runat="server" Enabled="false" CssClass="form-control disabled" placeholder="Orden" />
								</div>
								<div class="col-xs-2">
									<h6>Activo</h6>
									<asp:CheckBox ID="cbVisible" runat="server" />
								</div>
							</div>
						</asp:Panel>
						<div class="row" style="padding-top: 10px;">
							<div class="col-xs-12">
								<asp:GridView ID="dgvOpciones" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidOpciones" OnRowDataBound="dgvOpciones_RowDataBound" OnSelectedIndexChanged="dgvOpciones_SelectedIndexChanged">
									<EmptyDataTemplate>No existen Opciones</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="StrOpciones" HeaderText="Tipo" />
										<asp:BoundField DataField="IntOrden" HeaderText="Orden" />
										<asp:BoundField DataField="BlVisible" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" FooterStyle-CssClass="hidden" />
										<asp:TemplateField HeaderText="Activo">
											<ItemTemplate>
												<asp:Label runat="server" ID="lblActivo" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelPeriodos" runat="server">
						<div class="row" style="color: red; padding-top: 10px;">
							<div class="col-xs-12">
								<asp:Label ID="lblerrorPeriodicidad" Text="" runat="server" />
							</div>
						</div>
						<div class="col-md-2" style="margin-top: 15px">
							<asp:PlaceHolder runat="server" ID="Frecuencia" Visible="false">
								<div class="row">
									<label class="radio-inline">
										<asp:RadioButton ID="rdSinperiodicidad" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
										Sin Periodicidad</label>
								</div>
								<div class="row">
									<label class="radio-inline">
										<asp:RadioButton ID="rdDiaro" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
										Diaria</label>
								</div>
								<div class="row">
									<label class="radio-inline">

										<asp:RadioButton ID="rdSemanal" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
										Semanal</label>
								</div>
								<div class="row">
									<label class="radio-inline">
										<asp:RadioButton ID="rdMensual" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
										Mensual</label>
								</div>
								<div class="row">
									<label class="radio-inline">
										<asp:RadioButton ID="rdAnual" GroupName="TipoFrecuencia" AutoPostBack="true" OnCheckedChanged="rdSemanal_CheckedChanged" runat="server" />
										Anual</label>
								</div>
							</asp:PlaceHolder>
						</div>
						<div class=" col-md-10" style="margin-top: 15px">
							<asp:PlaceHolder runat="server" ID="PanelSemanal" Visible="false">
								<div class="row">
									<div class="col-md-3">
										<asp:Label Text="Repetir Cada" runat="server"></asp:Label>
									</div>
									<div class="col-md-2">
										<asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtFrecuenciaSemanal" runat="server"></asp:TextBox>

									</div>
									<div class="col-md-3">
										<asp:Label Text="Semanas el:" runat="server"></asp:Label>
									</div>
									<div class="row" style="margin-bottom: 20px;">
										<asp:Label runat="server"></asp:Label>
									</div>

									<div class="col-md-4">
										<label class="checkbox-inline">
											<asp:CheckBox AutoPostBack="true" ID="CBLunes" runat="server" />Lunes</label>
									</div>
									<div class="col-md-4">
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" AutoPostBack="true" ID="CBMartes" />Martes</label>
									</div>
									<div class="col-md-4">
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" AutoPostBack="true" ID="CBMiercoles" />Miercoles</label>
									</div>
									<div class="col-md-4">
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" AutoPostBack="true" ID="CBJueves" />Jueves</label>
									</div>
									<div class="col-md-4">
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" AutoPostBack="true" ID="CBViernes" />Viernes</label>
									</div>
									<div class="col-md-4">
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" AutoPostBack="true" ID="CBSabado" />Sabado</label>
									</div>
									<div class="col-md-4">
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" AutoPostBack="true" ID="CBDomingo" />Domingo</label>
									</div>
								</div>
							</asp:PlaceHolder>
						</div>
						<div class="col-md-10" style="margin-top: 15px">
							<asp:PlaceHolder runat="server" ID="PanelDiario" Visible="false">
								<div class="row">
									<div class="col-md-2">
										<label class="radio-inline">
											<asp:RadioButton ID="rdCadaDiario" OnCheckedChanged="rdCadaDiario_CheckedChanged" GroupName="FrecuenciaDiaria" AutoPostBack="true" runat="server" />
											Cada
										</label>
									</div>
									<div class="col-md-2">
										<asp:TextBox CssClass="form-control" ID="txtcadaDiario" AutoPostBack="true" runat="server" />
									</div>
									<div class="col-md-2">
										<asp:Label Text="Dias" runat="server" />
									</div>
									<div class="row" style="margin-bottom: 20px;">
										<asp:Label runat="server" />
									</div>
									<div class="col-md-8">
										<label class="radio-inline">
											<asp:RadioButton ID="rdtodoslosdias" OnCheckedChanged="rdCadaDiario_CheckedChanged" GroupName="FrecuenciaDiaria" AutoPostBack="true" runat="server" />
											todos los dias de la semana</label>
									</div>
								</div>
							</asp:PlaceHolder>
						</div>

						<div class="col-md-10">
							<asp:PlaceHolder runat="server" ID="PanelMensual" Visible="false">
								<div class="row">

									<div class="col-md-2">
										<label class="radio-inline">
											<asp:RadioButton OnCheckedChanged="rdElDiaMensual_CheckedChanged" GroupName="FrecuenciaMensual" AutoPostBack="true" ID="rdElDiaMensual" runat="server" />
											El dia</label>
									</div>
									<div class="col-md-2">
										<asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtDiasMes" runat="server"></asp:TextBox>
									</div>
									<div class="col-md-2">
										<asp:Label Text="de Cada" runat="server"></asp:Label>
									</div>
									<div class="col-md-2">
										<asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtCadames" runat="server"></asp:TextBox>
									</div>

									<div class="col-md-2">
										<asp:Label Text="Meses" runat="server"></asp:Label>
									</div>
									<div class="row" style="margin-bottom: 20px;">
										<asp:Label runat="server"></asp:Label>
									</div>
									<div class="col-md-1">
										<label class="radio-inline">
											<asp:RadioButton OnCheckedChanged="rdElDiaMensual_CheckedChanged" GroupName="FrecuenciaMensual" AutoPostBack="true" ID="rdElMeses" runat="server" />
											El</label>
									</div>
									<div class="col-md-5">
										<asp:DropDownList ID="DdOrdinalMensual" CssClass="form-control" runat="server"></asp:DropDownList>
									</div>
									<div class="col-md-5">
										<asp:DropDownList ID="DdDiasMensual" CssClass="form-control" runat="server"></asp:DropDownList>
									</div>
									<div class="row" style="margin-bottom: 20px;">
										<asp:Label runat="server"></asp:Label>
									</div>
									<div class="col-md-2">
										<asp:Label Text="de cada" runat="server"></asp:Label>
									</div>
									<div class="col-md-2">
										<asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtcadamensual" runat="server"></asp:TextBox>
									</div>
									<div class="col-md-2">
										<asp:Label Text="Meses" runat="server"></asp:Label>
									</div>
								</div>
							</asp:PlaceHolder>
						</div>

						<div class="col-md-10" style="margin-top: 15px">
							<asp:PlaceHolder runat="server" ID="PanelAnual" Visible="false">
								<div class="row">
									<div class="col-md-3">
										<asp:Label Text="Repetir Cada" runat="server"></asp:Label>
									</div>
									<div class="col-md-2">
										<asp:TextBox CssClass="form-control" ID="txtFrecuenciaAnual" AutoPostBack="true" runat="server"></asp:TextBox>
									</div>
									<div class="col-md-2">
										<asp:Label Text="Años" runat="server"></asp:Label>
									</div>
									<div class="row" style="margin-bottom: 20px;">
										<asp:Label runat="server"></asp:Label>
									</div>
									<div class="col-md-1">
										<label class="radio-inline">
											<asp:RadioButton OnCheckedChanged="rdElDelAño_CheckedChanged" GroupName="FrecuenciaAnual" AutoPostBack="true" ID="rdElDelAño" runat="server" />
											El</label>
									</div>
									<div class="col-md-5">
										<asp:DropDownList ID="DdMesAño1" CssClass="form-control" runat="server"></asp:DropDownList>
									</div>
									<div class="col-md-2">
										<asp:TextBox CssClass="form-control" AutoPostBack="true" ID="txtDiasmesaño" runat="server"></asp:TextBox>
									</div>
									<div class="row" style="margin-bottom: 20px;">
										<asp:Label runat="server"></asp:Label>
									</div>
									<div class="col-md-1">
										<label class="radio-inline">
											<asp:RadioButton OnCheckedChanged="rdElDelAño_CheckedChanged" GroupName="FrecuenciaAnual" AutoPostBack="true" ID="rdElDelAño2" runat="server" />
											El</label>
									</div>
									<div class="col-md-5">
										<asp:DropDownList ID="DdOrdinalAño" CssClass="form-control" runat="server"></asp:DropDownList>
									</div>
									<div class="col-md-5">
										<asp:DropDownList ID="DdDiasAño" CssClass="form-control" runat="server"></asp:DropDownList>
									</div>
									<div class="row" style="margin-bottom: 20px;">
										<asp:Label runat="server"></asp:Label>
									</div>
									<div class="col-md-1">
										<asp:Label Text="de" runat="server"></asp:Label>
									</div>
									<div class="col-md-5">
										<asp:DropDownList ID="DdMesAño2" CssClass="form-control" runat="server"></asp:DropDownList>
									</div>
								</div>
							</asp:PlaceHolder>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelAntecesor" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<label class="checkbox-inline">
									<asp:CheckBox ID="cbActivarAntecesor" runat="server" AutoPostBack="true" OnCheckedChanged="cbActivarAntecesor_CheckedChanged" />
									Activar antecesor
								</label>
							</div>
						</div>
						<asp:PlaceHolder ID="mostrarAntecesor" runat="server">
							<asp:PlaceHolder ID="panelbuscarantecesor" runat="server">
								<div class="row">
									<div class="col-md-12">
										<asp:HiddenField ID="UidAntecesor" runat="server" />
										<asp:TextBox CssClass="hide" ID="txtUidAntecesor" runat="server" />
										<small>Antecesor</small>
										<div class="input-group">
											<asp:TextBox ID="txtNombreAntecesor" runat="server" CssClass="form-control input-sm" />
											<span class="input-group-btn">
												<asp:LinkButton ID="btnBuscarAntecesor" OnClick="btnBuscarAntecesor_Click" runat="server" CssClass="btn btn-sm disabled btn-default">
													<span class="glyphicon glyphicon-search"></span>
													 Buscar Antecesor													
												</asp:LinkButton>
											</span>
										</div>
									</div>
								</div>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="gridAntecesor" runat="server">
								<div class="text-right">
									<asp:LinkButton ID="btnCancelarBusquedaAntecesor" OnClick="btnCancelarBusquedaAntecesor_Click" CssClass="btn btn-sm btn-danger" runat="server">
                                        <span class="glyphicon glyphicon-remove"></span>
									</asp:LinkButton>
								</div>
								<asp:GridView ID="dvgAntecesor" runat="server" CssClass="table table-bordered table-responsive table-condensed input-sm" OnSelectedIndexChanged="dvgAntecesor_SelectedIndexChanged" OnRowDataBound="dvgAntecesor_RowDataBound" DataKeyNames="UidTarea" AutoGenerateColumns="false">
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
									<EmptyDataTemplate>
										No existe ningun antecesor con ese nombre.
									</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="IntFolio" HeaderText="Folio" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
								</asp:GridView>
							</asp:PlaceHolder>
							<div class="row">
								<div class="col-xs-12">
									<h6>Cumplimiento</h6>
									Ejecutar tantos días
                                    <asp:TextBox ID="txtDiasDespues" runat="server" />
									después.
								</div>
								<div class="col-xs-12">
									<label class="checkbox-inline">
										<asp:CheckBox ID="cbUsarNotificacion" runat="server" AutoPostBack="true" OnCheckedChanged="cbUsarNotificacion_CheckedChanged" />
										Usar notificación del antecesor.
									</label>
								</div>
							</div>
							<asp:PlaceHolder ID="antecesorNotificacion" runat="server" Visible="false">
								<div class="row">
									<div class="col-xs-12">
										<h6>Número de repeticiones</h6>
										<div class="row">
											<div class="col-xs-12">
												<label class="radio-inline">
													<asp:RadioButton ID="rdNumeroLimitado" runat="server" AutoPostBack="true" GroupName="MaximoDias" />Máximo
                                                    <asp:TextBox ID="txtMaximo" runat="server" />
													veces
												</label>
											</div>
											<div class="col-xs-12">
												<label class="radio-inline">
													<asp:RadioButton ID="rdIlimitado" runat="server" AutoPostBack="true" GroupName="MaximoDias" />Siempre
												</label>
											</div>
										</div>
									</div>
								</div>
							</asp:PlaceHolder>
						</asp:PlaceHolder>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelDepartamentos" runat="server">
						<div>
							<asp:TextBox CssClass="hide" ID="txtUidDepartamentos" runat="server" />
							<asp:TextBox CssClass="hide" ID="txtUidArea" runat="server" />
							<asp:TextBox CssClass="hide" ID="txtBusquedaDeDepartamento" runat="server" />
							<asp:HiddenField ID="UidDepartamento" runat="server" />
							<div class="col-md-12 pd-left-right-5">
								<asp:Label runat="server">Asignar a: </asp:Label>
							</div>
							<div class="col-md-4">
								<label class="radio-inline" style="margin-top: 5px">
									<asp:RadioButton OnCheckedChanged="rdDepartamento_CheckedChanged" GroupName="DepartamentoArea" AutoPostBack="true" ID="rdDepartamento" runat="server" />
									Departamento</label>
							</div>
							<div class="col-md-4" style="margin-top: 5px">
								<label class="radio-inline">
									<asp:RadioButton OnCheckedChanged="rdDepartamento_CheckedChanged" GroupName="DepartamentoArea" AutoPostBack="true" ID="rdArea" runat="server" />
									Área</label>
							</div>
							<div class="row" style="color: red; padding-top: 10px;">
								<div class="col-xs-12">
									<asp:Label ID="lblErrorDepartamentos" Text="" runat="server" />
								</div>
							</div>
							<div class="col-xs-12 pd-left-right-5">
								<div class="col-md-12 pd-left-right-5">
									<asp:Label ID="lblAceptarEliminarDepartamento" runat="server" />
									<asp:LinkButton ID="btnAceptarEliminarDepartamento" OnClick="btnAceptarEliminarDepartamento_Click" CssClass="btn btn-sm btn-success" runat="server">
										<asp:Label ID="Label2" CssClass="glyphicon glyphicon-ok" runat="server" />
									</asp:LinkButton>
									<asp:LinkButton ID="btnCancelarEliminarDepartamento" OnClick="btnCancelarEliminarDepartamento_Click" CssClass="btn btn-sm btn-danger" runat="server">
										<span class="glyphicon glyphicon-remove"></span>
									</asp:LinkButton>
								</div>
								<h6>Nombre</h6>
								<div class="input-group">
									<asp:TextBox ID="txtNombreDepartamento" runat="server" CssClass="form-control input-sm" />
									<div class="input-group-btn">
										<asp:LinkButton ID="btnBuscarDepartamentos" OnClick="btnBuscarDepartamentos_Click" runat="server" CssClass="btn btn-sm disabled btn-default">
                                            Buscar Departamentos
										</asp:LinkButton>
										<asp:LinkButton ID="btnEliminarDepartamento" runat="server" CssClass="btn btn-default btn-sm disabled" OnClick="btnEliminarDepartamento_Click">
                                            Eliminar
                                            <span class="glyphicon glyphicon-trash"></span>
										</asp:LinkButton>
									</div>
								</div>
							</div>
							<div class="col-xs-12 pd-left-right-5">
								<asp:PlaceHolder Visible="false" runat="server" ID="PanelDepartamentosGuardar">
									<div style="padding-top: 10px">
										<asp:Label runat="server">Departamentos asignados.</asp:Label>
										<asp:GridView ID="dvgDepartamentosGuardar" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidDepartamento" OnRowDataBound="dvgDepartamentosGuardar_RowDataBound" OnSelectedIndexChanged="dvgDepartamentosGuardar_SelectedIndexChanged">
											<EmptyDataTemplate>No hay Departamentos Asignados</EmptyDataTemplate>
											<Columns>
												<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
												<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
												<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
											</Columns>
										</asp:GridView>
									</div>
								</asp:PlaceHolder>
								<asp:PlaceHolder Visible="false" runat="server" ID="PanelAreasGuardar">
									<div style="padding-top: 10px">
										<asp:Label runat="server">Áreas asignadas.</asp:Label>
										<asp:GridView ID="dvgAreasGuardar" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidArea" OnRowDataBound="dvgAreasGuardar_RowDataBound" OnSelectedIndexChanged="dvgAreasGuardar_SelectedIndexChanged">
											<EmptyDataTemplate>No hay Areas Asignadas</EmptyDataTemplate>
											<Columns>
												<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
												<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
												<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
											</Columns>
										</asp:GridView>
									</div>
								</asp:PlaceHolder>
								<asp:PlaceHolder ID="userGrid" Visible="false" runat="server">
									<div style="padding-top: 10px">
										<div class="text-left">
											<asp:LinkButton ID="btncancelarburcarDepartamento" OnClick="cancelarburcarDepartamento_Click" CssClass="btn btn-sm btn-danger" runat="server">
                                                    <span class="glyphicon glyphicon-remove"></span>
											</asp:LinkButton>
										</div>
										<div runat="server" id="divDepartamentos">
											<asp:Label runat="server">Lista de Departamentos</asp:Label>
											<asp:GridView ID="dgvDepartamentos" runat="server" AllowPaging="true" CssClass="table table-bordered table-responsive table-condensed input-sm" OnSelectedIndexChanged="dgvDepartamentos_SelectedIndexChanged" OnRowDataBound="dgvDepartamentos_RowDataBound" DataKeyNames="UidDepartamento" AutoGenerateColumns="false">
												<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
												<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
												<EmptyDataTemplate>
													No existen Departamentos que coincidan con la búsqueda.
												</EmptyDataTemplate>
												<Columns>
													<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
													<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
													<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
												</Columns>
											</asp:GridView>
										</div>
									</div>
								</asp:PlaceHolder>
								<asp:PlaceHolder Visible="false" ID="panelArea" runat="server">
									<div class="table-bordered" style="padding-top: 10px">
										<div class="text-left">
											<asp:LinkButton ID="cancelarArea" OnClick="cancelarArea_Click" CssClass="btn btn-sm btn-danger" runat="server">
                                                    <span class="glyphicon glyphicon-remove"></span>
											</asp:LinkButton>
										</div>
										<div>
											<asp:Label runat="server">Areas del Departamento Seleccionado</asp:Label>
											<asp:GridView ID="dvgArea" runat="server" AllowPaging="true" CssClass="table table-bordered table-responsive table-condensed input-sm" OnSelectedIndexChanged="dvgArea_SelectedIndexChanged" OnRowDataBound="dvgArea_RowDataBound" DataKeyNames="UidArea" AutoGenerateColumns="false">
												<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
												<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
												<EmptyDataTemplate>
													No existen Areas que coincidan con la búsqueda.
												</EmptyDataTemplate>
												<Columns>
													<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
													<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
													<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
												</Columns>
											</asp:GridView>
										</div>
									</div>
								</asp:PlaceHolder>
							</div>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelNotificacion" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<label class="checkbox-inline">
									<asp:CheckBox runat="server" ID="cbActivarNotificacion" AutoPostBack="true" OnCheckedChanged="cbActivarNotificacion_CheckedChanged" Checked="false" />Activar notificación
								</label>
							</div>
							<asp:Panel runat="server" ID="pnlSendTo" CssClass="col-md-12">
								<h6>Notificar a:</h6>
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="cbSendToAdministrador" />
									Administrador
								</label>
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="cbSendToSupervisor" />
									Supervisor
								</label>
							</asp:Panel>
							<asp:PlaceHolder runat="server" ID="notificacionBool">
								<div class="col-xs-12">
									<h6>Se lanzará la notificación cuando el valor sea:</h6>
									<label class="radio-inline">
										<asp:RadioButton runat="server" ID="rbVerdadero" GroupName="grpValorBool" />Sí</label>
									<label class="radio-inline">
										<asp:RadioButton runat="server" ID="rbFalso" GroupName="grpValorBool" />No</label>
								</div>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="notificacionValor" runat="server">
								<div class="col-xs-12">
									<h6>Se lanzará la notificación cuando se cumpla las condiciones: </h6>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-12">
											<label class="radio-inline">
												<asp:RadioButton ID="rbMayor" runat="server" GroupName="MayorIgual" />
												Mayor
											</label>
											<label class="radio-inline">
												<asp:RadioButton ID="rbMayorIgual" runat="server" GroupName="MayorIgual" />
												Mayor o igual
											</label>
										</div>
										<div class="col-xs-12">
											<div class="input-group">
												<asp:TextBox runat="server" ID="txtMayorQue" CssClass="form-control" />
												<asp:Label ID="txtMedida2" runat="server" CssClass="input-group-addon input-group-sm" />
											</div>
										</div>
									</div>
								</div>
								<div class="col-xs-6">
									<div class="row">
										<div class="col-xs-12">
											<label class="radio-inline">
												<asp:RadioButton ID="rbMenor" runat="server" GroupName="MenorIgual" />
												Menor
											</label>
											<label class="radio-inline">
												<asp:RadioButton ID="rbMenorIgual" runat="server" GroupName="MenorIgual" />
												Menor o igual
											</label>
										</div>
										<div class="col-xs-12">
											<div class="input-group">
												<asp:TextBox ID="txtMenorQue" runat="server" CssClass="form-control" />
												<asp:Label ID="txtMedida1" runat="server" CssClass="input-group-addon input-group-sm" />
											</div>
										</div>
									</div>
								</div>
							</asp:PlaceHolder>
							<asp:PlaceHolder ID="notificacionOpcion" runat="server">
								<div class="col-xs-12">
									<h6>Se lanzará la notificación la cumplir con las opciones:</h6>
									<asp:ListBox runat="server" ID="lbOpcionesSeleccionadas" CssClass="form-control" />
								</div>
							</asp:PlaceHolder>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function hora() {
			$('.input-group.clockpicker').clockpicker();
		}
	</script>

	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>
	<script>
		//<![CDATA[
		function pageLoad() {

			prepareFechaAll();
			hora();
		}
        //]]>
	</script>
</asp:Content>
