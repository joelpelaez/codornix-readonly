﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="DatosUsuario.aspx.cs" Inherits="CodorniX.Vista.DatosUsuario" MasterPageFile="Site1.Master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
    <div class="row">
        <!--panel izquierdo-->
        <div class="col-md-6">

            <asp:PlaceHolder ID="panelDireccion" runat="server">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        Dirección
                    </div>
                    <div class="btn-toolbar">
                        <div class="btn-group pull-right">
                            <asp:LinkButton ID="btnOkDireccion" runat="server" OnClick="btnOkDireccion_Click" CssClass="btn btn-success btn-sm disabled">
                                <span class="glyphicon glyphicon-ok"></span>
                            </asp:LinkButton>
                            <asp:LinkButton ID="btnCancelarDireccion" runat="server" OnClick="btnCancelarDireccion_Click" CssClass="btn btn-danger btn-sm disabled">
                                <span class="glyphicon glyphicon-remove"></span>
                            </asp:LinkButton>
                        </div>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:Label ID="lblErrorDireccion" Text="" runat="server" />
                            </div>
                        </div>
                        <div class="row">
                            <asp:TextBox ID="uidDireccion" runat="server" CssClass="hidden" />
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h6>País</h6>
                               <asp:DropDownList ID="ddPais" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddPais_SelectedIndexChanged" placeholder="País" />
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h6>Estado</h6>
                                <asp:DropDownList ID="ddEstado" runat="server" CssClass="form-control" placeholder="Estado" />
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <asp:Panel ID="frmGrpMunicipio" runat="server" CssClass="form-group">
                                    <h6>Municipio</h6>
                                    <asp:TextBox ID="txtMunicipio" MaxLength="30" runat="server" CssClass="form-control" placeholder="Municipio" />
                                </asp:Panel>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <asp:Panel ID="frmGrpCiudad" runat="server" CssClass="form-group">
                                    <h6>Ciudad</h6>
                                    <asp:TextBox ID="txtCiudad" MaxLength="30" runat="server" CssClass="form-control" placeholder="Ciudad" />
                                </asp:Panel>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <asp:Panel ID="frmGrpColonia" runat="server" CssClass="form-group">
                                    <h6>Colonia</h6>
                                    <asp:TextBox ID="txtColonia" MaxLength="30" runat="server" CssClass="form-control" placeholder="Colonia" />
                                </asp:Panel>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <asp:Panel ID="frmGrpCalle" runat="server" CssClass="form-group">
                                    <h6>Calle</h6>
                                    <asp:TextBox ID="txtCalle" MaxLength="20" runat="server" CssClass="form-control" placeholder="Calle" />
                                </asp:Panel>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <asp:Panel ID="frmGrpConCalle" runat="server" CssClass="form-group">
                                    <h6>Con Calle</h6>
                                    <asp:TextBox ID="txtConCalle" MaxLength="20" runat="server" CssClass="form-control" placeholder="Con Calle" />
                                </asp:Panel>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <asp:Panel ID="frmGrpYCalle" runat="server" CssClass="form-group">
                                    <h6>Y Calle</h6>
                                    <asp:TextBox ID="txtYCalle" MaxLength="20" runat="server" CssClass="form-control" placeholder="Y Calle" />
                                </asp:Panel>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <asp:Panel ID="frmGrpNoExt" runat="server" CssClass="form-group">
                                    <h6>No. Exterior</h6>
                                    <asp:TextBox ID="txtNoExt" MaxLength="15" runat="server" CssClass="form-control" placeholder="No. Exterior" />
                                </asp:Panel>
                            </div>
                            <div class="col-md-4 col-sm-6 col-xs-12">
                                <h6>No. Interior</h6>
                                <asp:TextBox ID="txtNoInt" MaxLength="15" runat="server" CssClass="form-control" placeholder="No. Interior" />
                            </div>
                            <div class="col-md-8 col-sm-12 col-xs-12">
                                <h6>Referencia</h6>
                                <asp:TextBox ID="txtReferencia" runat="server" CssClass="form-control" placeholder="Referencia" />
                            </div>
                        </div>
                    </div>
                </div>
            </asp:PlaceHolder>
        </div>
        <!-- panel derecho-->
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    Gestion de Usuario
                </div>
                <div class="text-left">
                   <asp:LinkButton ID="btnEditar" OnClick="btnEditar_Click" CssClass="btn btn-sm disabled btn-default" runat="server">
                            <span class="glyphicon glyphicon-cog"></span>
                            Editar
                    </asp:LinkButton><asp:LinkButton ID="btnAceptar" OnClick="btnAceptar_Click" CssClass="btn btn-sm btn-success" runat="server">
                        <asp:Label ID="lblAccion" CssClass="glyphicon glyphicon-ok" runat="server" />
                    </asp:LinkButton><asp:LinkButton ID="btnCancelar" OnClick="btnCancelar_Click" CssClass="btn btn-sm btn-danger" runat="server">
                            <span class="glyphicon glyphicon-remove"></span>
                    </asp:LinkButton>
                    <asp:Label ID="lblMensaje" runat="server" />
                    <asp:TextBox CssClass="hide" ID="txtUidUsuario" runat="server" />
                    <asp:TextBox CssClass="hide btn btn-sm btn-default" ID="txtEmpresa" runat="server" />
                </div>
                <div class="panel-body">
                    <ul class="nav nav-tabs">
                        <li  class="active" id="activeEmpresas" runat="server">
                            <asp:LinkButton ID="tabEmpresas" OnClick="tabEmpresas_Click" runat="server" Text="Empresa" />
                        </li>
                         <li id="activeSucursales" runat="server">
                            <asp:LinkButton ID="tabSucursales" runat="server" OnClick="tabSucursales_Click" Text="Sucursal" /></li>
                        <li id="activeDatosGenerales" runat="server">
                            <asp:LinkButton ID="tabDatos" runat="server" OnClick="tabDatos_Click" Text="Datos Generales" /></li>
                        <li id="activeDirecciones" runat="server">
                            <asp:LinkButton ID="tabDirecciones" runat="server" OnClick="tabDirecciones_Click" Text="Direcciones" /></li>
                        <li id="activeTelefonos" runat="server">
                            <asp:LinkButton ID="tabTelefonos" runat="server" OnClick="tabTelefonos_Click" Text="Teléfonos" /></li>
                        <li id="activeAccesos" runat="server">
                            <asp:LinkButton ID="tabAccesos" OnClick="tabAccesos_Click" runat="server" Text="Accesos" />
                        </li>
                    </ul>

                     <asp:PlaceHolder ID="PanelEmpresa" runat="server">
                        <div class="row">
                            <asp:TextBox CssClass="hide" ID="txtUidEmpresa" runat="server" />
                            <asp:HiddenField ID="UidEmpresa" runat="server" />
                            <div class="col-xs-4">
                                <h6>RFC</h6>
                                <asp:TextBox ID="txtRfc" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-4">
                                <h6>Nombre Comercial</h6>
                                <asp:TextBox ID="txtNombreComercial" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-4">
                                <h6>Razon Social</h6>
                                <asp:TextBox ID="txtRazonSocial" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder ID="PanelSucursales" runat="server">
                        <div class="row">
                            <asp:TextBox CssClass="hide" ID="txtUidSucursal" runat="server" />
                            <asp:HiddenField ID="HiddenField1" runat="server" />
                            <div class="col-xs-12">
                                <asp:Label ID="lblsucursal" runat="server"></asp:Label>
                            </div>
                            <div class="col-xs-4">
                                <h6>Nombre</h6>
                                <asp:TextBox ID="txtNombreSucursal" runat="server" CssClass="form-control" />
                            </div>
                            <div class="col-xs-4">
                                <h6>Tipo</h6>
                                <asp:TextBox ID="txtTipoSucursal" runat="server" CssClass="form-control" />
                            </div>
                        </div>
                    </asp:PlaceHolder>


                    <asp:PlaceHolder ID="PanelDatosGeneralesUsuario" runat="server">
                        <div class="row" style="color: red; padding-top: 10px;">
                            <div class="col-xs-12">
                                <asp:Label ID="lblErrorUsuario" Text="" runat="server" />
                            </div>
                        </div>
                        <div class="row">

                            <asp:UpdatePanel runat="server">

                                <ContentTemplate>
                                    
                            <div class="col-md-4">
                                <h6>Foto</h6>
                                <asp:Image runat="server" CssClass="img img-thumbnail" ID="ImgUsuario" Width="200px" Height="160px" />
                                 <asp:TextBox ID="txtimagen" CssClass="form-control hide" runat="server"></asp:TextBox>
                                <div>
                                    <label ID="lblFotoUsuario" class="btn btn-default btn-file form-control" runat="server">
                                        Escoger Foto
                                        <asp:FileUpload ID="FUImagen" runat="server" />
                                        <asp:Button ID="btnimagen" CssClass="hide" OnClick="imagen" runat="server" />
                                    </label>
                                </div>
                            </div>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="btnimagen"  />
                                </Triggers>
                            </asp:UpdatePanel>
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpNombre" runat="server" CssClass="form-group">
                                    <h6>Nombre</h6>
                                    <asp:TextBox ID="txtNombre" CssClass="form-control" placeholder="Nombre" runat="server"></asp:TextBox>
                                </asp:Panel>
                            </div>

                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpApellidoPaterno" runat="server" CssClass="form-group">
                                    <h6>Apellido Paterno</h6>
                                    <asp:TextBox ID="txtApellidoPaterno" placeholder="Apellido Paterno" CssClass="form-control" runat="server"></asp:TextBox>
                                </asp:Panel>
                            </div>
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpApellidoMaterno" runat="server" CssClass="form-group">
                                    <h6>Apellido Materno</h6>
                                    <asp:TextBox ID="txtApellidoMaterno" placeholder="Apellido Materno" CssClass="form-control" runat="server"></asp:TextBox>
                                </asp:Panel>
                            </div>
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpFechaNacimiento" runat="server" CssClass="form-group">
                                    <h6>Fecha Nacimiento</h6>
                                    <div class="input-group date extra">
                                        <asp:TextBox ID="txtFechaNacimiento" placeholder="Fecha Nacimiento" CssClass="form-control" runat="server" />
                                        <span class="input-group-addon input-sm ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpFechaInicio" runat="server" CssClass="form-group">
                                    <h6>Fecha Inicio</h6>

                                    <div class="input-group date extra">
                                        <asp:TextBox ID="txtFechaInicio" placeholder="Fecha Inicio" CssClass="form-control" runat="server"></asp:TextBox>
                                        <span class="input-group-addon input-sm ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </asp:Panel>
                            </div>
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpFechaFin" runat="server" CssClass="form-group">
                                    <h6>Fecha de terminación</h6>

                                    <div class="input-group date extra">
                                        <asp:TextBox ID="txtFechaFin" placeholder="Fecha de terminación" CssClass="form-control" runat="server"></asp:TextBox>
                                        <span class="input-group-addon input-sm ">
                                            <i class="glyphicon glyphicon-calendar"></i>
                                        </span>
                                    </div>
                                </asp:Panel>
                            </div>
                           
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpUsuario" runat="server" CssClass="form-group">
                                    <h6>Usuario</h6>
                                    <asp:TextBox ID="txtUsuario" placeholder="Usuario" CssClass="form-control" runat="server"></asp:TextBox>
                                </asp:Panel>
                            </div>
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpPassword" runat="server" CssClass="form-group">
                                    <h6>Contraseña</h6>
                                    <asp:TextBox ID="txtPassword" placeholder="Contraseña" CssClass="form-control" runat="server"></asp:TextBox>
                                </asp:Panel>
                            </div>
                            <div class="col-md-4">
                                <asp:Panel ID="frmGrpCorreo" runat="server" CssClass="form-group">
                                    <h6>Correo</h6>
                                    <asp:TextBox ID="txtCorreo" placeholder="Correo Electronico" CssClass="form-control" runat="server"></asp:TextBox>
                                </asp:Panel>
                            </div>
                            <div class="col-md-4">
                                <h6>Perfil</h6>
                                <asp:DropDownList ID="DdPerfil" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                            </div>
                            <div class="col-md-4">
                                <h6>Status</h6>
                                <asp:DropDownList ID="DdStatus" CssClass="form-control" runat="server">
                                </asp:DropDownList>
                            </div>
                             
                             
                        </div>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder ID="panelDirecciones" Visible="false" runat="server">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="btn-group">
                                    <asp:LinkButton ID="btnAgregarDireccion" runat="server" OnClick="btnAgregarDireccion_Click" CssClass="btn btn-default btn-sm disabled">
                                        Nuevo
                                        <span class="glyphicon glyphicon-file"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnEditarDireccion" OnClick="btnEditarDireccion_Click" runat="server" CssClass="btn btn-default btn-sm disabled">
                                        Editar
                                        <span class="glyphicon glyphicon-edit"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnEliminarDireccion" OnClick="btnEliminarDireccion_Click" runat="server" CssClass="btn btn-default btn-sm disabled">
                                        Eliminar
                                        <span class="glyphicon glyphicon-trash"></span>
                                    </asp:LinkButton>


                                </div>
                                <div style="padding-top: 10px;">
                                    <asp:Label ID="lblAceptarEliminarDireccion" runat="server" />
                                    <asp:LinkButton ID="btnAceptarEliminarDireccion" OnClick="btnAceptarEliminarDireccion_Click" CssClass="btn btn-sm btn-success" runat="server">
                                        <asp:Label ID="Label2" CssClass="glyphicon glyphicon-ok" runat="server" />
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="btnCancelarEliminarDireccion" OnClick="btnCancelarEliminarDireccion_Click" CssClass="btn btn-sm btn-danger" runat="server">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                       <div class="row" style="padding-top: 10px;">
                            <div class="col-xs-12">
                                <asp:GridView ID="dgvDirecciones" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false" DataKeyNames="UidDireccion" OnRowDataBound="dgvDirecciones_RowDataBound" OnSelectedIndexChanged="dgvDirecciones_SelectedIndexChanged">
                                    <EmptyDataTemplate>No hay direcciones asignadas a está empresa</EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hidden" ItemStyle-CssClass="hidden" FooterStyle-CssClass="hidden" />
                                        <asp:BoundField DataField="StrCiudad" HeaderText="Ciudad" />
                                        <asp:BoundField DataField="StrCalle" HeaderText="En Calle" />
                                        <asp:BoundField DataField="StrConCalle" HeaderText="Con Calle" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>

                    <asp:PlaceHolder ID="panelTelefonos" Visible="false" runat="server">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="btn-group">
                                    <asp:LinkButton ID="btnAgregarTelefono" OnClick="btnAgregarTelefono_Click" runat="server" Enabled="false" CssClass="btn btn-default btn-sm disabled">
                                        <span class="glyphicon glyphicon-plus"></span>
                                        Nuevo
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnEditarTelefono" runat="server" OnClick="btnEditarTelefono_Click" Enabled="false" CssClass="btn btn-default btn-sm disabled">
                                        <span class="glyphicon glyphicon-edit"></span>
                                        Editar
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnEliminarTelefono" runat="server" OnClick="btnEliminarTelefono_Click" Enabled="false" CssClass="btn btn-default btn-sm disabled">
                                        <span class="glyphicon glyphicon-trash"></span>
                                        Eliminar
                                    </asp:LinkButton>



                                    <asp:LinkButton ID="btnOKTelefono" OnClick="btnOKTelefono_Click" runat="server" Enabled="false" CssClass="btn btn-success btn-sm disabled hidden">
                                        <span class="glyphicon glyphicon-ok"></span>
                                    </asp:LinkButton>
                                    <asp:LinkButton ID="btnCancelarTelefono" OnClick="btnCancelarTelefono_Click" runat="server" Enabled="false" CssClass="btn btn-danger btn-sm disabled hidden">
                                        <span class="glyphicon glyphicon-remove"></span>
                                    </asp:LinkButton>
                                </div>
                                <div style="padding-top: 10px;">
                                    <asp:Label ID="lblAceptarEliminarTelefono" runat="server" />
                                    <asp:LinkButton ID="btnAceptarEliminarTelefono" OnClick="btnAceptarEliminarTelefono_Click" CssClass="btn btn-sm btn-success" runat="server">
                                        <asp:Label ID="Label3" CssClass="glyphicon glyphicon-ok" runat="server" />
                                    </asp:LinkButton>

                                    <asp:LinkButton ID="btnCancelarEliminarTelefono" OnClick="btnCancelarEliminarTelefono_Click" CssClass="btn btn-sm btn-danger" runat="server">
                            <span class="glyphicon glyphicon-remove"></span>
                                    </asp:LinkButton>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <asp:Label ID="lblErrorTelefono" Text="" runat="server" />
                            </div>
                        </div>
                        <div class="row" style="padding-top: 10px;">
                            <asp:TextBox ID="uidTelefono" runat="server" CssClass="hidden disabled" />
                            <div class="col-xs-12 col-md-6">
                                <asp:DropDownList ID="ddTipoTelefono" runat="server" CssClass="form-control" Enabled="false" />
                                <asp:ListBox Visible="false" ID="lbTipoTelefono" runat="server" SelectionMode="Multiple" CssClass="form-control" />
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <asp:Panel ID="frmGrpTelefono" runat="server" CssClass="form-group">
                                    <asp:TextBox ID="txtTelefono" MaxLength="15" runat="server" Enabled="false" CssClass="form-control disabled" placeholder="Teléfono" />
                                </asp:Panel>
                            </div>
                        </div>
                      <div class="row" style="padding-top: 10px;">
                            <div class="col-xs-12">
                                <asp:GridView ID="dgvTelefonos" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false" DataKeyNames="UidTelefono" OnRowDataBound="dgvTelefonos_RowDataBound" OnSelectedIndexChanged="dgvTelefonos_SelectedIndexChanged">
                                    <EmptyDataTemplate>No hay teléfonos asignados a este Usuario</EmptyDataTemplate>
                                    <Columns>
                                        <asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
                                        <asp:BoundField DataField="StrTipoTelefono" HeaderText="Tipo" />
                                        <asp:BoundField DataField="StrTelefono" HeaderText="Ciudad" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </div>
                    </asp:PlaceHolder>

                     <asp:PlaceHolder runat="server" ID="panelAccesos" Visible="false">
                        <div class="row">
                            <div class="col-xs-12">
                                <h4>Lista de módulos autorizados</h4>
                            </div>
                        </div>
                        <ul class="nav nav-tabs">
                            <li class="active" id="activeBackside" runat="server">
                                <asp:LinkButton ID="tabBackside" runat="server" OnClick="tabBackside_Click" Text="Backsite" /></li>
                            <li id="activeBackend" runat="server">
                                <asp:LinkButton ID="tabBackend" runat="server" OnClick="tabBackend_Click" Text="Backend" /></li>
                            <li id="activeFrontend" runat="server">
                                <asp:LinkButton ID="tabFrontend" OnClick="tabFrontend_Click" runat="server" Text="Frontend" /></li>
                        </ul>
                        <asp:PlaceHolder runat="server" ID="accesoBackside" Visible="true">
                            <div class="row">
                                <asp:PlaceHolder runat="server" ID="modulosBackside" EnableViewState="true" />
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="accesoBackend" Visible="false">
                            <div class="row">
                                <asp:PlaceHolder runat="server" ID="modulosBackend" EnableViewState="true" />
                            </div>
                        </asp:PlaceHolder>
                        <asp:PlaceHolder runat="server" ID="accesoFrontend" Visible="false">
                            <div class="row">
                                <asp:PlaceHolder runat="server" ID="modulosFrontend" EnableViewState="true" />
                            </div>
                        </asp:PlaceHolder>
                    </asp:PlaceHolder>
                </div>
            </div>
        </div>
    </div>
     <script>

         function upload(FileUpload1) {
             if (FileUpload1.value != '') {
                 document.getElementById("<% = btnimagen.ClientID  %>").click();
             }
         }
                            </script>
     <script type="text/javascript">
         //<![CDATA[
         function prepareFechaAll() {
             $('.input-group.date.extra').datepicker({
                 autoclose: true,
                 todayHighlight: true,
                 language: 'es',
                 format: 'dd/mm/yyyy',
                 clearBtn: true,
                 todayBtn: true,
             });
         }
                                        //]]>
                            </script>
    <script type="text/javascript">
        //<![CDATA[
        function prepareFechaF2() {
            $('#fechaF2').datepicker({
                autoclose: true,
                todayHighlight: true,
                language: 'es',
                format: 'dd/mm/yyyy',
                clearBtn: true,
                todayBtn: true,
            });
        }
                                    //]]>
                                    </script>
    <script type="text/javascript">
                                        //<![CDATA[
                                        function prepareFechaF() {
                                            $('#fechaF').datepicker({
                                                autoclose: true,
                                                todayHighlight: true,
                                                language: 'es',
                                                format: 'dd/mm/yyyy',
                                                clearBtn: true,
                                                todayBtn: true,
                                            }).on('changeDate', function (ev) {
                                                var fecha2 = $('.fechaF2');
                                                if (fecha2.val().length == 0)
                                                    fecha2.val($(".fechaF").val());
                                            });
                                        }
                                        //]]>
                                    </script>
     <script type="text/javascript">
                                        //<![CDATA[
                                        function prepareFechaI2() {
                                            $('#fechaI2').datepicker({
                                                autoclose: true,
                                                todayHighlight: true,
                                                language: 'es',
                                                format: 'dd/mm/yyyy',
                                                clearBtn: true,
                                                todayBtn: true,
                                            });
                                        }
                                        //]]>
                                    </script>
     <script type="text/javascript">
                                        //<![CDATA[
                                        function prepareFechaI() {
                                            $('#fechaI').datepicker({
                                                autoclose: true,
                                                todayHighlight: true,
                                                language: 'es',
                                                format: 'dd/mm/yyyy',
                                                clearBtn: true,
                                                todayBtn: true,
                                            }).on('changeDate', function (ev) {
                                                var fecha2 = $('.fechaI2');
                                                if (fecha2.val().length == 0)
                                                    fecha2.val($(".fechaI").val());
                                            });
                                        }
                                        //]]>
                                    </script>
     <script type="text/javascript">
                                        //<![CDATA[
                                        function prepareFechaN2() {
                                            $('#fechaN2').datepicker({
                                                autoclose: true,
                                                todayHighlight: true,
                                                language: 'es',
                                                format: 'dd/mm/yyyy',
                                                clearBtn: true,
                                                todayBtn: true,
                                            });
                                        }
                                        //]]>
                                    </script>
     <script type="text/javascript">
                                        //<![CDATA[
                                        function prepareFechaN() {
                                            $('#fechaN').datepicker({
                                                autoclose: true,
                                                todayHighlight: true,
                                                language: 'es',
                                                format: 'dd/mm/yyyy',
                                                clearBtn: true,
                                                todayBtn: true,
                                            }).on('changeDate', function (ev) {
                                                var fecha2 = $('.fechaN2');
                                                if (fecha2.val().length == 0)
                                                    fecha2.val($(".fechaN").val());
                                            });
                                        }
                                        //]]>
                                    </script>



    <script>
                                //<![CDATA[
                                function pageLoad() {
                                    prepareFechaN();
                                    prepareFechaN2();
                                    prepareFechaI();
                                    prepareFechaI2();
                                    prepareFechaF();
                                    prepareFechaF2();

                                    prepareFechaAll();
                                }
        //]]>

    </script>
    <style>
        .btn-file {
            position: relative;
            overflow: hidden;
        }

            .btn-file input[type=file] {
                position: absolute;
                top: 0;
                right: 0;
                min-width: 100%;
                min-height: 100%;
                font-size: 100px;
                text-align: right;
                filter: alpha(opacity=0);
                opacity: 0;
                outline: none;
                background: white;
                cursor: inherit;
                display: block;
            }
    </style>
</asp:Content>
