﻿using System;
using CodorniX.VistaDelModelo;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;

namespace CodorniX.Vista
{
	public partial class ReporteTareasNuevasTurno : System.Web.UI.Page
	{
		private VMReporteTarea VM = new VMReporteTarea();
		private Modelo.Sesion SesionActual
		{
			get { return (Modelo.Sesion)Session["Sesion"]; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Modelo.Acceso.TieneAccesoAModulo("ReporteTareas", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Modelo.Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			ReportViewer1.SizeToReportContent = true;
			ReportViewer1.Width = Unit.Percentage(100);
			ReportViewer1.Height = Unit.Percentage(100);

			if (!IsPostBack)
			{
				Guid UidPeriodo;
				DateTime DtFechaTurno;
				string StrHoraInicio, StrHoraFin;
				int IntFolioTurnoA, IntFolioTurnoN;
				try
				{
					UidPeriodo = (Guid)Session["CAPeriodo"];
					DtFechaTurno = (DateTime)Session["CAFecha"];
					StrHoraInicio = (string)Session["CAHoraInicio"];
					StrHoraFin = (string)Session["CAHoraFin"];
					IntFolioTurnoA = Session["CAFolioTurnoA"] == null ? 0 : ((int)Session["CAFolioTurnoA"]);
					IntFolioTurnoN = Session["CAFolioTurnoN"] == null ? 0 : ((int)Session["CAFolioTurnoN"]);
				}
				catch
				{
					Response.Redirect(Modelo.Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}

				VM.ObtenerPeriodo(UidPeriodo);
				VM.ObtenerTurno(VM.Periodo.UidTurno);
				VM.ObtenerDepartamento(VM.Periodo.UidDepartamento);
				VM.ObtenerSucursal(VM.Departamento.UidSucursal);
				VM.ObtenerEmpresa(VM.Sucursal.UidEmpresa);
				VM.ObtenerEncargado(VM.Periodo.UidUsuario);
				VM.ObtenerTareasNuevasTurno(UidPeriodo, DtFechaTurno, IntFolioTurnoA);

				ProcessingData(VM.Cumplimientos);

				ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ListaTareas2.rdlc");
				ReportParameter[] param = new ReportParameter[9];
				string nombreCompleto = VM.Encargado.STRNOMBRE + " " + VM.Encargado.STRAPELLIDOPATERNO + " " + VM.Encargado.STRAPELLIDOMATERNO;
				param[0] = new ReportParameter("NombreEncargado", nombreCompleto);
				param[1] = new ReportParameter("NombreDepartamento", VM.Departamento.StrNombre);
				param[2] = new ReportParameter("Fecha", DateTime.Today.ToString("dd/MM/yyyy"));
				param[3] = new ReportParameter("NombreEmpresa", VM.Empresa.StrNombreComercial);
				param[4] = new ReportParameter("NombreSucursal", VM.Sucursal.StrNombre);
				param[5] = new ReportParameter("Turno", VM.Turno.StrTurno);
				param[6] = new ReportParameter("HoraInicio", StrHoraInicio);
				param[7] = new ReportParameter("HoraFin", StrHoraFin);
				param[8] = new ReportParameter("FolioTurno", IntFolioTurnoN.ToString("0000"));
				ReportViewer1.LocalReport.SetParameters(param);
				ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", VM.Cumplimientos));

				//ReportViewer1.LocalReport.SubreportProcessing += LocalReport_SubreportProcessing;
				ReportViewer1.LocalReport.Refresh();
			}
		}

		private List<Modelo.Cumplimiento> ProcessingData(List<Modelo.Cumplimiento> originalList)
		{
			List<Modelo.Cumplimiento> transformed = new List<Modelo.Cumplimiento>(originalList.Count);

			for (int i = 0; i < originalList.Count; i++)
			{
				originalList[i].StrValor = string.Empty;

				var current = originalList[i];
				if (string.IsNullOrEmpty(current.StrArea))
					originalList[i].StrArea = "General";

				if (current.StrTipoMedicion.Equals("Verdadero/Falso"))
				{
					originalList[i].StrValor = "SI             NO";
				}
				else if (current.StrTipoMedicion.Equals("Numerico"))
				{
					originalList[i].StrValor = current.StrTipoUnidad.ToUpper() + ":";
				}
				else if (current.StrTipoMedicion.Equals("Seleccionable"))
				{
					VM.ObtenerOpciones(current.UidTarea);
					string Opciones = "-";
					Opciones += string.Join("|-", VM.Opciones.Select(o => o.StrOpciones));
					originalList[i].StrValor = Opciones;
				}

			}

			return transformed;
		}
	}
}