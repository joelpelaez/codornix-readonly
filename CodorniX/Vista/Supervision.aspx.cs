﻿using System;
using System.Globalization;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class Supervision : Page
	{
		private readonly VMSupervision VM = new VMSupervision();

		private DateTime DtFechaInicioTurnoSeleccionado
		{
			get
			{
				return (DateTime)ViewState["DtFechaTurnoEnc"];
			}
			set
			{
				ViewState["DtFechaTurnoEnc"] = value;
			}
		}
		private Sesion SesionActual => (Sesion)Session["Sesion"];
		private DateTime DtToday
		{
			get
			{
				return (DateTime)ViewState["FechaActual"];
			}
			set
			{
				ViewState["FechaActual"] = value;
			}
		}

		private Guid UidPeriodoTurno
		{
			get
			{
				return (Guid)ViewState["tPeriodo"];
			}
			set
			{
				ViewState["tPeriodo"] = value;
			}
		}
		private Guid UidDepartamentoTurno
		{
			get
			{
				return (Guid)ViewState["tDepartamento"];
			}
			set
			{
				ViewState["tDepartamento"] = value;
			}
		}
		private Guid UidUsuarioTurno
		{
			get
			{
				return (Guid)ViewState["tUsuario"];
			}
			set
			{
				ViewState["tUsuario"] = value;
			}
		}
		private DateTime DtFechaTurno
		{
			get
			{
				return (DateTime)ViewState["tFechaTurno"];
			}
			set
			{
				ViewState["tFechaTurno"] = value;
			}
		}
		private int IntFolioTurno
		{
			get
			{
				return (int)ViewState["tFolioTurno"];
			}
			set
			{
				ViewState["tFolioTurno"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("Bienvenido", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!SesionActual.uidTurnoSupervisor.HasValue)
			{
				Response.Redirect("InicioSupervisor.aspx", false);
				return;
			}

			if (!IsPostBack)
			{
				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				this.DtToday = horaLocal.DateTime;

				this.DtFechaInicioTurnoSeleccionado = horaLocal.DateTime;

				divCerrarturno.Visible = false;
				dvgDepartamentos.Visible = false;
				dvgDepartamentos.DataSource = null;
				dvgDepartamentos.DataBind();
				PanelCompletadas.Visible = false;
				PanelNoCompletadas.Visible = false;
				panelRequeridas.Visible = false;
				btnInicioTurno.Disable();
				btnCerrarTurno.Disable();
				btnActualizar_Click(sender, e);
			}
		}

		protected void DVGResumenTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
		}

		protected void DVGResumenTareas_Sorting(object sender, GridViewSortEventArgs e)
		{
		}

		protected void dvgDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				cbCumplimientoPosterior.Checked = true;
				lblAceptarCerrarTurno.Text = string.Empty;
				lblnoiniciarturno.Text = string.Empty;
				var date = Convert.ToDateTime(dvgDepartamentos.SelectedDataKey.Values[2].ToString());
				var usuario = new Guid(dvgDepartamentos.SelectedDataKey.Values[1].ToString());
				var content = dvgDepartamentos.SelectedDataKey.Values[3]?.ToString();

				VM.ObtenerDepartamento(new Guid(dvgDepartamentos.SelectedDataKey.Value.ToString()), usuario, date,
					SesionActual.uidSucursalActual.Value);

				if (VM.CTareasNoCumplidas == null)
				{
					btnIniciarTurno.Disable();
					btnInicioTurno.Disable();
					btnBloquearTurno.Disable();
					btnCerrarTurno.Disable();
					lblmensaje.Visible = true;
					lblmensaje.Text = "Error al obtener datos";
				}

				txtUidDepartamento.Text = VM.CTareasNoCumplidas.UidDepartamento.ToString();
				txtUidArea.Text = VM.CTareasNoCumplidas.UidArea.ToString();
				var uidDepartamento = VM.CTareasNoCumplidas.UidDepartamento;
				lblDepartamento.Text = VM.CTareasNoCumplidas.StrDepartamento.ToString();
				lblTurno.Text = VM.CTareasNoCumplidas.StrTurno;
				lblCumplidas.Text = VM.CTareasNoCumplidas.IntTareasCumplidas.ToString();
				lblContadorC.Text = VM.CTareasNoCumplidas.IntTareasCumplidas.ToString();
				lblNoCumplidas.Text = VM.CTareasNoCumplidas.IntTareasNoCumplidas.ToString();
				lblContadorNC.Text = VM.CTareasNoCumplidas.IntTareasNoCumplidas.ToString();
				lblRequeridasNoCumplidas.Text = VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas.ToString();
				lblContadorRNC.Text = VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas.ToString();
				//SesionActual.UidPeriodo = VM.CTareasNoCumplidas.UidPeriodo;
				fldUidPeriodo.Value = VM.CTareasNoCumplidas.UidPeriodo.ToString();

				if (!string.IsNullOrEmpty(content))
				{
					fldUidInicioTurno.Value = content;
					VM.ObtenerTurno(new Guid(fldUidInicioTurno.Value));
					btnCerrarTurno.Enable();
				}
				else
				{
					fldUidInicioTurno.Value = string.Empty;
					btnCerrarTurno.Disable();
				}

				fldUidUsuario.Value = usuario.ToString();

				if (VM.InicioTurno != null)
				{
					this.IntFolioTurno = VM.InicioTurno.IntFolio;
					this.UidPeriodoTurno = VM.InicioTurno.UidPeriodo.Value;
					this.DtFechaInicioTurnoSeleccionado = VM.InicioTurno.DtFechaHoraInicio.DateTime;
					lblFechaInicio.Text = VM.InicioTurno.DtFechaHoraInicio.ToString("dd/MM/yyyy");
					lblHoraInicio.Text = VM.InicioTurno.DtFechaHoraInicio.ToString("hh\\:mm tt");

					lblFechaFin.Text = VM.InicioTurno.DtFechaHoraFin.HasValue
						? VM.InicioTurno.DtFechaHoraFin.Value.ToString("dd/MM/yyyy")
						: "";
					lblHoraFin.Text = VM.InicioTurno.DtFechaHoraFin.HasValue
						? VM.InicioTurno.DtFechaHoraFin.Value.ToString("hh\\:mm tt")
						: "";
					if (lblHoraFin.Text.Length == 0)
						lblEstadoDelTurno.Text = "Abierto";
					else
						lblEstadoDelTurno.Text = "Cerrado";
				}
				else
				{
					lblHoraInicio.Text = string.Empty;
					lblHoraFin.Text = string.Empty;
					if (lblHoraInicio.Text.Length == 0 && lblHoraFin.Text.Length == 0) lblEstadoDelTurno.Text = "No creado";
				}

				this.UidDepartamentoTurno = uidDepartamento;
				this.UidUsuarioTurno = usuario;
				this.DtFechaTurno = date;

				VM.ObtenerTareasCumplidas(uidDepartamento, usuario, date);
				DVGTareasCumplidas.DataSource = VM.ltsTareasCumplidas;
				DVGTareasCumplidas.DataBind();

				VM.ObtenerTareasNoCumplidas(uidDepartamento, usuario, date, SesionActual.uidSucursalActual.Value);
				DvgTareasNoCumplidas.DataSource = VM.ltsTareasNoCumplidas;
				DvgTareasNoCumplidas.DataBind();

				VM.ObtenerTareasRequeridas(uidDepartamento, usuario, date, SesionActual.uidSucursalActual.Value);
				dgvTareasRequeridas.DataSource = VM.TareasRequeridas;
				dgvTareasRequeridas.DataBind();


				btnIniciarTurno.Disable();
				btnInicioTurno.Disable();
				btnCerrarTurno.Disable();
				btnBloquearTurno.Disable();

				cbCumplimientoPosterior.Enabled = false;
				if (VM.InicioTurno != null)
				{
					VM.ObtenerEstadoTurno(VM.InicioTurno.UidEstadoTurno);
					if (VM.EstadoTurno == null)
					{
						btnIniciarTurno.Text = "Iniciar";
						btnIniciarTurno.Enable();
					}
					else
						switch (VM.EstadoTurno.StrEstadoTurno)
						{
							case "Abierto":
								cbCumplimientoPosterior.Enabled = true;
								btnInicioTurno.Text = "Tomar";
								btnInicioTurno.Enable();
								btnBloquearTurno.Enable();
								btnCerrarTurno.Enable();
								//SesionActual.LsPeriodosEncargado.Add(new PeriodoEncargadoSupervision() { UidPeriodo = ,DtFecha= });
								break;
							case "Abierto (Controlado)":
							case "Abierto por Supervisor":
								cbCumplimientoPosterior.Enabled = true;
								btnInicioTurno.Text = "Ceder";
								btnInicioTurno.Enable();
								btnBloquearTurno.Enable();
								btnCerrarTurno.Enable();
								break;
							case "Bloqueado":
								btnInicioTurno.Text = "Tomar";
								btnBloquearTurno.Text = "Desbloquear";
								btnBloquearTurno.Enable();
								break;
							case "Cerrado":
								cbCumplimientoPosterior.Checked = VM.InicioTurno.BlCumplimientoPospterior;
								if (!VM.InicioTurno.BlCumplimientoPospterior )
								{
									btnIniciarTurno.Text = "Reabrir";
									btnIniciarTurno.Enable();
								}
								break;
							default:
								btnIniciarTurno.Text = "Iniciar";
								btnIniciarTurno.Enable();
								break;
						}
					//if (!VM.InicioTurno._blAbiertoEncargado) { btnInicioTurno.Disable(); }
				}
				else
				{
					btnIniciarTurno.Text = "Iniciar";
					btnIniciarTurno.Enable();
				}


				Guid? UidInicioTurno = null;
				if (VM.InicioTurno != null)
					UidInicioTurno = VM.InicioTurno.UidInicioTurno;

				bool Result = VM.ValidarExistenciaTurnosAbiertosDepartamento(UidInicioTurno, date, VM.CTareasNoCumplidas.UidDepartamento);
				if (!Result)
				{
					btnIniciarTurno.Disable();
					btnCerrarTurno.Disable();
					btnBloquearTurno.Disable();
					btnInicioTurno.Disable();
					SesionActual.UidPeriodo = null;
					lblmensaje.Visible = true;
					lblmensaje.Text = "Turno pendiente por cerrar";
					SesionActual.dtFechaInicioTurnoSeleccionado = null;
					cbCumplimientoPosterior.Enabled = false;
				}


				if (ViewState["DepartamentoPreviousRow"] != null)
				{
					var pos = (int)ViewState["DepartamentoPreviousRow"];
					var previousRow = dvgDepartamentos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}

				ViewState["DepartamentoPreviousRow"] = dvgDepartamentos.SelectedIndex;
				dvgDepartamentos.SelectedRow.AddCssClass("success");
			}
			catch (Exception ex)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error al obtener datos";
			}
		}

		protected void dvgDepartamentos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgDepartamentos, "Select$" + e.Row.RowIndex);

				if (e.Row.Cells[6].Text != "&nbsp;")
				{
					DateTime DtFechaInicioTurno = Convert.ToDateTime(DateTime.ParseExact(e.Row.Cells[6].Text.Trim().Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					int AuxDt = DateTime.Compare(DtFechaInicioTurno, this.DtToday.Date);
					if (AuxDt < 0)
					{
						e.Row.CssClass = "border-warning";
					}
				}
			}
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			btnActualizar.Enabled = false;
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				this.DtToday = horaLocal.DateTime;


				VM.ObtenerCumplimiento(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario, horaLocal.DateTime);

				dvgDepartamentos.Visible = true;
				dvgDepartamentos.DataSource = VM.ltsDepartamento;
				dvgDepartamentos.DataBind();

				var master = (Site1)Page.Master;
				master.Noiniciarturno();
				master.NoTurno();
				btnCancelarCerrarTurno_Click(null, null);

				// Eliminar los elementos procesados con anterioridad para evitar accesos innecesarios.
				SesionActual.UidPeriodos?.Clear();

				// Procesar turnos para encontrar si existe alguna que tenga acceso el supervisor.
				foreach (var turno in VM.ltsDepartamento)
					if (turno.StrEstadoTurno.Contains("Abierto"))
						if (!SesionActual.UidPeriodos.Contains(turno.UidPeriodo))
							SesionActual.UidPeriodos.Add(turno.UidPeriodo);

				SesionActual.LsPeriodosEncargado = new System.Collections.Generic.List<PeriodoEncargadoSupervision>();
				foreach (var turno in VM.ltsDepartamento)
				{
					if (turno.StrEstadoTurno.Contains("Abierto"))
					{
						SesionActual.LsPeriodosEncargado.Add(new PeriodoEncargadoSupervision()
						{
							UidPeriodo = turno.UidPeriodo,
							UidDepartamento = turno.UidDepartamento,
							DtFecha = turno.DtFechaHoraInicio.Value.Date,
							IntFolioTurno = turno.IntFolio
						});
					}
				}

				ViewState["DepartamentoPreviousRow"] = null;
			}
			catch (Exception ex)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error al actualizar";
			}
			finally { btnActualizar.Enabled = true; }
		}

		protected void tabResumen_Click(object sender, EventArgs e)
		{
			PanelResumen.Visible = true;
			PanelCompletadas.Visible = false;
			PanelNoCompletadas.Visible = false;
			panelRequeridas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeResumen.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasCumplidas.Visible = false;
			divTareasNoCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void tabCompletadas_Click(object sender, EventArgs e)
		{
			PanelCompletadas.Visible = true;
			PanelResumen.Visible = false;
			PanelNoCompletadas.Visible = false;
			panelRequeridas.Visible = false;

			activeResumen.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasNoCumplidas.Visible = false;
			divTareasCumplidas.Visible = true;
			PanelBusqueda.Visible = true;
		}

		protected void tabNoCompletadas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = true;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			panelRequeridas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");

			divTareasNoCumplidas.Visible = true;
			divTareasCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void btnReporte_Click(object sender, EventArgs e)
		{
			var uidInicioTurno = default(Guid);
			var uidPeriodo = new Guid(fldUidPeriodo.Value);
			if (!string.IsNullOrEmpty(fldUidInicioTurno.Value))
			{
				uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ObtenerTurno(uidInicioTurno);
				VM.ObtenerEstadoTurno(VM.InicioTurno.UidEstadoTurno);
				if (VM.EstadoTurno.StrEstadoTurno == "Cerrado")
				{
					Session["Periodo"] = uidPeriodo;
					Session["Fecha"] = this.DtFechaInicioTurnoSeleccionado;
					Session["HoraInicio"] = lblHoraInicio.Text;
					Session["HoraFin"] = lblHoraFin.Text.Length == 0 ? "Sin Cerrar Turno" : lblHoraFin.Text;
					Session["Replica"] = "[Duplicado]";
					Session["FolioTurno"] = VM.InicioTurno.IntFolio;
					ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
						"window.open('ReporteTareas.aspx', '_blank')", true);
					return;
				}
			}

			lblError.Text = "El turno no se ha cerrado y no puede obtener su reporte.";
			panelAlert.Visible = true;
		}

		protected void btnAceptarCerrarTurno_Click(object sender, EventArgs e)
		{
			var master = (Site1)Page.Master;
			master.AceptarCerrarTurno();
		}

		public void btnCancelarCerrarTurno_Click(object sender, EventArgs e)
		{
			divCerrarturno.Visible = false;
			lblAceptarCerrarTurno.Text = string.Empty;
		}

		protected void tabRequeridas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = false;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			panelRequeridas.Visible = true;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "";
			activeRequeridas.AddCssClass("active");

			divTareasNoCumplidas.Visible = false;
			divTareasCumplidas.Visible = false;
			PanelBusqueda.Visible = true;
		}

		protected void btnCloseAlert_Click(object sender, EventArgs e)
		{
			panelAlert.Visible = false;
			lblError.Text = string.Empty;
		}

		protected void btnLista_Click(object sender, EventArgs e)
		{
			if (panelAlert.Visible)
				panelAlert.Visible = false;

			var uidInicioTurno = default(Guid);
			var uidPeriodo = new Guid(fldUidPeriodo.Value);
			if (!string.IsNullOrEmpty(fldUidInicioTurno.Value))
			{
				uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ObtenerTurno(uidInicioTurno);
				VM.ObtenerEstadoTurno(VM.InicioTurno.UidEstadoTurno);

				Session["Usuario"] = new Guid(fldUidUsuario.Value);
				Session["Periodo"] = uidPeriodo;
				Session["Fecha"] = DateTime.Today;
				Session["HoraInicio"] = lblHoraInicio.Text;
				Session["HoraFin"] = lblHoraFin.Text.Length == 0 ? "Sin Cerrar Turno" : lblHoraFin.Text;
				Session["FolioTurno"] = VM.InicioTurno.IntFolio;
				ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
					"window.open('ListaTareas.aspx', '_blank')", true);
			}
			else
			{
				panelAlert.Visible = true;
				lblError.Text = "Turno no iniciado.";
			}
		}

		protected void btnIniciarTurno_Click(object sender, EventArgs e)
		{
			string control = btnIniciarTurno.Text;
			var uidPeriodo = new Guid(fldUidPeriodo.Value);

			if (control.Equals("Iniciar"))
			{

				var uidUsuario = new Guid(fldUidUsuario.Value);

				if (string.IsNullOrEmpty(fldUidInicioTurno.Value))
				{
					VM.CrearTurno(uidPeriodo, SesionActual.uidUsuario, SesionActual.uidUsuario, SesionActual.GetDateTimeOffset(), "Abierto por Supervisor", false, true);
				}
				else
				{
					var uidInicioTurno = new Guid(fldUidInicioTurno.Value);
					//VM.ObtenerTurno(uidInicioTurno);
					VM.ModificarEstadoTurno(uidInicioTurno, "Abierto por Supervisor");
				}
			}
			else if (control.Equals("Reabrir"))
			{
				var uidInicioTurno = (fldUidInicioTurno.Value);
				VM.ReabrirTurno(new Guid(uidInicioTurno), "Abierto por Supervisor");
			}

			btnIniciarTurno.Disable();
			btnInicioTurno.Enable();
			btnCerrarTurno.Enable();
			btnBloquearTurno.Enable();
			cbCumplimientoPosterior.Enabled = true;
			// Administrar el turno
			SesionActual.UidPeriodos.Add(uidPeriodo);
			//btnInicioTurno.Disable();
			btnActualizar_Click(null, null);
		}
		protected void btnInicioTurno_Click(object sender, EventArgs e)
		{
			if (btnInicioTurno.Text == "Tomar")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);

				if (string.IsNullOrEmpty(fldUidInicioTurno.Value))
				{
					VM.CrearTurno(uidPeriodo, SesionActual.uidUsuario, SesionActual.uidUsuario, SesionActual.GetDateTimeOffset(),
						"Abierto (Controlado)", false, true);
				}
				else
				{
					var uidInicioTurno = new Guid(fldUidInicioTurno.Value);
					VM.ObtenerTurno(uidInicioTurno);
					VM.ModificarEstadoTurno(uidInicioTurno, "Abierto (Controlado)");
				}

				SesionActual.UidPeriodos.Add(uidPeriodo);
				btnInicioTurno.Text = "Ceder";
			}
			else if (btnInicioTurno.Text == "Ceder")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);
				var uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ModificarEstadoTurno(uidInicioTurno, "Abierto");

				SesionActual.UidPeriodos.Remove(uidPeriodo);

				btnInicioTurno.Text = "Tomar";
			}

			cbCumplimientoPosterior.Enabled = true;
			btnIniciarTurno.Disable();
			btnInicioTurno.Enable();
			btnCerrarTurno.Enable();
			btnBloquearTurno.Enable();
			btnActualizar_Click(sender, e);
			var site = (Site1)Master;
			site.UpdateNavbar();
		}
		protected void btnBloquearTurno_Click(object sender, EventArgs e)
		{
			if (btnBloquearTurno.Text == "Bloquear")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);

				if (string.IsNullOrEmpty(fldUidInicioTurno.Value))
				{
					VM.CrearTurno(uidPeriodo, SesionActual.uidUsuario, SesionActual.uidUsuario, SesionActual.GetDateTimeOffset(),
						"Bloqueado", false, true);
				}
				else
				{
					var uidInicioTurno = new Guid(fldUidInicioTurno.Value);
					VM.ObtenerTurno(uidInicioTurno);
					VM.ModificarEstadoTurno(uidInicioTurno, "Bloqueado");
				}

				btnIniciarTurno.Disable();
				btnInicioTurno.Disable();
				btnCerrarTurno.Disable();
				btnBloquearTurno.Enable();
				cbCumplimientoPosterior.Enabled = false;
				SesionActual.UidPeriodos.Add(uidPeriodo);
				btnBloquearTurno.Text = "Desbloquear";
			}
			else if (btnBloquearTurno.Text == "Desbloquear")
			{
				var uidPeriodo = new Guid(fldUidPeriodo.Value);
				var uidUsuario = new Guid(fldUidUsuario.Value);
				var uidInicioTurno = new Guid(fldUidInicioTurno.Value);

				VM.ModificarEstadoTurno(uidInicioTurno, "Abierto (Controlado)");

				SesionActual.UidPeriodos.Remove(uidPeriodo);

				btnBloquearTurno.Text = "Bloquear";
				btnIniciarTurno.Disable();
				btnInicioTurno.Enable();
				btnCerrarTurno.Enable();
				btnBloquearTurno.Enable();
				cbCumplimientoPosterior.Enabled = true;
			}


			btnActualizar_Click(sender, e);
			var site = (Site1)Master;
			site.UpdateNavbar();
		}
		protected void btnCerrarTurno_Click(object sender, EventArgs e)
		{
			var uidPeriodo = new Guid(fldUidPeriodo.Value);
			var uidInicioTurno = new Guid(fldUidInicioTurno.Value);
			bool BlCumplimientoPosterior = cbCumplimientoPosterior.Checked;

			VM.CerrarTurno(uidInicioTurno, SesionActual.GetDateTimeOffset(), BlCumplimientoPosterior);

			SesionActual.UidPeriodos.Remove(uidPeriodo);

			if (BlCumplimientoPosterior)
			{
				VM.GenerarCumplimientoPosterior(this.DtFechaTurno, this.UidPeriodoTurno, this.UidUsuarioTurno, this.IntFolioTurno);
			}

			btnIniciarTurno.Enable();
			btnInicioTurno.Disable();
			btnCerrarTurno.Disable();
			btnBloquearTurno.Disable();
			btnIniciarTurno.Text = "Reabrir";
			btnIniciarTurno.Enable();
			cbCumplimientoPosterior.Enabled = false;
			btnActualizar_Click(sender, e);
		}
		/*Iniciado por supervisor, Tomar, Bloquear*/
	}
}