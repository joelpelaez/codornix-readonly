﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Calificaciones.aspx.cs" Inherits="CodorniX.Vista.Calificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Lista de calificaciones
				</div>
				<div class="panel-body panel-pd">
					<%--Botones de busqueda--%>
					<div class="col-lg-12 pd-left-right-5">
						<table style="width: 100%;">
							<tr>
								<td>
									<div class="btn-group">
										<asp:LinkButton runat="server" ID="btnConfiguracion" CssClass="btn btn-sm btn-default" OnClick="btnConfiguracion_Click">
											<span class="glyphicon glyphicon-cog"></span>
											Configuracion
										</asp:LinkButton>
										<asp:LinkButton runat="server" ID="btnEditarConfiguracion" CssClass="btn btn-sm btn-default" OnClick="btnEditarConfiguracion_Click">
											<span class="glyphicon glyphicon-pencil"></span>
											Editar
										</asp:LinkButton>
										<asp:LinkButton runat="server" ID="btnOkConfiguracion" CssClass="btn btn-sm btn-success" OnClick="btnOkConfiguracion_Click">
											<span class="glyphicon glyphicon-ok"></span>										
										</asp:LinkButton>
										<asp:LinkButton runat="server" ID="btnCancelConfiguracion" CssClass="btn btn-sm btn-danger" OnClick="btnCancelConfiguracion_Click">
											<span class="glyphicon glyphicon-remove"></span>										
										</asp:LinkButton>
									</div>
								</td>
								<td class="text-right">
									<div class="btn-group">
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnOcultarFiltros" OnClick="btnOcultarFiltros_Click">											
											<asp:Label Text="Ocultar" runat="server" ID="lblOcultarFiltros" />
										</asp:LinkButton>
										<asp:LinkButton Text="text" runat="server" CssClass="btn btn-sm btn-default" ID="btnLimpiarFiltros" OnClick="btnLimpiarFiltros_Click">
											<span class="glyphicon glyphicon-trash"></span>
											Limpiar
										</asp:LinkButton>
										<asp:LinkButton Text="text" runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscar" OnClick="btnBuscar_Click">
											<span class="glyphicon glyphicon-search"></span>
											Buscar
										</asp:LinkButton>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<%-- Mensajes --%>
					<div class="col-lg-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlMessageBusqueda" CssClass="alert alert-info">
							<asp:Label Text="Error" runat="server" ID="lblMessageBusqueda" />
							<asp:LinkButton OnClick="CloseMessageBusqueda" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</asp:Panel>
					</div>
					<%--Configuracion--%>
					<div class="col-lg-12 pd-0">
						<asp:Panel runat="server" ID="pnlConfiguracion">
							<div class="col-md-12 pd-left-right-5">
								<small>Realizar revision de tareas por:</small>
								<asp:DropDownList runat="server" CssClass="form-control" ID="ddlRevisionType">
									<asp:ListItem Text="Valor Cualitativo" Value="Number" />
									<asp:ListItem Text="Valor Cuantitativo" Value="Text" />									
								</asp:DropDownList>
							</div>
						</asp:Panel>
					</div>
					<%--Filtros de busqueda--%>
					<div class="col-lg-12 pd-0">
						<asp:Panel runat="server" ID="pnlFiltrosBusqueda">
							<div class="col-md-8 pd-left-right-5">
								<small>Calificación</small>
								<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroCalificacion" />
							</div>
							<div class="col-xs-6 col-md-2 pd-left-right-5">
								<small>Valor</small>
								<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroValor" ToolTip="El valor cuantitativo de la calificación." />
							</div>
							<div class="col-xs-6 col-md-2 pd-left-right-5">
								<small>Estatus</small>
								<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlFiltroEstatus">
									<asp:ListItem Text="Todos" Value="Empty" />
									<asp:ListItem Text="Activo" Value="True" />
									<asp:ListItem Text="Inactivo" Value="False" />
								</asp:DropDownList>
							</div>
						</asp:Panel>
					</div>
					<%--Resultados de busqueda--%>
					<div class="col-lg-12 pd-left-right-5">
						<asp:GridView runat="server" ID="gvCalificaciones" DataKeyNames="UidCalificacion" OnRowDataBound="gvCalificaciones_RowDataBound" OnSelectedIndexChanging="gvCalificaciones_SelectedIndexChanging" OnSelectedIndexChanged="gvCalificaciones_SelectedIndexChanged" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvCalificaciones_PageIndexChanging" AllowSorting="true" OnSorting="gvCalificaciones_Sorting" CssClass="table table-bordered table-hover table-condensed input-sm" AutoGenerateColumns="false" SelectedRowStyle-BackColor="#dff0d8">
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
								<asp:BoundField DataField="StrCalificacion" HeaderText="Calificación" SortExpression="Calificacion" />
								<asp:BoundField DataField="FlValor" HeaderText="Valor" SortExpression="Valor" />
								<asp:BoundField DataField="IntOrden" HeaderText="Orden" SortExpression="Orden" />
								<asp:TemplateField HeaderText="Estatus" SortExpression="Estatus">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblEstatusCalificacion" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField DataField="BlEstatus" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
							</Columns>
							<EmptyDataTemplate>
								No se encontraron registros
							</EmptyDataTemplate>
						</asp:GridView>
					</div>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Datos generales
				</div>
				<div class="panel-body panel-pd">
					<%--Botones--%>
					<div class="col-lg-12 pd-left-right-5">
						<div class="btn-group">
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnNuevo" OnClick="btnNuevo_Click">
								<span class="glyphicon glyphicon-plus"></span>
								Nuevo
							</asp:LinkButton>
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnEditar" OnClick="btnEditar_Click">
								<span class="glyphicon glyphicon-pencil"></span>
								Editar
							</asp:LinkButton>
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-success" ID="btnOk" OnClick="btnOk_Click">
								<span class="glyphicon glyphicon-ok"></span>
							</asp:LinkButton>
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" ID="btnCancel" OnClick="btnCancel_Click">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</div>
						<asp:HiddenField runat="server" ID="hfUidCalificacion" />
					</div>
					<%-- Mensajes --%>
					<div class="col-lg-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlMessageGeneral" CssClass="alert alert-info">
							<asp:Label Text="Error" runat="server" ID="lblMessageGeneral" />
							<asp:LinkButton OnClick="CloseMessageGeneral" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</asp:Panel>
					</div>
					<%-- Campos --%>
					<div class="col-lg-12 pd-0">
						<div class="col-md-6 pd-left-right-5">
							<small>Calificación</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtCalificacion" ToolTip="El valor cualitativo de la califcación." />
						</div>
						<div class="col-xs-6 col-md-2 pd-left-right-5">
							<small>Valor</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtValor" ToolTip="El valor cuantitativo de la calificación." />
						</div>
						<div class="col-xs-6 col-md-2 pd-left-right-5">
							<small>Orden</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtOrden" ToolTip="Orden en el que mostrara en la revision." />
						</div>
						<div class="col-md-2 pd-left-right-5">
							<small>Estatus</small>
							<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlEstatus">
								<asp:ListItem Text="Activo" Value="True" />
								<asp:ListItem Text="Inactivo" Value="False" />
							</asp:DropDownList>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
