﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class UnidadesDeMedida : System.Web.UI.Page
	{
		#region Properties
		private VMUnidadMedida VmUnidad = new VMUnidadMedida();
		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}
		private List<UnidadMedida> LsUnidades
		{
			get
			{
				return (List<UnidadMedida>)ViewState["UnidadesMedida"];
			}
			set
			{
				ViewState["UnidadesMedida"] = value;
			}
		}

		private string FUnidadMedida
		{
			get
			{
				return (string)ViewState["FUnidad"];
			}
			set
			{
				ViewState["FUnidad"] = value;
			}
		}
		private bool? FBlEstatus
		{
			get
			{
				return (bool?)ViewState["FEstatus"];
			}
			set
			{
				ViewState["FEstatus"] = value;
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{
				Response.Redirect("Login.aspx");
			}

			if (!IsPostBack)
			{
				this.FUnidadMedida = string.Empty;
				this.FBlEstatus = null;

				btnEdit.CssClass = "btn btn-sm btn-default disabled";
				btnOk.Visible = false;
				btnCancel.Visible = false;

				HideFilters("Ocultar");

				VmUnidad.Search(SesionActual.uidEmpresaActual.Value, this.FUnidadMedida, this.FBlEstatus);
				this.LsUnidades = VmUnidad.LsUnidades;
				this.gvUnidadesMedida.DataSource = VmUnidad.LsUnidades;
				gvUnidadesMedida.DataBind();

				pnlAlertGeneral.Visible = false;
				txtUnidadMedida.Enabled = false;
				ddlEstatus.Enabled = false;
			}
		}

		#region Left
		#region Buttons
		protected void btnMostrarFiltros_Click(object sender, EventArgs e)
		{
			HideFilters(lblMostrarFiltros.Text);
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroUnidadMedida.Text = string.Empty;
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			string Nombre = txtFiltroUnidadMedida.Text.Trim();

			VmUnidad.Search(SesionActual.uidEmpresaActual.Value, this.FUnidadMedida, this.FBlEstatus);
			this.LsUnidades = VmUnidad.LsUnidades;
			this.gvUnidadesMedida.DataSource = VmUnidad.LsUnidades;
			gvUnidadesMedida.DataBind();

			HideFilters("Ocultar");
		}
		private void HideFilters(string IsHide)
		{
			if (IsHide.Equals("Mostrar"))
			{
				lblMostrarFiltros.Text = "Ocultar";
				pnlFiltros.Visible = true;
				pnlListaUnidades.Visible = false;
			}
			else
			{
				lblMostrarFiltros.Text = "Mostrar";
				pnlFiltros.Visible = false;
				pnlListaUnidades.Visible = true;
			}
		}
		#endregion

		#region GridView
		protected void gvUnidadesMedida_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvUnidadesMedida, "Select$" + e.Row.RowIndex);

				Label lblIcon = e.Row.FindControl("lblEstatusIcon") as Label;
				if (e.Row.Cells[3].Text.Equals("True"))
				{
					lblIcon.ToolTip = "Activo";
					lblIcon.CssClass = "glyphicon glyphicon-ok";
				}
				else
				{
					lblIcon.ToolTip = "Inactivo";
					lblIcon.CssClass = "glyphicon glyphicon-remove";
				}
			}
		}
		protected void gvUnidadesMedida_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvUnidadesMedida.DataSource = this.LsUnidades;
			gvUnidadesMedida.PageIndex = e.NewPageIndex;
			gvUnidadesMedida.DataBind();
		}
		protected void gvUnidadesMedida_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (btnOk.Visible)
				e.Cancel = true;
		}
		protected void gvUnidadesMedida_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidUnidadMedida = new Guid(gvUnidadesMedida.SelectedDataKey.Value.ToString());
				btnEdit.CssClass = "btn btn-sm btn-default";
				VmUnidad.Datos(UidUnidadMedida);
				hfUidUnidadMedida.Value = UidUnidadMedida.ToString();
				txtUnidadMedida.Text = VmUnidad.umUnidadMedida.StrTipoUnidad;
				ddlEstatus.SelectedValue = VmUnidad.umUnidadMedida.BlEstatus ? "True" : "False";
			}
			catch (Exception ex)
			{
				DisplayAlertGeneral("Ocurrio un error <br/> " + ex.Message, "danger");
			}
		}
		#endregion
		#endregion

		#region Right
		#region Buttons
		protected void btnNew_Click(object sender, EventArgs e)
		{
			btnNew.CssClass = "btn btn-sm btn-default disabled";
			btnEdit.CssClass = "btn btn-sm btn-default disabled";
			btnOk.Visible = true;
			btnCancel.Visible = true;

			hfUidUnidadMedida.Value = string.Empty;

			txtUnidadMedida.Enabled = true;
			txtUnidadMedida.Text = string.Empty;
			ddlEstatus.Enabled = true;
			ddlEstatus.SelectedIndex = 0;
		}
		protected void btnEdit_Click(object sender, EventArgs e)
		{
			txtUnidadMedida.Enabled = true;
			ddlEstatus.Enabled = true;

			btnNew.CssClass = "btn btn-sm btn-default disabled";
			btnEdit.CssClass = "btn btn-sm btn-default disabled";
			btnOk.Visible = true;
			btnCancel.Visible = true;
		}
		protected void btnOk_Click(object sender, EventArgs e)
		{
			if (txtUnidadMedida.Text.Trim() == string.Empty)
			{
				DisplayAlertGeneral("El campo Unidad de medida es <strong>obligatorio</strong>", "warning");
				return;
			}

			string UnidadMedida = txtUnidadMedida.Text.Trim();
			bool BlEstatus = ddlEstatus.SelectedValue.Equals("True") ? true : false;

			if (hfUidUnidadMedida.Value == string.Empty)
			{
				if (VmUnidad.Agregar(SesionActual.uidEmpresaActual.Value, UnidadMedida, BlEstatus))
				{
					DisplayAlertGeneral("Guardado correctamente,", "success");
				}
				else
				{
					DisplayAlertGeneral("Ocurrio un error al guardar la informacion,", "danger");
				}
			}
			else
			{
				Guid UidUnidadMedida = new Guid(hfUidUnidadMedida.Value);
				if (VmUnidad.Actualizar(UidUnidadMedida, UnidadMedida, BlEstatus))
				{
					DisplayAlertGeneral("Actualizado correctamente,", "success");
				}
				else
				{
					DisplayAlertGeneral("Ocurrio un error al actualizar la informacion,", "danger");
				}
			}

			VmUnidad.Search(SesionActual.uidEmpresaActual.Value, this.FUnidadMedida, this.FBlEstatus);
			gvUnidadesMedida.SelectedIndex = -1;
			gvUnidadesMedida.DataSource = VmUnidad.LsUnidades;
			gvUnidadesMedida.DataBind();

			btnCancel_Click(null, null);
		}
		protected void btnCancel_Click(object sender, EventArgs e)
		{
			btnNew.CssClass = "btn btn-sm btn-default";
			btnOk.Visible = false;
			btnCancel.Visible = false;

			txtUnidadMedida.Enabled = false;
			ddlEstatus.Enabled = false;

			if (hfUidUnidadMedida.Value == string.Empty)
			{
				txtUnidadMedida.Text = string.Empty;
			}
			else
			{
				btnEdit.CssClass = "btn btn-sm btn-default";
			}
		}
		#endregion
		#endregion

		#region Complements
		private void DisplayAlertGeneral(string Message, string Type)
		{
			pnlAlertGeneral.CssClass = "alert alert-" + Type;
			lblAlertGeneral.Text = Message;
			pnlAlertGeneral.Visible = true;
		}
		protected void HideAlertGeneral(object sender, EventArgs e)
		{
			pnlAlertGeneral.Visible = false;
		}
		#endregion


	}
}