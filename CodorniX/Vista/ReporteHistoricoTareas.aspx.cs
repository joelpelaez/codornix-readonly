﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Modelo;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class ReporteHistoricoTareas : System.Web.UI.Page
	{
		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}
		private VmReporteHistoricoTareas VmHistorico = new VmReporteHistoricoTareas();
		private Guid UidTareaSeleccionada
		{
			get
			{
				return (Guid)ViewState["TareaSeleccionada"];
			}
			set
			{
				ViewState["TareaSeleccionada"] = value;
			}
		}
		private List<Tarea> LsTareas
		{
			get
			{
				return (List<Tarea>)ViewState["Tareas"];
			}
			set
			{
				ViewState["Tareas"] = value;
			}
		}
		private List<Modelo.Cumplimiento> LsCumplimientos
		{
			get
			{
				return (List<Modelo.Cumplimiento>)ViewState["Cumplimientos"];
			}
			set
			{
				ViewState["Cumplimientos"] = value;
			}
		}


		private int? BFolioCumplimiento
		{
			get
			{
				return ViewState["BFolio"] == null ? (int?)null : (int)ViewState["BFolio"];
			}
			set
			{
				ViewState["BFolio"] = value;
			}
		}
		private DateTime? BDtFechaInicio
		{
			get
			{
				return ViewState["BFInicio"] == null ? (DateTime?)null : (DateTime)ViewState["BFInicio"];
			}
			set
			{
				ViewState["BFInicio"] = value;
			}
		}
		private DateTime? BDtFechaFin
		{
			get
			{
				return ViewState["BFFin"] == null ? (DateTime?)null : (DateTime)ViewState["BFFin"];
			}
			set
			{
				ViewState["BFFin"] = value;
			}
		}
		private Guid BUidEstatus
		{
			get
			{
				return ViewState["BEstatus"] == null ? Guid.Empty : (Guid)ViewState["BEstatus"];
			}
			set
			{
				ViewState["BEstatus"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{

			}

			if (SesionActual.uidSucursalActual == null)
			{
				btnPrintReport.CssClass = "btn btn-sm btn-default disabled";
				pnlAlertBusqueda.Visible = false;
				pnlAlertGeneral.Visible = false;
				pnlBotonesBusqueda.Visible = false;
				DisplayAlertBusqueda("Error: selecione una sucursal.", "danger");
				return;
			}

			if (!IsPostBack)
			{
				UidTareaSeleccionada = Guid.Empty;

				btnPrintReport.CssClass = "btn btn-sm btn-default disabled";
				pnlAlertBusqueda.Visible = false;
				pnlAlertGeneral.Visible = false;
				pnlDetalleCumplimiento.Visible = false;
				MostrarFiltros("Ocultar");
				MostrarFiltrosCumplimiento("Ocultar");

				Guid UidSucursal = SesionActual.uidSucursalActual.Value;

				VmHistorico.GetDepartamentosSucursal(UidSucursal);
				lbFiltroDepartamento.DataSource = VmHistorico.LsDepartamentos;
				lbFiltroDepartamento.DataTextField = "StrNombre";
				lbFiltroDepartamento.DataValueField = "UidDepartamento";
				lbFiltroDepartamento.DataBind();

				VmHistorico.GetAreasSucursal(UidSucursal);
				lbFiltroArea.DataSource = VmHistorico.LsAreas;
				lbFiltroArea.DataTextField = "StrNombre";
				lbFiltroArea.DataValueField = "UidArea";
				lbFiltroArea.DataBind();

				VmHistorico.ObtenerEstadosCumplimiento();
				ddlFiltroEstadoCumplimiento.DataSource = VmHistorico.LsEstadosCumplimiento;
				ddlFiltroEstadoCumplimiento.DataTextField = "StrEstadoCumplimiento";
				ddlFiltroEstadoCumplimiento.DataValueField = "UidEstadoCumplimiento";
				ddlFiltroEstadoCumplimiento.DataBind();

				try
				{
					VmHistorico.BusquedaTareas(string.Empty, null, null, SesionActual.uidSucursalActual.Value, string.Empty, string.Empty, -1);
					this.LsTareas = VmHistorico.LsTareas;
					gvListaTareas.DataSource = VmHistorico.LsTareas;
					gvListaTareas.DataBind();
				}
				catch (Exception ex)
				{
					DisplayAlertBusqueda("Error: " + ex.Message, "warning");
				}

				liDatosGenerales.Attributes.Add("class", "active");
				liCumplimientos.Attributes.Add("class", "");

				pnlDatosGeneralesTarea.Visible = true;
				pnlCumplimientosTarea.Visible = false;
			}
		}

		#region Left
		#region Buttons
		protected void btnMostrarFiltros_Click(object sender, EventArgs e)
		{
			MostrarFiltros(lblMostrarFiltros.Text);
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroFolioTarea.Text = string.Empty;
			txtFiltroNombre.Text = string.Empty;
			txtFiltroFechaInicio.Text = string.Empty;
			txtFiltroFechaFin.Text = string.Empty;
			lbFiltroDepartamento.ClearSelection();
			lbFiltroArea.ClearSelection();
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			string Nombre = txtFiltroNombre.Text.Trim();
			int Folio = -1;
			DateTime? DtFechaInicio = null;
			DateTime? DtFechaFin = null;
			Guid UidSucursal = SesionActual.uidSucursalActual.Value;
			string Departamentos = string.Empty;
			string Areas = string.Empty;

			if (!string.IsNullOrEmpty(txtFiltroFolioTarea.Text.Trim()))
			{
				int Aux = 0;
				if (int.TryParse(txtFiltroFolioTarea.Text.Trim(), out Aux))
				{
					if (Aux >= 0)
					{
						Folio = Aux;
					}
					else
					{
						DisplayAlertBusqueda("Ingrese un folio mayor a 0.", "warning");
						return;
					}
				}
				else
				{
					DisplayAlertBusqueda("Ingrese un folio numerico.", "danger");
					return;
				}
			}
			if (!string.IsNullOrEmpty(txtFiltroFechaInicio.Text.Trim()))
			{
				try
				{
					DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayAlertBusqueda("Formato de fecha de (inicio) invalido.", "Warning");
					return;
				}
			}
			if (!string.IsNullOrEmpty(txtFiltroFechaFin.Text.Trim()))
			{
				try
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayAlertBusqueda("Formato de fecha (fin) invalido.", "Warning");
					return;
				}
			}

			Departamentos = GetSelectedValuesListBox(lbFiltroDepartamento);

			Areas = GetSelectedValuesListBox(lbFiltroArea);

			try
			{
				VmHistorico.BusquedaTareas(Nombre, DtFechaInicio, DtFechaFin, UidSucursal, Departamentos, Areas, Folio);
				this.LsTareas = VmHistorico.LsTareas;
				gvListaTareas.SelectedIndex = -1;
				gvListaTareas.DataSource = VmHistorico.LsTareas;
				gvListaTareas.DataBind();
				MostrarFiltros("Ocultar");
			}
			catch (Exception ex)
			{
				DisplayAlertBusqueda("Error: " + ex.Message, "warning");
			}
		}
		private void MostrarFiltros(string IsOpen)
		{
			if (IsOpen.Equals("Mostrar"))
			{
				pnlFiltros.Visible = true;
				pnlListaTareas.Visible = false;
				lblMostrarFiltros.Text = "Ocultar";
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";
			}
			else
			{
				pnlFiltros.Visible = false;
				pnlListaTareas.Visible = true;
				lblMostrarFiltros.Text = "Mostrar";
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
			}
		}


		protected void btnCerrarDetalleCumplimiento_Click(object sender, EventArgs e)
		{
			pnlDetalleCumplimiento.Visible = false;
			pnlBotonesBusqueda.Visible = true;

			if (lblMostrarFiltros.Text.Equals("Mostrar"))
				MostrarFiltros("Ocultar");
			else
				MostrarFiltros("Mostrar");
		}
		#endregion
		#region GridView
		protected void gvListaTareas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvListaTareas, "Select$" + e.Row.RowIndex);

				Label lblName = e.Row.FindControl("lblTaskName") as Label;
				if (lblName.Text.ToString().Length > 30)
				{
					lblName.ToolTip = lblName.Text;
					lblName.Text = lblName.Text.Substring(0, 27) + "...";
				}

				Label lblDepto = e.Row.FindControl("lblTaskDepto") as Label;
				if (lblDepto.Text.Length >= 13)
				{
					lblDepto.ToolTip = lblDepto.Text;
					lblDepto.Text = lblDepto.Text.Substring(0, 10) + "...";
				}

				Label lblArea = e.Row.FindControl("lblTaskArea") as Label;
				if (lblArea.Text.Length >= 13)
				{
					lblArea.ToolTip = lblArea.Text;
					lblArea.Text = lblArea.Text.Substring(0, 10) + "...";
				}

				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					//PERFIL.CssClass = "glyphicon glyphicon-user";
					e.Row.Cells[5].Text = "(Sin Asignar)";
				}
				if (e.Row.Cells[3].Text.Contains(","))
				{
					string split = e.Row.Cells[3].Text.Split(',')[0];
					e.Row.Cells[3].ToolTip = Server.HtmlDecode(e.Row.Cells[3].Text);
					e.Row.Cells[3].Text = split + ", ...";
				}
				if (e.Row.Cells[5].Text.Contains(","))
				{
					string split = e.Row.Cells[5].Text.Split(',')[0];
					e.Row.Cells[5].ToolTip = Server.HtmlDecode(e.Row.Cells[5].Text);
					e.Row.Cells[5].Text = split + ", ...";
				}
			}
		}
		protected void gvListaTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvListaTareas.SelectedIndex = -1;
			gvListaTareas.DataSource = this.LsTareas;
			gvListaTareas.PageIndex = e.NewPageIndex;
			gvListaTareas.DataBind();
		}
		protected void gvListaTareas_Sorting(object sender, GridViewSortEventArgs e)
		{
			string Direction = hfSortDirectionTareas.Value;
			if (Direction.Equals("ASC"))
				hfSortDirectionTareas.Value = "DESC";
			else
				hfSortDirectionTareas.Value = "ASC";

			switch (e.SortExpression)
			{
				case "Folio":
					if (Direction.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.IntFolio).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.IntFolio).ToList();
					break;
				case "Nombre":
					if (Direction.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.StrNombre).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.StrNombre).ToList();
					break;
				case "Periodicidad":
					if (Direction.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.StrPeriodicidad).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.StrPeriodicidad).ToList();
					break;
				case "Departamento":
					if (Direction.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.StrDepartamento).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.StrDepartamento).ToList();
					break;
				case "Area":
					if (Direction.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.StrArea).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.StrArea).ToList();
					break;
				case "FechaInicio":
					if (Direction.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.DtFechaInicio).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.DtFechaInicio).ToList();
					break;
				default:
					break;
			}

			gvListaTareas.SelectedIndex = -1;
			gvListaTareas.DataSource = this.LsTareas;
			gvListaTareas.DataBind();
		}
		protected void gvListaTareas_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertGeneral.Visible)
					pnlAlertGeneral.Visible = false;

				Guid UidTarea = new Guid(gvListaTareas.SelectedDataKey.Value.ToString());
				Guid UidDepartamento = new Guid(gvListaTareas.SelectedDataKey.Values[1].ToString());
				Guid UidArea = new Guid(gvListaTareas.SelectedDataKey.Values[2].ToString());
				Guid UidPeriodicidad = new Guid(gvListaTareas.SelectedDataKey.Values[3].ToString());

				VmHistorico.GetTarea(UidTarea);
				this.UidTareaSeleccionada = UidTarea;
				VmHistorico.GetDepartamento(UidDepartamento);
				VmHistorico.GetArea(UidArea);
				VmHistorico.GetPeriodicidad(UidPeriodicidad);
				VmHistorico.GetTipoFrecuenciaByTarea(UidTarea);

				lblTituloPanelDerecho.Text = "Historico de cumplimiento de la Tarea: " + VmHistorico._Tarea.IntFolio.ToString("0000");
				lblNombreTarea.Text = VmHistorico._Tarea.StrNombre.ToUpper();
				lblDescripcion.Text = VmHistorico._Tarea.StrDescripcion == string.Empty ? "(sin descripción)" : VmHistorico._Tarea.StrDescripcion.ToUpper();
				lblTipoTarea.Text = VmHistorico._Tarea.StrTipoTarea.ToUpper();
				lblDepartamento.Text = VmHistorico._Departamento.StrNombre.ToUpper();
				lblPeriodicidad.Text = VmHistorico._Frecuencia.StrTipoFrecuencia.ToUpper();
				lblArea.Text = VmHistorico._Area == null ? "(global)" : VmHistorico._Area.StrNombre.ToUpper();


				VmHistorico.GetCumplimientoTarea(UidTarea, Guid.Empty, null, null, null, Guid.Empty);
				this.LsCumplimientos = VmHistorico.LsCumplimientosTarea;
				gvCumplimientos.SelectedIndex = -1;
				gvCumplimientos.DataSource = VmHistorico.LsCumplimientosTarea;
				gvCumplimientos.DataBind();

				if (VmHistorico.LsCumplimientosTarea.Count > 0)
					btnPrintReport.CssClass = "btn btn-sm btn-default";
				else
					btnPrintReport.CssClass = "btn btn-sm btn-default disabled";

				MostrarFiltrosCumplimiento("Ocultar");
			}
			catch (Exception ex)
			{
				DisplayAlertGeneral("Error: " + ex.Message, "warning");
			}
		}
		#endregion
		#endregion

		#region Right
		#region Buttons
		protected void btnPrintReport_Click(object sender, EventArgs e)
		{
			Session["ReportUidTarea"] = this.UidTareaSeleccionada;
			Session["ReportFolio"] = BFolioCumplimiento;
			Session["ReportDtFechaInicio"] = BDtFechaInicio;
			Session["ReportDtFechaFin"] = BDtFechaFin;
			Session["ReportUidEstado"] = BUidEstatus;
			ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "open", "window.open('ReporteCumplimientoTarea.aspx', '_blank')", true);
		}

		protected void btnMostrarOcultarFiltrosCumplimiento_Click(object sender, EventArgs e)
		{
			MostrarFiltrosCumplimiento(lblMostrarOcultarFiltrosCumplimiento.Text);
		}
		protected void btnLimpiarFiltrosCumplimiento_Click(object sender, EventArgs e)
		{
			txtFiltroFolioCumplimiento.Text = string.Empty;
			txtFiltroFechaInicioCumplimiento.Text = string.Empty;
			txtFiltroFechaFinCumplimiento.Text = string.Empty;
			ddlFiltroEstadoCumplimiento.SelectedIndex = 0;
		}
		protected void btnBuscarCumplimiento_Click(object sender, EventArgs e)
		{
			this.BFolioCumplimiento = null;
			this.BDtFechaInicio = null;
			this.BDtFechaFin = null;
			this.BUidEstatus = Guid.Empty;

			int? FolioCumplimiento = null;
			DateTime? DtFechaInicio = null;
			DateTime? DtFechaFin = null;
			Guid UidEstatus = new Guid(ddlFiltroEstadoCumplimiento.SelectedValue.ToString());
			this.BUidEstatus = UidEstatus;

			if (!string.IsNullOrEmpty(txtFiltroFolioCumplimiento.Text.Trim()))
			{
				int Aux = 0;
				if (int.TryParse(txtFiltroFolioCumplimiento.Text.Trim(), out Aux))
				{
					if (Aux >= 0)
					{
						FolioCumplimiento = Aux;
						this.BFolioCumplimiento = FolioCumplimiento;
					}
					else
					{
						DisplayAlertGeneral("Ingrese un folio mayor a 0.", "warning");
						return;
					}
				}
				else
				{
					DisplayAlertGeneral("Ingrese un folio numerico valido", "danger");
					return;
				}
			}
			if (!string.IsNullOrEmpty(txtFiltroFechaInicioCumplimiento.Text.Trim()))
			{
				try
				{
					DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicioCumplimiento.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					this.BDtFechaInicio = DtFechaInicio;
				}
				catch (Exception)
				{
					DisplayAlertGeneral("Formato de fecha de (inicio) invalido.", "warning");
					return;
				}
			}
			if (!string.IsNullOrEmpty(txtFiltroFechaFinCumplimiento.Text.Trim()))
			{
				try
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFinCumplimiento.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					BDtFechaFin = DtFechaFin;
				}
				catch (Exception)
				{
					DisplayAlertGeneral("Formato de fecha (fin) invalido.", "warning");
					return;
				}
			}

			VmHistorico.GetCumplimientoTarea(this.UidTareaSeleccionada, Guid.Empty, FolioCumplimiento, DtFechaInicio, DtFechaFin, UidEstatus);
			this.LsCumplimientos = VmHistorico.LsCumplimientosTarea;
			gvCumplimientos.SelectedIndex = -1;
			gvCumplimientos.DataSource = VmHistorico.LsCumplimientosTarea;
			gvCumplimientos.DataBind();

			if (VmHistorico.LsCumplimientosTarea.Count > 0)
				btnPrintReport.CssClass = "btn btn-sm btn-default";
			else
				btnPrintReport.CssClass = "btn btn-sm btn-default disabled";

			MostrarFiltrosCumplimiento("Ocultar");
		}
		private void MostrarFiltrosCumplimiento(string IsOpen)
		{
			if (IsOpen.Equals("Mostrar"))
			{
				lblMostrarOcultarFiltrosCumplimiento.Text = "Ocultar";
				pnlFiltrosCumplimientos.Visible = true;

				btnLimpiarFiltrosCumplimiento.CssClass = "btn btn-sm btn-default";
				btnBuscarCumplimiento.CssClass = "btn btn-sm btn-default"
;
			}
			else
			{
				lblMostrarOcultarFiltrosCumplimiento.Text = "Mostrar";
				pnlFiltrosCumplimientos.Visible = false;

				btnLimpiarFiltrosCumplimiento.CssClass = "btn btn-sm btn-default disabled";
				btnBuscarCumplimiento.CssClass = "btn btn-sm btn-default disabled"
;
			}
		}

		protected void TabDatosGenerales_Click(object sender, EventArgs e)
		{
			liDatosGenerales.Attributes.Add("class", "active");
			liCumplimientos.Attributes.Add("class", "");

			pnlDatosGeneralesTarea.Visible = true;
			pnlCumplimientosTarea.Visible = false;
		}

		protected void TabCumplimientosClick_Click(object sender, EventArgs e)
		{
			liDatosGenerales.Attributes.Add("class", "");
			liCumplimientos.Attributes.Add("class", "active");

			pnlDatosGeneralesTarea.Visible = false;
			pnlCumplimientosTarea.Visible = true;
		}
		#endregion

		#region GridView
		protected void gvCumplimientos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvCumplimientos, "Select$" + e.Row.RowIndex);
				if (e.Row.Cells[6].Text.Equals("True"))
				{
					e.Row.Cells[4].CssClass = "txt-warning";
				}
			}
		}
		protected void gvCumplimientos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidCumplimiento = new Guid(gvCumplimientos.SelectedDataKey.Value.ToString());

				VmHistorico.GetCumplimientoById(UidCumplimiento);
				lblTareaAtrasada.Visible = false;
				if (VmHistorico._Cumplimiento.BlAtrasada.Value)
				{
					lblTareaAtrasada.Visible = true;
					txtFechaCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaAtrasada == null ? "(Sin fecha)" : VmHistorico._Cumplimiento.DtFechaAtrasada.Value.ToString("dd/MM/yyyy");
					txtHoraCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaAtrasada == null ? "(Sin hora)" : VmHistorico._Cumplimiento.DtFechaAtrasada.Value.ToString("hh:mm tt");
				}
				else
				{
					txtFechaCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaHora == null ? "(Sin fecha)" : VmHistorico._Cumplimiento.DtFechaHora.Value.ToString("dd/MM/yyyy");
					txtHoraCumplimiento.Text = VmHistorico._Cumplimiento.DtFechaHora == null ? "(Sin hora)" : VmHistorico._Cumplimiento.DtFechaHora.Value.ToString("hh:mm tt");
				}

				txtObservaciones.Text = VmHistorico._Cumplimiento.StrObservacion == string.Empty ? "(Sin observaciones)" : VmHistorico._Cumplimiento.StrObservacion;
				VmHistorico.GetTarea(VmHistorico._Cumplimiento.UidTarea);
				pnlCumplimientoSeleccion.Visible = false;
				pnlCumplimientoValor.Visible = false;
				pnlCumplimientoVerdaderoFalso.Visible = false;

				string Tipo = VmHistorico._Tarea.StrTipoMedicion;
				rbNo.Checked = false;
				rbYes.Checked = false;
				txtValorIngresado.Text = string.Empty;
				switch (Tipo)
				{
					case "Verdadero/Falso":
						pnlCumplimientoVerdaderoFalso.Visible = true;
						if (VmHistorico._Cumplimiento.BlValor != null)
						{
							if (VmHistorico._Cumplimiento.BlValor.Value)
								rbYes.Checked = true;
							else
								rbNo.Checked = true;
						}
						break;
					case "Numerico":
						pnlCumplimientoValor.Visible = true;
						if (VmHistorico._Cumplimiento.DcValor1 != null)
							txtValorIngresado.Text = VmHistorico._Cumplimiento.DcValor1.Value.ToString("0.00");
						else
							txtValorIngresado.Text = "0.00";
						break;
					case "Seleccionable":
						pnlCumplimientoSeleccion.Visible = true;
						VmHistorico.GetOpcionesTarea(VmHistorico._Cumplimiento.UidTarea);
						ddlOpcionSeleccionada.DataSource = VmHistorico.LsOpcionesTarea;
						ddlOpcionSeleccionada.DataTextField = "StrOpciones";
						ddlOpcionSeleccionada.DataValueField = "UidOpciones";
						ddlOpcionSeleccionada.DataBind();
						if (VmHistorico._Cumplimiento.UidOpcion != null)
						{
							ddlOpcionSeleccionada.SelectedValue = VmHistorico._Cumplimiento.UidOpcion.Value.ToString();
						}
						break;
					default:
						break;
				}

				pnlDetalleCumplimiento.Visible = true;
				pnlBotonesBusqueda.Visible = false;
				pnlFiltros.Visible = false;
				pnlListaTareas.Visible = false;
			}
			catch (Exception ex)
			{
				DisplayAlertGeneral("Ocurrion un error al obtener datos. <br> " + ex.Message, "alert");
			}
		}
		#endregion
		#endregion

		#region Complements
		protected void HideAlertBusqueda(object sender, EventArgs e)
		{
			pnlAlertBusqueda.Visible = false;
		}
		protected void HideAlertGeneral(object sender, EventArgs e)
		{
			pnlAlertGeneral.Visible = false;
		}
		private void DisplayAlertBusqueda(string Message, string Type)
		{
			pnlAlertBusqueda.CssClass = "alert alert-" + Type;
			pnlAlertBusqueda.Visible = true;
			lblAlertBusqueda.Text = Message;
		}
		private void DisplayAlertGeneral(string Message, string Type)
		{
			pnlAlertGeneral.CssClass = "alert alert-" + Type;
			pnlAlertGeneral.Visible = true;
			lblAlertGeneral.Text = Message;
		}

		private string GetSelectedValuesListBox(ListBox lbControl)
		{
			string SelectedValues = string.Empty;
			foreach (ListItem item in lbControl.Items)
			{
				if (item.Selected)
				{
					if (SelectedValues == string.Empty)
						SelectedValues = item.Value.ToString();
					else
						SelectedValues = "," + item.Value.ToString();
				}
			}
			return SelectedValues;
		}
		#endregion
	}
}