﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class Calificaciones : System.Web.UI.Page
	{
		#region Properties
		private string SortDirectionList
		{
			get
			{
				return (string)ViewState["SortDirectionListC"];
			}
			set
			{
				ViewState["SortDirectionListC"] = value;
			}
		}
		private string FiltroCalificacion
		{
			get
			{
				return (string)ViewState["FCalificacion"];
			}
			set
			{
				ViewState["FCalificacion"] = value;
			}
		}
		private bool? FiltroEstatus
		{
			get
			{
				return (bool?)ViewState["FEstatus"];
			}
			set
			{
				ViewState["FEstatus"] = value;
			}
		}
		private float FlValor
		{
			get
			{
				return (float)ViewState["FValor"];
			}
			set
			{
				ViewState["FValor"] = value;
			}
		}
		private Modelo.Sesion SesionActual
		{
			get { return (Modelo.Sesion)Session["Sesion"]; }
		}
		List<Modelo.Calificacion> LsCalificaciones
		{
			get
			{
				return (List<Modelo.Calificacion>)ViewState["LsCalificaciones"];
			}
			set
			{
				ViewState["LsCalificaciones"] = value;
			}
		}
		private VMCalificacion VmCalificacion = new VMCalificacion();
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{
				Response.Redirect("Login.aspx");
			}

			if (SesionActual.uidEmpresaActual == null)
			{
				Response.Redirect(Modelo.Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
			}

			if (!Modelo.Acceso.TieneAccesoAModulo("Calificacion", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Modelo.Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			//if (SesionActual.Rol.Equals("Encargado") || SesionActual.Rol.Equals("Administrador"))
			//{
			//	Response.Redirect(Modelo.Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
			//}

			if (!IsPostBack)
			{
				pnlMessageBusqueda.Visible = false;
				pnlMessageGeneral.Visible = false;

				ShowFilters("Mostrar");

				this.SortDirectionList = "ASC";
				this.FiltroCalificacion = string.Empty;
				this.FiltroEstatus = null;
				this.FlValor = -1;

				EnableFieldsGeneral(false);
				btnOk.Visible = false;
				btnCancel.Visible = false;

				btnEditar.CssClass = "btn btn-sm btn-default disabled";
				btnEditar.Enabled = false;

				pnlConfiguracion.Visible = false;
				btnEditarConfiguracion.Visible = false;
				btnOkConfiguracion.Visible = false;
				btnCancelConfiguracion.Visible = false;

				this.LsCalificaciones = new List<Modelo.Calificacion>();
				gvCalificaciones.DataSource = this.LsCalificaciones;
				gvCalificaciones.DataBind();
			}
		}

		#region Left
		#region Botones
		protected void btnConfiguracion_Click(object sender, EventArgs e)
		{
			pnlConfiguracion.Visible = true;
			btnEditarConfiguracion.Visible = true;
			btnEditarConfiguracion.CssClass = "btn btn-sm btn-default";
			EnableFieldsSettings(false);

			ShowFilters("Ocultar");
			gvCalificaciones.Visible = false;
			btnCancelConfiguracion.Visible = true;
			btnOcultarFiltros.CssClass = "btn btn-sm btn-default disabled";

			VmCalificacion.GetAjustes(SesionActual.uidEmpresaActual.Value);
			if (VmCalificacion.aAjuste.UidEmpresa != Guid.Empty)
			{
				ddlRevisionType.SelectedValue = VmCalificacion.aAjuste.BlRevisionCualitativa ? "Number" : "Text";
			}
		}
		protected void btnEditarConfiguracion_Click(object sender, EventArgs e)
		{
			EnableFieldsSettings(true);
			btnEditarConfiguracion.CssClass = "btn btn-sm btn-default disabled";
			btnOkConfiguracion.Visible = true;
			btnCancelConfiguracion.Visible = true;
		}
		protected void btnOkConfiguracion_Click(object sender, EventArgs e)
		{
			try
			{
				bool IsCualitativa = ddlRevisionType.SelectedValue.Equals("Text") ? false : true;

				VmCalificacion.SetAjustes(SesionActual.uidEmpresaActual.Value, IsCualitativa);
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error: " + ex.Message, "danger");
			}

			EnableFieldsSettings(false);
			pnlConfiguracion.Visible = false;
			btnEditarConfiguracion.Visible = false;
			btnOkConfiguracion.Visible = false;
			btnCancelConfiguracion.Visible = false;

			if (lblOcultarFiltros.Text.Equals("Mostrar"))
				ShowFilters("Ocultar");
			else
				ShowFilters("Mostrar");

			btnOcultarFiltros.CssClass = "btn btn-sm btn-default";
		}
		protected void btnCancelConfiguracion_Click(object sender, EventArgs e)
		{
			pnlConfiguracion.Visible = false;
			btnEditarConfiguracion.Visible = false;
			btnOkConfiguracion.Visible = false;
			btnCancelConfiguracion.Visible = false;

			if (lblOcultarFiltros.Text.Equals("Mostrar"))
				ShowFilters("Ocultar");
			else
				ShowFilters("Mostrar");

			EnableFieldsSettings(false);
			btnOcultarFiltros.CssClass = "btn btn-sm btn-default";
		}

		protected void btnOcultarFiltros_Click(object sender, EventArgs e)
		{
			ShowFilters(lblOcultarFiltros.Text);
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroCalificacion.Text = string.Empty;
			txtFiltroValor.Text = string.Empty;
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			if (pnlMessageBusqueda.Visible)
				pnlMessageBusqueda.Visible = false;

			this.FiltroCalificacion = txtFiltroCalificacion.Text;
			this.FlValor = -1;

			if (!string.IsNullOrWhiteSpace(txtFiltroValor.Text))
			{
				float FlAux = 0;
				if (!float.TryParse(txtFiltroValor.Text.Trim(), out FlAux))
				{
					txtFiltroValor.Focus();
					DisplayMessageBusqueda("Ingrese un valor númerico.", "danger");
					return;
				}
				else
				{
					this.FlValor = float.Parse(txtFiltroValor.Text.Trim());
				}
			}

			this.FiltroEstatus = null;
			if (!ddlFiltroEstatus.SelectedValue.Equals("Empty"))
			{
				this.FiltroEstatus = ddlFiltroEstatus.SelectedValue.Equals("True") ? true : false;
			}

			try
			{
				gvCalificaciones.SelectedIndex = -1;
				VmCalificacion.Search(SesionActual.uidEmpresaActual.Value, this.FiltroCalificacion, this.FiltroEstatus, this.FlValor);
				this.LsCalificaciones = VmCalificacion.LsCalificaciones;
				gvCalificaciones.DataSource = this.LsCalificaciones;
				gvCalificaciones.DataBind();
				ShowFilters("Ocultar");
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error al realizar la busqueda", "danger");
			}
		}
		private void ShowFilters(string ToShow)
		{
			if (ToShow.Equals("Mostrar"))
			{
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";

				lblOcultarFiltros.Text = "Ocultar";
				gvCalificaciones.Visible = false;
				pnlFiltrosBusqueda.Visible = true;
			}
			else
			{
				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";

				lblOcultarFiltros.Text = "Mostrar";
				gvCalificaciones.Visible = true;
				pnlFiltrosBusqueda.Visible = false;
			}
		}
		#endregion

		#region GridView
		protected void gvCalificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvCalificaciones, "Select$" + e.Row.RowIndex);

				Label lblEstatus = e.Row.FindControl("lblEstatusCalificacion") as Label;
				if (e.Row.Cells[5].Text.Equals("True"))
				{
					lblEstatus.ToolTip = "Activo";
					lblEstatus.CssClass = "glyphicon glyphicon-ok";
				}
				else
				{
					lblEstatus.ToolTip = "Inactivo";
					lblEstatus.CssClass = "glyphicon glyphicon-remove";
				}

				if (hfUidCalificacion.Value != string.Empty)
				{
					if (gvCalificaciones.DataKeys[e.Row.RowIndex].Value.ToString().Equals(hfUidCalificacion.Value))
						e.Row.RowState = DataControlRowState.Selected;
				}
			}
		}
		protected void gvCalificaciones_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (btnOk.Visible)
				e.Cancel = true;
		}
		protected void gvCalificaciones_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlMessageGeneral.Visible)
					pnlMessageGeneral.Visible = false;

				Guid UidCalificacion = new Guid(gvCalificaciones.SelectedDataKey.Value.ToString());
				this.hfUidCalificacion.Value = UidCalificacion.ToString();

				VmCalificacion.GetItemById(UidCalificacion);
				txtCalificacion.Text = VmCalificacion.cCalificacion.StrCalificacion;
				txtOrden.Text = VmCalificacion.cCalificacion.IntOrden.ToString();
				txtValor.Text = VmCalificacion.cCalificacion.FlValor.ToString();

				ddlEstatus.SelectedValue = VmCalificacion.cCalificacion.BlEstatus ? "True" : "False";

				btnEditar.CssClass = "btn btn-sm btn-default";
				btnEditar.Enabled = true;
			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Ocurrio un error: " + ex.Message, "danger");
			}
		}
		protected void gvCalificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvCalificaciones.SelectedIndex = -1;
			gvCalificaciones.DataSource = this.LsCalificaciones;
			gvCalificaciones.PageIndex = e.NewPageIndex;
			gvCalificaciones.DataBind();
		}
		protected void gvCalificaciones_Sorting(object sender, GridViewSortEventArgs e)
		{
			gvCalificaciones.SelectedIndex = -1;
			VmCalificacion.LsCalificaciones = this.LsCalificaciones;
			VmCalificacion.SortItemList(this.SortDirectionList, e.SortExpression);
			this.LsCalificaciones = VmCalificacion.LsCalificaciones;

			gvCalificaciones.DataSource = this.LsCalificaciones;
			gvCalificaciones.DataBind();

			if (this.SortDirectionList.Equals("ASC"))
				this.SortDirectionList = "DESC";
			else
				this.SortDirectionList = "ASC";
		}
		#endregion

		#region Fields
		private void EnableFieldsSettings(bool IsEnabled)
		{
			ddlRevisionType.Enabled = IsEnabled;
		}
		#endregion
		#endregion

		#region Right
		#region Buttons
		protected void btnNuevo_Click(object sender, EventArgs e)
		{
			if (pnlMessageGeneral.Visible)
				pnlMessageGeneral.Visible = false;

			gvCalificaciones.SelectedIndex = -1;

			btnNuevo.CssClass = "btn btn-sm btn-default disabled";
			btnNuevo.Enabled = false;

			btnEditar.CssClass = "btn btn-sm btn-default disabled";
			btnEditar.Enabled = false;

			btnOk.Visible = true;
			btnCancel.Visible = true;

			CleanFieldsGeneral();
			EnableFieldsGeneral(true);

			hfUidCalificacion.Value = string.Empty;
		}
		protected void btnEditar_Click(object sender, EventArgs e)
		{
			if (pnlMessageGeneral.Visible)
				pnlMessageGeneral.Visible = false;

			btnNuevo.CssClass = "btn btn-sm btn-default disabled";
			btnNuevo.Enabled = false;

			btnEditar.CssClass = "btn btn-sm btn-default disabled";
			btnEditar.Enabled = false;

			btnOk.Visible = true;
			btnCancel.Visible = true;

			EnableFieldsGeneral(true);
		}
		protected void btnOk_Click(object sender, EventArgs e)
		{
			if (pnlMessageGeneral.Visible)
				pnlMessageGeneral.Visible = false;

			if (string.IsNullOrWhiteSpace(txtCalificacion.Text))
			{
				txtCalificacion.Focus();
				DisplayMessageGeneral("Ingrese una <strong>Calificación</strong>.", "warning");
				return;
			}

			if (string.IsNullOrWhiteSpace(txtValor.Text))
			{
				txtValor.Focus();
				DisplayMessageGeneral("Ingrese una <strong>Valor</strong>.", "warning");
				return;
			}

			if (string.IsNullOrWhiteSpace(txtOrden.Text))
			{
				txtOrden.Focus();
				DisplayMessageGeneral("Ingrese un valor en <strong>Orden</strong>", "warning");
				return;
			}

			int Aux = 0;
			if (!int.TryParse(txtOrden.Text.Trim(), out Aux))
			{
				txtOrden.Focus();
				DisplayMessageGeneral("Ingrese un valor <strong>Númerico</strong> en <strong>Orden</strong>.", "warning");
				return;
			}

			float FlAux = 0;
			if (!float.TryParse(txtValor.Text.Trim(), out FlAux))
			{
				txtValor.Focus();
				DisplayMessageGeneral("Ingrese un valor <strong>Númerico</strong> en <strong>Valor</strong>.", "warning");
				return;
			}

			Guid UidEmpresa = SesionActual.uidEmpresaActual.Value;
			string Calificacion = txtCalificacion.Text.Trim();
			float FlValor = float.Parse(txtValor.Text.Trim());
			int IntOrden = int.Parse(txtOrden.Text.Trim());
			bool BlEstatus = true;
			if (ddlEstatus.SelectedValue.Equals("False"))
				BlEstatus = false;

			if (hfUidCalificacion.Value == string.Empty)
			{
				if (VmCalificacion.AddNew(UidEmpresa, Calificacion, IntOrden, BlEstatus, FlValor))
				{
					DisplayMessageGeneral("Guardado Correctamente.", "success");
				}
				else
				{
					DisplayMessageGeneral("Ocurrio un error al guardar.", "danger");
				}
			}
			else
			{
				Guid UidCalificacion = new Guid(hfUidCalificacion.Value);
				if (VmCalificacion.UpdateItem(UidCalificacion, Calificacion, IntOrden, BlEstatus, FlValor))
				{
					DisplayMessageGeneral("Datos Actualizado.", "success");
				}
				else
				{
					DisplayMessageGeneral("Ocurrio un error al actualizar.", "danger");
				}
			}

			try
			{
				gvCalificaciones.SelectedIndex = -1;
				VmCalificacion.Search(SesionActual.uidEmpresaActual.Value, this.FiltroCalificacion, this.FiltroEstatus, this.FlValor);
				this.LsCalificaciones = VmCalificacion.LsCalificaciones;
				gvCalificaciones.DataSource = this.LsCalificaciones;
				gvCalificaciones.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error al actualizar lista.", "danger");
			}

			btnCancel_Click(null, null);
		}
		protected void btnCancel_Click(object sender, EventArgs e)
		{
			if (pnlMessageGeneral.Visible)
				pnlMessageGeneral.Visible = false;

			btnOk.Visible = false;
			btnCancel.Visible = false;

			btnNuevo.CssClass = "btn btn-sm btn-default";
			btnNuevo.Enabled = true;
			EnableFieldsGeneral(false);

			if (hfUidCalificacion.Value == string.Empty)
			{
				CleanFieldsGeneral();
			}
			else
			{
				btnEditar.CssClass = "btn btn-sm btn-default";
				btnEditar.Enabled = true;
			}
		}
		#endregion

		#region Fields
		private void CleanFieldsGeneral()
		{
			txtCalificacion.Text = string.Empty;
			txtValor.Text = string.Empty;
			txtOrden.Text = string.Empty;
			ddlEstatus.SelectedIndex = 0;
		}
		private void EnableFieldsGeneral(bool IsEnabled)
		{
			txtCalificacion.Enabled = IsEnabled;
			txtValor.Enabled = IsEnabled;
			txtOrden.Enabled = IsEnabled;
			ddlEstatus.Enabled = IsEnabled;
		}
		#endregion
		#endregion

		#region Aux
		private void DisplayMessageBusqueda(string Message, string Type)
		{
			lblMessageBusqueda.Text = Message;
			pnlMessageBusqueda.CssClass = "alert alert-" + Type;
			pnlMessageBusqueda.Visible = true;
		}
		private void DisplayMessageGeneral(string Message, string Type)
		{
			lblMessageGeneral.Text = Message;
			pnlMessageGeneral.CssClass = "alert alert-" + Type;
			pnlMessageGeneral.Visible = true;
		}

		protected void CloseMessageBusqueda(object sender, EventArgs e)
		{
			pnlMessageBusqueda.Visible = false;
		}
		protected void CloseMessageGeneral(object sender, EventArgs e)
		{
			pnlMessageGeneral.Visible = false;
		}
		#endregion


	}
}