﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="ControlTurnos.aspx.cs" Inherits="CodorniX.Vista.ControlTurnos" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Control de Turnos</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="text-center">
								Turnos
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body panel-pd">
					<div class="col-lg-12  pd-left-right-5">
						<div class="text-right">
							<div class="btn-group">
								<asp:LinkButton ID="btnMostrar" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnMostrar_Click" Text="Mostrar" />
								<asp:LinkButton ID="btnLimpiar" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnLimpiar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Limpiar
								</asp:LinkButton>
								<asp:LinkButton ID="btnBuscar" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnBuscar_Click">
                            <span class="glyphicon glyphicon-search"></span>
                            Buscar
								</asp:LinkButton>
							</div>
						</div>
					</div>
					<div class="col-lg-12 pd-0">
						<asp:PlaceHolder ID="panelBusqueda" runat="server">
							<div class="col-xs-12  pd-left-right-5">
								<h6>Departamento</h6>
								<asp:ListBox ID="lbDepartamentos" CssClass="form-control" runat="server" SelectionMode="Multiple" Rows="4" />
							</div>
						</asp:PlaceHolder>
					</div>
					<div class="col-lg-12 pd-0">
						<asp:PlaceHolder ID="panelResultados" runat="server" Visible="false">
							<div class="col-xs-12 pd-left-right-5">
								<asp:GridView ID="dvgDepartamentos" runat="server" CssClass="table table-bordered table-sm table-condensed input-sm" OnRowDataBound="dvgDepartamentos_RowDataBound" OnSelectedIndexChanged="dvgDepartamentos_SelectedIndexChanged" AutoGenerateColumns="false" DataKeyNames="UidDepartamento,UidUsuario,DtFecha,UidInicioTurno,UidEstadoTurno">
									<EmptyDataTemplate>No hay Historial</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="IntFolio" HeaderText="Folio" />
										<asp:BoundField DataField="StrUsuario" HeaderText="Usuario" />
										<asp:BoundField DataField="StrTurno" HeaderText="Turno" />
										<asp:BoundField DataField="DtFecha" DataFormatString="{0:d}" HeaderText="Fecha" />
										<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" />
										<asp:BoundField DataField="DtFechaHoraInicio" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Inicio" />
										<asp:BoundField DataField="DtFechaHoraFin" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Fin" />
										<asp:BoundField DataField="StrEstadoTurno" HeaderText="Estado" />
									</Columns>
								</asp:GridView>
							</div>
						</asp:PlaceHolder>
					</div>
				</div>
			</div>
		</div>
		<div class="col-xs-12 col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Información del Turno							
				</div>
				<div class="panel-body panel-pd">
					<div class="col-lg-12 pd-0">
						<table style="width: 100%;">
							<tr>
								<td>
									<div class="btn-group">
										<asp:LinkButton ID="btnReporte" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnReporte_Click" OnClientClick="document.forms[0].target ='_blank';">
											<span class="glyphicon glyphicon-file"></span>
											Reporte
										</asp:LinkButton>
										<asp:LinkButton ID="btnLista" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnLista_Click" OnClientClick="document.forms[0].target = '_blank';">
											<span class="glyphicon glyphicon-list"></span>
											Lista
										</asp:LinkButton>
									</div>
								</td>
								<td class="text-right">
									<label class="radio-inline">
										<asp:CheckBox runat="server" ID="cbCumplimientoPosterior" Enabled="false"/>
										<small>Cumplimiento posterior.</small>
									</label>
									<div class="btn-group">
										<asp:LinkButton ID="btnIniciarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnIniciarTurno_Click" Text="Iniciar" />
										<asp:LinkButton ID="btnInicioTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnInicioTurno_Click" Text="Tomar" />
										<asp:LinkButton ID="btnBloquearTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnBloquearTurno_Click" Text="Bloquear" />
										<asp:LinkButton ID="btnCerrarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnCerrarTurno_Click" Text="Cerrar" />
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-lg-12 pd-left-right-5">
						<asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
							<div class="alert alert-danger">
								<asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">x</asp:LinkButton>
								<strong>Error: </strong>
								<asp:Label ID="lblError" runat="server" />
							</div>
						</asp:PlaceHolder>
					</div>
					<asp:HiddenField ID="fldUidPeriodo" runat="server" />
					<asp:HiddenField ID="fldUidUsuario" runat="server" />
					<asp:HiddenField ID="fldUidInicioTurno" runat="server" />
					<ul class="nav nav-tabs">
						<li class="active" id="activeResumen" runat="server">
							<asp:LinkButton ID="tabResumen" runat="server" Text="Resumen" OnClick="tabResumen_Click">
								Resumen
							</asp:LinkButton>
						</li>
						<li id="activeCompletadas" runat="server">
							<asp:LinkButton ID="tabCompletadas" runat="server" OnClick="tabCompletadas_Click" ToolTip="Tareas Completadas">
								<span class="glyphicon glyphicon-check"></span>
							</asp:LinkButton>
						</li>

						<li id="activeNoCompletadas" runat="server">
							<asp:LinkButton ID="tabNoCompletadas" runat="server" OnClick="tabNoCompletadas_Click" ToolTip="Tareas No Completadas">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</li>
						<li id="activeRequeridas" runat="server">
							<asp:LinkButton ID="tabRequeridas" runat="server" OnClick="tabRequeridas_Click" ToolTip="Tareas Requeridas">
								<span class="glyphicon glyphicon glyphicon-asterisk"></span>
							</asp:LinkButton>
						</li>
						<li id="activeCanceladas" runat="server">
							<asp:LinkButton runat="server" OnClick="tabCanceladas_Click" ToolTip="Tareas Canceladas">
								<span class="glyphicon glyphicon-ban-circle"></span>
							</asp:LinkButton>
						</li>
						<li id="activePospuestas" runat="server">
							<asp:LinkButton runat="server" OnClick="tabPospuestas_Click" ToolTip="Tareas Pospuestas">								
								<span class="glyphicon glyphicon-share-alt"></span>
							</asp:LinkButton>
						</li>
					</ul>
					<div class="col-lg-12 pd-0">
						<asp:PlaceHolder ID="PanelResumen" runat="server">
							<div class="col-md-12 pd-left-right-5">
								<small>Departamento</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblDepartamento" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Turno</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblTurno" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Hora Inicio</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraInicio"></asp:TextBox>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Hora Fin</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraFin"></asp:TextBox>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Cumplidas</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblCumplidas"></asp:TextBox>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>No Cumplidas</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblNoCumplidas"></asp:TextBox>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Requeridas No Cumplidas</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblRequeridasNoCumplidas"></asp:TextBox>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Estado</small>
								<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblEstadoDelTurno"></asp:TextBox>
							</div>
						</asp:PlaceHolder>
					</div>
					<div class="col-lg-12 pd-0">
						<asp:PlaceHolder ID="PanelCompletadas" runat="server">
							<asp:Label runat="server">Tareas Cumplidas</asp:Label>
							<asp:GridView ID="DVGTareasCumplidas" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidTareaCumplida">
								<EmptyDataTemplate>No hay Tareas Cumplidas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrStatus" HeaderText="Estatus" />
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodo" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
								<EmptyDataTemplate>
									No hay tareas completadas en este turno
								</EmptyDataTemplate>
							</asp:GridView>
						</asp:PlaceHolder>
					</div>
					<div class="col-lg-12 pd-0">
						<asp:PlaceHolder ID="PanelNoCompletadas" runat="server">
							<asp:Label runat="server"> No cumplidas</asp:Label>
							<asp:GridView ID="DvgTareasNoCumplidas" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidTareaNoCumplida">
								<EmptyDataTemplate>No hay Tareas No Cumplidas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrStatus" HeaderText="Estatus" />
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodo" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
								<EmptyDataTemplate>
									No hay tareas no completadas en este turno
								</EmptyDataTemplate>
							</asp:GridView>
						</asp:PlaceHolder>
					</div>
					<div class="col-lg-12 pd-0">
						<asp:PlaceHolder ID="panelRequeridas" runat="server">
							<asp:Label runat="server">Requeridas</asp:Label>
							<asp:GridView ID="dgvTareasRequeridas" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>No hay Tareas Requeridas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrStatus" HeaderText="Estatus" />
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodo" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
							</asp:GridView>
						</asp:PlaceHolder>
					</div>
					<div class="col-lg-12 pd-0">
						<asp:Panel runat="server" ID="pnlCanceladas">
							<small>Canceladas</small>
							<asp:GridView runat="server" ID="gvTareasCanceladas" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
								<EmptyDataTemplate>
									No hay tareas canceladas en este turno.
								</EmptyDataTemplate>
							</asp:GridView>
						</asp:Panel>
					</div>
					<div class="col-lg-12 pd-0">
						<asp:Panel runat="server" ID="pnlPospuestas">
							<small>Pospuestas</small>
							<asp:GridView runat="server" ID="gvTareasPospuestas" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" SortExpression="TipoT" />
								</Columns>
								<EmptyDataTemplate>
									No hay tareas pospuestas en este turno.
								</EmptyDataTemplate>
							</asp:GridView>
						</asp:Panel>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
