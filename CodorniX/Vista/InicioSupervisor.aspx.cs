﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Globalization;

namespace CodorniX.Vista
{
	public partial class InicioSupervisor : System.Web.UI.Page
	{
		private VMInicioSupervisor VM = new VMInicioSupervisor();
		private Sesion SesionActual => (Sesion)Session["Sesion"];
		private DateTime DtToday
		{
			get
			{
				return (DateTime)ViewState["FechaActual"];
			}
			set
			{
				ViewState["FechaActual"] = value;
			}
		}

		private Guid UidTurnoSucursal
		{
			get => (Guid)(ViewState["UidTurnoSucursal"] ?? Guid.Empty);
			set => ViewState["UidTurnoSucursal"] = value;
		}

		private void UpdateList()
		{
			if (pnlAlertList.Visible)
				pnlAlertList.Visible = false;

			DateTime? DtFechaInicio = DateTime.Today;
			DateTime? DtFechaFin = DateTime.Today;
			DateTime Today = SesionActual.GetDateTime();
			int? Folio = null;

			if (txtFiltroFolio.Text.Trim() != string.Empty)
			{
				int Aux;
				if (int.TryParse(txtFiltroFolio.Text.Trim(), out Aux))
				{
					Folio = Aux;
				}
			}

			if (txtFiltroFechaInicio.Text == string.Empty && txtFiltroFechaFin.Text == string.Empty && txtFiltroFolio.Text.Trim() == string.Empty)
			{
				VM.ObtenerTurnoDeHoy(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, Today);
				dvgTurnos.DataSource = VM.LsTurnosSupervisor;
				dvgTurnos.DataBind();
			}
			else
			{
				Guid UidUsuario = SesionActual.uidUsuario;
				Guid UidSucursal = SesionActual.uidSucursalActual.Value;
				if (txtFiltroFechaInicio.Text.Trim() != string.Empty)
				{
					try
					{
						DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					catch (Exception)
					{
						pnlAlertList.Visible = true;
						lblMessage.Text = "Fecha de Inicio con el formato incorrecto";
						return;
					}
				}
				if (txtFiltroFechaFin.Text.Trim() != string.Empty)
				{
					try
					{
						DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					catch (Exception)
					{
						pnlAlertList.Visible = true;
						lblMessage.Text = "Fecha Fin con el formato incorrecto";
						return;
					}
				}

				VM.LsTurnosSupervisor = new List<TurnoSupervisor>();
				int AuxCompare = 0;
				for (; DtFechaInicio.Value <= DtFechaFin.Value; DtFechaInicio = DtFechaInicio.Value.AddDays(1))
				{
					VM.BusquedaTurnos(UidUsuario, UidSucursal, DtFechaInicio.Value);
					AuxCompare = DateTime.Compare(DtFechaInicio.Value, Today);
					if (AuxCompare < 0 && VM.TurnoSupervisor.DtFechaInicio == null)
					{
					}
					else
					{
						if (Folio != null)
						{
							if (VM.TurnoSupervisor.IntFolio == Folio)
								VM.LsTurnosSupervisor.Add(VM.TurnoSupervisor);
						}
						else
							VM.LsTurnosSupervisor.Add(VM.TurnoSupervisor);
					}
				}
				dvgTurnos.DataSource = VM.LsTurnosSupervisor;
				dvgTurnos.DataBind();
				ViewState["TurnoPreviousRow"] = null;
			}
			/*
			if (VM.TurnoSupervisor.StrEstadoTurno == "Abierto")
			{
				btnIniciarTurno.Disable();
				btnCerrarTurno.Enable();
				SesionActual.uidTurnoSupervisor = VM.TurnoSupervisor.UidTurnoSupervisor;
			}
			else
			{
				btnIniciarTurno.Enable();
				btnCerrarTurno.Disable();
				SesionActual.uidTurnoSupervisor = null;
			}
			*/
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("InicioSupervisor", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				btnCerrarTurno.Disable();
				btnIniciarTurno.Disable();
				pnlAlertList.Visible = false;

				liResumen.Attributes.Add("class", "active");
				liRevisiones.Attributes.Remove("class");

				pnlResumenTurno.Visible = true;
				pnlListaRevisiones.Visible = false;

				gvTareasRevisadas.DataSource = null;
				gvTareasRevisadas.DataBind();

				VM.ObtenerCalificaciones(SesionActual.uidEmpresaActual.Value);
				ddlDetailReviewCalificacion.DataSource = VM.LsCalificaciones;
				ddlDetailReviewCalificacion.DataTextField = "StrCalificacion";
				ddlDetailReviewCalificacion.DataValueField = "UidCalificacion";
				ddlDetailReviewCalificacion.DataBind();

				DisplayDetailReview(false);

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				this.DtToday = horaLocal.DateTime;
				UpdateList();
			}
		}

		protected void btnIniciarTurno_Click(object sender, EventArgs e)
		{
			Site1 master = (Site1)Page.Master;
			btnIniciarTurno.Disable();
			try
			{
				if (pnlAlertList.Visible == true) { pnlAlertList.Visible = false; }
				Guid uid = new Guid(dvgTurnos.SelectedDataKey.Value.ToString());

				if (uid == Guid.Empty)
				{
					VM.CrearTurno(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, SesionActual.GetDateTimeOffset());
				}
				else
				{
					VM.IniciarTurno(uid);
				}

				UpdateList();
				btnCerrarTurno.Enable();
				btnIniciarTurno.Disable();

				if (uid == Guid.Empty)
				{
					VM.ObtenerTurnoDeHoy(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, SesionActual.GetDateTime());
					uid = VM.TurnoSupervisor.UidTurnoSupervisor;
				}

				SesionActual.uidTurnoSupervisor = uid;

				master.UpdateNavbar();
			}
			catch (Exception)
			{
				pnlAlertList.Visible = true;
				lblMessage.Text = "Error al iniciar turno";
			}
		}

		protected void btnCerrarTurno_Click(object sender, EventArgs e)
		{
			btnCerrarTurno.Disable();
			try
			{
				if (pnlAlertList.Visible == true) { pnlAlertList.Visible = false; }
				Guid uid = new Guid(dvgTurnos.SelectedDataKey.Value.ToString());
				VM.CerrarTurno(uid);
				VM.ModificarFechaFin(uid, SesionActual.GetDateTimeOffset());

				UpdateList();
				btnIniciarTurno.Enable();
				btnCerrarTurno.Disable();

				SesionActual.uidTurnoSupervisor = null;
			}
			catch (Exception)
			{
				pnlAlertList.Visible = true;
				lblMessage.Text = "Error al cerrar turno";
			}
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			btnActualizar.Enabled = false;
			try
			{
				if (pnlAlertList.Visible == true) { pnlAlertList.Visible = false; }
				UpdateList();
				btnCerrarTurno.Disable();
				btnIniciarTurno.Disable();

				int pos = -1;
				if (ViewState["TurnoPreviousRow"] != null)
				{
					pos = (int)ViewState["TurnoPreviousRow"];
					GridViewRow previousRow = dvgTurnos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}

				ViewState["TurnoPreviousRow"] = null;

			}
			catch (Exception ex)
			{
				pnlAlertList.Visible = true;
				lblMessage.Text = "Error al obtener datos";
			}
			finally { btnActualizar.Enabled = true; }
		}

		protected void dvgTurnos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgTurnos, "Select$" + e.Row.RowIndex);

				if (e.Row.Cells[3].Text != "(no iniciado)")
				{
					DateTime dtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(e.Row.Cells[3].Text.Trim().Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					int Aux = DateTime.Compare(dtFechaInicio, this.DtToday.Date);
					if (Aux < 0)
					{
						e.Row.CssClass = "border-warning bg-warning";
					}
				}
				/*
				if (SesionActual.uidTurnoSupervisor != null)
				{
					Guid Uid = new Guid(dvgTurnos.DataKeys[e.Row.RowIndex].Value.ToString());
					if (Uid == SesionActual.uidTurnoSupervisor.Value)
					{
						e.Row.CssClass = "success";
					}
				}
				*/
			}
		}

		protected void dvgTurnos_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertList.Visible == true) { pnlAlertList.Visible = false; }
				int pos = -1;
				if (ViewState["TurnoPreviousRow"] != null)
				{
					pos = (int)ViewState["TurnoPreviousRow"];
					GridViewRow previousRow = dvgTurnos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}

				ViewState["TurnoPreviousRow"] = dvgTurnos.SelectedIndex;
				dvgTurnos.SelectedRow.AddCssClass("success");

				gvTareasRevisadas.SelectedIndex = -1;
				gvTareasRevisadas.DataSource = null;
				gvTareasRevisadas.DataBind();

				Guid uid = new Guid(dvgTurnos.SelectedDataKey.Value.ToString());
				VM.ObtenerTurno(uid);
				if (VM.TurnoSupervisor != null)
				{
					Site1 master = (Site1)Page.Master;
					if (VM.TurnoPendienteCerrarUsuario(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, VM.TurnoSupervisor.DtFechaInicio.Value.DateTime))
					{
						pnlAlertList.Visible = true;
						lblMessage.Text = "Turno Pendiente por Cerrar";
						btnIniciarTurno.Disable();
						btnCerrarTurno.Disable();
						SesionActual.uidTurnoSupervisor = null;
						master.UpdateNavbar();
						return;
					}

					VM.ObtenerEstadoTurno(VM.TurnoSupervisor.UidEstadoTurno);
					master.ShowDetalleTurno(true);
					SesionActual.FolioTurno = VM.TurnoSupervisor.IntFolio;
					master.SetDetalleTurnoSupervisor(SesionActual.FolioTurno.Value);

					if (VM.EstadoTurno.StrEstadoTurno == "Abierto")
					{
						btnIniciarTurno.Disable();
						btnCerrarTurno.Enable();
						SesionActual.uidTurnoSupervisor = VM.TurnoSupervisor.UidTurnoSupervisor;

						master.UpdateNavbar();
					}
					else
					{
						SesionActual.uidTurnoSupervisor = null;
						btnIniciarTurno.Enable();
						btnCerrarTurno.Disable();
					}

					txtSucursal.Text = VM.TurnoSupervisor.StrSucursal;
					txtFechaInicio.Text = VM.TurnoSupervisor.FechaInicio;
					txtFechaFin.Text = VM.TurnoSupervisor.FechaFin;
					txtEstadoTurno.Text = VM.EstadoTurno.StrEstadoTurno;

					hfUidTurnoSupervisor.Value = uid.ToString();

					VM.ObtenerRevisionesTurno(uid);
					gvTareasRevisadas.DataSource = VM.LsRevisiones;
					ViewState["revisiones"] = VM.LsRevisiones;
					gvTareasRevisadas.DataBind();
				}
				else
				{
					Site1 master = (Site1)Page.Master;
					master.ShowDetalleTurno(false);

					txtSucursal.Text = "";
					txtFechaInicio.Text = "(No Iniciado)";
					txtFechaFin.Text = "(No Cerrado)";
					txtEstadoTurno.Text = "No Creado";

					if (VM.TurnoPendienteCerrarUsuario(SesionActual.uidUsuario, SesionActual.uidSucursalActual.Value, SesionActual.GetDateTime()))
					{
						pnlAlertList.Visible = true;
						lblMessage.Text = "Turno Pendiente por Cerrar";
						btnIniciarTurno.Disable();
						btnCerrarTurno.Disable();
						SesionActual.uidTurnoSupervisor = null;
						master.UpdateNavbar();
						return;
					}

					btnIniciarTurno.Enable();
					btnCerrarTurno.Disable();
				}
			}
			catch (Exception)
			{
				pnlAlertList.Visible = true;
				lblMessage.Text = "Error al obtener datos";
			}
		}

		protected void btnReporteSupervisor_Click(object sender, EventArgs e)
		{
			if (hfUidTurnoSupervisor.Value == string.Empty)
			{
				lblMessage.Text = "Seleccione un turno";
				return;
			}

			Session["UidTurnoSupervisorReport"] = new Guid(hfUidTurnoSupervisor.Value);

			ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
				"window.open('ReporteSupervisor.aspx', '_blank')", true);
		}

		protected void btnResumenSupervision_Click(object sender, EventArgs e)
		{
			ScriptManager.RegisterClientScriptBlock(this, GetType(), "open",
				"window.open('ResumenSupervision.aspx', '_blank')", true);
		}

		protected void btnCerrarPanelResumenTarea_Click(object sender, EventArgs e)
		{
			DisplayDetailReview(false);
			gvTareasRevisadas.SelectedIndex = -1;
		}

		#region Navs
		protected void TabResumen(object sender, EventArgs e)
		{
			liResumen.Attributes.Add("class", "active");
			liRevisiones.Attributes.Remove("class");

			pnlResumenTurno.Visible = true;
			pnlListaRevisiones.Visible = false;

			LimpiarCamposResumenTarea();
			DisplayDetailReview(false);
		}
		protected void TabRevisiones(object sender, EventArgs e)
		{
			liRevisiones.Attributes.Add("class", "active");
			liResumen.Attributes.Remove("class");

			pnlResumenTurno.Visible = false;
			pnlListaRevisiones.Visible = true;
		}
		#endregion

		#region GridView
		protected void gvTareasRevisadas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareasRevisadas, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvTareasRevisadas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{

		}
		protected void gvTareasRevisadas_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				DisplayDetailReview(true);
				LimpiarCamposResumenTarea();

				Guid UidRevision = new Guid(gvTareasRevisadas.SelectedDataKey.Value.ToString());
				VM.ObtenerDatosRevision(UidRevision);

				txtDetailReviewTarea.Text = VM.REVISION.StrTarea;
				txtDetailReviewDepartamento.Text = VM.REVISION.StrDepartamento;
				txtDetailReviewArea.Text = VM.REVISION.StrArea;

				ddlDetailReviewCalificacion.SelectedValue = VM.REVISION.UidCalificacion.ToString();

				txtDetailReviewNotas.Text = VM.REVISION.StrNotas;

				Guid UidTarea = VM.REVISION.UidTarea;
				string TipoMedicion = VM.REVISION.StrTipoMedicion;
				switch (TipoMedicion)
				{
					case "Verdadero/Falso":
						pnlDetailReviewSelection.Visible = false;
						pnlDetailReviewTrueFalse.Visible = true;
						pnlDetailReviewValue.Visible = false;
						if (VM.REVISION.BlValorOriginal)
							txtDetailValorIngresado.Text = "SI";
						else
							txtDetailValorIngresado.Text = "NO";

						if (VM.REVISION.BlValor != null)
						{
							if (VM.REVISION.BlValor.Value)
							{
								rbDetailReviewYes.Checked = true;
							}
							else
							{
								rbDetailReviewNo.Checked = true;
							}
						}
						break;
					case "Numerico":
						pnlDetailReviewSelection.Visible = false;
						pnlDetailReviewTrueFalse.Visible = false;
						pnlDetailReviewValue.Visible = true;
						lblDetailReviewUnidadMedida.Text = VM.REVISION.StrTipoUnidad;
						txtDetailValorIngresado.Text = VM.REVISION.DcValor1Original.ToString();

						if (VM.REVISION.DcValor1 != null)
						{
							txtDetailReviewValue.Text = VM.REVISION.DcValor1.ToString();
						}
						break;
					case "Seleccionable":
						pnlDetailReviewSelection.Visible = true;
						pnlDetailReviewTrueFalse.Visible = false;
						pnlDetailReviewValue.Visible = false;
						VM.ObtenerOpcionesTarea(UidTarea);

						int Index = VM.LsOpcionesTarea.FindIndex(x => x.UidOpciones == VM.REVISION.UidOpcionOriginal);
						if (Index >= 0)
							txtDetailValorIngresado.Text = VM.LsOpcionesTarea[Index].StrOpciones;

						if (VM.REVISION.UidOpcion == null)
						{
							ddlDetailReviewOptions.DataSource = null;
							ddlDetailReviewOptions.DataBind();
							ddlDetailReviewOptions.Items.Insert(0, new ListItem("(Ninguna)", Guid.Empty.ToString()));
						}
						else
						{
							ddlDetailReviewOptions.DataSource = VM.LsOpcionesTarea;
							ddlDetailReviewOptions.DataTextField = "StrOpciones";
							ddlDetailReviewOptions.DataValueField = "UidOpciones";
							ddlDetailReviewOptions.DataBind();
							ddlDetailReviewOptions.SelectedValue = VM.REVISION.UidOpcion.Value.ToString();
						}
						break;
					default:
						break;
				}
			}
			catch (Exception ex)
			{

			}
		}
		protected void gvTareasRevisadas_Sorting(object sender, GridViewSortEventArgs e)
		{
			if (hfSortDirectionTareasRevisadas.Value.Equals("DESC"))
				hfSortDirectionTareasRevisadas.Value = "ASC";
			else
				hfSortDirectionTareasRevisadas.Value = "DESC";
			gvTareasRevisadas.SelectedIndex = -1;
			this.VM.LsRevisiones = (List<Revision>)ViewState["revisiones"];
			VM.SortList(hfSortDirectionTareasRevisadas.Value, e.SortExpression);
			gvTareasRevisadas.DataSource = VM.LsRevisiones;
			ViewState["revisiones"] = VM.LsRevisiones;
			gvTareasRevisadas.DataBind();

		}
		protected void gvTareasRevisadas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			if (hfUidTurnoSupervisor.Value != string.Empty)
			{
				VM.ObtenerRevisionesTurno(new Guid(hfUidTurnoSupervisor.Value));
				gvTareasRevisadas.SelectedIndex = -1;
				gvTareasRevisadas.DataSource = VM.LsRevisiones;
			}
			gvTareasRevisadas.PageIndex = e.NewPageIndex;
			gvTareasRevisadas.DataBind();
		}
		#endregion

		private void DisplayDetailReview(bool Status)
		{
			pnlListaTurnos.Visible = Status ? false : true;
			pnlFiltrosBusqueda.Visible = Status ? false : true;
			pnlResumenTarea.Visible = Status;
		}
		private void LimpiarCamposResumenTarea()
		{
			txtDetailReviewTarea.Text = string.Empty;
			txtDetailReviewDepartamento.Text = string.Empty;
			txtDetailReviewArea.Text = string.Empty;
			txtDetailReviewValue.Text = string.Empty;
			rbDetailReviewNo.Checked = false;
			rbDetailReviewYes.Checked = false;
			ddlDetailReviewCalificacion.SelectedIndex = 0;
			txtDetailReviewNotas.Text = string.Empty;
		}

	}
}