﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class ReporteCumplimientoTarea : System.Web.UI.Page
	{
		private Modelo.Sesion SesionActual
		{
			get { return (Modelo.Sesion)Session["Sesion"]; }
		}
		VmReporteHistoricoTareas VmHistorico;
		private Guid UidTarea
		{
			get
			{
				return (Guid)ViewState["UidTarea"];
			}
			set
			{
				ViewState["UidTarea"] = value;
			}
		}
		private Guid UidEstado
		{
			get
			{
				return (Guid)ViewState["UidEstado"];
			}
			set
			{
				ViewState["UidEstado"] = value;
			}
		}
		private int? Folio
		{
			get
			{
				return ViewState["FolioT"] == null ? (int?)null : (int)ViewState["FolioT"];
			}
			set
			{
				ViewState["FolioT"] = value;
			}
		}
		private DateTime? DtFechaInicio
		{
			get
			{
				return ViewState["DtFechaInicio"] == null ? (DateTime?)null : (DateTime)ViewState["DtFechaInicio"];
			}
			set
			{
				ViewState["DtFechaInicio"] = value;
			}
		}
		private DateTime? DtFechaFin
		{
			get
			{
				return ViewState["DtFechaFin"] == null ? (DateTime?)null : (DateTime)ViewState["DtFechaFin"];
			}
			set
			{
				ViewState["DtFechaFin"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!IsPostBack)
			{
				rvReport.SizeToReportContent = true;
				rvReport.Width = Unit.Percentage(100);
				rvReport.Height = Unit.Percentage(100);

				try
				{
					this.UidTarea = (Guid)Session["ReportUidTarea"];
					this.Folio = (int?)Session["ReportFolio"];
					this.DtFechaInicio = (DateTime?)Session["ReportDtFechaInicio"];
					this.DtFechaFin = (DateTime?)Session["ReportDtFechaFin"];
					this.UidEstado = (Guid)Session["ReportUidEstado"];
				}
				catch (Exception ex)
				{
					return;
				}

				Session["ReportUidTarea"] = null;
				Session["ReportFolio"] = null;
				Session["ReportDtFechaInicio"] = null;
				Session["ReportDtFechaFin"] = null;
				Session["ReportUidEstado"] = null;

				VmHistorico = new VmReporteHistoricoTareas();

				VmHistorico.GetDatosEmpresa(SesionActual.uidEmpresaActual.Value);

				VmHistorico.GetDatosSucursal(SesionActual.uidSucursalActual.Value);

				VmHistorico.GetTarea(UidTarea);
				Guid UidDepartamento = VmHistorico._Tarea.UidDepartamento;
				Guid UidArea = VmHistorico._Tarea.UidArea == null ? Guid.Empty : VmHistorico._Tarea.UidArea;
				Guid UidPeriodicidad = VmHistorico._Tarea.UidPeriodicidad;
				VmHistorico.GetDepartamento(UidDepartamento);
				VmHistorico.GetArea(UidArea);
				VmHistorico.GetTipoFrecuenciaByTarea(UidTarea);
				VmHistorico.GetCumplimientoTarea(UidTarea, Guid.Empty, this.Folio, this.DtFechaInicio, this.DtFechaFin, this.UidEstado);

				string NombreEmpresa = VmHistorico.eEmpresa.StrNombreComercial.ToUpper();
				string Sucursal = VmHistorico.sSucursal.StrNombre.ToUpper();
				string FolioTarea = "Folio de tarea " + VmHistorico._Tarea.IntFolio.ToString("0000");
				string Nombre = VmHistorico._Tarea.StrNombre.ToUpper();
				string Descripcion = string.IsNullOrEmpty(VmHistorico._Tarea.StrDescripcion) ? "(Sin descripción)" : VmHistorico._Tarea.StrDescripcion.ToUpper();
				string Departamento = VmHistorico._Departamento.StrNombre.ToUpper();
				string Area = VmHistorico._Area == null ? "(global)" : VmHistorico._Area.StrNombre.ToUpper();
				string Tipo = VmHistorico._Tarea.StrTipoTarea.ToUpper();
				string Periodicidad = VmHistorico._Frecuencia.StrTipoFrecuencia.ToUpper();

				rvReport.LocalReport.ReportPath = Server.MapPath("~/Reports/ReporteCumplimientosTarea.rdlc");
				ReportParameter[] rpParameters = new ReportParameter[9];
				rpParameters[0] = new ReportParameter("rpNombreEmpresa", NombreEmpresa);
				rpParameters[1] = new ReportParameter("rpNombreSucursal", Sucursal);
				rpParameters[2] = new ReportParameter("rpDocumentTittle", FolioTarea);
				rpParameters[3] = new ReportParameter("rpTareaNombre", Nombre);
				rpParameters[4] = new ReportParameter("rpTareaDescripcion", Descripcion);
				rpParameters[5] = new ReportParameter("rpTareaDepartamento", Departamento);
				rpParameters[6] = new ReportParameter("rpTareaArea", Area);
				rpParameters[7] = new ReportParameter("rpTareaTipo", Tipo);
				rpParameters[8] = new ReportParameter("rpTareaPeriodicidad", Periodicidad);

				rvReport.LocalReport.SetParameters(rpParameters);
				rvReport.LocalReport.DataSources.Add(new ReportDataSource("dsCumplimientos", VmHistorico.LsCumplimientosTarea));
				rvReport.LocalReport.Refresh();
			}
		}
	}
}