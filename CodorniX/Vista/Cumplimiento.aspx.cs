﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class Cumplimiento : System.Web.UI.Page
	{
		private List<Modelo.Cumplimiento> VSTareas
		{
			get => ViewState["Tareas"] as List<Modelo.Cumplimiento>;
			set => ViewState["Tareas"] = value;
		}

		private DateTime? DtProximoCumplimientoTarea
		{
			get
			{
				return ViewState["NextDateTask"] == null ? (DateTime?)null : (DateTime?)ViewState["NextDateTask"];
			}
			set
			{
				ViewState["NextDateTask"] = value;
			}
		}
		private string FrecuenciaPeriodicidadTarea
		{
			get
			{
				return (string)ViewState["FrecuenciaTarea"];
			}
			set
			{
				ViewState["FrecuenciaTarea"] = value;
			}
		}
		private int FolioTareaBusqueda
		{
			get
			{
				return (int)ViewState["FiltroFolioT"];
			}
			set
			{
				ViewState["FiltroFolioT"] = value;
			}
		}
		private VMCumplimiento VM = new VMCumplimiento();
		private Sesion SesionActual => (Sesion)Session["Sesion"];
		private string TipoMedicion
		{
			get
			{
				return (string)ViewState["MedicionTarea"];
			}
			set
			{
				ViewState["MedicionTarea"] = value;
			}
		}

		private bool ModoSupervisor
		{
			get
			{
				if (ViewState["ModoSupervisor"] == null)
					ViewState["ModoSupervisor"] = false;

				return (bool)ViewState["ModoSupervisor"];
			}

			set => ViewState["ModoSupervisor"] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			// No hacer nada si no tiene algun turno iniciado. Master se hace cargo de ello.
			if (SesionActual == null)
				return;

			// No permite acceder en caso de no iniciar una sucursal (solo para administradores).
			if (!SesionActual.uidSucursalActual.HasValue)
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (SesionActual.perfil == "Supervisor" || SesionActual.perfil.Contains("dministrador"))
			{

				// El supervisor no tiene acceso al cumplimiento si no ha tomado  algun turno para cumplir.
				if (SesionActual.UidPeriodos.Count < 0)
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}
				else
				{
					ModoSupervisor = true;
					SesionActual.UidPeriodo = Guid.Empty;

					// El administrador registra sus tareas como propias
					if (!IsPostBack && SesionActual.perfil == "Supervisor")
					{
						panelUsuario.Visible = true;
						rbEncargado.Checked = true;
					}
				}
			}
			else
			{
				if (SesionActual.UidPeriodo == null)
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}
			}

			// Verificar permisos
			if (!Acceso.TieneAccesoAModulo("Cumplimiento", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				this.FolioTareaBusqueda = 0;
				TipoMedicion = string.Empty;
				btnRealizar.Disable();
				btnPosponer.Disable();
				btnCancelarTarea.Disable();
				btnDeshacer.Disable();
				btnDeshacer.Visible = false;
				rbEncargado.Disable();
				rbSupervisor.Disable();
				frmSiNo.Visible = false;
				frmValue.Visible = false;
				frmTwoValues.Visible = false;
				frmOptions.Visible = false;

				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;

				VSTareas = VM.CumplimientosPendientes;

				VM.ObtenerEstados();
				lbEstados.DataSource = VM.Estados;
				lbEstados.DataTextField = "StrEstadoCumplimiento";
				lbEstados.DataValueField = "UidEstadoCumplimiento";
				lbEstados.DataBind();

				if (SesionActual.UidPeriodos.Count > 0)
					VM.ObtenerDepartamentos(SesionActual.UidPeriodos);
				else if (SesionActual.UidPeriodo.Value != Guid.Empty)
					VM.ObtenerDepartamentoPeriodo(SesionActual.UidPeriodo.Value);

				Departamento departamento = new Departamento();
				departamento.UidDepartamento = Guid.Empty;
				departamento.StrNombre = "Todos";

				if (VM.Departamentos == null)
					VM.Departamentos = new List<Departamento>();
				VM.Departamentos.Insert(0, departamento);
				ddDepartamentos.DataSource = VM.Departamentos;
				ddDepartamentos.DataTextField = "StrNombre";
				ddDepartamentos.DataValueField = "UidDepartamento";
				ddDepartamentos.SelectedIndex = 0;
				ddDepartamentos.DataBind();

				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();

				VM.ObtenerTiposTarea();
				TipoTarea tipo = new TipoTarea();
				tipo.UidTipoTarea = Guid.Empty;
				tipo.StrTipoTarea = "Todos";
				VM.TiposTarea.Insert(0, tipo);
				ddTipo.DataSource = VM.TiposTarea;
				ddTipo.DataValueField = "UidTipoTarea";
				ddTipo.DataTextField = "StrTipoTarea";
				ddTipo.SelectedIndex = 0;
				ddTipo.DataBind();

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				var local = horaLocal.DateTime;

				if (SesionActual.Rol.Equals("Encargado"))
				{
					int AuxCompare = 1;
					if (SesionActual.dtFechaInicioTurnoSeleccionado != null)
						AuxCompare = DateTime.Compare(SesionActual.dtFechaInicioTurnoSeleccionado.Value, horaLocal.DateTime.Date);

					if (AuxCompare < 0)
						VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, null, null, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), SesionActual.dtFechaInicioTurnoSeleccionado.Value);
					else
						VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, null, null, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), local);

					VSTareas = VM.CumplimientosPendientes;
				}
				else
				{
					Guid UidUsuario = SesionActual.uidUsuario;
					this.VSTareas = new List<Modelo.Cumplimiento>();
					foreach (var turno in SesionActual.LsPeriodosEncargado)
					{
						VM.ObtenerTareasPeriodo(UidUsuario, SesionActual.UidPeriodo.Value, turno.UidPeriodo.ToString(), this.FolioTareaBusqueda, string.Empty, string.Empty, Guid.Empty, Guid.Empty, Guid.Empty, turno.DtFecha);
						foreach (var item in VM.CumplimientosPendientes)
						{
							VSTareas.Add(item);
						}
					}
				}

				dgvTareasPendientes.DataSource = VSTareas;
				dgvTareasPendientes.DataBind();
			}
		}

		protected void dgvTareasPendientes_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (panelAlert.Visible == true)
			{
				panelAlert.Visible = false;
			}

			btnOK.Enabled = true;
			DateTime? proximo;
			Guid uidTarea = new Guid(dgvTareasPendientes.SelectedDataKey.Value.ToString());
			Guid uidDepto = new Guid(dgvTareasPendientes.SelectedDataKey.Values[1].ToString());
			Guid uidArea = new Guid(dgvTareasPendientes.SelectedDataKey.Values[2].ToString());
			Guid uidCumplimiento = new Guid(dgvTareasPendientes.SelectedDataKey.Values[3].ToString());
			Guid uidPeriodo = new Guid(dgvTareasPendientes.SelectedDataKey.Values[4].ToString());

			fldUidTarea.Value = uidTarea.ToString();
			fldUidDepartamento.Value = uidDepto.ToString();
			fldUidArea.Value = uidArea.ToString();
			fldUidCumplimiento.Value = uidCumplimiento.ToString();
			fldUidPerido.Value = uidPeriodo.ToString();

			btnActualizar.Enable();
			btnOK.Visible = false;
			btnCancelar.Visible = false;

			VM.ObtenerCumplimiento(uidCumplimiento);
			VM.ObtenerTarea(uidTarea);
			VM.ObtenerDepartamento(uidDepto);
			VM.ObtenerArea(uidArea);
			VM.ObtenerPeriocidad(VM.Tarea.UidPeriodicidad);
			VM.ObtenerTipoFrecuencia(VM.Periodicidad.UidTipoFrecuencia);

			lblPeriodicidad.Text = VM.TipoFrecuencia.StrTipoFrecuencia;

			proximo = VM.ObtenerFechaSiguienteTarea(uidTarea, SesionActual.GetDateTime());

			DateTime DtFechaDeHoy = SesionActual.GetDateTime();
			// Nuevo 2019-01-08
			DateTime DtProximoCumplimiento = VM.ObtenerProximaFechaCumplimientoTarea(VM.Tarea.UidPeriodicidad, SesionActual.GetDateTime());
			DtProximoCumplimientoTarea = DtProximoCumplimiento;
			string FrecuenciaPeriodicidad = VM.Periodicidad.Frecuencia;
			FrecuenciaPeriodicidadTarea = FrecuenciaPeriodicidad;

			// Activar modo edición si esta completa
			bool editmode = (VM.Cumplimiento != null && VM.Cumplimiento.StrEstadoCumplimiento == "Completo");

			if (editmode)
			{
				fldEditing.Visible = true;
				fldEditing.Value = "1";
				btnRealizar.Text = "Editar";
			}
			else
			{
				fldEditing.Visible = false;
				fldEditing.Value = string.Empty;
				btnRealizar.Text = "Realizar";
			}

			lblTarea.Text = VM.Tarea.StrNombre;
			lblDepto.Text = VM.Departamento.StrNombre;
			lblArea.Text = VM.Area == null ? "(global)" : VM.Area.StrNombre;

			frmSiNo.Visible = false;
			frmValue.Visible = false;
			frmTwoValues.Visible = false;
			frmOptions.Visible = false;

			confirmarCancelar.Visible = false;
			confirmarPosponer.Visible = false;
			posponerCampos.Visible = false;

			lblErrorTarea.Text = string.Empty;

			rbYes.Checked = false;
			rbNo.Checked = false;
			txtValor.Text = string.Empty;
			txtValor1.Text = string.Empty;
			txtValor2.Text = string.Empty;
			rbSupervisor.Disable();
			rbEncargado.Disable();
			rbYes.Disable();
			rbNo.Disable();
			txtValor.Disable();
			ddOpciones.Disable();
			txtObservaciones.Disable();
			btnPosponer.Disable();

			this.TipoMedicion = VM.Tarea.StrTipoMedicion;
			switch (VM.Tarea.StrTipoMedicion)
			{
				case "Verdadero/Falso":
					frmSiNo.Visible = true;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.BlValor.HasValue)
						{
							if (VM.Cumplimiento.BlValor.Value)
							{
								rbYes.Checked = true;
								rbNo.Checked = false;
							}
							else
							{
								rbNo.Checked = true;
								rbYes.Checked = false;
							}
						}
					}
					break;
				case "Numerico":
					frmValue.Visible = true;
					lblUnidad.Text = VM.Tarea.StrUnidadMedida;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.DcValor1.HasValue)
						{
							txtValor.Text = VM.Cumplimiento.DcValor1.Value.ToString("0.####");
						}
					}
					else
					{
						txtValor.Text = "";
					}
					break;
				case "Seleccionable":
					VM.ObtenerOpcionesDeTarea(uidTarea);
					frmOptions.Visible = true;
					ddOpciones.DataSource = VM.Opciones;
					ddOpciones.DataValueField = "UidOpciones";
					ddOpciones.DataTextField = "StrOpciones";
					ddOpciones.DataBind();
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.UidOpcion.HasValue)
						{
							ddOpciones.SelectedValue = VM.Cumplimiento.UidOpcion.Value.ToString();
						}
					}
					break;
			}
			if (VM.Cumplimiento == null || VM.Cumplimiento.StrEstadoCumplimiento == "No Realizado")
			{
				btnDeshacer.Disable();
				btnDeshacer.Visible = false;
				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();
				DateTime DtMañana = DtFechaDeHoy.AddDays(1);
				int DateCompare = 0;

				if (VM.Tarea.StrTipoTarea == "Requerida" && !ModoSupervisor)
				{
					btnCancelarTarea.Disable();
					btnPosponer.Disable();
				}
				else if (FrecuenciaPeriodicidad.Equals("Sin periodicidad"))
				{
					btnPosponer.Enable();
				}
				else if ((DateCompare = DateTime.Compare(DtMañana, DtProximoCumplimiento)) >= 0)
				{
					btnPosponer.Disable();
				}
				txtObservaciones.Text = string.Empty;
			}
			else if (VM.Cumplimiento.StrEstadoCumplimiento == "Completo")
			{
				btnDeshacer.Enable();
				btnDeshacer.Visible = true;
				btnRealizar.Enable();
				btnCancelarTarea.Disable();
				btnPosponer.Disable();
				if (!string.IsNullOrWhiteSpace(VM.Cumplimiento.StrObservacion))
					txtObservaciones.Text = VM.Cumplimiento.StrObservacion;
				else
					txtObservaciones.Text = string.Empty;
				if (ModoSupervisor)
				{
					if (VM.Cumplimiento.UidUsuario == SesionActual.uidUsuario)
					{
						rbSupervisor.Checked = true;
						rbEncargado.Checked = false;
					}
					else
					{
						rbSupervisor.Checked = false;
						rbEncargado.Checked = true;
					}
				}
			}
			else
			{
				btnDeshacer.Enable();
				btnDeshacer.Visible = true;
				btnRealizar.Disable();
				btnCancelarTarea.Disable();
				btnPosponer.Disable();
				if (!string.IsNullOrWhiteSpace(VM.Cumplimiento.StrObservacion))
					txtObservaciones.Text = VM.Cumplimiento.StrObservacion;
				else
					txtObservaciones.Text = string.Empty;
			}

			int pos = -1;
			if (ViewState["CumplPreviousRow"] != null)
			{
				pos = (int)ViewState["CumplPreviousRow"];
				GridViewRow previousRow = dgvTareasPendientes.Rows[pos];
				previousRow.RemoveCssClass("success-forced");
			}

			if (VM.Cumplimiento != null)
			{
				if (VM.Cumplimiento.UidCreador != Guid.Empty)
				{
					Guid UidCreador = VM.Cumplimiento.UidCreador;
					string RolCreador = VM.Cumplimiento.VchRolCreador;
					if ((SesionActual.Rol.Equals("Supervisor") && RolCreador.Equals("Administrador")) || (SesionActual.Rol.Equals("Encargado") && RolCreador.Equals("Supervisor")) || (SesionActual.Rol.Equals("Encargado") && RolCreador.Equals("Administrador")))
					{
						if (UidCreador != SesionActual.uidUsuario)
						{
							btnRealizar.Disable();
							btnDeshacer.Disable();
							btnPosponer.Disable();
							btnCancelarTarea.Disable();
						}
					}
				}
			}

			ViewState["CumplPreviousRow"] = dgvTareasPendientes.SelectedIndex;
			dgvTareasPendientes.SelectedRow.AddCssClass("success-forced");

			lblError.Text = string.Empty;
			valorUnico.RemoveCssClass("has-error");
		}

		protected void dgvTareasPendientes_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTareasPendientes, "Select$" + e.Row.RowIndex);

				Label icon = e.Row.FindControl("lblCompleto") as Label;

				if (e.Row.Cells[2].Text.Equals("0"))
				{
					e.Row.Cells[2].Text = string.Empty;
				}

				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					e.Row.Cells[5].Text = "(global)";
				}

				if (e.Row.Cells[6].Text == "&nbsp;")
				{
					e.Row.Cells[6].Text = "N/A";
				}

				if (e.Row.Cells[8].Text == "Requerida")
				{
					//if (!e.Row.CssClass.Contains("success"))
					//    e.Row.AddCssClass("danger");
					e.Row.AddCssClass("font-bold");
					e.Row.Cells[3].Text += "*";
				}

				if (e.Row.Cells[7].Text == "&nbsp;" || e.Row.Cells[7].Text == "No Realizado")
				{
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-pencil");
					icon.ToolTip = "No Realizado";
				}
				else if (e.Row.Cells[7].Text == "Completo")
				{
					e.Row.AddCssClass("info");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-ok");
					icon.ToolTip = "Completo";
				}
				else if (e.Row.Cells[7].Text == "Pospuesto")
				{
					e.Row.AddCssClass("warning");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-arrow-right");
					icon.ToolTip = "Pospuesto";
				}
				else if (e.Row.Cells[7].Text == "Cancelado")
				{
					e.Row.AddCssClass("danger");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-remove");
					icon.ToolTip = "Cancelado";
				}
			}
		}

		protected void btnRealizar_Click(object sender, EventArgs e)
		{
			rbYes.Enable();
			rbNo.Enable();
			rbNo.Checked = true;
			rbEncargado.Enable();
			rbSupervisor.Enable();
			rbEncargado.Checked = true;
			txtValor.Enable();
			txtValor.Text = "0";
			ddOpciones.Enable();
			txtObservaciones.Enable();
			btnRealizar.Disable();
			btnOK.Visible = true;
			btnCancelar.Visible = true;

			btnCancelarTarea.Disable();
			btnPosponer.Disable();

			if (this.TipoMedicion != string.Empty)
			{
				switch (this.TipoMedicion)
				{
					case "Verdadero/Falso":
						break;
					case "Numerico":
						txtValor.Focus();
						break;
					case "Seleccionable":
						ddOpciones.Focus();
						break;
					default:
						break;
				}
			}
		}

		protected void btnOK_Click(object sender, EventArgs e)
		{
			try
			{
				if (panelAlert.Visible == true)
				{
					panelAlert.Visible = false;
				}

				btnOK.Enabled = false;
				Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
				Guid uidTarea = new Guid(fldUidTarea.Value);
				Guid uidDepartamento = new Guid(fldUidDepartamento.Value);
				Guid uidArea = new Guid(fldUidArea.Value);

				Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
				Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
				Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;
				bool? estado = null;
				decimal? valor1 = null;
				decimal? valor2 = null;
				Guid? opcion = null;

				lblError.Text = string.Empty;
				valorUnico.RemoveCssClass("has-error");

				VM.ObtenerTarea(uidTarea);

				switch (VM.Tarea.StrTipoMedicion)
				{
					case "Verdadero/Falso":
						if (rbYes.Checked)
							estado = true;
						else if (rbNo.Checked)
							estado = false;
						else
							estado = false;
						break;
					case "Numerico":
						try
						{
							valor1 = Convert.ToDecimal(txtValor.Text);
						}
						catch (FormatException ex)
						{
							lblError.Text = "Solo se aceptan valores numéricos";
							valorUnico.AddCssClass("has-error");
							return;
						}
						break;
					case "Seleccionable":
						opcion = new Guid(ddOpciones.SelectedValue.ToString());
						break;
				}

				Guid uidPeriodo = new Guid(fldUidPerido.Value);
				VM.ObtenerPeriodo(uidPeriodo);

				bool editmode = (fldEditing.Value == "1");
				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));

				try
				{
					if (editmode)
					{
						VM.ActualizarCumplimiento(realCumplimiento.Value, horaLocal, estado, valor1, valor2, opcion, txtObservaciones.Text, null);
					}
					else
					{
						Guid uid = SesionActual.uidUsuario;
						DateTime DtToday = horaLocal.DateTime;
						DateTime DtFechaTurno = DtToday;
						bool IsAtrasada = false;
						int IntFolioTurno;

						if (SesionActual.Rol.Equals("Encargado"))
						{
							DtFechaTurno = SesionActual.dtFechaInicioTurnoSeleccionado.Value;
							IntFolioTurno = SesionActual.FolioTurno.Value;
						}
						else
						{
							DtFechaTurno = SesionActual.LsPeriodosEncargado.Find(p => p.UidPeriodo == uidPeriodo).DtFecha;
							IntFolioTurno = SesionActual.LsPeriodosEncargado.Find(p => p.UidPeriodo == uidPeriodo).IntFolioTurno;
						}

						if (ModoSupervisor && rbEncargado.Checked)
						{
							uid = VM.Periodo.UidUsuario;
						}

						if (SesionActual.Rol.Equals("Administrador") || SesionActual.Rol.Equals("SuperAdministrador"))
							uid = VM.Periodo.UidUsuario;

						int AuxCompare = 1;
						AuxCompare = DateTime.Compare(DtFechaTurno, horaLocal.DateTime.Date);
						IsAtrasada = AuxCompare < 0 ? true : false;
										

						if (IsAtrasada)
						{
							VM.RegistrarCumplimiento(ref realCumplimiento, uidTarea, realDepartamento, realArea, this.DtProximoCumplimientoTarea,
								uid, DtFechaTurno, estado, valor1, valor2, opcion, txtObservaciones.Text, null, VM.Periodo.UidTurno, true, horaLocal.DateTime, SesionActual.uidUsuario, SesionActual.Rol, IntFolioTurno);
						}
						else
						{
							VM.RegistrarCumplimiento(ref realCumplimiento, uidTarea, realDepartamento, realArea, this.DtProximoCumplimientoTarea,
								uid, horaLocal, estado, valor1, valor2, opcion, txtObservaciones.Text, null, VM.Periodo.UidTurno, false, null, SesionActual.uidUsuario, SesionActual.Rol, IntFolioTurno);
						}

						VM.EnviarNotificacion(realCumplimiento.Value);
					}
				}
				catch (Exception ex)
				{
					btnOK.Enabled = true;
					lblErrorTarea.Text = ex.Message;
					panelAlert.Visible = true;
				}

				rbYes.Disable();
				rbNo.Disable();
				txtValor.Disable();
				ddOpciones.Disable();
				txtObservaciones.Disable();
				btnOK.Visible = false;
				btnOK.Enabled = true;
				btnCancelar.Visible = false;

				btnRealizar.Disable();
				btnCancelarTarea.Disable();
				btnPosponer.Disable();

				string lpr = null;
				int[] i = lbEstados.GetSelectedIndices();
				if (i.Length > 0)
				{
					lpr = lbEstados.Items[i[0]].Value.ToString();
					for (int j = 1; j < i.Length; j++)
					{
						lpr += "," + lbEstados.Items[i[j]].Value.ToString();
					}
				}
				DateTime real = horaLocal.DateTime;

				if (SesionActual.Rol.Equals("Encargado"))
				{
					int AuxCompare1 = 1;
					if (SesionActual.dtFechaInicioTurnoSeleccionado != null)
						AuxCompare1 = DateTime.Compare(SesionActual.dtFechaInicioTurnoSeleccionado.Value, horaLocal.DateTime.Date);

					if (AuxCompare1 < 0)
						VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
							 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), SesionActual.dtFechaInicioTurnoSeleccionado.Value);
					else
						VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
							 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), real);
					VSTareas = VM.CumplimientosPendientes;
				}
				else
				{
					Guid UidUsuario = SesionActual.uidUsuario;
					this.VSTareas = new List<Modelo.Cumplimiento>();
					foreach (var turno in SesionActual.LsPeriodosEncargado)
					{
						VM.ObtenerTareasPeriodo(UidUsuario, SesionActual.UidPeriodo.Value, turno.UidPeriodo.ToString(), this.FolioTareaBusqueda, txtNombre.Text, lpr,
							 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), turno.DtFecha);
						foreach (var item in VM.CumplimientosPendientes)
						{
							VSTareas.Add(item);
						}
					}
				}

				dgvTareasPendientes.DataSource = VSTareas;
				dgvTareasPendientes.DataBind();

				ViewState["CumplPreviousRow"] = null;
			}
			catch (Exception ex)
			{
				lblErrorTarea.Text = ex.Message;
				panelAlert.Visible = true;
			}
		}

		protected void btnCancelar_Click(object sender, EventArgs e)
		{
			rbYes.Disable();
			rbNo.Disable();
			txtValor.Disable();
			ddOpciones.Disable();
			btnRealizar.Enable();
			btnOK.Visible = false;
			btnCancelar.Visible = false;
			lblError.Text = string.Empty;
			valorUnico.RemoveCssClass("has-error");

			txtObservaciones.Disable();
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			frmSiNo.Visible = false;
			frmValue.Visible = false;
			frmTwoValues.Visible = false;
			frmOptions.Visible = false;
			int FolioTarea = 0;
			this.FolioTareaBusqueda = 0;

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}
			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
			DateTime real = horaLocal.DateTime;

			if (!string.IsNullOrEmpty(txtFolioTarea.Text.Trim()))
			{
				int Aux = 0;
				if (int.TryParse(txtFolioTarea.Text.Trim(), out Aux))
				{
					FolioTarea = Aux;
					this.FolioTareaBusqueda = FolioTarea;
				}
			}

			if (SesionActual.Rol.Equals("Encargado"))
			{
				int AuxCompare = 1;
				if (SesionActual.dtFechaInicioTurnoSeleccionado != null)
					AuxCompare = DateTime.Compare(SesionActual.dtFechaInicioTurnoSeleccionado.Value, horaLocal.DateTime);

				if (AuxCompare < 0)
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
					 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), SesionActual.dtFechaInicioTurnoSeleccionado.Value);
				else
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
					 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), real);

				VSTareas = VM.CumplimientosPendientes;
			}
			else
			{
				Guid UidUsuario = SesionActual.uidUsuario;
				this.VSTareas = new List<Modelo.Cumplimiento>();
				foreach (var turno in SesionActual.LsPeriodosEncargado)
				{
					VM.ObtenerTareasPeriodo(UidUsuario, SesionActual.UidPeriodo.Value, turno.UidPeriodo.ToString(), this.FolioTareaBusqueda, txtNombre.Text, lpr,
					 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), turno.DtFecha);
					foreach (var item in VM.CumplimientosPendientes)
					{
						VSTareas.Add(item);
					}
				}
			}
			dgvTareasPendientes.DataSource = VSTareas;
			dgvTareasPendientes.DataBind();

			lblError.Text = string.Empty;
			valorUnico.RemoveCssClass("has-error");

			ViewState["CumplPreviousRow"] = null;

			btnMostrar.Text = "Ocultar";
			panelBusqueda.Visible = false;
			panelResultados.Visible = true;
			btnLimpiar.Visible = false;
			btnActualizar.Visible = false;
		}

		protected void btnCancelarTarea_Click(object sender, EventArgs e)
		{
			confirmarCancelar.Visible = true;
			confirmarPosponer.Visible = false;

			btnRealizar.Disable();
			btnCancelarTarea.Disable();
			btnPosponer.Disable();

			txtObservaciones.Enable();
			txtObservaciones.Text = string.Empty;
		}

		protected void btnPosponer_Click(object sender, EventArgs e)
		{
			confirmarCancelar.Visible = false;
			confirmarPosponer.Visible = true;

			cumplimientoCampos.Visible = false;
			posponerCampos.Visible = true;

			//DateTime? proximo = VM.ObtenerFechaSiguienteTarea(new Guid(fldUidTarea.Value), DateTime.Now);

			DateTime? proximo = this.DtProximoCumplimientoTarea;
			VM.ObtenerPeriodo(SesionActual.UidPeriodo.Value);

			if (proximo == null || proximo.Value > VM.Periodo?.DtFechaFin)
				proximo = VM.Periodo.DtFechaFin;
			if (proximo == null)
				proximo = DateTime.MaxValue;

			if (FrecuenciaPeriodicidadTarea.Equals("Sin periodicidad"))
				proximo = DateTime.MaxValue;

			string script = "setStartDateLimit('" + DateTime.Today.AddDays(1.0).ToString("dd/MM/yyyy") + "', '" + proximo?.AddDays(-1).ToString("dd/MM/yyyy") + "')";
			ScriptManager.RegisterStartupScript(this, typeof(Page), "UpdateStartDate", script, true);

			btnRealizar.Disable();
			btnCancelarTarea.Disable();
			btnPosponer.Disable();

			txtFecha.Text = string.Empty;
			txtObservaciones.Enable();
			txtObservaciones.Text = string.Empty;
		}

		protected void btnCancelarCancelar_Click(object sender, EventArgs e)
		{
			confirmarCancelar.Visible = false;
			txtObservaciones.Disable();
			txtObservaciones.Text = string.Empty;

			btnRealizar.Enable();
			btnCancelarTarea.Enable();
			btnPosponer.Enable();
		}

		protected void btnOKCancelar_Click(object sender, EventArgs e)
		{
			if (panelAlert.Visible)
				panelAlert.Visible = false;

			if (txtObservaciones.Text.Trim() == string.Empty)
			{
				panelAlert.Visible = true;
				lblErrorTarea.Text = "Comentarios Requeridos.";
				return;
			}

			Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
			Guid uidTarea = new Guid(fldUidTarea.Value);
			Guid uidDepartamento = new Guid(fldUidDepartamento.Value);
			Guid uidArea = new Guid(fldUidArea.Value);
			Guid UidPeriodo = new Guid(fldUidPerido.Value);

			Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
			Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
			Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;

			VM.ObtenerPeriodo(UidPeriodo);

			string msg = string.Empty;

			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));

			DateTime DtToday = horaLocal.DateTime;
			DateTime DtFechaTurno = DtToday;
			bool IsAtrasada = false;
			int FolioTurno;

			if (SesionActual.Rol.Equals("Encargado"))
			{
				DtFechaTurno = SesionActual.dtFechaInicioTurnoSeleccionado.Value;
				FolioTurno = SesionActual.FolioTurno.Value;
			}
			else
			{
				DtFechaTurno = SesionActual.LsPeriodosEncargado.Find(p => p.UidPeriodo == UidPeriodo).DtFecha;
				FolioTurno = SesionActual.LsPeriodosEncargado.Find(p => p.UidPeriodo == UidPeriodo).IntFolioTurno;
			}
			int AuxCompare = 1;
			AuxCompare = DateTime.Compare(DtFechaTurno, horaLocal.DateTime.Date);
			IsAtrasada = AuxCompare < 0 ? true : false;

			int result = 3;
			if (IsAtrasada)
			{
				result = VM.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea,
				VM.Periodo.UidUsuario, null, VMCumplimiento.ModoCambio.Cancelar, DtFechaTurno, txtObservaciones.Text, true, horaLocal, SesionActual.uidUsuario, SesionActual.Rol,FolioTurno);
			}
			else
			{
				result = VM.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea,
				VM.Periodo.UidUsuario, null, VMCumplimiento.ModoCambio.Cancelar, horaLocal, txtObservaciones.Text, false, null, SesionActual.uidUsuario, SesionActual.Rol,FolioTurno);
			}

			if (result == 1)
			{
				msg = "La fecha nueva no corresponde a un periodo del usuario actual";
				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarCancelar.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}
			else if (result == 2)
			{
				msg = "La tarea es requerida y no puede cancelarse";

				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarCancelar.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}

			if (realCumplimiento != null)
				VM.NotificacionIncumplimiento(realCumplimiento.Value, "Cancelada");

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}

			if (SesionActual.Rol.Equals("Encargado"))
			{
				int AuxCompare1 = 1;
				if (SesionActual.dtFechaInicioTurnoSeleccionado != null)
					AuxCompare1 = DateTime.Compare(SesionActual.dtFechaInicioTurnoSeleccionado.Value, horaLocal.DateTime.Date);

				if (AuxCompare1 < 0)
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
						 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), SesionActual.dtFechaInicioTurnoSeleccionado.Value);
				else
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
						 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), horaLocal.Date);
				VSTareas = VM.CumplimientosPendientes;
			}
			else
			{
				Guid UidUsuario = SesionActual.uidUsuario;
				this.VSTareas = new List<Modelo.Cumplimiento>();
				foreach (var turno in SesionActual.LsPeriodosEncargado)
				{
					VM.ObtenerTareasPeriodo(UidUsuario, SesionActual.UidPeriodo.Value, turno.UidPeriodo.ToString(), this.FolioTareaBusqueda, txtNombre.Text, lpr,
							 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), turno.DtFecha);
					foreach (var item in VM.CumplimientosPendientes)
					{
						VSTareas.Add(item);
					}
				}
			}

			dgvTareasPendientes.DataSource = VSTareas;
			dgvTareasPendientes.DataBind();

			ViewState["CumplPreviousRow"] = null;

			lblErrorTarea.Text = string.Empty;

			confirmarCancelar.Visible = false;
			btnRealizar.Enable();
			btnCancelarTarea.Enable();
			btnPosponer.Enable();
		}

		protected void btnCancelarPosponer_Click(object sender, EventArgs e)
		{
			confirmarPosponer.Visible = false;

			cumplimientoCampos.Visible = true;
			posponerCampos.Visible = false;

			btnRealizar.Enable();
			btnCancelarTarea.Enable();
			btnPosponer.Enable();

			txtObservaciones.Disable();
			txtObservaciones.Text = string.Empty;
		}

		protected void btnOKPosponer_Click(object sender, EventArgs e)
		{
			if (panelAlert.Visible)
				panelAlert.Visible = false;

			if (txtObservaciones.Text.Trim() == string.Empty)
			{
				panelAlert.Visible = true;
				lblErrorTarea.Text = "Comentarios Requeridos.";
				return;
			}

			Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);
			Guid uidTarea = new Guid(fldUidTarea.Value);
			Guid uidDepartamento = new Guid(fldUidDepartamento.Value);
			Guid uidArea = new Guid(fldUidArea.Value);

			Guid UidPeriodo = new Guid(fldUidPerido.Value);

			Guid? realCumplimiento = uidCumplimiento == Guid.Empty ? (Guid?)null : uidCumplimiento;
			Guid? realDepartamento = uidDepartamento == Guid.Empty ? (Guid?)null : uidDepartamento;
			Guid? realArea = uidArea == Guid.Empty ? (Guid?)null : uidArea;
			DateTime fecha;
			VM.ObtenerPeriodo(UidPeriodo);

			try
			{
				fecha = Convert.ToDateTime(DateTime.ParseExact(txtFecha.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				panelAlert.Visible = true;
				lblErrorTarea.Text = "Ingrese una fecha valida";
				return;
			}

			string msg = string.Empty;

			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
			DateTime real = horaLocal.DateTime;

			DateTime DtToday = horaLocal.DateTime;
			DateTime DtFechaTurno = DtToday;
			bool IsAtrasada = false;
			int FolioTurno;
			if (SesionActual.Rol.Equals("Encargado"))
			{
				DtFechaTurno = SesionActual.dtFechaInicioTurnoSeleccionado.Value;
				FolioTurno = SesionActual.FolioTurno.Value;
			}
			else
			{
				DtFechaTurno = SesionActual.LsPeriodosEncargado.Find(p => p.UidPeriodo == UidPeriodo).DtFecha;
				FolioTurno = SesionActual.LsPeriodosEncargado.Find(p => p.UidPeriodo == UidPeriodo).IntFolioTurno;
			}
			int AuxCompare = 1;
			AuxCompare = DateTime.Compare(DtFechaTurno, horaLocal.DateTime.Date);
			IsAtrasada = AuxCompare < 0 ? true : false;

			int result = 3;
			if (IsAtrasada)
			{
				result = VM.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea,
				VM.Periodo.UidUsuario, fecha, VMCumplimiento.ModoCambio.Posponer, DtFechaTurno, txtObservaciones.Text, true, horaLocal, SesionActual.uidUsuario, SesionActual.Rol, FolioTurno);
			}
			else
			{
				result = VM.CambiarCumplimiento(realCumplimiento, uidTarea, realDepartamento, realArea,
				VM.Periodo.UidUsuario, fecha, VMCumplimiento.ModoCambio.Posponer, real, txtObservaciones.Text, false, null, SesionActual.uidUsuario, SesionActual.Rol, FolioTurno);
			}



			if (result == 1)
			{
				msg = "La fecha nueva no corresponde a un periodo del usuario actual";
				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarPosponer.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}
			else if (result == 2)
			{
				msg = "La tarea es requerida y no puede posponerse";
				lblErrorTarea.Text = msg;
				panelAlert.Visible = true;
				confirmarPosponer.Visible = false;

				btnRealizar.Enable();
				btnCancelarTarea.Enable();
				btnPosponer.Enable();

				return;
			}

			if (realCumplimiento != null)
				VM.NotificacionIncumplimiento(realCumplimiento.Value, "Pospuesta para el dia " + fecha.ToString("dd/MM/yyyy"));

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}

			if (SesionActual.Rol.Equals("Encargado"))
			{
				int AuxCompare1 = 1;
				if (SesionActual.dtFechaInicioTurnoSeleccionado != null)
					AuxCompare1 = DateTime.Compare(SesionActual.dtFechaInicioTurnoSeleccionado.Value, horaLocal.DateTime.Date);

				if (AuxCompare1 < 0)
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
						 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), SesionActual.dtFechaInicioTurnoSeleccionado.Value);
				else
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos, this.FolioTareaBusqueda, txtNombre.Text, lpr,
						 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), real);
				VSTareas = VM.CumplimientosPendientes;
			}
			else
			{
				Guid UidUsuario = SesionActual.uidUsuario;
				this.VSTareas = new List<Modelo.Cumplimiento>();
				foreach (var turno in SesionActual.LsPeriodosEncargado)
				{
					VM.ObtenerTareasPeriodo(UidUsuario, SesionActual.UidPeriodo.Value, turno.UidPeriodo.ToString(), this.FolioTareaBusqueda, txtNombre.Text, lpr,
							 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), turno.DtFecha);
					foreach (var item in VM.CumplimientosPendientes)
					{
						VSTareas.Add(item);
					}
				}
			}
			dgvTareasPendientes.DataSource = VSTareas;
			dgvTareasPendientes.DataBind();

			ViewState["CumplPreviousRow"] = null;

			lblErrorTarea.Text = string.Empty;

			cumplimientoCampos.Visible = true;
			posponerCampos.Visible = false;
			confirmarPosponer.Visible = false;

			btnRealizar.Disable();
			btnCancelarTarea.Disable();
			btnPosponer.Disable();
		}

		protected void btnDeshacer_Click(object sender, EventArgs e)
		{
			Guid uidCumplimiento = new Guid(fldUidCumplimiento.Value);

			VM.EnviarDeshacerNotificacion(uidCumplimiento);
			VM.DeshacerCumplimiento(uidCumplimiento);

			string lpr = null;
			int[] i = lbEstados.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbEstados.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbEstados.Items[i[j]].Value.ToString();
				}
			}

			/*
			DateTimeOffset time = Hora.ObtenerHoraServidor();
			DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
			var local = horaLocal.DateTime;
			*/
			if (SesionActual.Rol.Equals("Encargado"))
			{
				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));

				if (SesionActual.dtFechaInicioTurnoSeleccionado == null)
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos,
						this.FolioTareaBusqueda, txtNombre.Text, lpr, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), horaLocal.DateTime);
				else
					VM.ObtenerTareasDeHoy(SesionActual.uidUsuario, SesionActual.UidPeriodo.Value, SesionActual.UidPeriodos,
						this.FolioTareaBusqueda, txtNombre.Text, lpr, new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), SesionActual.dtFechaInicioTurnoSeleccionado.Value);
				VSTareas = VM.CumplimientosPendientes;
			}
			else
			{
				Guid UidUsuario = SesionActual.uidUsuario;
				this.VSTareas = new List<Modelo.Cumplimiento>();
				foreach (var turno in SesionActual.LsPeriodosEncargado)
				{
					VM.ObtenerTareasPeriodo(UidUsuario, SesionActual.UidPeriodo.Value, turno.UidPeriodo.ToString(), this.FolioTareaBusqueda, txtNombre.Text, lpr,
							 new Guid(ddDepartamentos.SelectedValue), new Guid(ddAreas.SelectedValue), new Guid(ddTipo.SelectedValue), turno.DtFecha);
					foreach (var item in VM.CumplimientosPendientes)
					{
						VSTareas.Add(item);
					}
				}
			}

			dgvTareasPendientes.DataSource = VSTareas;
			dgvTareasPendientes.DataBind();

			ViewState["CumplPreviousRow"] = null;

			lblErrorTarea.Text = string.Empty;

			btnRealizar.Disable();
			btnCancelarTarea.Disable();
			btnPosponer.Disable();
			btnDeshacer.Disable();

			lblTarea.Text = "(ninguno)";
			lblArea.Text = "(ninguno)";
			lblDepto.Text = "(ninguno)";
		}

		protected void btnCloseAlert_Click(object sender, EventArgs e)
		{
			panelAlert.Visible = false;
			lblErrorTarea.Text = string.Empty;
		}

		protected void dgvTareasPendientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
			{
				string SortExpression = (string)ViewState["SortColumn"];
				SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
				SortTareas(SortExpression, SortDirection, true);
			}
			else
			{
				dgvTareasPendientes.DataSource = VSTareas;
			}
			dgvTareasPendientes.PageIndex = e.NewPageIndex;
			dgvTareasPendientes.DataBind();
		}

		protected void dgvTareasPendientes_Sorting(object sender, GridViewSortEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			SortTareas(e.SortExpression, e.SortDirection, false);
			dgvTareasPendientes.DataBind();
		}

		private void SortTareas(string SortExpression, SortDirection SortDirection, bool same = false)
		{
			if (SortExpression == (string)ViewState["SortColumn"] && !same)
			{
				// We are resorting the same column, so flip the sort direction
				SortDirection =
					((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
					SortDirection.Descending : SortDirection.Ascending;
			}

			if (SortExpression == "Folio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolioTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolioTarea).ToList();
				}
			}
			if (SortExpression == "FolioCumpl")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolio).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolio).ToList();
				}
			}
			if (SortExpression == "Tarea")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrTarea).ToList();
				}
			}
			else if (SortExpression == "Departamento")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}
			else if (SortExpression == "Area")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrArea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrArea).ToList();
				}
			}
			else if (SortExpression == "Estado")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
			}
			else if (SortExpression == "Orden")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy((x => x.OrdenTarea)).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending((x => x.OrdenTarea)).ToList();
				}
			}

			dgvTareasPendientes.DataSource = VSTareas;
			ViewState["SortColumn"] = SortExpression;
			ViewState["SortColumnDirection"] = SortDirection;
		}

		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			txtFolioTarea.Text = string.Empty;
			txtNombre.Text = "";
			lbEstados.ClearSelection();
			ddAreas.SelectedIndex = 0;
			ddDepartamentos.SelectedIndex = 0;
			ddTipo.SelectedIndex = 0;
		}

		private class EstadoComparator : IComparer<string>
		{
			private int GetValue(string s)
			{
				if (s == "No Realizado")
					return -1;
				else if (s == "Completado")
					return 3;
				else if (s == "Cancelado")
					return 2;
				else if (s == "Pospuesto")
					return 1;
				else
					return 10;
			}
			public int Compare(string x, string y)
			{
				return GetValue(x) - GetValue(y);
			}
		}

		protected void btnMostrar_Click(object sender, EventArgs e)
		{
			if (btnMostrar.Text == "Mostrar")
			{
				btnMostrar.Text = "Ocultar";
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;
			}
			else
			{
				btnMostrar.Text = "Mostrar";
				panelBusqueda.Visible = true;
				panelResultados.Visible = false;
				btnLimpiar.Visible = true;
				btnActualizar.Visible = true;
			}
		}

		protected void ddDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid selected = new Guid(ddDepartamentos.SelectedValue);
			if (selected != Guid.Empty)
			{
				VM.ObtenerAreas(selected);
				Area area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000001"); // General
				area.StrNombre = "General";
				VM.Areas.Insert(0, area);
				area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000000"); // Todos
				area.StrNombre = "Todos";
				VM.Areas.Insert(0, area);
				ddAreas.DataSource = VM.Areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.DataBind();

				ddAreas.SelectedIndex = 0;
			}
			else
			{
				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();
			}
		}
	}
}