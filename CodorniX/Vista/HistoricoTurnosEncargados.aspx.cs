﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class HistoricoTurnosEncargados : System.Web.UI.Page
	{
		private VMHistoricoTurnosEncargados VmHistorico = new VMHistoricoTurnosEncargados();
		private Sesion SesionActual => (Sesion)Session["Sesion"];
		private List<TurnoIniciado> LsTurnos
		{
			get
			{
				return (List<TurnoIniciado>)ViewState["TurnosEncargados"];
			}
			set
			{
				ViewState["TurnosEncargados"] = value;
			}
		}
		private string DepartamentosAsignados
		{
			get
			{
				return (string)ViewState["DepartamentosAsign"];
			}
			set
			{
				ViewState["DepartamentosAsign"] = value;
			}
		}

		private Guid UidTurno
		{
			get
			{
				return (Guid)ViewState["TurnoUid"];
			}
			set
			{
				ViewState["TurnoUid"] = value;
			}
		}
		private Guid UidDepartamentoSeleccionado
		{
			get
			{
				return (Guid)ViewState["DeptoSeleccionado"];
			}
			set
			{
				ViewState["DeptoSeleccionado"] = value;
			}
		}
		private Guid UidUsuarioSeleccionad
		{
			get
			{
				return (Guid)ViewState["UsrSeleccionado"];
			}
			set
			{
				ViewState["UsrSeleccionado"] = value;
			}
		}
		private Guid UidSucursalSeleccionado
		{
			get
			{
				return (Guid)ViewState["SucSeleccionado"];
			}
			set
			{
				ViewState["SucSeleccionado"] = value;
			}
		}
		private Guid UidPeriodoSeleccionado
		{
			get
			{
				return (Guid)ViewState["PeriodoSeleccionado"];
			}
			set
			{
				ViewState["PeriodoSeleccionado"] = value;
			}
		}
		private DateTime DtFechaSeleccionado
		{
			get
			{
				return (DateTime)ViewState["FechaSeleccionado"];
			}
			set
			{
				ViewState["FechaSeleccionado"] = value;
			}
		}
		private bool IsClosedSelected
		{
			get
			{
				return (bool)ViewState["IsClosedSelected"];
			}
			set
			{
				ViewState["IsClosedSelected"] = value;
			}
		}
		private int FolioTurnoSeleccionado
		{
			get
			{
				return (int)ViewState["FolioTurno"];
			}
			set
			{
				ViewState["FolioTurno"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual.uidEmpresaActual == null)
			{
				pnlBotonesFiltro.Visible = false;
				DisplayDialogSearch("No tiene una empresa asignada", "alert");
				return;
			}

			if (!IsPostBack)
			{
				UidTurno = Guid.Empty;
				FolioTurnoSeleccionado = 0;
				pnlErrorBusqueda.Visible = false;
				pnlAlertGeneral.Visible = false;
				pnlResumen.Visible = true;
				pnlCompletadas.Visible = false;
				pnlNoCompletadas.Visible = false;
				pnlRequeridas.Visible = false;
				pnlCanceladas.Visible = false;
				pnlPospuestas.Visible = false;

				pnlDetallesTarea.Visible = false;
				pnlDetalleRevisión.Visible = false;
				HideShowFilters("Ocultar");
				DepartamentosAsignados = string.Empty;
				btnPrintReport.CssClass = "btn btn-sm btn-default disabled";


				if (SesionActual.Rol.Equals("Administrador"))
				{
					pnlFiltrosAdministrador.Visible = true;

					//VmHistorico.Search(string.Empty, null, null, null, Guid.Empty, Guid.Empty, SesionActual.uidEmpresaActual.Value);
					//this.LsTurnos = VmHistorico.LsTurnos;
					//gvTurnosEncargados.DataSource = VmHistorico.LsTurnos;
					//gvTurnosEncargados.DataBind();

					VmHistorico.ObtenerSucursalesEmpresa(SesionActual.uidEmpresaActual.Value);
					ddlSucursal.DataSource = VmHistorico.LsSucursales;
					ddlSucursal.DataTextField = "StrNombre";
					ddlSucursal.DataValueField = "UidSucursal";
					ddlSucursal.DataBind();

					VmHistorico.ObtenerEncargadosEmpresa(SesionActual.uidEmpresaActual.Value);
					ddlEncargado.DataSource = VmHistorico.LsEncargados;
					ddlEncargado.DataTextField = "STRNOMBRE";
					ddlEncargado.DataValueField = "UIDUSUARIO";
					ddlEncargado.DataBind();

					this.IsClosedSelected = true;
					this.UidPeriodoSeleccionado = Guid.Empty;
					this.DtFechaSeleccionado = DateTime.Today;
				}
				else
				{
					pnlFiltrosAdministrador.Visible = false;
					if (SesionActual.UidDepartamentos.Count > 0)
					{
						foreach (Guid item in SesionActual.UidDepartamentos)
						{
							if (DepartamentosAsignados == string.Empty)
								DepartamentosAsignados = item.ToString();
							else
								DepartamentosAsignados += "," + item.ToString();
						}

						//VmHistorico.Search(DepartamentosAsignados, null, null, null, Guid.Empty, Guid.Empty, SesionActual.uidEmpresaActual.Value);
						//this.LsTurnos = VmHistorico.LsTurnos;
						//gvTurnosEncargados.DataSource = VmHistorico.LsTurnos;
						//gvTurnosEncargados.DataBind();

						this.IsClosedSelected = true;
						this.UidPeriodoSeleccionado = Guid.Empty;
						this.DtFechaSeleccionado = DateTime.Today;
					}
					else
					{
						DisplayDialogSearch("No tiene asignaciones disponibles", "danger");
						btnBuscar.CssClass = "btn btn-sm btn-default disabled";
					}
				}

				HideShowFilters("Mostrar");
			}
		}

		#region Panel Izquierdo
		#region Buttons
		protected void btnOcultarFiltros_Click(object sender, EventArgs e)
		{
			HideShowFilters(lblOcultarFiltros.Text);
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroFolioTurno.Text = string.Empty;
			txtFiltroFechaInicio.Text = string.Empty;
			txtFiltroFechaFin.Text = string.Empty;

			if (SesionActual.Rol.Equals("Administrador"))
			{
				ddlSucursal.SelectedIndex = 0;
				ddlEncargado.SelectedIndex = 0;
			}
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			int? FolioTurno = null;
			DateTime? DtFechaInicio = null;
			DateTime? DtFechaFin = null;
			Guid UidSucursal = Guid.Empty;
			Guid UidEncargado = Guid.Empty;

			if (txtFiltroFolioTurno.Text.Trim() != string.Empty)
			{
				int Aux = 0;
				if (int.TryParse(txtFiltroFolioTurno.Text.Trim(), out Aux))
				{
					FolioTurno = Aux;
				}
				else
				{
					DisplayDialogSearch("Error: Ingrese un folio numerico.", "warning");
					return;
				}
			}
			if (txtFiltroFechaInicio.Text.Trim() != string.Empty)
			{
				try
				{
					DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayDialogSearch("Error: Formato de fecha de (inicio) invalido.", "warning");
					return;
				}
			}
			if (txtFiltroFechaFin.Text.Trim() != string.Empty)
			{
				try
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayDialogSearch("Error: Formato de fecha (fin) invalido.", "warning");
					return;
				}
			}

			if (SesionActual.Rol.Equals("Administrador"))
			{
				UidSucursal = new Guid(ddlSucursal.SelectedValue.ToString());
				UidEncargado = new Guid(ddlEncargado.SelectedValue.ToString());
			}

			try
			{
				VmHistorico.Search(DepartamentosAsignados, FolioTurno, DtFechaInicio, DtFechaFin, UidSucursal, UidEncargado, SesionActual.uidEmpresaActual.Value);
				this.LsTurnos = VmHistorico.LsTurnos;
				gvTurnosEncargados.DataSource = VmHistorico.LsTurnos;
				gvTurnosEncargados.DataBind();
				HideShowFilters("Ocultar");
			}
			catch (Exception ex)
			{
				DisplayDialogSearch("Error: " + ex.Message, "warning");
				return;
			}
		}
		private void HideShowFilters(string ToShow)
		{
			if (ToShow.Equals("Ocultar"))
			{
				lblOcultarFiltros.Text = "Mostrar";
				pnlFiltrosBusqueda.Visible = false;
				pnlListaTurnos.Visible = true;

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
			}
			else
			{
				lblOcultarFiltros.Text = "Ocultar";
				pnlFiltrosBusqueda.Visible = true;
				pnlListaTurnos.Visible = false;

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";
			}
		}

		protected void btnCerrarDetalleTarea_Click(object sender, EventArgs e)
		{
			pnlDetallesTarea.Visible = false;
			pnlDetalleRevisión.Visible = false;
			pnlBotonesFiltro.Visible = true;

			if (lblOcultarFiltros.Text.Equals("Mostrar"))
				HideShowFilters("Ocultar");
			else
				HideShowFilters("Mostrar");
		}

		protected void CloseAlertDialogBusqueda_Click(object sender, EventArgs e)
		{
			pnlErrorBusqueda.Visible = false;
		}
		#endregion

		#region GridView
		protected void gvTurnosEncargados_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTurnosEncargados, "Select$" + e.Row.RowIndex);


				if (UidTurno != Guid.Empty)
				{
					if (gvTurnosEncargados.DataKeys[e.Row.RowIndex].Value.ToString().Equals(this.UidTurno.ToString()))
					{
						e.Row.RowState = DataControlRowState.Selected;
					}
				}
			}
		}
		protected void gvTurnosEncargados_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertGeneral.Visible)
					pnlAlertGeneral.Visible = false;

				Guid UidTurnoIniciado = new Guid(gvTurnosEncargados.SelectedDataKey.Value.ToString());
				UidTurno = UidTurnoIniciado;

				VmHistorico.EncontrarTurnoIniciado(UidTurnoIniciado);
				btnPrintReport.CssClass = "btn btn-sm btn-default";
				Guid UidDepartamento = VmHistorico.tiTurnoIniciado.UidDepartamento;
				Guid UidUsuario = VmHistorico.tiTurnoIniciado.UidUsuario;
				Guid UidSucursal = VmHistorico.tiTurnoIniciado.UidSucursal;
				DateTime DtFecha = VmHistorico.tiTurnoIniciado.DtoFechaInicio.Value.Date;


				this.UidPeriodoSeleccionado = VmHistorico.tiTurnoIniciado.UidPeriodo;
				this.DtFechaSeleccionado = VmHistorico.tiTurnoIniciado.DtoFechaInicio.Value.DateTime;
				this.IsClosedSelected = VmHistorico.tiTurnoIniciado.DtoFechaFin == null ? false : true;
				this.UidDepartamentoSeleccionado = UidDepartamento;
				this.UidUsuarioSeleccionad = UidUsuario;
				this.UidSucursalSeleccionado = UidSucursal;

				FolioTurnoSeleccionado = VmHistorico.tiTurnoIniciado.Folio;
				txtUsuario.Text = VmHistorico.tiTurnoIniciado.Usuario;
				txtDepartamento.Text = VmHistorico.tiTurnoIniciado.Departamento;
				txtTurno.Text = VmHistorico.tiTurnoIniciado.Turno;
				txtFechaInicio.Text = VmHistorico.tiTurnoIniciado.DtoFechaInicio.Value.ToString("t");
				txtFechaFin.Text = VmHistorico.tiTurnoIniciado.DtoFechaFin == null ? "(no cerrado)" : VmHistorico.tiTurnoIniciado.DtoFechaFin.Value.ToString("t");
				txtNoTareasCumplidas.Text = string.Empty;
				txtNoTareasNoCumplidas.Text = string.Empty;
				//txtNoTareasRequeridaNC.Text = string.Empty;
				txtEstado.Text = VmHistorico.tiTurnoIniciado.EstadoTurno;

				VmHistorico.ObtenerTareasCumplidasTurno(UidDepartamento, UidUsuario, DtFecha, string.Empty, string.Empty);
				lblCountCompletadas.Text = VmHistorico.LsTareasCompletadas.Count.ToString();
				txtNoTareasCumplidas.Text = VmHistorico.LsTareasCompletadas.Count.ToString();
				gvTareasCompletadas.SelectedIndex = -1;
				gvTareasCompletadas.DataSource = VmHistorico.LsTareasCompletadas;
				gvTareasCompletadas.DataBind();

				VmHistorico.ObtenerTareasNoCumplidasTurno(UidDepartamento, UidUsuario, DtFecha, UidSucursal, string.Empty, string.Empty);
				lblCountNoCompletadas.Text = VmHistorico.LsTareasNoCompletadas.Count.ToString();
				txtNoTareasNoCumplidas.Text = VmHistorico.LsTareasNoCompletadas.Count.ToString();
				gvTareasNoCompletadas.DataSource = VmHistorico.LsTareasNoCompletadas;
				gvTareasNoCompletadas.DataBind();

				//VmHistorico.ObtenerTareasRequeridasNoCumplidas(UidDepartamento, UidUsuario, DtFecha, UidSucursal);
				//txtNoTareasRequeridaNC.Text = VmHistorico.LsTareasRequeridasNoCompletadas.Count.ToString();

				VmHistorico.ObtenerTareasRequeridasTurno(UidDepartamento, UidUsuario, DtFecha, UidSucursal, string.Empty, string.Empty);
				lblCountRequeridas.Text = VmHistorico.LsTareasRequeridas.Count.ToString();
				gvTareasRequeridas.DataSource = VmHistorico.LsTareasRequeridas;
				gvTareasRequeridas.DataBind();

				VmHistorico.ObtenerTareasCanceladasTurno(UidDepartamento, UidUsuario, DtFecha, UidSucursal, string.Empty, string.Empty);
				lblCountCanceladas.Text = VmHistorico.LsTareasCanceladas.Count.ToString();
				gvTareasCanceladas.DataSource = VmHistorico.LsTareasCanceladas;
				gvTareasCanceladas.DataBind();

				VmHistorico.ObtenerTareasPospuestasTurno(UidDepartamento, UidUsuario, DtFecha, UidSucursal, string.Empty, string.Empty);
				lblCountPospuestas.Text = VmHistorico.LsTareasPospuestas.Count.ToString();
				gvTareasPospuestas.DataSource = VmHistorico.LsTareasPospuestas;
				gvTareasPospuestas.DataBind();

			}
			catch (Exception ex)
			{
				DisplayDialogGeneral("Error: " + ex.Message, "danger");
			}
		}
		protected void gvTurnosEncargados_Sorting(object sender, GridViewSortEventArgs e)
		{
			string Expression = e.SortExpression;
			string Direction = hfSortDirectionGvTurnos.Value;
			if (hfSortDirectionGvTurnos.Value.Equals("DESC"))
				hfSortDirectionGvTurnos.Value = "ASC";
			else
				hfSortDirectionGvTurnos.Value = "DESC";

			switch (Expression)
			{
				case "Folio":
					if (Direction.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Folio).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Folio).ToList();
					break;
				case "Usuario":
					if (Direction.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Usuario).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Usuario).ToList();
					break;
				case "Turno":
					if (Direction.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Turno).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Turno).ToList();
					break;
				case "FInicio":
					if (Direction.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.DtoFechaInicio).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.DtoFechaFin).ToList();
					break;
				case "FFin":
					if (Direction.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.DtoFechaFin).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.DtoFechaFin).ToList();
					break;
				case "Departamento":
					if (Direction.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Departamento).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Departamento).ToList();
					break;
				case "Estado":
					if (Direction.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.EstadoTurno).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.EstadoTurno).ToList();
					break;
				default:
					break;
			}

			gvTurnosEncargados.SelectedIndex = -1;
			gvTurnosEncargados.DataSource = this.LsTurnos;
			gvTurnosEncargados.DataBind();
		}
		protected void gvTurnosEncargados_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvTurnosEncargados.SelectedIndex = -1;
			gvTurnosEncargados.PageIndex = e.NewPageIndex;
			gvTurnosEncargados.DataSource = this.LsTurnos;
			gvTurnosEncargados.DataBind();
		}
		#endregion
		#endregion

		#region Panel Derecho
		#region Methods

		#endregion
		#region Buttons
		protected void btnPrintReport_Click(object sender, EventArgs e)
		{
			if (!this.IsClosedSelected)
			{
				DisplayDialogGeneral("Error: El turno seleccionado esta abierto.", "warning");
				return;
			}


			Session["Periodo"] = this.UidPeriodoSeleccionado;
			Session["Fecha"] = this.DtFechaSeleccionado;
			Session["HoraInicio"] = txtFechaInicio.Text;
			Session["HoraFin"] = txtFechaFin.Text.Length == 0 ? "Sin Cerrar Turno" : txtFechaFin.Text;
			Session["Replica"] = "[Duplicado]";
			Session["FolioTurno"] = this.FolioTurnoSeleccionado;
			ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "open", "window.open('ReporteTareas.aspx', '_blank')", true);
		}

		protected void CloseAlertDialogGeneral_Click(object sender, EventArgs e)
		{
			pnlAlertGeneral.Visible = false;
		}
		#endregion
		#region Tabs
		protected void NavigateResumen_Click(object sender, EventArgs e)
		{
			if (!pnlResumen.Visible)
			{
				liTabResumen.Attributes.Add("class", "active");
				liTabCompletadas.Attributes.Add("class", "");
				liTabNoCompletadas.Attributes.Add("class", "");
				liTabRequeridas.Attributes.Add("class", "");
				liTabCanceladas.Attributes.Add("class", "");
				liTabPospuestas.Attributes.Add("class", "");

				pnlResumen.Visible = true;
				pnlCompletadas.Visible = false;
				pnlNoCompletadas.Visible = false;
				pnlRequeridas.Visible = false;
				pnlCanceladas.Visible = false;
				pnlPospuestas.Visible = false;
			}
		}
		protected void NavigateCompletadas_Click(object sender, EventArgs e)
		{
			if (!pnlCompletadas.Visible)
			{
				liTabResumen.Attributes.Add("class", "");
				liTabCompletadas.Attributes.Add("class", "active");
				liTabNoCompletadas.Attributes.Add("class", "");
				liTabRequeridas.Attributes.Add("class", "");
				liTabCanceladas.Attributes.Add("class", "");
				liTabPospuestas.Attributes.Add("class", "");

				pnlResumen.Visible = false;
				pnlCompletadas.Visible = true;
				pnlNoCompletadas.Visible = false;
				pnlRequeridas.Visible = false;
				pnlCanceladas.Visible = false;
				pnlPospuestas.Visible = false;
			}
		}
		protected void NavigateNoCompletadas_Click(object sender, EventArgs e)
		{
			if (!pnlNoCompletadas.Visible)
			{
				liTabResumen.Attributes.Add("class", "");
				liTabCompletadas.Attributes.Add("class", "");
				liTabNoCompletadas.Attributes.Add("class", "active");
				liTabRequeridas.Attributes.Add("class", "");
				liTabCanceladas.Attributes.Add("class", "");
				liTabPospuestas.Attributes.Add("class", "");

				pnlResumen.Visible = false;
				pnlCompletadas.Visible = false;
				pnlNoCompletadas.Visible = true;
				pnlRequeridas.Visible = false;
				pnlCanceladas.Visible = false;
				pnlPospuestas.Visible = false;
			}
		}
		protected void NavigateRequeridas_Click(object sender, EventArgs e)
		{
			if (!pnlRequeridas.Visible)
			{
				liTabResumen.Attributes.Add("class", "");
				liTabCompletadas.Attributes.Add("class", "");
				liTabNoCompletadas.Attributes.Add("class", "");
				liTabRequeridas.Attributes.Add("class", "active");
				liTabCanceladas.Attributes.Add("class", "");
				liTabPospuestas.Attributes.Add("class", "");

				pnlResumen.Visible = false;
				pnlCompletadas.Visible = false;
				pnlNoCompletadas.Visible = false;
				pnlRequeridas.Visible = true;
				pnlCanceladas.Visible = false;
				pnlPospuestas.Visible = false;
			}
		}
		protected void NavigateCanceladas_Click(object sender, EventArgs e)
		{
			if (!pnlCanceladas.Visible)
			{
				liTabResumen.Attributes.Add("class", "");
				liTabCompletadas.Attributes.Add("class", "");
				liTabNoCompletadas.Attributes.Add("class", "");
				liTabRequeridas.Attributes.Add("class", "");
				liTabCanceladas.Attributes.Add("class", "active");
				liTabPospuestas.Attributes.Add("class", "");

				pnlResumen.Visible = false;
				pnlCompletadas.Visible = false;
				pnlNoCompletadas.Visible = false;
				pnlRequeridas.Visible = false;
				pnlCanceladas.Visible = true;
				pnlPospuestas.Visible = false;
			}
		}
		protected void NavigatePospuestas_Click(object sender, EventArgs e)
		{
			if (!pnlPospuestas.Visible)
			{
				liTabResumen.Attributes.Add("class", "");
				liTabCompletadas.Attributes.Add("class", "");
				liTabNoCompletadas.Attributes.Add("class", "");
				liTabRequeridas.Attributes.Add("class", "");
				liTabCanceladas.Attributes.Add("class", "");
				liTabPospuestas.Attributes.Add("class", "active");

				pnlResumen.Visible = false;
				pnlCompletadas.Visible = false;
				pnlNoCompletadas.Visible = false;
				pnlRequeridas.Visible = false;
				pnlCanceladas.Visible = false;
				pnlPospuestas.Visible = true;
			}
		}
		#endregion
		#region GridView
		protected void gvTareasCompletadas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareasCompletadas, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvTareasCompletadas_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidCumplimiento = new Guid(gvTareasCompletadas.SelectedDataKey.Value.ToString());

				VmHistorico.ObtenerCumplimiento(UidCumplimiento);
				if (VmHistorico.cCumplimiento.BlAtrasada.Value)
				{
					lblTareaAtrasada.Visible = true;
					txtFechaCumplimiento.Text = VmHistorico.cCumplimiento.DtFechaAtrasada == null ? "(Sin fecha)" : VmHistorico.cCumplimiento.DtFechaAtrasada.Value.ToString("dd/MM/yyyy");
					txtHoraCumplimiento.Text = VmHistorico.cCumplimiento.DtFechaAtrasada == null ? "(Sin hora)" : VmHistorico.cCumplimiento.DtFechaAtrasada.Value.ToString("hh:mm tt");
				}
				else
				{
					txtFechaCumplimiento.Text = VmHistorico.cCumplimiento.DtFechaHora == null ? "(Sin fecha)" : VmHistorico.cCumplimiento.DtFechaHora.Value.ToString("dd/MM/yyyy");
					txtHoraCumplimiento.Text = VmHistorico.cCumplimiento.DtFechaHora == null ? "(Sin hora)" : VmHistorico.cCumplimiento.DtFechaHora.Value.ToString("hh:mm tt");
				}

				txtObservaciones.Text = VmHistorico.cCumplimiento.StrObservacion == string.Empty ? "(Sin observaciones)" : VmHistorico.cCumplimiento.StrObservacion;
				VmHistorico.ObtenerTarea(VmHistorico.cCumplimiento.UidTarea);
				pnlCumplimientoSeleccion.Visible = false;
				pnlCumplimientoValor.Visible = false;
				pnlCumplimientoVerdaderoFalso.Visible = false;

				string Tipo = VmHistorico.tTarea.StrTipoMedicion;
				rbNo.Checked = false;
				rbYes.Checked = false;
				txtValorIngresado.Text = string.Empty;
				switch (Tipo)
				{
					case "Verdadero/Falso":
						pnlCumplimientoVerdaderoFalso.Visible = true;
						if (VmHistorico.cCumplimiento.BlValor != null)
						{
							if (VmHistorico.cCumplimiento.BlValor.Value)
								rbYes.Checked = true;
							else
								rbNo.Checked = true;
						}
						break;
					case "Numerico":
						pnlCumplimientoValor.Visible = true;
						if (VmHistorico.cCumplimiento.DcValor1 != null)
							txtValorIngresado.Text = VmHistorico.cCumplimiento.DcValor1.Value.ToString("0.00");
						else
							txtValorIngresado.Text = "0.00";
						break;
					case "Seleccionable":
						pnlCumplimientoSeleccion.Visible = true;
						VmHistorico.ObtenerOpcionesTarea(VmHistorico.cCumplimiento.UidTarea);
						ddlOpcionSeleccionada.DataSource = VmHistorico.LsOpcionesTarea;
						ddlOpcionSeleccionada.DataTextField = "StrOpciones";
						ddlOpcionSeleccionada.DataValueField = "UidOpciones";
						ddlOpcionSeleccionada.DataBind();
						if (VmHistorico.cCumplimiento.UidOpcion != null)
						{
							ddlOpcionSeleccionada.SelectedValue = VmHistorico.cCumplimiento.UidOpcion.Value.ToString();
						}
						break;
					default:
						break;
				}

				VmHistorico.ObtenerRevision(UidCumplimiento);

				pnlVFSupervision.Visible = false;
				pnlValorSupervision.Visible = false;
				pnlOpcionMultipleSupervision.Visible = false;

				if (VmHistorico.rRevision.UidRevision != Guid.Empty)
				{
					pnlDetalleRevisión.Visible = true;
					txtNombreSupervisor.Text = VmHistorico.rRevision.StrNombreUsuario;
					VmHistorico.ObtenerCalificaciones(SesionActual.uidEmpresaActual.Value);
					ddlCalificaciones.DataSource = VmHistorico.LsCalificaciones;
					ddlCalificaciones.DataValueField = "UidCalificacion";
					ddlCalificaciones.DataTextField = "StrCalificacion";
					ddlCalificaciones.DataBind();

					if (VmHistorico.rRevision.UidCalificacion != Guid.Empty)
						ddlCalificaciones.SelectedValue = VmHistorico.rRevision.UidCalificacion.ToString();

					if (VmHistorico.rRevision.DtFechaHora != null)
						txtFechaSupervision.Text = VmHistorico.rRevision.DtFechaHora.Value.ToString("t");
					else
						txtFechaSupervision.Text = "(sin fecha)";

					switch (Tipo)
					{
						case "Verdadero/Falso":
							pnlVFSupervision.Visible = true;
							if (VmHistorico.rRevision.BlValor != null)
							{
								if (VmHistorico.rRevision.BlValor.Value)
									rbSiSupervision.Checked = true;
								else
									rbNoSupervision.Checked = true;
							}
							break;
						case "Numerico":
							pnlValorSupervision.Visible = true;
							if (VmHistorico.rRevision.DcValor1 != null)
								txtValorSupervision.Text = VmHistorico.rRevision.DcValor1.Value.ToString("0.00");
							else
								txtValorSupervision.Text = "0.00";
							break;
						case "Seleccionable":
							pnlOpcionMultipleSupervision.Visible = true;
							VmHistorico.ObtenerOpcionesTarea(VmHistorico.rRevision.UidTarea);
							ddlOpcionSeleccionadaSupervision.DataSource = VmHistorico.LsOpcionesTarea;
							ddlOpcionSeleccionadaSupervision.DataTextField = "StrOpciones";
							ddlOpcionSeleccionadaSupervision.DataValueField = "UidOpciones";
							ddlOpcionSeleccionadaSupervision.DataBind();
							if (VmHistorico.rRevision.UidOpcion != null)
							{
								ddlOpcionSeleccionada.SelectedValue = VmHistorico.rRevision.UidOpcion.Value.ToString();
							}
							break;
						default:
							break;
					}
				}
				else
				{
					pnlDetalleRevisión.Visible = false;
				}
				pnlDetallesTarea.Visible = true;
				pnlErrorBusqueda.Visible = false;
				pnlBotonesFiltro.Visible = false;
				pnlListaTurnos.Visible = false;
				pnlFiltrosBusqueda.Visible = false;
			}
			catch (Exception ex)
			{
				DisplayDialogGeneral("Error: " + ex.Message, "danger");
			}
		}
		protected void gvTareasCompletadas_Sorting(object sender, GridViewSortEventArgs e)
		{
			string Expression = e.SortExpression;
			string Direction = hfSortDirectionTC.Value;
			if (hfSortDirectionTC.Value.Equals("ASC"))
				hfSortDirectionTC.Value = "DESC";
			else
				hfSortDirectionTC.Value = "ASC";

			VmHistorico.ObtenerTareasCumplidasTurno(this.UidDepartamentoSeleccionado, this.UidUsuarioSeleccionad, this.DtFechaSeleccionado, Direction, Expression);

			gvTareasCompletadas.DataSource = VmHistorico.LsTareasCompletadas;
			gvTareasCompletadas.DataBind();
		}

		protected void gvTareasNoCompletadas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareasNoCompletadas, "Select$" + e.Row.RowIndex);
				if (e.Row.Cells[1].Text.Equals("0000"))
				{
					e.Row.Cells[1].Text = string.Empty;
				}

				Label myLabel = e.Row.FindControl("lblEstado") as Label;
				if (myLabel.Text.Equals("Completo"))
				{
					myLabel.CssClass = "glyphicon glyphicon-ok";
				}
				else if (myLabel.Text.Equals("Cancelado"))
				{
					myLabel.CssClass = "glyphicon glyphicon-remove-sign";
				}
				else
				{
					myLabel.CssClass = "glyphicon glyphicon-remove";
				}
				myLabel.ToolTip = myLabel.Text.ToUpper();
				myLabel.Text = string.Empty;
			}
		}
		protected void gvTareasNoCompletadas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{

		}
		protected void gvTareasNoCompletadas_Sorting(object sender, GridViewSortEventArgs e)
		{
			string Expression = e.SortExpression;
			string Direction = hfSortDirectionTNC.Value;
			if (hfSortDirectionTNC.Value.Equals("ASC"))
				hfSortDirectionTNC.Value = "DESC";
			else
				hfSortDirectionTNC.Value = "ASC";
		}

		protected void gvTareasRequeridas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareasRequeridas, "Select$" + e.Row.RowIndex);

				if (e.Row.Cells[1].Text.Equals("0000"))
				{
					e.Row.Cells[1].Text = string.Empty;
				}

				Label myLabel = e.Row.FindControl("lblEstado") as Label;
				if (myLabel.Text.Equals("Completo"))
				{
					myLabel.CssClass = "glyphicon glyphicon-ok";
				}
				else if (myLabel.Text.Equals("Cancelado"))
				{
					myLabel.CssClass = "glyphicon glyphicon-remove-sign";
				}
				else if (myLabel.Text.Equals("Pospuesto"))
				{
					myLabel.CssClass = "glyphicon glyphicon-share-alt";
				}
				else
				{
					myLabel.CssClass = "glyphicon glyphicon-remove";
				}
				myLabel.ToolTip = myLabel.Text.ToUpper();
				myLabel.Text = string.Empty;
			}
		}
		protected void gvTareasRequeridas_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
		protected void gvTareasRequeridas_Sorting(object sender, GridViewSortEventArgs e)
		{
			string Expression = e.SortExpression;
			string Direction = hfSortDirectionTR.Value;
			if (hfSortDirectionTR.Value.Equals("ASC"))
				hfSortDirectionTR.Value = "DESC";
			else
				hfSortDirectionTR.Value = "ASC";

			VmHistorico.ObtenerTareasRequeridasTurno(this.UidDepartamentoSeleccionado, this.UidUsuarioSeleccionad, this.DtFechaSeleccionado, this.UidSucursalSeleccionado, Direction, Expression);
			gvTareasRequeridas.DataSource = VmHistorico.LsTareasRequeridas;
			gvTareasRequeridas.DataBind();
		}

		protected void gvTareasCanceladas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareasCanceladas, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvTareasCanceladas_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
		protected void gvTareasCanceladas_Sorting(object sender, GridViewSortEventArgs e)
		{
			string Expression = e.SortExpression;
			string Direction = hfSortDirectionTCan.Value;
			if (hfSortDirectionTCan.Value.Equals("ASC"))
				hfSortDirectionTCan.Value = "DESC";
			else
				hfSortDirectionTCan.Value = "ASC";
		}

		protected void gvTareasPospuestas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareasPospuestas, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvTareasPospuestas_SelectedIndexChanged(object sender, EventArgs e)
		{

		}
		protected void gvTareasPospuestas_Sorting(object sender, GridViewSortEventArgs e)
		{
			string Expression = e.SortExpression;
			string Direction = hfSortDirectionTP.Value;
			if (hfSortDirectionTP.Value.Equals("ASC"))
				hfSortDirectionTP.Value = "DESC";
			else
				hfSortDirectionTP.Value = "ASC";
		}
		#endregion
		#endregion

		#region Complements
		private void DisplayDialogSearch(string Message, string Type)
		{
			pnlErrorBusqueda.CssClass = "alert alert-" + Type;
			lblErrorBusqueda.Text = Message;
			pnlErrorBusqueda.Visible = true;
		}
		private void DisplayDialogGeneral(string Message, string Type)
		{
			pnlAlertGeneral.CssClass = "alert alert-" + Type;
			lblAlertGeneral.Text = Message;
			pnlAlertGeneral.Visible = true;
		}
		#endregion


	}
}