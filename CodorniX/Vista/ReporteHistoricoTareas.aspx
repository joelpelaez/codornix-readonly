﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="ReporteHistoricoTareas.aspx.cs" Inherits="CodorniX.Vista.ReporteHistoricoTareas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tareas
				</div>
				<div class="panel-body panel-pd">
					<asp:Panel runat="server" ID="pnlBotonesBusqueda">
						<div class="col-md-12 pd-left-right-5 text-right">
							<div class="btn-group">
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnMostrarFiltros" OnClick="btnMostrarFiltros_Click">
									<span class="glyphicon glyphicon-eye-closed"></span>
									<asp:Label Text="Mostrar" runat="server" ID="lblMostrarFiltros" />
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnLimpiarFiltros" OnClick="btnLimpiarFiltros_Click">
									<span class="glyphicon glyphicon-trash"></span>
									Limpiar
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscar" OnClick="btnBuscar_Click">
									<span class="glyphicon glyphicon-search"></span>
									Buscar
								</asp:LinkButton>
							</div>
						</div>
					</asp:Panel>
					<div class="col-xs-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlAlertBusqueda">
							<asp:Label Text="Error: " runat="server" ID="lblAlertBusqueda" />
							<asp:LinkButton OnClick="HideAlertBusqueda" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</asp:Panel>
					</div>
					<asp:Panel runat="server" ID="pnlFiltros">
						<div class="col-md-12 pd-left-right-5">
							<small>Nombre</small>
							<asp:TextBox CssClass="form-control" ID="txtFiltroNombre" runat="server" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Folio de tarea</small>
							<asp:TextBox CssClass="form-control" ID="txtFiltroFolioTarea" runat="server" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Fecha Inicio</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFiltroFechaInicio" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Fecha Fin</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFiltroFechaFin" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Departamento</small>
							<asp:ListBox ID="lbFiltroDepartamento" runat="server" SelectionMode="Multiple" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Area</small>
							<asp:ListBox ID="lbFiltroArea" runat="server" SelectionMode="Multiple" CssClass="form-control" />
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlListaTareas">
						<div class="col-xs-12 pd-0">
							<asp:HiddenField runat="server" ID="hfSortDirectionTareas" Value="ASC" />
							<asp:GridView runat="server" ID="gvListaTareas" AllowPaging="true" PageSize="8" DataKeyNames="UidTarea,UidDepartamento,UidArea,UidPeriodicidad" OnRowDataBound="gvListaTareas_RowDataBound" OnPageIndexChanging="gvListaTareas_PageIndexChanging" OnSorting="gvListaTareas_Sorting" OnSelectedIndexChanged="gvListaTareas_SelectedIndexChanged" AutoGenerateColumns="false" AllowSorting="true" CssClass="table table-bordered table-condensed table-striped input-sm" SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
									<asp:BoundField DataField="IntFolio" HeaderText="Folio" DataFormatString="{0:D4}" SortExpression="Folio" />
									<asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrNombre") %>' runat="server" ID="lblTaskName" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodicidad" SortExpression="Periodicidad" />
									<asp:TemplateField HeaderText="Departamento" SortExpression="Departamento">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrDepartamento") %>' runat="server" ID="lblTaskDepto" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Area" SortExpression="Area">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("StrArea") %>' runat="server" ID="lblTaskArea" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:d}" HeaderText="Fecha Inicio" SortExpression="FechaInicio" />
								</Columns>
								<EmptyDataTemplate>
									<div class="alert alert-info">
										Sin resultados
									</div>
								</EmptyDataTemplate>
								<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
								<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
							</asp:GridView>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlDetalleCumplimiento">
						<div class="text-right">
							<asp:LinkButton runat="server" ID="btnCerrarDetalleCumplimiento" CssClass="btn btn-sm btn-danger" OnClick="btnCerrarDetalleCumplimiento_Click">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</div>
						<div class="col-md-12 pd-left-right-5">
							<strong>Datos de cumplimiento.</strong>
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h6>Fecha
								<asp:Label CssClass="glyphicon glyphicon-time text-danger" Text="(Atrasada)" runat="server" ToolTip="La Tarea fue realizada despues del tiempo programado" ID="lblTareaAtrasada" /></h6>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtFechaCumplimiento" />
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h6>Hora</h6>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtHoraCumplimiento" />
						</div>
						<div class="col-md-12 pd-left-right-5">
							<h6>Valor Ingresado</h6>
							<asp:Panel runat="server" ID="pnlCumplimientoSeleccion">
								<asp:DropDownList runat="server" CssClass="form-control" ID="ddlOpcionSeleccionada" Enabled="false">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlCumplimientoVerdaderoFalso">
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbYes" Enabled="false" />
									Sí
								</label>
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbNo" Enabled="false" />
									No
								</label>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlCumplimientoValor">
								<div class="input-group">
									<span class="input-group-addon">
										<asp:Label Text="$" runat="server" ID="lblUnidadMedida" />
									</span>
									<asp:TextBox runat="server" CssClass="form-control" ID="txtValorIngresado" Enabled="false" />
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-12 pd-left-right-5">
							<h6>Observaciones</h6>
							<asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" CssClass="form-control" Enabled="false" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					<asp:Label Text="Cumplimientos de la tarea" runat="server" ID="lblTituloPanelDerecho" />
				</div>
				<div class="panel-body panel-pd">
					<div class="col-xs-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlAlertGeneral">
							<asp:Label Text="Error: " runat="server" ID="lblAlertGeneral" />
							<asp:LinkButton OnClick="HideAlertGeneral" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</asp:Panel>
					</div>
					<div class="col-xs-12 pd-0">
						<table class="col-xs-12 pd-0">
							<tr>
								<td>
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnPrintReport" OnClick="btnPrintReport_Click">
										<span class="glyphicon glyphicon-print"></span>
										Reporte
									</asp:LinkButton>
								</td>
								<td class="text-right">
									<div class="btn-group">
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnMostrarOcultarFiltrosCumplimiento" OnClick="btnMostrarOcultarFiltrosCumplimiento_Click">
											<span class="glyphicon glyphicon-eye-close"></span>
											<asp:Label Text="Ocultar" runat="server" ID="lblMostrarOcultarFiltrosCumplimiento" />
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnLimpiarFiltrosCumplimiento" OnClick="btnLimpiarFiltrosCumplimiento_Click">
											<span class="glyphicon glyphicon-trash"></span>
											Limpiar
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscarCumplimiento" OnClick="btnBuscarCumplimiento_Click">
											<span class="glyphicon glyphicon-search"></span>
											Buscar
										</asp:LinkButton>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<ul class="nav nav-tabs">
						<li runat="server" id="liDatosGenerales" class="active">
							<asp:LinkButton Text="Datos Generales" runat="server" OnClick="TabDatosGenerales_Click" />
						</li>
						<li runat="server" id="liCumplimientos">
							<asp:LinkButton Text="Cumplimientos" runat="server" OnClick="TabCumplimientosClick_Click" />
						</li>
					</ul>
					<div class="col-xs-12 pd-0">
						<asp:Panel runat="server" ID="pnlFiltrosCumplimientos">
							<div class="col-md-3 pd-left-right-5">
								<h6>Folio Cumplimiento</h6>
								<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFolioCumplimiento" />
							</div>
							<div class="col-sm-6 col-md-3 pd-left-right-5">
								<h6>Desde:</h6>
								<div class="input-group date">
									<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFechaInicioCumplimiento" />
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-sm-6 col-md-3 pd-left-right-5">
								<h6>Hasta:</h6>
								<div class="input-group date">
									<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFechaFinCumplimiento" />
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-3 pd-left-right-5">
								<h6>Estado</h6>
								<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlFiltroEstadoCumplimiento">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</div>
						</asp:Panel>
					</div>
					<asp:Panel runat="server" ID="pnlDatosGeneralesTarea">
						<div class="col-xs-12 pd-left-right-5 mg-bottom-10">
							<div class="col-md-12 pd-0">
								<h5 class="mg-0-5">
									<small class="font-bold">Nombre:</small>
									<asp:Label Text="(ninguno)" runat="server" ID="lblNombreTarea" />
								</h5>
							</div>
							<div class="col-md-12 pd-0">
								<h5 class="mg-0-5">
									<small class="font-bold">Descripcion:</small>
									<asp:Label Text="(sin descripción)" runat="server" ID="lblDescripcion" />
								</h5>
							</div>
							<div class="col-md-6 pd-0">
								<h5 class="mg-0-5">
									<small class="font-bold">Departamento:</small>
									<asp:Label Text="(global)" runat="server" ID="lblDepartamento" />
								</h5>
							</div>
							<div class="col-md-6 pd-0">
								<h5 class="mg-0-5">
									<small class="font-bold">Area:</small>
									<asp:Label Text="(global)" runat="server" ID="lblArea" />
								</h5>
							</div>
							<div class="col-md-6 pd-0">
								<h5 class="mg-0-5">
									<small class="font-bold">Tipo:</small>
									<asp:Label Text="(ninguno)" runat="server" ID="lblTipoTarea" />
								</h5>
							</div>
							<div class="col-md-6 pd-0">
								<h5 class="mg-0-5">
									<small class="font-bold">Periodicidad</small>
									<asp:Label Text="(ninguno)" runat="server" ID="lblPeriodicidad" />
								</h5>
							</div>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlCumplimientosTarea">
						<div class="col-xs-12 pd-left-right-5">
							<div class="col-md-12 pd-0">
								<asp:HiddenField runat="server" ID="hfSortDirectionGvCumplimientos" Value="ASC" />
								<asp:GridView runat="server" OnRowDataBound="gvCumplimientos_RowDataBound" OnSelectedIndexChanged="gvCumplimientos_SelectedIndexChanged" AllowSorting="true" DataKeyNames="UidCumplimiento" ID="gvCumplimientos" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-hover input-sm" SelectedRowStyle-BackColor="#dff0d8">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
										<asp:BoundField HeaderText="F. Turno" DataField="IntFolioTurno" SortExpression="FolioT" />
										<asp:BoundField HeaderText="F. Cumplimiento" DataField="IntFolio" SortExpression="FolioC" />
										<asp:BoundField HeaderText="Usuario" DataField="StrNombreUsuario" />
										<asp:BoundField HeaderText="Fecha" DataField="DtFechaHora" DataFormatString="{0:dd/MM/yyyy}" SortExpression="Fecha" />
										<asp:BoundField HeaderText="Estado" DataField="StrEstadoCumplimiento" SortExpression="Estado" />
										<asp:BoundField HeaderText="Atrasada" DataField="BlAtrasada" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
										<asp:BoundField HeaderText="Tipo" DataField="StrTipoMedicion" />
										<asp:BoundField HeaderText="Valor" DataField="ValorCumplimiento" />
									</Columns>
									<EmptyDataTemplate>
										<div class="info">
											No hay cumplimientos
										</div>
									</EmptyDataTemplate>
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								</asp:GridView>
							</div>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<script>
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
		}
		function pageLoad() {
			enableDatapicker();
		}
	</script>
</asp:Content>
