﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using CodorniX.Modelo;
using CodorniX.Util;
using System.IO;

namespace CodorniX.Vista
{
    public partial class DatosEmpresa : System.Web.UI.Page
    {
        VMEmpresas VM = new VMEmpresas();

        #region Propiedades

        private Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }

        private List<EmpresaDireccion> DireccionRemoved
        {
            get
            {
                if (ViewState["DireccionRemoved"] == null)
                    ViewState["DireccionRemoved"] = new List<EmpresaDireccion>();

                return (List<EmpresaDireccion>)ViewState["DireccionRemoved"];
            }
        }

        private List<EmpresaTelefono> TelefonoRemoved
        {
            get
            {
                if (ViewState["TelefonoRemoved"] == null)
                    ViewState["TelefonoRemoved"] = new List<EmpresaTelefono>();

                return (List<EmpresaTelefono>)ViewState["TelefonoRemoved"];
            }
        }

        private bool EditingMode
        {
            get
            {
                if (ViewState["EditingMode"] == null)
                    return false;

                return (bool)ViewState["EditingMode"];
            }
            set
            {
                ViewState["EditingMode"] = value;
            }
        }

        private bool EditingModeDireccion
        {
            get
            {
                if (ViewState["EditingModeDireccion"] == null)
                    return false;

                return (bool)ViewState["EditingModeDireccion"];
            }
            set
            {
                ViewState["EditingModeDireccion"] = value;
            }
        }

        #endregion

        #region Metodos privados.

        private void ActivarCamposDatos(bool enable)
        {
            if (enable)
            {
                btnEditarEmpresa.AddCssClass("disabled");
                btnEditarEmpresa.Enabled = false;

                btnOkEmpresa.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnOkEmpresa.Enabled = true;

                btnCancelarEmpresa.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnCancelarEmpresa.Enabled = true;

                dgvDirecciones.Enabled = true;

                dgvTelefonos.Enabled = true;

                EditingMode = true;
            }
            else
            {
               

                btnEditarEmpresa.RemoveCssClass("disabled");
                btnEditarEmpresa.Enabled = true;

                txtNombreComercial.AddCssClass("disabled");
                txtNombreComercial.Enabled = false;

                txtRazonSocial.AddCssClass("disabled");
                txtRazonSocial.Enabled = false;

                txtGiro.AddCssClass("disabled");
                txtGiro.Enabled = false;

                txtRFC.AddCssClass("disabled");
                txtRFC.Enabled = false;

                txtFechaRegistro.AddCssClass("disabled");
                txtFechaRegistro.Enabled = false;

                btnOkEmpresa.AddCssClass("disabled").AddCssClass("hidden");
                btnOkEmpresa.Enabled = false;

                btnCancelarEmpresa.AddCssClass("disabled").AddCssClass("hidden");
                btnCancelarEmpresa.Enabled = false;

                dgvDirecciones.Enabled = false;

                dgvTelefonos.Enabled = false;

                EditingMode = false;
            }
        }

        private void ActivarCamposDireccion(bool enable)
        {
            if (enable)
            {
                btnOkDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnOkDireccion.Enabled = true;

                btnCancelarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnCancelarDireccion.Enabled = true;

                ddPais.RemoveCssClass("disabled");
                ddPais.Enabled = true;

                ddEstado.RemoveCssClass("disabled");
                ddEstado.Enabled = true;

                txtMunicipio.RemoveCssClass("disabled");
                txtMunicipio.Enabled = true;

                txtCiudad.RemoveCssClass("disabled");
                txtCiudad.Enabled = true;

                txtColonia.RemoveCssClass("disabled");
                txtColonia.Enabled = true;

                txtCalle.RemoveCssClass("disabled");
                txtCalle.Enabled = true;

                txtConCalle.RemoveCssClass("disabled");
                txtConCalle.Enabled = true;

                txtYCalle.RemoveCssClass("disabled");
                txtYCalle.Enabled = true;

                txtNoExt.RemoveCssClass("disabled");
                txtNoExt.Enabled = true;

                txtNoInt.RemoveCssClass("disabled");
                txtNoInt.Enabled = true;

                txtReferencia.RemoveCssClass("disabled");
                txtReferencia.Enabled = true;

            }
            else
            {
                btnOkDireccion.AddCssClass("disabled");
                btnOkDireccion.Enabled = false;

                btnCancelarDireccion.AddCssClass("disabled");
                btnCancelarDireccion.Enabled = false;

                ddPais.AddCssClass("disabled");
                ddPais.Enabled = false;

                ddEstado.AddCssClass("disabled");
                ddEstado.Enabled = false;

                txtMunicipio.AddCssClass("disabled");
                txtMunicipio.Enabled = false;

                txtCiudad.AddCssClass("disabled");
                txtCiudad.Enabled = false;

                txtColonia.AddCssClass("disabled");
                txtColonia.Enabled = false;

                txtCalle.AddCssClass("disabled");
                txtCalle.Enabled = false;

                txtConCalle.AddCssClass("disabled");
                txtConCalle.Enabled = false;

                txtYCalle.AddCssClass("disabled");
                txtYCalle.Enabled = false;

                txtNoExt.AddCssClass("disabled");
                txtNoExt.Enabled = false;

                txtNoInt.AddCssClass("disabled");
                txtNoInt.Enabled = false;

                txtReferencia.AddCssClass("disabled");
                txtReferencia.Enabled = false;
            }
        }

        protected void ddPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(ddPais.SelectedValue.ToString());
            VM.ObtenerEstados(uid);
            ddEstado.DataSource = VM.Estados;
            ddEstado.DataValueField = "UidEstado";
            ddEstado.DataTextField = "StrNombre";
            ddEstado.DataBind();
        }

        #endregion

        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;

            FUImagen.Attributes["onchange"] = "upload(this)";
            if (!IsPostBack)
            {
                #region Botones
                FUImagen.Enabled = false;
                btnOkEmpresa.Visible = false;
                btnCancelarEmpresa.Visible = false;
                txtRFC.Enabled = false;
                txtNombreComercial.Enabled = false;
                txtRazonSocial.Enabled = false;
                txtGiro.Enabled = false;
                txtFechaRegistro.Enabled = false;
                #endregion

                #region obtencion de datos


                Empresa empresa = new Empresa.Repository().Find(new Guid(SesionActual.uidEmpresaActual.ToString()));
                Guid UidEmpresa = empresa.UidEmpresa;
                VM.ObtenerEmpresa(UidEmpresa);
                uidEmpresa.Text = VM.Empresa.UidEmpresa.ToString();
                txtRFC.Text = VM.Empresa.StrRFC;
                txtNombreComercial.Text = VM.Empresa.StrNombreComercial;
                txtRazonSocial.Text = VM.Empresa.StrRazonSocial;
                txtGiro.Text = VM.Empresa.StrGiro;
                txtFechaRegistro.Text = VM.Empresa.DtFechaRegistro.ToString("dd/MM/yyyy");
                ImgEmpresas.ImageUrl = Page.ResolveUrl(VM.Empresa.RutaImagen);
                VM.ObtenerTelefonos();
                ViewState["Telefonos"] = VM.Telefonos;
                dgvTelefonos.DataSource = VM.Telefonos;
                dgvTelefonos.DataBind();
                btnEditarEmpresa.Enabled = true;
                btnEditarEmpresa.CssClass = "btn btn-sm btn-default";

                VM.ObtenerDirecciones();
                ViewState["Direcciones"] = VM.Direcciones;
                dgvDirecciones.DataSource = VM.Direcciones;
                dgvDirecciones.DataBind();

                #endregion

                #region DatosTelefono
                btnAceptarEliminarTelefono.Visible = false;
                btnCancelarEliminarTelefono.Visible = false;
                VM.ObtenerTipoTelefonos();
                ddTipoTelefono.DataSource = VM.TipoTelefonos;
                ddTipoTelefono.DataValueField = "UidTipoTelefono";
                ddTipoTelefono.DataTextField = "StrTipoTelefono";
                ddTipoTelefono.DataBind();
                #endregion

                #region DatosDireccion

                txtMunicipio.Enabled = false;
                txtCiudad.Enabled = false;
                txtColonia.Enabled = false;
                txtCalle.Enabled = false;
                txtConCalle.Enabled = false;
                txtYCalle.Enabled = false;
                txtNoExt.Enabled = false;
                txtNoInt.Enabled = false;
                txtReferencia.Enabled = false;
                ddPais.Enabled = false;
                ddEstado.Enabled = false;


                btnOkDireccion.Visible = false;
                btnCancelarDireccion.Visible = false;
                btnCancelarEliminarDireccion.Visible = false;
                btnAceptarEliminarDireccion.Visible = false;

                VM.ObtenerPaises();
                ddPais.DataSource = VM.Paises;
                ddPais.DataValueField = "UidPais";
                ddPais.DataTextField = "StrNombre";
                ddPais.DataBind();

                ddPais_SelectedIndexChanged(null, null);
                #endregion
            }
        }

        #region Panel Datos generales.

        protected void btnOkDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = false;
            lblErrorDireccion.Visible = true;
            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            if (string.IsNullOrWhiteSpace(txtMunicipio.Text))
            {
                lblErrorDireccion.Text = "El campo Municipio no debe estar vacío";
                txtMunicipio.Focus();
                frmGrpMunicipio.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtCiudad.Text))
            {
                lblErrorDireccion.Text = "El campo Ciudad no debe estar vacío";
                txtCiudad.Focus();
                frmGrpCiudad.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtColonia.Text))
            {
                lblErrorDireccion.Text = "El campo Colonia no debe estar vacío";
                txtColonia.Focus();
                frmGrpColonia.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Calle no debe estar vacío";
                txtCalle.Focus();
                frmGrpCalle.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtConCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Con Calle no debe estar vacío";
                txtConCalle.Focus();
                frmGrpConCalle.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtYCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Y Calle no debe estar vacío";
                txtYCalle.Focus();
                frmGrpYCalle.AddCssClass("has-error");
                return;
            }

            if (string.IsNullOrWhiteSpace(txtNoExt.Text))
            {
                lblErrorDireccion.Text = "El campo No. Exterior no debe estar vacío";
                txtNoExt.Focus();
                frmGrpNoExt.AddCssClass("has-error");
                return;
            }

            List<EmpresaDireccion> direcciones = (List<EmpresaDireccion>)ViewState["Direcciones"];
            EmpresaDireccion direccion = null;
            int pos = -1;
            if (!string.IsNullOrWhiteSpace(uidDireccion.Text))
            {
                IEnumerable<EmpresaDireccion> dir = from d in direcciones where d.UidDireccion.ToString() == uidDireccion.Text select d;
                direccion = dir.First();
                pos = direcciones.IndexOf(direccion);
                direcciones.Remove(direccion);
            }
            else
            {
                direccion = new EmpresaDireccion();
                direccion.UidDireccion = Guid.NewGuid();
            }
            direccion.UidPais = new Guid(ddPais.SelectedValue);
            direccion.UidEstado = new Guid(ddEstado.SelectedValue);
            direccion.StrMunicipio = txtMunicipio.Text;
            direccion.StrCiudad = txtCiudad.Text;
            direccion.StrColonia = txtColonia.Text;
            direccion.StrCalle = txtCalle.Text;
            direccion.StrConCalle = txtConCalle.Text;
            direccion.StrYCalle = txtYCalle.Text;
            direccion.StrNoExt = txtNoExt.Text;
            direccion.StrNoInt = txtNoInt.Text;
            direccion.StrReferencia = txtReferencia.Text;

            ActivarCamposDireccion(false);
            if (pos < 0)
                direcciones.Add(direccion);
            else
                direcciones.Insert(pos, direccion);

            dgvDirecciones.DataSource = direcciones;
            dgvDirecciones.DataBind();


            btnAgregarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnEditarDireccion.AddCssClass("disabled").AddCssClass("hidden");
            btnEliminarDireccion.AddCssClass("disabled").AddCssClass("hidden");
            btnOkDireccion.Visible = false;
            btnCancelarDireccion.Visible = false; 
        }

        protected void btnEditarEmpresa_Click(object sender, EventArgs e)
        {
            ActivarCamposDatos(true);
            btnOkEmpresa.Visible = true;
            btnCancelarEmpresa.Visible = true;
            btnAgregarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarDireccion.Enabled = true;
            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;
            FUImagen.Enabled = true;
            if (uidDireccion.Text.Length > 0)
            {
                btnEditarDireccion.Enable();
                btnEliminarDireccion.Enable();
            }
            if (uidTelefono.Text.Length > 0)
            {
                btnEditarTelefono.Enable();
                btnEliminarTelefono.Enable();
            }
        }

        protected void btnOkEmpresa_Click(object sender, EventArgs e)
        {

            lblErrorEmpresa.Visible = true;
            Empresa empresa = null;
            if (!string.IsNullOrWhiteSpace(uidEmpresa.Text))
            {
                VM.ObtenerEmpresa(new Guid(uidEmpresa.Text));
                empresa = VM.Empresa;
            }
            else
            {
                empresa = new Empresa();
            }

            // Eliminar marcas de error
            frmGrpRFC.RemoveCssClass("has-error");
            frmGrpNombreComercial.RemoveCssClass("has-error");
            frmGrpRazonSocial.RemoveCssClass("has-error");
            frmGrpGiro.RemoveCssClass("has-error");

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            frmGrpTelefono.RemoveCssClass("has-error");

            // Validación
            if (string.IsNullOrWhiteSpace(txtRFC.Text) || txtRFC.Text.Length < 12)
            {
                lblErrorEmpresa.Text = "El campo RFC debe tener una longitud de 12 o 13 caracteres.";
                txtRFC.Focus();
                frmGrpRFC.AddCssClass("has-error");
                return;
            }

            empresa.StrRFC = txtRFC.Text;

            if (string.IsNullOrWhiteSpace(txtNombreComercial.Text))
            {
                lblErrorEmpresa.Text = "El campo Nombre Comercial no debe estar vacío";
                txtNombreComercial.Focus();
                frmGrpNombreComercial.AddCssClass("has-error");
                return;
            }

            empresa.StrNombreComercial = txtNombreComercial.Text;

            if (string.IsNullOrWhiteSpace(txtRazonSocial.Text))
            {
                lblErrorEmpresa.Text = "El campo Razón Social no debe estar vacío";
                txtRazonSocial.Focus();
                frmGrpRazonSocial.AddCssClass("has-error");
                return;
            }

            empresa.StrRazonSocial = txtRazonSocial.Text;

            if (string.IsNullOrWhiteSpace(txtGiro.Text))
            {
                lblErrorEmpresa.Text = "El campo Giro Comercial no debe estar vacío";
                txtGiro.Focus();
                frmGrpGiro.AddCssClass("has-error");
                return;
            }

            empresa.StrGiro = txtGiro.Text;
            if (ViewState["rutaimg"] != null)
            {
                Empresa empresacargada = new Empresa.Repository().Find(new Guid(SesionActual.uidEmpresaActual.ToString()));
                Guid UidEmpresa = empresacargada.UidEmpresa;
                VM.ObtenerEmpresa(UidEmpresa);
                string Ruta = VM.Empresa.RutaImagen;

                if (File.Exists(Server.MapPath(Ruta)))
                {
                    File.Delete(Server.MapPath(Ruta));

                }
            }

            if (ViewState["rutaimg"] != null)
                empresa.RutaImagen = ViewState["rutaimg"].ToString();

            VM.GuardarEmpresa(empresa);

            List<EmpresaDireccion> direcciones = (List<EmpresaDireccion>)ViewState["Direcciones"];
            VM.GuardarDirecciones(direcciones, empresa.UidEmpresa);

            VM.EliminarDirecciones(DireccionRemoved);

            List<EmpresaTelefono> telefonos = (List<EmpresaTelefono>)ViewState["Telefonos"];
            VM.GuardarTelefonos(telefonos, empresa.UidEmpresa);

            VM.EliminarTelefonos(TelefonoRemoved);

            ActivarCamposDatos(false);
            ActivarCamposDireccion(false);

            

            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");

            btnOkDireccion.AddCssClass("disabled");
            btnCancelarDireccion.AddCssClass("disabled");

            ActivarCamposDireccion(false);

            btnAgregarTelefono.AddCssClass("disabled");
            btnEditarTelefono.AddCssClass("disabled");
            btnEliminarTelefono.AddCssClass("disabled");

            btnOKTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnCancelarTelefono.AddCssClass("disabled").AddCssClass("hidden");

            txtTelefono.Text = string.Empty;
            uidTelefono.Text = string.Empty;
            ddTipoTelefono.SelectedIndex = 0;
            FUImagen.Enabled = false;
            btnOkDireccion.Visible = false;
            btnCancelarDireccion.Visible = false;
        }

        protected void btnCancelarEmpresa_Click(object sender, EventArgs e)
        {
            ActivarCamposDatos(false);
            btnOkDireccion.Visible = false;
            btnCancelarDireccion.Visible = false;
            lblErrorEmpresa.Visible = false;
            lblErrorEmpresa.Text = "";
            lblErrorDireccion.Visible = false;
            lblErrorDireccion.Text = "";
            lblErrorTelefono.Visible = false;
            lblErrorTelefono.Text = "";
            FUImagen.Enabled = false;
            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");

            btnOkDireccion.AddCssClass("disabled");
            btnCancelarDireccion.AddCssClass("disabled");

            ActivarCamposDireccion(false);

            btnAgregarTelefono.AddCssClass("disabled");
            btnEditarTelefono.AddCssClass("disabled");
            btnEliminarTelefono.AddCssClass("disabled");

            btnOKTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnCancelarTelefono.AddCssClass("disabled").AddCssClass("hidden");

            txtTelefono.Text = string.Empty;
            uidTelefono.Text = string.Empty;
            ddTipoTelefono.SelectedIndex = 0;

            // Eliminar marcas de error
            frmGrpRFC.RemoveCssClass("has-error");
            frmGrpNombreComercial.RemoveCssClass("has-error");
            frmGrpRazonSocial.RemoveCssClass("has-error");
            frmGrpGiro.RemoveCssClass("has-error");

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            frmGrpTelefono.RemoveCssClass("has-error");

            btnCancelarEliminarDireccion_Click(sender, e);
            btnCancelarEliminarTelefono_Click(sender, e);

            if (uidEmpresa.Text.Length == 0)
            {
                txtNombreComercial.Text = string.Empty;
                txtRazonSocial.Text = string.Empty;
                txtGiro.Text = string.Empty;
                txtRFC.Text = string.Empty;
                txtFechaRegistro.Text = string.Empty;

                if (ViewState["Direcciones"] != null)
                {
                    dgvDirecciones.DataSource = ViewState["Direcciones"];
                    dgvDirecciones.DataBind();
                }
                if (ViewState["Telefonos"] != null)
                {
                    dgvTelefonos.DataSource = ViewState["Telefonos"];
                    dgvTelefonos.DataBind();
                }
                if (Session["RutaImagen"] != null)
                {
                    string Ruta = Session["RutaImagen"].ToString();

                    //Borra la imagen de la empresa
                    if (File.Exists(Server.MapPath(Ruta)))
                    {
                        File.Delete(Server.MapPath(Ruta));
                    }
                    //Recarga el controlador de la imagen con una imagen default
                    ImgEmpresas.ImageUrl = "Img/Default.jpg";
                    ImgEmpresas.DataBind();
                }
            }
            else
            {
                VM.ObtenerEmpresa(new Guid(uidEmpresa.Text));
                Session["EmpresaActual"] = VM.Empresa;
                Label lblEmpresa = (Label)Page.Master.FindControl("lblEmpresa");
                lblEmpresa.Text = VM.Empresa.StrNombreComercial;
                uidEmpresa.Text = VM.Empresa.UidEmpresa.ToString();
                txtNombreComercial.Text = VM.Empresa.StrNombreComercial;
                txtRazonSocial.Text = VM.Empresa.StrRazonSocial;
                txtGiro.Text = VM.Empresa.StrGiro;
                txtRFC.Text = VM.Empresa.StrRFC;
                txtFechaRegistro.Text = VM.Empresa.DtFechaRegistro.ToString("dd/MM/yyyy");
                ImgEmpresas.ImageUrl = Page.ResolveUrl(VM.Empresa.RutaImagen);

                VM.ObtenerDirecciones();
                ViewState["Direcciones"] = VM.Direcciones;
                DireccionRemoved.Clear();
                dgvDirecciones.DataSource = ViewState["Direcciones"];
                dgvDirecciones.DataBind();

                VM.ObtenerTelefonos();
                ViewState["Telefonos"] = VM.Telefonos;
                TelefonoRemoved.Clear();
                dgvTelefonos.DataSource = ViewState["Telefonos"];
                dgvTelefonos.DataBind();
            }
        }

        protected void tabDatos_Click(object sender, EventArgs e)
        {

            lblErrorTelefono.Visible = false;
            lblErrorEmpresa.Visible = false;
            lblErrorDireccion.Visible = false;
            panelDatosEmpresa.Visible = true;
            activeDatos.Attributes["class"] = "active";
            panelDirecciones.Visible = false;
            activeDirecciones.Attributes["class"] = "";
            panelTelefonos.Visible = false;
            activeTelefonos.Attributes["class"] = "";

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            
        }

        protected void tabDirecciones_Click(object sender, EventArgs e)
        {
            lblErrorTelefono.Visible = false;
            lblErrorEmpresa.Visible = false;
            lblErrorDireccion.Visible = false;
            panelDatosEmpresa.Visible = false;
            activeDatos.Attributes["class"] = "";
            panelDirecciones.Visible = true;
            activeDirecciones.Attributes["class"] = "active";
            panelTelefonos.Visible = false;
            activeTelefonos.Attributes["class"] = "";
        }

        protected void tabTelefonos_Click(object sender, EventArgs e)
        {
            lblErrorTelefono.Visible = false;
            lblErrorEmpresa.Visible = false;
            lblErrorDireccion.Visible = false;
            panelDatosEmpresa.Visible = false;
            activeDatos.Attributes["class"] = "";
            panelDirecciones.Visible = false;
            activeDirecciones.Attributes["class"] = "";
            panelTelefonos.Visible = true;
            activeTelefonos.Attributes["class"] = "active";

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            
        }

        #endregion

        #region Imagen

        protected void imagen(object sender, EventArgs e)
        {
            if (FUImagen.HasFile)
            {
                string extencion = Path.GetExtension(FUImagen.FileName).ToLower();
                string[] arreglo = { ".jpg", ".png", ".jpeg" };
                for (int i = 0; i < arreglo.Length; i++)
                {
                    if (extencion == arreglo[i])
                    {
                        string Nombrearchivo = Path.GetFileName(FUImagen.FileName);
                        int numero = new Random().Next(999999999);
                        string ruta = "~/Vista/Imagenes/Empresas/" + uidEmpresa.Text + '_' + numero + Nombrearchivo;


                        //guardar img
                        FUImagen.SaveAs(Server.MapPath(ruta));

                        string rutaimg = ruta + "?" + (numero - 1);

                        ViewState["rutaimg"] = ruta;

                        ImgEmpresas.ImageUrl = rutaimg;

                    }
                }
            }
        }

        #endregion

        #region Panel Direcciones

        protected void btnAgregarDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = true;
            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            uidDireccion.Text = string.Empty;
            ddPais.SelectedIndex = 0;
            ddEstado.SelectedIndex = 0;
            txtMunicipio.Text = string.Empty;
            txtCiudad.Text = string.Empty;
            txtColonia.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtConCalle.Text = string.Empty;
            txtYCalle.Text = string.Empty;
            txtNoExt.Text = string.Empty;
            txtNoInt.Text = string.Empty;
            txtReferencia.Text = string.Empty;

            ActivarCamposDireccion(true);
            
            panelDireccion.Visible = true;

            int pos = -1;
            if (ViewState["DireccionPreviousRow"] != null)
            {
                pos = (int)ViewState["DireccionPreviousRow"];
                GridViewRow previousRow = dgvDirecciones.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
        }

        protected void btnEditarDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = true;
            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            ActivarCamposDireccion(true);
           
            btnOkDireccion.Visible = true;
            btnCancelarDireccion.Visible = true;
            
            panelDireccion.Visible = true;
        }

        protected void btnEliminarDireccion_Click(object sender, EventArgs e)
        {
            lblAceptarEliminarDireccion.Visible = true;
            lblAceptarEliminarDireccion.Text = "¿Desea eliminar la direccion seleccionada?";
            btnAceptarEliminarDireccion.Visible = true;
            btnCancelarEliminarDireccion.Visible = true;

        }

        protected void btnAceptarEliminarDireccion_Click(object sender, EventArgs e)
        {
            ActivarCamposDireccion(false);
            Guid uid = new Guid(uidDireccion.Text);

            List<EmpresaDireccion> direcciones = (List<EmpresaDireccion>)ViewState["Direcciones"];
            EmpresaDireccion direccion = direcciones.Select(x => x).Where(x => x.UidDireccion == uid).First();
            direcciones.Remove(direccion);
            DireccionRemoved.Add(direccion);
            dgvDirecciones.DataSource = direcciones;
            dgvDirecciones.DataBind();

            btnAgregarDireccion.RemoveCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            btnAceptarEliminarDireccion.Visible = false;
            btnCancelarEliminarDireccion.Visible = false;
            lblAceptarEliminarDireccion.Visible = false;
        }

        protected void btnCancelarEliminarDireccion_Click(object sender, EventArgs e)
        {
            btnCancelarEliminarDireccion.Visible = false;
            btnAceptarEliminarDireccion.Visible = false;
            lblAceptarEliminarDireccion.Visible = false;
        }

        protected void dgvDirecciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvDirecciones, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvDirecciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<EmpresaDireccion> direcciones = (List<EmpresaDireccion>)ViewState["Direcciones"];
            EmpresaDireccion empresaDireccion = direcciones.Select(x => x).Where(x => x.UidDireccion.ToString() == dgvDirecciones.SelectedDataKey.Value.ToString()).First();
            uidDireccion.Text = empresaDireccion.UidDireccion.ToString();
            ddPais.SelectedValue = empresaDireccion.UidPais.ToString();
            ddPais_SelectedIndexChanged(sender, e);
            ddEstado.SelectedValue = empresaDireccion.UidEstado.ToString();
            txtMunicipio.Text = empresaDireccion.StrMunicipio;
            txtCiudad.Text = empresaDireccion.StrCiudad;
            txtColonia.Text = empresaDireccion.StrColonia;
            txtCalle.Text = empresaDireccion.StrCalle;
            txtConCalle.Text = empresaDireccion.StrConCalle;
            txtYCalle.Text = empresaDireccion.StrYCalle;
            txtReferencia.Text = empresaDireccion.StrReferencia;
            txtNoExt.Text = empresaDireccion.StrNoExt;
            txtNoInt.Text = empresaDireccion.StrNoInt;

            if (EditingMode)
            {
                btnEditarDireccion.Enabled = true;
                btnEditarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEliminarDireccion.Enabled = true;
                btnEliminarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            }

            int pos = -1;
            if (ViewState["DireccionPreviousRow"] != null)
            {
                pos = (int)ViewState["DireccionPreviousRow"];
                GridViewRow previousRow = dgvDirecciones.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["DireccionPreviousRow"] = dgvDirecciones.SelectedIndex;
            dgvDirecciones.SelectedRow.AddCssClass("success");

            panelDireccion.Visible = true;

            btnCancelarDireccion.Visible = false;
            btnOkDireccion.Visible = false;
        }

        protected void btnCancelarDireccion_Click(object sender, EventArgs e)
        {

            EditingModeDireccion = false;

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");
            lblErrorDireccion.Text = string.Empty;

            ActivarCamposDireccion(false);


            btnAgregarDireccion.Enabled = true;
            btnAgregarDireccion.RemoveCssClass("disabled");

            if (uidDireccion.Text.Length == 0)
            {
                btnEditarDireccion.Enabled = true;
                btnEditarDireccion.RemoveCssClass("disabled");

                btnEliminarDireccion.Enabled = true;
                btnEliminarDireccion.RemoveCssClass("disabled");
            }
            else
            {
                btnEditarDireccion.Enabled = false;
                btnEditarDireccion.AddCssClass("disabled");
                btnEliminarDireccion.Enabled = false;
                btnEliminarDireccion.AddCssClass("disabled");
            }
            btnCancelarDireccion.Visible = false;
            btnOkDireccion.Visible = false;
        }

        #endregion

        #region Panel Telefonos

        protected void btnAgregarTelefono_Click(object sender, EventArgs e)
        {
            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtTelefono.Enabled = true;
            txtTelefono.RemoveCssClass("disabled");
            ddTipoTelefono.SelectedIndex = 0;
            ddTipoTelefono.RemoveCssClass("disabled");
            ddTipoTelefono.Enabled = true;

            btnOKTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnOKTelefono.Enabled = true;
            btnCancelarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnCancelarTelefono.Enabled = true;

            btnAgregarTelefono.Disable();
            btnEditarTelefono.Disable();
            btnEliminarTelefono.Disable();

            int pos = -1;
            if (ViewState["TelefonoPreviousRow"] != null)
            {
                pos = (int)ViewState["TelefonoPreviousRow"];
                GridViewRow previousRow = dgvTelefonos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
        }

        protected void btnEditarTelefono_Click(object sender, EventArgs e)
        {
            txtTelefono.Enabled = true;
            txtTelefono.RemoveCssClass("disabled");

            ddTipoTelefono.Enabled = true;
            ddTipoTelefono.RemoveCssClass("disabled");

            btnAgregarTelefono.Enabled = false;
            btnAgregarTelefono.AddCssClass("disabled");

            btnEditarTelefono.Enabled = false;
            btnEditarTelefono.AddCssClass("disabled");

            btnEliminarTelefono.Enabled = false;
            btnEliminarTelefono.AddCssClass("disabled");

            btnOKTelefono.Enabled = true;
            btnOKTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");

            btnCancelarTelefono.Enabled = true;
            btnCancelarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
        }

        protected void btnEliminarTelefono_Click(object sender, EventArgs e)
        {
            lblAceptarEliminarTelefono.Visible = true;
            lblAceptarEliminarTelefono.Text = "¿Desea Eliminar el telefono seleccionado?";
            btnAceptarEliminarTelefono.Visible = true;
            btnCancelarEliminarTelefono.Visible = true;
        }

        protected void btnOKTelefono_Click(object sender, EventArgs e)
        {
            lblErrorTelefono.Visible = true;
            frmGrpTelefono.RemoveCssClass("has-error");

            if (string.IsNullOrWhiteSpace(txtTelefono.Text))
            {
                lblErrorTelefono.Text = "El campo Telefono no debe estar vacío";
                txtTelefono.Focus();
                frmGrpTelefono.AddCssClass("has-error");
                return;
            }
            List<EmpresaTelefono> telefonos = (List<EmpresaTelefono>)ViewState["Telefonos"];
            EmpresaTelefono telefono = null;
            int pos = -1;
            if (!string.IsNullOrWhiteSpace(uidTelefono.Text))
            {
                IEnumerable<EmpresaTelefono> dir = from t in telefonos where t.UidTelefono.ToString() == uidTelefono.Text select t;
                telefono = dir.First();
                pos = telefonos.IndexOf(telefono);
                telefonos.Remove(telefono);
            }
            else
            {
                telefono = new EmpresaTelefono();
                telefono.UidTelefono = Guid.NewGuid();
            }
            telefono.StrTelefono = txtTelefono.Text;
            telefono.UidTipoTelefono = new Guid(ddTipoTelefono.SelectedValue);
            telefono.StrTipoTelefono = ddTipoTelefono.SelectedItem.Text;

            if (pos < 0)
                telefonos.Add(telefono);
            else
                telefonos.Insert(pos, telefono);

            dgvTelefonos.DataSource = telefonos;
            dgvTelefonos.DataBind();

            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtTelefono.Enabled = false;
            txtTelefono.AddCssClass("disabled");
            ddTipoTelefono.SelectedIndex = 0;
            ddTipoTelefono.AddCssClass("disabled");
            ddTipoTelefono.Enabled = false;

            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnOKTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnCancelarTelefono.Enabled = false;

            btnEditarTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnEditarTelefono.Enabled = false;
            btnEliminarTelefono.AddCssClass("disabled").AddCssClass("hidden");
            btnEliminarTelefono.Enabled = false;

            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;
        }

        protected void btnCancelarTelefono_Click(object sender, EventArgs e)
        {
            frmGrpTelefono.RemoveCssClass("has-error");

            txtTelefono.Enabled = false;
            txtTelefono.AddCssClass("disabled");
            ddTipoTelefono.AddCssClass("disabled");
            ddTipoTelefono.Enabled = false;

            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnOKTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnCancelarTelefono.Enabled = false;

            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;


            if (uidTelefono.Text.Length == 0)
            {
                btnEditarTelefono.Disable();
                btnEliminarTelefono.Disable();

                ddTipoTelefono.SelectedIndex = 0;
                txtTelefono.Text = string.Empty;
            }
            else
            {
                btnEliminarTelefono.Enable();
                btnEditarTelefono.Enable();

                List<EmpresaTelefono> telefonos = (List<EmpresaTelefono>)ViewState["Telefonos"];
                EmpresaTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono.ToString() == dgvTelefonos.SelectedDataKey.Value.ToString()).First();

                uidTelefono.Text = telefono.UidTelefono.ToString();
                txtTelefono.Text = telefono.StrTelefono;
                ddTipoTelefono.SelectedValue = telefono.UidTipoTelefono.ToString();
            }
        }

        protected void btnAceptarEliminarTelefono_Click(object sender, EventArgs e)
        {
            btnAgregarTelefono.Enabled = true;
            btnAgregarTelefono.RemoveCssClass("disabled");

            btnOKTelefono.Enabled = false;
            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");

            btnCancelarTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");

            Guid uid = new Guid(uidTelefono.Text);

            List<EmpresaTelefono> telefonos = (List<EmpresaTelefono>)ViewState["Telefonos"];
            EmpresaTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono == uid).First();
            telefonos.Remove(telefono);
            TelefonoRemoved.Add(telefono);

            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            ddTipoTelefono.SelectedIndex = 0;

            dgvTelefonos.DataSource = telefonos;
            dgvTelefonos.DataBind();

            btnCancelarEliminarTelefono.Visible = false;
            btnAceptarEliminarTelefono.Visible = false;
            lblAceptarEliminarTelefono.Visible = false;
        }

        protected void btnCancelarEliminarTelefono_Click(object sender, EventArgs e)
        {
            btnCancelarEliminarTelefono.Visible = false;
            btnAceptarEliminarTelefono.Visible = false;
            lblAceptarEliminarTelefono.Visible = false;
        }

        protected void dgvTelefonos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTelefonos, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvTelefonos_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<EmpresaTelefono> telefonos = (List<EmpresaTelefono>)ViewState["Telefonos"];
            EmpresaTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono.ToString() == dgvTelefonos.SelectedDataKey.Value.ToString()).First();

            uidTelefono.Text = telefono.UidTelefono.ToString();
            txtTelefono.Text = telefono.StrTelefono;
            ddTipoTelefono.SelectedValue = telefono.UidTipoTelefono.ToString();

            if (EditingMode)
            {
                btnEditarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEditarTelefono.Enabled = true;
                btnEliminarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEliminarTelefono.Enabled = true;
                btnOKTelefono.AddCssClass("disabled").AddCssClass("hidden");
                btnOKTelefono.Enabled = false;
                btnCancelarTelefono.AddCssClass("disabled").AddCssClass("hidden");
                btnCancelarTelefono.Enabled = false;
            }

            int pos = -1;
            if (ViewState["TelefonoPreviousRow"] != null)
            {
                pos = (int)ViewState["TelefonoPreviousRow"];
                GridViewRow previousRow = dgvTelefonos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["TelefonoPreviousRow"] = dgvTelefonos.SelectedIndex;
            dgvTelefonos.SelectedRow.AddCssClass("success");

        }

        #endregion


    }
}