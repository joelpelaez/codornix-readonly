﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using CodorniX.Util;
using System.Globalization;

namespace CodorniX.Vista
{
	public partial class Bienvenido : System.Web.UI.Page
	{
		VMBienvenido VM = new VMBienvenido();
		VMIniciarTurno VMI = new VMIniciarTurno();
		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}
		protected Guid GuidDepartamento
		{
			get => (Guid)ViewState["departamento"];
			set => ViewState["departamento"] = value;
		}
		protected DateTime DtFechaTurnoSeleccionado
		{
			get => (DateTime)ViewState["fechaturno"];
			set => ViewState["fechaturno"] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("Bienvenido", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				try
				{
					pnlAlertBusqueda.Visible = false;
					pnlErrorGeneral.Visible = false;
					btnIniciarTurno.Disable();
					btnCerrarTurno.Disable();

					DateTime? DtFechaInicio = null;
					if (SesionActual.UidDepartamentos.Count > 0)
					{
						DtFechaInicio = VMI.ObtenerFechaUltimoTurnoPendienteCerrar(DateTime.Today, SesionActual.UidDepartamentos[0]);
					}

					if (DtFechaInicio != null)
						txtFecha.Text = DtFechaInicio.Value.ToString("dd/MM/yyyy");
					else
						txtFecha.Text = SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy");

					txtfecha2.Text = SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy");
					divCerrarturno.Visible = false;
					dvgDepartamentos.Visible = false;
					dvgDepartamentos.DataSource = null;
					dvgDepartamentos.DataBind();
					PanelCompletadas.Visible = false;
					PanelNoCompletadas.Visible = false;
					PanelRequeridas.Visible = false;
					PanelCanceladas.Visible = false;
					PanelPospuestas.Visible = false;

					VM.ObtenerEstadosTurno();
					ddlEstadoTurno.DataTextField = "StrEstadoTurno";
					ddlEstadoTurno.DataValueField = "UidEstadoTurno";
					ddlEstadoTurno.DataSource = VM.LsEstadoTurno;
					ddlEstadoTurno.DataBind();

					ActualizarGrid();
					OcultarMostrarResumenTarea(false);
				}
				catch (Exception)
				{

				}
			}
		}

		protected void DVGResumenTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{

		}
		protected void DVGResumenTareas_Sorting(object sender, GridViewSortEventArgs e)
		{

		}

		protected void dvgDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			Site1 master = (Site1)Page.Master;
			if (pnlErrorGeneral.Visible == true)
				pnlErrorGeneral.Visible = false;

			try
			{
				if (pnlAlertBusqueda.Visible == true) { pnlAlertBusqueda.Visible = false; }
				string estadoTurno = null;
				hfUidInicioTurno.Value = string.Empty;
				lblAceptarCerrarTurno.Text = string.Empty;
				lblnoiniciarturno.Text = string.Empty;

				string strDate = dvgDepartamentos.SelectedDataKey.Values[1].ToString();
				DateTime date = Convert.ToDateTime(strDate);
				int? FolioTurno = null;

				VM.ObtenerDepartamento(new Guid(dvgDepartamentos.SelectedDataKey.Value.ToString()), SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				if (VM.CTareasNoCumplidas == null)
				{
					SesionActual.UidPeriodo = null;
					lblmensaje.Visible = true;
					lblmensaje.Text = " No hay cumplimiento disponible";

					lblHoraInicio.Text = string.Empty;
					lblHoraFin.Text = string.Empty;
					txtUidDepartamento.Text = string.Empty;
					txtUidArea.Text = string.Empty;
					lblDepartamento.Text = string.Empty;
					lblTurno.Text = string.Empty;
					lblCumplidas.Text = string.Empty;
					lblContadorC.Text = "0";
					lblNoCumplidas.Text = string.Empty;
					lblContadorNC.Text = "0";
					lblRequeridasNoCumplidas.Text = string.Empty;
					lblContadorRNC.Text = "0";

					DVGTareasCumplidas.DataSource = null;
					DVGTareasCumplidas.DataBind();

					DvgTareasNoCumplidas.DataSource = null;
					DvgTareasNoCumplidas.DataBind();

					dgvTareasRequeridas.DataSource = null;
					dgvTareasRequeridas.DataBind();

					btnReporte.Disable();
					btnIniciarTurno.Disable();
					btnCerrarTurno.Disable();
					master.NoTurno();
					return;
				}
				txtUidDepartamento.Text = VM.CTareasNoCumplidas.UidDepartamento.ToString();
				txtUidArea.Text = VM.CTareasNoCumplidas.UidArea.ToString();
				Guid UidDepartamento = VM.CTareasNoCumplidas.UidDepartamento;
				lblDepartamento.Text = VM.CTareasNoCumplidas.StrDepartamento;
				lblTurno.Text = VM.CTareasNoCumplidas.StrTurno;

				lblCumplidas.Text = VM.CTareasNoCumplidas.IntTareasCumplidas.ToString();
				lblContadorC.Text = VM.CTareasNoCumplidas.IntTareasCumplidas.ToString();

				lblNoCumplidas.Text = VM.CTareasNoCumplidas.IntTareasNoCumplidas.ToString();
				lblContadorNC.Text = VM.CTareasNoCumplidas.IntTareasNoCumplidas.ToString();

				lblRequeridasNoCumplidas.Text = VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas.ToString();
				lblContadorRNC.Text = VM.CTareasNoCumplidas.IntNumTareasRequeridasdNoCumplidas.ToString();

				lblCanceladas.Text = VM.CTareasNoCumplidas.IntTareasCanceladas.ToString();
				lblPospuestas.Text = VM.CTareasNoCumplidas.IntTareasPospuestas.ToString();

				FolioTurno = VM.CTareasNoCumplidas.IntFolio;

				Guid uidPeriodo = VM.CTareasNoCumplidas.UidPeriodo;
				bool abiertoEncargado = false;
				bool abiertoSupervisor = false;
				bool isClosed = false;

				VM.ObtenerHora(date, SesionActual.uidUsuario, uidPeriodo);

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				DateTime local = horaLocal.DateTime.Date;

				VMI.ObtenerPeriodoTurno(uidPeriodo);

				int AuxDateCompare = DateTime.Compare(date, local);
				if (AuxDateCompare < 0)
				{
					VMI.ObtenerTurnoUsuario(uidPeriodo, date);
					SesionActual.dtFechaInicioTurnoSeleccionado = date;
				}
				else
				{
					VMI.ObtenerTurnoUsuario(uidPeriodo, local);
					SesionActual.dtFechaInicioTurnoSeleccionado = local;
				}

				if (VMI.CIniciarTurno != null)
				{
					hfUidInicioTurno.Value = VMI.CIniciarTurno.UidInicioTurno.ToString();
					hfUidPeriodo.Value = uidPeriodo.ToString();
					VMI.ObtenerEstadoTurno(VMI.CIniciarTurno.UidEstadoTurno);
					estadoTurno = VMI.EstadoTurno.StrEstadoTurno;
					abiertoEncargado = VMI.CIniciarTurno._blAbiertoEncargado;
					abiertoSupervisor = VMI.CIniciarTurno._blAbiertoSupervisor;

					if (FolioTurno != null)
						FolioTurno = VMI.CIniciarTurno.IntFolio;

					lblHoraInicio.Text = VMI.CIniciarTurno.DtFechaHoraInicio.ToString("t");
					lblHoraFin.Text = VMI.CIniciarTurno.DtFechaHoraFin == null ? "" : VMI.CIniciarTurno.DtFechaHoraFin?.ToString("t");
					if (lblHoraFin.Text.Length == 0)
					{
						lblEstadoDelTurno.Text = "Abierto";
					}
					else
					{
						lblEstadoDelTurno.Text = "Cerrado";
						isClosed = true;
					}
				}
				else
				{
					lblHoraInicio.Text = string.Empty;
					lblHoraFin.Text = string.Empty;
					if (lblHoraInicio.Text.Length == 0 && lblHoraFin.Text.Length == 0)
					{
						lblEstadoDelTurno.Text = "No Creado";
					}
				}

				VMI.ObtenerInicioTurno(SesionActual.uidUsuario);

				if (VMI.CIniciarTurno != null)
				{
					if (hfUidInicioTurno.Value != string.Empty) { hfUidInicioTurno.Value = VMI.CIniciarTurno.UidInicioTurno.ToString(); }
					if (hfUidPeriodo.Value != string.Empty)
					{
						hfUidPeriodo.Value = uidPeriodo.ToString();
					}

					if (abiertoEncargado)
					{
						if (lblHoraFin.Text.Length == 0 && lblHoraInicio.Text.Length != 0)
						{
							master.Noiniciarturno();
							btnIniciarTurno.Disable();
							master.SiCerrarTurno();
							btnCerrarTurno.Enable();
							master.siturno();
							master.SiCumplimiento();
							if (!abiertoEncargado) { master.siiniciarturno(); }
						}
						else
						{
							master.NoCumplimiento();
							master.NoTurno();
							isClosed = true;
						}
					}
				}
				else
				{
					DateTimeOffset dtOffset = SesionActual.GetDateTimeOffset();
					DateTime dtToday = dtOffset.DateTime.Date;

					if (VM.CTareasNoCumplidas.DtFecha.ToString("dd/MM/yyyy") == dtToday.ToString("dd/MM/yyyy"))
					{
						master.siturno();
						master.noCerrarTurno();
						master.siiniciarturno();
						master.NoCumplimiento();
						btnIniciarTurno.Enable();
						btnCerrarTurno.Disable();
					}
					else
					{

						master.siturno();
						//master.siiniciarturno();
						//master.noCerrarTurno();
						//master.NoCumplimiento();
						master.Noiniciarturno();
						master.SiCerrarTurno();
						master.SiCumplimiento();
						btnIniciarTurno.Disable();
						btnCerrarTurno.Enable();
					}
				}

				if (isClosed)
				{
					btnIniciarTurno.Disable();
					btnCerrarTurno.Disable();
					SesionActual.UidPeriodo = null;
					master.NoTurno();
				}
				else
				{
					Guid? uidInicioActual = null;
					if (!string.IsNullOrEmpty(hfUidInicioTurno.Value))
					{
						uidInicioActual = new Guid(hfUidInicioTurno.Value);
					}

					bool Result = VMI.ValidarCirreInicioTurnoAnteriorDepartamento(uidInicioActual, date, new Guid(txtUidDepartamento.Text));

					if (!Result)
					{
						master.NoTurno();
						btnIniciarTurno.Disable();
						btnCerrarTurno.Disable();

						pnlAlertBusqueda.Visible = true;
						lblmensaje.Text = "Turno pendiente por cerrar";
						SesionActual.UidPeriodo = null;
						SesionActual.dtFechaInicioTurnoSeleccionado = null;
					}

					if (estadoTurno != null)
					{
						if (estadoTurno == "Bloqueado")
						{
							SesionActual.UidPeriodo = null;
							master.NoTurno();
							btnIniciarTurno.Disable();
							btnCerrarTurno.Disable();
						}
						else if (estadoTurno == "Abierto por Supervisor")
						{
							SesionActual.UidPeriodo = null;
							master.NoTurno();
							btnIniciarTurno.Disable();
							btnCerrarTurno.Disable();
						}
						else if (estadoTurno == "Abierto (Controlado)")
						{
							master.noCerrarTurno();
							if (abiertoEncargado)
							{
								btnIniciarTurno.Disable();
							}
							else
							{
								btnIniciarTurno.Enable();
							}

							btnCerrarTurno.Disable();

						}
						else if (estadoTurno.Equals("Abierto") && !abiertoEncargado)
						{
							SesionActual.UidPeriodo = null;
							btnIniciarTurno.Enable();
						}
						else if (estadoTurno.Equals("Abierto") && abiertoEncargado)
						{
							btnIniciarTurno.Disable();
							btnCerrarTurno.Enable();
						}
					}
				}

				if (abiertoEncargado || (!abiertoEncargado && !abiertoSupervisor))
				{
					if (!isClosed)
					{
						SesionActual.UidPeriodo = uidPeriodo;
						SesionActual.FolioTurno = FolioTurno;
					}

				}

				///<summary>
				///Obtener Listado de tareas
				///</summary>
				this.GuidDepartamento = UidDepartamento;
				this.DtFechaTurnoSeleccionado = date;

				VM.ObtenerTareasCumplidas(UidDepartamento, SesionActual.uidUsuario, date);
				DVGTareasCumplidas.DataSource = VM.ltsTareasCumplidas;
				DVGTareasCumplidas.DataBind();

				VM.ObtenerTareasNoCumplidas(UidDepartamento, SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				DvgTareasNoCumplidas.DataSource = VM.ltsTareasNoCumplidas;
				DvgTareasNoCumplidas.DataBind();
				if (VM.ltsTareasNoCumplidas.Count > 0)
					lblContadorNC.Text = VM.ltsTareasNoCumplidas.Count.ToString();
				else
					lblContadorNC.Text = "0";

				VM.ObtenerTareasRequeridas(UidDepartamento, SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				//this.vMBienvenido.TareasRequeridas = VM.TareasRequeridas;
				dgvTareasRequeridas.DataSource = VM.TareasRequeridas;
				dgvTareasRequeridas.DataBind();

				VM.ObtenerTareasCanceladas(UidDepartamento, SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				//this.vMBienvenido.TareasCanceladas = VM.TareasCanceladas;
				gvTareasCanceladas.DataSource = VM.TareasCanceladas;
				gvTareasCanceladas.DataBind();
				if (VM.TareasCanceladas.Count > 0)
					lblContadorCanceladas.Text = VM.TareasCanceladas.Count.ToString();
				else
					lblContadorCanceladas.Text = "0";

				VM.ObtenerTareasPospuestas(UidDepartamento, SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);
				//this.vMBienvenido.TareasPospuestas = VM.TareasPospuestas;
				gvTareasPospuestas.DataSource = VM.TareasPospuestas;
				gvTareasPospuestas.DataBind();
				if (VM.TareasPospuestas.Count > 0)
					lblContadorPospuestas.Text = VM.TareasPospuestas.Count.ToString();
				else
					lblContadorPospuestas.Text = "0";

				int pos = -1;
				if (ViewState["DepartamentoPreviousRow"] != null)
				{
					pos = (int)ViewState["DepartamentoPreviousRow"];
					GridViewRow previousRow = dvgDepartamentos.Rows[pos];
					previousRow.RemoveCssClass("success");
				}
				ViewState["DepartamentoPreviousRow"] = dvgDepartamentos.SelectedIndex;
				dvgDepartamentos.SelectedRow.AddCssClass("success");
			}
			catch (Exception ex)
			{
				pnlAlertBusqueda.Visible = true;
				lblmensaje.Text = "Error al obtener datos: " + ex.Message;
			}

		}
		protected void dvgDepartamentos_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dvgDepartamentos, "Select$" + e.Row.RowIndex);

				if (e.Row.Cells[1].Text.Equals("&nbsp;"))
				{
					e.Row.Cells[1].Text = "(n/a)";
				}

				if (e.Row.Cells[5].Text.Equals("&nbsp;"))
				{
					e.Row.Cells[5].Text = "(No Iniciado)";
				}

				if (e.Row.Cells[6].Text.Equals("&nbsp;"))
				{
					e.Row.Cells[6].Text = "(No Cerrado)";
				}

				if (e.Row.Cells[7].Text.Equals("&nbsp;"))
				{
					e.Row.Cells[7].Text = "(No Creado)";
				}
			}
		}

		public void btnActualizar_Click(object sender, EventArgs e)
		{
			ActualizarGrid();
		}
		public void ActualizarGrid()
		{
			if (pnlErrorGeneral.Visible == true)
				pnlErrorGeneral.Visible = false;
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				string filtrofecha = txtFecha.Text;
				string filtrofecha2 = txtfecha2.Text;
				int? Folio = null;
				Guid? UidEstadoTurno = null;

				if (!string.IsNullOrEmpty(txtFolioTurno.Text))
				{
					int FolioAux = 0;
					if (int.TryParse(txtFolioTurno.Text, out FolioAux))
					{
						Folio = int.Parse(txtFolioTurno.Text);
					}
					else
					{
						pnlAlertBusqueda.Visible = true;
						lblmensaje.Text = "Ingrese un Folio Numerico";
						return;
					}
				}
				if (ddlEstadoTurno.Items.Count > 0)
				{
					UidEstadoTurno = new Guid(ddlEstadoTurno.SelectedValue.ToString());
					if (UidEstadoTurno == Guid.Empty)
					{
						UidEstadoTurno = null;
					}
				}

				if (txtfecha2.Text.Length == 0)
				{
					filtrofecha2 = filtrofecha;
				}
				if (txtFecha.Text.Length == 0 && txtfecha2.Text.Length != 0)
				{
					filtrofecha = filtrofecha2;
				}
				if (txtFecha.Text.Length == 0 && txtfecha2.Text.Length == 0)
				{
					dvgDepartamentos.DataSource = null;
					dvgDepartamentos.DataBind();
				}
				else
				{
					List<TareasNoCumplidas> cumplimiento = new List<TareasNoCumplidas>();
					DateTime fecha1 = Convert.ToDateTime(DateTime.ParseExact(filtrofecha, "dd/MM/yyyy", CultureInfo.InvariantCulture));
					DateTime fecha2 = Convert.ToDateTime(DateTime.ParseExact(filtrofecha2, "dd/MM/yyyy", CultureInfo.InvariantCulture));

					DateTime fechatemporal;
					fechatemporal = fecha1;

					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
					DateTime local = horaLocal.DateTime.Date;
					int Comparing = 0;
					for (; fechatemporal <= fecha2; fechatemporal = fechatemporal.AddDays(1))
					{
						VM.ObtenerCumplimiento(SesionActual.uidSucursalActual.Value, SesionActual.uidUsuario, fechatemporal, Folio, UidEstadoTurno);
						Comparing = DateTime.Compare(fechatemporal, local);

						if (VM.ltsDepartamento.Count > 0)
						{
							if (Comparing < 0 && VM.ltsDepartamento[0].DtFechaHoraInicio == null)
							{
							}
							else if (Comparing == 0 || Comparing < 0)
							{
								cumplimiento.AddRange(VM.ltsDepartamento);
							}
						}
					}

					dvgDepartamentos.Visible = true;
					dvgDepartamentos.DataSource = cumplimiento;
					dvgDepartamentos.DataBind();

					Site1 master = (Site1)Page.Master;
					master.Noiniciarturno();
					master.NoTurno();
					btnCancelarCerrarTurno_Click(null, null);
				}

				ViewState["DepartamentoPreviousRow"] = null;
			}
			catch (Exception ex)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error al realizar busqueda de datos";
			}
		}

		protected void btnReporte_Click(object sender, EventArgs e)
		{
			if (pnlErrorGeneral.Visible == true)
				pnlErrorGeneral.Visible = false;

			if (hfUidPeriodo.Value != string.Empty)
			{
				if (SesionActual.dtFechaInicioTurnoSeleccionado != null)
				{
					if (pnlErrorGeneral.Visible == true) { pnlErrorGeneral.Visible = false; }
					ShowReport();
				}
				else
				{
					pnlErrorGeneral.Visible = true;
					lblErrorGeneral.Text = "Turno no iniciado";
				}
			}
			else
			{
				pnlErrorGeneral.Visible = true;
				lblErrorGeneral.Text = "Turno no iniciado";
			}
		}

		public void ShowReport()
		{
			Session["Periodo"] = new Guid(hfUidPeriodo.Value);
			Session["Fecha"] = SesionActual.dtFechaInicioTurnoSeleccionado.Value;
			Session["HoraInicio"] = lblHoraInicio.Text;
			Session["HoraFin"] = lblHoraFin.Text.Length == 0 ? "Sin Cerrar Turno" : lblHoraFin.Text;
			Session["Replica"] = "[Duplicado]";
			ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "open", "window.open('ReporteTareas.aspx', '_blank')", true);
		}

		protected void btnAceptarCerrarTurno_Click(object sender, EventArgs e)
		{
			Site1 master = (Site1)Page.Master;
			master.AceptarCerrarTurno();
		}
		public void btnCancelarCerrarTurno_Click(object sender, EventArgs e)
		{
			divCerrarturno.Visible = false;
			lblAceptarCerrarTurno.Text = string.Empty;
		}

		public void cerrarturno()
		{
			divCerrarturno.Visible = true;
			lblAceptarCerrarTurno.Text = "¿Quieres cerrar Turno?";
		}
		public void NoIniciar()
		{
			divCerrarturno.Visible = true;
			lblAceptarCerrarTurno.Text = "No Puedes iniciar el mismo turno dos veces";
			lblAceptarCerrarTurno.Visible = true;
			btnAceptarCerrarTurno.Visible = false;
			btnCancelarCerrarTurno.Visible = false;
		}

		public void llenarhora()
		{
			VM.ObtenerHora(Convert.ToDateTime(SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy")), SesionActual.uidUsuario, SesionActual.UidPeriodo.Value);
			if (VM.CIniciarTurno != null)
			{
				lblHoraInicio.Text = VM.CIniciarTurno.DtFechaHoraInicio.ToString("hh\\:mm tt");
				lblHoraFin.Text = VM.CIniciarTurno.DtFechaHoraFin.HasValue ? VM.CIniciarTurno.DtFechaHoraFin.Value.ToString("hh\\:mm tt") : "";
				if (lblHoraFin.Text.Length == 0)
				{
					lblEstadoDelTurno.Text = "Abierto";
				}
				else
				{
					lblEstadoDelTurno.Text = "Cerrado";
				}
			}
			else
			{
				lblHoraInicio.Text = "12:00 AM";
				lblHoraFin.Text = "12:00 AM";
				if (lblHoraInicio.Text == "12:00 AM" && lblHoraFin.Text == "12:00 AM")
				{
					lblEstadoDelTurno.Text = "No Creado";
				}
			}
		}

		public void validarfecha()
		{
			DateTime date = Convert.ToDateTime(dvgDepartamentos.SelectedDataKey.Values[1].ToString());
			VM.ObtenerDepartamento(new Guid(dvgDepartamentos.SelectedDataKey.Value.ToString()), SesionActual.uidUsuario, date, SesionActual.uidSucursalActual.Value);

			if (VM.CTareasNoCumplidas.DtFecha.ToString("dd/MM/yyyy") != SesionActual.GetDateTimeOffset().ToString("dd/MM/yyyy"))
			{
				Site1 master = (Site1)Page.Master;
				master.Noiniciarturno();
				lblError.Text = "No Puedes iniciar Turno con Fecha Diferente a hoy";
				panelAlert.Visible = true;
			}
			else
			{
				Site1 master = (Site1)Page.Master;
				master.siiniciarturno();
			}
		}
		public void requeridonocumplido()
		{
			lblError.Text = "Existen tareas sin estado, revisar antes de cerrar turno.";
			panelAlert.Visible = true;
		}
		public void requeridoSicumplido()
		{
			lblError.Text = "";
		}

		protected void btnCloseAlert_Click(object sender, EventArgs e)
		{
			panelAlert.Visible = false;
			lblError.Text = string.Empty;
		}

		protected void btnIniciarTurno_Click(object sender, EventArgs e)
		{
			try
			{
				if (lblmensaje.Visible == true) { lblmensaje.Visible = false; }

				if (SesionActual.UidPeriodo == null)
				{
					Guid uidInicioTurno = new Guid(hfUidInicioTurno.Value.ToString());
					VM.ActualizarEstatusInicioTurnoEncargado(uidInicioTurno);
					ActualizarGrid();
					Site1 master = (Site1)Page.Master;
					master.UpdateNavbar();
				}
				else
				{
					Site1 master = (Site1)Page.Master;
					master.IniciarTurno();
				}
			}
			catch (Exception ex)
			{
				lblmensaje.Visible = true;
				lblmensaje.Text = "Error: " + ex.Message;
			}
			finally { btnIniciarTurno.Disable(); }
		}
		protected void btnCerrarTurno_Click(object sender, EventArgs e)
		{
			btnCerrarTurno.Disable();
			Site1 master = (Site1)Page.Master;
			master.CerrarTurno();
		}

		protected void DVGTareasCumplidas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(DVGTareasCumplidas, "Select$" + e.Row.RowIndex);
			}
		}
		protected void DvgTareasNoCumplidas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(DvgTareasNoCumplidas, "Select$" + e.Row.RowIndex);

				if (e.Row.Cells[1].Text.Equals("0000"))
				{
					e.Row.Cells[1].Text = string.Empty;
				}

				Label myLabel = e.Row.FindControl("lblEstado") as Label;
				if (myLabel.Text.Equals("Completo"))
				{
					myLabel.CssClass = "glyphicon glyphicon-ok";
				}
				else if (myLabel.Text.Equals("Cancelado"))
				{
					myLabel.CssClass = "glyphicon glyphicon-remove-sign";
				}
				else
				{
					myLabel.CssClass = "glyphicon glyphicon-remove";
				}
				myLabel.ToolTip = myLabel.Text.ToUpper();
				myLabel.Text = string.Empty;
			}
		}
		protected void dgvTareasRequeridas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				if (e.Row.Cells[1].Text.Equals("0000"))
				{
					e.Row.Cells[1].Text = string.Empty;
				}

				Label myLabel = e.Row.FindControl("lblEstado") as Label;
				if (myLabel.Text.Equals("Completo"))
				{
					myLabel.CssClass = "glyphicon glyphicon-ok";
				}
				else if (myLabel.Text.Equals("Cancelado"))
				{
					myLabel.CssClass = "glyphicon glyphicon-remove-sign";
				}
				else if (myLabel.Text.Equals("Pospuesto"))
				{
					myLabel.CssClass = "glyphicon glyphicon-share-alt";
				}
				else
				{
					myLabel.CssClass = "glyphicon glyphicon-remove";
				}
				myLabel.ToolTip = myLabel.Text.ToUpper();
				myLabel.Text = string.Empty;
			}
		}
		protected void gvTareasCanceladas_RowDataBound(object sender, GridViewRowEventArgs e)
		{

		}
		protected void gvTareasPospuestas_RowDataBound(object sender, GridViewRowEventArgs e)
		{

		}

		protected void tabResumen_Click(object sender, EventArgs e)
		{
			PanelResumen.Visible = true;
			PanelCompletadas.Visible = false;
			PanelNoCompletadas.Visible = false;
			PanelRequeridas.Visible = false;
			PanelCanceladas.Visible = false;
			PanelPospuestas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeResumen.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");
			activeCanceladas.RemoveCssClass("active");
			activePospuestas.RemoveCssClass("active");

			PanelBusqueda.Visible = true;
			CleanFieldsDetailTask();
			OcultarMostrarResumenTarea(false);
		}
		protected void tabCompletadas_Click(object sender, EventArgs e)
		{
			PanelCompletadas.Visible = true;
			PanelResumen.Visible = false;
			PanelNoCompletadas.Visible = false;
			PanelRequeridas.Visible = false;
			PanelCanceladas.Visible = false;
			PanelPospuestas.Visible = false;

			activeResumen.Attributes["class"] = "";
			activeNoCompletadas.RemoveCssClass("active");
			activeCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");
			activeCanceladas.RemoveCssClass("active");
			activePospuestas.RemoveCssClass("active");

			PanelBusqueda.Visible = true;
			CleanFieldsDetailTask();
			OcultarMostrarResumenTarea(false);
		}
		protected void tabNoCompletadas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = true;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			PanelRequeridas.Visible = false;
			PanelCanceladas.Visible = false;
			PanelPospuestas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "active";
			activeRequeridas.RemoveCssClass("active");
			activeCanceladas.RemoveCssClass("active");
			activePospuestas.RemoveCssClass("active");

			PanelBusqueda.Visible = true;
			CleanFieldsDetailTask();
			OcultarMostrarResumenTarea(false);
		}
		protected void tabRequeridas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = false;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			PanelRequeridas.Visible = true;
			PanelCanceladas.Visible = false;
			PanelPospuestas.Visible = false;

			activeCompletadas.Attributes["class"] = "";
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.Attributes["class"] = "";
			activeRequeridas.AddCssClass("active");
			activeCanceladas.RemoveCssClass("active");
			activePospuestas.RemoveCssClass("active");

			PanelBusqueda.Visible = true;
			CleanFieldsDetailTask();
			OcultarMostrarResumenTarea(false);
		}
		protected void tabCanceladas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = false;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			PanelRequeridas.Visible = false;
			PanelCanceladas.Visible = true;
			PanelPospuestas.Visible = false;

			activeCompletadas.RemoveCssClass("active");
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.RemoveCssClass("active");
			activeRequeridas.RemoveCssClass("active");
			activeCanceladas.AddCssClass("active");
			activePospuestas.RemoveCssClass("active");

			CleanFieldsDetailTask();
			OcultarMostrarResumenTarea(false);
		}
		protected void tabPospuestas_Click(object sender, EventArgs e)
		{
			PanelNoCompletadas.Visible = false;
			PanelCompletadas.Visible = false;
			PanelResumen.Visible = false;
			PanelRequeridas.Visible = false;
			PanelCanceladas.Visible = false;
			PanelPospuestas.Visible = true;

			activeCompletadas.RemoveCssClass("active");
			activeResumen.RemoveCssClass("active");
			activeNoCompletadas.RemoveCssClass("active");
			activeRequeridas.RemoveCssClass("active");
			activeCanceladas.RemoveCssClass("active");
			activePospuestas.AddCssClass("active");

			CleanFieldsDetailTask();
			OcultarMostrarResumenTarea(false);
		}

		protected void btnCerrarResumentTarea_Click(object sender, EventArgs e)
		{
			OcultarMostrarResumenTarea(false);
		}

		protected void DVGTareasCumplidas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{

		}
		protected void DVGTareasCumplidas_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidCumplimiento = new Guid(DVGTareasCumplidas.SelectedDataKey.Value.ToString());
				OcultarMostrarResumenTarea(true);
				CleanFieldsDetailTask();
				VM.ObtenerCumplimientoTarea(UidCumplimiento);
				if (VM.CUMPLIMIENTO != null)
				{
					Guid UidTarea = VM.CUMPLIMIENTO.UidTarea;
					VM.ObtenerTarea(UidTarea);
					txtDetailNombreTarea.Text = VM.TAREA.StrNombre;
					txtDetailTipoTarea.Text = VM.ObtenerTipoTarea(UidTarea);
					txtDetailPeriodicidad.Text = VM.ObtenerTipoFrecuenciaTarea(UidTarea);

					lblTittleLeftPanel.Text = "Folio Tarea:" + VM.TAREA.IntFolio.ToString("0000")+ ", Folio Cumplimiento:" + VM.CUMPLIMIENTO.IntFolio.ToString("0000");

					if (VM.CUMPLIMIENTO.UidArea != null)
					{
						VM.ObtenerArea(VM.CUMPLIMIENTO.UidArea.Value);
						txtDetailArea.Text = VM.AREA.StrNombre;
					}
					else
						txtDetailArea.Text = "(global)";

					if (VM.CUMPLIMIENTO.UidDepartamento != null)
					{
						VM.ObtenerDepartamento(VM.CUMPLIMIENTO.UidDepartamento.Value);
						txtDetailDepartamento.Text = VM.DEPARTAMENTO.StrNombre;
					}
					else
						txtDetailDepartamento.Text = "(global)";

					if (VM.CUMPLIMIENTO.StrObservacion != null)
						txtDetailObservaciones.Text = VM.CUMPLIMIENTO.StrObservacion;

					switch (VM.TAREA.StrTipoMedicion)
					{
						case "Verdadero/Falso":
							pnlReviewTaskSelection.Visible = false;
							pnlReviewTaskTrueFalse.Visible = true;
							pnlReviewTaskValue.Visible = false;
							if (VM.CUMPLIMIENTO.BlValor != null)
							{
								if (VM.CUMPLIMIENTO.BlValor.Value)
									rbDetailYes.Checked = true;
								else
									rbDetailNo.Checked = true;
							}
							break;
						case "Numerico":
							pnlReviewTaskSelection.Visible = false;
							pnlReviewTaskTrueFalse.Visible = false;
							pnlReviewTaskValue.Visible = true;
							lblDetailUnidadMedida.Text = VM.ObtenerUnidadMedida(VM.TAREA.UidUnidadMedida.Value);
							if (VM.CUMPLIMIENTO.DcValor1 != null)
							{
								txtDetailValue.Text = VM.CUMPLIMIENTO.DcValor1.Value.ToString("00.00");
							}
							break;
						case "Seleccionable":
							pnlReviewTaskSelection.Visible = true;
							pnlReviewTaskTrueFalse.Visible = false;
							pnlReviewTaskValue.Visible = false;
							VM.ObtenerOpcionesTarea(UidTarea);
							ddlDetailOptions.DataSource = VM.LsOpcionesTarea;
							ddlDetailOptions.DataTextField = "StrOpciones";
							ddlDetailOptions.DataValueField = "UidOpciones";
							ddlDetailOptions.DataBind();

							if (VM.CUMPLIMIENTO.UidOpcion != null)
								ddlDetailOptions.SelectedValue = VM.CUMPLIMIENTO.UidOpcion.Value.ToString();
							break;
						default:
							break;
					}
				}
			}
			catch (Exception)
			{
				throw;
			}
		}
		protected void DVGTareasCumplidas_Sorting(object sender, GridViewSortEventArgs e)
		{
			List<Modelo.ResumenTarea> LSTareas = new List<ResumenTarea>();
			SortDirection Direction = SortDirection.Ascending;
			if (hfSortDirectionTareasCompletadas.Value.Equals("ASC"))
			{
				hfSortDirectionTareasCompletadas.Value = "DESC";
			}
			else
			{
				hfSortDirectionTareasCompletadas.Value = "ASC";
				Direction = SortDirection.Descending;
			}
			VM.ObtenerTareasCumplidas(GuidDepartamento, SesionActual.uidUsuario, this.DtFechaTurnoSeleccionado);
			LSTareas = VM.ltsTareasCumplidas;
			DVGTareasCumplidas.SelectedIndex = -1;
			DVGTareasCumplidas.DataSource = OrdernarListaTareas(Direction, e.SortExpression, LSTareas);
			DVGTareasCumplidas.DataBind();
		}
		protected void DvgTareasNoCumplidas_Sorting(object sender, GridViewSortEventArgs e)
		{
			List<Modelo.ResumenTarea> LSTareas = new List<ResumenTarea>();
			SortDirection Direction = SortDirection.Ascending;

			if (hfSortDirectionTareasNoCompletadas.Value.Equals("ASC"))
			{
				hfSortDirectionTareasNoCompletadas.Value = "DESC";
			}
			else
			{
				hfSortDirectionTareasNoCompletadas.Value = "ASC";
				Direction = SortDirection.Descending;
			}

			VM.ObtenerTareasNoCumplidas(GuidDepartamento, SesionActual.uidUsuario, this.DtFechaTurnoSeleccionado, SesionActual.uidSucursalActual.Value);
			LSTareas = VM.ltsTareasNoCumplidas;
			DvgTareasNoCumplidas.DataSource = OrdernarListaTareas(Direction, e.SortExpression, LSTareas);
			DvgTareasNoCumplidas.DataBind();
		}
		protected void dgvTareasRequeridas_Sorting(object sender, GridViewSortEventArgs e)
		{
			List<Modelo.ResumenTarea> LSTareas = new List<ResumenTarea>();
			SortDirection Direction = SortDirection.Ascending;
			if (hfSortDirectionTareasRequeridas.Value.Equals("ASC"))
			{
				hfSortDirectionTareasRequeridas.Value = "DESC";
			}
			else
			{
				hfSortDirectionTareasRequeridas.Value = "ASC";
				Direction = SortDirection.Descending;
			}
			VM.ObtenerTareasRequeridas(GuidDepartamento, SesionActual.uidUsuario, this.DtFechaTurnoSeleccionado, SesionActual.uidSucursalActual.Value);
			LSTareas = VM.TareasRequeridas;
			dgvTareasRequeridas.DataSource = OrdernarListaTareas(Direction, e.SortExpression, LSTareas);
			dgvTareasRequeridas.DataBind();
		}
		protected void gvTareasCanceladas_Sorting(object sender, GridViewSortEventArgs e)
		{
			List<Modelo.ResumenTarea> LSTareas = new List<ResumenTarea>();
			SortDirection Direction = SortDirection.Ascending;
			if (hfSortDirectionTareasRequeridas.Value.Equals("ASC"))
			{
				hfSortDirectionTareasRequeridas.Value = "DESC";
			}
			else
			{
				hfSortDirectionTareasRequeridas.Value = "ASC";
				Direction = SortDirection.Descending;
			}
			VM.ObtenerTareasCanceladas(GuidDepartamento, SesionActual.uidUsuario, this.DtFechaTurnoSeleccionado, SesionActual.uidSucursalActual.Value);
			LSTareas = VM.TareasCanceladas;
			gvTareasCanceladas.DataSource = OrdernarListaTareas(Direction, e.SortExpression, LSTareas);
			gvTareasCanceladas.DataBind();
		}
		protected void gvTareasPospuestas_Sorting(object sender, GridViewSortEventArgs e)
		{
			List<Modelo.ResumenTarea> LSTareas = new List<ResumenTarea>();
			SortDirection Direction = SortDirection.Ascending;
			if (hfSortDirectionTareasPospuestas.Value.Equals("ASC"))
			{
				hfSortDirectionTareasPospuestas.Value = "DESC";
			}
			else
			{
				hfSortDirectionTareasPospuestas.Value = "ASC";
				Direction = SortDirection.Descending;
			}
			VM.ObtenerTareasPospuestas(GuidDepartamento, SesionActual.uidUsuario, this.DtFechaTurnoSeleccionado, SesionActual.uidSucursalActual.Value);
			LSTareas = VM.TareasPospuestas;
			gvTareasPospuestas.DataSource = OrdernarListaTareas(Direction, e.SortExpression, LSTareas);
			gvTareasPospuestas.DataBind();
		}

		private List<Modelo.ResumenTarea> OrdernarListaTareas(SortDirection Direction, string Expresion, List<Modelo.ResumenTarea> LsTareas)
		{
			if (Expresion.Equals("FolioC"))
			{
				if (Direction == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.IntFolioCumplimiento).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.IntFolioCumplimiento).ToList();
				}
			}
			else if (Expresion.Equals("FolioT"))
			{
				if (Direction == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.IntFolio).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.IntFolio).ToList();
				}
			}
			else if (Expresion.Equals("Nombre"))
			{
				if (Direction == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrNombre).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrNombre).ToList();
				}
			}
			else if (Expresion.Equals("Estado"))
			{
				if (Direction == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrEstadoCumplimiento).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrEstadoCumplimiento).ToList();
				}
			}
			else if (Expresion.Equals("TipoT"))
			{
				if (Direction == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrTipoTarea).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrTipoTarea).ToList();
				}
			}

			return LsTareas;
		}
		private void OcultarMostrarResumenTarea(bool Status)
		{
			PanelBusquedas.Visible = Status == true ? false : true;
			pnlListaTareas.Visible = Status == true ? false : true;
			pnlResumenTarea.Visible = Status;

			if (!Status)
				lblTittleLeftPanel.Text = "Turnos";
		}
		private void CleanFieldsDetailTask()
		{
			txtDetailNombreTarea.Text = string.Empty;
			txtDetailArea.Text = string.Empty;
			txtDetailDepartamento.Text = string.Empty;
			txtDetailTipoTarea.Text = string.Empty;
			txtDetailPeriodicidad.Text = string.Empty;
			txtDetailValue.Text = string.Empty;
			lblDetailUnidadMedida.Text = string.Empty;
			rbDetailYes.Checked = false;
			rbDetailNo.Checked = false;
			if (ddlDetailOptions.Items.Count > 0)
				ddlDetailOptions.SelectedIndex = 0;
			txtDetailObservaciones.Text = string.Empty;
		}

		public void DisplayMessage(string Text)
		{
			pnlAlertBusqueda.Visible = true;
			lblmensaje.Text = Text;
		}
	}
}