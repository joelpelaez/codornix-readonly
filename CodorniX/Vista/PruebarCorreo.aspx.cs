﻿using CodorniX.Util;
using System;
using System.Net.Mail;
using System.Net.Mime;
using System.Web.UI;

namespace CodorniX.Vista
{
    public partial class PruebarCorreo : Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void btnEnviar_Click(object sender, EventArgs e)
        {
            Correo correo = new Correo();
            PageRender render = PageRender.Instance();
            var html = render.RenderViewToString("Example.cshtml",
                new {Title = txtAsunto.Text, Text = txtMensaje.Text});
            ContentType contentType = new ContentType();
            contentType.Name = fuAttachment.FileName;
            contentType.MediaType = "application/pdf";
            Attachment attachment = new Attachment(fuAttachment.FileContent, contentType);
            correo.EnviarCorreo(txtCorreo.Text, txtAsunto.Text, html, new []{ attachment }, true);
        }
    }
}