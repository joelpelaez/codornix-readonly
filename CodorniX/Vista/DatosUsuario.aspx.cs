﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using CodorniX.Util;
using System.IO;
using System.Web.UI.HtmlControls;

namespace CodorniX.Vista
{
    public partial class DatosUsuario : System.Web.UI.Page
    {
        VMUsuarios VM = new VMUsuarios();
        VMEncargados VME = new VMEncargados();
        #region Propiedades

        Sesion SesionActual
        {
            get { return (Sesion)Session["Sesion"]; }
        }

        private bool EditingMode
        {
            get
            {
                if (ViewState["EditingMode"] == null)
                    return false;

                return (bool)ViewState["EditingMode"];
            }
            set
            {
                ViewState["EditingMode"] = value;
            }
        }

        private bool EditingModeDireccion
        {
            get
            {
                if (ViewState["EditingModeDireccion"] == null)
                    return false;

                return (bool)ViewState["EditingModeDireccion"];
            }
            set
            {
                ViewState["EditingModeDireccion"] = value;
            }
        }

        private List<UsuarioTelefono> TelefonoRemoved
        {
            get
            {
                if (ViewState["TelefonoRemoved"] == null)
                    ViewState["TelefonoRemoved"] = new List<UsuarioTelefono>();

                return (List<UsuarioTelefono>)ViewState["TelefonoRemoved"];
            }
        }
        private List<UsuarioDireccion> DireccionRemoved
        {
            get
            {
                if (ViewState["DireccionRemoved"] == null)
                    ViewState["DireccionRemoved"] = new List<UsuarioDireccion>();

                return (List<UsuarioDireccion>)ViewState["DireccionRemoved"];
            }
        }

        private List<Guid> ModulosBacksite
        {
            get
            {
                if (ViewState["ModulosBackside"] == null)
                    ViewState["ModulosBackside"] = new List<Guid>();

                return (List<Guid>)ViewState["ModulosBackside"];
            }
        }

        private List<Guid> ModulosBackend
        {
            get
            {
                if (ViewState["ModulosBackend"] == null)
                    ViewState["ModulosBackend"] = new List<Guid>();

                return (List<Guid>)ViewState["ModulosBackend"];
            }
        }

        private List<Guid> ModulosFrontend
        {
            get
            {
                if (ViewState["ModulosFrontend"] == null)
                    ViewState["ModulosFrontend"] = new List<Guid>();

                return (List<Guid>)ViewState["ModulosFrontend"];
            }
        }

        private bool Generated = false;

        #endregion

        #region Metodos

        private void ActivarCamposDireccion(bool enable)
        {
            if (enable)
            {
                btnOkDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnOkDireccion.Enabled = true;

                btnCancelarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnCancelarDireccion.Enabled = true;

                ddPais.RemoveCssClass("disabled");
                ddPais.Enabled = true;

                ddEstado.RemoveCssClass("disabled");
                ddEstado.Enabled = true;

                txtMunicipio.RemoveCssClass("disabled");
                txtMunicipio.Enabled = true;

                txtCiudad.RemoveCssClass("disabled");
                txtCiudad.Enabled = true;

                txtColonia.RemoveCssClass("disabled");
                txtColonia.Enabled = true;

                txtCalle.RemoveCssClass("disabled");
                txtCalle.Enabled = true;

                txtConCalle.RemoveCssClass("disabled");
                txtConCalle.Enabled = true;

                txtYCalle.RemoveCssClass("disabled");
                txtYCalle.Enabled = true;

                txtNoExt.RemoveCssClass("disabled");
                txtNoExt.Enabled = true;

                txtNoInt.RemoveCssClass("disabled");
                txtNoInt.Enabled = true;

                txtReferencia.RemoveCssClass("disabled");
                txtReferencia.Enabled = true;

            }
            else
            {
                btnOkDireccion.AddCssClass("disabled");
                btnOkDireccion.Enabled = false;

                btnCancelarDireccion.AddCssClass("disabled");
                btnCancelarDireccion.Enabled = false;

                ddPais.AddCssClass("disabled");
                ddPais.Enabled = false;

                ddEstado.AddCssClass("disabled");
                ddEstado.Enabled = false;

                txtMunicipio.AddCssClass("disabled");
                txtMunicipio.Enabled = false;

                txtCiudad.AddCssClass("disabled");
                txtCiudad.Enabled = false;

                txtColonia.AddCssClass("disabled");
                txtColonia.Enabled = false;

                txtCalle.AddCssClass("disabled");
                txtCalle.Enabled = false;

                txtConCalle.AddCssClass("disabled");
                txtConCalle.Enabled = false;

                txtYCalle.AddCssClass("disabled");
                txtYCalle.Enabled = false;

                txtNoExt.AddCssClass("disabled");
                txtNoExt.Enabled = false;

                txtNoInt.AddCssClass("disabled");
                txtNoInt.Enabled = false;

                txtReferencia.AddCssClass("disabled");
                txtReferencia.Enabled = false;
            }
        }

        protected void ddPais_SelectedIndexChanged(object sender, EventArgs e)
        {
            Guid uid = new Guid(ddPais.SelectedValue.ToString());
            VM.ObtenerEstados(uid);
            ddEstado.DataSource = VM.Estados;
            ddEstado.DataValueField = "UidEstado";
            ddEstado.DataTextField = "StrNombre";
            ddEstado.DataBind();
        }

        private void GenerarCampos(bool regenerate, List<Modulo> modulos, List<Guid> uidModulos, List<Modulo> modulosDenegados, PlaceHolder holder)
        {
            if (regenerate)
                holder.Controls.Clear();

            uidModulos.Clear();

            foreach (Modulo modulo in modulos)
            {
                Panel panel = new Panel();
                panel.AddCssClass("col-xs-6");
                Panel div = new Panel();
                div.AddCssClass("checkbox");
                HtmlGenericControl label = new HtmlGenericControl("label");
                CheckBox checkBox = new CheckBox();
                checkBox.ID = modulo.UidModulo.ToString();
                checkBox.AutoPostBack = false;
                label.Controls.Add(checkBox);
                Label name = new Label();
                name.Text = modulo.StrModulo;
                label.Controls.Add(name);
                div.Controls.Add(label);
                panel.Controls.Add(div);
                holder.Controls.Add(panel);

                uidModulos.Add(modulo.UidModulo);
                if (regenerate)
                {
                    checkBox.Checked = true;
                    foreach (Modulo usuarioModulo in modulosDenegados)
                    {
                        if (usuarioModulo.UidModulo == modulo.UidModulo)
                        {
                            checkBox.Checked = false;
                        }
                    }
                }
            }
        }

        private void GenerarCampos(bool regenerate = false)
        {
            if (!regenerate && Generated)
                return;

            Guid perfil = new Guid(DdPerfil.SelectedValue);
            VM.ObtenerPerfil(perfil);
            Guid nivelAcceso = VM.CLASSPERFIL.UidNivelAcceso;
            VM.ObtenerModulosPerfil(new Guid(DdPerfil.SelectedValue), nivelAcceso);

            VM.ObtenerModulosPerfil(new Guid(DdPerfil.SelectedValue));
            if (!string.IsNullOrWhiteSpace(txtUidUsuario.Text))
                VM.ObtenerModulosUsuario(new Guid(txtUidUsuario.Text));
            else
                VM.ObtenerModulosUsuario(Guid.Empty);

            VM.ObtenerNivelAccesos();
            VM.ObtenerNivelAcceso(nivelAcceso);

            IEnumerable<NivelAcceso> acceso;
            acceso = from e in VM.NivelAccesos where e.StrNivelAcceso == "Backsite" select e;
            VM.ObtenerModulosPerfil(perfil, acceso.FirstOrDefault().UidNivelAcceso);
            GenerarCampos(regenerate, VM.ModulosPerfil, ModulosBacksite, VM.ModulosUsuario, modulosBackside);

            acceso = from e in VM.NivelAccesos where e.StrNivelAcceso == "Backend" select e;
            VM.ObtenerModulosPerfil(perfil, acceso.FirstOrDefault().UidNivelAcceso);
            GenerarCampos(regenerate, VM.ModulosPerfil, ModulosBackend, VM.ModulosUsuario, modulosBackend);

            /*acceso = from e in VM.NivelAccesos where e.StrNivelAcceso == "Frontend" select e;
            VM.ObtenerModulosPerfil(perfil, acceso.FirstOrDefault().UidNivelAcceso);
            GenerarCampos(regenerate, VM.ModulosPerfil, ModulosFrontend, VM.ModulosUsuario, modulosFrontend);*/

            Generated = true;

            string nivel = VM.NivelAcceso.StrNivelAcceso;

            if (nivel == "Backsite")
            {
                accesoBackside.Visible = true;
                accesoBackend.Visible = false;
                accesoFrontend.Visible = false;

                activeBackside.Visible = true;
                activeBackend.Visible = true;
                activeFrontend.Visible = true;

                activeBackside.AddCssClass("active");
                activeBackend.RemoveCssClass("active");
                activeFrontend.RemoveCssClass("active");

                tabBackside.Disable();
                tabBackend.Enable();
                tabFrontend.Enable();
            }
            else if (nivel == "Backend")
            {
                ModulosBacksite.Clear();
                accesoBackside.Visible = false;
                accesoBackend.Visible = true;
                accesoFrontend.Visible = false;

                activeBackside.Visible = false;
                activeBackend.Visible = true;
                activeFrontend.Visible = true;

                activeBackside.RemoveCssClass("active");
                activeBackend.AddCssClass("active");
                activeFrontend.RemoveCssClass("active");

                tabBackside.Enable();
                tabBackend.Disable();
                tabFrontend.Enable();
            }
            else if (nivel == "Frontend")
            {
                ModulosBacksite.Clear();
                ModulosBackend.Clear();

                accesoBackside.Visible = false;
                accesoBackend.Visible = false;
                accesoFrontend.Visible = true;

                activeBackside.Visible = false;
                activeBackend.Visible = false;
                activeFrontend.Visible = true;

                activeBackside.RemoveCssClass("active");
                activeBackend.RemoveCssClass("active");
                activeFrontend.AddCssClass("active");

                tabBackside.Enable();
                tabBackend.Enable();
                tabFrontend.Disable();
            }
        }

        private void ActivarCamposModulo(bool enable)
        {
            List<Guid> modulos = ModulosBacksite;
            foreach (Guid modulo in modulos)
            {
                string controlID = modulo.ToString();
                CheckBox box = (CheckBox)modulosBackside.FindControl(controlID);
                if (enable)
                    box.Enable();
                else
                    box.Disable();
            }
            modulos = ModulosBackend;
            foreach (Guid modulo in modulos)
            {
                string controlID = modulo.ToString();
                CheckBox box = (CheckBox)modulosBackend.FindControl(controlID);
                if (enable)
                    box.Enable();
                else
                    box.Disable();
            }
            modulos = ModulosFrontend;
            foreach (Guid modulo in modulos)
            {
                string controlID = modulo.ToString();
                CheckBox box = (CheckBox)modulosFrontend.FindControl(controlID);
                if (enable)
                    box.Enable();
                else
                    box.Disable();
            }
        }

        #endregion


        protected void Page_Load(object sender, EventArgs e)
        {
            if (SesionActual == null)
                return;

            FUImagen.Attributes["onchange"] = "upload(this)";
            if (!IsPostBack)
            {
                panelAccesos.Visible = false;
                #region Botones
                PanelDatosGeneralesUsuario.Visible = false;
                PanelSucursales.Visible = false;
                btnAceptar.Visible = false;
                btnCancelar.Visible = false;
                txtNombre.Enabled = false;
                txtApellidoPaterno.Enabled = false;
                txtApellidoMaterno.Enabled = false;
                txtFechaNacimiento.Enabled = false;
                txtFechaInicio.Enabled = false;
                txtFechaFin.Enabled = false;
                txtUsuario.Enabled = false;
                txtPassword.Enabled = false;
                txtCorreo.Enabled = false;
                DdPerfil.Enabled = false;
                DdStatus.Enabled = false;
                txtRfc.Enabled = false;
                txtRazonSocial.Enabled = false;
                txtNombreComercial.Enabled = false;
                //lblsucursal.Visible = false;
                txtNombreSucursal.Enabled = false;
                txtTipoSucursal.Enabled = false;



                #endregion

                #region DatosTelefono
                btnAceptarEliminarTelefono.Visible = false;
                btnCancelarEliminarTelefono.Visible = false;
                VM.ObtenerTipoTelefonos();
                ddTipoTelefono.DataSource = VM.TipoTelefonos;
                ddTipoTelefono.DataValueField = "UidTipoTelefono";
                ddTipoTelefono.DataTextField = "StrTipoTelefono";
                ddTipoTelefono.DataBind();
                #endregion

                #region DatosDireccion

                txtMunicipio.Enabled = false;
                txtCiudad.Enabled = false;
                txtColonia.Enabled = false;
                txtCalle.Enabled = false;
                txtConCalle.Enabled = false;
                txtYCalle.Enabled = false;
                txtNoExt.Enabled = false;
                txtNoInt.Enabled= false;
                txtReferencia.Enabled =false;
                ddPais.Enabled = false;
                ddEstado.Enabled = false;


                btnOkDireccion.Visible = false;
                btnCancelarDireccion.Visible = false;
                btnCancelarEliminarDireccion.Visible = false;
                btnAceptarEliminarDireccion.Visible = false;

                VM.ObtenerPaises();
                ddPais.DataSource = VM.Paises;
                ddPais.DataValueField = "UidPais";
                ddPais.DataTextField = "StrNombre";
                ddPais.DataBind();

                ddPais_SelectedIndexChanged(null, null);
                #endregion

                #region Obtencion de datos
               
                Usuario usuario = new Usuario.Repository().Find(new Guid(SesionActual.uidUsuario.ToString()));
                Guid UidUsuario = usuario.UIDUSUARIO;
                VM.Obtenerusuario(UidUsuario);
                txtUidUsuario.Text = VM.CLASSUSUARIO.UIDUSUARIO.ToString();
                txtNombre.Text = VM.CLASSUSUARIO.STRNOMBRE;
                txtApellidoPaterno.Text = VM.CLASSUSUARIO.STRAPELLIDOPATERNO;
                txtApellidoMaterno.Text = VM.CLASSUSUARIO.STRAPELLIDOMATERNO;
                txtFechaNacimiento.Text = VM.CLASSUSUARIO.DtFechaNacimiento.ToString("dd/MM/yyyy");
                txtFechaInicio.Text = VM.CLASSUSUARIO.DtFechaInicio.ToString("dd/MM/yyyy");
                txtFechaFin.Text = VM.CLASSUSUARIO.DtFechaFin?.ToString("dd/MM/yyyy");
                txtUsuario.Text = VM.CLASSUSUARIO.STRUSUARIO.ToString();
                txtPassword.Text = VM.CLASSUSUARIO.STRPASSWORD.ToString();
                txtCorreo.Text = VM.CLASSUSUARIO.STRCORREO.ToString();
                DdStatus.SelectedIndex = DdStatus.Items.IndexOf(DdStatus.Items.FindByValue(VM.CLASSUSUARIO.UidStatus.ToString()));
                ImgUsuario.ImageUrl = Page.ResolveUrl(VM.CLASSUSUARIO.RutaImagen);
                //VM.ObtenerUsuarioPerfilEmpresa(UidUsuario);
                //DdEmpresa.SelectedValue = SesionActual.uidEmpresaActual.Value.ToString();
                DdPerfil.SelectedValue = SesionActual.uidPerfilActual.Value.ToString();

                txtUidEmpresa.Text = SesionActual.uidEmpresaActual.Value.ToString();
                VM.ObtenerEmpresaUsuario(txtUidEmpresa.Text);
                txtRfc.Text = VM.CEmpresa.StrRFC;
                txtRazonSocial.Text = VM.CEmpresa.StrRazonSocial;
                txtNombreComercial.Text = VM.CEmpresa.StrNombreComercial;



                List<UsuarioPerfilSucursal> sp = new UsuarioPerfilSucursal.Repository().FindAll(UidUsuario);
                if (sp.Count > 0)
                {
                    txtUidSucursal.Text = SesionActual.uidSucursalActual.Value.ToString();
                    VME.Obtener(new Guid(txtUidSucursal.Text));
                    txtNombreSucursal.Text = VME.CSucursal.StrNombre;
                    txtTipoSucursal.Text = VME.CSucursal.StrTipoSucursal;
                }
                else
                {
                    //lblsucursal.Visible = true;
                    //lblsucursal.Text = "No tiene sucursal asignada";
                    txtNombreSucursal.Text = "No hay sucursales asignadas";
                    txtTipoSucursal.Text = "No hay sucursales asignadas";
                }
                
                DdPerfil.SelectedValue = SesionActual.uidPerfilActual.Value.ToString();
                
                VM.ObtenerTelefonos();
                ViewState["Telefonos"] = VM.Telefonos;
                dgvTelefonos.DataSource = VM.Telefonos;
                dgvTelefonos.DataBind();
                btnEditar.Enabled = true;
                btnEditar.CssClass = "btn btn-sm btn-default";

                VM.ObtenerDirecciones();
                ViewState["Direcciones"] = VM.Direcciones;
                dgvDirecciones.DataSource = VM.Direcciones;
                dgvDirecciones.DataBind();

                VM.CargarListaDePerfilTodos();
                DdPerfil.DataSource = VM.LISTADEPERFIL;
                DdPerfil.DataTextField = "strPerfil";
                DdPerfil.DataValueField = "UidPerfil";
                DdPerfil.DataBind();

                VM.CargarListaDeStatus();
                DdStatus.DataSource = VM.LISTADESTATUS;
                DdStatus.DataTextField = "strStatus";
                DdStatus.DataValueField = "UidStatus";
                DdStatus.DataBind();

                /*VM.ObtenerEmpresa();
                DdEmpresa.DataSource = VM.Empresas;
                DdEmpresa.DataValueField = "UidEmpresa";
                DdEmpresa.DataTextField = "StrNombreComercial";
                DdEmpresa.DataBind();*/ 

                /*VME.ObtenerSucursales(SesionActual.uidEmpresaActual.Value);
                DdSucursal.DataSource = VME.Sucursales;
                DdSucursal.DataValueField = "UidSucursal";
                DdSucursal.DataTextField = "StrNombre";
                DdSucursal.DataBind();
                if (SesionActual.uidSucursalActual.HasValue)
                    DdSucursal.SelectedValue = SesionActual.uidSucursalActual.Value.ToString();*/
                FUImagen.Enabled = false;
                #endregion

                GenerarCampos(true);
                ActivarCamposModulo(false);
            }
            else
            {
                GenerarCampos();
            }
        }
        
        #region DatosGeneralesdelUsuario

        protected void tabDatos_Click(object sender, EventArgs e)
        {
            PanelDatosGeneralesUsuario.Visible = true;
            panelDirecciones.Visible = false;
            panelTelefonos.Visible = false;
            PanelEmpresa.Visible = false;
            PanelSucursales.Visible = false;
            panelAccesos.Visible = false;

            activeAccesos.Attributes["class"] = "";

            activeDatosGenerales.Attributes["class"] = "active";

            activeDirecciones.Attributes["class"] = "";

            activeTelefonos.Attributes["class"] = "";

            activeEmpresas.RemoveCssClass("active");

            activeSucursales.RemoveCssClass("active");

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            else
            {
                //panelDireccion.Visible = false;
            }
        }

        protected void tabDirecciones_Click(object sender, EventArgs e)
        {

            panelDirecciones.Visible = true;
            PanelDatosGeneralesUsuario.Visible = false;
            panelTelefonos.Visible = false;
            PanelSucursales.Visible = false;
            PanelEmpresa.Visible = false;
            panelAccesos.Visible = false;
            activeEmpresas.RemoveCssClass("active");
            activeAccesos.Attributes["class"] = "";

            activeDatosGenerales.Attributes["class"] = "";

            activeDirecciones.Attributes["class"] = "active";

            activeTelefonos.Attributes["class"] = "";

            activeSucursales.RemoveCssClass("active");
        }

        protected void tabTelefonos_Click(object sender, EventArgs e)
        {
            panelTelefonos.Visible = true;
            PanelEmpresa.Visible = false;
            PanelDatosGeneralesUsuario.Visible = false;
            panelDirecciones.Visible = false;
            PanelSucursales.Visible = false;
            panelAccesos.Visible = false;
            activeSucursales.RemoveCssClass("active");
            activeAccesos.Attributes["class"] = "";

            activeDatosGenerales.Attributes["class"] = "";

            activeDirecciones.Attributes["class"] = "";

            activeTelefonos.Attributes["class"] = "active";

            activeEmpresas.RemoveCssClass("active");

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            
        }

        protected void btnEditar_Click(object sender, EventArgs e)
        {
            EditingMode = true;
            btnAceptar.Visible = true;
            btnCancelar.Visible = true;
            btnEditar.Enabled = false;
            btnEditar.AddCssClass("disabled");
            txtUidUsuario.Enabled = true;

            FUImagen.Enabled = true;
            txtCorreo.Enabled = true;
            txtUsuario.Enabled = true;
            txtPassword.Enabled = true;
            lblMensaje.Text = string.Empty;
            
            if (uidDireccion.Text.Length > 0)
            {
                btnEditarDireccion.Enable();
                btnEliminarDireccion.Enable();
            }
            if (uidTelefono.Text.Length > 0)
            {
                btnEditarTelefono.Enable();
                btnEliminarTelefono.Enable();
            }

            btnAgregarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarDireccion.Enabled = true;
            btnAgregarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnAgregarTelefono.Enabled = true;
            FUImagen.Enabled = true;
        }

        protected void btnAceptar_Click(object sender, EventArgs e)
        {
            EditingMode = false;
            string Nombre = txtNombre.Text;
            string ApellidoPaterno = txtApellidoPaterno.Text;
            string ApellidoMaterno = txtApellidoMaterno.Text;
            string Correo = txtCorreo.Text;
            string FechaNacimiento = txtFechaNacimiento.Text;
            string FechaInicio = txtFechaInicio.Text;
            string FechaFin = txtFechaFin.Text;
            string UidPerfil = DdPerfil.SelectedValue;
            string UidStatus = DdStatus.SelectedValue;

            frmGrpUsuario.RemoveCssClass("has-error");
            frmGrpPassword.RemoveCssClass("has-error");

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            frmGrpTelefono.RemoveCssClass("has-error");


            string Usuario = txtUsuario.Text;

            if (txtPassword.Text.Trim() == string.Empty)
            {
                lblErrorUsuario.Text = "El campo Contraseña tiene que ser llenado necesariamente.";
                txtPassword.Focus();
                frmGrpPassword.AddCssClass("has-error");

                return;
            }
            string Password = txtPassword.Text;


            if (ViewState["rutaimg"] != null)
            {
                Usuario usuario = new Usuario.Repository().Find(new Guid(SesionActual.uidUsuario.ToString()));
                Guid UidUsuario = usuario.UIDUSUARIO;

                VM.Obtenerusuario(UidUsuario);
                string Ruta = VM.CLASSUSUARIO.RutaImagen;

                if (File.Exists(Server.MapPath(Ruta)))
                {
                    File.Delete(Server.MapPath(Ruta));

                }
            }
            else
            {
                Usuario usuario = new Usuario.Repository().Find(new Guid(SesionActual.uidUsuario.ToString()));
                Guid UidUsuario = usuario.UIDUSUARIO;

                VM.Obtenerusuario(UidUsuario);
                ViewState["rutaimg"] = VM.CLASSUSUARIO.RutaImagen;
            }


            if (VM.ModificarUsuario(txtUidUsuario.Text, Nombre, ApellidoPaterno, ApellidoMaterno, FechaNacimiento,
                Correo, FechaInicio, FechaFin, Usuario, Password, UidStatus, ViewState["rutaimg"].ToString()))
            {
                if (VM.ModificarUsuarioPerfilEmpresa(txtUidUsuario.Text, UidPerfil,txtUidEmpresa.Text))
                {
                    lblMensaje.Text = "Se ha actualizado";

                }
                btnAceptar.Visible = false;
                btnCancelar.Visible = false;
                btnEditar.Enabled = false;
                btnEditar.CssClass = "btn btn-sm disabled btn-default";
                txtNombre.Enabled = false;
                txtApellidoPaterno.Enabled = false;
                txtApellidoMaterno.Enabled = false;
                txtFechaNacimiento.Enabled = false;
                txtCorreo.Enabled = false;
                txtFechaInicio.Enabled = false;
                txtFechaFin.Enabled = false;
                txtUsuario.Enabled = false;
                txtPassword.Enabled = false;
                DdPerfil.Enabled = false;
                DdPerfil.CssClass = "form-control";
                DdStatus.Enabled = false;

            }
            else
            {
                lblMensaje.Text = "No se ha actualizado";
            }


            Guid uidUsuario = VM.CLASSUSUARIO.UIDUSUARIO;

            List<UsuarioTelefono> telefonos = (List<UsuarioTelefono>)ViewState["Telefonos"];
            VM.GuardarTelefonos(telefonos, uidUsuario);

            VM.EliminarTelefonos(TelefonoRemoved);

            List<UsuarioDireccion> direcciones = (List<UsuarioDireccion>)ViewState["Direcciones"];
            VM.GuardarDirecciones(direcciones, uidUsuario);
            VM.EliminarDirecciones(DireccionRemoved);
            if (DdPerfil.SelectedValue == "Superadministrador")
            {
                VM.GuardarUsuarioPerfilEmpresa(UidPerfil, uidUsuario, Guid.Empty.ToString());
            }
            else
            {
                if (txtUidUsuario.Text == string.Empty)
                {
                    VM.GuardarUsuarioPerfilEmpresa(UidPerfil, uidUsuario, txtUidEmpresa.Text);
                }

            }


            btnAgregarTelefono.Enabled = false;
            btnAgregarTelefono.CssClass = "btn btn-sm disabled btn-default";
            btnAgregarDireccion.Enabled = false;
            btnAgregarDireccion.CssClass = "btn btn-sm disabled btn-default";
            VM.ObtenerTelefonos();
            ViewState["Telefonos"] = VM.Telefonos;
            dgvTelefonos.DataSource = VM.Telefonos;
            dgvTelefonos.DataBind();
            btnEditar.Enabled = true;
            btnEditar.CssClass = "btn btn-sm btn-default";

            VM.ObtenerDirecciones();
            ViewState["Direcciones"] = VM.Direcciones;
            dgvDirecciones.DataSource = VM.Direcciones;
            dgvDirecciones.DataBind();
            FUImagen.Enabled = false;
        }

        protected void btnCancelar_Click(object sender, EventArgs e)
        {
            EditingMode = false;
            btnCancelarDireccion.Visible = false;
            btnOkDireccion.Visible = false;
            btnCancelarTelefono.Visible = false;
            btnOKTelefono.Visible = false;
            ddTipoTelefono.Enabled = false;
            txtTelefono.Enabled = false;

            frmGrpNombre.RemoveCssClass("has-error");
            frmGrpApellidoPaterno.RemoveCssClass("has-error");
            frmGrpApellidoMaterno.RemoveCssClass("has-error");
            frmGrpFechaNacimiento.RemoveCssClass("has-error");
            frmGrpFechaInicio.RemoveCssClass("has-error");
            frmGrpFechaFin.RemoveCssClass("has-error");
            frmGrpUsuario.RemoveCssClass("has-error");
            frmGrpPassword.RemoveCssClass("has-error");

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");

            frmGrpTelefono.RemoveCssClass("has-error");

            //Activa cajas
            txtNombre.Enabled = false;
            txtApellidoPaterno.Enabled = false;
            txtApellidoMaterno.Enabled = false;
            txtFechaNacimiento.Enabled = false;
            txtCorreo.Enabled = false;
            //txtCurp.Enabled = false;
            txtFechaNacimiento.Enabled = false;
            txtFechaInicio.Enabled = false;
            txtFechaFin.Enabled = false;
            txtUsuario.Enabled = false;
            txtPassword.Enabled = false;
            DdPerfil.Enabled = false;
            DdStatus.Enabled = false;
            //Muestra botones de guardar
            btnAceptar.Visible = false;
            btnCancelar.Visible = false;
            FUImagen.Enabled = false;

            btnAgregarTelefono.Enabled = false;
            btnAgregarTelefono.CssClass = "btn btn-sm disabled btn-default";
            btnEditarTelefono.Enabled = false;
            btnEditarTelefono.AddCssClass("disabled");
            btnEliminarTelefono.Enabled = false;
            btnEliminarTelefono.AddCssClass("disabled");

            btnAgregarDireccion.Enabled = false;
            btnAgregarDireccion.CssClass = "btn btn-sm disabled btn-default";
            btnEditarDireccion.Enabled = false;
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.Enabled = false;
            btnEliminarDireccion.AddCssClass("disabled");

            btnEditar.Enabled = true;
            btnEditar.RemoveCssClass("disabled");

            btnCancelarEliminarDireccion_Click(sender, e);
            btnCancelarEliminarTelefono_Click(sender, e);

            if (txtUidUsuario.Text.Length == 0)
            {
                btnEditar.Enabled = false;
                btnEditar.AddCssClass("disabled");
                //Limpia cajas de texto
                txtNombre.Text = string.Empty;
                txtApellidoPaterno.Text = string.Empty;
                txtApellidoMaterno.Text = string.Empty;
                txtCorreo.Text = string.Empty;
                //txtCurp.Text = string.Empty;
                txtFechaNacimiento.Text = string.Empty;
                txtFechaInicio.Text = string.Empty;
                txtFechaFin.Text = string.Empty;
                txtUsuario.Text = string.Empty;
                txtPassword.Text = string.Empty;

                int pos;
                if (ViewState["DireccionPreviousRow"] != null)
                {
                    pos = (int)ViewState["DireccionPreviousRow"];
                    GridViewRow previousRow = dgvDirecciones.Rows[pos];
                    previousRow.RemoveCssClass("success");
                }

                if (ViewState["TelefonoPreviousRow"] != null)
                {
                    pos = (int)ViewState["TelefonoPreviousRow"];
                    GridViewRow previousRow = dgvTelefonos.Rows[pos];
                    previousRow.RemoveCssClass("success");
                }


                ViewState["Telefonos"] = null;
                ViewState["Direcciones"] = null;
                dgvDirecciones.DataSource = null;
                dgvDirecciones.DataBind();
                dgvTelefonos.DataSource = null;
                dgvTelefonos.DataBind();
                if (Session["RutaImagen"] != null)
                {
                    string Ruta = Session["RutaImagen"].ToString();

                    if (File.Exists(Server.MapPath(Ruta)))
                    {
                        File.Delete(Server.MapPath(Ruta));
                    }
                    ImgUsuario.ImageUrl = "Img/Default.jpg";
                    ImgUsuario.DataBind();
                }
                
            }
            else
            {
                VM.Obtenerusuario(new Guid(txtUidUsuario.Text));
                txtUidUsuario.Text = VM.CLASSUSUARIO.UIDUSUARIO.ToString();
                txtNombre.Text = VM.CLASSUSUARIO.STRNOMBRE;
                txtApellidoPaterno.Text = VM.CLASSUSUARIO.STRAPELLIDOPATERNO;
                txtApellidoMaterno.Text = VM.CLASSUSUARIO.STRAPELLIDOMATERNO;
                txtFechaNacimiento.Text = VM.CLASSUSUARIO.DtFechaNacimiento.ToString("dd/MM/yyyy");
                txtCorreo.Text = VM.CLASSUSUARIO.STRCORREO;
                txtFechaInicio.Text = VM.CLASSUSUARIO.DtFechaInicio.ToString("dd/MM/yyyy");
                txtFechaFin.Text = VM.CLASSUSUARIO.DtFechaFin?.ToString("dd/MM/yyyy");
                txtUsuario.Text = VM.CLASSUSUARIO.STRUSUARIO;
                txtPassword.Text = VM.CLASSUSUARIO.STRPASSWORD;
                ImgUsuario.ImageUrl = Page.ResolveUrl(VM.CLASSUSUARIO.RutaImagen);
                DdPerfil.SelectedIndex = DdPerfil.Items.IndexOf(DdPerfil.Items.FindByValue(VM.CLASSUSUARIO.UidPerfil.ToString()));
                DdStatus.SelectedIndex = DdStatus.Items.IndexOf(DdStatus.Items.FindByValue(VM.CLASSUSUARIO.UidStatus.ToString()));

                VM.ObtenerUsuarioPerfilEmpresa(new Guid(txtUidUsuario.Text));
                txtUidEmpresa.Text = VM.UsuarioPerfilEmpresa.UidEmpresa.ToString();
                VM.ObtenerEmpresaUsuario(txtUidEmpresa.Text);
                txtRfc.Text = VM.CEmpresa.StrRFC;
                txtRazonSocial.Text = VM.CEmpresa.StrRazonSocial;
                txtNombreComercial.Text = VM.CEmpresa.StrNombreComercial;

                List<UsuarioPerfilSucursal> sp = new UsuarioPerfilSucursal.Repository().FindAll(new Guid(txtUidUsuario.Text));
                if (sp.Count > 0)
                {
                    txtUidSucursal.Text = SesionActual.uidSucursalActual.Value.ToString();
                    VME.Obtener(new Guid(txtUidSucursal.Text));
                    txtNombreSucursal.Text = VME.CSucursal.StrNombre;
                    txtTipoSucursal.Text = VME.CSucursal.StrTipoSucursal;
                }
                else
                {
                    txtNombreSucursal.Text = "No hay sucursales asignadas";
                    txtTipoSucursal.Text = "No hay sucursales asignadas";
                }



                VM.ObtenerTelefonos();
                ViewState["Telefonos"] = VM.Telefonos;
                dgvTelefonos.DataSource = VM.Telefonos;
                dgvTelefonos.DataBind();
                btnEditar.Enabled = true;
                btnEditar.CssClass = "btn btn-sm btn-default";

                VM.ObtenerDirecciones();
                ViewState["Direcciones"] = VM.Direcciones;
                dgvDirecciones.DataSource = VM.Direcciones;
                dgvDirecciones.DataBind();
            }
            FUImagen.Enabled = false;
        }

        #endregion


        #region Panel Direcciones

        protected void dgvDirecciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvDirecciones, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvDirecciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<UsuarioDireccion> direcciones = (List<UsuarioDireccion>)ViewState["Direcciones"];
            UsuarioDireccion empresaDireccion = direcciones.Select(x => x).Where(x => x.UidDireccion.ToString() == dgvDirecciones.SelectedDataKey.Value.ToString()).First();
            uidDireccion.Text = empresaDireccion.UidDireccion.ToString();
            ddPais.SelectedValue = empresaDireccion.UidPais.ToString();
            ddPais_SelectedIndexChanged(sender, e);
            ddEstado.SelectedValue = empresaDireccion.UidEstado.ToString();
            txtMunicipio.Text = empresaDireccion.StrMunicipio;
            txtCiudad.Text = empresaDireccion.StrCiudad;
            txtColonia.Text = empresaDireccion.StrColonia;
            txtCalle.Text = empresaDireccion.StrCalle;
            txtConCalle.Text = empresaDireccion.StrConCalle;
            txtYCalle.Text = empresaDireccion.StrYCalle;
            txtReferencia.Text = empresaDireccion.StrReferencia;
            txtNoExt.Text = empresaDireccion.StrNoExt;
            txtNoInt.Text = empresaDireccion.StrNoInt;

            if (EditingMode)
            {
                btnEditarDireccion.Enabled = true;
                btnEditarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEliminarDireccion.Enabled = true;
                btnEliminarDireccion.RemoveCssClass("disabled").RemoveCssClass("hidden");
            }

            int pos = -1;
            if (ViewState["DireccionPreviousRow"] != null)
            {
                pos = (int)ViewState["DireccionPreviousRow"];
                GridViewRow previousRow = dgvDirecciones.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["DireccionPreviousRow"] = dgvDirecciones.SelectedIndex;
            dgvDirecciones.SelectedRow.AddCssClass("success");
            
            btnCancelarDireccion.Visible = false;
            btnOkDireccion.Visible = false;
        }

        protected void btnCancelarDireccion_Click(object sender, EventArgs e)
        {

            EditingModeDireccion = false;

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");
            lblErrorDireccion.Text = string.Empty;

            ActivarCamposDireccion(false);

            uidDireccion.Text = string.Empty;
            ddPais.SelectedIndex = 0;
            ddEstado.SelectedIndex = 0;
            txtMunicipio.Text = string.Empty;
            txtCiudad.Text = string.Empty;
            txtColonia.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtConCalle.Text = string.Empty;
            txtYCalle.Text = string.Empty;
            txtNoExt.Text = string.Empty;
            txtNoInt.Text = string.Empty;
            txtReferencia.Text = string.Empty;

            btnAgregarDireccion.Enabled = true;
            btnAgregarDireccion.RemoveCssClass("disabled");

            if (uidDireccion.Text.Length == 0)
            {
                btnEditarDireccion.Enabled = true;
                btnEditarDireccion.RemoveCssClass("disabled");

                btnEliminarDireccion.Enabled = true;
                btnEliminarDireccion.RemoveCssClass("disabled");
            }
            else
            {
                btnEditarDireccion.Enabled = false;
                btnEditarDireccion.AddCssClass("disabled");
                btnEliminarDireccion.Enabled = false;
                btnEliminarDireccion.AddCssClass("disabled");
            }
        }

        protected void btnOkDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = false;

            frmGrpMunicipio.RemoveCssClass("has-error");
            frmGrpCiudad.RemoveCssClass("has-error");
            frmGrpColonia.RemoveCssClass("has-error");
            frmGrpCalle.RemoveCssClass("has-error");
            frmGrpConCalle.RemoveCssClass("has-error");
            frmGrpYCalle.RemoveCssClass("has-error");
            frmGrpNoExt.RemoveCssClass("has-error");
            lblErrorDireccion.Text = string.Empty;

            if (string.IsNullOrWhiteSpace(txtMunicipio.Text))
            {
                lblErrorDireccion.Text = "El campo Municipio no debe estar vacío";
                txtMunicipio.Focus();
                frmGrpMunicipio.AddCssClass("has-error");

                return;
            }

            if (string.IsNullOrWhiteSpace(txtCiudad.Text))
            {
                lblErrorDireccion.Text = "El campo Ciudad no debe estar vacío";
                txtCiudad.Focus();
                frmGrpCiudad.AddCssClass("has-error");

                return;
            }

            if (string.IsNullOrWhiteSpace(txtColonia.Text))
            {
                lblErrorDireccion.Text = "El campo Colonia no debe estar vacío";
                txtColonia.Focus();
                frmGrpColonia.AddCssClass("has-error");

                return;
            }

            if (string.IsNullOrWhiteSpace(txtCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Calle no debe estar vacío";
                txtCalle.Focus();
                frmGrpCalle.AddCssClass("has-error");

                return;
            }

            if (string.IsNullOrWhiteSpace(txtConCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Con Calle no debe estar vacío";
                txtConCalle.Focus();
                frmGrpConCalle.AddCssClass("has-error");

                return;
            }

            if (string.IsNullOrWhiteSpace(txtYCalle.Text))
            {
                lblErrorDireccion.Text = "El campo Y Calle no debe estar vacío";
                txtYCalle.Focus();
                frmGrpYCalle.AddCssClass("has-error");

                return;
            }

            if (string.IsNullOrWhiteSpace(txtNoExt.Text))
            {
                lblErrorDireccion.Text = "El campo No. Exterior no debe estar vacío";
                txtNoExt.Focus();
                frmGrpNoExt.AddCssClass("has-error");

                return;
            }

            List<UsuarioDireccion> direcciones = (List<UsuarioDireccion>)ViewState["Direcciones"];
            UsuarioDireccion direccion = null;
            int pos = -1;
            if (!string.IsNullOrWhiteSpace(uidDireccion.Text))
            {
                IEnumerable<UsuarioDireccion> dir = from d in direcciones where d.UidDireccion.ToString() == uidDireccion.Text select d;
                direccion = dir.First();
                pos = direcciones.IndexOf(direccion);
                direcciones.Remove(direccion);
            }
            else
            {
                direccion = new UsuarioDireccion();
                direccion.UidDireccion = Guid.NewGuid();
            }
            direccion.UidPais = new Guid(ddPais.SelectedValue);
            direccion.UidEstado = new Guid(ddEstado.SelectedValue);
            direccion.StrMunicipio = txtMunicipio.Text;
            direccion.StrCiudad = txtCiudad.Text;
            direccion.StrColonia = txtColonia.Text;
            direccion.StrCalle = txtCalle.Text;
            direccion.StrConCalle = txtConCalle.Text;
            direccion.StrYCalle = txtYCalle.Text;
            direccion.StrNoExt = txtNoExt.Text;
            direccion.StrNoInt = txtNoInt.Text;
            direccion.StrReferencia = txtReferencia.Text;

            ActivarCamposDireccion(false);
            if (pos < 0)
                direcciones.Add(direccion);
            else
                direcciones.Insert(pos, direccion);

            dgvDirecciones.DataSource = direcciones;
            dgvDirecciones.DataBind();


            btnAgregarDireccion.RemoveCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            
        }

        protected void btnAgregarDireccion_Click(object sender, EventArgs e)
        {

            EditingModeDireccion = true;
            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            uidDireccion.Text = string.Empty;
            ddPais.SelectedIndex = 0;
            ddEstado.SelectedIndex = 0;
            txtMunicipio.Text = string.Empty;
            txtCiudad.Text = string.Empty;
            txtColonia.Text = string.Empty;
            txtCalle.Text = string.Empty;
            txtConCalle.Text = string.Empty;
            txtYCalle.Text = string.Empty;
            txtNoExt.Text = string.Empty;
            txtNoInt.Text = string.Empty;
            txtReferencia.Text = string.Empty;

            ActivarCamposDireccion(true);
            
            
            btnOkDireccion.Visible = true;
            btnCancelarDireccion.Visible = true;

            int pos = -1;
            if (ViewState["DireccionPreviousRow"] != null)
            {
                pos = (int)ViewState["DireccionPreviousRow"];
                GridViewRow previousRow = dgvDirecciones.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
        }

        protected void btnEditarDireccion_Click(object sender, EventArgs e)
        {
            EditingModeDireccion = true;

            btnAgregarDireccion.AddCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            ActivarCamposDireccion(true);
            

            btnOkDireccion.Visible = true;
            btnCancelarDireccion.Visible = true;
        }

        protected void btnEliminarDireccion_Click(object sender, EventArgs e)
        {
            btnAceptarEliminarDireccion.Visible = true;
            lblAceptarEliminarDireccion.Visible = true;
            lblAceptarEliminarDireccion.Text = "Desea Eliminar La Direccion Seleccionada. ";

            btnCancelarEliminarDireccion.Visible = true;
        }

        protected void btnAceptarEliminarDireccion_Click(object sender, EventArgs e)
        {
            ActivarCamposDireccion(false);
            Guid uid = new Guid(uidDireccion.Text);

            List<UsuarioDireccion> direcciones = (List<UsuarioDireccion>)ViewState["Direcciones"];
            UsuarioDireccion direccion = direcciones.Select(x => x).Where(x => x.UidDireccion == uid).First();
            direcciones.Remove(direccion);
            DireccionRemoved.Add(direccion);
            dgvDirecciones.DataSource = direcciones;
            dgvDirecciones.DataBind();

            btnAgregarDireccion.RemoveCssClass("disabled");
            btnEditarDireccion.AddCssClass("disabled");
            btnEliminarDireccion.AddCssClass("disabled");
            btnAceptarEliminarDireccion.Visible = false;
            btnCancelarEliminarDireccion.Visible = false;
            lblAceptarEliminarDireccion.Visible = false;
        }

        protected void btnCancelarEliminarDireccion_Click(object sender, EventArgs e)
        {
            btnAceptarEliminarDireccion.Visible = false;
            btnCancelarEliminarDireccion.Visible = false;
            lblAceptarEliminarDireccion.Visible = false;
        }

        #endregion


        #region Datos del Telefono

        protected void dgvTelefonos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTelefonos, "Select$" + e.Row.RowIndex);
            }
        }

        protected void dgvTelefonos_SelectedIndexChanged(object sender, EventArgs e)
        {

            List<UsuarioTelefono> telefonos = (List<UsuarioTelefono>)ViewState["Telefonos"];
            UsuarioTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono.ToString() == dgvTelefonos.SelectedDataKey.Value.ToString()).First();

            uidTelefono.Text = telefono.UidTelefono.ToString();
            txtTelefono.Text = telefono.StrTelefono;
            ddTipoTelefono.SelectedValue = telefono.UidTipoTelefono.ToString();

            if (EditingMode)
            {
                btnEditarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEditarTelefono.Enabled = true;
                btnEliminarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
                btnEliminarTelefono.Enabled = true;
                btnOKTelefono.AddCssClass("disabled").AddCssClass("hidden");
                btnOKTelefono.Enabled = false;
                btnCancelarTelefono.AddCssClass("disabled").AddCssClass("hidden");
                btnCancelarTelefono.Enabled = false;
            }

            int pos = -1;
            if (ViewState["TelefonoPreviousRow"] != null)
            {
                pos = (int)ViewState["TelefonoPreviousRow"];
                GridViewRow previousRow = dgvTelefonos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }

            ViewState["TelefonoPreviousRow"] = dgvTelefonos.SelectedIndex;
            dgvTelefonos.SelectedRow.AddCssClass("success");
        }

        protected void btnAgregarTelefono_Click(object sender, EventArgs e)
        {
            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtTelefono.Enabled = true;
            txtTelefono.RemoveCssClass("disabled");
            ddTipoTelefono.SelectedIndex = 0;
            ddTipoTelefono.RemoveCssClass("disabled");
            ddTipoTelefono.Enabled = true;

            btnOKTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnOKTelefono.Enabled = true;
            btnCancelarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
            btnCancelarTelefono.Enabled = true;

            btnAgregarTelefono.Enabled = false;
            btnAgregarTelefono.AddCssClass("disabled");
            btnEditarTelefono.Enabled = false;
            btnEditarTelefono.AddCssClass("disabled");
            btnEliminarTelefono.Enabled = false;
            btnEliminarTelefono.AddCssClass("disabled");

            int pos = -1;
            if (ViewState["TelefonoPreviousRow"] != null)
            {
                pos = (int)ViewState["TelefonoPreviousRow"];
                GridViewRow previousRow = dgvTelefonos.Rows[pos];
                previousRow.RemoveCssClass("success");
            }
        }

        protected void btnEditarTelefono_Click(object sender, EventArgs e)
        {
            txtTelefono.Enabled = true;
            txtTelefono.RemoveCssClass("disabled");

            ddTipoTelefono.Enabled = true;
            ddTipoTelefono.RemoveCssClass("disabled");

            btnAgregarTelefono.Enabled = false;
            btnAgregarTelefono.AddCssClass("disabled");

            btnEditarTelefono.Enabled = false;
            btnEditarTelefono.AddCssClass("disabled");

            btnEliminarTelefono.Enabled = false;
            btnEliminarTelefono.AddCssClass("disabled");

            btnOKTelefono.Enabled = true;
            btnOKTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");

            btnCancelarTelefono.Enabled = true;
            btnCancelarTelefono.RemoveCssClass("disabled").RemoveCssClass("hidden");
        }

        protected void btnEliminarTelefono_Click(object sender, EventArgs e)
        {
            lblAceptarEliminarTelefono.Visible = true;
            lblAceptarEliminarTelefono.Text = "¿Desea Eliminar El Telefono Seleccionado?";
            btnAceptarEliminarTelefono.Visible = true;
            btnCancelarEliminarTelefono.Visible = true;
        }

        protected void btnOKTelefono_Click(object sender, EventArgs e)
        {
            frmGrpTelefono.RemoveCssClass("has-error");
            lblErrorTelefono.Text = string.Empty;

            if (string.IsNullOrWhiteSpace(txtTelefono.Text))
            {
                lblErrorTelefono.Text = "El campo Telefono no debe estar vacío";
                txtTelefono.Focus();
                frmGrpTelefono.AddCssClass("has-error");

                return;
            }
            List<UsuarioTelefono> telefonos = (List<UsuarioTelefono>)ViewState["Telefonos"];
            UsuarioTelefono telefono = null;
            int pos = -1;
            if (!string.IsNullOrWhiteSpace(uidTelefono.Text))
            {
                IEnumerable<UsuarioTelefono> dir = from t in telefonos where t.UidTelefono.ToString() == uidTelefono.Text select t;
                telefono = dir.First();
                pos = telefonos.IndexOf(telefono);
                telefonos.Remove(telefono);
            }
            else
            {
                telefono = new UsuarioTelefono();
                telefono.UidTelefono = Guid.NewGuid();
            }
            telefono.StrTelefono = txtTelefono.Text;
            telefono.UidTipoTelefono = new Guid(ddTipoTelefono.SelectedValue);
            telefono.StrTipoTelefono = ddTipoTelefono.SelectedItem.Text;

            if (pos < 0)
                telefonos.Add(telefono);
            else
                telefonos.Insert(pos, telefono);

            dgvTelefonos.DataSource = telefonos;
            dgvTelefonos.DataBind();

            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            txtTelefono.Enabled = false;
            txtTelefono.AddCssClass("disabled");
            ddTipoTelefono.SelectedIndex = 0;
            ddTipoTelefono.AddCssClass("disabled");
            ddTipoTelefono.Enabled = false;

            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnOKTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnCancelarTelefono.Enabled = false;

            if (uidTelefono.Text.Length == 0)
            {
                btnEditarTelefono.AddCssClass("disabled");
                btnEditarTelefono.Enabled = false;
                btnEliminarTelefono.AddCssClass("disabled");
                btnEliminarTelefono.Enabled = false;
            }
            else
            {
                btnEditarTelefono.RemoveCssClass("disabled");
                btnEditarTelefono.Enabled = false;
                btnEliminarTelefono.RemoveCssClass("disabled");
                btnEliminarTelefono.Enabled = false;
            }

            btnAgregarTelefono.RemoveCssClass("disabled");
            btnAgregarTelefono.Enabled = true;
        }

        protected void btnCancelarTelefono_Click(object sender, EventArgs e)
        {
            frmGrpTelefono.RemoveCssClass("has-error");
            lblErrorTelefono.Text = string.Empty;

            txtTelefono.Enabled = false;
            txtTelefono.AddCssClass("disabled");
            ddTipoTelefono.SelectedIndex = 0;
            ddTipoTelefono.AddCssClass("disabled");
            ddTipoTelefono.Enabled = false;

            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnOKTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");
            btnCancelarTelefono.Enabled = false;

            if (uidTelefono.Text.Length == 0)
            {
                btnEditarTelefono.AddCssClass("disabled");
                btnEditarTelefono.Enabled = false;
                btnEliminarTelefono.AddCssClass("disabled");
                btnEliminarTelefono.Enabled = false;
                uidTelefono.Text = string.Empty;
                txtTelefono.Text = string.Empty;
            }
            else
            {
                btnEditarTelefono.RemoveCssClass("disabled");
                btnEditarTelefono.Enabled = true;
                btnEliminarTelefono.RemoveCssClass("disabled");
                btnEliminarTelefono.Enabled = true;

                List<UsuarioTelefono> telefonos = (List<UsuarioTelefono>)ViewState["Telefonos"];
                UsuarioTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono.ToString() == dgvTelefonos.SelectedDataKey.Value.ToString()).First();

                uidTelefono.Text = telefono.UidTelefono.ToString();
                txtTelefono.Text = telefono.StrTelefono;
                ddTipoTelefono.SelectedValue = telefono.UidTipoTelefono.ToString();
            }

            btnAgregarTelefono.RemoveCssClass("disabled");
            btnAgregarTelefono.Enabled = true;
        }

        protected void btnAceptarEliminarTelefono_Click(object sender, EventArgs e)
        {

            btnAgregarTelefono.Enabled = true;
            btnAgregarTelefono.RemoveCssClass("disabled");

            btnOKTelefono.Enabled = false;
            btnOKTelefono.AddCssClass("hidden").AddCssClass("disabled");

            btnCancelarTelefono.Enabled = false;
            btnCancelarTelefono.AddCssClass("hidden").AddCssClass("disabled");

            Guid uid = new Guid(uidTelefono.Text);

            List<UsuarioTelefono> telefonos = (List<UsuarioTelefono>)ViewState["Telefonos"];
            UsuarioTelefono telefono = telefonos.Select(x => x).Where(x => x.UidTelefono == uid).First();
            telefonos.Remove(telefono);
            TelefonoRemoved.Add(telefono);

            uidTelefono.Text = string.Empty;
            txtTelefono.Text = string.Empty;
            ddTipoTelefono.SelectedIndex = 0;

            dgvTelefonos.DataSource = telefonos;
            dgvTelefonos.DataBind();
            btnCancelarEliminarTelefono.Visible = false;
            btnAceptarEliminarTelefono.Visible = false;
            lblAceptarEliminarTelefono.Visible = false;
        }

        protected void btnCancelarEliminarTelefono_Click(object sender, EventArgs e)
        {
            btnAceptarEliminarTelefono.Visible = false;
            btnCancelarEliminarTelefono.Visible = false;
            lblAceptarEliminarTelefono.Visible = false;
        }

        #endregion

        #region imagen

        protected void imagen(object sender, EventArgs e)
        {
            if (FUImagen.HasFile)
            {
                string extencion = Path.GetExtension(FUImagen.FileName).ToLower();
                string[] arreglo = { ".jpg", ".png", ".jpeg" };
                for (int i = 0; i < arreglo.Length; i++)
                {
                    if (extencion == arreglo[i])
                    {
                        string Nombrearchivo = Path.GetFileName(FUImagen.FileName);
                        int numero = new Random().Next(999999999);
                        string ruta = "~/Vista/Imagenes/Encargados/" + txtUidUsuario.Text + '_' + numero + Nombrearchivo;


                        //guardar img
                        FUImagen.SaveAs(Server.MapPath(ruta));

                        string rutaimg = ruta + "?" + (numero - 1);

                        ViewState["rutaimg"] = ruta;

                        ImgUsuario.ImageUrl = rutaimg;

                    }
                }
            }
        }

        #endregion

        

        protected void tabEmpresas_Click(object sender, EventArgs e)
        {
            panelTelefonos.Visible = false;
            PanelDatosGeneralesUsuario.Visible = false;
            panelDirecciones.Visible = false;
            PanelEmpresa.Visible = true;
            PanelSucursales.Visible = false;
            panelAccesos.Visible = false;
            activeSucursales.RemoveCssClass("active");

            activeAccesos.Attributes["class"] = "";

            activeDatosGenerales.Attributes["class"] = "";

            activeDirecciones.Attributes["class"] = "";

            activeTelefonos.Attributes["class"] = "";

            activeEmpresas.AddCssClass("active");

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            
        }

        protected void tabSucursales_Click(object sender, EventArgs e)
        {
            panelTelefonos.Visible = false;
            PanelDatosGeneralesUsuario.Visible = false;
            panelDirecciones.Visible = false;
            PanelEmpresa.Visible = false;
            PanelSucursales.Visible = true;
            panelAccesos.Visible = false;

            activeAccesos.Attributes["class"] = "";
            activeDatosGenerales.Attributes["class"] = "";

            activeDirecciones.Attributes["class"] = "";

            activeTelefonos.Attributes["class"] = "";

            activeEmpresas.RemoveCssClass("active");

            activeSucursales.AddCssClass("active");

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);

        }
        protected void tabAccesos_Click(object sender, EventArgs e)
        {
            panelTelefonos.Visible = false;
            PanelDatosGeneralesUsuario.Visible = false;
            PanelEmpresa.Visible = false;
            panelDirecciones.Visible = false;
            panelAccesos.Visible = true;

            activeDatosGenerales.Attributes["class"] = "";

            activeDirecciones.Attributes["class"] = "";

            activeTelefonos.Attributes["class"] = "";

            activeEmpresas.RemoveCssClass("active");

            activeAccesos.AddCssClass("active");

            if (EditingModeDireccion)
                btnCancelarDireccion_Click(null, null);
            
        }

        protected void tabBackside_Click(object sender, EventArgs e)
        {
            accesoBackside.Visible = true;
            accesoBackend.Visible = false;
            accesoFrontend.Visible = false;

            activeBackside.AddCssClass("active");
            activeBackend.RemoveCssClass("active");
            activeFrontend.RemoveCssClass("active");

            tabBackside.Disable();
            tabBackend.Enable();
            tabFrontend.Enable();
        }

        protected void tabBackend_Click(object sender, EventArgs e)
        {
            accesoBackside.Visible = false;
            accesoBackend.Visible = true;
            accesoFrontend.Visible = false;

            activeBackside.RemoveCssClass("active");
            activeBackend.AddCssClass("active");
            activeFrontend.RemoveCssClass("active");

            tabBackside.Enable();
            tabBackend.Disable();
            tabFrontend.Enable();
        }

        protected void tabFrontend_Click(object sender, EventArgs e)
        {
            accesoBackside.Visible = false;
            accesoBackend.Visible = false;
            accesoFrontend.Visible = true;

            activeBackside.RemoveCssClass("active");
            activeBackend.RemoveCssClass("active");
            activeFrontend.AddCssClass("active");

            tabBackside.Enable();
            tabBackend.Enable();
            tabFrontend.Disable();
        }
    }
}