﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="NotificacionesEmpresa.aspx.cs" Inherits="CodorniX.Vista.NotificacionesEmpresa" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Busqueda de notificaciones.
				</div>
				<div class="panel-body panel-pd">
					<div class="text-right">
						<div class="btn-group pd-0">
							<asp:LinkButton runat="server" ID="btnOcultarFiltros" CssClass="btn btn-sm btn-default" OnClick="btnOcultarFiltros_Click">
								<span class="glyphicon glyphicon-eye-close"></span>
								<asp:Label Text="Ocultar" runat="server" ID="lblOcultarFiltros" />
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnLimpiarFiltros" CssClass="btn btn-sm btn-default" OnClick="btnLimpiarFiltros_Click">
								<span class="glyphicon glyphicon-trash"></span>
								Limpiar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-sm btn-default" OnClick="btnBuscar_Click">
								<span class="glyphicon glyphicon-search"></span>
								Buscar
							</asp:LinkButton>
						</div>
					</div>
					<asp:Panel runat="server" ID="pnlAlertBusqueda">
						<div class="alert alert-danger">
							<asp:Label Text="Error" runat="server" ID="lblErrorBusqueda" />
							<asp:LinkButton ID="btnCloseAlerBusqueda" CssClass="close" runat="server" OnClick="CloseAlertBusqueda_Click"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlFiltrosBusqueda">
						<div class="col-md-4 pd-left-right-5">
							<h6>Fecha de Inicio</h6>
							<div class="input-group date start-date">
								<asp:TextBox runat="server" ID="txtFiltroFechaInicio" CssClass="form-control" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Fecha Fin</h6>
							<div class="input-group date start-date">
								<asp:TextBox runat="server" ID="txtFiltroFechaFin" CssClass="form-control" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Estado</h6>
							<asp:DropDownList runat="server" ID="ddlFiltroEstadoNotificacion" CssClass="form-control">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Sucursal</h6>
							<asp:DropDownList runat="server" ID="ddlFiltroSucursal" CssClass="form-control" OnSelectedIndexChanged="ddlFiltroSucursal_SelectedIndexChanged" AutoPostBack="true">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Departamento</h6>
							<asp:DropDownList runat="server" ID="ddlFiltroDepartamento" CssClass="form-control" OnSelectedIndexChanged="ddlFiltroDepartamento_SelectedIndexChanged" AutoPostBack="true">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Area</h6>
							<asp:DropDownList runat="server" ID="ddlFiltroArea" CssClass="form-control">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlListaNotificaciones">
						<div class="col-md-12 pd-left-right-5">
							<div class="table-responsive">
								<asp:GridView runat="server" DataKeyNames="UidNotificacion" ID="gvNotificaciones" AllowPaging="true" PageSize="12" OnRowDataBound="gvNotificaciones_RowDataBound" OnSelectedIndexChanged="gvNotificaciones_SelectedIndexChanged" OnPageIndexChanging="gvNotificaciones_PageIndexChanging" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" SelectedRowStyle-BackColor="#caf4c0">
									<Columns>
										<asp:BoundField HeaderText="Sucursal" DataField="Sucursal" />
										<asp:BoundField HeaderText="FC" DataField="FolioCumplimiento" DataFormatString="{0:0000}" />
										<asp:BoundField HeaderText="FT" DataField="FolioTarea" DataFormatString="{0:0000}" />
										<asp:BoundField HeaderText="Tarea" DataField="Tarea" />
										<asp:BoundField HeaderText="Departamento" DataField="Departamento" />
										<asp:BoundField HeaderText="Fecha" DataField="DtFechaCumplimiento" DataFormatString="{0:dd/MM/yyyy}" />
										<asp:BoundField HeaderText="Estado" DataField="EstadoNotificacion" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
										<asp:TemplateField HeaderText="Estado">
											<ItemTemplate>
												<asp:Label runat="server" ID="lblIcon" CssClass="glyphicon glyphicon-ok"></asp:Label>
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No se encontraron registros con los parametros de busqueda ingresados.
										</div>
									</EmptyDataTemplate>
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								</asp:GridView>
							</div>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Datos Generales
				</div>
				<div class="panel-body panel-pd">
					<div class="btn-group pd-0">
						<asp:LinkButton Text="text" runat="server" CssClass="btn btn-sm btn-default" ID="btnCambiarEstado" OnClick="btnCambiarEstado_Click">
							<span class="glyphicon glyphicon-refresh"></span>
							<asp:Label Text="Revisado" runat="server" ID="lblCambiarEstado" />
						</asp:LinkButton>
						<asp:HiddenField runat="server" ID="hfUidMensajeNotificacion" />
					</div>
					<asp:Panel runat="server" ID="pnlAlertGeneral">
						<div class="alert alert-danger">
							<asp:Label Text="Error" runat="server" ID="lblErrorGeneral" />
							<asp:LinkButton ID="btnCloseAlertGeneral" CssClass="close" runat="server" OnClick="CloseAlertGeneral_Click"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</div>
					</asp:Panel>
					<br />
					<div class="col-md-12 pd-left-right-5 font-bold">
						Detalles de Cumplimiento de Tarea						
					</div>
					<div class="col-md-12 pd-left-right-5">
						<asp:Table runat="server">
							<asp:TableRow>
								<asp:TableCell CssClass="font-bold text-right text-color-555">
									Sucursal
								</asp:TableCell>
								<asp:TableCell CssClass="pd-5">
									<asp:Label Text="(Ninguno)" runat="server" ID="lblSucursal" />
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="font-bold text-right text-color-555">
									Departamento
								</asp:TableCell>
								<asp:TableCell CssClass="pd-5">
									<asp:Label Text="(Ninguno)" runat="server" ID="lblDepartamento" />
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="font-bold text-right text-color-555">
									Area
								</asp:TableCell>
								<asp:TableCell CssClass="pd-5">
									<asp:Label Text="(Ninguno)" runat="server" ID="lblArea" />
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="font-bold text-right text-color-555">
									Tarea
								</asp:TableCell>
								<asp:TableCell CssClass="pd-5">
									<asp:Label Text="(Ninguno)" runat="server" ID="lblTarea" />
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="font-bold text-right text-color-555">
									Tipo de Tarea
								</asp:TableCell>
								<asp:TableCell CssClass="pd-5">
									<asp:Label Text="(Ninguno)" runat="server" ID="lblTipoTarea" />
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="font-bold text-right text-color-555">
									Valor Ingresado
								</asp:TableCell>
								<asp:TableCell CssClass="pd-5">
									<asp:Label Text="(Ninguno)" runat="server" ID="lblValorIngresado" />
								</asp:TableCell>
							</asp:TableRow>
							<asp:TableRow>
								<asp:TableCell CssClass="font-bold text-right pd-5 text-color-555">
									Observaciones
								</asp:TableCell>
								<asp:TableCell CssClass="pd-5">
									<asp:Label Text="(Ninguno)" runat="server" ID="lblObservaciones" />
								</asp:TableCell>
							</asp:TableRow>
						</asp:Table>
					</div>
					<br />
					<div class=" pd-left-right-5 font-bold col-md-12">
						Detalles Notificación
					</div>
					<asp:Panel runat="server" ID="pnlNotificacionVerdaderoFalso">
						<div class="col-md-12">
							El valor 
							<b>
								<asp:Label Text="Verdadero" runat="server" ID="lblNotificacionVF" />
							</b>
							emite notificación.
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlNotificacionValor">
						<div class="col-md-12">
							El Valor ingresado es 
							<b>
								<asp:Label Text="Menor" runat="server" ID="lblNotificacionValor" />
							</b>

						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlNotificacionOpcionMultiple">
						<div class="col-md-12">
							El valor seleccionado es una opcion que emite notificación
							<asp:ListBox runat="server" Enabled="false" CssClass="form-control" ID="lbNotificacionOpcionMultiple">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
								<asp:ListItem Text="text3" />
							</asp:ListBox>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<script>
		//<![CDATA[
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date.start-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
		}

		function setStartDateLimit(start, end) {
			$(".input-group.date.start-date").datepicker('remove');
			$(".input-group.date.start-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: start,
				endDate: end
			})
			startDateReady = true;
		}


		function pageLoad() {
			enableDatapicker();
		}
        //]]>
	</script>
</asp:Content>
