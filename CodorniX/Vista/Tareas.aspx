﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Tareas.aspx.cs" Inherits="CodorniX.Vista.Tareas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<style>
		.text-overflow {
			display: block;
			overflow: hidden;
			white-space: nowrap;
			text-overflow: ellipsis;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tareas
				</div>
				<div class="panel-body panel-pd">
					<%--Botones de busqueda--%>
					<div class="col-md-12 pd-0 ">
						<table style="width: 100%;">
							<tr>
								<td>
									<div class="btn-group">
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnOrdenamiento" OnClick="btnOrdenamiento_Click">
											<span class="glyphicon glyphicon-list"></span>
											Ordenamiento
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnEditarOrdenamiento" OnClick="btnEditarOrdenamiento_Click">
											<span class="glyphicon glyphicon-pencil"></span>
											Editar
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-success" ID="btnOkOrdenamiento" OnClick="btnOkOrdenamiento_Click">
											<span class="glyphicon glyphicon-ok"></span>
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" ID="btnCancelOrdenamiento" OnClick="btnCancelOrdenamiento_Click">
											<span class="glyphicon glyphicon-remove"></span>
										</asp:LinkButton>
									</div>
								</td>
								<td class="text-right">
									<div class="btn-group">
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnOcultarMostrarFiltros" OnClick="btnOcultarMostrarFiltros_Click">
											<asp:Label Text="Ocultar" runat="server" ID="lblOcultarMostrarFiltros" />
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnLimpiarFiltros" OnClick="btnLimpiarFiltros_Click">
											<span class="glyphicon glyphicon-trash"></span>
											Limpiar
										</asp:LinkButton>
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscar" OnClick="btnBuscar_Click">
											<span class="glyphicon glyphicon-search"></span>
											Buscar
										</asp:LinkButton>
									</div>
								</td>
							</tr>
						</table>
					</div>
					<div class="col-md-12 pd-0">
						<asp:Panel runat="server" ID="pnlMessageSearch" CssClass="alert alert-info">
							<asp:Label runat="server" ID="lblMessageSearch" Text="Error" />
							<asp:LinkButton runat="server" CssClass="close" OnClick="HideMessageBusqueda">
								<span aria-hidden="true">&times;</span>
							</asp:LinkButton>
						</asp:Panel>
					</div>
					<%--Filtros de busqueda--%>
					<asp:Panel runat="server" ID="pnlFiltrosBusqueda" CssClass="col-md-12 pd-0">
						<div class="col-md-3 pd-left-right-5 mg-top-5">
							<small>Folio</small>
							<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFolio" />
						</div>
						<div class="col-md-9 pd-left-right-5 mg-top-5">
							<small>Nombre</small>
							<asp:TextBox CssClass="form-control" ID="txtFiltroNombre" runat="server" />
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Folio de Antecesor</small>
							<asp:TextBox ID="txtFiltroFolioAntecesor" runat="server" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Fecha de Inicio</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFiltroFechaInicio" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Fecha Fin</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFiltroFechaFin" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Departamento</small>
							<asp:ListBox ID="lbFiltroDepartamento" runat="server" SelectionMode="Multiple" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Area</small>
							<asp:ListBox ID="lbFiltroArea" runat="server" SelectionMode="Multiple" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Unidad Medida</small>
							<asp:ListBox ID="lbFiltroUnidad" runat="server" SelectionMode="Multiple" CssClass="form-control" />
						</div>
					</asp:Panel>
					<%--Lista de las tareas--%>
					<asp:Panel runat="server" ID="pnlListaTareas" CssClass="col-md-12 pd-0">
						<asp:HiddenField ID="hfGvTareasSortDirection" runat="server" Value="ASC" />
						<asp:GridView runat="server"
							ID="gvTareas"
							DataKeyNames="UidTarea"
							OnRowDataBound="gvTareas_RowDataBound"
							OnPreRender="gvTareas_PreRender"
							OnSelectedIndexChanging="gvTareas_SelectedIndexChanging"
							OnSelectedIndexChanged="gvTareas_SelectedIndexChanged"
							OnPageIndexChanging="gvTareas_PageIndexChanging"
							OnSorting="gvTareas_Sorting"
							AutoGenerateColumns="false"
							AllowPaging="true" AllowSorting="true"
							PageSize="10"
							CssClass="table table-bordered table-condensed table-hover input-sm"
							SelectedRowStyle-BackColor="#dff0d8">
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
								<asp:BoundField DataField="IntFolio" HeaderText="Folio" DataFormatString="{0:D4}" SortExpression="Folio" ItemStyle-VerticalAlign="Middle" />
								<asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
									<ItemTemplate>
										<asp:Label Text='<%# Eval("StrNombre") %>' runat="server" ID="lblTaskName" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodicidad" SortExpression="Periodicidad" />
								<asp:TemplateField HeaderText="Departamento" SortExpression="Departamento">
									<ItemTemplate>
										<asp:Label Text='<%# Eval("StrDepartamento") %>' runat="server" ID="lblTaskDepto" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="Area" SortExpression="Area">
									<ItemTemplate>
										<asp:Label Text='<%# Eval("StrArea") %>' runat="server" ID="lblTaskArea" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Inicio" SortExpression="FechaInicio" />
								<asp:BoundField DataField="BlAutorizado" HeaderText="Autorizado" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
							</Columns>
							<EmptyDataTemplate>
								No se encontraron registros
							</EmptyDataTemplate>
							<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="5" />
							<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
						</asp:GridView>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlOrdenamientoTareas">
						<asp:HiddenField runat="server" ID="hfUidTareaOrdenamiento" />
						<asp:HiddenField runat="server" ID="hfSortDirectionOrdenamiento" Value="ASC" />
						<asp:GridView runat="server"
							ID="gvTareasOrdenamiento"
							DataKeyNames="UidTarea"
							OnRowDataBound="gvTareasOrdenamiento_RowDataBound"
							OnRowCommand="gvTareasOrdenamiento_RowCommand"
							AllowSorting="true"
							OnSorting="gvTareasOrdenamiento_Sorting"
							AllowPaging="true" PageSize="10"
							OnPageIndexChanging="gvTareasOrdenamiento_PageIndexChanging"
							AutoGenerateColumns="false"
							CssClass="table table-bordered table-condensed table-hover input-sm"
							SelectedRowStyle-BackColor="#dff0d8">
							<Columns>
								<asp:BoundField DataField="IntFolio" HeaderText="Folio" DataFormatString="{0:D4}" ItemStyle-VerticalAlign="Middle" />
								<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
								<asp:TemplateField HeaderText="Departamento" SortExpression="Departamento" ControlStyle-Width="50">
									<ItemTemplate>
										<asp:Label Text='<%# Eval("StrDepartamento") %>' ToolTip='<%# Eval("StrDepartamento") %>' runat="server" CssClass="text-overflow" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:TemplateField HeaderText="Area" SortExpression="Area" ControlStyle-Width="65">
									<ItemTemplate>
										<asp:Label Text='<%# Eval("StrArea") %>' ToolTip='<%# Eval("StrArea") %>' runat="server" CssClass="text-overflow" />
									</ItemTemplate>
								</asp:TemplateField>
								<asp:BoundField DataField="IntOrden" HeaderText="Posicion" />
								<asp:TemplateField HeaderText="Orden">
									<ItemTemplate>
										<table>
											<tr>
												<td style="padding-right: 4px;">
													<asp:LinkButton runat="server" CssClass="btn btn-xs btn-default" ToolTip="Pagina izquierda" CommandArgument='<%# Eval("UidTarea") %>' CommandName="Left">
														<span class="glyphicon glyphicon-chevron-left"></span>
													</asp:LinkButton>
												</td>
												<td>
													<asp:LinkButton runat="server" CssClass="btn btn-xs btn-default" ToolTip="Arriba" CommandArgument='<%# Eval("UidTarea") %>' CommandName="Up">
													<span class="glyphicon glyphicon-arrow-up"></span>
													</asp:LinkButton>
												</td>
												<td>
													<asp:LinkButton runat="server" CssClass="btn btn-xs btn-default" ToolTip="Abajo" CommandArgument='<%# Eval("UidTarea") %>' CommandName="Down">
													<span class="glyphicon glyphicon-arrow-down"></span>
													</asp:LinkButton>
												</td>
												<td style="padding-left: 4px;">
													<asp:LinkButton runat="server" CssClass="btn btn-xs btn-default" ToolTip="Pagina derecha" CommandArgument='<%# Eval("UidTarea") %>' CommandName="Right">
														<span class="glyphicon glyphicon-chevron-right"></span>
													</asp:LinkButton>
												</td>
											</tr>
										</table>
									</ItemTemplate>
								</asp:TemplateField>
							</Columns>
							<EmptyDataTemplate>
								No se encontraron registros
							</EmptyDataTemplate>
							<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="5" />
							<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
						</asp:GridView>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Datos generales
				</div>
				<div class="panel-body panel-pd">
					<%--Botones de gestion general--%>
					<div class="col-md-12 pd-0">
						<table style="width: 100%;">
							<tr>
								<td class="btn-group">
									<asp:LinkButton runat="server" ID="btnNuevo" CssClass="btn btn-sm btn-default" OnClick="btnNuevo_Click">
										<span class="glyphicon glyphicon-plus"></span>
										Nuevo
									</asp:LinkButton>
									<asp:LinkButton runat="server" ID="btnEditar" CssClass="btn btn-sm btn-default" OnClick="btnEditar_Click">
										<span class="glyphicon glyphicon-pencil"></span>
										Editar
									</asp:LinkButton>
									<asp:LinkButton runat="server" ID="btnOk" CssClass="btn btn-sm btn-success" OnClick="btnOk_Click">
										<span class="glyphicon glyphicon-ok"></span>
									</asp:LinkButton>
									<asp:LinkButton runat="server" ID="btnCancelar" CssClass="btn btn-sm btn-danger" OnClick="btnCancelar_Click">
										<span class="glyphicon glyphicon-remove"></span>
									</asp:LinkButton>
								</td>
								<td class="text-right">
									<asp:LinkButton Text="Habilitar" runat="server" ID="btnHabilitarTarea" OnClick="btnHabilitarTarea_Click" ToolTip="Confirmar tarea realizada por el supervisor" />
								</td>
							</tr>
						</table>
						<asp:HiddenField runat="server" ID="hfUidTarea" />
					</div>
					<%--Aqui se gestionan las notificaciones del modulo en esta seccion--%>
					<div class="col-md-12 pd-0">
						<asp:Panel runat="server" ID="pnlMessageGeneral" CssClass="alert alert-info">
							<asp:Label runat="server" ID="lblMessageGeneral" Text="Error" />
							<asp:LinkButton runat="server" CssClass="close" OnClick="HideMessageGeneral">
								<span aria-hidden="true">&times;</span>
							</asp:LinkButton>
						</asp:Panel>
					</div>
					<%--Navegacion de pestañas--%>
					<div class="col-md-12 pd-0">
						<ul class="nav nav-tabs">
							<li runat="server" id="liDatosGenerales">
								<asp:LinkButton Text="Datos" runat="server" OnClick="btnTabDatosGenerales_Click" />
							</li>
							<li runat="server" id="liOpciones">
								<asp:LinkButton Text="Opciones" runat="server" OnClick="btnTabOpciones_Click" />
							</li>
							<li runat="server" id="liPeriodos">
								<asp:LinkButton Text="Periodos" runat="server" OnClick="btnTabPeriodos_Click" />
							</li>
							<li runat="server" id="liAsignacion">
								<asp:LinkButton Text="Asignación" runat="server" OnClick="btnTabAsignacion_Click" />
							</li>
							<li runat="server" id="liAntecesor">
								<asp:LinkButton Text="Antecesor" runat="server" OnClick="btnTabAntecesor_Click" />
							</li>
							<li runat="server" id="liNotificacion">
								<asp:LinkButton Text="Notificación" runat="server" OnClick="btnTabNotificacion_Click" />
							</li>
						</ul>
					</div>
					<%--Datos generales de la tarea--%>
					<asp:Panel runat="server" CssClass="col-md-12 pd-0" ID="pnlTabDatosGenerales">
						<div class="col-md-8 pd-left-right-5 mg-top-5">
							<small>Nombre</small>
							<asp:TextBox runat="server" CssClass="form-control" ID="txtNombreTarea" />
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Estatus</small>
							<asp:DropDownList runat="server" CssClass="form-control" ID="ddlEstatusTarea">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Descripcion</small>
							<asp:TextBox runat="server" CssClass="form-control" ID="txtDescripcionTarea" TextMode="MultiLine" Rows="9" />
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Fecha de inicio</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFechaInicio" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Fecha fin</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFechaFin" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Tipo de tarea</small>
							<asp:DropDownList runat="server" CssClass="form-control" ID="ddlTipoTarea">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5 mg-top-5">
							<small>Tipo de medicion</small>
							<asp:DropDownList runat="server" CssClass="form-control" ID="ddlTipoMedicion" OnSelectedIndexChanged="ddlTipoMedicion_SelectedIndexChanged" AutoPostBack="true">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<asp:Panel runat="server" ID="pnlUnidadMedida">
							<div class="col-md-4 pd-left-right-5 mg-top-5">
								<small>Unidad de medida</small>
								<asp:DropDownList runat="server" CssClass="form-control" ID="ddlUnidadMedida">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</div>
						</asp:Panel>
						<div class="col-md-8 pd-0" hidden>
							<div class="col-md-6 pd-left-right-5">
								<label class="checkbox-inline">
									<asp:CheckBox AutoPostBack="true" ID="CbFoto" runat="server" Checked="false" />
									Foto requerida
								</label>
							</div>
							<div class="col-md-6 pd-left-right-5">
								<label class="checkbox-inline">
									<asp:CheckBox AutoPostBack="true" ID="cbHabilitarHoraTarea" runat="server" />
									Habilitar Hora
								</label>
							</div>
						</div>
						<asp:Panel runat="server" ID="pnlHabilitarHoraTarea" Visible="false">
							<div class="col-md-4 pd-left-right-5">
								<h6>Hora</h6>
								<div class=" input-group clockpicker" data-placement="top" data-align="left" data-autoclose="true">
									<asp:TextBox runat="server" ID="txtHoraTarea" CssClass="form-control" />
									<span id="clock" runat="server" class="input-group-addon input-sm">
										<i class="glyphicon glyphicon-time"></i>
									</span>
								</div>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Tolerancia</h6>
								<div class="input-group">
									<asp:TextBox runat="server" ID="txtToleranciaTarea" CssClass="form-control" />
									<span id="min" runat="server" class="input-group-addon input-sm"><i>min</i> </span>
								</div>
							</div>
						</asp:Panel>
					</asp:Panel>
					<%--Seccion de las opciones de la tarea--%>
					<asp:Panel runat="server" CssClass="col-md-12 pd-0" ID="pnlTabOpciones">
						<div class="col-md-12 pd-left-right-5">
							<div class="btn-group">
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnNuevaOpcion" OnClick="btnNuevaOpcion_Click">
									<span class="glyphicon glyphicon-plus"></span>
									Nuevo
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnEditarOpcion" OnClick="btnEditarOpcion_Click">
									<span class="glyphicon glyphicon-pencil"></span>
									Editar
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnEliminarOpcion" OnClick="btnEliminarOpcion_Click">
									<span class="glyphicon glyphicon-trash"></span>
									Eliminar
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-success" ID="btnOkOpcion" OnClick="btnOkOpcion_Click">
									<span class="glyphicon glyphicon-ok"></span>
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" ID="btnCancelarOpcion" OnClick="btnCancelarOpcion_Click">
									<span class="glyphicon glyphicon-remove"></span>
								</asp:LinkButton>
							</div>
							<asp:HiddenField runat="server" ID="hfUidOpcionTarea" />
						</div>
						<div class="col-md-12 pd-0">
							<asp:Panel runat="server" ID="pnlMessageOptions" CssClass="alert alert-info">
								<asp:Label runat="server" ID="lblMessageOptions" Text="Error" />
								<asp:LinkButton runat="server" CssClass="close" OnClick="HideMessageOpcionTarea">
									<span aria-hidden="true">&times;</span>
								</asp:LinkButton>
							</asp:Panel>
						</div>
						<div class="col-md-8 pd-left-right-5">
							<small>Opción</small>
							<asp:TextBox runat="server" ID="txtNombreOpcionTarea" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-2 pd-left-right-5">
							<small>Orden</small>
							<asp:TextBox runat="server" ID="txtOrdenOpcionTarea" CssClass="form-control  input-sm" />
						</div>
						<div class="col-md-2 pd-left-right-5">
							<small>Activo</small>
							<br />
							<asp:CheckBox runat="server" ID="cbActivoOpcionTarea" />
						</div>
						<div class="col-md-12 pd-left-right-5">
							<asp:GridView runat="server" ID="gvOpcionesTarea" OnRowDataBound="gvOpcionesTarea_RowDataBound" OnSelectedIndexChanging="gvOpcionesTarea_SelectedIndexChanging" OnSelectedIndexChanged="gvOpcionesTarea_SelectedIndexChanged" DataKeyNames="UidOpciones" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm" SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField HeaderText="Opción" DataField="StrOpciones" />
									<asp:BoundField HeaderText="Orden" DataField="IntOrden" DataFormatString="{0:00}" />
									<asp:TemplateField HeaderText="Activo">
										<ItemTemplate>
											<asp:Label runat="server" ID="lblActivo" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField HeaderText="Activo" DataField="BlVisible" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
								</Columns>
								<EmptyDataTemplate>
									<div class="alert alert-info">
										No hay opciones
									</div>
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:Panel>
					<%-- Periodos de cumplimiento de la tarea --%>
					<asp:Panel runat="server" CssClass="col-md-12 pd-0" ID="pnlTabPeriodos">
						<asp:Panel runat="server" ID="pnlMessagePeriodicity">
							<asp:Label Text="Ocurrion un error" runat="server" ID="lblMessagePeriodicity" />
						</asp:Panel>
						<asp:HiddenField runat="server" ID="hfUidPeriodicidad" />
						<asp:HiddenField runat="server" ID="hfTipoPeriodicidad" />
						<div class="col-lg-3 pd-left-right-5">
							<asp:RadioButton ID="rbSinPeriodicidad" OnCheckedChanged="rbSinPeriodicidad_CheckedChanged" AutoPostBack="true" GroupName="PeriodicidadTarea" Text="Sin Periodicidad" runat="server" />
							<br />
							<asp:RadioButton ID="rbPeriodicidadDiaria" OnCheckedChanged="rbPeriodicidadDiaria_CheckedChanged" AutoPostBack="true" GroupName="PeriodicidadTarea" Text="Diaria" runat="server" />
							<br />
							<asp:RadioButton ID="rbPeriodicidadSemanal" OnCheckedChanged="rbPeriodicidadSemanal_CheckedChanged" AutoPostBack="true" GroupName="PeriodicidadTarea" Text="Semanal" runat="server" />
							<br />
							<asp:RadioButton ID="rbPeriodicidadMensual" OnCheckedChanged="rbPeriodicidadMensual_CheckedChanged" AutoPostBack="true" GroupName="PeriodicidadTarea" Text="Mensual" runat="server" />
							<br />
							<asp:RadioButton ID="rbPeriodicidadAnual" OnCheckedChanged="rbPeriodicidadAnual_CheckedChanged" AutoPostBack="true" GroupName="PeriodicidadTarea" Text="Anual" runat="server" />
						</div>
						<div class="col-lg-9 pd-left-right-5">
							<asp:Panel runat="server" ID="subpnlPeriodicidadDiaria">
								<h5>
									<asp:CheckBox runat="server" ID="chkPDOpcion1" AutoPostBack="true" OnCheckedChanged="chkPDOpcion1_CheckedChanged" />
									Repetir cada
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPDNumeroDias"></asp:TextBox>
									dias
								</h5>
								<asp:CheckBox Text="Todos los dias de la semana" runat="server" ID="chkPDOpcion2" AutoPostBack="true" OnCheckedChanged="chkPDOpcion2_CheckedChanged" />
							</asp:Panel>
							<asp:Panel runat="server" ID="subpnlPeriodicidadSemanal">
								<div>
									<h5>Repetir cada
                                        <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPSNumeroSemanas"></asp:TextBox>
										semanas el:
									</h5>
									<div class="col-md-12" style="padding-left: 25px;">
										<asp:CheckBoxList runat="server" TextAlign="Right" CssClass="checkbox" ID="cblPSListaDias">
											<asp:ListItem Text="Lunes" />
											<asp:ListItem Text="Martes" />
											<asp:ListItem Text="Miercoles" />
											<asp:ListItem Text="Jueves" />
											<asp:ListItem Text="Viernes" />
											<asp:ListItem Text="Sabado" />
											<asp:ListItem Text="Domingo" />
										</asp:CheckBoxList>
									</div>
								</div>
							</asp:Panel>
							<asp:Panel runat="server" ID="subpnlPeriodicidadMensual">
								<h5>
									<asp:CheckBox runat="server" ID="chkPMRepetirCadaNDiasCadaNMes" AutoPostBack="true" OnCheckedChanged="chkPMRepetirCadaNDiasCadaNMes_CheckedChanged" />
									El dia
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPMDiaDelMes"></asp:TextBox>
									de cada
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPMNumeroDeMesesOpc1"></asp:TextBox>
									meses.
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnAgregarFechaMensual" OnClick="btnAgregarFechaPeriodicidadMensual">
										<span class="glyphicon glyphicon-plus"></span>
									</asp:LinkButton>
								</h5>
								<h5>
									<asp:CheckBox runat="server" ID="chkPMRepetirCadaNMESConParametros" AutoPostBack="true" OnCheckedChanged="chkPMRepetirCadaNMESConParametros_CheckedChanged" />
									El
                                    <asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPMListaNumerales">
										<asp:ListItem Text="Primer" Value="1" />
										<asp:ListItem Text="Segundo" Value="2" />
										<asp:ListItem Text="Tercer" Value="3" />
										<asp:ListItem Text="Cuarto" Value="4" />
										<asp:ListItem Text="Ultimo" Value="0" />
									</asp:DropDownList>
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPMDiaSemanaOLapsoDias">
										<asp:ListItem Text="lunes" Value="Lunes" />
										<asp:ListItem Text="martes" Value="Martes" />
										<asp:ListItem Text="miércoles" Value="Miercoles" />
										<asp:ListItem Text="jueves" Value="Jueves" />
										<asp:ListItem Text="viernes" Value="Viernes" />
										<asp:ListItem Text="sábado" Value="Sabado" />
										<asp:ListItem Text="domingo" Value="Domingo" />
									</asp:DropDownList>
									de cada
                                    <asp:TextBox runat="server" CssClass="txtContadorPeriodicidad" Text="1" ID="txtPMNumeroDeMesesOpc2"></asp:TextBox>
									meses
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnAgregarFechaMensualOpc2" OnClick="btnAgregarFechaMensualOpc2_Click">
										<span class="glyphicon glyphicon-plus"></span>
									</asp:LinkButton>
								</h5>
								<p>
									<asp:CheckBox runat="server" ID="chkPMTipoC" AutoPostBack="true" OnCheckedChanged="chkPMTipoC_CheckedChanged" />
									Los
									<asp:TextBox runat="server" Text="2" CssClass="txtContadorPeriodicidad" ID="txtPMDiasOpc3"></asp:TextBox>
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPMNumeralesOpc3">
										<asp:ListItem Text="Primeros" Value="1" />
										<asp:ListItem Text="Primer y ultimo" Value="2" />
										<asp:ListItem Text="Ultimos" Value="3" />
									</asp:DropDownList>
									dias
									de cada
									<asp:TextBox runat="server" CssClass="txtContadorPeriodicidad" Text="1" ID="txtPMMesesOpc3"></asp:TextBox>
									meses.
								</p>
							</asp:Panel>
							<asp:Panel runat="server" ID="subpnlPeriodicidadAnual">
								<p>
									Repetir cada 
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPANumeroAnios"></asp:TextBox>
									años
								</p>
								<p>
									<asp:CheckBox runat="server" ID="chkPAOpcion1" AutoPostBack="true" OnCheckedChanged="chkPAOpcion1_CheckedChanged" />
									El:                                    
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaMesesOpc1">
										<asp:ListItem Text="Enero" />
										<asp:ListItem Text="Febrero" />
										<asp:ListItem Text="Marzo" />
										<asp:ListItem Text="Abril" />
										<asp:ListItem Text="Mayo" />
										<asp:ListItem Text="Junio" />
										<asp:ListItem Text="Julio" />
										<asp:ListItem Text="Agosto" />
										<asp:ListItem Text="Septiembre" />
										<asp:ListItem Text="Octubre" />
										<asp:ListItem Text="Noviembre" />
										<asp:ListItem Text="Diciembre" />
									</asp:DropDownList>
									<asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPADiaDelMesOpc1"></asp:TextBox>
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnAgregarFechaAnual" OnClick="btnAgregarFechaPeriodicidadAnual">
										<span class="glyphicon glyphicon-plus"></span>
									</asp:LinkButton>
								</p>
								<p>
									<asp:CheckBox runat="server" ID="chkPAOpcion2" AutoPostBack="true" OnCheckedChanged="chkPAOpcion2_CheckedChanged" />
									El:
                                    <asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaNumerales">
										<asp:ListItem Text="Primer" Value="1" />
										<asp:ListItem Text="Segundo" Value="2" />
										<asp:ListItem Text="Tercer" Value="3" />
										<asp:ListItem Text="Cuarto" Value="4" />
										<asp:ListItem Text="Ultimo" Value="0" />
									</asp:DropDownList>
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaPeriodosyDias">
										<asp:ListItem Text="lunes" />
										<asp:ListItem Text="martes" />
										<asp:ListItem Text="miércoles" />
										<asp:ListItem Text="jueves" />
										<asp:ListItem Text="viernes" />
										<asp:ListItem Text="sábado" />
										<asp:ListItem Text="domingo" />
									</asp:DropDownList>
									de
                                    <asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaMesesOpc2">
										<asp:ListItem Text="Enero" />
										<asp:ListItem Text="Febrero" />
										<asp:ListItem Text="Marzo" />
										<asp:ListItem Text="Abril" />
										<asp:ListItem Text="Mayo" />
										<asp:ListItem Text="Junio" />
										<asp:ListItem Text="Julio" />
										<asp:ListItem Text="Agosto" />
										<asp:ListItem Text="Septiembre" />
										<asp:ListItem Text="Octubre" />
										<asp:ListItem Text="Noviembre" />
										<asp:ListItem Text="Diciembre" />
									</asp:DropDownList>
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnAgregarFechaAnualTipo2" OnClick="btnAgregarFechaAnualTipo2_Click">
										<span class="glyphicon glyphicon-plus"></span>
									</asp:LinkButton>
								</p>
							</asp:Panel>

							<asp:GridView
								runat="server"
								ID="gvFechasPeriodicidad"
								OnRowDataBound="gvFechasPeriodicidad_RowDataBound"
								DataKeyNames="Identificador"
								OnRowCommand="gvFechasPeriodicidad_RowCommand"
								OnRowDeleting="gvFechasPeriodicidad_RowDeleting"
								CssClass="table table-bordered table-condensed input-sm"
								AutoGenerateColumns="false"
								SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:BoundField HeaderText="Fecha" DataField="Text" />
									<asp:BoundField HeaderText="Guardado" DataField="IsSaved"/>
									<asp:BoundField HeaderText="Eliminar" DataField="ToDelete"/>
									<asp:TemplateField HeaderText="Opciones">
										<ItemTemplate>
											<asp:LinkButton runat="server"
												CssClass="btn btn-sm btn-default"
												CommandName="Delete"
												CommandArgument='<%#Eval("Identificador")%>'>
												<span class="glyphicon glyphicon-minus"></span>
											</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" CssClass="col-md-12 pd-0" ID="pnlTabAsignacion">
						<div class="col-md-12 pd-left-right-5">
							<h6>Asignar a:</h6>
							<div class="col-sm-12 col-md-12 mg-bottom-10">
								<div class="checkbox-inline">
									<asp:RadioButton runat="server" Text="Departamento" ID="rbAsignarADepartamento" AutoPostBack="true" GroupName="AsignacionTarea" OnCheckedChanged="rbAsignarADepartamento_CheckedChanged" />
								</div>
								<div class="checkbox-inline">
									<asp:RadioButton runat="server" Text="Area" ID="rbAsignarAArea" AutoPostBack="true" GroupName="AsignacionTarea" OnCheckedChanged="rbAsignarAArea_CheckedChanged" />
								</div>
							</div>
						</div>
						<div class="input-group">
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtNombreDepartamentoAsignacion" />
							<span class="input-group-btn pd-0">
								<asp:LinkButton Text="Buscar Departamento" runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscarDepartamentoAsignacion" OnClick="btnBuscarDepartamentoAsignacion_Click" />
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" ID="btnCancelarBuscarDepartamentoAsignacion" OnClick="btnCancelarBuscarDepartamentoAsignacion_Click"> 
								<span class="glyphicon glyphicon-remove"></span>
								</asp:LinkButton>
							</span>
						</div>
						<asp:Panel runat="server" ID="pnlAsignacionDepartamentos" CssClass="col-md-12 pd-0">
							<div class="col-md-6 pd-left-right-5">
								<span class="font-bold">Asignados</span>
								<asp:GridView runat="server" ID="gvDepartamentosAsignados" OnRowDataBound="gvDepartamentosAsignados_RowDataBound" OnSelectedIndexChanging="gvDepartamentosAsignados_SelectedIndexChanging" OnSelectedIndexChanged="gvDepartamentosAsignados_SelectedIndexChanged" DataKeyNames="UidDepartamento" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="ToDelete" HeaderText="ToDelete" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No se encontraron departamentos asignados
										</div>
									</EmptyDataTemplate>
								</asp:GridView>
							</div>
							<div class="col-md-6 pd-left-right-5">
								<span class="font-bold">Disponibles</span>
								<asp:GridView runat="server" ID="gvDepartamentosAsignacionBusqueda" OnRowDataBound="gvDepartamentosAsignacionBusqueda_RowDataBound" OnSelectedIndexChanged="gvDepartamentosAsignacionBusqueda_SelectedIndexChanged" DataKeyNames="UidDepartamento" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No se encontraron departamentos
										</div>
									</EmptyDataTemplate>
								</asp:GridView>
							</div>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlAsignacionAreas" CssClass="col-md-12 pd-0">
							<div class="col-md-6 pd-left-right-5">
								<span class="font-bold">Asignados</span>
								<asp:GridView runat="server" ID="gvAreasAsignadas" OnRowDataBound="gvAreasAsignadas_RowDataBound" OnSelectedIndexChanging="gvAreasAsignadas_SelectedIndexChanging" OnSelectedIndexChanged="gvAreasAsignadas_SelectedIndexChanged" DataKeyNames="UidArea" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="ToDelete" HeaderText="ToDelete" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No se encontraron Areas asignadas
										</div>
									</EmptyDataTemplate>
								</asp:GridView>
							</div>
							<div class="col-md-6 pd-left-right-5">
								<span class="font-bold">Disponibles</span>
								<small>
									<asp:Label Text="" runat="server" ID="lblMessageAsignacionArea" />
								</small>
								<asp:GridView runat="server" ID="gvDepartamentosAsignacionArea" OnRowDataBound="gvDepartamentosAsignacionArea_RowDataBound" OnSelectedIndexChanged="gvDepartamentosAsignacionArea_SelectedIndexChanged" DataKeyNames="UidDepartamento" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No se encontraron departamentos
										</div>
									</EmptyDataTemplate>
								</asp:GridView>
								<asp:GridView runat="server" ID="gvAreasDisponiblesDepartamento" OnRowDataBound="gvAreasDisponiblesDepartamento_RowDataBound" OnSelectedIndexChanged="gvAreasDisponiblesDepartamento_SelectedIndexChanged" DataKeyNames="UidArea" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No se encontraron Areas
										</div>
									</EmptyDataTemplate>
								</asp:GridView>
								<div class="text-right">
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnMostrarDepartamentosAsignacion" OnClick="btnMostrarDepartamentosAsignacion_Click">
											<span class="glyphicon glyphicon-triangle-left"></span>
											Volver
									</asp:LinkButton>
								</div>
							</div>
						</asp:Panel>
					</asp:Panel>
					<asp:Panel runat="server" CssClass="col-md-12 pd-0" ID="pnlTabAntecesor">
						<asp:CheckBox Text="Activar Antecesor" runat="server" ID="cbActivarAntecesor" Checked="false" AutoPostBack="true" OnCheckedChanged="cbActivarAntecesor_CheckedChanged" />
						<asp:HiddenField runat="server" ID="hfUidAntecesor" />
						<asp:Panel runat="server" ID="pnlCamposAntecesor">
							<div class="col-md-12 pd-left-right-5">
								<div class="input-group">
									<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtNombreAntecesor" />
									<span class="input-group-btn">
										<asp:Button Text="Buscar" ID="btnBuscarAntecesor" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnBuscarAntecesor_Click" />
										<asp:LinkButton runat="server" ID="btnCancelarBuscarAntecesor" CssClass="btn btn-sm btn-danger" OnClick="btnCancelarBuscarAntecesor_Click">
											<span class="glyphicon glyphicon-remove"></span>
										</asp:LinkButton>
									</span>
								</div>
							</div>
							<div class="col-md-12 pd-left-right-5">
								<asp:HiddenField runat="server" ID="hfAntecesorSeleccionado" />
								<asp:GridView runat="server" ID="gvListaTareasAntecesor" OnRowDataBound="gvListaTareasAntecesor_RowDataBound" OnSelectedIndexChanged="gvListaTareasAntecesor_SelectedIndexChanged" DataKeyNames="UidTarea" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="IntFolio" HeaderText="Folio" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No existe ningun antecesor con ese nombre.
										</div>
									</EmptyDataTemplate>
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								</asp:GridView>
							</div>
							<div class="col-md-12 pd-left-right-5">
								<h6>Cumplimiento</h6>
								Ejecutar tantos días
                                <asp:TextBox ID="txtDiasDespues" runat="server" Text="0" />
								después.
							</div>
							<div class="col-md-12 pd-left-right-5">
								<label class="checkbox-inline">
									<asp:CheckBox ID="cbUsarNotificacionAntecesor" runat="server" AutoPostBack="true" OnCheckedChanged="cbUsarNotificacionAntecesor_CheckedChanged" />
									Usar notificación del antecesor.
								</label>
							</div>
							<asp:Panel runat="server" ID="pnlNotificacionAntecesor">
								<div class="col-md-12 pd-left-right-5">
									<h6>Número de repeticiones</h6>
									<div class="row">
										<div class="col-xs-12">
											<label class="radio-inline">
												<asp:RadioButton ID="rdNumeroLimitado" runat="server" AutoPostBack="true" GroupName="MaximoDias" />Máximo
                                                    <asp:TextBox ID="txtMaximo" runat="server" />
												veces
											</label>
										</div>
										<div class="col-xs-12">
											<label class="radio-inline">
												<asp:RadioButton ID="rdIlimitado" runat="server" AutoPostBack="true" GroupName="MaximoDias" />Siempre
											</label>
										</div>
									</div>
								</div>
							</asp:Panel>
						</asp:Panel>
					</asp:Panel>
					<asp:Panel runat="server" CssClass="col-md-12 pd-0" ID="pnlTabNotificacion">
						<asp:CheckBox Text="Activar Notificación" runat="server" AutoPostBack="true" ID="cbHabilitarNotificacionTarea" OnCheckedChanged="cbHabilitarNotificacionTarea_CheckedChanged" />
						<asp:HiddenField runat="server" ID="hfUidNotificacion" />
						<asp:Panel runat="server" ID="pnlNotificationSendTo" CssClass="col-md-12 pd-left-right-5">
							<small>Notificar a:</small>
							<label class="radio-inline">
								<asp:CheckBox runat="server" ID="cbSendToAdministrador" />
								Administrador
							</label>
							<label class="radio-inline">
								<asp:CheckBox runat="server" ID="cbSendToSupervisor" />
								Supervisor
							</label>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlNotificacionVerdaderoFalso" CssClass="col-md-12  pd-left-right-5">
							<small>Se lanzará la notificación cuando el valor sea:</small>
							<br />
							<label class="radio-inline">
								<asp:RadioButton runat="server" ID="rbVerdaderoNotificacion" GroupName="NotificacionVerdaderoFalso" />
								Sí
							</label>
							<label class="radio-inline">
								<asp:RadioButton runat="server" ID="rbFalsoNotificacion" GroupName="NotificacionVerdaderoFalso" />
								No
							</label>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlNotificacionValor" CssClass="col-md-12 pd-0">
							<div class="col-md-6 pd-left-right-5">
								<div class="col-xs-12 pd-0">
									<label class="radio-inline">
										<asp:CheckBox ID="cbMayor" runat="server" />
										Mayor
									</label>
									<label class="radio-inline">
										<asp:CheckBox ID="cbMayorIgual" runat="server" />
										Mayor o igual
									</label>
								</div>
								<div class="col-xs-12 pd-0">
									<div class="input-group">
										<asp:TextBox runat="server" ID="txtMayorQue" CssClass="form-control" />
										<asp:Label ID="txtUnidadMedidaV1" runat="server" CssClass="input-group-addon input-group-sm" />
									</div>
								</div>
							</div>
							<div class="col-md-6 pd-left-right-5">
								<div class="col-xs-12 pd-0">
									<label class="radio-inline">
										<asp:CheckBox ID="cbMenor" runat="server" />
										Menor
									</label>
									<label class="radio-inline">
										<asp:CheckBox ID="cbMenorIgual" runat="server" />
										Menor o igual
									</label>
								</div>
								<div class="col-xs-12 pd-0">
									<div class="input-group">
										<asp:TextBox ID="txtMenorQue" runat="server" CssClass="form-control" />
										<asp:Label ID="txtUnidadMedidaV2" runat="server" CssClass="input-group-addon input-group-sm" />
									</div>
								</div>
							</div>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlNotificacionOpcionMultiple">
							<div class="col-md-12 pd-left-right-5">
								<small>Se lanzará la notificación la cumplir con las opciones:</small>
								<br />
								<asp:ListBox runat="server" ID="lbNotificacionOpcionesSeleccionadas" CssClass="form-control" SelectionMode="Multiple" />
							</div>
						</asp:Panel>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function hora() {
			$('.input-group.clockpicker').clockpicker();
		}
	</script>

	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>
	<script>
		//<![CDATA[
		function pageLoad() {

			prepareFechaAll();
			hora();
		}
        //]]>
	</script>
</asp:Content>
