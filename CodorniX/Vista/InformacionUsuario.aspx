﻿<%@ Page Title="Datos del usuario" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="InformacionUsuario.aspx.cs" Inherits="CodorniX.Vista.InformacionUsuario" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Datos generales
				</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-0 text-right">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="btnEditarDatosUsuario" OnClick="btnEditarDatosUsuario_Click" CssClass="btn btn-sm btn-default">
								<span class="glyphicon glyphicon-pencil"></span>
								Editar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnAceptarDatosUsuario" OnClick="btnAceptarDatosUsuario_Click" CssClass="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-pencil"></span>
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnCancelarDatosUsuario" OnClick="btnCancelarDatosUsuario_Click" CssClass="btn btn-sm btn-danger">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</div>
					</div>
					<div class="col-md-12 pd-0">
						<asp:Panel runat="server" ID="pnlMessageLeft" CssClass="alert alert-info">
							<asp:Label Text="" ID="lblMessageLeft" runat="server" />
							<asp:LinkButton runat="server" CssClass="close" OnClick="btnCloseMessageLeft">
								<span aria-hidden="true">&times;</span>
							</asp:LinkButton>
						</asp:Panel>
					</div>
					<div class="col-md-12 pd-0">
						<div class="col-md-12 pd-left-right-5 pd-top-5">
							<small>Nombre</small>
							<asp:TextBox runat="server" ID="txtNombre" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Apellido Paterno</small>
							<asp:TextBox runat="server" ID="txtApellidoPaterno" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Apellido Materno</small>
							<asp:TextBox runat="server" ID="txtApellidoMaterno" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-4 pd-left-right-5 pd-top-5">
							<small>Fecha Nacimiento</small>
							<div class="input-group date extra">
								<asp:TextBox runat="server" ID="txtFechaNacimiento" CssClass="form-control input-sm" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-8 pd-left-right-5 pd-top-5">
							<small>Correo Electronico</small>
							<asp:TextBox runat="server" ID="txtCorreoElectronico" CssClass="form-control input-sm" />
						</div>
					</div>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Datos de acceso
				</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-0">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="btnEditarDatosAcceso" OnClick="btnEditarDatosAcceso_Click" CssClass="btn btn-sm btn-default">
								<span class="glyphicon glyphicon-pencil"></span>
								Editar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnAceptarDatosAcceso" OnClick="btnAceptarDatosAcceso_Click" CssClass="btn btn-sm btn-success">
								<span class="glyphicon glyphicon-pencil"></span>
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnCancelarDatosAcceso" OnClick="btnCancelarDatosAcceso_Click" CssClass="btn btn-sm btn-danger">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</div>
					</div>
					<div class="col-md-12 pd-0">
						<asp:Panel runat="server" ID="pnlMessageRight" CssClass="alert alert-info">
							<asp:Label Text="" ID="lblMessageRight" runat="server" />
							<asp:LinkButton runat="server" CssClass="close" OnClick="btnCloseMessageRight">
								<span aria-hidden="true">&times;</span>
							</asp:LinkButton>
						</asp:Panel>
					</div>
					<div class="col-md-12 pd-0">
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Usuario</small>
							<asp:TextBox runat="server" ID="txtUsuario" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Contraseña Actual</small>
							<asp:TextBox runat="server" ID="txtContraseniaActual" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Nueva Contraseña</small>
							<asp:TextBox runat="server" ID="txtContraseniaNueva" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-6 pd-left-right-5 pd-top-5">
							<small>Confirmar Contraseña</small>
							<asp:TextBox runat="server" ID="txtContraseniaNueva2" CssClass="form-control input-sm" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">	
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>
	<script>
		function pageLoad() {
			prepareFechaAll();
		}
	</script>
</asp:Content>
