﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="DetalleTareaAntecesor.aspx.cs" Inherits="CodorniX.Vista.DetalleTareaAntecesor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading  text-center">Tareas</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-left-right-5 text-right">
						<div class="btn-group">
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnMostrarFiltros" OnClick="btnMostrarFiltros_Click">
								<span class="glyphicon glyphicon-eye-closed"></span>
								<asp:Label Text="Mostrar" runat="server" ID="lblMostrarFiltros" />
							</asp:LinkButton>
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnLimpiarFiltros" OnClick="btnLimpiarFiltros_Click">
								<span class="glyphicon glyphicon-trash"></span>
								Limpiar
							</asp:LinkButton>
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscar" OnClick="btnBuscar_Click">
								<span class="glyphicon glyphicon-search"></span>
								Buscar
							</asp:LinkButton>
						</div>
					</div>
					<div class="col-xs-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlAlertBusqueda">
							<asp:Label Text="Error: " runat="server" ID="lblAlertBusqueda" />
							<asp:LinkButton OnClick="HideAlertBusqueda" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</asp:Panel>
					</div>
					<asp:Panel runat="server" ID="pnlFiltros">
						<div class="col-md-12 pd-left-right-5">
							<small>Nombre</small>
							<asp:TextBox CssClass="form-control input-sm" ID="txtFiltroNombre" runat="server" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Folio de tarea</small>
							<asp:TextBox CssClass="form-control input-sm" ID="txtFiltroFolioTarea" runat="server" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Fecha Inicio</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFiltroFechaInicio" CssClass="form-control input-sm" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Fecha Fin</small>
							<div class="input-group date extra">
								<asp:TextBox ID="txtFiltroFechaFin" CssClass="form-control input-sm" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Departamento</small>
							<asp:ListBox ID="lbFiltroDepartamento" runat="server" SelectionMode="Multiple" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Area</small>
							<asp:ListBox ID="lbFiltroArea" runat="server" SelectionMode="Multiple" CssClass="form-control input-sm" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Tarea</small>
							<asp:DropDownList runat="server" ID="ddlFiltroTipoTarea" CssClass="form-control input-sm">
								<asp:ListItem Text="Antecesora" Value="A" />
								<asp:ListItem Text="Sucesora" Value="S" />
							</asp:DropDownList>
						</div>
					</asp:Panel>

					<asp:Panel runat="server" ID="pnlListaTareas">
						<div class="col-md-12 pd-left-right-5">
							<asp:GridView runat="server" ID="gvListaTareas" DataKeyNames="UidTarea,UidDepartamento,UidArea,UidPeriodicidad,Type" OnPageIndexChanging="gvListaTareas_PageIndexChanging" OnRowDataBound="gvListaTareas_RowDataBound" OnSelectedIndexChanging="gvListaTareas_SelectedIndexChanging" OnSelectedIndexChanged="gvListaTareas_SelectedIndexChanged" CssClass="table table-bordered table-condensed table-hover input-sm" AutoGenerateColumns="false" SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
									<asp:BoundField DataField="Folio" HeaderText="Folio" DataFormatString="{0:D4}" SortExpression="Folio" />
									<asp:TemplateField HeaderText="Nombre" SortExpression="Nombre">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("Nombre") %>' runat="server" ID="lblTaskName" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="TipoFrecuencia" HeaderText="Periodicidad" SortExpression="Periodicidad" />
									<asp:TemplateField HeaderText="Departamento" SortExpression="Departamento">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("Departamento") %>' runat="server" ID="lblTaskDepto" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Area" SortExpression="Area">
										<ItemTemplate>
											<asp:Label Text='<%# Eval("Area") %>' runat="server" ID="lblTaskArea" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:d}" HeaderText="Fecha Inicio" SortExpression="FechaInicio" />
								</Columns>
								<EmptyDataTemplate>
									No se encontraron registros.
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>

		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Tareas Relacionadas</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-left-right-5">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="LinkButton1" CssClass="btn btn-sm btn-default" OnClick="Edit_Click">
								<span class="glyphicon glyphicon-pencil"></span>
								Editar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnOk" CssClass="btn btn-sm btn-success" OnClick="btnOk_Click">
								<span class="glyphicon glyphicon-ok"></span>
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnCancel" CssClass="btn btn-sm btn-danger" OnClick="btnCancel_Click">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
							<asp:HiddenField runat="server" ID="hfUidTarea" />
							<asp:HiddenField runat="server" ID="hfTipoTarea" />
						</div>
					</div>
					<ul class="nav nav-tabs">
						<li class="active" id="liTabDatosGenerales" runat="server">
							<asp:LinkButton Text="Datos Generales" runat="server" OnClick="TabDatosGenerales_Click" />
						</li>
						<li id="liTabTareasRelacionadas" runat="server">
							<asp:LinkButton Text="Antecesores" runat="server" ID="btnTabTareasRelacionadas" OnClick="TabTareasRelaciondas_Click" />
						</li>
					</ul>
					<div class="col-xs-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlErrorGeneral">
							<asp:Label Text="Error: " runat="server" ID="lblErrorGeneral" />
							<asp:LinkButton OnClick="HideAlertGeneral" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</asp:Panel>
					</div>
					<asp:Panel runat="server" ID="pnlDetallesTarea">
						<div class="col-md-8 pd-left-right-5">
							<small>Nombre</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtNombreTarea" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Estatus</small>
							<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlEstatusTarea">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<asp:Panel ID="frmGrpDescripcion" runat="server" CssClass="form-group">
								<h6>Descripcion</h6>
								<asp:TextBox ID="txtDescripcionTarea" runat="server" TextMode="MultiLine" Rows="9" CssClass="form-control input-sm" />
							</asp:Panel>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Inicio</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFechaInicioTarea" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Fin</small>
							<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFechaFinTarea" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Requerida</small>
							<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlRequeridaTarea">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<small>Tipo Medicion</small>
							<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlTipoMedicionTarea">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlTareasRelacionadas">
						<div class="col-md-12 pd-left-right-5">
							<asp:DataList runat="server" RepeatDirection="Vertical" ID="dlTareasRelacionadas" OnItemDataBound="dlTareasRelacionadas_ItemDataBound" OnItemCommand="dlTareasRelacionadas_ItemCommand" CssClass="col-md-12 pd-0" CellSpacing="3">
								<ItemStyle CssClass="col-md-12 pd-5" />
								<ItemTemplate>
									<hr style="margin-top:2px; margin-bottom:2px; border-color:#ddd;"/>
									<strong>Tarea</strong>
									<input value='<%# Eval("Nombre") %>' type="text" class="form-control input-sm disabled" disabled />
									
									<strong>Cumplimiento</strong>
									<br />
									Ejecutar despues de
										<asp:TextBox runat="server" ID="tbDiasDespues"></asp:TextBox>
									dias despues.
									<br />
									<label class="checkbox-inline">
										<asp:CheckBox runat="server" ID="cbUsarNotificacion" OnCheckedChanged="HabilitarNotificacionTarea_CheckedChanged" AutoPostBack="true" />
										Usar notificacion del antecesor
									</label>
									<br />
									<small>La tarea no tiene notificacion</small>
									<br />
									<asp:Panel runat="server" ID="pnlRepeticiones">
										<strong>Numero de repeticiones</strong>
										<br />
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" ID="cbMaximo" />
											Máximo
										<asp:TextBox runat="server" ID="tbNumeroVeces"></asp:TextBox>
											Veces
										</label>
										<br />
										<label class="checkbox-inline">
											<asp:CheckBox runat="server" ID="cbSiembre" />
											Siempre
										</label>
									</asp:Panel>
								</ItemTemplate>
							</asp:DataList>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
