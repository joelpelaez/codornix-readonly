﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using CodorniX.Modelo;
using System.Globalization;
using CodorniX.Util;

namespace CodorniX.Vista
{
	public partial class TareasSupervisor : System.Web.UI.Page
	{
		#region Properties
		private int FolioTar
		{
			get
			{
				return (int)ViewState["FiltroFolioT"];
			}
			set
			{
				ViewState["FiltroFolioT"] = value;
			}
		}
		private string NombreTarea
		{
			get
			{
				return ViewState["FNombre"] == null ? string.Empty : (string)ViewState["FNombre"];
			}
			set
			{
				ViewState["FNombre"] = value;
			}
		}
		private DateTime? DtFechaInicio
		{
			get
			{
				return ViewState["FFechaInicio"] == null ? (DateTime?)null : (DateTime?)ViewState["FFechaInicio"];
			}
			set
			{
				ViewState["FFechaInicio"] = value;
			}
		}
		private DateTime? DtFechaFin
		{
			get
			{
				return ViewState["FFFin"] == null ? (DateTime?)null : (DateTime?)ViewState["FFFin"];
			}
			set
			{
				ViewState["FFFin"] = value;
			}
		}
		private string Departamentos
		{
			get
			{
				return ViewState["FDeptos"] == null ? string.Empty : (string)ViewState["FDeptos"];
			}
			set
			{
				ViewState["FDeptos"] = value;
			}
		}
		private string UnidadesMedida
		{
			get
			{
				return ViewState["FUnMed"] == null ? string.Empty : (string)ViewState["FUnMed"];
			}
			set
			{
				ViewState["FUnMed"] = value;
			}
		}
		private int FolioAnt
		{
			get
			{
				return ViewState["FFolio"] == null ? 0 : (int)ViewState["FFolio"];
			}
			set
			{
				ViewState["FFolio"] = value;
			}
		}

		private List<Modelo.FechaPeriodicidad> LsFechasPeriodicidad
		{
			get
			{
				return (List<Modelo.FechaPeriodicidad>)ViewState["FechasPeriodicidad"];
			}
			set
			{
				ViewState["FechasPeriodicidad"] = value;
			}
		}
		private VMTareasSupervisor VmTareas = new VMTareasSupervisor();
		private Sesion SesionActual
		{
			get
			{
				return (Sesion)Session["Sesion"];
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{
				Response.Redirect("Login.aspx");
				return;
			}
			if (!SesionActual.Rol.Equals("Supervisor"))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}


			if (!IsPostBack)
			{
				pnlNotificationSendTo.Visible = false;
				HideMessagePeriodicity(null, null);
				btnGuardar.Visible = false;
				btnCancelar.Visible = false;
				hfUidTarea.Value = string.Empty;

				btnAceptarOpcionTarea.Visible = false;
				btnCancelarOpcionTarea.Visible = false;

				OcultarMostrarFiltros(true);
				HabilitarCamposDatosGenerales(false);
				HabilitarCamposOpcionTarea(false);
				HabilitarCamposPeriodicidad(false);

				HabilitarCamposAsignacion(false);
				btnCancelarBuscarDepartamentoAsignacion.Visible = false;
				pnlAsignacionDepartamentos.Visible = true;
				gvDepartamentosAsignados.DataSource = null;
				gvDepartamentosAsignados.DataBind();
				gvDepartamentosAsignacionBusqueda.DataSource = null;
				gvDepartamentosAsignacionBusqueda.DataBind();
				pnlAsignacionAreas.Visible = false;

				liOpcionesTarea.Visible = false;
				MostrarHora(false);

				ddlTipoMedicionTarea.SelectedIndex = 0;
				pnlUnidadMedida.Visible = false;

				pnlTabOpcionesTarea.Visible = false;
				pnlTabPeriodos.Visible = false;
				pnlTabAsignacion.Visible = false;
				pnlTabAntecesor.Visible = false;
				pnlTabNotificacion.Visible = false;

				VmTareas.ObtenerDepartamentos(SesionActual.UidDepartamentos);
				lbFiltroDepartamento.DataSource = VmTareas.LsFiltrosDepartamento;
				lbFiltroDepartamento.DataValueField = "UidDepartamento";
				lbFiltroDepartamento.DataTextField = "StrNombre";
				lbFiltroDepartamento.DataBind();

				VmTareas.ObtenerEstatusTarea();
				ddlEstatusTarea.DataSource = VmTareas.LsEstadosTarea;
				ddlEstatusTarea.DataTextField = "strStatus";
				ddlEstatusTarea.DataValueField = "UidStatus";
				ddlEstatusTarea.DataBind();
				ddlEstatusTarea.Enabled = false;

				VmTareas.ObtenerTipoTarea();
				ddlTareaRequerida.DataSource = VmTareas.LsTiposTarea;
				ddlTareaRequerida.DataTextField = "StrTipoTarea";
				ddlTareaRequerida.DataValueField = "UidTipoTarea";
				ddlTareaRequerida.DataBind();

				VmTareas.ObtenerTipoMedicion();
				ddlTipoMedicionTarea.DataSource = VmTareas.LsTipoMedicionTarea;
				ddlTipoMedicionTarea.DataValueField = "UidTipoMedicion";
				ddlTipoMedicionTarea.DataTextField = "StrTipoMedicion";
				ddlTipoMedicionTarea.DataBind();

				VmTareas.ObtenerUnidadesMedida(SesionActual.uidEmpresaActual.Value);
				lbFiltroUnidadMedida.DataSource = VmTareas.LsUnidadMedida;
				lbFiltroUnidadMedida.DataValueField = "UidUnidadMedida";
				lbFiltroUnidadMedida.DataTextField = "StrTipoUnidad";
				lbFiltroUnidadMedida.DataBind();

				ddlUnidadMedida.DataSource = VmTareas.LsUnidadMedida;
				ddlUnidadMedida.DataValueField = "UidUnidadMedida";
				ddlUnidadMedida.DataTextField = "StrTipoUnidad";
				ddlUnidadMedida.DataBind();

				VmTareas.ObtenerDias();
				cblPSListaDias.DataSource = VmTareas.LsDias;
				cblPSListaDias.DataValueField = "UidDias";
				cblPSListaDias.DataTextField = "StrDias";
				cblPSListaDias.DataBind();
				ddlPMDiaSemanaOLapsoDias.DataSource = VmTareas.LsDias;
				ddlPMDiaSemanaOLapsoDias.DataValueField = "UidDias";
				ddlPMDiaSemanaOLapsoDias.DataTextField = "StrDias";
				ddlPMDiaSemanaOLapsoDias.DataBind();
				ddlPAListaPeriodosyDias.DataSource = VmTareas.LsDias;
				ddlPAListaPeriodosyDias.DataValueField = "UidDias";
				ddlPAListaPeriodosyDias.DataTextField = "StrDias";
				ddlPAListaPeriodosyDias.DataBind();

				VmTareas.ObtenerMeses();
				ddlPAListaMesesOpc1.DataSource = VmTareas.LsMeses;
				ddlPAListaMesesOpc1.DataTextField = "StrMes";
				ddlPAListaMesesOpc1.DataValueField = "UidMes";
				ddlPAListaMesesOpc1.DataBind();
				ddlPAListaMesesOpc2.DataSource = VmTareas.LsMeses;
				ddlPAListaMesesOpc2.DataTextField = "StrMes";
				ddlPAListaMesesOpc2.DataValueField = "UidMes";
				ddlPAListaMesesOpc2.DataBind();

				VmTareas.ObtenerOrdinales();
				ddlPMListaNumerales.DataSource = VmTareas.LsOrdinales;
				ddlPMListaNumerales.DataTextField = "StrOrdinal";
				ddlPMListaNumerales.DataValueField = "UidOrdinal";
				ddlPMListaNumerales.DataBind();
				ddlPAListaNumerales.DataSource = VmTareas.LsOrdinales;
				ddlPAListaNumerales.DataTextField = "StrOrdinal";
				ddlPAListaNumerales.DataValueField = "UidOrdinal";
				ddlPAListaNumerales.DataBind();

				gvTareas.DataSource = null;
				gvTareas.DataBind();

				chkSinPeriodicida.Checked = true;
				DisplayPeriodicityFields("SN");

				cbActivarAntecesor.Enabled = false;
				pnlCamposAntecesor.Visible = false;

				pnlNotificacionOpcionMultiple.Visible = false;
				pnlNotificacionValor.Visible = false;
				pnlNotificacionVerdaderoFalso.Visible = false;

				pnlAlertBusqueda.Visible = false;
				pnlAlertGeneral.Visible = false;
				pnlAlertOpcionTarea.Visible = false;

				cbHabilitarNotificacionTarea.Enabled = false;
				cbSendToAdministrador.Enabled = false;
				cbSendToSupervisor.Enabled = false;
			}
		}

		#region Methodos
		#region Panel Izquierdo
		#region Botones
		protected void btnOcultar_Click(object sender, EventArgs e)
		{
			if (lblTextoOcultarFiltros.Text.Equals("Ocultar"))
			{
				OcultarMostrarFiltros(false);
			}
			else
			{
				OcultarMostrarFiltros(true);
			}
		}
		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			txtFiltroTarea.Text = string.Empty;
			txtFiltroFechaInicio.Text = string.Empty;
			txtFiltroFechaFin.Text = string.Empty;
			lbFiltroDepartamento.ClearSelection();
			lbFiltroUnidadMedida.ClearSelection();
			TxtFiltroFolioAntecesor.Text = string.Empty;
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			try
			{
				string NombreTarea = txtFiltroTarea.Text.Trim();
				DateTime? DtFechaInicio = null;
				DateTime? DtFechaFin = null;
				string UidsDepartamentos = string.Empty;
				string UidsUnidadMedida = string.Empty;
				int FolioTarea = 0;
				int FolioAntecesor = 0;

				///<summary>
				///Obtener fechas de los campos de texto
				///</summary>
				if (!string.IsNullOrEmpty(txtFiltroFechaInicio.Text))
				{
					DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				if (!string.IsNullOrEmpty(txtFiltroFechaFin.Text))
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}

				///Obtener Departamentos seleccionados en el listbox				
				if (lbFiltroDepartamento.Items.Count > 0)
					UidsDepartamentos = GetListBoxValueSelected(lbFiltroDepartamento);

				///Obtener Unidad de medida seleccionado
				if (lbFiltroUnidadMedida.Items.Count > 0)
					UidsUnidadMedida = GetListBoxValueSelected(lbFiltroUnidadMedida);

				///Obtener Folio de antecesor
				if (!string.IsNullOrEmpty(TxtFiltroFolioAntecesor.Text))
				{
					int AuxFolioAntecesor = 0;
					if (int.TryParse(TxtFiltroFolioAntecesor.Text, out AuxFolioAntecesor))
					{
						FolioAntecesor = AuxFolioAntecesor;
					}
					else
					{
						pnlAlertBusqueda.Visible = true;
						lblErrorBusqueda.Text = "Ingrese un folio númerico";
						return;
					}
				}

				// Obtener folio de tarea
				if (!string.IsNullOrEmpty(txtFiltroFolio.Text))
				{
					int Aux = 0;
					if (int.TryParse(txtFiltroFolio.Text.Trim(), out Aux))
					{
						FolioTarea = Aux;
					}
					else
					{
						pnlAlertBusqueda.Visible = true;
						lblErrorBusqueda.Text = "Ingrese un folio de tarea númerico.";
						return;
					}
				}
				if (UidsDepartamentos == string.Empty)
				{
					foreach (Guid item in SesionActual.UidDepartamentos)
					{
						if (UidsDepartamentos == string.Empty)
							UidsDepartamentos = item.ToString();
						else
							UidsDepartamentos += "," + item.ToString();
					}
				}

				this.FolioTar = FolioTarea;
				this.Departamentos = UidsDepartamentos;
				this.NombreTarea = NombreTarea;
				this.DtFechaInicio = DtFechaInicio;
				this.DtFechaFin = DtFechaFin;
				this.FolioAnt = FolioAntecesor;

				VmTareas.BusquedaTareas(this.FolioTar, SesionActual.uidSucursalActual.Value, this.Departamentos, this.NombreTarea, this.DtFechaInicio, this.DtFechaFin, this.FolioAnt);
				ViewState["Tareas"] = VmTareas.LsTareas;
				gvTareas.DataSource = VmTareas.LsTareas;
				gvTareas.DataBind();
				OcultarMostrarFiltros(false);
			}
			catch (Exception ex)
			{
				pnlAlertBusqueda.Visible = true;
				lblErrorBusqueda.Text = "Error al realizar busqueda";
			}
		}
		protected void CloseAlertDialogSearch(object sender, EventArgs e)
		{
			pnlAlertBusqueda.Visible = false;
		}
		private void OcultarMostrarFiltros(bool Status)
		{
			if (Status)
			{
				gvTareas.Visible = false;
				pnlFiltrosBusqueda.Visible = true;
				lblIconOcultarFiltros.CssClass = "glyphicon glyphicon-eye-close";
				btnLimpiar.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";
				lblTextoOcultarFiltros.Text = "Ocultar";
			}
			else
			{
				gvTareas.Visible = true;
				pnlFiltrosBusqueda.Visible = false;
				lblIconOcultarFiltros.CssClass = "glyphicon glyphicon-eye-open";
				btnLimpiar.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
				lblTextoOcultarFiltros.Text = "Mostrar";
			}
		}
		#endregion
		#region GridView
		protected void gvTareas_PreRender(object sender, EventArgs e)
		{
			GridViewRow pagerRow = gvTareas.TopPagerRow;

			if (pagerRow != null && pagerRow.Visible == false)
				pagerRow.Visible = true;
		}
		protected void gvTareas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareas, "Select$" + e.Row.RowIndex);
				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					//PERFIL.CssClass = "glyphicon glyphicon-user";
					e.Row.Cells[5].Text = "(Sin Asignar)";
				}
				if (e.Row.Cells[3].Text.Contains(","))
				{
					string split = e.Row.Cells[3].Text.Split(',')[0];
					e.Row.Cells[3].ToolTip = Server.HtmlDecode(e.Row.Cells[3].Text);
					e.Row.Cells[3].Text = split + ", ...";
				}
				if (e.Row.Cells[5].Text.Contains(","))
				{
					string split = e.Row.Cells[5].Text.Split(',')[0];
					e.Row.Cells[5].ToolTip = Server.HtmlDecode(e.Row.Cells[5].Text);
					e.Row.Cells[5].Text = split + ", ...";
				}
			}
		}
		protected void gvTareas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (btnGuardar.Visible)
			{
				e.Cancel = true;
			}
		}
		protected void gvTareas_SelectedIndexChanged(object sender, EventArgs e)
		{
			if (btnGuardar.Visible == false)
			{
				Guid UidTarea = new Guid(gvTareas.SelectedDataKey.Value.ToString());

				///<summary> 
				///Campos datos generales
				///</summary>
				VmTareas.ObtenerTarea(UidTarea);
				txtNombreTarea.Text = VmTareas.TAREA.StrNombre;
				txtDescripcionTarea.Text = VmTareas.TAREA.StrDescripcion;

				ddlEstatusTarea.SelectedValue = VmTareas.TAREA.UidStatus.ToString();
				ddlTareaRequerida.SelectedValue = VmTareas.TAREA.UidTipoTarea.ToString();
				ddlTipoMedicionTarea.SelectedValue = VmTareas.TAREA.UidMedicion.ToString();

				string TipoMedicion = ddlTipoMedicionTarea.SelectedItem.Text.ToString();
				if (TipoMedicion.Equals("Numerico"))
				{
					pnlUnidadMedida.Visible = true;
					liOpcionesTarea.Visible = false;
					ddlUnidadMedida.SelectedIndex = ddlUnidadMedida.Items.IndexOf(ddlUnidadMedida.Items.FindByValue(VmTareas.TAREA.UidUnidadMedida.ToString()));
				}
				if (TipoMedicion.Equals("Seleccionable"))
				{
					pnlUnidadMedida.Visible = true;
					liOpcionesTarea.Visible = true;
					btnNuevoOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
					VmTareas.ObtenerOpcionesTarea(UidTarea);
					ViewState["OpcionesTarea"] = VmTareas.LsOpcionesTarea;


					gvOpcionesTarea.DataSource = VmTareas.LsOpcionesTarea;
					gvOpcionesTarea.DataBind();
				}
				if (TipoMedicion.Equals("Verdadero/Falso"))
				{
					pnlUnidadMedida.Visible = true;
					liOpcionesTarea.Visible = false;
				}

				///<summary>
				///Campos Periodicidad
				///</summary>
				this.LsFechasPeriodicidad = new List<FechaPeriodicidad>();
				Guid UidPeriodicidad = VmTareas.TAREA.UidPeriodicidad;
				VmTareas.ObtenerPeriodicidad(UidPeriodicidad);
				txtFechaInicioTarea.Text = VmTareas.PERIODICIDAD.DtFechaInicio.ToString("dd/MM/yyyy");
				txtFechaFinTarea.Text = VmTareas.PERIODICIDAD.DtFechaFin == null ? string.Empty : VmTareas.PERIODICIDAD.DtFechaFin.Value.ToString("dd/MM/yyyy");
				VmTareas.ObtenerFrecuenciaPeriodicidad(VmTareas.PERIODICIDAD.UidTipoFrecuencia);
				string Tipo = VmTareas.TIPOFRECUENCIA.StrTipoFrecuencia;

				DisplayPeriodicityFields(Tipo);
				if (Tipo.Equals("Sin periodicidad"))
				{
					chkSinPeriodicida.Checked = true;
				}
				else if (Tipo.Equals("Diaria"))
				{
					LimpiarCamposPeriodicidadDiaria();
					chkPeriodicidadDiaria.Checked = true;
					HabilitarCamposPeriodicidadDiaria(false);

					if (VmTareas.PERIODICIDAD.IntFrecuencia == 1)
					{
						chkPDOpcion1.Checked = false;
						chkPDOpcion2.Checked = true;
					}
					else
					{
						chkPDOpcion1.Checked = false;
						chkPDOpcion2.Checked = true;
						txtPDNumeroDias.Text = VmTareas.PERIODICIDAD.IntFrecuencia.ToString();
					}
				}
				else if (Tipo.Equals("Semanal"))
				{
					LimpiarCamposPeriodicidadSemanal();

					chkPeriodicidadSemanal.Checked = true;
					HabilitarCamposPeriodicidadSemanal(false);
					VmTareas.ObtenerPeriodicidadSemanal(UidPeriodicidad);

					foreach (ListItem item in cblPSListaDias.Items)
					{
						if (item.Text.Equals("Lunes"))
							item.Selected = VmTareas.PSemanal.BlLunes;
						else if (item.Text.Equals("Martes"))
							item.Selected = VmTareas.PSemanal.BlMartes;
						else if (item.Text.Equals("Miercoles"))
							item.Selected = VmTareas.PSemanal.BlMiercoles;
						else if (item.Text.Equals("Jueves"))
							item.Selected = VmTareas.PSemanal.BlJueves;
						else if (item.Text.Equals("Viernes"))
							item.Selected = VmTareas.PSemanal.BlViernes;
						else if (item.Text.Equals("Sabado"))
							item.Selected = VmTareas.PSemanal.BlSabado;
						else if (item.Text.Equals("Domingo"))
							item.Selected = VmTareas.PSemanal.BlDomingo;
					}

					txtPSNumeroSemanas.Text = VmTareas.PERIODICIDAD.IntFrecuencia.ToString();
				}
				else if (Tipo.Equals("Mensual"))
				{
					chkPeriodicidadMensual.Checked = true;
					LimpiarCamposPeriodicidadMensual();
					HabilitarCamposPeriodicidadMensual(false);
					VmTareas.ObtenerPeriodicidadMensual(UidPeriodicidad);

					if (VmTareas.PMensual.Tipo.Equals("A"))
					{
						chkPMRepetirCadaNDiasCadaNMes.Checked = true;
						chkPMRepetirCadaNMESConParametros.Checked = false;
						chkPMTipoC.Checked = false;

						txtPMDiaDelMes.Text = VmTareas.PMensual.IntDiasMes.ToString();
						txtPMNumeroDeMesesOpc1.Text = VmTareas.PERIODICIDAD.IntFrecuencia.ToString();

						List<string> LsDias = VmTareas.PMensual.DiasMes.Split(',').ToList();
						this.LsFechasPeriodicidad = new List<Modelo.FechaPeriodicidad>();
						foreach (var item in LsDias)
						{
							LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(
									Guid.Empty,
									int.Parse(item),
									0,
									0,
									string.Empty,
									string.Empty,
									"Mensual",
									"A",
									true
								));
						}

						gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
						gvFechasPeriodicidad.DataBind();
					}
					else if (VmTareas.PMensual.Tipo.Equals("B"))
					{
						chkPMRepetirCadaNDiasCadaNMes.Checked = false;
						chkPMRepetirCadaNMESConParametros.Checked = true;
						chkPMTipoC.Checked = false;

						txtPMNumeroDeMesesOpc2.Text = VmTareas.PERIODICIDAD.IntFrecuencia.ToString();

						if (VmTareas.PMensual.IntDiasMes == 1)
							ddlPMListaNumerales.SelectedIndex = ddlPMListaNumerales.Items.IndexOf(ddlPMListaNumerales.Items.FindByText("Primer"));
						else if (VmTareas.PMensual.IntDiasMes == 2)
							ddlPMListaNumerales.SelectedIndex = ddlPMListaNumerales.Items.IndexOf(ddlPMListaNumerales.Items.FindByText("Segundo"));
						else if (VmTareas.PMensual.IntDiasMes == 3)
							ddlPMListaNumerales.SelectedIndex = ddlPMListaNumerales.Items.IndexOf(ddlPMListaNumerales.Items.FindByText("Tercer"));
						else if (VmTareas.PMensual.IntDiasMes == 4)
							ddlPMListaNumerales.SelectedIndex = ddlPMListaNumerales.Items.IndexOf(ddlPMListaNumerales.Items.FindByText("Cuarto"));
						else if (VmTareas.PMensual.IntDiasMes == -1)
							ddlPMListaNumerales.SelectedIndex = ddlPMListaNumerales.Items.IndexOf(ddlPMListaNumerales.Items.FindByText("Ultimo"));

						if (VmTareas.PMensual.IntDiasSemana == 2)
							ddlPMDiaSemanaOLapsoDias.SelectedIndex = ddlPMDiaSemanaOLapsoDias.Items.IndexOf(ddlPMDiaSemanaOLapsoDias.Items.FindByText("Lunes"));
						else if (VmTareas.PMensual.IntDiasSemana == 3)
							ddlPMDiaSemanaOLapsoDias.SelectedIndex = ddlPMDiaSemanaOLapsoDias.Items.IndexOf(ddlPMDiaSemanaOLapsoDias.Items.FindByText("Martes"));
						else if (VmTareas.PMensual.IntDiasSemana == 4)
							ddlPMDiaSemanaOLapsoDias.SelectedIndex = ddlPMDiaSemanaOLapsoDias.Items.IndexOf(ddlPMDiaSemanaOLapsoDias.Items.FindByText("Miercoles"));
						else if (VmTareas.PMensual.IntDiasSemana == 5)
							ddlPMDiaSemanaOLapsoDias.SelectedIndex = ddlPMDiaSemanaOLapsoDias.Items.IndexOf(ddlPMDiaSemanaOLapsoDias.Items.FindByText("Jueves"));
						else if (VmTareas.PMensual.IntDiasSemana == 6)
							ddlPMDiaSemanaOLapsoDias.SelectedIndex = ddlPMDiaSemanaOLapsoDias.Items.IndexOf(ddlPMDiaSemanaOLapsoDias.Items.FindByText("Viernes"));
						else if (VmTareas.PMensual.IntDiasSemana == 7)
							ddlPMDiaSemanaOLapsoDias.SelectedIndex = ddlPMDiaSemanaOLapsoDias.Items.IndexOf(ddlPMDiaSemanaOLapsoDias.Items.FindByText("Sabado"));
						else if (VmTareas.PMensual.IntDiasSemana == 1)
							ddlPMDiaSemanaOLapsoDias.SelectedIndex = ddlPMDiaSemanaOLapsoDias.Items.IndexOf(ddlPMDiaSemanaOLapsoDias.Items.FindByText("Domingo"));

					}
					else if (VmTareas.PMensual.Tipo.Equals("C"))
					{
						chkPMRepetirCadaNDiasCadaNMes.Checked = false;
						chkPMRepetirCadaNMESConParametros.Checked = false;
						chkPMTipoC.Checked = true;

						txtPMDiasOpc3.Text = VmTareas.PMensual.IntDiasSemana.ToString();
						ddlPMNumeralesOpc3.SelectedValue = VmTareas.PMensual.IntDiasMes.ToString();
					}
				}
				else if (Tipo.Equals("Anual"))
				{
					chkPeriodicidadAnual.Checked = true;
					LimpiarCamposPeriodicidadAnual();
					HabilitarCamposPeriodicidadAnual(false);
					VmTareas.ObtenerPeriodicidadAnual(UidPeriodicidad);

					if (VmTareas.PAnual.Tipo.Equals("A"))
					{
						chkPAOpcion1.Checked = true;
						chkPAOpcion2.Checked = false;

						VmTareas.ObtenerFechasPerodicidad(UidPeriodicidad);

						this.LsFechasPeriodicidad = VmTareas.LsFechasPeriodicidad;
						gvFechasPeriodicidad.DataSource = VmTareas.LsFechasPeriodicidad;
						gvFechasPeriodicidad.DataBind();
					}
					else if (VmTareas.PAnual.Tipo.Equals("B"))
					{
						chkPAOpcion1.Checked = false;
						chkPAOpcion2.Checked = true;

						if (VmTareas.PAnual.IntDiasMes == 1)
							ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Primer"));
						else if (VmTareas.PAnual.IntDiasMes == 2)
							ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Segundo"));
						else if (VmTareas.PAnual.IntDiasMes == 3)
							ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Tercer"));
						else if (VmTareas.PAnual.IntDiasMes == 4)
							ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Cuarto"));
						else if (VmTareas.PAnual.IntDiasMes == -1)
							ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Ultimo"));

						if (VmTareas.PAnual.IntDiasSemanas == 2)
							ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Lunes"));
						else if (VmTareas.PAnual.IntDiasSemanas == 3)
							ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Martes"));
						else if (VmTareas.PAnual.IntDiasSemanas == 4)
							ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Miercoles"));
						else if (VmTareas.PAnual.IntDiasSemanas == 5)
							ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Jueves"));
						else if (VmTareas.PAnual.IntDiasSemanas == 6)
							ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Viernes"));
						else if (VmTareas.PAnual.IntDiasSemanas == 7)
							ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Sabado"));
						else if (VmTareas.PAnual.IntDiasSemanas == 1)
							ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Domingo"));
					}

					txtPANumeroAnios.Text = VmTareas.PERIODICIDAD.IntFrecuencia.ToString();
				}

				///<summary>
				///Campos Asignacion
				///</summary>
				VmTareas.ObtenerDepartamentosAsignadosTarea(UidTarea);
				if (VmTareas.LsDepartamentosAsignados.Count > 0)
				{
					rbAsignarADepartamento.Checked = true;
					rbAsignarAArea.Checked = false;
					pnlAsignacionDepartamentos.Visible = true;
					pnlAsignacionAreas.Visible = false;

					ViewState["DepartamentoAsignado"] = VmTareas.LsDepartamentosAsignados;
					gvDepartamentosAsignados.DataSource = VmTareas.LsDepartamentosAsignados;
					gvDepartamentosAsignados.DataBind();
				}
				else
				{
					rbAsignarADepartamento.Checked = false;
					rbAsignarAArea.Checked = true;

					pnlAsignacionDepartamentos.Visible = false;
					pnlAsignacionAreas.Visible = true;
					gvDepartamentosAsignacionArea.Visible = true;
					gvAreasDisponiblesDepartamento.Visible = false;
					btnMostrarDepartamentosAsignacion.Visible = false;

					ViewState["AreasAsignadas"] = VmTareas.LsAreasAsignadas;
					VmTareas.ObtenerAreasAsignadasTarea(UidTarea);
					gvAreasAsignadas.DataSource = VmTareas.LsAreasAsignadas;
					gvAreasAsignadas.DataBind();

				}

				///<summary>
				///Antecesor
				///</summary>
				VmTareas.ObtenerAntecesorTarea(UidTarea);
				HabilitarCamposAntecesor(false);
				LimpiarCamposAntecesor();
				cbActivarAntecesor.Enabled = false;
				if (VmTareas.ANTECESOR == null)
				{
					cbActivarAntecesor.Checked = false;
					pnlCamposAntecesor.Visible = false;
				}
				else
				{
					cbActivarAntecesor.Checked = true;
					pnlCamposAntecesor.Visible = true;

					txtDiasDespues.Text = VmTareas.ANTECESOR.IntDiasDespues.ToString();
					VmTareas.ObtenerTarea(VmTareas.ANTECESOR.UidTareaAnterior);
					txtNombreAntecesor.Text = VmTareas.TAREA.StrNombre;

					if (VmTareas.ANTECESOR.BlUsarNotificacion.HasValue && VmTareas.ANTECESOR.BlUsarNotificacion.Value)
					{
						pnlNotificacionAntecesor.Visible = true;
						cbUsarNotificacionAntecesor.Checked = true;
						txtMaximo.Text = VmTareas.ANTECESOR.IntRepeticion?.ToString() ?? "";
						if (VmTareas.ANTECESOR.IntRepeticion.HasValue)
						{
							rdIlimitado.Checked = false;
							rdNumeroLimitado.Checked = true;
						}
						else
						{
							rdIlimitado.Checked = true;
							rdNumeroLimitado.Checked = false;
						}
					}
					else
					{
						cbUsarNotificacionAntecesor.Checked = false;
						pnlNotificacionAntecesor.Visible = false;
					}
				}

				///<summary>
				///Notificacion
				///</summary>
				VmTareas.ObtenerNotificacionTarea(UidTarea);
				cbHabilitarNotificacionTarea.Enabled = false;
				HabilitarCamposNotificacion(false);
				LimpiarCamposNotificacion();
				lbOpcionesSeleccionadasNotificacion.DataSource = null;
				lbOpcionesSeleccionadasNotificacion.DataBind();
				pnlNotificacionOpcionMultiple.Visible = false;
				pnlNotificacionValor.Visible = false;
				pnlNotificacionVerdaderoFalso.Visible = false;

				if (VmTareas.NOTIFICACION == null)
				{
					cbHabilitarNotificacionTarea.Checked = false;
				}
				else
				{
					cbHabilitarNotificacionTarea.Checked = VmTareas.NOTIFICACION.IsEnabled;

					if (TipoMedicion.Equals("Numerico"))
					{
						pnlNotificacionValor.Visible = true;
						if (VmTareas.NOTIFICACION.BlMenorIgual.HasValue && VmTareas.NOTIFICACION.BlMenorIgual.Value)
						{
							rbMenorIgual.Checked = true;
							rbMenor.Checked = false;
						}
						else
						{
							rbMenor.Checked = true;
							rbMenorIgual.Checked = false;
						}
						if (VmTareas.NOTIFICACION.BlMayorIgual.HasValue && VmTareas.NOTIFICACION.BlMayorIgual.Value)
						{
							rbMayorIgual.Checked = true;
							rbMayor.Checked = false;
						}
						else
						{
							rbMayor.Checked = true;
							rbMayorIgual.Checked = false;
						}
						if (VmTareas.NOTIFICACION.DcMayorQue.HasValue)
						{
							txtMayorQue.Text = VmTareas.NOTIFICACION.DcMayorQue.Value.ToString();
						}
						if (VmTareas.NOTIFICACION.DcMenorQue.HasValue)
						{
							txtMenorQue.Text = VmTareas.NOTIFICACION.DcMenorQue.Value.ToString();
						}
					}
					else if (TipoMedicion.Equals("Seleccionable"))
					{
						List<TareaOpcion> LsOpcionesTarea = (List<TareaOpcion>)ViewState["OpcionesTarea"];
						lbOpcionesSeleccionadasNotificacion.DataSource = LsOpcionesTarea;
						lbOpcionesSeleccionadasNotificacion.DataValueField = "UidOpciones";
						lbOpcionesSeleccionadasNotificacion.DataTextField = "StrOpciones";
						lbOpcionesSeleccionadasNotificacion.DataBind();
						lbOpcionesSeleccionadasNotificacion.Enabled = false;

						pnlNotificacionOpcionMultiple.Visible = true;
						if (VmTareas.NOTIFICACION.UidOpciones != null)
						{
							List<Guid> Opciones = VmTareas.NOTIFICACION.UidOpciones;
							int Index;
							foreach (ListItem item in lbOpcionesSeleccionadasNotificacion.Items)
							{
								Index = Opciones.FindIndex(x => x.ToString() == item.Value);
								if (Index >= 0)
								{
									item.Selected = true;
								}
							}
						}
					}
					else if (TipoMedicion.Equals("Verdadero/Falso"))
					{
						pnlNotificacionVerdaderoFalso.Visible = true;
						if (VmTareas.NOTIFICACION.BlValor.HasValue)
						{
							if (VmTareas.NOTIFICACION.BlValor.Value)
							{
								rbVerdaderoNotificacion.Checked = true;
							}
							else
							{
								rbFalsoNotificacion.Checked = true;
							}
						}
					}
				}
			}
			else
			{
				gvTareas.SelectedRowStyle.BackColor = System.Drawing.Color.White;
			}
		}
		protected void gvTareas_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection Direction = SortDirection.Ascending;
			if (hfGvTareasSortDirection.Value.Equals("ASC"))
			{
				hfGvTareasSortDirection.Value = "DESC";
			}
			else
			{
				hfGvTareasSortDirection.Value = "ASC";
				Direction = SortDirection.Descending;
			}

			gvTareas.SelectedIndex = -1;
			SortListaTareas(e.SortExpression, Direction);
			gvTareas.DataBind();
		}
		protected void gvTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvTareas.SelectedIndex = -1;
			gvTareas.DataSource = ViewState["Tareas"];
			gvTareas.PageIndex = e.NewPageIndex;
			gvTareas.DataBind();
		}
		public void SortListaTareas(string SortExpression, SortDirection SortDirection)
		{
			List<Tarea> LsTareas = (List<Tarea>)ViewState["Tareas"];
			if (SortExpression == "Folio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.IntFolio).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.IntFolio).ToList();
				}
			}
			else if (SortExpression == "Nombre")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrNombre).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrNombre).ToList();
				}
			}
			else if (SortExpression == "Encargado")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrUsuario).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrUsuario).ToList();
				}
			}
			else if (SortExpression == "FechaInicio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.DtFechaInicio).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.DtFechaInicio).ToList();
				}
			}
			else if (SortExpression == "Periodicidad")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrTipoFrecuencia).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrTipoFrecuencia).ToList();
				}
			}
			else if (SortExpression == "Departamento")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}

			gvTareas.DataSource = LsTareas;
		}
		#endregion
		#endregion

		#region Panel Derecho
		#region Botones
		/// <summary>
		/// Alerta
		/// </summary>
		protected void CloseAlertDialogGeneral(object sender, EventArgs e)
		{
			pnlAlertGeneral.Visible = false;
		}

		///<summary>
		///Gestion
		///</summary>		
		protected void btnNuevo_Click(object sender, EventArgs e)
		{
			gvTareas.SelectedIndex = -1;
			btnGuardar.Visible = true;
			btnCancelar.Visible = true;

			btnNuevo.CssClass = "btn btn-sm btn-default disabled";
			btnEditar.CssClass = "btn btn-sm btn-default disabled";

			HabilitarCamposDatosGenerales(true);
			LimpiarCamposDatosGenerales();

			txtFechaInicioTarea.Text = DateTime.Today.ToString("dd/MM/yyyy");

			// Periodicidad
			HabilitarCamposPeriodicidad(true);
			hfTipoPeriodicidad.Value = string.Empty;
			this.LsFechasPeriodicidad = new List<Modelo.FechaPeriodicidad>();
			DisplayPeriodicityFields("SN");
			LimpiarCamposPeriodicidadDiaria();
			LimpiarCamposPeriodicidadSemanal();
			LimpiarCamposPeriodicidadMensual();
			LimpiarCamposPeriodicidadAnual();

			rbAsignarADepartamento.Checked = true;
			pnlAsignacionDepartamentos.Visible = true;
			pnlAsignacionAreas.Visible = false;
			HabilitarCamposPeriodicidad(true);
			HabilitarCamposAsignacion(true);

			gvDepartamentosAsignados.DataSource = null;
			gvDepartamentosAsignados.DataBind();
			gvDepartamentosAsignacionBusqueda.DataSource = null;
			gvDepartamentosAsignacionBusqueda.DataBind();

			ddlTipoMedicionTarea.SelectedIndex = 0;

			pnlUnidadMedida.Visible = false;
			hfUidTarea.Value = string.Empty;
			hfAntecesorSeleccionado.Value = string.Empty;

			cbActivarAntecesor.Enabled = true;
			cbActivarAntecesor.Checked = false;

			cbHabilitarNotificacionTarea.Enabled = true;
			cbSendToAdministrador.Enabled = true;
			cbSendToSupervisor.Enabled = true;
			cbHabilitarNotificacionTarea.Checked = false;
			cbSendToAdministrador.Checked = false;
			cbSendToSupervisor.Checked = true;

			ViewState["OpcionesTarea"] = new List<TareaOpcion>();
			ViewState["DepartamentoAsignado"] = new List<Departamento>();
			ViewState["AreasAsignadas"] = new List<Modelo.Area>();
		}
		protected void btnEditar_Click(object sender, EventArgs e)
		{

		}
		protected void btnGuardar_Click(object sender, EventArgs e)
		{
			if (pnlAlertGeneral.Visible == true)
				pnlAlertGeneral.Visible = false;

			string NombreTarea;
			string DescripcionTarea = txtDescripcionTarea.Text.Trim() == string.Empty ? string.Empty : txtDescripcionTarea.Text.Trim();
			DateTime DtFechaInicio = DateTime.Today;
			DateTime? DtFechaFin = null;
			Guid UidTipoTarea = new Guid(ddlTareaRequerida.SelectedValue.ToString());
			Guid UidEstatusTarea = new Guid(ddlEstatusTarea.SelectedValue.ToString());
			Guid UidTipoMedicion = new Guid(ddlTipoMedicionTarea.SelectedValue.ToString());
			Guid UidUnidadMedida = Guid.Empty;
			Guid UidAntecesor = Guid.Empty;
			bool BlRequiereFoto = false;
			bool BlHabilitarHora = false;
			string Hora = string.Empty;
			int Tolerancia = -1;
			List<Modelo.TareaOpcion> LsOpcionesTarea;
			List<Modelo.Departamento> LsDepartamentosAsignados;
			List<Modelo.Area> LsAreasAsignadas;

			///<summary>
			///Validar campos [DATOS GENERALES]
			///</summary>			
			if (!string.IsNullOrEmpty(txtNombreTarea.Text.Trim()))
			{
				NombreTarea = txtNombreTarea.Text.Trim();
			}
			else
			{
				txtNombreTarea.Focus();
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Ingrese un nombre valido";
				return;
			}
			///<summary>
			///Fecha de Inicio
			///</summary>
			try
			{
				DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFechaInicioTarea.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				txtFechaInicioTarea.Focus();
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Ingrese una fecha de inicio valida";
				return;
			}
			///<summary>
			///Fecha fin
			///</summary>
			if (!string.IsNullOrEmpty(txtFechaFinTarea.Text))
			{
				try
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFechaFinTarea.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					txtFechaFinTarea.Focus();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Ingrese una fecha fin valida";
					return;
				}
			}

			if (CbFoto.Checked)
				BlRequiereFoto = true;
			///<summary>
			/// Validar si se habilitar hora de tarea
			///</summary>
			if (cbHabilitarHoraTarea.Checked)
			{
				BlHabilitarHora = true;
				if (string.IsNullOrEmpty(txtHoraTarea.Text.Trim()))
				{
					txtHoraTarea.Focus();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Ingrese una hora valida";
					return;
				}
				Hora = txtHoraTarea.Text.Trim();
				if (string.IsNullOrEmpty(txtToleranciaTarea.Text.Trim()))
				{
					txtToleranciaTarea.Focus();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Tolerancia Requerida";
					return;
				}

				int AuxTolerancia = 0;
				if (int.TryParse(txtToleranciaTarea.Text.Trim(), out AuxTolerancia))
				{
					Tolerancia = int.Parse(txtToleranciaTarea.Text.Trim());
				}
				else
				{
					txtToleranciaTarea.Focus();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Ingrese un valor numerico en Tolerancia.";
					return;
				}
			}

			string TipoMedicionTarea = ddlTipoMedicionTarea.SelectedItem.ToString();

			if (ddlTipoMedicionTarea.SelectedItem.ToString().Equals("Numerico"))
			{
				UidUnidadMedida = new Guid(ddlUnidadMedida.SelectedValue.ToString());
			}
			else if (ddlTipoMedicionTarea.SelectedItem.ToString().Equals("Seleccionable"))
			{
				LsOpcionesTarea = (List<Modelo.TareaOpcion>)ViewState["OpcionesTarea"];
				if (LsOpcionesTarea.Count == 0)
				{
					DisplayTabOpcionesTarea();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Agregue Opciones";
					return;
				}
			}

			///<summary>
			///Campos Periodicidad
			///</summary>
			if (!chkSinPeriodicida.Checked && !chkPeriodicidadSemanal.Checked && !chkPeriodicidadMensual.Checked && !chkPeriodicidadAnual.Checked && !chkPeriodicidadDiaria.Checked)
			{
				DisplayTabPeriodos();
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Es necesario que seleccione una periodicidad.";
				return;
			}

			bool auxError = false;
			string auxErrorMessage = string.Empty;
			///<summary>
			///Validar paramentros Periodicidad diaria
			///</summary>
			if (chkPeriodicidadDiaria.Checked)
			{
				if (!chkPDOpcion1.Checked && !chkPDOpcion2.Checked)
				{
					DisplayTabPeriodos();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Selecciona una opcion";
					return;
				}

				if (chkPDOpcion1.Checked)
				{
					if (!IsNumberValue(txtPDNumeroDias.Text.Trim()))
					{
						txtPDNumeroDias.Focus();
						auxError = true;
						auxErrorMessage = "Ingrese un valor numerico";
					}
				}
			}
			///<summary>
			///Validar paramentros Periodicidad Semanal
			///</summary>
			else if (chkPeriodicidadSemanal.Checked)
			{
				if (!IsNumberValue(txtPSNumeroSemanas.Text.Trim()))
				{
					txtPSNumeroSemanas.Focus();
					auxError = true;
					auxErrorMessage = "Ingrese un valor numerico";
				}

				bool IsSelected = false;
				foreach (ListItem item in cblPSListaDias.Items)
				{
					IsSelected = item.Selected;
					if (IsSelected)
						break;
				}

				if (!IsSelected)
				{
					this.DisplayTabPeriodos();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Selecciona los dias de cumplimiento.";
					return;
				}
			}
			///<summary>
			///Validar paramentros Periodicidad Mensual
			///</summary>
			else if (chkPeriodicidadMensual.Checked)
			{
				if (!chkPMRepetirCadaNDiasCadaNMes.Checked && !chkPMRepetirCadaNMESConParametros.Checked)
				{
					DisplayTabPeriodos();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Selecciona una opcion";
					return;
				}

				if (chkPMRepetirCadaNDiasCadaNMes.Checked)
				{
					if (this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Count == 0)
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Agregue minimo un dia.";
						return;
					}
				}
				else if (chkPMRepetirCadaNMESConParametros.Checked)
				{
					if (!IsNumberValue(txtPMNumeroDeMesesOpc2.Text.Trim()))
					{
						txtPMNumeroDeMesesOpc2.Focus();
						auxError = true;
						auxErrorMessage = "Ingrese un valor numerico";
					}
				}
				else if (chkPMTipoC.Checked)
				{
					if (!IsNumberValue(txtPMDiasOpc3.Text.Trim()))
					{
						txtPMDiasOpc3.Focus();
						auxError = true;
						auxErrorMessage = "Ingrese un valor numerico";
					}
				}
			}
			///<summary>
			///Validar paramentros Periodicidad Anual
			///</summary>
			else if (chkPeriodicidadAnual.Checked)
			{
				if (!chkPAOpcion1.Checked && !chkPAOpcion2.Checked)
				{
					DisplayTabPeriodos();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Selecciona una opcion";
					return;
				}

				if (!IsNumberValue(txtPANumeroAnios.Text.Trim()))
				{
					if (this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Count == 0)
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Agregue minimo un día.";
						return;
					}
				}
				if (!IsNumberValue(txtPADiaDelMesOpc1.Text.Trim()))
				{
					txtPADiaDelMesOpc1.Focus();
					auxError = true;
					auxErrorMessage = "Ingrese un valor numerico";
				}
			}
			///<summary>
			///Imprimir error
			///</summary>
			if (auxError)
			{
				DisplayTabPeriodos();
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = auxErrorMessage;
				return;
			}

			///<summary>
			///Asignacion de tarea
			///</summary>
			if (!rbAsignarADepartamento.Checked && !rbAsignarAArea.Checked)
			{
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Seleccione una opcion de asignación";
				return;
			}
			else if (rbAsignarADepartamento.Checked)
			{
				LsDepartamentosAsignados = (List<Departamento>)ViewState["DepartamentoAsignado"];
				if (LsDepartamentosAsignados.Count == 0)
				{
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Es necesario Asignar Minimo un Departamento";
				}
			}
			else if (rbAsignarAArea.Checked)
			{
				LsAreasAsignadas = (List<Modelo.Area>)ViewState["AreasAsignadas"];
				if (LsAreasAsignadas.Count == 0)
				{
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Es necesario Asignar Minimo un Area";
				}
			}

			///<summary>
			///Antecesor
			///</summary>
			if (cbActivarAntecesor.Checked)
			{
				if (hfAntecesorSeleccionado.Value == string.Empty)
				{
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Seleccione un antecesor";
					return;
				}
				else
					UidAntecesor = new Guid(hfAntecesorSeleccionado.Value);

				if (!IsNumberValue(txtDiasDespues.Text.Trim()))
				{
					txtDiasDespues.Focus();
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Ingrese un valor numerico";
					return;
				}

				if (cbUsarNotificacionAntecesor.Checked)
				{
					if (rdNumeroLimitado.Checked)
					{
						if (!IsNumberValue(txtMaximo.Text.Trim()))
						{
							txtMaximo.Focus();
							pnlAlertGeneral.Visible = true;
							lblErrorGeneral.Text = "Ingrese un valor numerico";
							return;
						}
					}
				}
			}

			///<summary>
			///Notificacion
			///</summary>		
			if (cbHabilitarNotificacionTarea.Checked)
			{
				if (TipoMedicionTarea.Equals("Numerico"))
				{
					decimal d;
					if (!string.IsNullOrWhiteSpace(txtMayorQue.Text) && !decimal.TryParse(txtMayorQue.Text, out d))
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "El valor limite es incorrecto";
						return;
					}
					if (!string.IsNullOrWhiteSpace(txtMenorQue.Text) && !decimal.TryParse(txtMenorQue.Text, out d))
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "El valor limite es incorrecto";
						return;
					}
				}
				else if (TipoMedicionTarea.Equals("Seleccionable"))
				{
					if (lbOpcionesSeleccionadasNotificacion.GetSelectedIndices().Count() == 0)
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Seleccione una opcion para la notificación";
						return;
					}
				}
				else if (TipoMedicionTarea.Equals("Verdadero/Falso"))
				{
					if (rbVerdaderoNotificacion.Checked && rbFalsoNotificacion.Checked)
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Solo puede seleccionar una opcion para la notificación.";
						return;
					}
					else if (!rbVerdaderoNotificacion.Checked && !rbFalsoNotificacion.Checked)
					{
						pnlAlertGeneral.Visible = true;
						lblErrorGeneral.Text = "Debe seleccionar una opcion para la noficicación.";
						return;
					}
				}

				if (!cbSendToAdministrador.Checked && !cbSendToSupervisor.Checked)
				{
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Seleccione a quien se le enviará la notificación";
					return;
				}
			}

			if (hfUidTarea.Value == string.Empty)
			{
				VmTareas.ObtenerTipoFrecuencia(hfTipoPeriodicidad.Value);
				Guid UidTipoFrecuencia = VmTareas.TIPOFRECUENCIA.UidTipoFrecuencia;

				// Fecha inicial del cumplimiento
				DateTime DtCumplimientoInicial = DtFechaInicio;
				// Frecuencia en la que se llevara a cabo el cumplimiento de la tarea
				int IntFrecuenciaCumplimiento = 1;
				// Frecuencia de la periodicidad
				string FrecuenciaCumplimiento = VmTareas.TIPOFRECUENCIA.StrTipoFrecuencia;

				#region GuardarPeriodicidad	
				if (chkSinPeriodicida.Checked)
				{
					VmTareas.GuardarPeriodicidad(0, UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
				}
				else if (chkPeriodicidadDiaria.Checked)
				{
					if (chkPDOpcion1.Checked)
					{
						VmTareas.GuardarPeriodicidad(int.Parse(txtPDNumeroDias.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPDNumeroDias.Text.Trim());
					}
					else if (chkPDOpcion2.Checked)
					{
						VmTareas.GuardarPeriodicidad(1, UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
					}
				}
				else if (chkPeriodicidadSemanal.Checked)
				{
					VmTareas.GuardarPeriodicidad(int.Parse(txtPSNumeroSemanas.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
					IntFrecuenciaCumplimiento = int.Parse(txtPSNumeroSemanas.Text.Trim());
				}
				else if (chkPeriodicidadMensual.Checked)
				{
					if (chkPMRepetirCadaNDiasCadaNMes.Checked)
					{
						VmTareas.GuardarPeriodicidad(int.Parse(txtPMNumeroDeMesesOpc1.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc1.Text.Trim());
					}
					else if (chkPMRepetirCadaNMESConParametros.Checked)
					{
						VmTareas.GuardarPeriodicidad(int.Parse(txtPMNumeroDeMesesOpc2.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc2.Text.Trim());
					}
				}
				else if (chkPeriodicidadAnual.Checked)
				{
					if (chkPAOpcion1.Checked)
					{
						VmTareas.GuardarPeriodicidad(int.Parse(txtPANumeroAnios.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPANumeroAnios.Text.Trim());
					}
					else if (chkPAOpcion2.Checked)
					{
						VmTareas.GuardarPeriodicidad(int.Parse(txtPANumeroAnios.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPANumeroAnios.Text.Trim());
					}
				}
				#endregion

				Guid UidPeriodicidad = VmTareas.PERIODICIDAD.UidPeriodicidad;
				VmTareas.GuardarTarea(NombreTarea, DescripcionTarea, UidAntecesor, UidUnidadMedida, UidPeriodicidad, UidTipoMedicion, Hora, Tolerancia, UidTipoTarea, UidEstatusTarea, BlRequiereFoto, false, SesionActual.uidSucursalActual.Value);
				Guid UidTarea = VmTareas.TAREA.UidTarea;

				string StrTipo = string.Empty;
				if (hfTipoPeriodicidad.Value.Equals("Semanal"))
				{
					bool BlLunes = true;
					bool BLMartes = false;
					bool BLMiercoles = false;
					bool BLJueves = false;
					bool BLViernes = false;
					bool BlSabado = false;
					bool BlDomingo = false;
					foreach (ListItem item in cblPSListaDias.Items)
					{
						if (item.Text.Equals("Lunes"))
							BlLunes = item.Selected;
						else if (item.Text.Equals("Martes"))
							BLMartes = item.Selected;
						else if (item.Text.Equals("Miercoles"))
							BLMiercoles = item.Selected;
						else if (item.Text.Equals("Jueves"))
							BLJueves = item.Selected;
						else if (item.Text.Equals("Viernes"))
							BLViernes = item.Selected;
						else if (item.Text.Equals("Sabado"))
							BlSabado = item.Selected;
						else if (item.Text.Equals("Domingo"))
							BlDomingo = item.Selected;
					}

					VmTareas.GuardarPeriodicidadSemanal(UidPeriodicidad, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
					DtCumplimientoInicial = VmTareas.CalcularCumplimientoPeriodicidadSemanal(DtFechaInicio.Date, IntFrecuenciaCumplimiento, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
				}
				else if (hfTipoPeriodicidad.Value.Equals("Mensual"))
				{
					if (chkPMRepetirCadaNDiasCadaNMes.Checked)
					{
						this.LsFechasPeriodicidad = this.LsFechasPeriodicidad.OrderBy(f => f.IntDia).ToList();
						string DiasMes = string.Join(",", this.LsFechasPeriodicidad.Select(f => f.IntDia));

						StrTipo = "A";
						VmTareas.GuardarPeriodicidadMensual(UidPeriodicidad, int.Parse(txtPMDiaDelMes.Text.Trim()), 0, DiasMes, StrTipo);
						DtCumplimientoInicial = VmTareas.CalcularCumplimientoPeriodicidadMensual("A", DtFechaInicio, new List<FechaPeriodicidad>(), DiasMes, 1, 2, IntFrecuenciaCumplimiento);
					}
					else if (chkPMRepetirCadaNMESConParametros.Checked)
					{
						StrTipo = "B";
						VmTareas.GuardarPeriodicidadMensual(UidPeriodicidad, 0, 0, string.Empty, StrTipo);
						foreach (Modelo.FechaPeriodicidad item in this.LsFechasPeriodicidad)
						{
							if (!item.IsSaved)
							{
								VmTareas.GuardarFechaPeriodicidad(UidPeriodicidad, item.IntNumeral, item.IntDia, item.IntNumeroMes, "B");
							}
						}
						DtCumplimientoInicial = VmTareas.CalcularCumplimientoPeriodicidadMensual("B", DtFechaInicio, this.LsFechasPeriodicidad, string.Empty, 1, 1, IntFrecuenciaCumplimiento);
					}
					else if (chkPMTipoC.Checked)
					{
						int NumeroDias = int.Parse(txtPMDiasOpc3.Text.Trim());
						int Ordinal = int.Parse(ddlPMNumeralesOpc3.SelectedValue.ToString());
						StrTipo = "C";
						VmTareas.GuardarPeriodicidadMensual(UidPeriodicidad, Ordinal, NumeroDias, StrTipo, string.Empty);
						DtCumplimientoInicial = VmTareas.CalcularCumplimientoPeriodicidadMensual("C", DtFechaInicio, new List<FechaPeriodicidad>(), string.Empty, Ordinal, NumeroDias, IntFrecuenciaCumplimiento);
					}
				}
				else if (hfTipoPeriodicidad.Value.Equals("Anual"))
				{
					if (chkPAOpcion1.Checked)
					{
						StrTipo = "A";

						VmTareas.GuardarPeriodicidadAnual(UidPeriodicidad, 0, 0, 0, StrTipo);
						foreach (Modelo.FechaPeriodicidad fp in this.LsFechasPeriodicidad)
						{
							VmTareas.GuardarFechaPeriodicidad(UidPeriodicidad, 0, fp.IntDia, fp.IntNumeroMes, "A");
						}
						DateTime DtFechaActual = SesionActual.GetDateTimeOffset().Date;
						List<DateTime> LsDates = new List<DateTime>();
						foreach (FechaPeriodicidad fp in this.LsFechasPeriodicidad)
						{
							LsDates.Add(DateTime.Parse(
								fp.IntDia.ToString("00") + "/" +
								fp.IntNumeroMes.ToString("00") + "/" +
								DtFechaActual.Year.ToString("00")));
						}
						DtCumplimientoInicial = VmTareas.CalcularCumplimientoPeriodicidadAnual("A", DtFechaInicio, LsDates, 0, 1, 2, IntFrecuenciaCumplimiento);
					}
					else if (chkPAOpcion2.Checked)
					{
						StrTipo = "B";
						int Ordinal = 1;
						int DiaSemana = 1;
						int NumeroMes = 1;

						if (ddlPMListaNumerales.SelectedItem.Text == "Primer")
							Ordinal = 1;
						else if (ddlPMListaNumerales.SelectedItem.Text == "Segundo")
							Ordinal = 2;
						else if (ddlPMListaNumerales.SelectedItem.Text == "Tercer")
							Ordinal = 3;
						else if (ddlPMListaNumerales.SelectedItem.Text == "Cuarto")
							Ordinal = 4;
						else if (ddlPMListaNumerales.SelectedItem.Text == "Ultimo")
							Ordinal = -1;

						if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Domingo")
							DiaSemana = 1;
						else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Lunes")
							DiaSemana = 2;
						else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Martes")
							DiaSemana = 3;
						else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Miercoles")
							DiaSemana = 4;
						else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Jueves")
							DiaSemana = 5;
						else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Viernes")
							DiaSemana = 6;
						else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Sabado")
							DiaSemana = 7;

						if (ddlPAListaMesesOpc1.SelectedItem.Text == "Enero")
							NumeroMes = 1;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Febrero")
							NumeroMes = 2;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Marzo")
							NumeroMes = 3;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Abril")
							NumeroMes = 4;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Mayo")
							NumeroMes = 5;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Junio")
							NumeroMes = 6;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Julio")
							NumeroMes = 7;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Agosto")
							NumeroMes = 8;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Septiembre")
							NumeroMes = 9;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Octubre")
							NumeroMes = 10;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Noviembre")
							NumeroMes = 11;
						else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Diciembre")
							NumeroMes = 12;

						VmTareas.GuardarPeriodicidadAnual(UidPeriodicidad, Ordinal, DiaSemana, NumeroMes, StrTipo);
						DtCumplimientoInicial = VmTareas.CalcularCumplimientoPeriodicidadAnual("B", DtFechaInicio, new List<DateTime>(), NumeroMes, Ordinal, DiaSemana, IntFrecuenciaCumplimiento);
					}
				}

				///<summary>
				/// Guardar Asignaciones
				///</summary>
				if (rbAsignarADepartamento.Checked)
				{
					LsDepartamentosAsignados = (List<Departamento>)ViewState["DepartamentoAsignado"];
					VmTareas.GuardarDepartamentos(LsDepartamentosAsignados, UidTarea);

					foreach (var depto in LsDepartamentosAsignados)
					{
						VmTareas.GuardarPrimerCumplimiento(UidTarea, depto.UidDepartamento, Guid.Empty, DtCumplimientoInicial);
					}
				}
				else if (rbAsignarAArea.Checked)
				{
					LsAreasAsignadas = (List<Modelo.Area>)ViewState["AreasAsignadas"];
					VmTareas.GuardarAreas(LsAreasAsignadas, UidTarea);

					foreach (var area in LsAreasAsignadas)
					{
						VmTareas.GuardarPrimerCumplimiento(UidTarea, Guid.Empty, area.UidArea, DtCumplimientoInicial);
					}
				}

				///<summary>
				///Guardar Opciones de tarea
				///</summary>
				LsOpcionesTarea = (List<TareaOpcion>)ViewState["OpcionesTarea"];
				if (LsOpcionesTarea.Count > 0)
				{
					VmTareas.GuardarOpcionesTarea(LsOpcionesTarea, UidTarea);
				}

				///<summary>
				///Guardar Nofiticacion
				///</summary>
				if (cbHabilitarNotificacionTarea.Checked)
				{
					Notificacion _Notificacion = new Notificacion();
					if (ddlTipoMedicionTarea.SelectedItem.Text == "Verdadero/Falso")
					{
						_Notificacion.BlValor = rbVerdaderoNotificacion.Checked;
					}
					else if (ddlTipoMedicionTarea.SelectedItem.Text == "Numerico")
					{
						if (!string.IsNullOrWhiteSpace(txtMenorQue.Text))
						{
							_Notificacion.DcMenorQue = Convert.ToDecimal(txtMenorQue.Text);
							_Notificacion.BlMenorIgual = rbMenorIgual.Checked;
						}
						if (!string.IsNullOrWhiteSpace(txtMayorQue.Text))
						{
							_Notificacion.DcMayorQue = Convert.ToDecimal(txtMayorQue.Text);
							_Notificacion.BlMayorIgual = rbMayorIgual.Checked;
						}
					}
					else if (ddlTipoMedicionTarea.SelectedItem.Text == "Seleccionable")
					{
						VmTareas.ObtenerOpcionesTarea(UidTarea);
						List<TareaOpcion> OpcionesTarea = VmTareas.LsOpcionesTarea;
						string OpcionesSeleccionadas = string.Empty;
						int Index = -1;
						foreach (ListItem item in lbOpcionesSeleccionadasNotificacion.Items)
						{
							if (item.Selected)
							{
								Index = OpcionesTarea.FindIndex(o => o.StrOpciones == item.Text);
								if (Index >= 0)
								{
									if (OpcionesSeleccionadas == string.Empty)
										OpcionesSeleccionadas = OpcionesTarea[Index].UidOpciones.ToString();
									else
										OpcionesSeleccionadas += "," + OpcionesTarea[Index].UidOpciones.ToString();
								}
							}
						}
						_Notificacion.VchOpciones = OpcionesSeleccionadas;
					}

					_Notificacion.UidTarea = UidTarea;
					_Notificacion.SendToAdministrador = cbSendToAdministrador.Checked;
					_Notificacion.SendToSupervisor = cbSendToSupervisor.Checked;

					VmTareas.GuardarNotificacion(_Notificacion);
				}

				///<summary>
				///Guardar Antecesor
				///</summary>
				if (cbActivarAntecesor.Checked)
				{
					Antecesor _Antecesor = new Antecesor();
					_Antecesor.IntDiasDespues = int.Parse(txtDiasDespues.Text.Trim());
					if (cbUsarNotificacionAntecesor.Checked && rdNumeroLimitado.Checked)
						_Antecesor.IntRepeticion = int.Parse(txtMaximo.Text.Trim());
					_Antecesor.BlUsarNotificacion = cbUsarNotificacionAntecesor.Checked;
					_Antecesor.UidTarea = UidTarea;
					_Antecesor.UidTareaAnterior = UidAntecesor;

					VmTareas.GuardaAntecesor(_Antecesor);
				}
			}
			else
			{
				btnEditar.CssClass = "btn btn-sm btn-default";
			}

			btnNuevo.CssClass = "btn btn-sm btn-default";
			btnEditar.CssClass = "btn btn-sm btn-default disabled";
			btnGuardar.Visible = false;
			btnCancelar.Visible = false;

			HabilitarCamposDatosGenerales(false);
			HabilitarCamposPeriodicidad(false);
			HabilitarCamposAsignacion(false);
			HabilitarCamposAntecesor(false);


			VmTareas.BusquedaTareas(this.FolioTar, SesionActual.uidSucursalActual.Value, this.Departamentos, this.NombreTarea, this.DtFechaInicio, this.DtFechaFin, this.FolioAnt);
			ViewState["Tareas"] = VmTareas.LsTareas;
			gvTareas.DataSource = VmTareas.LsTareas;
			gvTareas.DataBind();
		}
		protected void btnCancelar_Click(object sender, EventArgs e)
		{
			if (pnlAlertGeneral.Visible)
				pnlAlertGeneral.Visible = false;

			btnGuardar.Visible = false;
			btnCancelar.Visible = false;

			btnNuevo.CssClass = "btn btn-sm btn-default";

			HabilitarCamposDatosGenerales(false);
			HabilitarCamposOpcionTarea(false);
			HabilitarCamposPeriodicidad(false);
			HabilitarCamposAsignacion(false);

			cbActivarAntecesor.Enabled = false;
			cbHabilitarNotificacionTarea.Enabled = false;

			if (string.IsNullOrEmpty(hfUidTarea.Value))
			{
				LimpiarCamposDatosGenerales();
				ddlTipoMedicionTarea.SelectedIndex = 0;

				///<summary>
				/// Tab Opciones
				///</summary>
				LimpiarCamposOpcionTarea();
				btnNuevoOpcionTarea.CssClass = "btn btn-sm btn-default";
				btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
				btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
				btnAceptarOpcionTarea.Visible = false;
				btnCancelarOpcionTarea.Visible = false;

				if (liOpcionesTarea.Visible)
					liOpcionesTarea.Visible = false;

				if (pnlTabOpcionesTarea.Visible)
					DisplayTabDatosGenerales();

				gvOpcionesTarea.DataSource = null;
				gvOpcionesTarea.DataBind();

				///<summary>
				///Periodicidad
				///</summary>
				DisplaySinPeriodicidad();

				///<summary>
				///Tab Asignacion Departamentos
				///</summary>
				rbAsignarADepartamento.Checked = true;
				rbAsignarAArea.Checked = false;

				///<summary>
				/// Tab Antecesor
				///</summary>
				pnlCamposAntecesor.Visible = false;

				if (btnCancelarBuscarDepartamentoAsignacion.Visible == true)
					btnCancelarBuscarDepartamentoAsignacion.Visible = false;
				btnMostrarDepartamentosAsignacion.Visible = false;
				pnlAsignacionDepartamentos.Visible = true;
				pnlAsignacionAreas.Visible = false;

				gvDepartamentosAsignados.DataSource = null;
				gvDepartamentosAsignados.DataBind();
				gvDepartamentosAsignacionBusqueda.DataSource = null;
				gvDepartamentosAsignacionBusqueda.DataBind();

				gvAreasAsignadas.DataSource = null;
				gvAreasAsignadas.DataBind();
				gvDepartamentosAsignacionArea.DataSource = null;
				gvDepartamentosAsignacionArea.DataBind();
				gvAreasDisponiblesDepartamento.DataSource = null;
				gvAreasDisponiblesDepartamento.DataBind();

				cbSendToAdministrador.Enabled = false;
				cbSendToSupervisor.Enabled = false;
			}
			else
			{
				btnEditar.CssClass = "btn btn-sm btn-default";
			}
		}

		///<summary>
		///Opciones de tarea
		///</summary>
		protected void btnNuevoOpcionTarea_Click(object sender, EventArgs e)
		{
			HabilitarCamposOpcionTarea(true);
			LimpiarCamposOpcionTarea();
			btnAceptarOpcionTarea.Visible = true;
			btnCancelarOpcionTarea.Visible = true;

			btnNuevoOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
			btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
			btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";

			txtOrdenOpcionTarea.Text = "0";
			cbActivoOpcionTarea.Checked = true;
			hfUidOpcionTarea.Value = string.Empty;

			if (gvOpcionesTarea.Rows.Count > 0)
				gvOpcionesTarea.SelectedIndex = -1;
		}
		protected void btnEditarOpcionTarea_Click(object sender, EventArgs e)
		{
			HabilitarCamposOpcionTarea(true);
			btnNuevoOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
			btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
			btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
			btnAceptarOpcionTarea.Visible = true;
			btnCancelarOpcionTarea.Visible = true;
		}
		protected void btnEliminarOpcionTarea_Click(object sender, EventArgs e)
		{

		}
		protected void btnAceptarOpcionTarea_Click(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtNombreOpcionTarea.Text.Trim()))
			{
				pnlAlertOpcionTarea.Visible = true;
				lblErrorOpcionTarea.Text = "Ingrese un nombre valido";
				return;
			}
			if (string.IsNullOrEmpty(txtOrdenOpcionTarea.Text.Trim()))
			{
				int AuxOrden = 0;
				if (!int.TryParse(txtOrdenOpcionTarea.Text.Trim(), out AuxOrden))
				{
					pnlAlertOpcionTarea.Visible = true;
					lblErrorOpcionTarea.Text = "Ingrese un valor numerico valido en el ordén";
					return;
				}
			}

			List<TareaOpcion> LsOpcionesTarea = (List<TareaOpcion>)ViewState["OpcionesTarea"];
			if (LsOpcionesTarea == null)
				LsOpcionesTarea = new List<TareaOpcion>();

			if (hfUidOpcionTarea.Value == string.Empty)
				LsOpcionesTarea.Add(new TareaOpcion()
				{
					UidOpciones = Guid.NewGuid(),
					StrOpciones = txtNombreOpcionTarea.Text.Trim(),
					IntOrden = int.Parse(txtOrdenOpcionTarea.Text.Trim()),
					BlVisible = cbActivoOpcionTarea.Checked ? true : false
				});
			else
			{
				Guid UidOpcion = new Guid(hfUidOpcionTarea.Value);
				int Index = LsOpcionesTarea.FindIndex(x => x.UidOpciones == UidOpcion);
				if (Index >= 0)
				{
					LsOpcionesTarea[Index].StrOpciones = txtNombreOpcionTarea.Text.Trim();
					LsOpcionesTarea[Index].IntOrden = int.Parse(txtOrdenOpcionTarea.Text.Trim());
					LsOpcionesTarea[Index].BlVisible = cbActivoOpcionTarea.Checked ? true : false;
				}
			}

			ViewState["OpcionesTarea"] = LsOpcionesTarea;
			HabilitarCamposOpcionTarea(false);
			btnAceptarOpcionTarea.Visible = false;
			btnCancelarOpcionTarea.Visible = false;
			btnNuevoOpcionTarea.CssClass = "btn btn-sm btn-default";
			if (string.IsNullOrEmpty(hfUidOpcionTarea.Value))
			{
				LimpiarCamposOpcionTarea();
				btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
				btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
			}
			else
			{
				btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default";
				btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default";
			}

			gvOpcionesTarea.DataSource = LsOpcionesTarea;
			gvOpcionesTarea.DataBind();
		}
		protected void btnCancelarOpcionTarea_Click(object sender, EventArgs e)
		{
			HabilitarCamposOpcionTarea(false);

			btnAceptarOpcionTarea.Visible = false;
			btnCancelarOpcionTarea.Visible = false;
			btnNuevoOpcionTarea.CssClass = "btn btn-sm btn-default";
			btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
			btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";

			if (string.IsNullOrEmpty(hfUidOpcionTarea.Value))
			{
				LimpiarCamposOpcionTarea();
			}
		}
		protected void CloseAlertDialogOpcionTarea(object sender, EventArgs e)
		{
			pnlAlertOpcionTarea.Visible = false;
		}

		/// <summary>
		/// Periodicidad
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void btnAgregarFechaMensual_Click(object sender, EventArgs e)
		{
			if (pnlMessagePeriodicity.Visible)
				pnlMessagePeriodicity.Visible = false;

			if (chkPMRepetirCadaNDiasCadaNMes.Checked)
			{
				int Dia = 1;

				if (!int.TryParse(txtPMDiaDelMes.Text.Trim(), out Dia))
				{
					DisplayMessagePeriodicity("Ingrese un valor numerico.", "warning");
					return;
				}

				if (Dia > 31 || Dia == 0)
				{
					DisplayMessagePeriodicity("El valor ingresado es invalido.", "warning");
					return;
				}

				this.LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(Guid.Empty, Dia, 0, 0, "None", string.Empty, "Mensual", "A", false));
				gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
				gvFechasPeriodicidad.DataBind();
			}
		}
		protected void btnAgregarFechaAnual_Click(object sender, EventArgs e)
		{
			if (pnlMessagePeriodicity.Visible)
				pnlMessagePeriodicity.Visible = false;

			if (chkPAOpcion1.Checked)
			{
				int Dia = 1;
				int NumeroMes = 1;
				string Mes = string.Empty;

				if (!int.TryParse(txtPADiaDelMesOpc1.Text.Trim(), out Dia))
				{
					DisplayMessagePeriodicity("Ingrese un valor numerico.", "warning");
					return;
				}

				if (Dia > 31 || Dia == 0)
				{
					DisplayMessagePeriodicity("El valor ingresado es invalido.", "warning");
					return;
				}

				Mes = ddlPAListaMesesOpc1.SelectedItem.Text.ToString();
				switch (Mes)
				{
					case "Enero":
						NumeroMes = 1;
						break;
					case "Febrero":
						NumeroMes = 2;
						break;
					case "Marzo":
						NumeroMes = 3;
						break;
					case "Abril":
						NumeroMes = 4;
						break;
					case "Mayo":
						NumeroMes = 5;
						break;
					case "Junio":
						NumeroMes = 6;
						break;
					case "Julio":
						NumeroMes = 7;
						break;
					case "Agosto":
						NumeroMes = 8;
						break;
					case "Septiembre":
						NumeroMes = 9;
						break;
					case "Octubre":
						NumeroMes = 10;
						break;
					case "Noviembre":
						NumeroMes = 11;
						break;
					case "Diciembre":
						NumeroMes = 12;
						break;
					default:
						break;
				}

				try
				{
					DateTime DtFecha = Convert.ToDateTime(DateTime.ParseExact(Dia.ToString("00") + "/" + NumeroMes.ToString("00") + "/" + DateTime.Now.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayMessagePeriodicity("El valor ingresado es invalido.", "warning");
					return;
				}

				this.LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(Guid.Empty, Dia, NumeroMes, 0, Mes, string.Empty, "Anual", "A", false));
				gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
				gvFechasPeriodicidad.DataBind();
			}
		}


		/// <summary>
		/// Asignacion de Departamentos
		/// </summary>
		protected void btnBuscarDepartamentoAsignacion_Click(object sender, EventArgs e)
		{
			VmTareas.ObtenerDepartamentos(SesionActual.UidDepartamentos);
			if (rbAsignarADepartamento.Checked)
			{
				gvDepartamentosAsignacionBusqueda.DataSource = VmTareas.LsFiltrosDepartamento;
				gvDepartamentosAsignacionBusqueda.DataBind();
			}
			else if (rbAsignarAArea.Checked)
			{
				gvDepartamentosAsignacionArea.DataSource = VmTareas.LsFiltrosDepartamento;
				gvDepartamentosAsignacionArea.DataBind();
			}
			btnCancelarBuscarDepartamentoAsignacion.Visible = true;
		}
		protected void btnCancelarBuscarDepartamentoAsignacion_Click(object sender, EventArgs e)
		{
			txtNombreDepartamentoAsignacion.Text = string.Empty;
		}
		protected void btnMostrarDepartamentosAsignacion_Click(object sender, EventArgs e)
		{
			gvDepartamentosAsignacionArea.Visible = true;
			gvAreasDisponiblesDepartamento.Visible = false;
			gvAreasDisponiblesDepartamento.DataSource = null;
			gvAreasDisponiblesDepartamento.DataBind();

			btnMostrarDepartamentosAsignacion.Visible = false;
		}

		///<summary>
		/// Antecesor
		///</summary>
		protected void btnBuscarAntecesor_Click(object sender, EventArgs e)
		{
			try
			{
				string txtNombre = txtNombreAntecesor.Text.Trim() == string.Empty ? string.Empty : txtNombreAntecesor.Text.Trim();
				VmTareas.BuscarAntecesor(SesionActual.uidSucursalActual.Value, SesionActual.UidDepartamentos, txtNombre);

				gvListaTareasAntecesor.Visible = true;
				gvListaTareasAntecesor.DataSource = VmTareas.LsAntecesores;
				gvListaTareasAntecesor.DataBind();

			}
			catch (Exception)
			{

			}
		}
		protected void btnCancelarBuscarAntecesor_Click(object sender, EventArgs e)
		{
			hfAntecesorSeleccionado.Value = string.Empty;
			gvListaTareasAntecesor.Visible = false;
			txtNombreAntecesor.Text = string.Empty;
			txtNombreAntecesor.Enabled = true;
		}

		/// <summary>
		/// Navegacion
		/// </summary>
		protected void NavDatosGenerales_Click(object sender, EventArgs e)
		{
			DisplayTabDatosGenerales();
		}
		private void DisplayTabDatosGenerales()
		{
			liDatosGenerales.Attributes.Add("class", "active");
			liOpcionesTarea.Attributes.Add("class", "");
			liPeriodos.Attributes.Add("class", "");
			liAsignacion.Attributes.Add("class", "");
			liAntecesor.Attributes.Add("class", "");
			liNotificacion.Attributes.Add("class", "");

			pnlTabDatosGenerales.Visible = true;
			pnlTabOpcionesTarea.Visible = false;
			pnlTabPeriodos.Visible = false;
			pnlTabAsignacion.Visible = false;
			pnlTabAntecesor.Visible = false;
			pnlTabNotificacion.Visible = false;
		}
		protected void NavOpcionesTarea_Click(object sender, EventArgs e)
		{
			DisplayTabOpcionesTarea();
		}
		private void DisplayTabOpcionesTarea()
		{
			liDatosGenerales.Attributes.Add("class", "");
			liOpcionesTarea.Attributes.Add("class", "active");
			liPeriodos.Attributes.Add("class", "");
			liAsignacion.Attributes.Add("class", "");
			liAntecesor.Attributes.Add("class", "");
			liNotificacion.Attributes.Add("class", "");

			pnlTabDatosGenerales.Visible = false;
			pnlTabOpcionesTarea.Visible = true;
			pnlTabPeriodos.Visible = false;
			pnlTabAsignacion.Visible = false;
			pnlTabAntecesor.Visible = false;
			pnlTabNotificacion.Visible = false;
		}
		protected void NavPeriodos_Click(object sender, EventArgs e)
		{
			DisplayTabPeriodos();
		}
		private void DisplayTabPeriodos()
		{
			liDatosGenerales.Attributes.Add("class", "");
			liOpcionesTarea.Attributes.Add("class", "");
			liPeriodos.Attributes.Add("class", "active");
			liAsignacion.Attributes.Add("class", "");
			liAntecesor.Attributes.Add("class", "");
			liNotificacion.Attributes.Add("class", "");

			pnlTabDatosGenerales.Visible = false;
			pnlTabOpcionesTarea.Visible = false;
			pnlTabPeriodos.Visible = true;
			pnlTabAsignacion.Visible = false;
			pnlTabAntecesor.Visible = false;
			pnlTabNotificacion.Visible = false;
		}
		protected void NavAsignacion_Click(object sender, EventArgs e)
		{
			liDatosGenerales.Attributes.Add("class", "");
			liOpcionesTarea.Attributes.Add("class", "");
			liPeriodos.Attributes.Add("class", "");
			liAsignacion.Attributes.Add("class", "active");
			liAntecesor.Attributes.Add("class", "");
			liNotificacion.Attributes.Add("class", "");

			pnlTabDatosGenerales.Visible = false;
			pnlTabOpcionesTarea.Visible = false;
			pnlTabPeriodos.Visible = false;
			pnlTabAsignacion.Visible = true;
			pnlTabAntecesor.Visible = false;
			pnlTabNotificacion.Visible = false;
		}
		protected void NavAntecesor_Click(object sender, EventArgs e)
		{
			liDatosGenerales.Attributes.Add("class", "");
			liOpcionesTarea.Attributes.Add("class", "");
			liPeriodos.Attributes.Add("class", "");
			liAsignacion.Attributes.Add("class", "");
			liAntecesor.Attributes.Add("class", "active");
			liNotificacion.Attributes.Add("class", "");

			pnlTabDatosGenerales.Visible = false;
			pnlTabOpcionesTarea.Visible = false;
			pnlTabPeriodos.Visible = false;
			pnlTabAsignacion.Visible = false;
			pnlTabAntecesor.Visible = true;
			pnlTabNotificacion.Visible = false;
		}
		protected void NavNotificacion_Click(object sender, EventArgs e)
		{
			liDatosGenerales.Attributes.Add("class", "");
			liOpcionesTarea.Attributes.Add("class", "");
			liPeriodos.Attributes.Add("class", "");
			liAsignacion.Attributes.Add("class", "");
			liAntecesor.Attributes.Add("class", "");
			liNotificacion.Attributes.Add("class", "active");

			pnlTabDatosGenerales.Visible = false;
			pnlTabOpcionesTarea.Visible = false;
			pnlTabPeriodos.Visible = false;
			pnlTabAsignacion.Visible = false;
			pnlTabAntecesor.Visible = false;
			pnlTabNotificacion.Visible = true;
		}
		#endregion
		#region DropDownList
		protected void ddlTipoMedicionTarea_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (ddlTipoMedicionTarea.SelectedItem.ToString())
			{
				case "Numerico":
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("Numerico");
					pnlUnidadMedida.Visible = true;
					if (liOpcionesTarea.Visible == true)
						liOpcionesTarea.Visible = false;
					break;
				case "Seleccionable":
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("Seleccionable");
					if (pnlUnidadMedida.Visible)
						pnlUnidadMedida.Visible = false;
					liOpcionesTarea.Visible = true;
					break;
				case "Verdadero/Falso":
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("Verdadero/Falso");
					if (pnlUnidadMedida.Visible)
						pnlUnidadMedida.Visible = false;
					if (liOpcionesTarea.Visible == true)
						liOpcionesTarea.Visible = false;
					break;
				default:
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("none");
					if (pnlUnidadMedida.Visible)
						pnlUnidadMedida.Visible = false;
					if (liOpcionesTarea.Visible == true)
						liOpcionesTarea.Visible = false;
					break;
			}
		}
		#endregion
		#region CheckBox
		protected void cbHabilitarHoraTarea_CheckedChanged(object sender, EventArgs e)
		{
			if (cbHabilitarHoraTarea.Checked)
			{
				MostrarHora(true);
			}
			else
			{
				MostrarHora(false);
			}
		}

		protected void chkSinPeriodicida_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("SN");
		}
		protected void chkPeriodicidadDiaria_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Diaria");
		}
		protected void chkPeriodicidadSemanal_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Semanal");
		}
		protected void chkPeriodicidadMensual_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Mensual");
		}
		protected void chkPeriodicidadAnual_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Anual");
		}
		private void DisplayPeriodicityFields(string Type)
		{
			switch (Type)
			{
				case "Diaria":
					DisplayDiaria();
					hfTipoPeriodicidad.Value = "Diaria";
					break;
				case "Semanal":
					DisplaySemanal();
					hfTipoPeriodicidad.Value = "Semanal";
					break;
				case "Mensual":
					DisplayMesual();
					hfTipoPeriodicidad.Value = "Mensual";
					break;
				case "Anual":
					DisplayAnual();
					hfTipoPeriodicidad.Value = "Anual";
					break;
				default:
					DisplaySinPeriodicidad();
					hfTipoPeriodicidad.Value = "Sin periodicidad";
					break;
			}
		}
		private void DisplaySinPeriodicidad()
		{
			chkPeriodicidadDiaria.Checked = false;
			chkPeriodicidadSemanal.Checked = false;
			chkPeriodicidadMensual.Checked = false;
			chkPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = false;

			if (gvFechasPeriodicidad.Visible)
				gvFechasPeriodicidad.Visible = false;
		}
		private void DisplayDiaria()
		{
			chkSinPeriodicida.Checked = false;
			chkPeriodicidadSemanal.Checked = false;
			chkPeriodicidadMensual.Checked = false;
			chkPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = true;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = false;

			LimpiarCamposPeriodicidadDiaria();

			if (gvFechasPeriodicidad.Visible)
				gvFechasPeriodicidad.Visible = false;
		}
		private void DisplaySemanal()
		{
			chkSinPeriodicida.Checked = false;
			chkPeriodicidadDiaria.Checked = false;
			chkPeriodicidadMensual.Checked = false;
			chkPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = true;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = false;

			LimpiarCamposPeriodicidadSemanal();

			if (gvFechasPeriodicidad.Visible)
				gvFechasPeriodicidad.Visible = false;
		}
		private void DisplayMesual()
		{
			chkSinPeriodicida.Checked = false;
			chkPeriodicidadDiaria.Checked = false;
			chkPeriodicidadSemanal.Checked = false;
			chkPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = true;
			subpnlPeriodicidadAnual.Visible = false;

			LimpiarCamposPeriodicidadMensual();
			gvFechasPeriodicidad.Visible = true;
			this.LsFechasPeriodicidad = new List<Modelo.FechaPeriodicidad>();
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}
		private void DisplayAnual()
		{
			chkSinPeriodicida.Checked = false;
			chkPeriodicidadDiaria.Checked = false;
			chkPeriodicidadSemanal.Checked = false;
			chkPeriodicidadMensual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = true;

			LimpiarCamposPeriodicidadAnual();

			gvFechasPeriodicidad.Visible = true;
			this.LsFechasPeriodicidad = new List<Modelo.FechaPeriodicidad>();
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}

		/// <summary>
		/// Opciones peridicidad Diaria
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkPDOpcion1_CheckedChanged(object sender, EventArgs e)
		{
			txtPDNumeroDias.Text = "1";
			chkPDOpcion2.Checked = false;
		}
		protected void chkPDOpcion2_CheckedChanged(object sender, EventArgs e)
		{
			chkPDOpcion1.Checked = false;
		}
		/// <summary>
		/// Opciones Periodicidad Mensual
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkPMRepetirCadaNDiasCadaNMes_CheckedChanged(object sender, EventArgs e)
		{
			txtPMDiaDelMes.Text = "1";
			txtPMNumeroDeMesesOpc1.Text = "1";
			chkPMRepetirCadaNMESConParametros.Checked = false;
			chkPMTipoC.Checked = false;
		}
		protected void chkPMRepetirCadaNMESConParametros_CheckedChanged(object sender, EventArgs e)
		{
			chkPMRepetirCadaNDiasCadaNMes.Checked = false;
			chkPMTipoC.Checked = false;
			ddlPMListaNumerales.SelectedIndex = 0;
			ddlPMDiaSemanaOLapsoDias.SelectedIndex = 0;
			txtPMNumeroDeMesesOpc2.Text = "1";
		}
		protected void chkPMTipoC_CheckedChanged(object sender, EventArgs e)
		{
			chkPMRepetirCadaNDiasCadaNMes.Checked = false;
			chkPMRepetirCadaNMESConParametros.Checked = false;

			txtPMDiasOpc3.Text = "2";
			ddlPMNumeralesOpc3.SelectedIndex = 0;
			txtPMMesesOpc3.Text = "1";

			btnAgregarFechaMensual.Visible = false;
			gvFechasPeriodicidad.Visible = false;
		}
		/// <summary>
		/// Opciones Periodicidad Anual
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkPAOpcion1_CheckedChanged(object sender, EventArgs e)
		{
			ddlPAListaMesesOpc1.SelectedIndex = 0;
			txtPADiaDelMesOpc1.Text = "1";
			chkPAOpcion2.Checked = false;
		}
		protected void chkPAOpcion2_CheckedChanged(object sender, EventArgs e)
		{
			ddlPAListaNumerales.SelectedIndex = 0;
			ddlPAListaPeriodosyDias.SelectedIndex = 0;
			ddlPAListaMesesOpc2.SelectedIndex = 0;
			chkPAOpcion1.Checked = false;
		}

		/// <summary>
		/// Antecesor
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void cbActivarAntecesor_CheckedChanged(object sender, EventArgs e)
		{
			pnlCamposAntecesor.Visible = cbActivarAntecesor.Checked;
			if (cbActivarAntecesor.Checked)
			{
				hfAntecesorSeleccionado.Value = string.Empty;
				txtNombreAntecesor.Text = string.Empty;

				gvListaTareasAntecesor.Visible = false;
				gvListaTareasAntecesor.DataSource = null;
				gvListaTareasAntecesor.DataBind();

				txtDiasDespues.Text = "1";

				cbUsarNotificacionAntecesor.Checked = false;

				pnlNotificacionAntecesor.Visible = false;
			}
		}
		protected void cbUsarNotificacionAntecesor_CheckedChanged(object sender, EventArgs e)
		{
			pnlNotificacionAntecesor.Visible = cbUsarNotificacionAntecesor.Checked;
			if (cbUsarNotificacionAntecesor.Checked)
			{
				rdNumeroLimitado.Checked = false;
				txtMaximo.Text = "1";
				rdIlimitado.Checked = true;
			}
		}

		/// <summary>
		/// Notificacion
		/// </summary>
		protected void cbHabilitarNotificacionTarea_CheckedChanged(object sender, EventArgs e)
		{
			if (cbHabilitarNotificacionTarea.Checked)
			{
				pnlNotificationSendTo.Visible = true;
				SetFieldsNotificacion(ddlTipoMedicionTarea.SelectedItem.ToString());
			}
			else
			{
				pnlNotificationSendTo.Visible = false;
				SetFieldsNotificacion("none");
			}
		}
		#endregion
		#region RadioButton
		protected void rbAsignarADepartamento_CheckedChanged(object sender, EventArgs e)
		{
			if (rbAsignarADepartamento.Checked)
			{
				gvDepartamentosAsignados.DataSource = null;
				gvDepartamentosAsignados.DataBind();
				rbAsignarAArea.Checked = false;
				pnlAsignacionDepartamentos.Visible = true;
				pnlAsignacionAreas.Visible = false;
			}
		}
		protected void rbAsignarAArea_CheckedChanged(object sender, EventArgs e)
		{
			if (rbAsignarAArea.Checked)
			{
				gvAreasAsignadas.DataSource = null;
				gvAreasAsignadas.DataBind();

				rbAsignarADepartamento.Checked = false;
				pnlAsignacionDepartamentos.Visible = false;
				pnlAsignacionAreas.Visible = true;
				gvDepartamentosAsignacionArea.Visible = true;
				gvAreasDisponiblesDepartamento.Visible = false;
				btnMostrarDepartamentosAsignacion.Visible = false;
			}
		}
		#endregion
		#region GridView
		protected void gvOpcionesTarea_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvOpcionesTarea, "Select$" + e.Row.RowIndex);

				Label lblEstatus = e.Row.FindControl("lblActivo") as Label;
				if (e.Row.Cells[4].Text.Equals("True"))
				{
					//e.Row.Cells.
					lblEstatus.CssClass = "glyphicon glyphicon-ok";
					lblEstatus.ToolTip = "Activo";
				}
				else
				{
					lblEstatus.CssClass = "glyphicon glyphicon-remove";
					lblEstatus.ToolTip = "Inactivo";
				}
			}
		}
		protected void gvOpcionesTarea_SelectedIndexChanged(object sender, EventArgs e)
		{

			if (btnAceptarOpcionTarea.Visible != true)
			{
				Guid UidOpcionTarea = new Guid(gvOpcionesTarea.SelectedDataKey.Value.ToString());
				hfUidOpcionTarea.Value = UidOpcionTarea.ToString();
				List<TareaOpcion> LsOpcionesTarea = (List<TareaOpcion>)ViewState["OpcionesTarea"];
				int Index = LsOpcionesTarea.FindIndex(x => x.UidOpciones == UidOpcionTarea);

				txtNombreOpcionTarea.Text = LsOpcionesTarea[Index].StrOpciones;
				txtOrdenOpcionTarea.Text = LsOpcionesTarea[Index].IntOrden.ToString();
				cbActivoOpcionTarea.Checked = LsOpcionesTarea[Index].BlVisible;

				if (hfUidTarea.Value == string.Empty && btnGuardar.Visible)
				{
					btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default ";
					btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default ";
				}
				else
				{
					btnEditarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
					btnEliminarOpcionTarea.CssClass = "btn btn-sm btn-default disabled";
				}
			}
			else
			{
				gvOpcionesTarea.SelectedRowStyle.BackColor = System.Drawing.Color.White;
			}

		}

		protected void gvFechasPeriodicidad_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvFechasPeriodicidad, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvFechasPeriodicidad_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (btnGuardar.Visible)
			{
				if (e.CommandName.Equals("Delete"))
				{
					Guid Uid = new Guid(e.CommandArgument.ToString());
					int Index = this.LsFechasPeriodicidad.FindIndex(t => t.Identificador == Uid);

					if (this.LsFechasPeriodicidad[Index].IsSaved)
						this.LsFechasPeriodicidad[Index].ToDelete = true;
					else
						this.LsFechasPeriodicidad.RemoveAt(Index);
				}
			}
		}
		protected void gvFechasPeriodicidad_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
			gvFechasPeriodicidad.DataBind();
		}

		/// <summary>
		/// Asignacion de departamentos
		/// </summary>
		protected void gvDepartamentosAsignados_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvDepartamentosAsignados, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvDepartamentosAsignados_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (hfUidTarea.Value == string.Empty)
				{
					Guid UidDepartamento = new Guid(gvDepartamentosAsignados.SelectedDataKey.Value.ToString());

					List<Departamento> LsDepartamentosSeleccionados = (List<Departamento>)ViewState["DepartamentoAsignado"];

					if (LsDepartamentosSeleccionados.Count > 0)
					{
						int Index = LsDepartamentosSeleccionados.FindIndex(x => x.UidDepartamento == UidDepartamento);
						if (Index > -1)
						{
							LsDepartamentosSeleccionados.RemoveAt(Index);
						}
					}

					ViewState["DepartamentoAsignado"] = LsDepartamentosSeleccionados;
					gvDepartamentosAsignados.DataSource = LsDepartamentosSeleccionados;
					gvDepartamentosAsignados.DataBind();
				}
			}
			catch (Exception ex)
			{

			}
		}

		protected void gvDepartamentosAsignacionBusqueda_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvDepartamentosAsignacionBusqueda, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvDepartamentosAsignacionBusqueda_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (hfUidTarea.Value == string.Empty)
				{
					Guid UidDepartamento = new Guid(gvDepartamentosAsignacionBusqueda.SelectedDataKey.Value.ToString());
					VmTareas.ObtenerDepartamento(UidDepartamento);

					List<Departamento> LsDepartamentosSeleccionados = (List<Departamento>)ViewState["DepartamentoAsignado"];

					if (LsDepartamentosSeleccionados.Count > 0)
					{
						int index = LsDepartamentosSeleccionados.FindIndex(x => x.UidDepartamento == UidDepartamento);
						if (index == -1)
							LsDepartamentosSeleccionados.Add(new Departamento()
							{
								UidDepartamento = VmTareas.DEPARTAMENTO.UidDepartamento,
								StrNombre = VmTareas.DEPARTAMENTO.StrNombre,
								StrDescripcion = VmTareas.DEPARTAMENTO.StrDescripcion
							});
					}
					else
						LsDepartamentosSeleccionados.Add(new Departamento()
						{
							UidDepartamento = VmTareas.DEPARTAMENTO.UidDepartamento,
							StrNombre = VmTareas.DEPARTAMENTO.StrNombre,
							StrDescripcion = VmTareas.DEPARTAMENTO.StrDescripcion
						});

					ViewState["DepartamentoAsignado"] = LsDepartamentosSeleccionados;
					gvDepartamentosAsignados.DataSource = LsDepartamentosSeleccionados;
					gvDepartamentosAsignados.DataBind();
				}
			}
			catch (Exception ex)
			{

			}
		}

		/// <summary>
		/// Asignacion de areas
		/// </summary>
		protected void gvAreasAsignadas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvAreasAsignadas, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvAreasAsignadas_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (hfUidTarea.Value == string.Empty)
				{
					Guid UidArea = new Guid(gvAreasAsignadas.SelectedDataKey.Value.ToString());

					List<Modelo.Area> LsAreas = (List<Modelo.Area>)ViewState["AreasAsignadas"];
					if (LsAreas.Count > 0)
					{
						int Index = LsAreas.FindIndex(x => x.UidArea == UidArea);
						if (Index > -1)
						{
							LsAreas.RemoveAt(Index);
						}
					}
					ViewState["AreasAsignadas"] = LsAreas;

					gvAreasAsignadas.DataSource = LsAreas;
					gvAreasAsignadas.DataBind();
				}
			}
			catch (Exception ex)
			{

			}
		}

		protected void gvDepartamentosAsignacionArea_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvDepartamentosAsignacionArea, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvDepartamentosAsignacionArea_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (hfUidTarea.Value == string.Empty)
				{
					Guid UidDepartamento = new Guid(gvDepartamentosAsignacionArea.SelectedDataKey.Value.ToString());

					VmTareas.ObtenerAreasDepartamento(UidDepartamento);
					if (VmTareas.LsAreas.Count > 0)
					{
						gvDepartamentosAsignacionArea.Visible = false;
						gvAreasDisponiblesDepartamento.Visible = true;
						gvAreasDisponiblesDepartamento.DataSource = VmTareas.LsAreas;
						gvAreasDisponiblesDepartamento.DataBind();
						btnMostrarDepartamentosAsignacion.Visible = true;
					}
					else
					{
					}
				}
			}
			catch (Exception ex)
			{
				throw;
			}
		}

		protected void gvAreasDisponiblesDepartamento_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvAreasDisponiblesDepartamento, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvAreasDisponiblesDepartamento_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (hfUidTarea.Value == string.Empty)
				{
					Guid UidArea = new Guid(gvAreasDisponiblesDepartamento.SelectedDataKey.Value.ToString());

					VmTareas.ObtenerArea(UidArea);

					List<Modelo.Area> LsAreasAsignadas = (List<Area>)ViewState["AreasAsignadas"];
					if (LsAreasAsignadas.Count > 0)
					{
						int index = LsAreasAsignadas.FindIndex(x => x.UidArea == UidArea);
						if (index == -1)
						{
							LsAreasAsignadas.Add(new Area()
							{
								UidArea = VmTareas.AREA.UidArea,
								StrNombre = VmTareas.AREA.StrNombre,
								StrDescripcion = VmTareas.AREA.StrDescripcion
							});
						}
					}
					else
					{
						LsAreasAsignadas.Add(new Area()
						{
							UidArea = VmTareas.AREA.UidArea,
							StrNombre = VmTareas.AREA.StrNombre,
							StrDescripcion = VmTareas.AREA.StrDescripcion
						});
					}
					ViewState["AreasAsignadas"] = LsAreasAsignadas;
					gvAreasAsignadas.DataSource = LsAreasAsignadas;
					gvAreasAsignadas.DataBind();
				}

			}
			catch (Exception)
			{

			}
		}

		/// <summary>
		/// Antecesor
		/// </summary>
		protected void gvListaTareasAntecesor_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvListaTareasAntecesor, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvListaTareasAntecesor_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidTarea = new Guid(gvListaTareasAntecesor.SelectedDataKey.Value.ToString());
				hfAntecesorSeleccionado.Value = UidTarea.ToString();
				VmTareas.ObtenerDatosAntecesor(UidTarea);

				txtNombreAntecesor.Text = VmTareas.TAREA.StrNombre;
				txtNombreAntecesor.Enabled = false;
				gvListaTareasAntecesor.Visible = false;
			}
			catch (Exception ex)
			{

			}
		}
		#endregion
		#endregion
		#region Aux
		protected string GetListBoxValueSelected(ListBox Lb)
		{
			string Values = string.Empty;
			foreach (ListItem item in Lb.Items)
			{
				if (item.Selected)
				{
					if (Values == string.Empty)
						Values = item.Value;
					else
						Values += "," + item.Value;
				}
			}
			return Values;
		}
		protected void MostrarHora(bool Status)
		{
			if (Status)
			{
				pnlHabilitarHoraTarea.Visible = true;
			}
			else
			{
				pnlHabilitarHoraTarea.Visible = false;
			}
		}

		///<summary>
		///Habilitar Controles
		///</summary>
		private void HabilitarCamposDatosGenerales(bool Status)
		{
			txtNombreTarea.Enabled = Status;
			txtFechaInicioTarea.Enabled = Status;
			txtFechaFinTarea.Enabled = Status;
			txtDescripcionTarea.Enabled = Status;
			ddlEstatusTarea.Enabled = Status;
			ddlTareaRequerida.Enabled = Status;
			ddlTipoMedicionTarea.Enabled = Status;
			ddlUnidadMedida.Enabled = Status;
			CbFoto.Enabled = Status;
			cbHabilitarHoraTarea.Enabled = Status;
		}
		private void HabilitarCamposOpcionTarea(bool Status)
		{
			txtNombreOpcionTarea.Enabled = Status;
			txtOrdenOpcionTarea.Enabled = Status;
			cbActivoOpcionTarea.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidad(bool Status)
		{
			chkSinPeriodicida.Enabled = Status;
			chkPeriodicidadDiaria.Enabled = Status;
			chkPeriodicidadSemanal.Enabled = Status;
			chkPeriodicidadMensual.Enabled = Status;
			chkPeriodicidadAnual.Enabled = Status;
		}
		private void HabilitarCamposAsignacion(bool Status)
		{
			rbAsignarADepartamento.Enabled = Status;
			rbAsignarAArea.Enabled = Status;
			txtNombreDepartamentoAsignacion.Enabled = Status;

			if (Status)
				btnBuscarDepartamentoAsignacion.CssClass = "btn btn-sm btn-default";
			else
				btnBuscarDepartamentoAsignacion.CssClass = "btn btn-sm btn-default disabled";
		}
		private void HabilitarCamposAntecesor(bool Status)
		{
			txtNombreAntecesor.Enabled = Status;
			btnBuscarAntecesor.Enabled = Status;
			btnCancelarBuscarAntecesor.Enabled = Status;

			txtDiasDespues.Enabled = Status;
			cbUsarNotificacionAntecesor.Enabled = Status; ;
			rdNumeroLimitado.Enabled = Status; ;
			txtMaximo.Enabled = Status;
			rdIlimitado.Enabled = Status;
		}
		private void HabilitarCamposNotificacion(bool Status)
		{
			rbVerdaderoNotificacion.Enabled = Status;
			rbFalsoNotificacion.Enabled = Status;

			rbMayor.Enabled = Status;
			rbMayorIgual.Enabled = Status;
			txtMayorQue.Enabled = Status;

			rbMenor.Enabled = Status;
			rbMenorIgual.Enabled = Status;
			txtMenorQue.Enabled = Status;

			lbOpcionesSeleccionadasNotificacion.Enabled = Status;
		}

		///<summary>
		///Limpiar Controles
		///</summary>
		private void LimpiarCamposDatosGenerales()
		{
			txtNombreTarea.Text = string.Empty;
			txtFechaInicioTarea.Text = string.Empty;
			txtFechaFinTarea.Text = string.Empty;
			txtDescripcionTarea.Text = string.Empty;
			if (ddlEstatusTarea.Items.Count > 0)
				ddlEstatusTarea.SelectedIndex = 0;
			if (ddlTareaRequerida.Items.Count > 0)
				ddlTareaRequerida.SelectedIndex = 0;
			if (ddlTipoMedicionTarea.Items.Count > 0)
				ddlTipoMedicionTarea.SelectedIndex = 0;
			if (ddlUnidadMedida.Items.Count > 0)
				ddlUnidadMedida.SelectedIndex = 0;
			CbFoto.Checked = false;
			cbHabilitarHoraTarea.Checked = false;
		}
		private void LimpiarCamposOpcionTarea()
		{
			txtNombreOpcionTarea.Text = string.Empty;
			txtOrdenOpcionTarea.Text = string.Empty;
			cbActivoOpcionTarea.Checked = false;
		}

		private void LimpiarCamposPeriodicidadDiaria()
		{
			chkPDOpcion1.Checked = false;
			txtPDNumeroDias.Text = "1";
			chkPDOpcion2.Checked = true;
		}
		private void LimpiarCamposPeriodicidadSemanal()
		{
			txtPSNumeroSemanas.Text = "1";
			cblPSListaDias.ClearSelection();
		}
		private void LimpiarCamposPeriodicidadMensual()
		{
			chkPMRepetirCadaNDiasCadaNMes.Checked = true;
			txtPMDiaDelMes.Text = "1";
			txtPMNumeroDeMesesOpc1.Text = "1";

			chkPMRepetirCadaNMESConParametros.Checked = false;
			ddlPMListaNumerales.SelectedIndex = 0;
			ddlPMDiaSemanaOLapsoDias.SelectedIndex = 0;
			txtPMNumeroDeMesesOpc2.Text = "";

			chkPMTipoC.Checked = false;
			ddlPMNumeralesOpc3.SelectedIndex = 0;
			txtPMDiasOpc3.Text = "2";
			txtPMMesesOpc3.Text = "1";
		}
		private void LimpiarCamposPeriodicidadAnual()
		{
			txtPANumeroAnios.Text = "1";
			chkPAOpcion1.Checked = true;
			ddlPAListaMesesOpc1.SelectedIndex = 0;
			txtPADiaDelMesOpc1.Text = "1";

			chkPAOpcion2.Checked = false;
			ddlPAListaNumerales.SelectedIndex = 0;
			ddlPAListaPeriodosyDias.SelectedIndex = 0;
			ddlPAListaMesesOpc2.SelectedIndex = 0;
		}
		private void LimpiarCamposAntecesor()
		{
			txtNombreAntecesor.Text = string.Empty;

			txtDiasDespues.Text = string.Empty;
			cbUsarNotificacionAntecesor.Checked = false;
			rdNumeroLimitado.Checked = false;
			txtMaximo.Text = string.Empty;
			rdIlimitado.Checked = false;
		}
		private void LimpiarCamposNotificacion()
		{
			rbVerdaderoNotificacion.Checked = false;
			rbFalsoNotificacion.Checked = false;

			rbMayor.Checked = false;
			rbMayorIgual.Checked = false;
			txtMayorQue.Text = string.Empty;

			rbMenor.Checked = false;
			rbMenorIgual.Checked = false;
			txtMenorQue.Text = string.Empty;

			lbOpcionesSeleccionadasNotificacion.ClearSelection();
		}

		private void HabilitarCamposPeriodicidadDiaria(bool Status)
		{
			chkPDOpcion1.Enabled = Status;
			txtPDNumeroDias.Enabled = Status;
			chkPDOpcion2.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidadSemanal(bool Status)
		{
			txtPSNumeroSemanas.Enabled = Status;
			cblPSListaDias.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidadMensual(bool Status)
		{
			chkPMRepetirCadaNDiasCadaNMes.Enabled = Status;
			txtPMDiaDelMes.Enabled = Status;
			txtPMNumeroDeMesesOpc1.Enabled = Status;

			chkPMRepetirCadaNMESConParametros.Enabled = Status;
			ddlPMListaNumerales.Enabled = Status;
			ddlPMDiaSemanaOLapsoDias.Enabled = Status;
			txtPMNumeroDeMesesOpc2.Enabled = Status;

			chkPMTipoC.Enabled = Status;
			ddlPMNumeralesOpc3.Enabled = Status;
			txtPMDiasOpc3.Enabled = Status;
			txtPMMesesOpc3.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidadAnual(bool Status)
		{
			txtPANumeroAnios.Enabled = Status;
			chkPAOpcion1.Enabled = Status;
			ddlPAListaMesesOpc1.Enabled = Status;
			txtPADiaDelMesOpc1.Enabled = Status;

			chkPAOpcion2.Enabled = Status;
			ddlPAListaNumerales.Enabled = Status;
			ddlPAListaPeriodosyDias.Enabled = Status;
			ddlPAListaMesesOpc2.Enabled = Status;
		}

		///<summary>
		///Validar campos 
		///</summary>
		public bool IsNumberValue(string Value)
		{
			bool Result = false;
			int Aux = 0;
			if (int.TryParse(Value, out Aux))
			{
				Result = true;
			}
			return Result;
		}

		///<summary>
		/// Habilitar Controler de Acuerdo al Tipo de medicion
		///</summary>
		private void SetFieldsNotificacion(string TipoMedicion)
		{
			switch (TipoMedicion)
			{
				case "Numerico":
					pnlNotificacionVerdaderoFalso.Visible = false;
					pnlNotificacionValor.Visible = true;
					pnlNotificacionOpcionMultiple.Visible = false;
					break;
				case "Seleccionable":
					pnlNotificacionVerdaderoFalso.Visible = false;
					pnlNotificacionValor.Visible = false;
					pnlNotificacionOpcionMultiple.Visible = true;
					List<TareaOpcion> LsOpcionesTarea = (List<TareaOpcion>)ViewState["OpcionesTarea"];
					lbOpcionesSeleccionadasNotificacion.DataSource = LsOpcionesTarea;
					lbOpcionesSeleccionadasNotificacion.DataValueField = "UidOpciones";
					lbOpcionesSeleccionadasNotificacion.DataTextField = "StrOpciones";
					lbOpcionesSeleccionadasNotificacion.DataBind();
					break;
				case "Verdadero/Falso":
					pnlNotificacionVerdaderoFalso.Visible = true;
					pnlNotificacionValor.Visible = false;
					pnlNotificacionOpcionMultiple.Visible = false;
					rbFalsoNotificacion.Checked = false;
					rbVerdaderoNotificacion.Checked = false;
					break;
				default:
					pnlNotificacionVerdaderoFalso.Visible = false;
					pnlNotificacionValor.Visible = false;
					pnlNotificacionOpcionMultiple.Visible = false;
					break;
			}
		}

		private void DisplayMessagePeriodicity(string Message, string MessageType)
		{
			lblMessagePeriodicity.Text = Message;
			pnlMessagePeriodicity.CssClass = "alert alert-" + MessageType;
			pnlMessagePeriodicity.Visible = true;
		}
		protected void HideMessagePeriodicity(object sender, EventArgs e)
		{
			pnlMessagePeriodicity.Visible = false;
		}

		#endregion

		#endregion
	}
}