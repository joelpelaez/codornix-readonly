﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;
using System.Globalization;

namespace CodorniX.Vista
{
	public partial class NotificacionesEmpresa : System.Web.UI.Page
	{
		#region Properties
		private Sesion SesionActual
		{
			get
			{
				return (Sesion)Session["Sesion"];
			}
		}
		protected VMNotificacionEmpresa VmNotificacionEmpresa = new VMNotificacionEmpresa();
		protected string[] FiltrosBusqueda
		{
			get
			{
				return ViewState["FBusqueda"] as string[];
			}
			set
			{
				ViewState["FBusqueda"] = value;
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (!Acceso.TieneAccesoAModulo("NotificacionesSucursales", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				pnlAlertBusqueda.Visible = false;
				pnlAlertGeneral.Visible = false;

				gvNotificaciones.DataSource = null;
				gvNotificaciones.DataBind();

				pnlFiltrosBusqueda.Visible = true;
				pnlListaNotificaciones.Visible = false;
				lblOcultarFiltros.Text = "Ocultar";

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";

				if (SesionActual.uidEmpresaActual == null)
				{
					pnlAlertGeneral.Visible = true;
					lblErrorGeneral.Text = "Seleccione una empresa";
					return;
				}

				VmNotificacionEmpresa.GetEstadosNotificacion();
				ddlFiltroEstadoNotificacion.DataSource = VmNotificacionEmpresa.LsEstadoNotificacion;
				ddlFiltroEstadoNotificacion.DataTextField = "StrEstadoNotificacion";
				ddlFiltroEstadoNotificacion.DataValueField = "UidEstadoNotitifacion";
				ddlFiltroEstadoNotificacion.DataBind();

				VmNotificacionEmpresa.GetSucursalesEmpresa(SesionActual.uidEmpresaActual.Value);
				ddlFiltroSucursal.DataSource = VmNotificacionEmpresa.LsSucursales;
				ddlFiltroSucursal.DataTextField = "StrNombre";
				ddlFiltroSucursal.DataValueField = "UidSucursal";
				ddlFiltroSucursal.DataBind();

				VmNotificacionEmpresa.GetDepartamentosSucursal(Guid.Empty);
				ddlFiltroDepartamento.DataSource = VmNotificacionEmpresa.LsDepartamento;
				ddlFiltroDepartamento.DataTextField = "StrNombre";
				ddlFiltroDepartamento.DataValueField = "UidDepartamento";
				ddlFiltroDepartamento.DataBind();

				VmNotificacionEmpresa.GetAreasDepartamento(Guid.Empty);
				ddlFiltroArea.DataSource = VmNotificacionEmpresa.LsArea;
				ddlFiltroArea.DataTextField = "StrNombre";
				ddlFiltroArea.DataValueField = "UidArea";
				ddlFiltroArea.DataBind();

				string EmptyGuid = Guid.Empty.ToString();
				this.FiltrosBusqueda = new string[6];
				FiltrosBusqueda[0] = EmptyGuid;
				FiltrosBusqueda[1] = string.Empty;
				FiltrosBusqueda[2] = string.Empty;
				FiltrosBusqueda[3] = EmptyGuid;
				FiltrosBusqueda[4] = EmptyGuid;
				FiltrosBusqueda[5] = EmptyGuid;

				pnlNotificacionOpcionMultiple.Visible = false;
				pnlNotificacionValor.Visible = false;
				pnlNotificacionVerdaderoFalso.Visible = false;
			}
		}

		#region Methods
		#region Panel Izquierdo
		#region Buttons
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			if (SesionActual.uidEmpresaActual == null)
			{
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Seleccione una empresa";
				return;
			}

			Guid UidEmpresa = SesionActual.uidEmpresaActual.Value;
			Guid UidEstadNotificacion = Guid.Empty;
			DateTime? DtFechaInicio = null;
			DateTime? DtFechaFin = null;
			Guid UidSucursal = Guid.Empty;
			Guid UidDepartamento = Guid.Empty;
			Guid UidArea = Guid.Empty;

			if (!ddlFiltroEstadoNotificacion.SelectedValue.Equals(Guid.Empty.ToString()))
				UidEstadNotificacion = new Guid(ddlFiltroEstadoNotificacion.SelectedValue);

			if (!string.IsNullOrEmpty(txtFiltroFechaInicio.Text.Trim()))
			{
				try
				{
					DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					pnlAlertBusqueda.Visible = true;
					lblErrorBusqueda.Text = "Formato de fecha de inicio incorrecto.";
					return;
				}
			}

			if (!string.IsNullOrEmpty(txtFiltroFechaFin.Text.Trim()))
			{
				try
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					pnlAlertBusqueda.Visible = true;
					lblErrorBusqueda.Text = "Formato de fecha fin incorrecto.";
					return;
				}
			}

			if (!ddlFiltroSucursal.SelectedValue.Equals(Guid.Empty.ToString()))
				UidSucursal = new Guid(ddlFiltroSucursal.SelectedValue);

			if (!ddlFiltroDepartamento.SelectedValue.Equals(Guid.Empty.ToString()))
				UidDepartamento = new Guid(ddlFiltroDepartamento.SelectedValue);

			if (!ddlFiltroArea.SelectedValue.Equals(Guid.Empty.ToString()))
				UidArea = new Guid(ddlFiltroArea.SelectedValue);

			if (this.FiltrosBusqueda == null)
				this.FiltrosBusqueda = new string[6];

			FiltrosBusqueda[0] = UidEstadNotificacion.ToString();
			FiltrosBusqueda[1] = DtFechaInicio == null ? string.Empty : DtFechaInicio.Value.ToString("dd/MM/yyyy");
			FiltrosBusqueda[2] = DtFechaFin == null ? string.Empty : DtFechaFin.Value.ToString("dd/MM/yyyy");
			FiltrosBusqueda[3] = UidSucursal.ToString();
			FiltrosBusqueda[4] = UidDepartamento.ToString();
			FiltrosBusqueda[5] = UidArea.ToString();

			this.VmNotificacionEmpresa.Search(UidEmpresa, DtFechaInicio, DtFechaFin, UidEstadNotificacion, UidSucursal, UidDepartamento, UidArea);
			gvNotificaciones.DataSource = VmNotificacionEmpresa.LsNotificaciones;
			gvNotificaciones.DataBind();

			lblOcultarFiltros.Text = "Mostrar";
			pnlFiltrosBusqueda.Visible = false;
			pnlListaNotificaciones.Visible = true;

			btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
			btnBuscar.CssClass = "btn btn-sm btn-default disabled";
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroFechaInicio.Text = string.Empty;
			txtFiltroFechaFin.Text = string.Empty;
			ddlFiltroEstadoNotificacion.SelectedIndex = 0;
			ddlFiltroSucursal.SelectedIndex = 0;
			ddlFiltroDepartamento.SelectedIndex = 0;
			ddlFiltroArea.SelectedIndex = 0;
		}
		protected void btnOcultarFiltros_Click(object sender, EventArgs e)
		{
			if (lblOcultarFiltros.Text.Equals("Ocultar"))
			{
				lblOcultarFiltros.Text = "Mostrar";
				pnlFiltrosBusqueda.Visible = false;
				pnlListaNotificaciones.Visible = true;

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
			}
			else if (lblOcultarFiltros.Text.Equals("Mostrar"))
			{
				lblOcultarFiltros.Text = "Ocultar";
				pnlFiltrosBusqueda.Visible = true;
				pnlListaNotificaciones.Visible = false;

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";
			}
		}
		protected void CloseAlertBusqueda_Click(object sender, EventArgs e)
		{
			pnlAlertBusqueda.Visible = false;
		}
		#endregion
		#region GridView
		protected void gvNotificaciones_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvNotificaciones, "Select$" + e.Row.RowIndex);

				Label Icon = e.Row.FindControl("lblIcon") as Label;
				if (e.Row.Cells[6].Text.Equals("Leido"))
				{
					Icon.CssClass = "glyphicon glyphicon-ok";
					Icon.ToolTip = "Leido";
				}
				else if (e.Row.Cells[6].Text.Equals("No leido"))
				{
					e.Row.CssClass = "font-bold bg-low-grey";
					Icon.CssClass = "glyphicon glyphicon-remove";
					Icon.ToolTip = "No Leido";
				}
			}
		}
		protected void gvNotificaciones_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertGeneral.Visible)
					pnlAlertGeneral.Visible = false;

				pnlNotificacionOpcionMultiple.Visible = false;
				pnlNotificacionValor.Visible = false;
				pnlNotificacionVerdaderoFalso.Visible = false;
				Guid UidNotificacion = new Guid(gvNotificaciones.SelectedDataKey.Value.ToString());
				hfUidMensajeNotificacion.Value = UidNotificacion.ToString();
				VmNotificacionEmpresa.BuscarPorId(UidNotificacion);

				string ValorIngresado = VmNotificacionEmpresa.NotificacionEmpresa.ValorIngresado;
				VmNotificacionEmpresa.GetParametrosNotificacionTarea(VmNotificacionEmpresa.NotificacionEmpresa.UidTarea);

				if (VmNotificacionEmpresa.NotificacionEmpresa.TipoMedicion.Equals("Verdadero/Falso"))
				{
					ValorIngresado = VmNotificacionEmpresa.NotificacionEmpresa.ValorIngresado.Equals("1") ? "Verdadero" : "Falso";

					if (VmNotificacionEmpresa.nNotificacion != null)
					{
						pnlNotificacionVerdaderoFalso.Visible = true;
						lblNotificacionVF.Text = VmNotificacionEmpresa.nNotificacion.BlValor.Value ? "Verdadero" : "Falso";
					}
				}
				else if (VmNotificacionEmpresa.NotificacionEmpresa.TipoMedicion.Equals("Numerico"))
				{
					ValorIngresado = VmNotificacionEmpresa.NotificacionEmpresa.ValorIngresado.ToString();
					if (VmNotificacionEmpresa.nNotificacion != null)
					{
						pnlNotificacionValor.Visible = true;
						bool IsComa = false;
						if (VmNotificacionEmpresa.nNotificacion.DcMenorQue != null)
						{
							if (IsComa)
								lblNotificacionValor.Text = ", Menor que " + VmNotificacionEmpresa.nNotificacion.DcMenorQue.Value;
							else
								lblNotificacionValor.Text = "Menor que " + VmNotificacionEmpresa.nNotificacion.DcMenorQue.Value;

							IsComa = true;
						}
						else if (VmNotificacionEmpresa.nNotificacion.DcMayorQue != null)
						{
							if (IsComa)
								lblNotificacionValor.Text = ", Mayor que " + VmNotificacionEmpresa.nNotificacion.DcMayorQue.Value;
							else
								lblNotificacionValor.Text = "Mayor que " + VmNotificacionEmpresa.nNotificacion.DcMayorQue.Value;

							IsComa = true;
						}
						else if (VmNotificacionEmpresa.nNotificacion.BlMenorIgual != null)
						{
							if (VmNotificacionEmpresa.nNotificacion.BlMenorIgual.Value)
							{
								if (IsComa)
									lblNotificacionValor.Text = ", Menor o igual que " + VmNotificacionEmpresa.nNotificacion.DcMenorQue.Value;
								else
									lblNotificacionValor.Text = "Menor o igual que " + VmNotificacionEmpresa.nNotificacion.DcMenorQue.Value;
								IsComa = true;
							}
						}

						else if (VmNotificacionEmpresa.nNotificacion.BlMayorIgual != null)
						{
							if (VmNotificacionEmpresa.nNotificacion.BlMayorIgual.Value)
							{
								if (IsComa)
									lblNotificacionValor.Text = ", Mayor o igual que " + VmNotificacionEmpresa.nNotificacion.DcMayorQue.Value;
								else
									lblNotificacionValor.Text = "Mayor o igual que " + VmNotificacionEmpresa.nNotificacion.DcMayorQue.Value;
								IsComa = true;
							}
						}
					}
				}
				else if (VmNotificacionEmpresa.NotificacionEmpresa.TipoMedicion.Equals("Seleccionable"))
				{
					ValorIngresado = VmNotificacionEmpresa.NotificacionEmpresa.ValorIngresado.ToString();
					if (VmNotificacionEmpresa.nNotificacion != null)
					{
						pnlNotificacionOpcionMultiple.Visible = true;
						VmNotificacionEmpresa.GetOpcionesTarea(VmNotificacionEmpresa.NotificacionEmpresa.UidTarea);
						lbNotificacionOpcionMultiple.DataSource = VmNotificacionEmpresa.LsOpcionesTarea;
						lbNotificacionOpcionMultiple.DataTextField = "StrOpciones";
						lbNotificacionOpcionMultiple.DataValueField = "UidOpciones";
						lbNotificacionOpcionMultiple.DataBind();
					}
				}

				lblSucursal.Text = VmNotificacionEmpresa.NotificacionEmpresa.Sucursal;
				lblDepartamento.Text = VmNotificacionEmpresa.NotificacionEmpresa.Departamento;
				lblArea.Text = VmNotificacionEmpresa.NotificacionEmpresa.Area;
				lblTarea.Text = VmNotificacionEmpresa.NotificacionEmpresa.Tarea;
				lblTipoTarea.Text = VmNotificacionEmpresa.NotificacionEmpresa.TipoTarea;
				lblValorIngresado.Text = ValorIngresado;
				lblObservaciones.Text = VmNotificacionEmpresa.NotificacionEmpresa.Observaciones;

				lblCambiarEstado.Text = VmNotificacionEmpresa.NotificacionEmpresa.EstadoNotificacion.Equals("Leido") ? "No leido " : "Leido";
			}
			catch (Exception)
			{
				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Error al obtener datos.";
			}
		}
		protected void gvNotificaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			string EmptyGuid = Guid.Empty.ToString();
			Guid UidEmpresa = SesionActual.uidEmpresaActual.Value;
			Guid UidEstadNotificacion = Guid.Empty;
			DateTime? DtFechaInicio = null;
			DateTime? DtFechaFin = null;
			Guid UidSucursal = Guid.Empty;
			Guid UidDepartamento = Guid.Empty;
			Guid UidArea = Guid.Empty;

			if (!FiltrosBusqueda[0].Equals(Guid.Empty.ToString()))
				UidEstadNotificacion = new Guid(FiltrosBusqueda[0]);

			if (!FiltrosBusqueda[1].Equals(string.Empty))
				DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(FiltrosBusqueda[1], "dd/MM/yyyy", CultureInfo.InvariantCulture));

			if (!FiltrosBusqueda[2].Equals(string.Empty))
				DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(FiltrosBusqueda[2], "dd/MM/yyyy", CultureInfo.InvariantCulture));

			if (!FiltrosBusqueda[3].Equals(Guid.Empty.ToString()))
				UidSucursal = new Guid(FiltrosBusqueda[3]);

			if (!FiltrosBusqueda[4].Equals(Guid.Empty.ToString()))
				UidDepartamento = new Guid(FiltrosBusqueda[4]);

			if (!FiltrosBusqueda[5].Equals(Guid.Empty.ToString()))
				UidArea = new Guid(FiltrosBusqueda[5]);

			this.VmNotificacionEmpresa.Search(UidEmpresa, DtFechaInicio, DtFechaFin, UidEstadNotificacion, UidSucursal, UidDepartamento, UidArea);
			gvNotificaciones.SelectedIndex = -1;
			gvNotificaciones.DataSource = VmNotificacionEmpresa.LsNotificaciones;
			gvNotificaciones.PageIndex = e.NewPageIndex;
			gvNotificaciones.DataBind();
		}
		#endregion
		#endregion

		#region Panel Derecho		
		#region Buttons
		protected void btnCambiarEstado_Click(object sender, EventArgs e)
		{
			try
			{
				Guid Uid = new Guid(hfUidMensajeNotificacion.Value);
				string Estatus = lblCambiarEstado.Text;
				if (VmNotificacionEmpresa.CambiarEstatus(Uid, Estatus))
				{
					lblCambiarEstado.Text = lblCambiarEstado.Text.Equals("Leido") ? "No leido" : "Leido";

					string EmptyGuid = Guid.Empty.ToString();
					Guid UidEmpresa = SesionActual.uidEmpresaActual.Value;
					Guid UidEstadNotificacion = Guid.Empty;
					DateTime? DtFechaInicio = null;
					DateTime? DtFechaFin = null;
					Guid UidSucursal = Guid.Empty;
					Guid UidDepartamento = Guid.Empty;
					Guid UidArea = Guid.Empty;

					if (!FiltrosBusqueda[0].Equals(Guid.Empty.ToString()))
						UidEstadNotificacion = new Guid(FiltrosBusqueda[0]);

					if (!FiltrosBusqueda[1].Equals(string.Empty))
						DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(FiltrosBusqueda[1], "dd/MM/yyyy", CultureInfo.InvariantCulture));

					if (!FiltrosBusqueda[2].Equals(string.Empty))
						DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(FiltrosBusqueda[2], "dd/MM/yyyy", CultureInfo.InvariantCulture));

					if (!FiltrosBusqueda[3].Equals(Guid.Empty.ToString()))
						UidSucursal = new Guid(FiltrosBusqueda[3]);

					if (!FiltrosBusqueda[4].Equals(Guid.Empty.ToString()))
						UidDepartamento = new Guid(FiltrosBusqueda[4]);

					if (!FiltrosBusqueda[5].Equals(Guid.Empty.ToString()))
						UidArea = new Guid(FiltrosBusqueda[5]);

					this.VmNotificacionEmpresa.Search(UidEmpresa, DtFechaInicio, DtFechaFin, UidEstadNotificacion, UidSucursal, UidDepartamento, UidArea);
					gvNotificaciones.DataSource = VmNotificacionEmpresa.LsNotificaciones;
					gvNotificaciones.DataBind();
				}

			}
			catch (Exception)
			{

				pnlAlertGeneral.Visible = true;
				lblErrorGeneral.Text = "Ocurrio un error al actualizar estado";
			}
		}
		protected void CloseAlertGeneral_Click(object sender, EventArgs e)
		{
			pnlAlertGeneral.Visible = false;
		}
		#endregion
		#region Fields
		private void CleanFieldsNotificacion()
		{

		}
		#endregion
		#region DropDownList
		protected void ddlFiltroSucursal_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid UidSucursal = new Guid(ddlFiltroSucursal.SelectedValue);
			VmNotificacionEmpresa.GetDepartamentosSucursal(UidSucursal);
			ddlFiltroDepartamento.DataSource = VmNotificacionEmpresa.LsDepartamento;
			ddlFiltroDepartamento.DataTextField = "StrNombre";
			ddlFiltroDepartamento.DataValueField = "UidDepartamento";
			ddlFiltroDepartamento.DataBind();

			VmNotificacionEmpresa.GetAreasDepartamento(Guid.Empty);
			ddlFiltroArea.DataSource = VmNotificacionEmpresa.LsArea;
			ddlFiltroArea.DataTextField = "StrNombre";
			ddlFiltroArea.DataValueField = "UidArea";
			ddlFiltroArea.DataBind();
		}

		protected void ddlFiltroDepartamento_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid UidDepartamento = new Guid(ddlFiltroDepartamento.SelectedValue);
			VmNotificacionEmpresa.GetAreasDepartamento(UidDepartamento);
			ddlFiltroArea.DataSource = VmNotificacionEmpresa.LsArea;
			ddlFiltroArea.DataTextField = "StrNombre";
			ddlFiltroArea.DataValueField = "UidArea";
			ddlFiltroArea.DataBind();
		}
		#endregion
		#endregion

		#endregion
	}
}