﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.Util;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class Tareas : System.Web.UI.Page
	{
		#region Properties
		/// <summary>
		/// Vista del modelo
		/// </summary>
		VMTareas VmTarea = new VMTareas();

		/// <summary>
		/// Lista de tareas
		/// </summary>	
		private List<Modelo.Tarea> LsTareas
		{
			get
			{
				return (List<Modelo.Tarea>)ViewState["ListaTareas"];
			}
			set
			{
				ViewState["ListaTareas"] = value;
			}
		}
		/// <summary>
		/// Lista de tareas
		/// </summary>	
		private List<Modelo.Tarea> LsOrdenamientoTareas
		{
			get
			{
				return (List<Modelo.Tarea>)ViewState["ListaOrdenamientoTareas"];
			}
			set
			{
				ViewState["ListaOrdenamientoTareas"] = value;
			}
		}

		/// <summary>
		/// Lista de departamentos asignados a la tarea
		/// </summary>
		private List<Modelo.Departamento> LsDepartamentosAsignados
		{
			get
			{
				return (List<Modelo.Departamento>)ViewState["DepartamentosAsignados"];
			}
			set
			{
				ViewState["DepartamentosAsignados"] = value;
			}
		}
		/// <summary>
		/// Lista de areas asignadas a la tarea
		/// </summary>
		private List<Modelo.Area> LsAreasAsignadas
		{
			get
			{
				return (List<Modelo.Area>)ViewState["AreasAsignadas"];
			}
			set
			{
				ViewState["AreasAsignadas"] = value;
			}
		}
		private List<Modelo.TareaOpcion> LsOpcionesTarea
		{
			get
			{
				return (List<Modelo.TareaOpcion>)ViewState["OpcionesTarea"];
			}
			set
			{
				ViewState["OpcionesTarea"] = value;
			}
		}
		private List<Modelo.FechaPeriodicidad> LsFechasPeriodicidad
		{
			get
			{
				return (List<Modelo.FechaPeriodicidad>)ViewState["FechasPeriodicidad"];
			}
			set
			{
				ViewState["FechasPeriodicidad"] = value;
			}
		}

		/// <summary>
		/// Variables de filtros de busqueda de las tareas
		/// </summary>
		private int FiltroFolioTarea
		{
			get
			{
				return (int)ViewState["FiltroFolioT"];
			}
			set
			{
				ViewState["FiltroFolioT"] = value;
			}
		}
		private string FiltroNombreTarea
		{
			get
			{
				return (string)ViewState["FiltroNombreT"];
			}
			set
			{
				ViewState["FiltroNombreT"] = value;
			}
		}
		private DateTime? DtFiltroFechaInicio
		{
			get
			{
				return (DateTime?)ViewState["FiltroFechaIT"];
			}
			set
			{
				ViewState["FiltroFechaIT"] = value;
			}
		}
		private DateTime? DtFiltroFechaFin
		{
			get
			{
				return (DateTime?)ViewState["FiltroFechaFT"];
			}
			set
			{
				ViewState["FiltroFechaFT"] = value;
			}
		}
		private string FiltroDepartamentos
		{
			get
			{
				return (string)ViewState["FiltroDepartamentosT"];
			}
			set
			{
				ViewState["FiltroDepartamentosT"] = value;
			}
		}
		private string FiltroAreas
		{
			get
			{
				return (string)ViewState["FiltroAreasT"];
			}
			set
			{
				ViewState["FiltroAreasT"] = value;
			}
		}
		private string FiltroUnidadesMedida
		{
			get
			{
				return (string)ViewState["FiltroUnidadMT"];
			}
			set
			{
				ViewState["FiltroUnidadMT"] = value;
			}
		}
		private int FiltroFolioAntecesor
		{
			get
			{
				return (int)ViewState["FiltroFolioAntT"];
			}
			set
			{
				ViewState["FiltroFolioAntT"] = value;
			}
		}
		private int TotalCumplimientosTarea
		{
			get
			{
				return (int)ViewState["TCumplimientosTarea"];
			}
			set
			{
				ViewState["TCumplimientosTarea"] = value;
			}
		}
		/// <summary>
		/// Variable de sesion que almacena los datos del usuario 
		/// </summary>
		private Modelo.Sesion SesionActual
		{
			get
			{
				return (Modelo.Sesion)Session["Sesion"];
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack)
			{
				this.LsFechasPeriodicidad = new List<Modelo.FechaPeriodicidad>();
				this.LsOrdenamientoTareas = new List<Modelo.Tarea>();
				this.LsTareas = new List<Modelo.Tarea>();
				btnEditarOrdenamiento.Visible = false;
				btnOkOrdenamiento.Visible = false;
				btnCancelOrdenamiento.Visible = false;

				// Inicializar controles
				btnEditar.CssClass = "btn btn-sm btn-default disabled";
				btnOk.Visible = false;
				btnCancelar.Visible = false;
				btnHabilitarTarea.CssClass = "btn btn-sm btn-default disabled";
				btnHabilitarTarea.Enabled = false;

				HabilitarCamposDatosGenerales(false);

				HabilitarCamposOpcionTarea(false);
				btnNuevaOpcion.CssClass = "btn btn-sm btn-default disabled";
				btnEditarOpcion.CssClass = "btn btn-sm btn-default disabled";
				btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";
				btnOkOpcion.Visible = false;
				btnCancelarOpcion.Visible = false;

				HabilitarCamposAsignacion(false);
				rbAsignarADepartamento.Checked = true;
				rbAsignarADepartamento_CheckedChanged(null, null);
				btnCancelarBuscarDepartamentoAsignacion.Visible = false;
				//pnlAsignacionAreas.Visible = false;
				//pnlAsignacionDepartamentos.Visible = false;

				HabilitarCamposAntecesor(false);
				cbActivarAntecesor.Enabled = false;
				pnlCamposAntecesor.Visible = false;

				HabilitarCamposPeriodicidad(false);
				rbSinPeriodicidad.Checked = true;
				DisplayPeriodicityFields("None");

				HabilitarCamposNotificacion(false);
				cbHabilitarNotificacionTarea.Enabled = false;
				pnlNotificationSendTo.Visible = false;
				pnlNotificacionOpcionMultiple.Visible = false;
				pnlNotificacionValor.Visible = false;
				pnlNotificacionVerdaderoFalso.Visible = false;

				this.Natigate(true, false, false, false, false, false);

				//Inicializar variables de busqueda
				FiltroFolioTarea = 0;
				FiltroNombreTarea = string.Empty;
				DtFiltroFechaInicio = null;
				DtFiltroFechaFin = null;
				FiltroDepartamentos = string.Empty;
				FiltroAreas = string.Empty;
				FiltroUnidadesMedida = string.Empty;
				FiltroFolioAntecesor = 0;

				// Inicializar lista de tareas
				gvTareas.DataSource = null;
				gvTareas.DataBind();
				// Inicializar lista de opciones de tarea
				gvOpcionesTarea.DataSource = null;
				gvOpcionesTarea.DataBind();
				// Inicializar lista de departamentos asignados
				gvDepartamentosAsignados.DataSource = null;
				gvDepartamentosAsignados.DataBind();
				// Inicializar lista de busqueda de departamentos
				gvDepartamentosAsignacionBusqueda.DataSource = null;
				gvDepartamentosAsignacionBusqueda.DataBind();
				// Inicializar lista de areas asignadas
				gvAreasAsignadas.DataSource = null;
				gvAreasAsignadas.DataBind();
				// Inicializar lista de busqueda de departamentos
				gvDepartamentosAsignacionArea.DataSource = null;
				gvDepartamentosAsignacionArea.DataBind();
				// Inicializar lista de busqueda de areas
				gvAreasDisponiblesDepartamento.DataSource = null;
				gvAreasDisponiblesDepartamento.DataBind();

				// Mostral los filtros
				ShowFilters("Mostrar");

				// Ocultar paneles de notificaciones
				HideMessageBusqueda(null, null);
				HideMessageGeneral(null, null);
				HideMessageOpcionTarea(null, null);
				HideMessagePeriodicity(null, null);

				ddlTipoMedicion_SelectedIndexChanged(null, null);

				// Cargar elementos 
				VmTarea.GetDepartamentosSucursal(SesionActual.uidSucursalActual.Value);
				lbFiltroDepartamento.DataSource = VmTarea.LsDepartamentos;
				lbFiltroDepartamento.DataValueField = "UidDepartamento";
				lbFiltroDepartamento.DataTextField = "StrNombre";
				lbFiltroDepartamento.DataBind();

				VmTarea.GetAreasSucursal(SesionActual.uidSucursalActual.Value);
				lbFiltroArea.DataSource = VmTarea.LsAreas;
				lbFiltroArea.DataValueField = "UidArea";
				lbFiltroArea.DataTextField = "StrNombre";
				lbFiltroArea.DataBind();

				VmTarea.GetUnidadesMedida(SesionActual.uidEmpresaActual.Value);
				lbFiltroUnidad.DataSource = VmTarea.LsUnidadesMedida;
				lbFiltroUnidad.DataValueField = "UidUnidadMedida";
				lbFiltroUnidad.DataTextField = "StrTipoUnidad";
				lbFiltroUnidad.DataBind();

				ddlUnidadMedida.DataSource = VmTarea.LsUnidadesMedida;
				ddlUnidadMedida.DataValueField = "UidUnidadMedida";
				ddlUnidadMedida.DataTextField = "StrTipoUnidad";
				ddlUnidadMedida.DataBind();

				VmTarea.GetEstadosTarea();
				ddlEstatusTarea.DataSource = VmTarea.LsEstatusTarea;
				ddlEstatusTarea.DataTextField = "strStatus";
				ddlEstatusTarea.DataValueField = "UidStatus";
				ddlEstatusTarea.DataBind();

				VmTarea.GetTiposTarea();
				ddlTipoTarea.DataSource = VmTarea.LsTiposTarea;
				ddlTipoTarea.DataTextField = "StrTipoTarea";
				ddlTipoTarea.DataValueField = "UidTipoTarea";
				ddlTipoTarea.DataBind();

				VmTarea.GetTipoMedicion();
				ddlTipoMedicion.DataSource = VmTarea.LsMediciones;
				ddlTipoMedicion.DataValueField = "UidTipoMedicion";
				ddlTipoMedicion.DataTextField = "StrTipoMedicion";
				ddlTipoMedicion.DataBind();

				VmTarea.GetDias();
				cblPSListaDias.DataSource = VmTarea.LsDias;
				cblPSListaDias.DataValueField = "UidDias";
				cblPSListaDias.DataTextField = "StrDias";
				cblPSListaDias.DataBind();
				ddlPMDiaSemanaOLapsoDias.DataSource = VmTarea.LsDias;
				ddlPMDiaSemanaOLapsoDias.DataValueField = "UidDias";
				ddlPMDiaSemanaOLapsoDias.DataTextField = "StrDias";
				ddlPMDiaSemanaOLapsoDias.DataBind();
				ddlPAListaPeriodosyDias.DataSource = VmTarea.LsDias;
				ddlPAListaPeriodosyDias.DataValueField = "UidDias";
				ddlPAListaPeriodosyDias.DataTextField = "StrDias";
				ddlPAListaPeriodosyDias.DataBind();

				VmTarea.GetMeses();
				ddlPAListaMesesOpc1.DataSource = VmTarea.LsMeses;
				ddlPAListaMesesOpc1.DataTextField = "StrMes";
				ddlPAListaMesesOpc1.DataValueField = "UidMes";
				ddlPAListaMesesOpc1.DataBind();
				ddlPAListaMesesOpc2.DataSource = VmTarea.LsMeses;
				ddlPAListaMesesOpc2.DataTextField = "StrMes";
				ddlPAListaMesesOpc2.DataValueField = "UidMes";
				ddlPAListaMesesOpc2.DataBind();

				VmTarea.GetOrdinales();
				ddlPMListaNumerales.DataSource = VmTarea.LsOrdinales;
				ddlPMListaNumerales.DataTextField = "StrOrdinal";
				ddlPMListaNumerales.DataValueField = "UidOrdinal";
				ddlPMListaNumerales.DataBind();
				ddlPAListaNumerales.DataSource = VmTarea.LsOrdinales;
				ddlPAListaNumerales.DataTextField = "StrOrdinal";
				ddlPAListaNumerales.DataValueField = "UidOrdinal";
				ddlPAListaNumerales.DataBind();
			}
		}

		#region Left
		#region Buttons
		protected void btnOrdenamiento_Click(object sender, EventArgs e)
		{
			btnOcultarMostrarFiltros.CssClass = "btn btn-sm btn-default disabled";
			btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
			btnBuscar.CssClass = "btn btn-sm btn-default disabled";

			pnlFiltrosBusqueda.Visible = false;
			pnlListaTareas.Visible = false;
			pnlOrdenamientoTareas.Visible = true;

			btnEditarOrdenamiento.CssClass = "btn btn-sm btn-default";
			btnEditarOrdenamiento.Visible = true;
			btnOkOrdenamiento.Visible = false;
			btnCancelOrdenamiento.Visible = true;

			try
			{
				VmTarea.Busqueda(SesionActual.uidSucursalActual.Value, 0, string.Empty, null, null, this.FiltroDepartamentos, this.FiltroAreas, string.Empty, 0);
				this.LsOrdenamientoTareas = VmTarea.LsTareas;
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error al obtener tareas: <br/>" + ex.Message, "danger");
				return;
			}

			int Position = 1;
			for (int i = 0; i < this.LsOrdenamientoTareas.Count; i++)
			{
				this.LsOrdenamientoTareas[i].IntOrden = LsOrdenamientoTareas[i].IntOrden == -1 ? LsOrdenamientoTareas[i].IntFolio : LsOrdenamientoTareas[i].IntOrden;
				Position++;
			}

			VmTarea.LsTareas = this.LsOrdenamientoTareas;
			VmTarea.SortOrdenamientoTareas("ASC", "Orden");
			this.LsOrdenamientoTareas = VmTarea.LsTareas;

			hfUidTareaOrdenamiento.Value = string.Empty;
			gvTareasOrdenamiento.PageIndex = 0;
			this.LsOrdenamientoTareas = this.LsOrdenamientoTareas.OrderBy(t => t.IntOrden).ToList();
			gvTareasOrdenamiento.DataSource = this.LsOrdenamientoTareas;
			gvTareasOrdenamiento.DataBind();
		}
		protected void btnEditarOrdenamiento_Click(object sender, EventArgs e)
		{
			btnOrdenamiento.CssClass = "btn btn-sm btn-default disabled";
			btnEditarOrdenamiento.CssClass = "btn btn-sm btn-default disabled";
			btnOkOrdenamiento.Visible = true;
			btnCancelOrdenamiento.Visible = true;
		}
		protected void btnOkOrdenamiento_Click(object sender, EventArgs e)
		{
			try
			{
				bool Estatus = false;
				foreach (var tarea in this.LsOrdenamientoTareas)
				{
					Estatus = VmTarea.SetOrdenTarea(tarea.UidTarea, tarea.IntOrden);
				}

				if (!Estatus)
					DisplayMessageBusqueda("Ocurrio un error al cambiar el orden de las tareas.", "danger");
				else
					DisplayMessageBusqueda("Actualizado correctamente.", "success");
			}
			catch (Exception)
			{

				throw;
			}

			try
			{
				VmTarea.Busqueda(SesionActual.uidSucursalActual.Value, 0, string.Empty, null, null, string.Empty, string.Empty, string.Empty, 0);
				this.LsOrdenamientoTareas = VmTarea.LsTareas;
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error al obtener tareas: <br/>" + ex.Message, "danger");
				return;
			}

			btnEditarOrdenamiento.CssClass = "btn btn-sm btn-default";
			btnEditarOrdenamiento.Visible = true;
			btnOkOrdenamiento.Visible = false;

			this.LsOrdenamientoTareas = this.LsOrdenamientoTareas.OrderBy(t => t.IntOrden).ToList();
			gvTareasOrdenamiento.DataSource = this.LsOrdenamientoTareas;
			gvTareasOrdenamiento.DataBind();
		}
		protected void btnCancelOrdenamiento_Click(object sender, EventArgs e)
		{
			if (pnlMessageSearch.Visible)
				pnlMessageSearch.Visible = false;

			btnOrdenamiento.CssClass = "btn btn-sm btn-default";
			btnOcultarMostrarFiltros.CssClass = "btn btn-sm btn-default";

			if (lblOcultarMostrarFiltros.Equals("Mostrar"))
				ShowFilters("Ocultar");
			else
				ShowFilters("Mostrar");

			btnOkOrdenamiento.Visible = false;
			btnEditarOrdenamiento.Visible = false;
			btnCancelOrdenamiento.Visible = false;

			pnlOrdenamientoTareas.Visible = false;
		}

		protected void btnOcultarMostrarFiltros_Click(object sender, EventArgs e)
		{
			ShowFilters(lblOcultarMostrarFiltros.Text);
		}
		protected void btnLimpiarFiltros_Click(object sender, EventArgs e)
		{
			txtFiltroFolio.Text = string.Empty;
			txtFiltroNombre.Text = string.Empty;
			txtFiltroFolioAntecesor.Text = string.Empty;
			txtFiltroFechaInicio.Text = string.Empty;
			txtFiltroFechaFin.Text = string.Empty;

			lbFiltroDepartamento.ClearSelection();
			lbFiltroArea.ClearSelection();
			lbFiltroUnidad.ClearSelection();
		}
		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			this.FiltroNombreTarea = txtFiltroNombre.Text.Trim();
			if (pnlMessageSearch.Visible)
				pnlMessageSearch.Visible = false;

			int Aux = 0;
			if (!string.IsNullOrEmpty(txtFiltroFolio.Text))
			{
				if (int.TryParse(txtFiltroFolio.Text.Trim(), out Aux))
				{
					this.FiltroFolioTarea = int.Parse(txtFiltroFolio.Text.Trim());
				}
				else
				{
					DisplayMessageBusqueda("Ingrese un valor numerico valido en <strong>Folio</strong>", "warning");
					return;
				}
			}

			if (!string.IsNullOrEmpty(txtFiltroFechaInicio.Text))
			{
				try
				{
					this.DtFiltroFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayMessageBusqueda("Formato de fecha de <strong>Inicio</strong> invalido.", "warning");
					return;
				}
			}

			if (!string.IsNullOrEmpty(txtFiltroFechaFin.Text))
			{
				try
				{
					this.DtFiltroFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayMessageBusqueda("Formato de fecha <strong>Fin</strong> invalido.", "warning");
					return;
				}
			}

			if (!string.IsNullOrEmpty(txtFiltroFolioAntecesor.Text))
			{
				if (int.TryParse(txtFiltroFolioAntecesor.Text.Trim(), out Aux))
				{
					this.FiltroFolioTarea = int.Parse(txtFiltroFolioAntecesor.Text.Trim());
				}
				else
				{
					DisplayMessageBusqueda("Ingrese un valor numerico valido en <strong>Folio de Antecesor</strong>", "warning");
					return;
				}
			}

			this.FiltroDepartamentos = GetListBoxValueSelected(lbFiltroDepartamento);
			this.FiltroAreas = GetListBoxValueSelected(lbFiltroArea);
			this.FiltroUnidadesMedida = GetListBoxValueSelected(lbFiltroUnidad);

			try
			{
				VmTarea.Busqueda(SesionActual.uidSucursalActual.Value, this.FiltroFolioTarea, this.FiltroNombreTarea, this.DtFiltroFechaInicio, this.DtFiltroFechaFin, this.FiltroDepartamentos, this.FiltroAreas, this.FiltroUnidadesMedida, this.FiltroFolioAntecesor);
				this.LsTareas = VmTarea.LsTareas;
				gvTareas.SelectedIndex = -1;
				gvTareas.PageIndex = 0;
				gvTareas.DataSource = this.LsTareas;
				gvTareas.DataBind();

				ShowFilters("Ocultar");
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error al realizar la busqueda: <br/>" + ex.Message, "danger");
			}
		}
		private void ShowFilters(string Action)
		{
			if (Action.Equals("Ocultar"))
			{
				lblOcultarMostrarFiltros.Text = "Mostrar";
				pnlFiltrosBusqueda.Visible = false;
				pnlListaTareas.Visible = true;

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default disabled";
				btnBuscar.CssClass = "btn btn-sm btn-default disabled";
			}
			else
			{
				lblOcultarMostrarFiltros.Text = "Ocultar";
				pnlFiltrosBusqueda.Visible = true;
				pnlListaTareas.Visible = false;

				btnLimpiarFiltros.CssClass = "btn btn-sm btn-default";
				btnBuscar.CssClass = "btn btn-sm btn-default";
			}
		}
		#endregion

		#region GridView
		protected void gvTareas_PreRender(object sender, EventArgs e)
		{
			GridViewRow pagerRow = gvTareas.TopPagerRow;

			if (pagerRow != null && pagerRow.Visible == false)
				pagerRow.Visible = true;
		}
		protected void gvTareas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvTareas, "Select$" + e.Row.RowIndex);

				Label lblName = e.Row.FindControl("lblTaskName") as Label;
				if (lblName.Text.ToString().Length > 30)
				{
					lblName.ToolTip = lblName.Text;
					lblName.Text = lblName.Text.Substring(0, 27) + "...";
				}

				Label lblDepto = e.Row.FindControl("lblTaskDepto") as Label;
				if (lblDepto.Text.Length >= 13)
				{
					lblDepto.ToolTip = lblDepto.Text;
					lblDepto.Text = lblDepto.Text.Substring(0, 10) + "...";
				}

				Label lblArea = e.Row.FindControl("lblTaskArea") as Label;
				if (lblArea.Text.Length >= 13)
				{
					lblArea.ToolTip = lblArea.Text;
					lblArea.Text = lblArea.Text.Substring(0, 10) + "...";
				}

				if (e.Row.Cells[5].Text == "&nbsp;")
				{
					//PERFIL.CssClass = "glyphicon glyphicon-user";
					e.Row.Cells[5].Text = "(Sin Asignar)";
				}
				if (e.Row.Cells[3].Text.Contains(","))
				{
					string split = e.Row.Cells[3].Text.Split(',')[0];
					e.Row.Cells[3].ToolTip = Server.HtmlDecode(e.Row.Cells[3].Text);
					e.Row.Cells[3].Text = split + ", ...";
				}
				if (e.Row.Cells[5].Text.Contains(","))
				{
					string split = e.Row.Cells[5].Text.Split(',')[0];
					e.Row.Cells[5].ToolTip = Server.HtmlDecode(e.Row.Cells[5].Text);
					e.Row.Cells[5].Text = split + ", ...";
				}

				if (e.Row.Cells[6].Text.Equals("False"))
					e.Row.CssClass = "border-warning";

				if (hfUidTarea.Value != string.Empty)
				{
					if (gvTareas.DataKeys[e.Row.RowIndex].Value.ToString().Equals(hfUidTarea.Value))
						e.Row.RowState = DataControlRowState.Selected;
				}
			}
		}
		protected void gvTareas_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvTareas.DataSource = this.LsTareas;
			gvTareas.PageIndex = e.NewPageIndex;
			gvTareas.SelectedIndex = -1;
			gvTareas.DataBind();
		}
		protected void gvTareas_Sorting(object sender, GridViewSortEventArgs e)
		{
			SortDirection Direction = SortDirection.Ascending;
			if (hfGvTareasSortDirection.Value.Equals("ASC"))
			{
				hfGvTareasSortDirection.Value = "DESC";
			}
			else
			{
				hfGvTareasSortDirection.Value = "ASC";
				Direction = SortDirection.Descending;
			}

			SortListaTareas(e.SortExpression, Direction);
			gvTareas.SelectedIndex = -1;
			gvTareas.DataBind();
		}
		protected void gvTareas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (btnOk.Visible)
				e.Cancel = true;
		}
		protected void gvTareas_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid UidTarea = new Guid(gvTareas.SelectedDataKey.Value.ToString());

			// Obtener datos de la tarea
			VmTarea.GetTareaById(UidTarea);
			this.TotalCumplimientosTarea = VmTarea.GetTotalCumplimientosTarea(UidTarea);

			if (!VmTarea.tTarea.BlAutorizado)
			{
				btnHabilitarTarea.CssClass = "btn btn-sm btn-default";
				btnHabilitarTarea.Enabled = true;
			}
			else
			{
				btnHabilitarTarea.CssClass = "btn btn-sm btn-default disabled";
				btnHabilitarTarea.Enabled = false;
			}

			hfUidTarea.Value = UidTarea.ToString();
			txtNombreTarea.Text = VmTarea.tTarea.StrNombre;
			txtDescripcionTarea.Text = VmTarea.tTarea.StrDescripcion;

			ddlEstatusTarea.SelectedValue = VmTarea.tTarea.UidStatus.ToString();
			ddlTipoTarea.SelectedValue = VmTarea.tTarea.UidTipoTarea.ToString();
			ddlTipoMedicion.SelectedValue = VmTarea.tTarea.UidMedicion.ToString();
			ddlTipoMedicion_SelectedIndexChanged(null, null);

			this.LsOpcionesTarea = new List<Modelo.TareaOpcion>();
			string TipoMedicion = ddlTipoMedicion.SelectedItem.Text.ToString();
			if (TipoMedicion.Equals("Numerico"))
			{
				pnlUnidadMedida.Visible = true;
				ddlUnidadMedida.SelectedIndex = ddlUnidadMedida.Items.IndexOf(ddlUnidadMedida.Items.FindByValue(VmTarea.tTarea.UidUnidadMedida.ToString()));
			}
			if (TipoMedicion.Equals("Seleccionable"))
			{
				//pnlUnidadMedida.Visible = true;
				btnNuevaOpcion.CssClass = "btn btn-sm btn-default disabled";
				VmTarea.GetOpcionesTarea(UidTarea);
				this.LsOpcionesTarea = VmTarea.LsOpcionesTarea;
				gvOpcionesTarea.DataSource = VmTarea.LsOpcionesTarea;
				gvOpcionesTarea.DataBind();
			}
			if (TipoMedicion.Equals("Verdadero/Falso"))
			{
				//pnlUnidadMedida.Visible = true;
			}

			// Periodicidad
			Guid UidPeriodicidad = VmTarea.tTarea.UidPeriodicidad;
			hfUidPeriodicidad.Value = hfUidPeriodicidad.ToString();
			VmTarea.GetPeriodicidad(UidPeriodicidad);
			txtFechaInicio.Text = VmTarea.pPeriodicidad.DtFechaInicio.ToString("dd/MM/yyyy");
			txtFechaFin.Text = VmTarea.pPeriodicidad.DtFechaFin == null ? string.Empty : VmTarea.pPeriodicidad.DtFechaFin.Value.ToString("dd/MM/yyyy");
			VmTarea.GetFrecuenciaTarea(UidTarea);
			string Tipo = VmTarea.fFrecuencia.StrTipoFrecuencia;
			hfTipoPeriodicidad.Value = Tipo;

			DisplayPeriodicityFields(Tipo);
			rbSinPeriodicidad.Checked = false;
			rbPeriodicidadDiaria.Checked = false;
			rbPeriodicidadSemanal.Checked = false;
			rbPeriodicidadMensual.Checked = false;
			rbPeriodicidadAnual.Checked = false;

			if (Tipo.Equals("Sin periodicidad"))
			{
				rbSinPeriodicidad.Checked = true;
			}
			else if (Tipo.Equals("Diaria"))
			{
				LimpiarCamposPeriodicidadDiaria();
				rbPeriodicidadDiaria.Checked = true;
				HabilitarCamposPeriodicidadDiaria(false);

				if (VmTarea.pPeriodicidad.IntFrecuencia == 1)
				{
					chkPDOpcion1.Checked = false;
					chkPDOpcion2.Checked = true;
				}
				else
				{
					chkPDOpcion1.Checked = true;
					chkPDOpcion2.Checked = false;
					txtPDNumeroDias.Text = VmTarea.pPeriodicidad.IntFrecuencia.ToString();
				}
			}
			else if (Tipo.Equals("Semanal"))
			{
				rbPeriodicidadSemanal.Checked = true;
				LimpiarCamposPeriodicidadSemanal();
				HabilitarCamposPeriodicidadSemanal(false);
				VmTarea.GetPeriodicidadSemanal(UidPeriodicidad);

				foreach (ListItem item in cblPSListaDias.Items)
				{
					if (item.Text.Equals("Lunes"))
						item.Selected = VmTarea.pSemanal.BlLunes;
					else if (item.Text.Equals("Martes"))
						item.Selected = VmTarea.pSemanal.BlMartes;
					else if (item.Text.Equals("Miercoles"))
						item.Selected = VmTarea.pSemanal.BlMiercoles;
					else if (item.Text.Equals("Jueves"))
						item.Selected = VmTarea.pSemanal.BlJueves;
					else if (item.Text.Equals("Viernes"))
						item.Selected = VmTarea.pSemanal.BlViernes;
					else if (item.Text.Equals("Sabado"))
						item.Selected = VmTarea.pSemanal.BlSabado;
					else if (item.Text.Equals("Domingo"))
						item.Selected = VmTarea.pSemanal.BlDomingo;
				}

				txtPSNumeroSemanas.Text = VmTarea.pPeriodicidad.IntFrecuencia.ToString();
			}
			else if (Tipo.Equals("Mensual"))
			{
				rbPeriodicidadMensual.Checked = true;
				LimpiarCamposPeriodicidadMensual();
				HabilitarCamposPeriodicidadMensual(false);
				VmTarea.GetPeriodicidadMesual(UidPeriodicidad);

				if (VmTarea.pMensual.Tipo.Equals("A"))
				{
					chkPMRepetirCadaNDiasCadaNMes.Checked = true;
					chkPMRepetirCadaNMESConParametros.Checked = false;
					chkPMTipoC.Checked = false;

					txtPMDiaDelMes.Text = VmTarea.pMensual.IntDiasMes.ToString();
					txtPMNumeroDeMesesOpc1.Text = VmTarea.pPeriodicidad.IntFrecuencia.ToString();

					List<string> LsDias = VmTarea.pMensual.DiasMes.Split(',').ToList();
					this.LsFechasPeriodicidad = new List<Modelo.FechaPeriodicidad>();
					foreach (var item in LsDias)
					{
						LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(
								Guid.NewGuid(),
								int.Parse(item),
								0,
								0,
								string.Empty,
								string.Empty,
								"Mensual",
								"A",
								true
							));
					}

					gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
					gvFechasPeriodicidad.DataBind();
				}
				else if (VmTarea.pMensual.Tipo.Equals("B"))
				{
					chkPMRepetirCadaNDiasCadaNMes.Checked = false;
					chkPMRepetirCadaNMESConParametros.Checked = true;
					chkPMTipoC.Checked = false;

					txtPMNumeroDeMesesOpc2.Text = VmTarea.pPeriodicidad.IntFrecuencia.ToString();

					VmTarea.GetFechaPeriodicidadMensual(UidPeriodicidad, "Mensual");

					this.LsFechasPeriodicidad = VmTarea.LsFechasPeriodicidad;
					gvFechasPeriodicidad.DataSource = VmTarea.LsFechasPeriodicidad;
					gvFechasPeriodicidad.DataBind();

				}
				else if (VmTarea.pMensual.Tipo.Equals("C"))
				{
					chkPMRepetirCadaNDiasCadaNMes.Checked = false;
					chkPMRepetirCadaNMESConParametros.Checked = false;
					chkPMTipoC.Checked = true;

					txtPMDiasOpc3.Text = VmTarea.pMensual.IntDiasSemana.ToString();
					ddlPMNumeralesOpc3.SelectedValue = VmTarea.pMensual.IntDiasMes.ToString();
				}
			}
			else if (Tipo.Equals("Anual"))
			{
				rbPeriodicidadAnual.Checked = true;
				LimpiarCamposPeriodicidadAnual();
				HabilitarCamposPeriodicidadAnual(false);
				VmTarea.GetPeriodicidadAnual(UidPeriodicidad);

				if (VmTarea.pAnual.Tipo.Equals("A"))
				{
					chkPAOpcion1.Checked = true;
					chkPAOpcion2.Checked = false;

					VmTarea.GetFechaPeriodicidad(UidPeriodicidad);

					this.LsFechasPeriodicidad = VmTarea.LsFechasPeriodicidad;
					gvFechasPeriodicidad.DataSource = VmTarea.LsFechasPeriodicidad;
					gvFechasPeriodicidad.DataBind();
				}
				else if (VmTarea.pAnual.Tipo.Equals("B"))
				{
					chkPAOpcion1.Checked = false;
					chkPAOpcion2.Checked = true;

					if (VmTarea.pAnual.IntDiasMes == 1)
						ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Primer"));
					else if (VmTarea.pAnual.IntDiasMes == 2)
						ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Segundo"));
					else if (VmTarea.pAnual.IntDiasMes == 3)
						ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Tercer"));
					else if (VmTarea.pAnual.IntDiasMes == 4)
						ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Cuarto"));
					else if (VmTarea.pAnual.IntDiasMes == -1)
						ddlPAListaNumerales.SelectedIndex = ddlPAListaNumerales.Items.IndexOf(ddlPAListaNumerales.Items.FindByText("Ultimo"));

					if (VmTarea.pAnual.IntDiasSemanas == 2)
						ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Lunes"));
					else if (VmTarea.pAnual.IntDiasSemanas == 3)
						ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Martes"));
					else if (VmTarea.pAnual.IntDiasSemanas == 4)
						ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Miercoles"));
					else if (VmTarea.pAnual.IntDiasSemanas == 5)
						ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Jueves"));
					else if (VmTarea.pAnual.IntDiasSemanas == 6)
						ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Viernes"));
					else if (VmTarea.pAnual.IntDiasSemanas == 7)
						ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Sabado"));
					else if (VmTarea.pAnual.IntDiasSemanas == 1)
						ddlPAListaPeriodosyDias.SelectedIndex = ddlPAListaPeriodosyDias.Items.IndexOf(ddlPAListaPeriodosyDias.Items.FindByText("Domingo"));
				}

				txtPANumeroAnios.Text = VmTarea.pPeriodicidad.IntFrecuencia.ToString();
			}


			//Asignacion
			VmTarea.GetDepartamentosAsignadosTarea(UidTarea);
			rbAsignarADepartamento.Checked = false;
			rbAsignarAArea.Checked = false;
			this.LsDepartamentosAsignados = new List<Modelo.Departamento>();
			this.LsAreasAsignadas = new List<Modelo.Area>();
			if (VmTarea.LsDepartamentos.Count > 0)
			{
				rbAsignarADepartamento.Checked = true;
				this.LsDepartamentosAsignados = VmTarea.LsDepartamentos;
				gvDepartamentosAsignados.DataSource = this.LsDepartamentosAsignados;
				gvDepartamentosAsignados.DataBind();

				gvDepartamentosAsignacionBusqueda.DataSource = null;
				gvDepartamentosAsignacionBusqueda.DataBind();

				rbAsignarAArea.Checked = false;
				pnlAsignacionDepartamentos.Visible = true;
				pnlAsignacionAreas.Visible = false;
			}
			else
			{
				rbAsignarAArea.Checked = true;
				VmTarea.GetAreasAsignadasTarea(UidTarea);
				this.LsAreasAsignadas = VmTarea.LsAreas;
				gvAreasAsignadas.DataSource = this.LsAreasAsignadas;
				gvAreasAsignadas.DataBind();

				rbAsignarADepartamento.Checked = false;
				pnlAsignacionDepartamentos.Visible = false;
				pnlAsignacionAreas.Visible = true;
				gvDepartamentosAsignacionArea.Visible = true;
				gvAreasDisponiblesDepartamento.Visible = false;
				btnMostrarDepartamentosAsignacion.Visible = false;
			}

			// Antecesor
			VmTarea.GetAntecesorTarea(UidTarea);
			LimpiarCamposAntecesor();
			HabilitarCamposAntecesor(false);
			cbActivarAntecesor.Enabled = false;
			if (VmTarea.aAntecesor == null)
			{
				cbActivarAntecesor.Checked = false;
				pnlCamposAntecesor.Visible = false;
			}
			else
			{
				pnlCamposAntecesor.Visible = true;
				cbActivarAntecesor.Checked = true;

				this.hfAntecesorSeleccionado.Value = VmTarea.aAntecesor.UidTareaAnterior.ToString();
				txtDiasDespues.Text = VmTarea.aAntecesor.IntDiasDespues.ToString();
				VmTarea.GetTareaById(VmTarea.aAntecesor.UidTareaAnterior);
				txtNombreAntecesor.Text = VmTarea.tTarea.StrNombre;
				if (VmTarea.aAntecesor.BlUsarNotificacion.HasValue && VmTarea.aAntecesor.BlUsarNotificacion.Value)
				{
					pnlNotificacionAntecesor.Visible = true;
					cbUsarNotificacionAntecesor.Checked = true;
					txtMaximo.Text = VmTarea.aAntecesor.IntRepeticion?.ToString() ?? "";
					if (VmTarea.aAntecesor.IntRepeticion.HasValue)
					{
						rdIlimitado.Checked = false;
						rdNumeroLimitado.Checked = true;
					}
					else
					{
						rdIlimitado.Checked = true;
						rdNumeroLimitado.Checked = false;
					}
				}
				else
				{
					cbUsarNotificacionAntecesor.Checked = false;
					pnlNotificacionAntecesor.Visible = false;
				}
			}

			//Notificacion
			VmTarea.GetNotificacionTarea(UidTarea);
			LimpiarCamposNotificacion();
			cbHabilitarNotificacionTarea.Enabled = false;
			if (VmTarea.nNotificacion == null)
			{
				cbHabilitarNotificacionTarea.Checked = false;
				pnlNotificationSendTo.Visible = false;
				SetFieldsNotificacion("none");
			}
			else
			{
				pnlNotificationSendTo.Visible = true;
				cbSendToAdministrador.Checked = VmTarea.nNotificacion.SendToAdministrador;
				cbSendToSupervisor.Checked = VmTarea.nNotificacion.SendToSupervisor;

				hfUidNotificacion.Value = VmTarea.nNotificacion.UidNotificacion.ToString();
				cbHabilitarNotificacionTarea.Checked = true;
				if (TipoMedicion.Equals("Numerico"))
				{
					pnlNotificacionValor.Visible = true;
					if (VmTarea.nNotificacion.BlMenorIgual.HasValue && VmTarea.nNotificacion.BlMenorIgual.Value)
					{
						cbMenorIgual.Checked = true;
						cbMenor.Checked = false;
					}
					else
					{
						cbMenor.Checked = true;
						cbMenorIgual.Checked = false;
					}
					if (VmTarea.nNotificacion.BlMayorIgual.HasValue && VmTarea.nNotificacion.BlMayorIgual.Value)
					{
						cbMayorIgual.Checked = true;
						cbMayor.Checked = false;
					}
					else
					{
						cbMayor.Checked = true;
						cbMayorIgual.Checked = false;
					}
					if (VmTarea.nNotificacion.DcMayorQue.HasValue)
					{
						txtMayorQue.Text = VmTarea.nNotificacion.DcMayorQue.Value.ToString();
					}
					else
					{
						txtMayorQue.Text = "1";
					}

					if (VmTarea.nNotificacion.DcMenorQue.HasValue)
					{
						txtMenorQue.Text = VmTarea.nNotificacion.DcMenorQue.Value.ToString();
					}
					else
					{
						txtMenorQue.Text = "1";
					}
				}
				else if (TipoMedicion.Equals("Seleccionable"))
				{
					lbNotificacionOpcionesSeleccionadas.DataSource = this.LsOpcionesTarea;
					lbNotificacionOpcionesSeleccionadas.DataValueField = "UidOpciones";
					lbNotificacionOpcionesSeleccionadas.DataTextField = "StrOpciones";
					lbNotificacionOpcionesSeleccionadas.DataBind();
					lbNotificacionOpcionesSeleccionadas.Enabled = false;

					pnlNotificacionOpcionMultiple.Visible = true;
					if (VmTarea.nNotificacion.UidOpciones != null)
					{
						List<Guid> Opciones = VmTarea.nNotificacion.UidOpciones;
						int Index;
						foreach (ListItem item in lbNotificacionOpcionesSeleccionadas.Items)
						{
							Index = Opciones.FindIndex(x => x.ToString() == item.Value);
							if (Index >= 0)
							{
								item.Selected = true;
							}
						}
					}
				}
				else if (TipoMedicion.Equals("Verdadero/Falso"))
				{
					pnlNotificacionVerdaderoFalso.Visible = true;
					if (VmTarea.nNotificacion.BlValor.HasValue)
					{
						if (VmTarea.nNotificacion.BlValor.Value)
						{
							rbVerdaderoNotificacion.Checked = true;
						}
						else
						{
							rbFalsoNotificacion.Checked = true;
						}
					}
				}
			}

			HabilitarCamposNotificacion(false);

			btnEditar.CssClass = "btn btn-sm btn-default";
			btnEditar.Enabled = true;
		}

		protected void gvTareasOrdenamiento_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				if (hfUidTareaOrdenamiento.Value != string.Empty)
				{
					if (gvTareasOrdenamiento.DataKeys[e.Row.RowIndex].Value.ToString().Equals(hfUidTareaOrdenamiento.Value))
						e.Row.RowState = DataControlRowState.Selected;
				}
			}
		}
		protected void gvTareasOrdenamiento_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (btnOkOrdenamiento.Visible)
			{
				if (e.CommandName.Equals("Left"))
				{
					Guid UidTarea = new Guid(e.CommandArgument.ToString());
					hfUidTareaOrdenamiento.Value = UidTarea.ToString();
					int Index = this.LsOrdenamientoTareas.FindIndex(t => t.UidTarea == UidTarea);

					int PageSize = gvTareasOrdenamiento.PageSize;
					if (LsOrdenamientoTareas[Index].IntOrden > PageSize)
					{
						int Current = LsOrdenamientoTareas[Index].IntOrden;
						int Next = LsOrdenamientoTareas[Index].IntOrden - PageSize;

						int Index2 = this.LsOrdenamientoTareas.FindIndex(t => t.IntOrden == Next);

						this.LsOrdenamientoTareas[Index].IntOrden = Next;
						this.LsOrdenamientoTareas[Index2].IntOrden = Current;
					}
					this.LsOrdenamientoTareas = this.LsOrdenamientoTareas.OrderBy(t => t.IntOrden).ToList();
					gvTareasOrdenamiento.DataSource = LsOrdenamientoTareas;
					gvTareasOrdenamiento.DataBind();
				}
				else if (e.CommandName.Equals("Up"))
				{
					Guid UidTarea = new Guid(e.CommandArgument.ToString());
					hfUidTareaOrdenamiento.Value = UidTarea.ToString();
					int Index = this.LsOrdenamientoTareas.FindIndex(t => t.UidTarea == UidTarea);

					if (LsOrdenamientoTareas[Index].IntOrden > 1)
					{
						int CurrentPosition = LsOrdenamientoTareas[Index].IntOrden;
						int Index2 = Index - 1;
						int NextPosition = LsOrdenamientoTareas[Index2].IntOrden;

						LsOrdenamientoTareas[Index].IntOrden = NextPosition;
						LsOrdenamientoTareas[Index2].IntOrden = CurrentPosition;
					}
					this.LsOrdenamientoTareas = this.LsOrdenamientoTareas.OrderBy(t => t.IntOrden).ToList();
					gvTareasOrdenamiento.DataSource = LsOrdenamientoTareas;
					gvTareasOrdenamiento.DataBind();
				}
				else if (e.CommandName.Equals("Down"))
				{
					Guid UidTarea = new Guid(e.CommandArgument.ToString());
					hfUidTareaOrdenamiento.Value = UidTarea.ToString();
					int Index = this.LsOrdenamientoTareas.FindIndex(t => t.UidTarea == UidTarea);
					int ListSize = this.LsOrdenamientoTareas.Count;
					if (LsOrdenamientoTareas[Index].IntOrden < ListSize)
					{
						int CurrentPosition = LsOrdenamientoTareas[Index].IntOrden;
						int Index2 = Index + 1;
						int NextPosition = LsOrdenamientoTareas[Index2].IntOrden;

						LsOrdenamientoTareas[Index].IntOrden = NextPosition;
						LsOrdenamientoTareas[Index2].IntOrden = CurrentPosition;
					}
					this.LsOrdenamientoTareas = this.LsOrdenamientoTareas.OrderBy(t => t.IntOrden).ToList();
					gvTareasOrdenamiento.DataSource = LsOrdenamientoTareas;
					gvTareasOrdenamiento.DataBind();
				}
				else if (e.CommandName.Equals("Right"))
				{
					Guid UidTarea = new Guid(e.CommandArgument.ToString());
					hfUidTareaOrdenamiento.Value = UidTarea.ToString();
					int Index = this.LsOrdenamientoTareas.FindIndex(t => t.UidTarea == UidTarea);
					int Current = this.LsOrdenamientoTareas[Index].IntOrden;
					int ListSize = this.LsOrdenamientoTareas.Count;
					int PageSize = gvTareasOrdenamiento.PageSize;

					if ((Current + PageSize) <= ListSize)
					{
						int Next = LsOrdenamientoTareas[Index].IntOrden + PageSize;
						int Index2 = this.LsOrdenamientoTareas.FindIndex(t => t.IntOrden == Next);

						this.LsOrdenamientoTareas[Index].IntOrden = Next;
						this.LsOrdenamientoTareas[Index2].IntOrden = Current;
					}
					this.LsOrdenamientoTareas = this.LsOrdenamientoTareas.OrderBy(t => t.IntOrden).ToList();
					gvTareasOrdenamiento.DataSource = LsOrdenamientoTareas;
					gvTareasOrdenamiento.DataBind();
				}
			}
		}
		protected void gvTareasOrdenamiento_Sorting(object sender, GridViewSortEventArgs e)
		{
			gvTareasOrdenamiento.SelectedIndex = -1;
			VmTarea.LsTareas = this.LsOrdenamientoTareas;
			VmTarea.SortOrdenamientoTareas(hfGvTareasSortDirection.Value, e.SortExpression);
			this.LsOrdenamientoTareas = VmTarea.LsTareas;

			//if (btnOkOrdenamiento.Visible)
			//{
			//	for (int i = 0; i < this.LsOrdenamientoTareas.Count; i++)
			//	{
			//		this.LsOrdenamientoTareas[i].IntOrden = this.LsOrdenamientoTareas[i].IntFolio;
			//	}
			//}

			gvTareasOrdenamiento.DataSource = this.LsOrdenamientoTareas;
			gvTareasOrdenamiento.DataBind();

			if (hfGvTareasSortDirection.Value.Equals("ASC"))
				hfGvTareasSortDirection.Value = "DESC";
			else
				hfGvTareasSortDirection.Value = "ASC";
		}
		protected void gvTareasOrdenamiento_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			gvTareasOrdenamiento.SelectedIndex = -1;
			gvTareasOrdenamiento.PageIndex = e.NewPageIndex;
			gvTareasOrdenamiento.DataSource = this.LsOrdenamientoTareas;
			gvTareasOrdenamiento.DataBind();
		}
		#endregion
		#endregion

		#region Right
		#region Buttons
		protected void btnNuevo_Click(object sender, EventArgs e)
		{
			btnOk.Visible = true;
			btnCancelar.Visible = true;

			btnNuevo.CssClass = "btn btn-sm btn-default disabled";
			btnNuevo.Enabled = false;
			btnEditar.CssClass = "btn btn-sm btn-default disabled";
			btnEditar.Enabled = false;
			btnHabilitarTarea.CssClass = "btn btn-sm btn-default disabled";
			btnHabilitarTarea.Enabled = false;

			btnNuevaOpcion.CssClass = "btn btn-sm btn-default";
			btnEditarOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnOkOpcion.Visible = false;
			btnCancelarOpcion.Visible = false;

			// Datos generales
			hfUidTarea.Value = string.Empty;
			HabilitarCamposDatosGenerales(true);
			LimpiarCamposDatosGenerales();
			ddlTipoMedicion_SelectedIndexChanged(null, null);

			//Opciones
			hfUidOpcionTarea.Value = string.Empty;
			this.LsOpcionesTarea = new List<Modelo.TareaOpcion>();
			gvOpcionesTarea.DataSource = this.LsOpcionesTarea;
			gvOpcionesTarea.DataBind();

			// Periodicidad
			HabilitarCamposPeriodicidad(true);
			hfUidPeriodicidad.Value = string.Empty;
			hfTipoPeriodicidad.Value = string.Empty;
			rbSinPeriodicidad.Checked = true;
			DisplayPeriodicityFields("SN");
			this.LsFechasPeriodicidad = new List<Modelo.FechaPeriodicidad>();

			//Antecesor
			cbActivarAntecesor.Enabled = true;
			hfUidAntecesor.Value = string.Empty;

			//Notificacion
			cbHabilitarNotificacionTarea.Enabled = true;
			cbHabilitarNotificacionTarea.Checked = false;
			cbHabilitarNotificacionTarea_CheckedChanged(null, null);
			hfUidNotificacion.Value = string.Empty;

			// Asignacion
			HabilitarCamposAsignacion(true);
			rbAsignarADepartamento.Checked = true;
			rbAsignarAArea.Checked = false;
			pnlAsignacionDepartamentos.Visible = true;
			pnlAsignacionAreas.Visible = false;
			this.LsDepartamentosAsignados = new List<Modelo.Departamento>();
			gvDepartamentosAsignados.DataSource = this.LsDepartamentosAsignados;
			gvDepartamentosAsignados.DataBind();

			this.LsAreasAsignadas = new List<Modelo.Area>();
			gvAreasAsignadas.DataSource = this.LsDepartamentosAsignados;
			gvAreasAsignadas.DataBind();

			// Lista de tarea
			gvTareas.SelectedIndex = -1;

		}
		protected void btnEditar_Click(object sender, EventArgs e)
		{
			btnNuevo.CssClass = "btn btn-sm btn-default disabled";
			btnNuevo.Enabled = false;
			btnEditar.CssClass = "btn btn-sm btn-default disabled";
			btnEditar.Enabled = false;

			btnOk.Visible = true;
			btnCancelar.Visible = true;

			// Datos generales
			HabilitarCamposDatosGenerales(true);
			if (this.TotalCumplimientosTarea > 1)
			{
				txtFechaInicio.Enabled = false;
				ddlTipoMedicion.Enabled = false;
			}

			// Opciones Tarea
			btnNuevaOpcion.CssClass = "btn btn-sm btn-default";

			// Periodicidad
			HabilitarCamposPeriodicidad(true);
			if (hfTipoPeriodicidad.Value.Equals("Diaria"))
				HabilitarCamposPeriodicidadDiaria(true);
			else if (hfTipoPeriodicidad.Value.Equals("Semanal"))
				HabilitarCamposPeriodicidadSemanal(true);
			else if (hfTipoPeriodicidad.Value.Equals("Mensual"))
				HabilitarCamposPeriodicidadMensual(true);
			else if (hfTipoPeriodicidad.Value.Equals("Anual"))
				HabilitarCamposPeriodicidadAnual(true);

			//Asignacion 
			HabilitarCamposAsignacion(true);

			// Antecesor
			cbActivarAntecesor.Enabled = true;
			HabilitarCamposAntecesor(true);

			//Notificacion
			HabilitarCamposNotificacion(true);
			cbHabilitarNotificacionTarea.Enabled = true;
		}
		protected void btnOk_Click(object sender, EventArgs e)
		{
			if (pnlMessageGeneral.Visible == true)
				pnlMessageGeneral.Visible = false;

			if (pnlMessageOptions.Visible)
				pnlMessageOptions.Visible = false;

			if (pnlMessagePeriodicity.Visible)
				pnlMessagePeriodicity.Visible = false;

			string NombreTarea;
			string DescripcionTarea = txtDescripcionTarea.Text.Trim() == string.Empty ? string.Empty : txtDescripcionTarea.Text.Trim();
			DateTime DtFechaInicio = DateTime.Today;
			DateTime? DtFechaFin = null;
			Guid UidTipoTarea = new Guid(ddlTipoTarea.SelectedValue.ToString());
			Guid UidEstatusTarea = new Guid(ddlEstatusTarea.SelectedValue.ToString());
			Guid UidTipoMedicion = new Guid(ddlTipoMedicion.SelectedValue.ToString());
			Guid UidUnidadMedida = Guid.Empty;
			Guid UidAntecesor = Guid.Empty;
			bool BlRequiereFoto = false;
			bool BlHabilitarHora = false;
			string Hora = string.Empty;
			int Tolerancia = -1;

			///<summary>
			///Validar campos [DATOS GENERALES]
			///</summary>			
			if (!string.IsNullOrEmpty(txtNombreTarea.Text.Trim()))
			{
				NombreTarea = txtNombreTarea.Text.Trim();
			}
			else
			{
				txtNombreTarea.Focus();
				DisplayMessageGeneral("Ingrese un nombre valido", "warning");
				return;
			}
			///<summary>
			///Fecha de Inicio
			///</summary>
			try
			{
				DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				txtFechaInicio.Focus();
				DisplayMessageGeneral("Ingrese una fecha de inicio valida", "danger");
				return;
			}
			///<summary>
			///Fecha fin
			///</summary>
			if (!string.IsNullOrEmpty(txtFechaFin.Text))
			{
				try
				{
					DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					txtFechaFin.Focus();
					DisplayMessageGeneral("Ingrese una fecha fin valida", "danger");
					return;
				}
			}

			if (CbFoto.Checked)
				BlRequiereFoto = true;
			///<summary>
			/// Validar si se habilitar hora de tarea
			///</summary>
			if (cbHabilitarHoraTarea.Checked)
			{
				BlHabilitarHora = true;
				if (string.IsNullOrEmpty(txtHoraTarea.Text.Trim()))
				{
					txtHoraTarea.Focus();
					DisplayMessageGeneral("Ingrese una hora valida", "warning");
					return;
				}
				Hora = txtHoraTarea.Text.Trim();
				if (string.IsNullOrEmpty(txtToleranciaTarea.Text.Trim()))
				{
					txtToleranciaTarea.Focus();
					DisplayMessageGeneral("Tolerancia Requerida", "warning");
					return;
				}

				int AuxTolerancia = 0;
				if (int.TryParse(txtToleranciaTarea.Text.Trim(), out AuxTolerancia))
				{
					Tolerancia = int.Parse(txtToleranciaTarea.Text.Trim());
				}
				else
				{
					txtToleranciaTarea.Focus();
					DisplayMessageGeneral("Ingrese un valor numerico en Tolerancia.", "danger");
					return;
				}
			}

			string TipoMedicionTarea = ddlTipoMedicion.SelectedItem.ToString();

			if (ddlTipoMedicion.SelectedItem.ToString().Equals("Numerico"))
			{
				UidUnidadMedida = new Guid(ddlUnidadMedida.SelectedValue.ToString());
			}
			else if (ddlTipoMedicion.SelectedItem.ToString().Equals("Seleccionable"))
			{
				if (LsOpcionesTarea.Count == 0)
				{
					this.Natigate(false, true, false, false, false, false);
					DisplayMessageGeneral("Agregue Opciones", "danger");
					return;
				}
			}

			///<summary>
			///Campos Periodicidad
			///</summary>
			if (!rbSinPeriodicidad.Checked && !rbPeriodicidadSemanal.Checked && !rbPeriodicidadMensual.Checked && !rbPeriodicidadAnual.Checked && !rbPeriodicidadDiaria.Checked)
			{
				this.Natigate(false, false, true, false, false, false);
				DisplayMessageGeneral("Es necesario que seleccione una periodicidad.", "warning");
				return;
			}

			bool auxError = false;
			string auxErrorMessage = string.Empty;
			///<summary>
			///Validar paramentros Periodicidad diaria
			///</summary>
			if (rbPeriodicidadDiaria.Checked)
			{
				if (!chkPDOpcion1.Checked && !chkPDOpcion2.Checked)
				{
					this.Natigate(false, false, true, false, false, false);
					DisplayMessageGeneral("Selecciona una opcion", "warning");
					return;
				}

				if (chkPDOpcion1.Checked)
				{
					if (!IsNumberValue(txtPDNumeroDias.Text.Trim()))
					{
						this.Natigate(false, false, true, false, false, false);
						txtPDNumeroDias.Focus();
						DisplayMessageGeneral("Ingrese un valor numerico", "danger");
						return;
					}
				}
			}
			///<summary>
			///Validar paramentros Periodicidad Semanal
			///</summary>
			else if (rbPeriodicidadSemanal.Checked)
			{
				if (!IsNumberValue(txtPSNumeroSemanas.Text.Trim()))
				{
					txtPSNumeroSemanas.Focus();
					auxError = true;
					auxErrorMessage = "Ingrese un valor numerico";
				}

				bool IsSelected = false;
				foreach (ListItem item in cblPSListaDias.Items)
				{
					IsSelected = item.Selected;
					if (IsSelected)
						break;
				}

				if (!IsSelected)
				{
					this.Natigate(false, false, true, false, false, false);
					DisplayMessageGeneral("Seleccione los dias de cumplimiento", "danger");
					return;
				}
			}
			///<summary>
			///Validar paramentros Periodicidad Mensual
			///</summary>
			else if (rbPeriodicidadMensual.Checked)
			{
				if (!chkPMRepetirCadaNDiasCadaNMes.Checked && !chkPMRepetirCadaNMESConParametros.Checked && !chkPMTipoC.Checked)
				{
					this.Natigate(false, false, true, false, false, false);
					DisplayMessageGeneral("Selecciona una opcion", "warning");
					return;
				}

				if (chkPMRepetirCadaNDiasCadaNMes.Checked)
				{
					if (this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Count == 0)
					{
						this.Natigate(false, false, true, false, false, false);
						DisplayMessageGeneral("Agregue minimo un día.", "warning");
						return;
					}
				}
				else if (chkPMRepetirCadaNMESConParametros.Checked)
				{
					if (this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Count == 0)
					{
						this.Natigate(false, false, true, false, false, false);
						DisplayMessageGeneral("Agregue minimo un día.", "warning");
						return;
					}
				}
				else if (chkPMTipoC.Checked)
				{
					if (this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Count == 0)
					{
						this.Natigate(false, false, true, false, false, false);
						DisplayMessageGeneral("Agregue minimo un día.", "warning");
						return;
					}
				}
			}
			///<summary>
			///Validar paramentros Periodicidad Anual
			///</summary>
			else if (rbPeriodicidadAnual.Checked)
			{
				if (!chkPAOpcion1.Checked && !chkPAOpcion2.Checked)
				{
					this.Natigate(false, false, true, false, false, false);
					DisplayMessageGeneral("Selecciona una opcion", "warning");
					return;
				}

				if (chkPAOpcion1.Checked)
				{
					if (this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Count == 0)
					{
						this.Natigate(false, false, true, false, false, false);
						DisplayMessageGeneral("Agregue minimo un día.", "warning");
						return;
					}
				}

				if (chkPAOpcion2.Checked)
				{
					if (this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Count == 0)
					{
						this.Natigate(false, false, true, false, false, false);
						DisplayMessageGeneral("Agregue minimo un día.", "warning");
						return;
					}
				}

				//if (!IsNumberValue(txtPANumeroAnios.Text.Trim()))
				//{
				//	txtPANumeroAnios.Focus();
				//	auxError = true;
				//	auxErrorMessage = "Ingrese un valor numerico";
				//}
				//if (!IsNumberValue(txtPADiaDelMesOpc1.Text.Trim()))
				//{
				//	txtPADiaDelMesOpc1.Focus();
				//	auxError = true;
				//	auxErrorMessage = "Ingrese un valor numerico";
				//}
			}
			///<summary>
			///Imprimir error
			///</summary>
			if (auxError)
			{
				this.Natigate(false, false, true, false, false, false);
				DisplayMessageGeneral(auxErrorMessage, "danger");
				return;
			}

			///<summary>
			///Asignacion de tarea
			///</summary>
			if (!rbAsignarADepartamento.Checked && !rbAsignarAArea.Checked)
			{
				this.Natigate(false, false, false, true, false, false);
				DisplayMessageGeneral("Seleccione una opcion de asignación", "warning");
				return;
			}
			else if (rbAsignarADepartamento.Checked)
			{
				if (this.LsDepartamentosAsignados.FindAll(d => d.ToDelete == false).Count == 0)
				{
					this.Natigate(false, false, false, true, false, false);
					DisplayMessageGeneral("Es necesario Asignar Minimo un Departamento", "warning");
					return;
				}
			}
			else if (rbAsignarAArea.Checked)
			{
				if (LsAreasAsignadas.FindAll(a => a.ToDelete == false).Count == 0)
				{
					this.Natigate(false, false, false, true, false, false);
					DisplayMessageGeneral("Es necesario Asignar Minimo un Area", "warning");
					return;
				}
			}

			///<summary>
			///Antecesor
			///</summary>
			if (cbActivarAntecesor.Checked)
			{
				if (hfAntecesorSeleccionado.Value == string.Empty)
				{
					this.Natigate(false, false, false, false, true, false);
					DisplayMessageGeneral("Seleccione un antecesor", "warning");
					return;
				}
				else
					UidAntecesor = new Guid(hfAntecesorSeleccionado.Value);

				if (!IsNumberValue(txtDiasDespues.Text.Trim()))
				{
					this.Natigate(false, false, false, false, true, false);
					txtDiasDespues.Focus();
					DisplayMessageGeneral("Ingrese un valor numerico", "danger");
					return;
				}

				if (cbUsarNotificacionAntecesor.Checked)
				{
					if (rdNumeroLimitado.Checked)
					{
						if (!IsNumberValue(txtMaximo.Text.Trim()))
						{
							this.Natigate(false, false, false, false, true, false);
							txtMaximo.Focus();
							DisplayMessageGeneral("Ingrese un valor numerico", "danger");
							return;
						}
					}
				}
			}

			///<summary>
			///Notificacion
			///</summary>		
			if (cbHabilitarNotificacionTarea.Checked)
			{
				if (TipoMedicionTarea.Equals("Numerico"))
				{
					decimal d;
					if (string.IsNullOrWhiteSpace(txtMayorQue.Text) || !decimal.TryParse(txtMayorQue.Text, out d))
					{
						this.Natigate(false, false, false, false, false, true);
						txtMayorQue.Focus();
						DisplayMessageGeneral("El valor limite es incorrecto", "danger");
						return;
					}
					if (string.IsNullOrWhiteSpace(txtMenorQue.Text) || !decimal.TryParse(txtMenorQue.Text, out d))
					{
						txtMenorQue.Focus();
						this.Natigate(false, false, false, false, false, true);
						DisplayMessageGeneral("El valor limite es incorrecto", "danger");
						return;
					}
				}
				else if (TipoMedicionTarea.Equals("Seleccionable"))
				{
					if (lbNotificacionOpcionesSeleccionadas.GetSelectedIndices().Count() == 0)
					{
						this.Natigate(false, false, false, false, false, true);
						DisplayMessageGeneral("Seleccione una opcion para la notificación", "warning");
						return;
					}
				}
				else if (TipoMedicionTarea.Equals("Verdadero/Falso"))
				{
					if (rbVerdaderoNotificacion.Checked && rbFalsoNotificacion.Checked)
					{
						this.Natigate(false, false, false, false, false, true);
						DisplayMessageGeneral("Solo puede seleccionar una opcion para la notificación.", "warning");
						return;
					}
					else if (!rbVerdaderoNotificacion.Checked && !rbFalsoNotificacion.Checked)
					{
						this.Natigate(false, false, false, false, false, true);
						DisplayMessageGeneral("Debe seleccionar una opcion para la noficicación.", "warning");
						return;
					}
				}

				if (!cbSendToAdministrador.Checked && !cbSendToSupervisor.Checked)
				{
					this.Natigate(false, false, false, false, false, true);
					DisplayMessageGeneral("Seleccione a quien se le enviará la notificación", "warning");
					return;
				}
			}

			btnOk.Visible = false;
			btnCancelar.Visible = false;

			//Guardar datos
			if (hfUidTarea.Value == string.Empty)
			{
				VmTarea.GetFrecuenciaByName(hfTipoPeriodicidad.Value);
				Guid UidTipoFrecuencia = VmTarea.fFrecuencia.UidTipoFrecuencia;

				// Fecha inicial del cumplimiento
				DateTime DtCumplimientoInicial = DtFechaInicio;
				// Frecuencia en la que se llevara a cabo el cumplimiento de la tarea
				int IntFrecuenciaCumplimiento = 1;
				// Frecuencia de la periodicidad
				string FrecuenciaCumplimiento = VmTarea.fFrecuencia.StrTipoFrecuencia;

				#region GuardarPeriodicidad	
				if (rbSinPeriodicidad.Checked)
				{
					VmTarea.SavePeriodicidad(0, UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
				}
				else if (rbPeriodicidadDiaria.Checked)
				{
					if (chkPDOpcion1.Checked)
					{
						VmTarea.SavePeriodicidad(int.Parse(txtPDNumeroDias.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPDNumeroDias.Text.Trim());
					}
					else if (chkPDOpcion2.Checked)
					{
						VmTarea.SavePeriodicidad(1, UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
					}
				}
				else if (rbPeriodicidadSemanal.Checked)
				{
					VmTarea.SavePeriodicidad(int.Parse(txtPSNumeroSemanas.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
					IntFrecuenciaCumplimiento = int.Parse(txtPSNumeroSemanas.Text.Trim());
				}
				else if (rbPeriodicidadMensual.Checked)
				{
					if (chkPMRepetirCadaNDiasCadaNMes.Checked)
					{
						VmTarea.SavePeriodicidad(int.Parse(txtPMNumeroDeMesesOpc1.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc1.Text.Trim());
					}
					else if (chkPMRepetirCadaNMESConParametros.Checked)
					{
						VmTarea.SavePeriodicidad(int.Parse(txtPMNumeroDeMesesOpc2.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc2.Text.Trim());
					}
					else if (chkPMTipoC.Checked)
					{
						VmTarea.SavePeriodicidad(int.Parse(txtPMMesesOpc3.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPMMesesOpc3.Text.Trim());
					}
				}
				else if (rbPeriodicidadAnual.Checked)
				{
					if (chkPAOpcion1.Checked)
					{
						VmTarea.SavePeriodicidad(int.Parse(txtPANumeroAnios.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPANumeroAnios.Text.Trim());
					}
					else if (chkPAOpcion2.Checked)
					{
						VmTarea.SavePeriodicidad(int.Parse(txtPANumeroAnios.Text.Trim()), UidTipoFrecuencia, DtFechaInicio, DtFechaFin);
						IntFrecuenciaCumplimiento = int.Parse(txtPANumeroAnios.Text.Trim());
					}
				}
				#endregion

				string EstatusMessage = "";
				Guid UidPeriodicidad = VmTarea.pPeriodicidad.UidPeriodicidad;
				if (VmTarea.SaveTarea(NombreTarea, DescripcionTarea, UidAntecesor, UidUnidadMedida, UidPeriodicidad, UidTipoMedicion, Hora, Tolerancia, UidTipoTarea, UidEstatusTarea, BlRequiereFoto, false, SesionActual.uidSucursalActual.Value))
				{
					DisplayMessageGeneral("Guardado Correctamente", "success");
					LimpiarCamposDatosGenerales();
					HabilitarCamposDatosGenerales(false);
				}

				Guid UidTarea = VmTarea.tTarea.UidTarea;
				string StrTipo = string.Empty;
				if (hfTipoPeriodicidad.Value.Equals("Semanal"))
				{
					bool BlLunes = true;
					bool BLMartes = false;
					bool BLMiercoles = false;
					bool BLJueves = false;
					bool BLViernes = false;
					bool BlSabado = false;
					bool BlDomingo = false;
					foreach (ListItem item in cblPSListaDias.Items)
					{
						if (item.Text.Equals("Lunes"))
							BlLunes = item.Selected;
						else if (item.Text.Equals("Martes"))
							BLMartes = item.Selected;
						else if (item.Text.Equals("Miercoles"))
							BLMiercoles = item.Selected;
						else if (item.Text.Equals("Jueves"))
							BLJueves = item.Selected;
						else if (item.Text.Equals("Viernes"))
							BLViernes = item.Selected;
						else if (item.Text.Equals("Sabado"))
							BlSabado = item.Selected;
						else if (item.Text.Equals("Domingo"))
							BlDomingo = item.Selected;
					}

					VmTarea.SavePeriodicidadSemanal(UidPeriodicidad, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
					DtCumplimientoInicial = VmTarea.CalcularCumplimientoPeriodicidadSemanal(DtFechaInicio.Date, IntFrecuenciaCumplimiento, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
				}
				else if (hfTipoPeriodicidad.Value.Equals("Mensual"))
				{
					if (chkPMRepetirCadaNDiasCadaNMes.Checked)
					{
						this.LsFechasPeriodicidad = this.LsFechasPeriodicidad.OrderBy(f => f.IntDia).ToList();
						string DiasMes = string.Join(",", this.LsFechasPeriodicidad.Select(f => f.IntDia));

						StrTipo = "A";
						VmTarea.SavePeriodicidadMensual(UidPeriodicidad, 0, 0, StrTipo, DiasMes);
						DtCumplimientoInicial = VmTarea.CalcularCumplimientoPeriodicidadMensual("A", DtFechaInicio, new List<Modelo.FechaPeriodicidad>(), DiasMes, 0, 0, IntFrecuenciaCumplimiento);
					}
					else if (chkPMRepetirCadaNMESConParametros.Checked)
					{
						StrTipo = "B";
						VmTarea.SavePeriodicidadMensual(UidPeriodicidad, 0, 0, StrTipo, string.Empty);
						foreach (Modelo.FechaPeriodicidad item in this.LsFechasPeriodicidad)
						{
							if (!item.IsSaved)
							{
								VmTarea.SaveFechaPeriodicidad(UidPeriodicidad, item.IntNumeral, item.IntDia, item.IntNumeroMes, "B");
							}
						}
						DtCumplimientoInicial = VmTarea.CalcularCumplimientoPeriodicidadMensual("B", DtFechaInicio, this.LsFechasPeriodicidad, string.Empty, 1, 1, IntFrecuenciaCumplimiento);
					}
					else if (chkPMTipoC.Checked)
					{
						int NumeroDias = int.Parse(txtPMDiasOpc3.Text.Trim());
						int Ordinal = int.Parse(ddlPMNumeralesOpc3.SelectedValue.ToString());
						StrTipo = "C";
						VmTarea.SavePeriodicidadMensual(UidPeriodicidad, Ordinal, NumeroDias, StrTipo, string.Empty);
						DtCumplimientoInicial = VmTarea.CalcularCumplimientoPeriodicidadMensual("C", DtFechaInicio, new List<Modelo.FechaPeriodicidad>(), string.Empty, Ordinal, NumeroDias, IntFrecuenciaCumplimiento);
					}
				}
				else if (hfTipoPeriodicidad.Value.Equals("Anual"))
				{
					if (chkPAOpcion1.Checked)
					{
						StrTipo = "A";
						VmTarea.SavePeriodicidadAnual(UidPeriodicidad, 0, 0, 0, StrTipo);

						//this.LsFechasPeriodicidad = this.LsFechasPeriodicidad.OrderBy(f => f.IntNumeroMes).ThenBy(f=>f.IntDia).ToList();
						foreach (Modelo.FechaPeriodicidad fp in this.LsFechasPeriodicidad)
						{
							VmTarea.SaveFechaPeriodicidad(UidPeriodicidad, 0, fp.IntDia, fp.IntNumeroMes, "A");
						}
						DateTime DtFechaActual = SesionActual.GetDateTimeOffset().Date;
						List<DateTime> LsDates = new List<DateTime>();
						foreach (Modelo.FechaPeriodicidad fp in this.LsFechasPeriodicidad)
						{
							LsDates.Add(DateTime.Parse(
								fp.IntDia.ToString("00") + "/" +
								fp.IntNumeroMes.ToString("00") + "/" +
								DtFechaActual.Year.ToString("00")));
						}
						DtCumplimientoInicial = VmTarea.CalcularCumplimientoPeriodicidadAnual("A", DtFechaInicio, LsDates, 1, 1, 2, IntFrecuenciaCumplimiento);
					}
					else if (chkPAOpcion2.Checked)
					{
						VmTarea.SavePeriodicidadAnual(UidPeriodicidad, 0, 0, 0, StrTipo);
						foreach (Modelo.FechaPeriodicidad item in this.LsFechasPeriodicidad)
						{
							if (!item.IsSaved)
							{
								VmTarea.SaveFechaPeriodicidad(UidPeriodicidad, item.IntNumeral, item.IntDia, item.IntNumeroMes, "B");
							}
						}
						DtCumplimientoInicial = VmTarea.CalcularCumplimientoPeriodicidadAnual("B", DtFechaInicio, new List<DateTime>(), 1, 1, 1, IntFrecuenciaCumplimiento);
					}
				}

				rbSinPeriodicidad.Checked = true;
				rbSinPeriodicidad_CheckedChanged(null, null);
				HabilitarCamposPeriodicidad(false);

				///<summary>
				/// Guardar Asignaciones
				///</summary>
				if (rbAsignarADepartamento.Checked)
				{
					VmTarea.SaveDepartamentos(LsDepartamentosAsignados, UidTarea);

					foreach (var depto in LsDepartamentosAsignados)
					{
						VmTarea.GuardarPrimerCumplimiento(UidTarea, depto.UidDepartamento, Guid.Empty, DtCumplimientoInicial);
					}
					this.LsDepartamentosAsignados = new List<Modelo.Departamento>();
				}
				else if (rbAsignarAArea.Checked)
				{
					VmTarea.SaveAreas(LsAreasAsignadas, UidTarea);

					foreach (var area in LsAreasAsignadas)
					{
						VmTarea.GuardarPrimerCumplimiento(UidTarea, Guid.Empty, area.UidArea, DtCumplimientoInicial);
					}
					this.LsAreasAsignadas = new List<Modelo.Area>();
				}
				HabilitarCamposAsignacion(false);
				rbAsignarADepartamento.Enabled = false;
				rbAsignarAArea.Enabled = false;
				rbAsignarADepartamento.Checked = true;
				rbAsignarADepartamento_CheckedChanged(null, null);

				///<summary>
				///Guardar Opciones de tarea
				///</summary>			
				if (this.LsOpcionesTarea.Count > 0)
				{
					VmTarea.SaveOpcionesTarea(this.LsOpcionesTarea, UidTarea);
					this.LsOpcionesTarea = new List<Modelo.TareaOpcion>();
					hfUidOpcionTarea.Value = string.Empty;
					btnCancelarOpcion_Click(null, null);
					btnNuevaOpcion.CssClass = "btn btn-sm btn-default disabled";
					gvOpcionesTarea.DataSource = null;
					gvOpcionesTarea.DataBind();
				}

				///<summary>
				///Guardar Nofiticacion
				///</summary>
				if (cbHabilitarNotificacionTarea.Checked)
				{
					Modelo.Notificacion _Notificacion = new Modelo.Notificacion();
					if (TipoMedicionTarea == "Verdadero/Falso")
					{
						_Notificacion.BlValor = rbVerdaderoNotificacion.Checked;
					}
					else if (TipoMedicionTarea == "Numerico")
					{
						if (!string.IsNullOrWhiteSpace(txtMenorQue.Text))
						{
							_Notificacion.DcMenorQue = Convert.ToDecimal(txtMenorQue.Text);
							_Notificacion.BlMenorIgual = cbMenorIgual.Checked;
						}
						if (!string.IsNullOrWhiteSpace(txtMayorQue.Text))
						{
							_Notificacion.DcMayorQue = Convert.ToDecimal(txtMayorQue.Text);
							_Notificacion.BlMayorIgual = cbMayorIgual.Checked;
						}
					}
					else if (TipoMedicionTarea == "Seleccionable")
					{
						VmTarea.GetOpcionesTarea(UidTarea);
						List<Modelo.TareaOpcion> OpcionesTarea = VmTarea.LsOpcionesTarea;
						string OpcionesSeleccionadas = string.Empty;
						int Index = -1;
						foreach (ListItem item in lbNotificacionOpcionesSeleccionadas.Items)
						{
							if (item.Selected)
							{
								Index = OpcionesTarea.FindIndex(o => o.StrOpciones == item.Text);
								if (Index >= 0)
								{
									if (OpcionesSeleccionadas == string.Empty)
										OpcionesSeleccionadas = OpcionesTarea[Index].UidOpciones.ToString();
									else
										OpcionesSeleccionadas += "," + OpcionesTarea[Index].UidOpciones.ToString();
								}
							}
						}
						_Notificacion.VchOpciones = OpcionesSeleccionadas;
					}

					_Notificacion.UidTarea = UidTarea;
					_Notificacion.SendToAdministrador = cbSendToAdministrador.Checked;
					_Notificacion.SendToSupervisor = cbSendToSupervisor.Checked;

					VmTarea.SaveNotificacion(_Notificacion);
				}
				cbHabilitarNotificacionTarea.Enabled = false;
				cbHabilitarNotificacionTarea.Checked = false;
				cbHabilitarNotificacionTarea_CheckedChanged(null, null);

				///<summary>
				///Guardar Antecesor
				///</summary>
				if (cbActivarAntecesor.Checked)
				{
					Modelo.Antecesor _Antecesor = new Modelo.Antecesor();
					_Antecesor.IntDiasDespues = int.Parse(txtDiasDespues.Text.Trim());
					if (cbUsarNotificacionAntecesor.Checked && rdNumeroLimitado.Checked)
						_Antecesor.IntRepeticion = int.Parse(txtMaximo.Text.Trim());
					_Antecesor.BlUsarNotificacion = cbUsarNotificacionAntecesor.Checked;
					_Antecesor.UidTarea = UidTarea;
					_Antecesor.UidTareaAnterior = UidAntecesor;

					VmTarea.SaveAntecesor(_Antecesor);
				}
				cbActivarAntecesor.Enabled = false;
				cbActivarAntecesor.Checked = false;
				cbActivarAntecesor_CheckedChanged(null, null);
			}
			else
			{
				// ACTUALIZAR DATOS DE LA TAREA
				Guid UidTarea = new Guid(hfUidTarea.Value);
				VmTarea.GetTareaById(UidTarea);
				if (VmTarea.tTarea.StrTipoMedicion.Equals("Seleccionable"))
				{
					string MedicionTarea = ddlTipoMedicion.SelectedItem.ToString();
					if (!MedicionTarea.Equals(VmTarea.tTarea.StrTipoMedicion))
					{

					}
				}
				Guid UidPeriodicidad = VmTarea.tTarea.UidPeriodicidad;

				int DateCompare = DateTime.Compare(DtFechaInicio, VmTarea.tTarea.DtFechaInicio);
				if (DateCompare < 0)
				{
					btnOk.Visible = true;
					btnCancelar.Visible = true;
					DisplayMessageGeneral("La fecha de (inicio) es menor a la anterior.", "danger");
					return;
				}

				// Datos generales
				bool UpdateResult = false;
				if (this.TotalCumplimientosTarea > 1)
					UpdateResult = VmTarea.UpdateTarea(UidTarea, NombreTarea, DescripcionTarea, null, null, UidTipoTarea, UidEstatusTarea, false, null, UidUnidadMedida);
				else
					UpdateResult = VmTarea.UpdateTarea(UidTarea, NombreTarea, DescripcionTarea, null, null, UidTipoTarea, UidEstatusTarea, false, DtFechaInicio, UidUnidadMedida);

				if (UpdateResult)
				{
					VmTarea.UpdatePeriodicidad(UidPeriodicidad, DtFechaFin);
				}
				HabilitarCamposDatosGenerales(false);

				///<summary>
				///Guardar Opciones de tarea
				///</summary>			
				if (this.LsOpcionesTarea.Count > 0)
				{
					VmTarea.SaveOpcionesTarea(this.LsOpcionesTarea, UidTarea);
					VmTarea.GetOpcionesTarea(UidTarea);
					this.LsOpcionesTarea = VmTarea.LsOpcionesTarea;
					hfUidOpcionTarea.Value = string.Empty;
					btnCancelarOpcion_Click(null, null);
					btnNuevaOpcion.CssClass = "btn btn-sm btn-default disabled";
				}

				// Periodicidad
				int FrecuenciaCumplimiento = 1;
				// Fecha para identificar el inicio de la periodicidad
				DateTime DtFechaBase;
				if (this.TotalCumplimientosTarea > 1)
				{
					VmTarea.GetUltimoCumplimientoTarea(UidTarea);
					if (VmTarea.cCumplimiento.BlAtrasada.Value)
					{
						DtFechaBase = VmTarea.cCumplimiento.DtFechaAtrasada.Value.DateTime;
					}
					else
					{
						DtFechaBase = VmTarea.cCumplimiento.DtFechaHora == null ? DtFechaInicio : VmTarea.cCumplimiento.DtFechaHora.Value.DateTime;
					}
				}
				else
					DtFechaBase = DtFechaInicio;

				VmTarea.GetFrecuenciaTarea(UidTarea);
				DateTime DtFechaProximoCumplimiento = DtFechaBase;
				string StrTipo = string.Empty;
				bool SameFrecuency = VmTarea.fFrecuencia.StrTipoFrecuencia.Equals(hfTipoPeriodicidad.Value);
				if (!SameFrecuency)
				{
					// Eliminar registros anteriores
					switch (VmTarea.fFrecuencia.StrTipoFrecuencia)
					{
						case "Semanal":
							VmTarea.DeletePeriodicidadSemanal(UidPeriodicidad);
							break;
						case "Mensual":
							VmTarea.DeletePeriodicidadMensual(UidPeriodicidad);
							break;
						case "Anual":
							VmTarea.GetPeriodicidadAnual(UidPeriodicidad);
							if (VmTarea.pAnual.Tipo.Equals("A"))
								VmTarea.DeleteAllFechaPeriodicidad(UidPeriodicidad);
							VmTarea.DeletePeriodicidadAnual(UidPeriodicidad);
							break;
						default:
							break;
					}

					VmTarea.GetFrecuenciaByName(hfTipoPeriodicidad.Value);
					Guid UidTipoFrecuencia = VmTarea.fFrecuencia.UidTipoFrecuencia;

					if (rbPeriodicidadDiaria.Checked)
					{
						FrecuenciaCumplimiento = chkPDOpcion1.Checked ? int.Parse(txtPDNumeroDias.Text.Trim()) : 1;
						DtFechaProximoCumplimiento = DtFechaProximoCumplimiento.AddDays(FrecuenciaCumplimiento);
						HabilitarCamposPeriodicidadDiaria(false);
					}
					else if (rbPeriodicidadSemanal.Checked)
					{
						bool BlLunes = true;
						bool BLMartes = false;
						bool BLMiercoles = false;
						bool BLJueves = false;
						bool BLViernes = false;
						bool BlSabado = false;
						bool BlDomingo = false;
						foreach (ListItem item in cblPSListaDias.Items)
						{
							if (item.Text.Equals("Lunes"))
								BlLunes = item.Selected;
							else if (item.Text.Equals("Martes"))
								BLMartes = item.Selected;
							else if (item.Text.Equals("Miercoles"))
								BLMiercoles = item.Selected;
							else if (item.Text.Equals("Jueves"))
								BLJueves = item.Selected;
							else if (item.Text.Equals("Viernes"))
								BLViernes = item.Selected;
							else if (item.Text.Equals("Sabado"))
								BlSabado = item.Selected;
							else if (item.Text.Equals("Domingo"))
								BlDomingo = item.Selected;
						}
						FrecuenciaCumplimiento = int.Parse(txtPSNumeroSemanas.Text.Trim());
						VmTarea.SavePeriodicidadSemanal(UidPeriodicidad, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
						DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadSemanal(DtFechaInicio.Date, FrecuenciaCumplimiento, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
						HabilitarCamposPeriodicidadSemanal(false);
					}
					else if (rbPeriodicidadMensual.Checked)
					{
						if (chkPMRepetirCadaNDiasCadaNMes.Checked)
						{
							this.LsFechasPeriodicidad = this.LsFechasPeriodicidad.OrderBy(t => t.IntDia).ToList();
							string DiasMes = string.Join(",", this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Select(f => f.IntDia));
							StrTipo = "A";
							VmTarea.SavePeriodicidadMensual(UidPeriodicidad, 0, 0, StrTipo, DiasMes);
							FrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc1.Text.Trim());
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadMensual("A", DtFechaBase, new List<Modelo.FechaPeriodicidad>(), DiasMes, 1, 2, FrecuenciaCumplimiento);

						}
						else if (chkPMRepetirCadaNMESConParametros.Checked)
						{
							StrTipo = "B";
							VmTarea.SavePeriodicidadMensual(UidPeriodicidad, 0, 0, StrTipo, string.Empty);
							foreach (Modelo.FechaPeriodicidad item in this.LsFechasPeriodicidad)
							{
								if (!item.IsSaved)
								{
									VmTarea.SaveFechaPeriodicidad(UidPeriodicidad, item.IntNumeral, item.IntDia, item.IntNumeroMes, "B");
								}
							}
							FrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc2.Text.Trim());
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadMensual("B", DtFechaBase, this.LsFechasPeriodicidad, string.Empty, 0, 0, FrecuenciaCumplimiento);
						}
						else if (chkPMTipoC.Checked)
						{
							int NumeroDias = int.Parse(txtPMDiasOpc3.Text.Trim());
							int Ordinal = int.Parse(ddlPMNumeralesOpc3.SelectedValue.ToString());
							StrTipo = "C";
							VmTarea.SavePeriodicidadMensual(UidPeriodicidad, Ordinal, NumeroDias, StrTipo, string.Empty);
							FrecuenciaCumplimiento = int.Parse(txtPMMesesOpc3.Text.Trim());
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadMensual("C", DtFechaBase, new List<Modelo.FechaPeriodicidad>(), string.Empty, Ordinal, NumeroDias, FrecuenciaCumplimiento);
						}
						HabilitarCamposPeriodicidadMensual(false);
					}
					else if (rbPeriodicidadAnual.Checked)
					{
						if (chkPAOpcion1.Checked)
						{
							StrTipo = "A";
							VmTarea.SavePeriodicidadAnual(UidPeriodicidad, 0, 0, 0, StrTipo);

							//this.LsFechasPeriodicidad = this.LsFechasPeriodicidad.OrderBy(f => f.IntNumeroMes).ThenBy(f=>f.IntDia).ToList();
							foreach (Modelo.FechaPeriodicidad fp in this.LsFechasPeriodicidad)
							{
								if (!fp.IsSaved)
									VmTarea.SaveFechaPeriodicidad(UidPeriodicidad, 0, fp.IntDia, fp.IntNumeroMes, "A");
								else if (fp.ToDelete)
									VmTarea.DeleteFechaPeriodicidad(fp.Identificador);

								fp.IsSaved = true;
								fp.ToDelete = false;
							}
							DateTime DtFechaActual = SesionActual.GetDateTimeOffset().Date;
							List<DateTime> LsDates = new List<DateTime>();
							foreach (Modelo.FechaPeriodicidad fp in this.LsFechasPeriodicidad)
							{
								LsDates.Add(DateTime.Parse(
									fp.IntDia.ToString("00") + "/" +
									fp.IntNumeroMes.ToString("00") + "/" +
									DtFechaActual.Year.ToString("00")));
							}
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadAnual("A", DtFechaInicio, LsDates, 1, 1, 2, FrecuenciaCumplimiento);
						}
						else if (chkPAOpcion2.Checked)
						{
							int Ordinal = 1;
							int DiaSemana = 1;
							int NumeroMes = 1;

							if (ddlPMListaNumerales.SelectedItem.Text == "Primer")
								Ordinal = 1;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Segundo")
								Ordinal = 2;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Tercer")
								Ordinal = 3;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Cuarto")
								Ordinal = 4;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Ultimo")
								Ordinal = -1;

							if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Domingo")
								DiaSemana = 1;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Lunes")
								DiaSemana = 2;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Martes")
								DiaSemana = 3;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Miercoles")
								DiaSemana = 4;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Jueves")
								DiaSemana = 5;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Viernes")
								DiaSemana = 6;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Sabado")
								DiaSemana = 7;

							if (ddlPAListaMesesOpc1.SelectedItem.Text == "Enero")
								NumeroMes = 1;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Febrero")
								NumeroMes = 2;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Marzo")
								NumeroMes = 3;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Abril")
								NumeroMes = 4;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Mayo")
								NumeroMes = 5;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Junio")
								NumeroMes = 6;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Julio")
								NumeroMes = 7;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Agosto")
								NumeroMes = 8;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Septiembre")
								NumeroMes = 9;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Octubre")
								NumeroMes = 10;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Noviembre")
								NumeroMes = 11;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Diciembre")
								NumeroMes = 12;

							StrTipo = "B";
							VmTarea.SavePeriodicidadAnual(UidPeriodicidad, Ordinal, DiaSemana, NumeroMes, StrTipo);
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadAnual("B", DtFechaInicio, new List<DateTime>(), NumeroMes, Ordinal, DiaSemana, FrecuenciaCumplimiento);
						}
						HabilitarCamposPeriodicidadAnual(false);
					}

					VmTarea.UpdateIdentificadorFrecuenciaPeriodicidad(UidPeriodicidad, UidTipoFrecuencia);
				}
				else
				{
					if (rbPeriodicidadDiaria.Checked)
					{
						FrecuenciaCumplimiento = chkPDOpcion1.Checked ? int.Parse(txtPDNumeroDias.Text.Trim()) : 1;
						DtFechaProximoCumplimiento = DtFechaProximoCumplimiento.AddDays(FrecuenciaCumplimiento);
						HabilitarCamposPeriodicidadDiaria(false);
					}
					else if (rbPeriodicidadSemanal.Checked)
					{
						FrecuenciaCumplimiento = int.Parse(txtPSNumeroSemanas.Text.Trim());
						bool BlLunes = true;
						bool BLMartes = false;
						bool BLMiercoles = false;
						bool BLJueves = false;
						bool BLViernes = false;
						bool BlSabado = false;
						bool BlDomingo = false;
						foreach (ListItem item in cblPSListaDias.Items)
						{
							if (item.Text.Equals("Lunes"))
								BlLunes = item.Selected;
							else if (item.Text.Equals("Martes"))
								BLMartes = item.Selected;
							else if (item.Text.Equals("Miercoles"))
								BLMiercoles = item.Selected;
							else if (item.Text.Equals("Jueves"))
								BLJueves = item.Selected;
							else if (item.Text.Equals("Viernes"))
								BLViernes = item.Selected;
							else if (item.Text.Equals("Sabado"))
								BlSabado = item.Selected;
							else if (item.Text.Equals("Domingo"))
								BlDomingo = item.Selected;
						}
						VmTarea.UpdatePeriodicidadSemanal(UidPeriodicidad, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
						DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadSemanal(DtFechaBase, FrecuenciaCumplimiento, BlLunes, BLMartes, BLMiercoles, BLJueves, BLViernes, BlSabado, BlDomingo);
						HabilitarCamposPeriodicidadSemanal(false);
					}
					else if (rbPeriodicidadMensual.Checked)
					{
						if (chkPMRepetirCadaNDiasCadaNMes.Checked)
						{
							this.LsFechasPeriodicidad = this.LsFechasPeriodicidad.OrderBy(t => t.IntDia).ToList();
							string DiasMes = string.Join(",", this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false).Select(f => f.IntDia));

							StrTipo = "A";
							VmTarea.UpdatePeriodicidadMensual(UidPeriodicidad, 0, 0, StrTipo, DiasMes);
							FrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc1.Text.Trim());
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadMensual("A", DtFechaBase, new List<Modelo.FechaPeriodicidad>(), DiasMes, 1, 2, FrecuenciaCumplimiento);
						}
						else if (chkPMRepetirCadaNMESConParametros.Checked)
						{
							StrTipo = "B";
							VmTarea.UpdatePeriodicidadMensual(UidPeriodicidad, 0, 0, StrTipo, string.Empty);
							foreach (Modelo.FechaPeriodicidad item in this.LsFechasPeriodicidad)
							{
								if (!item.IsSaved)
								{
									VmTarea.SaveFechaPeriodicidad(UidPeriodicidad, item.IntNumeral, item.IntDia, item.IntNumeroMes, "B");
								}
							}
							FrecuenciaCumplimiento = int.Parse(txtPMNumeroDeMesesOpc2.Text.Trim());
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadMensual("B", DtFechaBase, this.LsFechasPeriodicidad, string.Empty, 0, 0, FrecuenciaCumplimiento);
						}
						else if (chkPMTipoC.Checked)
						{
							int NumeroDias = int.Parse(txtPMDiasOpc3.Text.Trim());
							int Ordinal = int.Parse(ddlPMNumeralesOpc3.SelectedValue.ToString());
							StrTipo = "C";
							VmTarea.UpdatePeriodicidadMensual(UidPeriodicidad, Ordinal, NumeroDias, StrTipo, string.Empty);
							FrecuenciaCumplimiento = int.Parse(txtPMMesesOpc3.Text.Trim());
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadMensual("C", DtFechaBase, new List<Modelo.FechaPeriodicidad>(), string.Empty, Ordinal, NumeroDias, FrecuenciaCumplimiento);
						}
						HabilitarCamposPeriodicidadMensual(false);
					}
					else if (rbPeriodicidadAnual.Checked)
					{
						FrecuenciaCumplimiento = int.Parse(txtPANumeroAnios.Text.Trim());
						if (chkPAOpcion1.Checked)
						{
							StrTipo = "A";
							VmTarea.UpdatePeriodicidadAnual(UidPeriodicidad, 0, 0, 0, StrTipo);
							foreach (Modelo.FechaPeriodicidad fp in this.LsFechasPeriodicidad)
							{
								if (!fp.IsSaved)
									VmTarea.SaveFechaPeriodicidad(UidPeriodicidad, 0, fp.IntDia, fp.IntNumeroMes, "A");
								else if (fp.ToDelete)
									VmTarea.DeleteFechaPeriodicidad(fp.Identificador);

								fp.IsSaved = true;
								fp.ToDelete = false;
							}
							DateTime DtFechaActual = SesionActual.GetDateTimeOffset().Date;
							List<DateTime> LsDates = new List<DateTime>();
							foreach (Modelo.FechaPeriodicidad fp in this.LsFechasPeriodicidad)
							{
								LsDates.Add(DateTime.Parse(
									fp.IntDia.ToString("00") + "/" +
									fp.IntNumeroMes.ToString("00") + "/" +
									DtFechaActual.Year.ToString("00")));
							}
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadAnual("A", DtFechaBase, LsDates, 1, 1, 2, FrecuenciaCumplimiento);
						}
						else if (chkPAOpcion2.Checked)
						{
							int Ordinal = 1;
							int DiaSemana = 1;
							int NumeroMes = 1;

							if (ddlPMListaNumerales.SelectedItem.Text == "Primer")
								Ordinal = 1;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Segundo")
								Ordinal = 2;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Tercer")
								Ordinal = 3;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Cuarto")
								Ordinal = 4;
							else if (ddlPMListaNumerales.SelectedItem.Text == "Ultimo")
								Ordinal = -1;

							if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Domingo")
								DiaSemana = 1;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Lunes")
								DiaSemana = 2;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Martes")
								DiaSemana = 3;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Miercoles")
								DiaSemana = 4;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Jueves")
								DiaSemana = 5;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Viernes")
								DiaSemana = 6;
							else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Sabado")
								DiaSemana = 7;

							if (ddlPAListaMesesOpc1.SelectedItem.Text == "Enero")
								NumeroMes = 1;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Febrero")
								NumeroMes = 2;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Marzo")
								NumeroMes = 3;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Abril")
								NumeroMes = 4;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Mayo")
								NumeroMes = 5;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Junio")
								NumeroMes = 6;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Julio")
								NumeroMes = 7;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Agosto")
								NumeroMes = 8;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Septiembre")
								NumeroMes = 9;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Octubre")
								NumeroMes = 10;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Noviembre")
								NumeroMes = 11;
							else if (ddlPAListaMesesOpc1.SelectedItem.Text == "Diciembre")
								NumeroMes = 12;

							StrTipo = "A";
							VmTarea.UpdatePeriodicidadAnual(UidPeriodicidad, Ordinal, DiaSemana, NumeroMes, StrTipo);
							DtFechaProximoCumplimiento = VmTarea.CalcularCumplimientoPeriodicidadAnual("B", DtFechaInicio, new List<DateTime>(), NumeroMes, Ordinal, DiaSemana, FrecuenciaCumplimiento);
						}
						HabilitarCamposPeriodicidadAnual(false);
					}
				}
				HabilitarCamposPeriodicidad(false);

				VmTarea.UpdateFrecuenciaPeriodicidad(UidPeriodicidad, FrecuenciaCumplimiento);

				//Asignacion
				HabilitarCamposAsignacion(false);
				if (this.LsDepartamentosAsignados.Count > 0)
				{
					VmTarea.UpdateAsignacionDepartamentos(UidTarea, this.LsDepartamentosAsignados, DtFechaProximoCumplimiento);
				}
				if (this.LsAreasAsignadas.Count > 0)
				{
					VmTarea.UpdateAsignacionAreas(UidTarea, this.LsAreasAsignadas, DtFechaProximoCumplimiento);
				}

				VmTarea.GetDepartamentosAsignadosTarea(UidTarea);
				this.LsDepartamentosAsignados = new List<Modelo.Departamento>();
				this.LsAreasAsignadas = new List<Modelo.Area>();
				if (VmTarea.LsDepartamentos.Count > 0)
				{
					this.LsDepartamentosAsignados = VmTarea.LsDepartamentos;
					gvDepartamentosAsignados.DataSource = this.LsDepartamentosAsignados;
					gvDepartamentosAsignados.DataBind();
				}
				else
				{
					VmTarea.GetAreasAsignadasTarea(UidTarea);
					this.LsAreasAsignadas = VmTarea.LsAreas;
					gvAreasAsignadas.DataSource = this.LsAreasAsignadas;
					gvAreasAsignadas.DataBind();
				}

				// Antecesor
				if (cbActivarAntecesor.Checked)
				{
					Modelo.Antecesor _Antecesor = new Modelo.Antecesor();
					_Antecesor.IntDiasDespues = int.Parse(txtDiasDespues.Text.Trim());
					if (cbUsarNotificacionAntecesor.Checked && rdNumeroLimitado.Checked)
						_Antecesor.IntRepeticion = int.Parse(txtMaximo.Text.Trim());
					_Antecesor.BlUsarNotificacion = cbUsarNotificacionAntecesor.Checked;
					_Antecesor.UidTarea = UidTarea;
					_Antecesor.UidTareaAnterior = UidAntecesor;

					VmTarea.SaveAntecesor(_Antecesor);
				}
				else
				{
					VmTarea.DeleteAntecesor(UidTarea);
				}
				cbActivarAntecesor.Enabled = false;
				HabilitarCamposAntecesor(false);

				// Notificacion
				if (cbHabilitarNotificacionTarea.Checked)
				{
					Modelo.Notificacion _Notificacion = new Modelo.Notificacion();
					if (TipoMedicionTarea == "Verdadero/Falso")
					{
						_Notificacion.BlValor = rbVerdaderoNotificacion.Checked;
					}
					else if (TipoMedicionTarea == "Numerico")
					{
						if (!string.IsNullOrWhiteSpace(txtMenorQue.Text))
						{
							_Notificacion.DcMenorQue = Convert.ToDecimal(txtMenorQue.Text);
							_Notificacion.BlMenorIgual = cbMenorIgual.Checked;
						}
						if (!string.IsNullOrWhiteSpace(txtMayorQue.Text))
						{
							_Notificacion.DcMayorQue = Convert.ToDecimal(txtMayorQue.Text);
							_Notificacion.BlMayorIgual = cbMayorIgual.Checked;
						}
					}
					else if (TipoMedicionTarea == "Seleccionable")
					{
						VmTarea.GetOpcionesTarea(UidTarea);
						List<Modelo.TareaOpcion> OpcionesTarea = VmTarea.LsOpcionesTarea;
						string OpcionesSeleccionadas = string.Empty;
						int Index = -1;
						foreach (ListItem item in lbNotificacionOpcionesSeleccionadas.Items)
						{
							if (item.Selected)
							{
								Index = OpcionesTarea.FindIndex(o => o.StrOpciones == item.Text);
								if (Index >= 0)
								{
									if (OpcionesSeleccionadas == string.Empty)
										OpcionesSeleccionadas = OpcionesTarea[Index].UidOpciones.ToString();
									else
										OpcionesSeleccionadas += "," + OpcionesTarea[Index].UidOpciones.ToString();
								}
							}
						}
						_Notificacion.VchOpciones = OpcionesSeleccionadas;
					}

					_Notificacion.UidTarea = UidTarea;
					_Notificacion.SendToAdministrador = cbSendToAdministrador.Checked;
					_Notificacion.SendToSupervisor = cbSendToSupervisor.Checked;

					VmTarea.SaveNotificacion(_Notificacion);
				}
				else
				{
					VmTarea.DisableNotificacion(UidTarea);
				}

				HabilitarCamposNotificacion(false);
				cbHabilitarNotificacionTarea.Enabled = false;

				btnEditar.CssClass = "btn btn-sm btn-default";
				btnEditar.Enabled = true;
			}

			btnNuevo.CssClass = "btn btn-sm btn-default";
			btnNuevo.Enabled = true;

			try
			{
				VmTarea.Busqueda(SesionActual.uidSucursalActual.Value, this.FiltroFolioTarea, this.FiltroNombreTarea, this.DtFiltroFechaInicio, this.DtFiltroFechaFin, this.FiltroDepartamentos, this.FiltroAreas, this.FiltroUnidadesMedida, this.FiltroFolioAntecesor);
				this.LsTareas = VmTarea.LsTareas;
				gvTareas.SelectedIndex = -1;
				gvTareas.DataSource = this.LsTareas;
				gvTareas.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error al actualizar la lista de tareas", "danger");
			}
		}
		protected void btnCancelar_Click(object sender, EventArgs e)
		{
			btnOk.Visible = false;
			btnCancelar.Visible = false;
			btnNuevo.CssClass = "btn btn-sm btn-default";
			btnNuevo.Enabled = true;

			// Datos generales
			HabilitarCamposDatosGenerales(false);

			// Periodicidad
			HabilitarCamposPeriodicidad(false);
			if (pnlMessagePeriodicity.Visible)
				pnlMessagePeriodicity.Visible = false;

			// Asignacion
			HabilitarCamposAsignacion(false);
			rbAsignarADepartamento.Checked = true;
			rbAsignarADepartamento_CheckedChanged(null, null);
			btnCancelarBuscarDepartamentoAsignacion.Visible = false;

			// Antecesor
			cbActivarAntecesor.Enabled = false;
			HabilitarCamposAntecesor(false);

			// Notificacion
			cbHabilitarNotificacionTarea.Enabled = false;
			HabilitarCamposNotificacion(false);

			// Opciones de tarea
			hfUidOpcionTarea.Value = string.Empty;
			btnCancelarOpcion_Click(null, null);
			btnNuevaOpcion.CssClass = "btn btn-sm btn-default disabled";

			if (hfUidTarea.Value == string.Empty)
			{
				LimpiarCamposDatosGenerales();

				// Periodicidad
				rbSinPeriodicidad.Checked = true;
				DisplayPeriodicityFields("NA");

				// Antecesor
				cbActivarAntecesor.Enabled = false;
				cbActivarAntecesor.Checked = false;
				cbActivarAntecesor_CheckedChanged(null, null);
				cbUsarNotificacionAntecesor.Checked = false;
				cbUsarNotificacionAntecesor_CheckedChanged(null, null);

				//Notificacion
				cbHabilitarNotificacionTarea.Checked = false;
				cbHabilitarNotificacionTarea_CheckedChanged(null, null);
			}
			else
			{
				btnEditar.CssClass = "btn btn-sm btn-default";
				btnEditar.Enabled = true;

				// Periodicidad
				if (rbPeriodicidadDiaria.Checked)
					HabilitarCamposPeriodicidadDiaria(false);
				else if (rbPeriodicidadSemanal.Checked)
					HabilitarCamposPeriodicidadSemanal(false);
				else if (rbPeriodicidadMensual.Checked)
					HabilitarCamposPeriodicidadMensual(false);
				else if (rbPeriodicidadAnual.Checked)
					HabilitarCamposPeriodicidadAnual(false);

				// Asignacion
				HabilitarCamposAsignacion(false);

				// Antecesor
				HabilitarCamposAntecesor(false);
			}
		}
		protected void btnHabilitarTarea_Click(object sender, EventArgs e)
		{
			Guid UidTarea = new Guid(hfUidTarea.Value);

			VmTarea.HabilitarTareaCreadaSupervisor(UidTarea, SesionActual.uidSucursalActual.Value);
			try
			{
				VmTarea.Busqueda(SesionActual.uidSucursalActual.Value, this.FiltroFolioTarea, this.FiltroNombreTarea, this.DtFiltroFechaInicio, this.DtFiltroFechaFin, this.FiltroDepartamentos, this.FiltroAreas, this.FiltroUnidadesMedida, this.FiltroFolioAntecesor);
				this.LsTareas = VmTarea.LsTareas;
				gvTareas.SelectedIndex = -1;
				gvTareas.DataSource = this.LsTareas;
				gvTareas.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageBusqueda("Ocurrio un error al actualizar la lista de tareas", "danger");
			}

			btnHabilitarTarea.CssClass = "btn btn-sm btn-default disabled";
			btnHabilitarTarea.Enabled = false;
		}

		protected void btnNuevaOpcion_Click(object sender, EventArgs e)
		{
			HabilitarCamposOpcionTarea(true);
			LimpiarCamposOpcionTarea();
			btnOkOpcion.Visible = true;
			btnCancelarOpcion.Visible = true;

			btnNuevaOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnEditarOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";

			txtOrdenOpcionTarea.Text = "0";
			cbActivoOpcionTarea.Checked = true;
			hfUidOpcionTarea.Value = string.Empty;

			if (gvOpcionesTarea.Rows.Count > 0)
				gvOpcionesTarea.SelectedIndex = -1;
		}
		protected void btnEditarOpcion_Click(object sender, EventArgs e)
		{
			HabilitarCamposOpcionTarea(true);
			btnNuevaOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnEditarOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnOkOpcion.Visible = true;
			btnCancelarOpcion.Visible = true;
		}
		protected void btnEliminarOpcion_Click(object sender, EventArgs e)
		{
			if (hfUidOpcionTarea.Value != string.Empty)
			{
				try
				{
					Guid UidOpcion = new Guid(hfUidOpcionTarea.Value);

					int Index = this.LsOpcionesTarea.FindIndex(o => o.UidOpciones == UidOpcion);

					if (!LsOpcionesTarea[Index].ExistsInDatabase)
					{
						LsOpcionesTarea.RemoveAt(Index);
						hfUidOpcionTarea.Value = string.Empty;
						btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";
						btnEditarOpcion.CssClass = "btn btn-sm btn-default disabled";
					}
					else
						DisplayMessageOpcionTarea("La opcion seleccionada no se puede eliminar", "info");

					gvOpcionesTarea.DataSource = this.LsOpcionesTarea;
					gvOpcionesTarea.DataBind();
				}
				catch (Exception ex)
				{
					DisplayMessageOpcionTarea("Ocurrio un error al borrar opcion: <br>" + ex.Message, "danger");
				}
			}
		}
		protected void btnOkOpcion_Click(object sender, EventArgs e)
		{
			if (pnlMessageOptions.Visible)
				pnlMessageOptions.Visible = false;

			if (string.IsNullOrEmpty(txtNombreOpcionTarea.Text.Trim()))
			{
				DisplayMessageOpcionTarea("El campo <strong>Opción</strong> es requerido", "danger");
				return;
			}

			int AuxOrden = 0;
			if (!int.TryParse(txtOrdenOpcionTarea.Text.Trim(), out AuxOrden))
			{
				DisplayMessageOpcionTarea("Ingrese un valor numerico en el campo <strong>Orden</strong>", "danger");
				return;
			}

			if (LsOpcionesTarea == null)
				LsOpcionesTarea = new List<Modelo.TareaOpcion>();

			try
			{
				if (hfUidOpcionTarea.Value == string.Empty)
					LsOpcionesTarea.Add(new Modelo.TareaOpcion()
					{
						UidOpciones = Guid.NewGuid(),
						StrOpciones = txtNombreOpcionTarea.Text.Trim(),
						IntOrden = int.Parse(txtOrdenOpcionTarea.Text.Trim()),
						BlVisible = cbActivoOpcionTarea.Checked ? true : false
					});
				else
				{
					Guid UidOpcion = new Guid(hfUidOpcionTarea.Value);
					int Index = LsOpcionesTarea.FindIndex(x => x.UidOpciones == UidOpcion);
					if (Index >= 0)
					{
						LsOpcionesTarea[Index].StrOpciones = txtNombreOpcionTarea.Text.Trim();
						LsOpcionesTarea[Index].IntOrden = int.Parse(txtOrdenOpcionTarea.Text.Trim());
						LsOpcionesTarea[Index].BlVisible = cbActivoOpcionTarea.Checked ? true : false;
					}
				}
			}
			catch (Exception ex)
			{
				DisplayMessageOpcionTarea("Ocurrio un error: </br>" + ex.Message, "danger");
			}

			HabilitarCamposOpcionTarea(false);
			btnOkOpcion.Visible = false;
			btnCancelarOpcion.Visible = false;
			btnNuevaOpcion.CssClass = "btn btn-sm btn-default";
			if (string.IsNullOrEmpty(hfUidOpcionTarea.Value))
			{
				LimpiarCamposOpcionTarea();
				btnEditarOpcion.CssClass = "btn btn-sm btn-default disabled";
				btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";
			}
			else
			{
				btnEditarOpcion.CssClass = "btn btn-sm btn-default";
				btnEliminarOpcion.CssClass = "btn btn-sm btn-default";
			}

			gvOpcionesTarea.DataSource = LsOpcionesTarea;
			gvOpcionesTarea.DataBind();
		}
		protected void btnCancelarOpcion_Click(object sender, EventArgs e)
		{
			HabilitarCamposOpcionTarea(false);

			btnOkOpcion.Visible = false;
			btnCancelarOpcion.Visible = false;
			btnNuevaOpcion.CssClass = "btn btn-sm btn-default";
			btnEditarOpcion.CssClass = "btn btn-sm btn-default disabled";
			btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";

			if (string.IsNullOrEmpty(hfUidOpcionTarea.Value))
			{
				LimpiarCamposOpcionTarea();
			}
		}

		protected void btnAgregarFechaPeriodicidadMensual(object sender, EventArgs e)
		{
			if (pnlMessagePeriodicity.Visible)
				pnlMessagePeriodicity.Visible = false;

			if (chkPMRepetirCadaNDiasCadaNMes.Checked)
			{
				int Dia = 1;

				if (!int.TryParse(txtPMDiaDelMes.Text.Trim(), out Dia))
				{
					DisplayMessagePeriodicity("Ingrese un valor numerico.", "warning");
					return;
				}

				if (Dia > 31 || Dia == 0)
				{
					DisplayMessagePeriodicity("El valor ingresado es invalido.", "warning");
					return;
				}

				this.LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(Guid.NewGuid(), Dia, 0, 0, "None", string.Empty, "Mensual", "A", false));
				//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
				gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
				gvFechasPeriodicidad.DataBind();
			}
		}
		protected void btnAgregarFechaMensualOpc2_Click(object sender, EventArgs e)
		{
			if (chkPMRepetirCadaNMESConParametros.Checked)
			{
				int IntOrdinal = 1;
				int IntDiaSemana = 1;

				string StrNumeral = ddlPMListaNumerales.SelectedItem.Text;
				if (ddlPMListaNumerales.SelectedItem.Text == "Primer")
					IntOrdinal = 1;
				else if (ddlPMListaNumerales.SelectedItem.Text == "Segundo")
					IntOrdinal = 2;
				else if (ddlPMListaNumerales.SelectedItem.Text == "Tercer")
					IntOrdinal = 3;
				else if (ddlPMListaNumerales.SelectedItem.Text == "Cuarto")
					IntOrdinal = 4;
				else if (ddlPMListaNumerales.SelectedItem.Text == "Ultimo")
					IntOrdinal = -1;

				if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Domingo")
					IntDiaSemana = 1;
				else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Lunes")
					IntDiaSemana = 2;
				else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Martes")
					IntDiaSemana = 3;
				else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Miercoles")
					IntDiaSemana = 4;
				else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Jueves")
					IntDiaSemana = 5;
				else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Viernes")
					IntDiaSemana = 6;
				else if (ddlPMDiaSemanaOLapsoDias.SelectedItem.Text == "Sabado")
					IntDiaSemana = 7;

				this.LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(Guid.NewGuid(), IntDiaSemana, 0, IntOrdinal, "None", StrNumeral, "Mensual", "B", false));
				//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
				gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
				gvFechasPeriodicidad.DataBind();
			}
		}

		protected void btnAgregarFechaPeriodicidadAnual(object sender, EventArgs e)
		{
			if (pnlMessagePeriodicity.Visible)
				pnlMessagePeriodicity.Visible = false;

			if (chkPAOpcion1.Checked)
			{
				int Dia = 1;
				int NumeroMes = 1;
				string Mes = string.Empty;

				if (!int.TryParse(txtPADiaDelMesOpc1.Text.Trim(), out Dia))
				{
					DisplayMessagePeriodicity("Ingrese un valor numerico.", "warning");
					return;
				}

				if (Dia > 31 || Dia == 0)
				{
					DisplayMessagePeriodicity("El valor ingresado es invalido.", "warning");
					return;
				}

				Mes = ddlPAListaMesesOpc1.SelectedItem.Text.ToString();
				switch (Mes)
				{
					case "Enero":
						NumeroMes = 1;
						break;
					case "Febrero":
						NumeroMes = 2;
						break;
					case "Marzo":
						NumeroMes = 3;
						break;
					case "Abril":
						NumeroMes = 4;
						break;
					case "Mayo":
						NumeroMes = 5;
						break;
					case "Junio":
						NumeroMes = 6;
						break;
					case "Julio":
						NumeroMes = 7;
						break;
					case "Agosto":
						NumeroMes = 8;
						break;
					case "Septiembre":
						NumeroMes = 9;
						break;
					case "Octubre":
						NumeroMes = 10;
						break;
					case "Noviembre":
						NumeroMes = 11;
						break;
					case "Diciembre":
						NumeroMes = 12;
						break;
					default:
						break;
				}

				try
				{
					DateTime DtFecha = Convert.ToDateTime(DateTime.ParseExact(Dia.ToString("00") + "/" + NumeroMes.ToString("00") + "/" + DateTime.Now.Year, "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				catch (Exception)
				{
					DisplayMessagePeriodicity("El valor ingresado es invalido.", "warning");
					return;
				}

				this.LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(Guid.NewGuid(), Dia, NumeroMes, 0, Mes, string.Empty, "Anual", "A", false));
				//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
				gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
				gvFechasPeriodicidad.DataBind();
			}
		}
		protected void btnAgregarFechaAnualTipo2_Click(object sender, EventArgs e)
		{
			if (chkPAOpcion2.Checked)
			{
				int IntOrdinal = 1;
				int IntDiaSemana = 1;
				int IntNumeroMes = 1;

				string Numeral = ddlPAListaNumerales.SelectedItem.Text;
				if (ddlPAListaNumerales.SelectedItem.Text == "Primer")
					IntOrdinal = 1;
				else if (ddlPAListaNumerales.SelectedItem.Text == "Segundo")
					IntOrdinal = 2;
				else if (ddlPAListaNumerales.SelectedItem.Text == "Tercer")
					IntOrdinal = 3;
				else if (ddlPAListaNumerales.SelectedItem.Text == "Cuarto")
					IntOrdinal = 4;
				else if (ddlPAListaNumerales.SelectedItem.Text == "Ultimo")
					IntOrdinal = -1;

				if (ddlPAListaPeriodosyDias.SelectedItem.Text == "Domingo")
					IntDiaSemana = 1;
				else if (ddlPAListaPeriodosyDias.SelectedItem.Text == "Lunes")
					IntDiaSemana = 2;
				else if (ddlPAListaPeriodosyDias.SelectedItem.Text == "Martes")
					IntDiaSemana = 3;
				else if (ddlPAListaPeriodosyDias.SelectedItem.Text == "Miercoles")
					IntDiaSemana = 4;
				else if (ddlPAListaPeriodosyDias.SelectedItem.Text == "Jueves")
					IntDiaSemana = 5;
				else if (ddlPAListaPeriodosyDias.SelectedItem.Text == "Viernes")
					IntDiaSemana = 6;
				else if (ddlPAListaPeriodosyDias.SelectedItem.Text == "Sabado")
					IntDiaSemana = 7;

				string Mes = ddlPAListaMesesOpc2.SelectedItem.Text.ToString();
				switch (Mes)
				{
					case "Enero":
						IntNumeroMes = 1;
						break;
					case "Febrero":
						IntNumeroMes = 2;
						break;
					case "Marzo":
						IntNumeroMes = 3;
						break;
					case "Abril":
						IntNumeroMes = 4;
						break;
					case "Mayo":
						IntNumeroMes = 5;
						break;
					case "Junio":
						IntNumeroMes = 6;
						break;
					case "Julio":
						IntNumeroMes = 7;
						break;
					case "Agosto":
						IntNumeroMes = 8;
						break;
					case "Septiembre":
						IntNumeroMes = 9;
						break;
					case "Octubre":
						IntNumeroMes = 10;
						break;
					case "Noviembre":
						IntNumeroMes = 11;
						break;
					case "Diciembre":
						IntNumeroMes = 12;
						break;
					default:
						break;
				}

				this.LsFechasPeriodicidad.Add(new Modelo.FechaPeriodicidad(Guid.NewGuid(), IntDiaSemana, IntNumeroMes, IntOrdinal, Mes, Numeral, "Anual", "B", false));
				//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
				gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
				gvFechasPeriodicidad.DataBind();
			}
		}

		protected void btnBuscarDepartamentoAsignacion_Click(object sender, EventArgs e)
		{
			VmTarea.GetDepartamentosSucursal(SesionActual.uidSucursalActual.Value);
			if (rbAsignarADepartamento.Checked)
			{
				gvDepartamentosAsignacionBusqueda.DataSource = VmTarea.LsDepartamentos;
				gvDepartamentosAsignacionBusqueda.DataBind();
			}
			else if (rbAsignarAArea.Checked)
			{
				gvDepartamentosAsignacionArea.DataSource = VmTarea.LsDepartamentos;
				gvDepartamentosAsignacionArea.DataBind();
			}
			btnCancelarBuscarDepartamentoAsignacion.Visible = true;
		}
		protected void btnCancelarBuscarDepartamentoAsignacion_Click(object sender, EventArgs e)
		{
			txtNombreDepartamentoAsignacion.Text = string.Empty;
		}
		protected void btnMostrarDepartamentosAsignacion_Click(object sender, EventArgs e)
		{
			gvDepartamentosAsignacionArea.Visible = true;
			gvAreasDisponiblesDepartamento.Visible = false;
			gvAreasDisponiblesDepartamento.DataSource = null;
			gvAreasDisponiblesDepartamento.DataBind();

			btnMostrarDepartamentosAsignacion.Visible = false;
		}

		protected void btnBuscarAntecesor_Click(object sender, EventArgs e)
		{
			try
			{
				string txtNombre = txtNombreAntecesor.Text.Trim() == string.Empty ? string.Empty : txtNombreAntecesor.Text.Trim();
				VmTarea.GetAntecesores(SesionActual.uidSucursalActual.Value, txtNombre);

				gvListaTareasAntecesor.Visible = true;
				gvListaTareasAntecesor.DataSource = VmTarea.LsAntecesores;
				gvListaTareasAntecesor.DataBind();

			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Ocurrio un error al obtener las tareas </br>" + ex.Message, "warning");
			}
		}
		protected void btnCancelarBuscarAntecesor_Click(object sender, EventArgs e)
		{
			hfAntecesorSeleccionado.Value = string.Empty;
			gvListaTareasAntecesor.Visible = false;
			txtNombreAntecesor.Text = string.Empty;
			txtNombreAntecesor.Enabled = true;

			btnBuscarAntecesor.CssClass = "btn btn-sm btn-default";
			btnBuscarAntecesor.Enabled = true;

			cbUsarNotificacionAntecesor.Enabled = false;
		}

		protected void btnTabDatosGenerales_Click(object sender, EventArgs e)
		{
			this.Natigate(true, false, false, false, false, false);
		}
		protected void btnTabOpciones_Click(object sender, EventArgs e)
		{
			this.Natigate(false, true, false, false, false, false);
		}
		protected void btnTabPeriodos_Click(object sender, EventArgs e)
		{
			this.Natigate(false, false, true, false, false, false);
		}
		protected void btnTabAsignacion_Click(object sender, EventArgs e)
		{
			this.Natigate(false, false, false, true, false, false);
		}
		protected void btnTabAntecesor_Click(object sender, EventArgs e)
		{
			this.Natigate(false, false, false, false, true, false);
		}
		protected void btnTabNotificacion_Click(object sender, EventArgs e)
		{
			this.Natigate(false, false, false, false, false, true);
		}
		#endregion

		#region GridView
		protected void gvOpcionesTarea_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvOpcionesTarea, "Select$" + e.Row.RowIndex);

				Label lblEstatus = e.Row.FindControl("lblActivo") as Label;
				if (e.Row.Cells[4].Text.Equals("True"))
				{
					//e.Row.Cells.
					lblEstatus.CssClass = "glyphicon glyphicon-ok";
					lblEstatus.ToolTip = "Activo";
				}
				else
				{
					lblEstatus.CssClass = "glyphicon glyphicon-remove";
					lblEstatus.ToolTip = "Inactivo";
				}
			}
		}
		protected void gvOpcionesTarea_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (btnOkOpcion.Visible)
				e.Cancel = true;
		}
		protected void gvOpcionesTarea_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid UidOpcionTarea = new Guid(gvOpcionesTarea.SelectedDataKey.Value.ToString());
			hfUidOpcionTarea.Value = UidOpcionTarea.ToString();
			int Index = LsOpcionesTarea.FindIndex(x => x.UidOpciones == UidOpcionTarea);

			txtNombreOpcionTarea.Text = LsOpcionesTarea[Index].StrOpciones;
			txtOrdenOpcionTarea.Text = LsOpcionesTarea[Index].IntOrden.ToString();
			cbActivoOpcionTarea.Checked = LsOpcionesTarea[Index].BlVisible;

			if (hfUidTarea.Value == string.Empty && btnOk.Visible)
			{
				btnEditarOpcion.CssClass = "btn btn-sm btn-default ";
				btnEliminarOpcion.CssClass = "btn btn-sm btn-default ";
			}
			else
			{
				btnEditar.CssClass = "btn btn-sm btn-default disabled";
				btnEliminarOpcion.CssClass = "btn btn-sm btn-default disabled";
			}
		}

		protected void gvFechasPeriodicidad_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvFechasPeriodicidad, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvFechasPeriodicidad_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (btnOk.Visible)
			{
				if (e.CommandName.Equals("Delete"))
				{
					Guid Uid = new Guid(e.CommandArgument.ToString());
					int Index = this.LsFechasPeriodicidad.FindIndex(t => t.Identificador == Uid);

					if (this.LsFechasPeriodicidad[Index].IsSaved)
						this.LsFechasPeriodicidad[Index].ToDelete = true;
					else
						this.LsFechasPeriodicidad.RemoveAt(Index);
				}
			}
		}
		protected void gvFechasPeriodicidad_RowDeleting(object sender, GridViewDeleteEventArgs e)
		{
			//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}

		/// <summary>
		/// Asignacion de departamentos
		/// </summary>
		protected void gvDepartamentosAsignados_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvDepartamentosAsignados, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvDepartamentosAsignados_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (!btnOk.Visible)
				e.Cancel = true;
		}
		protected void gvDepartamentosAsignados_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidDepartamento = new Guid(gvDepartamentosAsignados.SelectedDataKey.Value.ToString());

				if (this.LsDepartamentosAsignados.Count > 0)
				{
					int Index = this.LsDepartamentosAsignados.FindIndex(x => x.UidDepartamento == UidDepartamento);
					if (Index > -1)
					{
						if (LsDepartamentosAsignados[Index].IsSaved)
							LsDepartamentosAsignados[Index].ToDelete = true;
						else
							this.LsDepartamentosAsignados.RemoveAt(Index);
					}
				}

				gvDepartamentosAsignados.DataSource = this.LsDepartamentosAsignados.FindAll(d => d.ToDelete == false);
				gvDepartamentosAsignados.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Departamentos asignados: </br>" + ex.Message, "danger");
			}
		}

		protected void gvDepartamentosAsignacionBusqueda_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvDepartamentosAsignacionBusqueda, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvDepartamentosAsignacionBusqueda_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidDepartamento = new Guid(gvDepartamentosAsignacionBusqueda.SelectedDataKey.Value.ToString());
				VmTarea.GetDepartamentoById(UidDepartamento);

				if (this.LsDepartamentosAsignados.Count > 0)
				{
					int index = this.LsDepartamentosAsignados.FindIndex(x => x.UidDepartamento == UidDepartamento);
					if (index == -1)
						this.LsDepartamentosAsignados.Add(new Modelo.Departamento()
						{
							UidDepartamento = VmTarea.dDepartamento.UidDepartamento,
							StrNombre = VmTarea.dDepartamento.StrNombre,
							StrDescripcion = VmTarea.dDepartamento.StrDescripcion
						});
					else
					{
						if (this.LsDepartamentosAsignados[index].IsSaved)
							this.LsDepartamentosAsignados[index].ToDelete = false;
					}
				}
				else
					this.LsDepartamentosAsignados.Add(new Modelo.Departamento()
					{
						UidDepartamento = VmTarea.dDepartamento.UidDepartamento,
						StrNombre = VmTarea.dDepartamento.StrNombre,
						StrDescripcion = VmTarea.dDepartamento.StrDescripcion
					});

				gvDepartamentosAsignados.DataSource = this.LsDepartamentosAsignados.FindAll(d => d.ToDelete == false);
				gvDepartamentosAsignados.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Busqueda de departamentos: </br>" + ex.Message, "danger");
			}
		}

		/// <summary>
		/// Asignacion de areas
		/// </summary>
		protected void gvAreasAsignadas_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvAreasAsignadas, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvAreasAsignadas_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
		{
			if (!btnOk.Visible)
				e.Cancel = true;
		}
		protected void gvAreasAsignadas_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidArea = new Guid(gvAreasAsignadas.SelectedDataKey.Value.ToString());

				if (this.LsAreasAsignadas.Count > 0)
				{
					int Index = this.LsAreasAsignadas.FindIndex(x => x.UidArea == UidArea);
					if (Index > -1)
					{
						if (this.LsAreasAsignadas[Index].IsSaved)
							this.LsAreasAsignadas[Index].ToDelete = true;
						else
							this.LsAreasAsignadas.RemoveAt(Index);
					}
				}
				gvAreasAsignadas.DataSource = this.LsAreasAsignadas.FindAll(a => a.ToDelete == false);
				gvAreasAsignadas.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Areas asignadas: </br>" + ex.Message, "danger");
			}
		}

		protected void gvDepartamentosAsignacionArea_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvDepartamentosAsignacionArea, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvDepartamentosAsignacionArea_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				lblMessageAsignacionArea.Text = string.Empty;
				Guid UidDepartamento = new Guid(gvDepartamentosAsignacionArea.SelectedDataKey.Value.ToString());

				VmTarea.GetAreasDepartamento(UidDepartamento);
				if (VmTarea.LsAreas.Count > 0)
				{
					gvDepartamentosAsignacionArea.Visible = false;
					gvAreasDisponiblesDepartamento.Visible = true;
					gvAreasDisponiblesDepartamento.DataSource = VmTarea.LsAreas;
					gvAreasDisponiblesDepartamento.DataBind();
					btnMostrarDepartamentosAsignacion.Visible = true;
				}
				else
				{
					lblMessageAsignacionArea.Text = "Departamento sin areas";
				}
			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Busqueda departamentos </br>" + ex.Message, "danger");
			}
		}

		protected void gvAreasDisponiblesDepartamento_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvAreasDisponiblesDepartamento, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvAreasDisponiblesDepartamento_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidArea = new Guid(gvAreasDisponiblesDepartamento.SelectedDataKey.Value.ToString());
				VmTarea.GetAreaById(UidArea);

				if (this.LsAreasAsignadas.Count > 0)
				{
					int index = this.LsAreasAsignadas.FindIndex(x => x.UidArea == UidArea);
					if (index == -1)
					{
						this.LsAreasAsignadas.Add(new Modelo.Area()
						{
							UidArea = VmTarea.aArea.UidArea,
							StrNombre = VmTarea.aArea.StrNombre,
							StrDescripcion = VmTarea.aArea.StrDescripcion
						});
					}
					else
					{
						if (this.LsAreasAsignadas[index].IsSaved)
							this.LsAreasAsignadas[index].ToDelete = false;
					}
				}
				else
				{
					LsAreasAsignadas.Add(new Modelo.Area()
					{
						UidArea = VmTarea.aArea.UidArea,
						StrNombre = VmTarea.aArea.StrNombre,
						StrDescripcion = VmTarea.aArea.StrDescripcion
					});
				}
				gvAreasAsignadas.DataSource = this.LsAreasAsignadas.FindAll(a => a.ToDelete == false);
				gvAreasAsignadas.DataBind();
			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Seleccion de area </br>" + ex.Message, "danger");
			}
		}

		/// <summary>
		/// Antecesores
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void gvListaTareasAntecesor_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvListaTareasAntecesor, "Select$" + e.Row.RowIndex);
			}
		}
		protected void gvListaTareasAntecesor_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Guid UidTarea = new Guid(gvListaTareasAntecesor.SelectedDataKey.Value.ToString());
				hfAntecesorSeleccionado.Value = UidTarea.ToString();
				VmTarea.GetTareaById(UidTarea);

				txtNombreAntecesor.Text = VmTarea.tTarea.StrNombre;
				txtNombreAntecesor.Enabled = false;
				gvListaTareasAntecesor.Visible = false;

				btnBuscarAntecesor.CssClass = "btn btn-sm btn-default disabled";
				btnBuscarAntecesor.Enabled = false;

				if (!VmTarea.ValidarNotificacionAntecesor(UidTarea))
					cbUsarNotificacionAntecesor.Enabled = false;
				else

					cbUsarNotificacionAntecesor.Enabled = true;
			}
			catch (Exception ex)
			{
				DisplayMessageGeneral("Ocurrio un error al obtener antecesores: </br>" + ex.Message, "danger");
			}
		}
		#endregion

		#region Dropdownlist
		protected void ddlTipoMedicion_SelectedIndexChanged(object sender, EventArgs e)
		{
			switch (ddlTipoMedicion.SelectedItem.ToString())
			{
				case "Numerico":
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("Numerico");
					pnlUnidadMedida.Visible = true;
					if (liOpciones.Visible == true)
						liOpciones.Visible = false;
					break;
				case "Seleccionable":
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("Seleccionable");
					if (pnlUnidadMedida.Visible)
						pnlUnidadMedida.Visible = false;
					liOpciones.Visible = true;
					break;
				case "Verdadero/Falso":
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("Verdadero/Falso");
					if (pnlUnidadMedida.Visible)
						pnlUnidadMedida.Visible = false;
					if (liOpciones.Visible == true)
						liOpciones.Visible = false;
					break;
				default:
					if (cbHabilitarNotificacionTarea.Checked)
						SetFieldsNotificacion("none");
					if (pnlUnidadMedida.Visible)
						pnlUnidadMedida.Visible = false;
					if (liOpciones.Visible == true)
						liOpciones.Visible = false;
					break;
			}
		}
		#endregion

		#region CheckBox
		protected void rbAsignarADepartamento_CheckedChanged(object sender, EventArgs e)
		{
			if (rbAsignarADepartamento.Checked)
			{
				rbAsignarAArea.Checked = false;
				pnlAsignacionDepartamentos.Visible = true;
				pnlAsignacionAreas.Visible = false;

				if (string.IsNullOrEmpty(hfUidTarea.Value))
				{
					this.LsDepartamentosAsignados = new List<Modelo.Departamento>();
					gvDepartamentosAsignados.DataSource = this.LsDepartamentosAsignados;
					gvDepartamentosAsignados.DataBind();
				}
				else
				{
					foreach (var item in this.LsDepartamentosAsignados)
					{
						item.ToDelete = false;
					}

					gvDepartamentosAsignados.DataSource = this.LsDepartamentosAsignados.FindAll(d => d.ToDelete == false);
					gvDepartamentosAsignados.DataBind();

					if (this.LsAreasAsignadas.Count > 0)
					{
						this.LsAreasAsignadas.RemoveAll(a => a.IsSaved == false);
						foreach (Modelo.Area item in this.LsAreasAsignadas)
						{
							if (item.IsSaved)
								item.ToDelete = true;
						}
					}
				}

				gvDepartamentosAsignacionBusqueda.DataSource = null;
				gvDepartamentosAsignacionBusqueda.DataBind();
			}
		}
		protected void rbAsignarAArea_CheckedChanged(object sender, EventArgs e)
		{
			if (rbAsignarAArea.Checked)
			{
				rbAsignarADepartamento.Checked = false;
				pnlAsignacionDepartamentos.Visible = false;
				pnlAsignacionAreas.Visible = true;
				gvDepartamentosAsignacionArea.Visible = true;
				gvAreasDisponiblesDepartamento.Visible = false;
				btnMostrarDepartamentosAsignacion.Visible = false;

				if (string.IsNullOrEmpty(hfUidTarea.Value))
				{
					this.LsAreasAsignadas = new List<Modelo.Area>();
					gvAreasAsignadas.DataSource = this.LsAreasAsignadas;
					gvAreasAsignadas.DataBind();
				}
				else
				{
					foreach (var item in this.LsAreasAsignadas)
					{
						item.ToDelete = false;
					}

					gvAreasAsignadas.DataSource = this.LsAreasAsignadas.FindAll(a => a.ToDelete == false);
					gvAreasAsignadas.DataBind();

					if (this.LsDepartamentosAsignados.Count > 0)
					{
						this.LsDepartamentosAsignados.RemoveAll(d => d.IsSaved == false);
						foreach (Modelo.Departamento item in this.LsDepartamentosAsignados)
						{
							if (item.IsSaved)
								item.ToDelete = true;
						}
					}
				}
			}
		}

		protected void rbSinPeriodicidad_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("SN");
		}
		protected void rbPeriodicidadDiaria_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Diaria");
		}
		protected void rbPeriodicidadSemanal_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Semanal");
		}
		protected void rbPeriodicidadMensual_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Mensual");
		}
		protected void rbPeriodicidadAnual_CheckedChanged(object sender, EventArgs e)
		{
			DisplayPeriodicityFields("Anual");
		}

		private void DisplayPeriodicityFields(string Type)
		{
			switch (Type)
			{
				case "Diaria":
					DisplayDiaria();
					hfTipoPeriodicidad.Value = "Diaria";
					break;
				case "Semanal":
					DisplaySemanal();
					hfTipoPeriodicidad.Value = "Semanal";
					break;
				case "Mensual":
					DisplayMesual();
					hfTipoPeriodicidad.Value = "Mensual";
					break;
				case "Anual":
					DisplayAnual();
					hfTipoPeriodicidad.Value = "Anual";
					break;
				default:
					DisplaySinPeriodicidad();
					hfTipoPeriodicidad.Value = "Sin periodicidad";
					break;
			}
		}
		private void DisplaySinPeriodicidad()
		{
			rbPeriodicidadDiaria.Checked = false;
			rbPeriodicidadSemanal.Checked = false;
			rbPeriodicidadMensual.Checked = false;
			rbPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = false;

			if (gvFechasPeriodicidad.Visible)
				gvFechasPeriodicidad.Visible = false;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved)
					item.ToDelete = true;
			}
		}
		private void DisplayDiaria()
		{
			//rbSinPeriodicidad.Checked = false;
			//rbPeriodicidadSemanal.Checked = false;
			//rbPeriodicidadMensual.Checked = false;
			//rbPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = true;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = false;

			LimpiarCamposPeriodicidadDiaria();

			if (gvFechasPeriodicidad.Visible)
				gvFechasPeriodicidad.Visible = false;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved)
					item.ToDelete = true;
			}
		}
		private void DisplaySemanal()
		{
			//rbSinPeriodicidad.Checked = false;
			//rbPeriodicidadDiaria.Checked = false;
			//rbPeriodicidadMensual.Checked = false;
			//rbPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = true;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = false;

			LimpiarCamposPeriodicidadSemanal();

			if (gvFechasPeriodicidad.Visible)
				gvFechasPeriodicidad.Visible = false;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved)
					item.ToDelete = true;
			}
		}
		private void DisplayMesual()
		{
			//rbSinPeriodicidad.Checked = false;
			//rbPeriodicidadDiaria.Checked = false;
			//rbPeriodicidadSemanal.Checked = false;
			//rbPeriodicidadAnual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = true;
			subpnlPeriodicidadAnual.Visible = false;

			LimpiarCamposPeriodicidadMensual();

			gvFechasPeriodicidad.Visible = true;
			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved && item.Frecuencia.Equals("Mensual") && item.Tipo.Equals("A"))
					item.ToDelete = false;
				else if (item.IsSaved)
					item.ToDelete = true;
			}
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}
		private void DisplayAnual()
		{
			//rbSinPeriodicidad.Checked = false;
			//rbPeriodicidadDiaria.Checked = false;
			//rbPeriodicidadSemanal.Checked = false;
			//rbPeriodicidadMensual.Checked = false;

			subpnlPeriodicidadDiaria.Visible = false;
			subpnlPeriodicidadSemanal.Visible = false;
			subpnlPeriodicidadMensual.Visible = false;
			subpnlPeriodicidadAnual.Visible = true;

			LimpiarCamposPeriodicidadAnual();

			gvFechasPeriodicidad.Visible = true;
			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved && item.Frecuencia.Equals("Anual") && item.Tipo.Equals("A"))
					item.ToDelete = false;
				else if (item.IsSaved)
					item.ToDelete = true;
			}
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}

		/// <summary>
		/// Opciones peridicidad Diaria
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkPDOpcion1_CheckedChanged(object sender, EventArgs e)
		{
			txtPDNumeroDias.Text = "1";
			chkPDOpcion2.Checked = false;
		}
		protected void chkPDOpcion2_CheckedChanged(object sender, EventArgs e)
		{
			chkPDOpcion1.Checked = false;
		}
		/// <summary>
		/// Opciones Periodicidad Mensual
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkPMRepetirCadaNDiasCadaNMes_CheckedChanged(object sender, EventArgs e)
		{
			txtPMDiaDelMes.Text = "1";
			txtPMNumeroDeMesesOpc1.Text = "1";
			chkPMRepetirCadaNMESConParametros.Checked = false;
			chkPMTipoC.Checked = false;

			btnAgregarFechaMensual.Visible = true;
			btnAgregarFechaMensualOpc2.Visible = false;
			gvFechasPeriodicidad.Visible = true;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved && item.Frecuencia.Equals("Mensual") && item.Tipo.Equals("A"))
					item.ToDelete = false;
				else if (item.IsSaved)
					item.ToDelete = true;
			}

			//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}
		protected void chkPMRepetirCadaNMESConParametros_CheckedChanged(object sender, EventArgs e)
		{
			chkPMRepetirCadaNDiasCadaNMes.Checked = false;
			chkPMTipoC.Checked = false;
			ddlPMListaNumerales.SelectedIndex = 0;
			ddlPMDiaSemanaOLapsoDias.SelectedIndex = 0;
			txtPMNumeroDeMesesOpc2.Text = "1";

			btnAgregarFechaMensual.Visible = false;
			btnAgregarFechaMensualOpc2.Visible = true;
			gvFechasPeriodicidad.Visible = true;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved && item.Frecuencia.Equals("Mensual") && item.Tipo.Equals("B"))
					item.ToDelete = false;
				else if (item.IsSaved)
					item.ToDelete = true;
			}

			//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}
		protected void chkPMTipoC_CheckedChanged(object sender, EventArgs e)
		{
			chkPMRepetirCadaNDiasCadaNMes.Checked = false;
			chkPMRepetirCadaNMESConParametros.Checked = false;

			txtPMDiasOpc3.Text = "2";
			ddlPMNumeralesOpc3.SelectedIndex = 0;
			txtPMMesesOpc3.Text = "1";

			btnAgregarFechaMensual.Visible = false;
			gvFechasPeriodicidad.Visible = false;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved)
					item.ToDelete = true;
			}
		}
		/// <summary>
		/// Opciones Periodicidad Anual
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void chkPAOpcion1_CheckedChanged(object sender, EventArgs e)
		{
			ddlPAListaMesesOpc1.SelectedIndex = 0;
			txtPADiaDelMesOpc1.Text = "1";
			chkPAOpcion2.Checked = false;

			btnAgregarFechaAnual.Visible = true;
			btnAgregarFechaAnualTipo2.Visible = false;
			gvFechasPeriodicidad.Visible = true;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved && item.Frecuencia.Equals("Anual") && item.Tipo.Equals("A"))
					item.ToDelete = false;
				else if (item.IsSaved)
					item.ToDelete = true;
			}

			//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}
		protected void chkPAOpcion2_CheckedChanged(object sender, EventArgs e)
		{
			ddlPAListaNumerales.SelectedIndex = 0;
			ddlPAListaPeriodosyDias.SelectedIndex = 0;
			ddlPAListaMesesOpc2.SelectedIndex = 0;
			chkPAOpcion1.Checked = false;

			btnAgregarFechaAnual.Visible = false;
			btnAgregarFechaAnualTipo2.Visible = true;
			gvFechasPeriodicidad.Visible = true;

			this.LsFechasPeriodicidad.RemoveAll(f => f.IsSaved == false);
			foreach (var item in this.LsFechasPeriodicidad)
			{
				if (item.IsSaved && item.Frecuencia.Equals("Anual") && item.Tipo.Equals("B"))
					item.ToDelete = false;
				else if (item.IsSaved)
					item.ToDelete = true;
			}

			//gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad.FindAll(f => f.ToDelete == false);
			gvFechasPeriodicidad.DataSource = this.LsFechasPeriodicidad;
			gvFechasPeriodicidad.DataBind();
		}

		/// <summary>
		/// Habilitar antecesor
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void cbActivarAntecesor_CheckedChanged(object sender, EventArgs e)
		{
			pnlCamposAntecesor.Visible = cbActivarAntecesor.Checked;
			if (cbActivarAntecesor.Checked)
			{
				hfAntecesorSeleccionado.Value = string.Empty;
				txtNombreAntecesor.Text = string.Empty;

				gvListaTareasAntecesor.Visible = false;
				gvListaTareasAntecesor.DataSource = null;
				gvListaTareasAntecesor.DataBind();

				txtDiasDespues.Text = "0";

				pnlNotificacionAntecesor.Visible = false;

				HabilitarCamposAntecesor(true);

				cbUsarNotificacionAntecesor.Enabled = false;
				cbUsarNotificacionAntecesor.Checked = false;
			}
		}
		/// <summary>
		/// Habilitar el uso de la notificacion de la tarea antecesora
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void cbUsarNotificacionAntecesor_CheckedChanged(object sender, EventArgs e)
		{
			pnlNotificacionAntecesor.Visible = cbUsarNotificacionAntecesor.Checked;
			if (cbUsarNotificacionAntecesor.Checked)
			{
				rdNumeroLimitado.Checked = true;
				txtMaximo.Text = "1";
				rdIlimitado.Checked = false;
			}
		}

		/// <summary>
		/// Notificacion de la tarea
		/// </summary>
		/// <param name="sender"></param>
		/// <param name="e"></param>
		protected void cbHabilitarNotificacionTarea_CheckedChanged(object sender, EventArgs e)
		{
			if (cbHabilitarNotificacionTarea.Checked)
			{
				pnlNotificationSendTo.Visible = true;
				SetFieldsNotificacion(ddlTipoMedicion.SelectedItem.ToString());
			}
			else
			{
				pnlNotificationSendTo.Visible = false;
				SetFieldsNotificacion("none");
			}
		}
		#endregion

		#region Section Fields
		private void HabilitarCamposDatosGenerales(bool Status)
		{
			txtNombreTarea.Enabled = Status;
			txtFechaInicio.Enabled = Status;
			txtFechaFin.Enabled = Status;
			txtDescripcionTarea.Enabled = Status;
			ddlEstatusTarea.Enabled = Status;
			ddlTipoTarea.Enabled = Status;
			ddlTipoMedicion.Enabled = Status;
			ddlUnidadMedida.Enabled = Status;
			CbFoto.Enabled = Status;
			cbHabilitarHoraTarea.Enabled = Status;
		}
		private void HabilitarCamposOpcionTarea(bool Status)
		{
			txtNombreOpcionTarea.Enabled = Status;
			txtOrdenOpcionTarea.Enabled = Status;
			cbActivoOpcionTarea.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidad(bool Status)
		{
			rbSinPeriodicidad.Enabled = Status;
			rbPeriodicidadDiaria.Enabled = Status;
			rbPeriodicidadSemanal.Enabled = Status;
			rbPeriodicidadMensual.Enabled = Status;
			rbPeriodicidadAnual.Enabled = Status;
		}
		private void HabilitarCamposAsignacion(bool Status)
		{
			rbAsignarADepartamento.Enabled = Status;
			rbAsignarAArea.Enabled = Status;
			txtNombreDepartamentoAsignacion.Enabled = Status;

			if (Status)
				btnBuscarDepartamentoAsignacion.CssClass = "btn btn-sm btn-default";
			else
				btnBuscarDepartamentoAsignacion.CssClass = "btn btn-sm btn-default disabled";
		}
		private void HabilitarCamposAntecesor(bool Status)
		{
			txtNombreAntecesor.Enabled = Status;
			btnBuscarAntecesor.Enabled = Status;
			btnCancelarBuscarAntecesor.Enabled = Status;

			txtDiasDespues.Enabled = Status;
			cbUsarNotificacionAntecesor.Enabled = Status; ;
			rdNumeroLimitado.Enabled = Status; ;
			txtMaximo.Enabled = Status;
			rdIlimitado.Enabled = Status;
		}
		private void HabilitarCamposNotificacion(bool Status)
		{
			cbSendToAdministrador.Enabled = Status;
			cbSendToSupervisor.Enabled = Status;

			rbVerdaderoNotificacion.Enabled = Status;
			rbFalsoNotificacion.Enabled = Status;

			cbMayor.Enabled = Status;
			cbMayorIgual.Enabled = Status;
			txtMayorQue.Enabled = Status;

			cbMenor.Enabled = Status;
			cbMenorIgual.Enabled = Status;
			txtMenorQue.Enabled = Status;

			lbNotificacionOpcionesSeleccionadas.Enabled = Status;
		}

		///<summary>
		///Limpiar Controles
		///</summary>
		private void LimpiarCamposDatosGenerales()
		{
			txtNombreTarea.Text = string.Empty;
			txtFechaInicio.Text = string.Empty;
			txtFechaFin.Text = string.Empty;
			txtDescripcionTarea.Text = string.Empty;
			if (ddlEstatusTarea.Items.Count > 0)
				ddlEstatusTarea.SelectedIndex = 0;
			if (ddlTipoTarea.Items.Count > 0)
				ddlTipoTarea.SelectedIndex = 0;
			if (ddlTipoMedicion.Items.Count > 0)
				ddlTipoMedicion.SelectedIndex = 0;
			if (ddlUnidadMedida.Items.Count > 0)
				ddlUnidadMedida.SelectedIndex = 0;

			CbFoto.Checked = false;
			cbHabilitarHoraTarea.Checked = false;
		}
		private void LimpiarCamposOpcionTarea()
		{
			txtNombreOpcionTarea.Text = string.Empty;
			txtOrdenOpcionTarea.Text = string.Empty;
			cbActivoOpcionTarea.Checked = false;
		}

		private void LimpiarCamposPeriodicidadDiaria()
		{
			chkPDOpcion1.Checked = false;
			txtPDNumeroDias.Text = "1";
			chkPDOpcion2.Checked = true;

			if (btnOk.Visible)
				HabilitarCamposPeriodicidadDiaria(true);
		}
		private void LimpiarCamposPeriodicidadSemanal()
		{
			txtPSNumeroSemanas.Text = "1";
			cblPSListaDias.ClearSelection();

			if (btnOk.Visible)
				HabilitarCamposPeriodicidadSemanal(true);
		}
		private void LimpiarCamposPeriodicidadMensual()
		{
			chkPMRepetirCadaNDiasCadaNMes.Checked = true;
			txtPMDiaDelMes.Text = "1";
			txtPMNumeroDeMesesOpc1.Text = "1";
			btnAgregarFechaMensual.Visible = true;

			chkPMRepetirCadaNMESConParametros.Checked = false;
			ddlPMListaNumerales.SelectedIndex = 0;
			ddlPMDiaSemanaOLapsoDias.SelectedIndex = 0;
			txtPMNumeroDeMesesOpc2.Text = "";
			btnAgregarFechaMensualOpc2.Visible = false;

			chkPMTipoC.Checked = false;
			ddlPMNumeralesOpc3.SelectedIndex = 0;
			txtPMDiasOpc3.Text = "2";
			txtPMMesesOpc3.Text = "1";

			if (btnOk.Visible)
				HabilitarCamposPeriodicidadMensual(true);

		}
		private void LimpiarCamposPeriodicidadAnual()
		{
			txtPANumeroAnios.Text = "1";
			chkPAOpcion1.Checked = true;
			ddlPAListaMesesOpc1.SelectedIndex = 0;
			txtPADiaDelMesOpc1.Text = "1";
			btnAgregarFechaAnual.Visible = true;

			chkPAOpcion2.Checked = false;
			ddlPAListaNumerales.SelectedIndex = 0;
			ddlPAListaPeriodosyDias.SelectedIndex = 0;
			ddlPAListaMesesOpc2.SelectedIndex = 0;
			btnAgregarFechaAnualTipo2.Visible = false;

			if (btnOk.Visible)
				HabilitarCamposPeriodicidadAnual(true);
		}
		private void LimpiarCamposAntecesor()
		{
			txtNombreAntecesor.Text = string.Empty;

			txtDiasDespues.Text = string.Empty;
			cbUsarNotificacionAntecesor.Checked = false;
			rdNumeroLimitado.Checked = false;
			txtMaximo.Text = string.Empty;
			rdIlimitado.Checked = false;
		}
		private void LimpiarCamposNotificacion()
		{
			rbVerdaderoNotificacion.Checked = false;
			rbFalsoNotificacion.Checked = false;

			cbMayor.Checked = false;
			cbMayorIgual.Checked = false;
			txtMayorQue.Text = string.Empty;

			cbMenor.Checked = false;
			cbMenorIgual.Checked = false;
			txtMenorQue.Text = string.Empty;

			lbNotificacionOpcionesSeleccionadas.ClearSelection();
		}

		private void HabilitarCamposPeriodicidadDiaria(bool Status)
		{
			chkPDOpcion1.Enabled = Status;
			txtPDNumeroDias.Enabled = Status;
			chkPDOpcion2.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidadSemanal(bool Status)
		{
			txtPSNumeroSemanas.Enabled = Status;
			cblPSListaDias.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidadMensual(bool Status)
		{
			chkPMRepetirCadaNDiasCadaNMes.Enabled = Status;
			txtPMDiaDelMes.Enabled = Status;
			txtPMNumeroDeMesesOpc1.Enabled = Status;

			if (Status)
				btnAgregarFechaMensual.CssClass = "btn btn-sm btn-default";
			else
				btnAgregarFechaMensual.CssClass = "btn btn-sm btn-default disabled";

			chkPMRepetirCadaNMESConParametros.Enabled = Status;
			ddlPMListaNumerales.Enabled = Status;
			ddlPMDiaSemanaOLapsoDias.Enabled = Status;
			txtPMNumeroDeMesesOpc2.Enabled = Status;

			chkPMTipoC.Enabled = Status;
			ddlPMNumeralesOpc3.Enabled = Status;
			txtPMDiasOpc3.Enabled = Status;
			txtPMMesesOpc3.Enabled = Status;
		}
		private void HabilitarCamposPeriodicidadAnual(bool Status)
		{
			txtPANumeroAnios.Enabled = Status;
			chkPAOpcion1.Enabled = Status;
			ddlPAListaMesesOpc1.Enabled = Status;
			txtPADiaDelMesOpc1.Enabled = Status;

			if (Status)
				btnAgregarFechaAnual.CssClass = "btn btn-sm btn-default";
			else
				btnAgregarFechaAnual.CssClass = "btn btn-sm btn-default disabled";

			chkPAOpcion2.Enabled = Status;
			ddlPAListaNumerales.Enabled = Status;
			ddlPAListaPeriodosyDias.Enabled = Status;
			ddlPAListaMesesOpc2.Enabled = Status;
		}
		#endregion
		#endregion

		#region Complements
		private void DisplayMessageBusqueda(string Message, string MessageType)
		{
			lblMessageSearch.Text = Message;
			pnlMessageSearch.CssClass = "alert alert-" + MessageType;
			pnlMessageSearch.Visible = true;
		}
		protected void HideMessageBusqueda(object sender, EventArgs e)
		{
			pnlMessageSearch.Visible = false;
		}

		private void DisplayMessageGeneral(string Message, string MessageType)
		{
			lblMessageGeneral.Text = Message;
			pnlMessageGeneral.CssClass = "alert alert-" + MessageType;
			pnlMessageGeneral.Visible = true;
		}
		protected void HideMessageGeneral(object sender, EventArgs e)
		{
			pnlMessageGeneral.Visible = false;
		}

		private void DisplayMessagePeriodicity(string Message, string MessageType)
		{
			lblMessagePeriodicity.Text = Message;
			pnlMessagePeriodicity.CssClass = "alert alert-" + MessageType;
			pnlMessagePeriodicity.Visible = true;
		}
		protected void HideMessagePeriodicity(object sender, EventArgs e)
		{
			pnlMessagePeriodicity.Visible = false;
		}

		private void DisplayMessageOpcionTarea(string Message, string MessageType)
		{
			lblMessageOptions.Text = Message;
			pnlMessageOptions.CssClass = "alert alert-" + MessageType;
			pnlMessageOptions.Visible = true;
		}
		protected void HideMessageOpcionTarea(object sender, EventArgs e)
		{
			pnlMessageOptions.Visible = false;
		}

		protected string GetListBoxValueSelected(ListBox Lb)
		{
			string Values = string.Empty;
			foreach (ListItem item in Lb.Items)
			{
				if (item.Selected)
				{
					if (Values == string.Empty)
						Values = item.Value;
					else
						Values += "," + item.Value;
				}
			}
			return Values;
		}

		public void SortListaTareas(string SortExpression, SortDirection SortDirection)
		{
			if (SortExpression == "Folio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.IntFolio).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.IntFolio).ToList();
				}
			}
			else if (SortExpression == "Nombre")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrNombre).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrNombre).ToList();
				}
			}
			else if (SortExpression == "Encargado")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrUsuario).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrUsuario).ToList();
				}
			}
			else if (SortExpression == "FechaInicio")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.DtFechaInicio).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.DtFechaInicio).ToList();
				}
			}
			else if (SortExpression == "Periodicidad")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrTipoFrecuencia).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrTipoFrecuencia).ToList();
				}
			}
			else if (SortExpression == "Departamento")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}
			else if (SortExpression == "Area")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					LsTareas = LsTareas.OrderBy(x => x.StrArea).ToList();
				}
				else
				{
					LsTareas = LsTareas.OrderByDescending(x => x.StrArea).ToList();
				}
			}

			gvTareas.DataSource = LsTareas;
		}

		public void Natigate(bool ToDatos, bool ToOpciones, bool ToPeriodos, bool ToAsignacion, bool ToAntecesor, bool ToNotificacion)
		{
			if (ToDatos)
			{
				liDatosGenerales.Attributes.Add("class", "active");
				liOpciones.Attributes.Add("class", "");
				liPeriodos.Attributes.Add("class", "");
				liAsignacion.Attributes.Add("class", "");
				liAntecesor.Attributes.Add("class", "");
				liNotificacion.Attributes.Add("class", "");

				pnlTabDatosGenerales.Visible = true;
				pnlTabOpciones.Visible = false;
				pnlTabPeriodos.Visible = false;
				pnlTabAsignacion.Visible = false;
				pnlTabAntecesor.Visible = false;
				pnlTabNotificacion.Visible = false;
			}
			else if (ToOpciones)
			{
				liDatosGenerales.Attributes.Add("class", "");
				liOpciones.Attributes.Add("class", "active");
				liPeriodos.Attributes.Add("class", "");
				liAsignacion.Attributes.Add("class", "");
				liAntecesor.Attributes.Add("class", "");
				liNotificacion.Attributes.Add("class", "");

				pnlTabDatosGenerales.Visible = false;
				pnlTabOpciones.Visible = true;
				pnlTabPeriodos.Visible = false;
				pnlTabAsignacion.Visible = false;
				pnlTabAntecesor.Visible = false;
				pnlTabNotificacion.Visible = false;
			}
			else if (ToPeriodos)
			{
				liDatosGenerales.Attributes.Add("class", "");
				liOpciones.Attributes.Add("class", "");
				liPeriodos.Attributes.Add("class", "active");
				liAsignacion.Attributes.Add("class", "");
				liAntecesor.Attributes.Add("class", "");
				liNotificacion.Attributes.Add("class", "");

				pnlTabDatosGenerales.Visible = false;
				pnlTabOpciones.Visible = false;
				pnlTabPeriodos.Visible = true;
				pnlTabAsignacion.Visible = false;
				pnlTabAntecesor.Visible = false;
				pnlTabNotificacion.Visible = false;
			}
			else if (ToAsignacion)
			{
				liDatosGenerales.Attributes.Add("class", "");
				liOpciones.Attributes.Add("class", "");
				liPeriodos.Attributes.Add("class", "");
				liAsignacion.Attributes.Add("class", "active");
				liAntecesor.Attributes.Add("class", "");
				liNotificacion.Attributes.Add("class", "");

				pnlTabDatosGenerales.Visible = false;
				pnlTabOpciones.Visible = false;
				pnlTabPeriodos.Visible = false;
				pnlTabAsignacion.Visible = true;
				pnlTabAntecesor.Visible = false;
				pnlTabNotificacion.Visible = false;
			}
			else if (ToAntecesor)
			{
				liDatosGenerales.Attributes.Add("class", "");
				liOpciones.Attributes.Add("class", "");
				liPeriodos.Attributes.Add("class", "");
				liAsignacion.Attributes.Add("class", "");
				liAntecesor.Attributes.Add("class", "active");
				liNotificacion.Attributes.Add("class", "");

				pnlTabDatosGenerales.Visible = false;
				pnlTabOpciones.Visible = false;
				pnlTabPeriodos.Visible = false;
				pnlTabAsignacion.Visible = false;
				pnlTabAntecesor.Visible = true;
				pnlTabNotificacion.Visible = false;
			}
			else if (ToNotificacion)
			{
				liDatosGenerales.Attributes.Add("class", "");
				liOpciones.Attributes.Add("class", "");
				liPeriodos.Attributes.Add("class", "");
				liAsignacion.Attributes.Add("class", "");
				liAntecesor.Attributes.Add("class", "");
				liNotificacion.Attributes.Add("class", "active");

				pnlTabDatosGenerales.Visible = false;
				pnlTabOpciones.Visible = false;
				pnlTabPeriodos.Visible = false;
				pnlTabAsignacion.Visible = false;
				pnlTabAntecesor.Visible = false;
				pnlTabNotificacion.Visible = true;
			}
		}

		///<summary>
		/// Habilitar Controler de Acuerdo al Tipo de medicion
		///</summary>
		private void SetFieldsNotificacion(string TipoMedicion)
		{
			HabilitarCamposNotificacion(true);
			LimpiarCamposNotificacion();

			switch (TipoMedicion)
			{
				case "Numerico":
					pnlNotificacionVerdaderoFalso.Visible = false;
					pnlNotificacionValor.Visible = true;
					pnlNotificacionOpcionMultiple.Visible = false;
					txtMayorQue.Text = "0";
					txtMenorQue.Text = "0";
					break;
				case "Seleccionable":
					pnlNotificacionVerdaderoFalso.Visible = false;
					pnlNotificacionValor.Visible = false;
					pnlNotificacionOpcionMultiple.Visible = true;
					List<Modelo.TareaOpcion> LsOpcionesTarea = (List<Modelo.TareaOpcion>)ViewState["OpcionesTarea"];
					lbNotificacionOpcionesSeleccionadas.DataSource = LsOpcionesTarea;
					lbNotificacionOpcionesSeleccionadas.DataValueField = "UidOpciones";
					lbNotificacionOpcionesSeleccionadas.DataTextField = "StrOpciones";
					lbNotificacionOpcionesSeleccionadas.DataBind();
					break;
				case "Verdadero/Falso":
					pnlNotificacionVerdaderoFalso.Visible = true;
					pnlNotificacionValor.Visible = false;
					pnlNotificacionOpcionMultiple.Visible = false;
					rbFalsoNotificacion.Checked = false;
					rbVerdaderoNotificacion.Checked = false;
					break;
				default:
					pnlNotificacionVerdaderoFalso.Visible = false;
					pnlNotificacionValor.Visible = false;
					pnlNotificacionOpcionMultiple.Visible = false;
					break;
			}
		}

		/// <summary>
		/// Validar si una cadena de texto contiene un valor numerico valido
		/// </summary>
		/// <param name="Value"></param>
		/// <returns></returns>
		public bool IsNumberValue(string Value)
		{
			bool Result = false;
			int Aux = 0;
			if (int.TryParse(Value, out Aux))
			{
				Result = true;
			}
			return Result;
		}
		#endregion

	}
}