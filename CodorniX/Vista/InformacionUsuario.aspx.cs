﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CodorniX.VistaDelModelo;

namespace CodorniX.Vista
{
	public partial class InformacionUsuario : System.Web.UI.Page
	{
		#region Properties
		private VMInformacionUsuario VmInformacionUsuario = new VMInformacionUsuario();
		private Modelo.Sesion SesionActual
		{
			get
			{
				return (Modelo.Sesion)Session["Sesion"];
			}
		}
		#endregion

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{
				Response.Redirect("Login.aspx");
				return;
			}

			if (!IsPostBack)
			{
				pnlMessageLeft.Visible = false;
				pnlMessageRight.Visible = false;

				btnAceptarDatosUsuario.Visible = false;
				btnCancelarDatosUsuario.Visible = false;
				HabilitarCamposDatosUsuario(false);

				btnAceptarDatosAcceso.Visible = false;
				btnCancelarDatosAcceso.Visible = false;
				HabilitarCamposDatosAcceso(false);

				this.RefrescarDatosCamposDatosUsuario();

				this.RefrescarDatosCamposDatosAcceso();
			}
		}

		#region Left
		#region Buttons
		protected void btnEditarDatosUsuario_Click(object sender, EventArgs e)
		{
			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			btnEditarDatosUsuario.CssClass = "btn btn-sm btn-default disabled";
			btnAceptarDatosUsuario.Visible = true;
			btnCancelarDatosUsuario.Visible = true;
			HabilitarCamposDatosUsuario(true);
		}
		protected void btnAceptarDatosUsuario_Click(object sender, EventArgs e)
		{
			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			if (string.IsNullOrEmpty(txtNombre.Text.Trim()))
			{
				DisplayMessageLeft("El <strong>nombre</strong> es un dato requerido", "warning");
				return;
			}

			if (!SesionActual.strIdNivelAcceso.Equals("SuperAdministrador"))
			{
				if (string.IsNullOrWhiteSpace(txtApellidoPaterno.Text.Trim()))
				{
					DisplayMessageLeft("El <strong>apellido paterno</strong> es un dato requerido", "warning");
					return;
				}

				if (string.IsNullOrWhiteSpace(txtApellidoMaterno.Text.Trim()))
				{
					DisplayMessageLeft("El <strong>apellido materno</strong> es un dato requerido", "warning");
					return;
				}
			}

			if (string.IsNullOrWhiteSpace(txtFechaNacimiento.Text.Trim()))
			{
				DisplayMessageLeft("La <strong>fecha de nacimiento</strong> es un dato requerido", "warning");
				return;
			}

			string Nombre = txtNombre.Text.Trim();
			string ApellidoPaterno = txtApellidoPaterno.Text.Trim();
			string ApellidoMaterno = txtApellidoMaterno.Text.Trim();
			DateTime DtFechaNacimiento;
			try
			{
				DtFechaNacimiento = Convert.ToDateTime(DateTime.ParseExact(txtFechaNacimiento.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				DisplayMessageLeft("Ingrese una <strong>fecha de nacimiento</strong> valida.", "danger");
				return;
			}
			string CorreoElectronico = txtCorreoElectronico.Text.Trim();

			try
			{
				if (VmInformacionUsuario.ActualizarDatos(SesionActual.uidUsuario, Nombre, ApellidoPaterno, ApellidoMaterno, DtFechaNacimiento, CorreoElectronico))
				{
					DisplayMessageLeft("Datos actualizados.", "success");
				}
				else
				{
					DisplayMessageLeft("Ocurrio un error al actualizar los datos.", "danger");
				}
			}
			catch (Exception ex)
			{
				DisplayMessageLeft("Error inesperado: </br>" + ex.Message, "danger");
				return;
			}

			btnEditarDatosUsuario.CssClass = "btn btn-sm btn-default";
			btnAceptarDatosUsuario.Visible = false;
			btnCancelarDatosUsuario.Visible = false;
			HabilitarCamposDatosUsuario(false);
		}
		protected void btnCancelarDatosUsuario_Click(object sender, EventArgs e)
		{
			if (pnlMessageLeft.Visible)
				pnlMessageLeft.Visible = false;

			btnEditarDatosUsuario.CssClass = "btn btn-sm btn-default";
			btnAceptarDatosUsuario.Visible = false;
			btnCancelarDatosUsuario.Visible = false;
			HabilitarCamposDatosUsuario(false);

			RefrescarDatosCamposDatosUsuario();
		}

		protected void btnNone_Click(object sender, EventArgs e)
		{
			btnEditarDatosUsuario.CssClass = "btn btn-sm btn-default";
			btnAceptarDatosUsuario.Visible = false;
			btnCancelarDatosUsuario.Visible = false;
			HabilitarCamposDatosUsuario(false);
		}

		protected void btnCloseMessageLeft(object sender, EventArgs e)
		{
			pnlMessageLeft.Visible = false;
		}
		#endregion

		#region Fields
		private void RefrescarDatosCamposDatosUsuario()
		{
			VmInformacionUsuario.GetUsuario(SesionActual.uidUsuario);
			txtNombre.Text = VmInformacionUsuario.uUsuario.STRNOMBRE;
			txtApellidoPaterno.Text = VmInformacionUsuario.uUsuario.STRAPELLIDOPATERNO;
			txtApellidoMaterno.Text = VmInformacionUsuario.uUsuario.STRAPELLIDOMATERNO;
			txtFechaNacimiento.Text = VmInformacionUsuario.uUsuario.DtFechaNacimiento.ToString("dd/MM/yyyy");
			txtCorreoElectronico.Text = VmInformacionUsuario.uUsuario.STRCORREO;
		}
		private void HabilitarCamposDatosUsuario(bool IsEnabled)
		{
			txtNombre.Enabled = IsEnabled;
			txtApellidoPaterno.Enabled = IsEnabled;
			txtApellidoMaterno.Enabled = IsEnabled;
			txtFechaNacimiento.Enabled = IsEnabled;
			txtCorreoElectronico.Enabled = IsEnabled;
		}

		private void DisplayMessageLeft(string Message, string Type)
		{
			lblMessageLeft.Text = Message;
			pnlMessageLeft.CssClass = "alert alert-" + Type;
			pnlMessageLeft.Visible = true;
		}
		#endregion
		#endregion

		#region Right
		#region Buttons
		protected void btnEditarDatosAcceso_Click(object sender, EventArgs e)
		{
			if (pnlMessageRight.Visible)
				pnlMessageRight.Visible = false;

			btnEditarDatosAcceso.CssClass = "btn btn-sm btn-default disabled";
			btnAceptarDatosAcceso.Visible = true;
			btnCancelarDatosAcceso.Visible = true;
			HabilitarCamposDatosAcceso(true);
		}
		protected void btnAceptarDatosAcceso_Click(object sender, EventArgs e)
		{
			if (pnlMessageRight.Visible)
				pnlMessageRight.Visible = false;

			if (string.IsNullOrWhiteSpace(txtUsuario.Text))
			{
				DisplayMessageRight("El <strong>Usuario</strong> es un dato requerido.", "warning");
				return;
			}

			if (string.IsNullOrWhiteSpace(txtContraseniaActual.Text))
			{
				DisplayMessageRight("La <strong>Contraseña Actual</strong> es un dato requerido.", "warning");
				return;
			}

			if (string.IsNullOrWhiteSpace(txtContraseniaNueva.Text))
			{
				DisplayMessageRight("La <strong>Nueva Contraseña</strong> es un dato requerido.", "warning");
				return;
			}

			if (string.IsNullOrWhiteSpace(txtContraseniaNueva2.Text))
			{
				DisplayMessageRight("Confirmacion de <strong>contraseña</strong> requerida.", "warning");
				return;
			}

			if (!VmInformacionUsuario.ValidarContraseñaActual(SesionActual.uidUsuario, txtContraseniaActual.Text.Trim()))
			{
				DisplayMessageRight("La <strong>contraseña actual</strong> ingresada es invalida.", "danger");
				return;
			}

			if (!txtContraseniaNueva.Text.Trim().Equals(txtContraseniaNueva2.Text.Trim()))
			{
				DisplayMessageRight("La <strong>nueva contraseña</strong> no coincide.", "danger");
				return;
			}

			if (txtContraseniaNueva.Text.Trim().Length < 8)
			{
				DisplayMessageRight("La <strong>nueva contraseña</strong> debe tener minimo (8) caracteres.", "danger");
				return;
			}

			try
			{
				if (VmInformacionUsuario.UpdatePassword(SesionActual.uidUsuario, txtContraseniaNueva.Text.Trim()))
				{
					DisplayMessageRight("Contraseña actualizada.", "success");
				}
				else
				{
					DisplayMessageRight("Ocurrio un error al actualizar contraseña.", "danger");
				}
			}
			catch (Exception ex)
			{
				DisplayMessageRight("Error inesperado: </br>" + ex.Message, "danger");
				return;
			}
			btnEditarDatosAcceso.CssClass = "btn btn-sm btn-default";
			btnAceptarDatosAcceso.Visible = false;
			btnCancelarDatosAcceso.Visible = false;
			HabilitarCamposDatosAcceso(false);

			RefrescarDatosCamposDatosAcceso();
		}
		protected void btnCancelarDatosAcceso_Click(object sender, EventArgs e)
		{
			if (pnlMessageRight.Visible)
				pnlMessageRight.Visible = false;

			btnEditarDatosAcceso.CssClass = "btn btn-sm btn-default";
			btnAceptarDatosAcceso.Visible = false;
			btnCancelarDatosAcceso.Visible = false;
			HabilitarCamposDatosAcceso(false);

			RefrescarDatosCamposDatosAcceso();
		}

		protected void btnCloseMessageRight(object sender, EventArgs e)
		{
			pnlMessageRight.Visible = false;
		}
		#endregion

		#region Fields
		private void RefrescarDatosCamposDatosAcceso()
		{
			VmInformacionUsuario.GetUsuario(SesionActual.uidUsuario);
			txtUsuario.Text = VmInformacionUsuario.uUsuario.STRUSUARIO;

			txtContraseniaActual.Text = string.Empty;
			txtContraseniaNueva.Text = string.Empty;
			txtContraseniaNueva2.Text = string.Empty;
		}
		private void HabilitarCamposDatosAcceso(bool IsEnabled)
		{
			txtUsuario.Enabled = IsEnabled;
			txtContraseniaActual.Enabled = IsEnabled;
			txtContraseniaNueva.Enabled = IsEnabled;
			txtContraseniaNueva2.Enabled = IsEnabled;
		}
		private void LimpiarCamposContrasenia()
		{
			txtContraseniaActual.Text = string.Empty;
			txtContraseniaNueva.Text = string.Empty;
			txtContraseniaNueva2.Text = string.Empty;
		}

		private void DisplayMessageRight(string Message, string Type)
		{
			lblMessageRight.Text = Message;
			pnlMessageRight.CssClass = "alert alert-" + Type;
			pnlMessageRight.Visible = true;
		}
		#endregion
		#endregion
	}
}