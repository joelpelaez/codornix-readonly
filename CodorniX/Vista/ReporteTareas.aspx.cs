﻿using CodorniX.Modelo;
using CodorniX.VistaDelModelo;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class ReporteTareas : System.Web.UI.Page
	{
		VMReporteTarea VM = new VMReporteTarea();

		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
				return;

			if (!Acceso.TieneAccesoAModulo("ReporteTareas", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				ReportViewer1.SizeToReportContent = true;
				ReportViewer1.Width = Unit.Percentage(100);
				ReportViewer1.Height = Unit.Percentage(100);
				Guid periodo;
				DateTime date;
				double Promedio = 0;
				string horaInicio, horaFin, Replica, FolioTurno, ResumenCalificacion;
				try
				{
					periodo = (Guid)Session["Periodo"];
					date = (DateTime)Session["Fecha"];
					horaInicio = (string)Session["HoraInicio"];
					horaFin = (string)Session["HoraFin"];
					Replica = (string)Session["Replica"];
					FolioTurno = Session["FolioTurno"] == null ? "0000" : ((int)Session["FolioTurno"]).ToString("0000");
				}
				catch
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}

				VM.ObtenerPeriodo(periodo);
				VM.ObtenerTurno(VM.Periodo.UidTurno);
				VM.ObtenerDepartamento(VM.Periodo.UidDepartamento);
				VM.ObtenerSucursal(VM.Departamento.UidSucursal);
				VM.ObtenerEmpresa(VM.Sucursal.UidEmpresa);
				VM.ObtenerEncargado(SesionActual.uidUsuario);
				VM.ObtenerTareas(SesionActual.uidUsuario, periodo, date);
				VM.ObtenerResumenCalificacion(VM.Periodo.UidDepartamento, date);
				ResumenCalificacion = string.Join("     ", VM.LsCalificaciones.Select(c => c.Resumen));
				float Total = VM.LsCalificaciones.Sum(c => c.FlValor * c.Total);
				int TotalItems = VM.LsCalificaciones.Sum(c => c.Total);

				if (Total == 0 && TotalItems == 0)
					Promedio = 0;
				else
					Promedio = (Total / TotalItems);


				ProcessingData(VM.Cumplimientos);

				ReportViewer1.LocalReport.ReportPath = Server.MapPath("~/Reports/ReporteTareas2.rdlc");
				ReportParameter[] param = new ReportParameter[13];
				string nombreCompleto = VM.Encargado.STRNOMBRE + " " + VM.Encargado.STRAPELLIDOPATERNO + " " + VM.Encargado.STRAPELLIDOMATERNO;
				param[0] = new ReportParameter("NombreEncargado", nombreCompleto);
				param[1] = new ReportParameter("NombreDepartamento", VM.Departamento.StrNombre);
				param[2] = new ReportParameter("Fecha", date.ToString("dd/MM/yyyy"));
				param[3] = new ReportParameter("NombreEmpresa", VM.Empresa.StrNombreComercial);
				param[4] = new ReportParameter("NombreSucursal", VM.Sucursal.StrNombre);
				param[5] = new ReportParameter("Turno", VM.Turno.StrTurno);
				param[6] = new ReportParameter("HoraInicio", horaInicio);
				param[7] = new ReportParameter("HoraFin", horaFin);
				param[8] = new ReportParameter("FechaReporte", DateTime.Today.ToString("dd/MM/yyyy"));
				param[9] = new ReportParameter("Replica", Replica);
				param[10] = new ReportParameter("FolioTurno", FolioTurno);
				param[11] = new ReportParameter("ResumenCalificacion", ResumenCalificacion);
				param[12] = new ReportParameter("Promedio", Promedio.ToString("N2"));
				ReportViewer1.LocalReport.SetParameters(param);
				ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("DataSet1", VM.Cumplimientos));
				ReportViewer1.LocalReport.Refresh();
			}
		}

		private List<Modelo.Cumplimiento> ProcessingData(List<Modelo.Cumplimiento> originalList)
		{
			List<Modelo.Cumplimiento> transformed = new List<Modelo.Cumplimiento>(originalList.Count);

			for (int i = 0; i < originalList.Count; i++)
			{
				var current = originalList[i];
				if (string.IsNullOrEmpty(current.StrArea))
					originalList[i].StrArea = "General";

				if (current.StrEstadoCumplimiento != "Completo")
					continue;

				switch (current.StrTipoMedicion)
				{
					case "Verdadero/Falso":
						originalList[i].StrValor = current.BlValor.Value ? "Sí" : "No";
						break;
					case "Numerico":
						originalList[i].StrValor = current.DcValor1.Value.ToString() + " " + current.StrTipoUnidad;
						break;
					case "Seleccionable":
						originalList[i].StrValor = current.StrOpcion;
						break;
					case "Sin medición":
						originalList[i].StrValor = "N/A";
						break;
				}
			}

			return transformed;
		}
	}
}