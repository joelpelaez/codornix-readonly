﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="InicioSupervisor.aspx.cs" Inherits="CodorniX.Vista.InicioSupervisor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Supervisión</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-sm-12 col-md-6 col-xs-6 ">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="text-center">
						Turno de Supervisor
					</div>
				</div>
				<div class="panel-body panel-pd">
					<asp:Panel runat="server" ID="pnlFiltrosBusqueda">
						<div class="text-right">
							<div class="btn-group">
								<asp:LinkButton ID="btnActualizar" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnActualizar_Click">
                            Actualizar
								</asp:LinkButton>
							</div>
						</div>
						<div class="col-md-12 pd-0">
							<div class="col-md-3 pd-left-right-5">
								<h5>Folio</h5>
								<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFolio"/>
							</div>
							<div class="col-md-3 pd-left-right-5">
								<h5>Fecha Inicio</h5>
								<div class="input-group date extra">
									<asp:TextBox ID="txtFiltroFechaInicio" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-3 pd-left-right-5">
								<h5>Fecha Fin</h5>
								<div class="input-group date extra">
									<asp:TextBox ID="txtFiltroFechaFin" CssClass="form-control" runat="server" />
									<span class="input-group-addon input-sm ">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlListaTurnos">
						<div class="row">
							<div class="col-xs-12">
								<asp:Panel runat="server" ID="pnlAlertList">
									<div class="alert alert-danger">
										<asp:Label runat="server" ID="lblMessage"></asp:Label>
									</div>
								</asp:Panel>
								<asp:GridView ID="dvgTurnos" runat="server" CssClass="table table-bordered table-sm table-condensed margin-top-10" AutoGenerateColumns="false" OnRowDataBound="dvgTurnos_RowDataBound" OnSelectedIndexChanged="dvgTurnos_SelectedIndexChanged" DataKeyNames="UidTurnoSupervisor">
									<EmptyDataTemplate>No hay Historial</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="IntFolio" HeaderText="Folio" />
										<asp:BoundField DataField="Usuario" HeaderText="Usuario" />
										<asp:BoundField DataField="FechaInicio" HeaderText="Inicio" />
										<asp:BoundField DataField="FechaFin" HeaderText="Fin" />
										<asp:BoundField DataField="StrEstadoTurno" HeaderText="Estado" />
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlResumenTarea">
						<p class="text-right">
							<asp:LinkButton runat="server" ID="btnCerrarPanelResumenTarea" OnClick="btnCerrarPanelResumenTarea_Click" CssClass="btn btn-sm btn-danger">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</p>
						<h5 class="font-bold">Datos Tarea</h5>
						<div class="col-md-12 pd-left-right-5">
							<h5>Tarea.</h5>
							<asp:TextBox runat="server" ID="txtDetailReviewTarea" CssClass="form-control input-sm" Enabled="false" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Departamento.</h5>
							<asp:TextBox runat="server" ID="txtDetailReviewDepartamento" CssClass="form-control input-sm" Enabled="false" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Area.</h5>
							<asp:TextBox runat="server" ID="txtDetailReviewArea" CssClass="form-control input-sm" Enabled="false" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Valor Ingresado.</h5>
							<asp:TextBox runat="server" ID="txtDetailValorIngresado" CssClass="form-control input-sm" Enabled="false" />
						</div>
						<h5 class="font-bold">Revision</h5>
						<div class="col-md-6 pd-left-right-5">
							<h5>Valor Evaluado</h5>
							<asp:Panel runat="server" ID="pnlDetailReviewTrueFalse">
								<label class="radio-inline">
									<asp:RadioButton runat="server" GroupName="rbgSiNo" Enabled="false" ID="rbDetailReviewYes" />
									Sí
								</label>
								<label class="radio-inline">
									<asp:RadioButton runat="server" GroupName="rbgSiNo" Enabled="false" ID="rbDetailReviewNo" />
									No
								</label>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlDetailReviewSelection">
								<asp:DropDownList runat="server" ID="ddlDetailReviewOptions" Enabled="false" CssClass="form-control input-sm">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlDetailReviewValue">
								<div class="input-group">
									<asp:TextBox runat="server" CssClass="form-control input-sm" Enabled="false" ID="txtDetailReviewValue" />
									<div class="input-group-addon input-sm">
										<asp:Label runat="server" ID="lblDetailReviewUnidadMedida" />
									</div>
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h5>Calificación</h5>
							<asp:DropDownList runat="server" ID="ddlDetailReviewCalificacion" CssClass="form-control input-sm" Enabled="false">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:DropDownList>
						</div>
						<div class="col-md-12 pd-left-right-5">
							<h5>Notas</h5>
							<asp:TextBox runat="server" ID="txtDetailReviewNotas" TextMode="MultiLine" CssClass="form-control input-sm" Enabled="false" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-sm-12 col-md-6 col-xs-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Datos Generales
				</div>
				<div class="panel-body panel-pd">
					<div class="row">
						<div class="col-xs-6">
							<div class="text-left">
								<div class="btn-group">
									<asp:LinkButton ID="btnReporteSupervisor" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnReporteSupervisor_Click">
                                    Reporte Supervisión
									</asp:LinkButton>
									<asp:LinkButton ID="btnResumenSupervision" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnResumenSupervision_Click">
                                    Resumen Supervisión
									</asp:LinkButton>
									<asp:HiddenField runat="server" ID="hfUidTurnoSupervisor" />
								</div>
							</div>
						</div>
						<div class="col-xs-6">
							<div class="text-right">
								<div class="btn-group">
									<asp:LinkButton ID="btnIniciarTurno" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnIniciarTurno_Click">
                                    Iniciar Turno
									</asp:LinkButton>
									<asp:LinkButton ID="btnCerrarTurno" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnCerrarTurno_Click">
                                    Cerrar Turno
									</asp:LinkButton>
								</div>
							</div>
						</div>
					</div>
					<ul class="nav nav-tabs">
						<li id="liResumen" runat="server">
							<asp:LinkButton Text="Resumen" runat="server" OnClick="TabResumen" />
						</li>
						<li id="liRevisiones" runat="server">
							<asp:LinkButton Text="Revisiones" runat="server" OnClick="TabRevisiones" />
						</li>
					</ul>
					<asp:Panel runat="server" ID="pnlResumenTurno">
						<div class="col-md-12 pd-left-right-5">
							<h5>Sucursal</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="txtSucursal" />
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h5>Inicio</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="txtFechaInicio" />
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h5>Fin</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="txtFechaFin" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Estado</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="txtEstadoTurno" />
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlListaRevisiones">
						<h5 class="font-bold">Tareas Revisadas</h5>
						<asp:HiddenField runat="server" ID="hfSortDirectionTareasRevisadas" Value="ASC" />
						<asp:GridView runat="server" ID="gvTareasRevisadas" DataKeyNames="UidRevision" OnRowDataBound="gvTareasRevisadas_RowDataBound" OnSelectedIndexChanging="gvTareasRevisadas_SelectedIndexChanging" OnSelectedIndexChanged="gvTareasRevisadas_SelectedIndexChanged" AllowSorting="true" OnSorting="gvTareasRevisadas_Sorting" AllowPaging="true" PageSize="7" OnPageIndexChanging="gvTareasRevisadas_PageIndexChanging" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm" SelectedRowStyle-BackColor="#dff0d8">
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
								<asp:BoundField HeaderText="F.C." DataField="FolioCumplimiento" DataFormatString="{0:0000}" SortExpression="FC" />
								<asp:BoundField HeaderText="F.T." DataField="FolioTarea" DataFormatString="{0:0000}" SortExpression="FT" />
								<asp:BoundField HeaderText="Tarea" DataField="StrTarea" SortExpression="Tarea" />
								<asp:BoundField HeaderText="Departamento" DataField="StrDepartamento" SortExpression="Depto" />
								<asp:BoundField HeaderText="Area" DataField="StrArea" SortExpression="Area" />
							</Columns>
							<EmptyDataTemplate>
								<div class="alert alert-info">
									No hay tareas revisadas en este turno.
								</div>
							</EmptyDataTemplate>
							<PagerSettings Mode="NumericFirstLast" Position="Bottom" PageButtonCount="4" />
							<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
						</asp:GridView>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>

	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>

	<script>
		//<![CDATA[
		function pageLoad() {
			prepareFechaAll();
		}
        //]]>
	</script>
</asp:Content>
