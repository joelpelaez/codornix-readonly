﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class HistoricoCumplimiento : System.Web.UI.Page
	{
		private List<Modelo.Cumplimiento> VSTareas
		{
			get => ViewState["Tareas"] as List<Modelo.Cumplimiento>;
			set => ViewState["Tareas"] = value;
		}
		public List<Modelo.ParametroOrdenamiento> LsOrdenamientos
		{
			get => ViewState["Ordenamiento"] as List<ParametroOrdenamiento>;
			set => ViewState["Ordenamiento"] = value;
		}

		private VMHistoricoCumplimiento VM = new VMHistoricoCumplimiento();
		private Sesion SesionActual => (Sesion)Session["Sesion"];

		private bool ModoSupervisor
		{
			get
			{
				if (ViewState["ModoSupervisor"] == null)
					ViewState["ModoSupervisor"] = false;

				return (bool)ViewState["ModoSupervisor"];
			}

			set => ViewState["ModoSupervisor"] = value;
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			// No hacer nada si no tiene algun turno iniciado. Master se hace cargo de ello.
			if (SesionActual == null)
				return;

			// No permite acceder en caso de no iniciar una sucursal (solo para administradores).
			if (!SesionActual.uidSucursalActual.HasValue)
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (SesionActual.perfil == "Supervisor" || SesionActual.perfil == "Administrador")
			{

				// El supervisor o administrador puede acceder a casi todos los cumplimientos
				ModoSupervisor = true;
			}
			else
			{
				if (SesionActual.UidPeriodo == null)
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}
			}

			// Verificar permisos
			if (!Acceso.TieneAccesoAModulo("HistoricoCumplimiento", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!IsPostBack)
			{
				frmSiNo.Visible = false;
				frmValue.Visible = false;
				frmTwoValues.Visible = false;
				frmOptions.Visible = false;

				btnMostrar.Text = "Mostrar";
				panelBusqueda.Visible = true;
				panelResultados.Visible = false;
				btnLimpiar.Visible = true;
				btnActualizar.Visible = true;

				VM.ObtenerEstados();
				lbEstados.DataSource = VM.Estados;
				lbEstados.DataTextField = "StrEstadoCumplimiento";
				lbEstados.DataValueField = "UidEstadoCumplimiento";
				lbEstados.DataBind();

				pnlFiltrosAdministrador.Visible = false;

				if (SesionActual.Rol.Equals("Administrador"))
				{
					VM.ObtenerDepartamentosSucursal(SesionActual.uidSucursalActual.Value);

					pnlFiltrosAdministrador.Visible = true;
					VM.ObtenerEncargadosSucursal(SesionActual.uidSucursalActual.Value);
					ddlFiltroEncargado.DataSource = VM.LsEncargados;
					ddlFiltroEncargado.DataTextField = "STRNOMBRE";
					ddlFiltroEncargado.DataValueField = "UIDUSUARIO";
					ddlFiltroEncargado.DataBind();
				}
				else
				{
					ddlFiltroEncargado.DataSource = null;
					ddlFiltroEncargado.DataBind();

					if (SesionActual.UidPeriodos.Count > 0)
						VM.ObtenerDepartamentos(SesionActual.UidPeriodos);
					else if (SesionActual.UidPeriodo.Value != Guid.Empty)
						VM.ObtenerDepartamentoPeriodo(SesionActual.UidPeriodo.Value);
				}

				if (VM.Departamentos == null)
					VM.Departamentos = new List<Departamento>();
				ddDepartamentos.DataSource = VM.Departamentos;
				ddDepartamentos.DataTextField = "StrNombre";
				ddDepartamentos.DataValueField = "UidDepartamento";
				ddDepartamentos.SelectedIndex = 0;
				ddDepartamentos.DataBind();

				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();

				VM.ObtenerTiposTarea();
				TipoTarea tipo = new TipoTarea();
				tipo.UidTipoTarea = Guid.Empty;
				tipo.StrTipoTarea = "Todos";
				VM.TiposTarea.Insert(0, tipo);
				ddTipo.DataSource = VM.TiposTarea;
				ddTipo.DataValueField = "UidTipoTarea";
				ddTipo.DataTextField = "StrTipoTarea";
				ddTipo.SelectedIndex = 0;
				ddTipo.DataBind();

				DateTimeOffset time = Hora.ObtenerHoraServidor();
				DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(SesionActual.uidSucursalActual.Value));
				var local = horaLocal.DateTime;

				txtFechaInicio.Text = txtFechaFin.Text = local.ToString("dd/MM/yyyy");

				pnlAlertBusquedas.Visible = false;
				VM.CargarParametrosOrdenamiento();

				pnlOrdenamiento.Visible = false;
				this.LsOrdenamientos = VM.LsOrdenamiento;
				gvOrdenamiento.DataSource = VM.LsOrdenamiento.OrderBy(o => o.Posicion);
				gvOrdenamiento.DataBind();
			}
		}

		protected void dgvTareasPendientes_SelectedIndexChanged(object sender, EventArgs e)
		{
			DateTime? proximo;
			Guid uidTarea = new Guid(dgvTareasPendientes.SelectedDataKey.Value.ToString());
			Guid uidDepto = new Guid(dgvTareasPendientes.SelectedDataKey.Values[1].ToString());
			Guid uidArea = new Guid(dgvTareasPendientes.SelectedDataKey.Values[2].ToString());
			Guid uidCumplimiento = new Guid(dgvTareasPendientes.SelectedDataKey.Values[3].ToString());
			Guid uidPeriodo = new Guid(dgvTareasPendientes.SelectedDataKey.Values[4].ToString());

			fldUidTarea.Value = uidTarea.ToString();
			fldUidDepartamento.Value = uidDepto.ToString();
			fldUidArea.Value = uidArea.ToString();
			fldUidCumplimiento.Value = uidCumplimiento.ToString();
			fldUidPerido.Value = uidPeriodo.ToString();

			VM.ObtenerCumplimiento(uidCumplimiento);
			VM.ObtenerTarea(uidTarea);
			VM.ObtenerDepartamento(uidDepto);
			VM.ObtenerArea(uidArea);
			VM.ObtenerPeriocidad(VM.Tarea.UidPeriodicidad);
			VM.ObtenerTipoFrecuencia(VM.Periodicidad.UidTipoFrecuencia);

			lblPeriodicidad.Text = VM.TipoFrecuencia.StrTipoFrecuencia;

			proximo = VM.ObtenerFechaSiguienteTarea(uidTarea, DateTime.Now);


			lblTarea.Text = VM.Tarea.StrNombre;
			lblDepto.Text = VM.Departamento.StrNombre;
			lblArea.Text = VM.Area == null ? "(global)" : VM.Area.StrNombre;

			frmSiNo.Visible = false;
			frmValue.Visible = false;
			frmTwoValues.Visible = false;
			frmOptions.Visible = false;

			rbYes.Checked = false;
			rbNo.Checked = false;
			txtValor.Text = string.Empty;
			txtValor1.Text = string.Empty;
			txtValor2.Text = string.Empty;
			rbYes.Disable();
			rbNo.Disable();
			txtValor.Disable();
			ddOpciones.Disable();
			txtObservaciones.Disable();

			switch (VM.Tarea.StrTipoMedicion)
			{
				case "Verdadero/Falso":
					frmSiNo.Visible = true;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.BlValor.HasValue)
						{
							if (VM.Cumplimiento.BlValor.Value)
							{
								rbYes.Checked = true;
								rbNo.Checked = false;
							}
							else
							{
								rbNo.Checked = true;
								rbYes.Checked = false;
							}
						}
					}
					break;
				case "Numerico":
					frmValue.Visible = true;
					lblUnidad.Text = VM.Tarea.StrUnidadMedida;
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.DcValor1.HasValue)
						{
							txtValor.Text = VM.Cumplimiento.DcValor1.Value.ToString("0.####");
						}
					}
					else
					{
						txtValor.Text = "";
					}
					break;
				case "Seleccionable":
					VM.ObtenerOpcionesDeTarea(uidTarea);
					frmOptions.Visible = true;
					ddOpciones.DataSource = VM.Opciones;
					ddOpciones.DataValueField = "UidOpciones";
					ddOpciones.DataTextField = "StrOpciones";
					ddOpciones.DataBind();
					if (VM.Cumplimiento != null)
					{
						if (VM.Cumplimiento.UidOpcion.HasValue)
						{
							ddOpciones.SelectedValue = VM.Cumplimiento.UidOpcion.Value.ToString();
						}
					}
					break;
			}

			lblFechaCumplimiento.Text = VM.Cumplimiento.DtFechaHora?.ToString("dd/MM/yyyy HH:mm:ss");

			if (!string.IsNullOrWhiteSpace(VM.Cumplimiento.StrObservacion))
				txtObservaciones.Text = VM.Cumplimiento.StrObservacion;
			else
				txtObservaciones.Text = string.Empty;

			int pos = -1;
			if (ViewState["CumplPreviousRow"] != null)
			{
				pos = (int)ViewState["CumplPreviousRow"];
				GridViewRow previousRow = dgvTareasPendientes.Rows[pos];
				previousRow.RemoveCssClass("success-forced");
			}

			ViewState["CumplPreviousRow"] = dgvTareasPendientes.SelectedIndex;
			dgvTareasPendientes.SelectedRow.AddCssClass("success-forced");

		}

		protected void dgvTareasPendientes_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvTareasPendientes, "Select$" + e.Row.RowIndex);

				Label icon = e.Row.FindControl("lblCompleto") as Label;

				if (e.Row.Cells[6].Text == "&nbsp;")
				{
					e.Row.Cells[6].Text = "(global)";
				}

				if (e.Row.Cells[6].Text == "&nbsp;")
				{
					e.Row.Cells[6].Text = "N/A";
				}

				if (e.Row.Cells[9].Text == "Requerida")
				{
					e.Row.AddCssClass("font-bold");
					e.Row.Cells[4].Text += "*";
				}

				if (e.Row.Cells[8].Text == "&nbsp;" || e.Row.Cells[7].Text == "No Realizado")
				{
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-pencil");
					icon.ToolTip = "No Realizado";
				}
				else if (e.Row.Cells[8].Text == "Completo")
				{
					e.Row.AddCssClass("info");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-ok");
					icon.ToolTip = "Completo";
				}
				else if (e.Row.Cells[8].Text == "Pospuesto")
				{
					e.Row.AddCssClass("warning");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-arrow-right");
					icon.ToolTip = "Pospuesto";
				}
				else if (e.Row.Cells[8].Text == "Cancelado")
				{
					e.Row.AddCssClass("danger");
					icon.AddCssClass("glyphicon").AddCssClass("glyphicon-remove");
					icon.ToolTip = "Cancelado";
				}
			}
		}

		protected void btnActualizar_Click(object sender, EventArgs e)
		{
			try
			{
				if (pnlAlertBusquedas.Visible == true) { pnlAlertBusquedas.Visible = false; }
				frmSiNo.Visible = false;
				frmValue.Visible = false;
				frmTwoValues.Visible = false;
				frmOptions.Visible = false;

				Guid UidSucursal = SesionActual.uidSucursalActual.Value;
				Guid UidDepartamento = new Guid(ddDepartamentos.SelectedValue.ToString());
				Guid UidArea = new Guid(ddAreas.SelectedValue.ToString());
				Guid UidTipoTarea = new Guid(ddTipo.SelectedValue.ToString());
				Guid UidUsuario = Guid.Empty;
				int? FolioTarea = null;
				int? FolioCumplimiento = null;
				int? FolioTurno = null;
				string NombreTarea = txtNombre.Text.Trim() == string.Empty ? "" : txtNombre.Text.Trim();
				DateTime? DtFechaInicio = null;
				DateTime? DtFechaFin = null;

				string UidsEstadosCumplimiento = "";

				string lpr = null;
				int[] i = lbEstados.GetSelectedIndices();
				if (i.Length > 0)
				{
					lpr = lbEstados.Items[i[0]].Value.ToString();
					for (int j = 1; j < i.Length; j++)
					{
						lpr += "," + lbEstados.Items[i[j]].Value.ToString();
					}
				}
				UidsEstadosCumplimiento = lpr == null ? "" : lpr;

				///<summary>
				///Obtener Folios ingresados en los campos de texto
				///</summary>
				if (!string.IsNullOrEmpty(txtFolioTareaFiltro.Text.Trim()))
				{
					int AuxFolioT;
					if (int.TryParse(txtFolioTareaFiltro.Text, out AuxFolioT))
					{
						FolioTarea = AuxFolioT;
					}
					else
					{
						pnlAlertBusquedas.Visible = true;
						lblError.Text = "Ingrese un folio de tarea numerico";
						return;
					}
				}
				if (!string.IsNullOrEmpty(txtFolioCumplimientoFiltro.Text.Trim()))
				{
					int AuxFolioC;
					if (int.TryParse(txtFolioCumplimientoFiltro.Text, out AuxFolioC))
					{
						FolioCumplimiento = AuxFolioC;
					}
					else
					{
						pnlAlertBusquedas.Visible = true;
						lblError.Text = "Ingrese un folio de cumplimiento numerico";
						return;
					}
				}

				///<summary>
				///Obtener Fechas seleccionadas en los campos de texto
				///</summary>
				if (!string.IsNullOrEmpty(txtFechaInicio.Text.Trim()))
				{
					try
					{
						DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFechaInicio.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					catch (Exception)
					{
						pnlAlertBusquedas.Visible = true;
						lblError.Text = "Ingrese una (Fecha de Inicio) valida";
						return;

					}

				}
				if (!string.IsNullOrEmpty(txtFechaFin.Text.Trim()))
				{
					try
					{
						DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFechaFin.Text.Trim(), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					catch (Exception)
					{
						pnlAlertBusquedas.Visible = true;
						lblError.Text = "Ingrese una (Fecha Fin) valida";
						return;
					}
				}

				///<summary>
				/// Obtener Folio de turno ingresado
				///</summary>
				if (!string.IsNullOrEmpty(txtFolioTurno.Text.Trim()))
				{
					int AuxFolioTurno;
					if (int.TryParse(txtFolioTurno.Text, out AuxFolioTurno))
					{
						FolioTurno = AuxFolioTurno;
					}
					else
					{
						pnlAlertBusquedas.Visible = true;
						lblError.Text = "Ingrese un folio de turno numerico";
						return;
					}
				}

				if (ModoSupervisor)
				{
					UidUsuario = Guid.Empty;
					if (ddlFiltroEncargado.Items.Count > 0)
						UidUsuario = new Guid(ddlFiltroEncargado.SelectedValue.ToString());
				}
				else
					UidUsuario = SesionActual.uidUsuario;


				bool ExpFolioTurno = true;
				string DirFolioTurno = "ASC";
				int PosFolioTurno = 1;

				bool ExpFolioCumplimiento = false;
				string DirFolioCumplimiento = "ASC";
				int PosFolioCumplimiento = 2;

				bool ExpFolioTarea = false;
				string DirFolioTarea = "ASC";
				int PosFolioTarea = 3;

				bool ExpTarea = false;
				string DirTarea = "ASC";
				int PosTarea = 4;

				bool ExpDepartamento = false;
				string DirDepartamento = "ASC";
				int PosDepartamento = 5;

				bool ExpArea = false;
				string DirArea = "ASC";
				int PosArea = 6;

				bool ExpHora = false;
				string DirHora = "ASC";
				int PosHora = 7;

				bool ExpEstado = false;
				string DirEstado = "ASC";
				int PosEstado = 8;

				foreach (var item in this.LsOrdenamientos)
				{
					if (item.Parametro.Equals("Folio de Turno"))
					{
						ExpFolioTurno = item.BlOrdenar;
						DirFolioTurno = item.Direccion;
						PosFolioTurno = item.Posicion;
					}
					else if (item.Parametro.Equals("Folio de Cumplimiento"))
					{
						ExpFolioCumplimiento = item.BlOrdenar;
						DirFolioCumplimiento = item.Direccion;
						PosFolioCumplimiento = item.Posicion;
					}
					else if (item.Parametro.Equals("Folio de Tarea"))
					{
						ExpFolioTarea = item.BlOrdenar;
						DirFolioTarea = item.Direccion;
						PosFolioTarea = item.Posicion;
					}
					else if (item.Parametro.Equals("Tarea"))
					{
						ExpTarea = item.BlOrdenar;
						DirTarea = item.Direccion;
						PosTarea = item.Posicion;
					}
					else if (item.Parametro.Equals("Departamento"))
					{
						ExpDepartamento = item.BlOrdenar;
						DirDepartamento = item.Direccion;
						PosDepartamento = item.Posicion;
					}
					else if (item.Parametro.Equals("Area"))
					{
						ExpArea = item.BlOrdenar;
						DirArea = item.Direccion;
						PosArea = item.Posicion;
					}
					else if (item.Parametro.Equals("Hora"))
					{
						ExpHora = item.BlOrdenar;
						DirHora = item.Direccion;
						PosHora = item.Posicion;
					}
					else if (item.Parametro.Equals("Estado"))
					{
						ExpEstado = item.BlOrdenar;
						DirEstado = item.Direccion;
						PosEstado = item.Posicion;
					}
				}

				VM.Busqueda(UidSucursal, UidUsuario, UidDepartamento, UidArea, UidTipoTarea, FolioTarea, FolioCumplimiento, FolioTurno, NombreTarea, UidsEstadosCumplimiento, DtFechaInicio, DtFechaFin, ExpFolioTurno, DirFolioTurno, ExpFolioCumplimiento, DirFolioCumplimiento, ExpFolioTarea, DirFolioTarea, ExpTarea, DirTarea, ExpDepartamento, DirDepartamento, ExpArea, DirArea, ExpHora, DirHora, ExpEstado, DirEstado, PosFolioTurno, PosFolioCumplimiento, PosFolioTarea, PosTarea, PosDepartamento, PosArea, PosHora, PosEstado);
				VSTareas = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataSource = VM.CumplimientosPendientes;
				dgvTareasPendientes.DataBind();

				ViewState["CumplPreviousRow"] = null;

				btnMostrar.Text = "Ocultar";
				pnlOrdenamiento.Visible = false;
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnOrdenamiento.Visible = false;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;
			}
			catch (Exception ex)
			{
				pnlAlertBusquedas.Visible = true;
				lblError.Text = "Error: " + ex.Message;
			}
		}


		protected void dgvTareasPendientes_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			if (ViewState["SortColumn"] != null && ViewState["SortColumnDirection"] != null)
			{
				string SortExpression = (string)ViewState["SortColumn"];
				SortDirection SortDirection = (SortDirection)ViewState["SortColumnDirection"];
				SortTareas(SortExpression, SortDirection, true);
			}
			else
			{
				dgvTareasPendientes.DataSource = VSTareas;
			}
			dgvTareasPendientes.PageIndex = e.NewPageIndex;
			dgvTareasPendientes.DataBind();
		}

		protected void dgvTareasPendientes_Sorting(object sender, GridViewSortEventArgs e)
		{
			ViewState["CumplPreviousRow"] = null;
			SortTareas(e.SortExpression, e.SortDirection, false);
			dgvTareasPendientes.DataBind();
		}

		private void SortTareas(string SortExpression, SortDirection SortDirection, bool same = false)
		{
			if (SortExpression == (string)ViewState["SortColumn"] && !same)
			{
				// We are resorting the same column, so flip the sort direction
				SortDirection =
					((SortDirection)ViewState["SortColumnDirection"] == SortDirection.Ascending) ?
					SortDirection.Descending : SortDirection.Ascending;
			}

			if (SortExpression == "FolioTurno")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolioTurno).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolioTurno).ToList();
				}
			}
			else if (SortExpression == "FolioTarea")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolioTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolioTarea).ToList();
				}
			}
			else if (SortExpression == "FolioCumpl")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.IntFolio).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.IntFolio).ToList();
				}
			}
			else if (SortExpression == "Tarea")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrTarea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrTarea).ToList();
				}
			}
			else if (SortExpression == "Departamento")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrDepartamento).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrDepartamento).ToList();
				}
			}
			else if (SortExpression == "Area")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy(x => x.StrArea).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending(x => x.StrArea).ToList();
				}
			}
			else if (SortExpression == "Estado")
			{
				if (SortDirection == SortDirection.Ascending)
				{
					VSTareas = VSTareas.OrderBy((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
				else
				{
					VSTareas = VSTareas.OrderByDescending((x => x.StrEstadoCumplimiento), new EstadoComparator()).ToList();
				}
			}

			dgvTareasPendientes.DataSource = VSTareas;
			ViewState["SortColumn"] = SortExpression;
			ViewState["SortColumnDirection"] = SortDirection;
		}

		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			if (pnlAlertBusquedas.Visible == true)
				pnlAlertBusquedas.Visible = false;

			txtNombre.Text = string.Empty;
			lbEstados.ClearSelection();
			if (lblError.Visible == true) { lblError.Visible = false; }


			txtFechaInicio.Text = txtFechaFin.Text = string.Empty;

			ddDepartamentos.SelectedIndex = 0;
			ddAreas.SelectedIndex = 0;
			ddTipo.SelectedIndex = 0;
			txtFolioCumplimientoFiltro.Text = string.Empty;
			txtFolioTareaFiltro.Text = string.Empty;
			txtFolioTurno.Text = string.Empty;
		}

		private class EstadoComparator : IComparer<string>
		{
			private int GetValue(string s)
			{
				if (s == "No Realizado")
					return -1;
				else if (s == "Completado")
					return 3;
				else if (s == "Cancelado")
					return 2;
				else if (s == "Pospuesto")
					return 1;
				else
					return 10;
			}
			public int Compare(string x, string y)
			{
				return GetValue(x) - GetValue(y);
			}
		}

		protected void btnMostrar_Click(object sender, EventArgs e)
		{
			if (btnMostrar.Text == "Mostrar")
			{
				btnMostrar.Text = "Ocultar";

				btnOrdenamiento.Visible = false;
				pnlOrdenamiento.Visible = false;
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				btnLimpiar.Visible = false;
				btnActualizar.Visible = false;
			}
			else
			{
				btnMostrar.Text = "Mostrar";

				if (btnOrdenamiento.Text.Equals("Ordenamiento"))
				{
					panelBusqueda.Visible = true;
					btnLimpiar.Visible = true;
					pnlOrdenamiento.Visible = false;
				}
				else if (btnOrdenamiento.Text.Equals("Filtros"))
				{
					panelBusqueda.Visible = false;
					btnLimpiar.Visible = false;
					pnlOrdenamiento.Visible = true;
				}
				btnOrdenamiento.Visible = true;
				panelResultados.Visible = false;
				btnActualizar.Visible = true;
			}
		}

		protected void ddDepartamentos_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid selected = new Guid(ddDepartamentos.SelectedValue);
			if (selected != Guid.Empty)
			{
				VM.ObtenerAreas(selected);
				Area area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000001"); // General
				area.StrNombre = "General";
				VM.Areas.Insert(0, area);
				area = new Area();
				area.UidArea = new Guid("00000000-0000-0000-0000-000000000000"); // Todos
				area.StrNombre = "Todos";
				VM.Areas.Insert(0, area);
				ddAreas.DataSource = VM.Areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.DataBind();

				ddAreas.SelectedIndex = 0;
			}
			else
			{
				List<Area> areas = new List<Area>();
				Area area = new Area();
				area.UidArea = Guid.Empty; // Todos
				area.StrNombre = "Todos";
				areas.Add(area);
				ddAreas.DataSource = areas;
				ddAreas.DataTextField = "StrNombre";
				ddAreas.DataValueField = "UidArea";
				ddAreas.SelectedIndex = 0;
				ddAreas.DataBind();
			}
		}

		protected void gvOrdenamiento_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(gvOrdenamiento, "Select$" + e.Row.RowIndex);

				LinkButton Change = e.Row.FindControl("btnChangeDirection") as LinkButton;
				Label Icon = e.Row.FindControl("lblIcon") as Label;
				if (e.Row.Cells[4].Text.Equals("ASC"))
				{
					Icon.CssClass = "glyphicon glyphicon-chevron-down";
					Change.ToolTip = "Descendente";
				}
				else if (e.Row.Cells[4].Text.Equals("DESC"))
				{
					Icon.CssClass = "glyphicon glyphicon-chevron-up";
					Change.ToolTip = "Ascendente";
				}
			}
		}
		protected void gvOrdenamiento_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName.Equals("CambiarOrden"))
			{
				Guid Uid = new Guid(e.CommandArgument.ToString());
				int Index = this.LsOrdenamientos.FindIndex(o => o.ID == Uid);
				if (Index >= 0)
				{
					if (this.LsOrdenamientos[Index].Direccion.Equals("ASC"))
						this.LsOrdenamientos[Index].Direccion = "DESC";
					else
						this.LsOrdenamientos[Index].Direccion = "ASC";

					gvOrdenamiento.DataSource = this.LsOrdenamientos.OrderBy(o => o.Posicion);
					gvOrdenamiento.DataBind();
				}
			}
			else if (e.CommandName.Equals("Up"))
			{
				Guid Uid = new Guid(e.CommandArgument.ToString());
				int Index = this.LsOrdenamientos.FindIndex(o => o.ID == Uid);
				if (Index >= 0)
				{
					if (LsOrdenamientos[Index].Posicion > 1)
					{
						int CurrentPosition = LsOrdenamientos[Index].Posicion;
						int Index2 = LsOrdenamientos.FindIndex(o => o.Posicion == (CurrentPosition - 1));
						LsOrdenamientos[Index].Posicion = CurrentPosition - 1;
						LsOrdenamientos[Index2].Posicion = CurrentPosition;
						gvOrdenamiento.DataSource = this.LsOrdenamientos.OrderBy(o => o.Posicion);
						gvOrdenamiento.DataBind();

						gvOrdenamiento.Rows[CurrentPosition - 2].RowState = DataControlRowState.Selected;
					}
					else
						gvOrdenamiento.Rows[0].RowState = DataControlRowState.Selected;
				}
			}
			else if (e.CommandName.Equals("Down"))
			{
				Guid Uid = new Guid(e.CommandArgument.ToString());
				int Index = this.LsOrdenamientos.FindIndex(o => o.ID == Uid);
				if (Index >= 0)
				{
					if (LsOrdenamientos[Index].Posicion < 8)
					{
						int CurrentPosition = LsOrdenamientos[Index].Posicion;
						int Index2 = LsOrdenamientos.FindIndex(o => o.Posicion == (CurrentPosition + 1));
						LsOrdenamientos[Index].Posicion = CurrentPosition + 1;
						LsOrdenamientos[Index2].Posicion = CurrentPosition;
						gvOrdenamiento.DataSource = this.LsOrdenamientos.OrderBy(o => o.Posicion);
						gvOrdenamiento.DataBind();

						gvOrdenamiento.Rows[CurrentPosition].RowState = DataControlRowState.Selected;
					}
					else
						gvOrdenamiento.Rows[gvOrdenamiento.Rows.Count - 1].RowState = DataControlRowState.Selected;
				}
			}
		}
		protected void Ordenamiento_CheckedChanged(object sender, EventArgs e)
		{
			try
			{
				GridViewRow row = (sender as CheckBox).Parent.Parent as GridViewRow;
				CheckBox cbRowCheckBox = ((CheckBox)sender) as CheckBox;

				Guid Uid = new Guid(gvOrdenamiento.DataKeys[row.RowIndex].Value.ToString());
				int Index = this.LsOrdenamientos.FindIndex(o => o.ID == Uid);
				if (Index >= 0)
				{
					LsOrdenamientos[Index].BlOrdenar = cbRowCheckBox.Checked;
				}
			}
			catch (Exception)
			{

				throw;
			}
		}

		protected void btnOrdenamiento_Click(object sender, EventArgs e)
		{
			if (btnOrdenamiento.Text.Equals("Ordenamiento"))
			{
				btnOrdenamiento.Text = "Filtros";
				panelBusqueda.Visible = false;
				btnLimpiar.Visible = false;
				pnlOrdenamiento.Visible = true;
			}
			else if (btnOrdenamiento.Text.Equals("Filtros"))
			{
				btnOrdenamiento.Text = "Ordenamiento";
				panelBusqueda.Visible = true;
				btnLimpiar.Visible = true;
				pnlOrdenamiento.Visible = false;
			}
		}
	}
}