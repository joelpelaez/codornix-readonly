﻿using CodorniX.Modelo;
using CodorniX.Util;
using CodorniX.VistaDelModelo;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CodorniX.Vista
{
	public partial class AsignacionInactividad : System.Web.UI.Page
	{
		private VMAsignacionInactividad VM = new VMAsignacionInactividad();
		private Sesion SesionActual
		{
			get { return (Sesion)Session["Sesion"]; }
		}
		private bool ModoSupervisor = false;
		private Guid UidEncargado
		{
			get { return (Guid)Session["UidEncargado"]; }
			set { Session["UidEncargado"] = value; }
		}
		private Guid UidPeriodoInactividad
		{
			get { return (Guid)Session["UidPeriodoInactividad"]; }
			set { Session["UidPeriodoInactividad"] = value; }
		}
		private DateTime FechaInicio
		{
			get { return (DateTime)Session["FechaInicio"]; }
			set { Session["FechaInicio"] = value; }
		}
		private DateTime FechaFin
		{
			get { return (DateTime)Session["FechaFin"]; }
			set { Session["FechaFin"] = value; }
		}

		private DateTime DtFiltroDiaFechaInicio
		{
			get
			{
				return (DateTime)ViewState["FiltroFechaInicio"];
			}
			set
			{
				ViewState["FiltroFechaInicio"] = value;
			}
		}
		private DateTime DtFiltroDiaFechaFin
		{
			get
			{
				return (DateTime)ViewState["FiltroFechaFin"];
			}
			set
			{
				ViewState["FiltroFechaFin"] = value;
			}
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			if (SesionActual == null)
			{
				return;
			}

			if (!SesionActual.uidSucursalActual.HasValue)
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (!Acceso.TieneAccesoAModulo("Asignacion", SesionActual.uidUsuario, SesionActual.uidPerfilActual.Value))
			{
				Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
				return;
			}

			if (SesionActual.perfil == "Supervisor")
			{

				// El supervisor no tiene acceso al cumplimiento si no ha tomado  algun turno para cumplir.
				if (SesionActual.UidDepartamentos.Count == 0)
				{
					Response.Redirect(Acceso.ObtenerHomePerfil(SesionActual.uidPerfilActual.Value), false);
					return;
				}

				ModoSupervisor = true;
			}

			if (!IsPostBack)
			{
				FechaInicio = default(DateTime);
				FechaFin = default(DateTime);

				if (ModoSupervisor)
					VM.ObtenerDepartamentosPorLista(SesionActual.UidDepartamentos);
				else
					VM.ObtenerDepartamentos(SesionActual.uidSucursalActual.Value);

				lbDepartamentos.DataSource = VM.Departamentos;
				lbDepartamentos.DataTextField = "StrNombre";
				lbDepartamentos.DataValueField = "UidDepartamento";
				lbDepartamentos.DataBind();

				txtFechaInicio.Text = DateTime.Today.ToString("dd/MM/yyyy");
				txtFechaFin.Text = txtFechaInicio.Text;

				pnlAlertBusqueda.Visible = false;

				dgvDiasInactividad.DataSource = null;
				dgvDiasInactividad.DataBind();

				pnlAlertGeneral.Visible = false;

				btnBuscarDiasInactividad.Disable();
				EnableFiltrosBusquedaDias(false);
				EnableFiltrosbusquedaEncargados(false);
				lblOcultarFiltrosDiasInactividad.Text = "Mostrar";
				pnlFiltrosDiasInactividad.Visible = false;

				dgvEncargados.DataSource = null;
				dgvEncargados.DataBind();
			}
		}

		private void RealizarBusqueda()
		{
			if (pnlAlertBusqueda.Visible)
				pnlAlertBusqueda.Visible = false;

			string lpr = null;
			int[] i = lbDepartamentos.GetSelectedIndices();
			if (i.Length > 0)
			{
				lpr = lbDepartamentos.Items[i[0]].Value.ToString();
				for (int j = 1; j < i.Length; j++)
				{
					lpr += "," + lbDepartamentos.Items[i[j]].Value.ToString();
				}
			}

			if (ModoSupervisor && lpr == null)
			{
				foreach (var depto in SesionActual.UidDepartamentos)
				{
					lpr = "";
					var value = depto.ToString();
					if (!lpr.Any())
						lpr += value;
					else
						lpr += "," + value;
				}
			}

			DateTime fechaInicio, fechaFin;
			try
			{
				fechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				pnlAlertBusqueda.Visible = true;
				lblAlertBusqueda.Text = "Error formato de fecha (inicio) invalida.";
				return;
			}

			try
			{
				fechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				pnlAlertBusqueda.Visible = true;
				lblAlertBusqueda.Text = "Error formato de fecha (fin) invalida.";
				return;
			}

			FechaInicio = fechaInicio;
			FechaFin = fechaFin;

			VM.ObtenerPeriodos(SesionActual.uidSucursalActual.Value, fechaInicio, fechaFin, lpr);

			dgvListaInactividades.DataSource = VM.Periodos;
			dgvListaInactividades.DataBind();
		}

		protected void btnBuscar_Click(object sender, EventArgs e)
		{
			RealizarBusqueda();
			lblOcultarFiltros.Text = "Ocultar";
			btnMostrar_Click(sender, e);
		}

		protected void btnLimpiar_Click(object sender, EventArgs e)
		{
			if (pnlAlertBusqueda.Visible)
				pnlAlertBusqueda.Visible = false;

			lbDepartamentos.ClearSelection();
		}

		protected void btnMostrar_Click(object sender, EventArgs e)
		{
			if (lblOcultarFiltros.Text == "Mostrar")
			{
				panelBusqueda.Visible = true;
				panelResultados.Visible = false;
				lblOcultarFiltros.Text = "Ocultar";
			}
			else
			{
				panelBusqueda.Visible = false;
				panelResultados.Visible = true;
				lblOcultarFiltros.Text = "Mostrar";
			}
		}

		protected void btnAsignar_Click(object sender, EventArgs e)
		{
			GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

			foreach (GridViewRow row in gridViewRowCollection)
			{
				if (row.RowType == DataControlRowType.DataRow)
				{
					CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
					if (checkBox.Checked)
					{
						DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);

						VM.AsignarDia(UidPeriodoInactividad, fecha, UidEncargado);
					}
				}
			}
			ListarDias();
		}

		protected void btnQuitar_Click(object sender, EventArgs e)
		{
			GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

			foreach (GridViewRow row in gridViewRowCollection)
			{
				if (row.RowType == DataControlRowType.DataRow)
				{
					CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
					if (checkBox.Checked)
					{
						DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);

						VM.QuitarDia(UidPeriodoInactividad, fecha);
					}
				}
			}

			ListarDias();
		}

		protected void dgvEncargados_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid uid = new Guid(dgvEncargados.SelectedDataKey.Value.ToString());
			UidEncargado = uid;
			int pos = -1;
			if (ViewState["EncargadosPreviousRow"] != null)
			{
				pos = (int)ViewState["EncargadosPreviousRow"];
				GridViewRow previousRow = dgvEncargados.Rows[pos];
				previousRow.RemoveCssClass("success");
			}

			ViewState["EncargadosPreviousRow"] = dgvEncargados.SelectedIndex;
			dgvEncargados.SelectedRow.AddCssClass("success");
		}

		protected void dgvEncargados_RowDataBound(object sender, GridViewRowEventArgs e)
		{

		}

		protected void dgvDiasInactividad_SelectedIndexChanged(object sender, EventArgs e)
		{

		}

		protected void dgvDiasInactividad_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				string status = e.Row.Cells[3].Text;
				LinkButton button = e.Row.FindControl("QuitarButton") as LinkButton;
				if (status == "False")
				{
					button.Disable();
				}
				else
				{
					e.Row.CssClass = "font-bold";
					button.Enable();
				}
			}
		}

		protected void dgvListaInactividades_SelectedIndexChanged(object sender, EventArgs e)
		{
			Guid uid = new Guid(dgvListaInactividades.SelectedDataKey.Value.ToString());
			//DateTime fechaInicio = default(DateTime);
			//DateTime fechaFin = default(DateTime);
			DateTime DtFechaActual = SesionActual.GetDateTime();
			DateTime fechaInicioCalculado = FechaInicio;
			DateTime fechaFinCalculado = FechaFin;

			UidPeriodoInactividad = uid;

			this.FechaInicio = Convert.ToDateTime(DateTime.ParseExact(dgvListaInactividades.SelectedDataKey.Values[1].ToString().Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture));
			this.FechaFin = Convert.ToDateTime(DateTime.ParseExact(dgvListaInactividades.SelectedDataKey.Values[2].ToString().Substring(0, 10), "dd/MM/yyyy", CultureInfo.InvariantCulture));
						
			txtFiltroDiaFechaInicio.Text = DtFechaActual.ToString("dd/MM/yyyy");
			txtFiltroDiaFechaFin.Text = DtFechaActual.AddMonths(1).ToString("dd/MM/yyyy");

			this.DtFiltroDiaFechaInicio = DtFechaActual;
			this.DtFiltroDiaFechaFin = DtFechaActual.AddMonths(1);

			//if (DateTime.TryParse(dgvListaInactividades.SelectedDataKey.Values[1].ToString(), out fechaInicio))
			//	FechaInicio = fechaInicio > FechaInicio ? fechaInicio : fechaInicio;
			//if (DateTime.TryParse(dgvListaInactividades.SelectedDataKey.Values[2].ToString(), out fechaFin))
			//	FechaFin = fechaFin < FechaFin ? fechaFin : FechaFin;

			ListarDias();

			int pos = -1;
			if (ViewState["ListaPreviousRow"] != null)
			{
				pos = (int)ViewState["ListaPreviousRow"];
				GridViewRow previousRow = dgvEncargados.Rows[pos];
				previousRow.RemoveCssClass("success");
			}

		}

		private void ListarDias()
		{
			txtFiltroDiaFechaInicio.Text = this.DtFiltroDiaFechaInicio.ToString("dd/MM/yyyy");
			txtFiltroDiaFechaFin.Text = this.DtFiltroDiaFechaFin.ToString("dd/MM/yyyy");

			VM.ObtenerDias(UidPeriodoInactividad, DtFiltroDiaFechaInicio, DtFiltroDiaFechaFin);
			dgvDiasInactividad.DataSource = VM.Dias;
			dgvDiasInactividad.DataBind();

			if (VM.Dias.Count > 0)
			{
				btnBuscarDiasInactividad.Enable();
				EnableFiltrosBusquedaDias(true);
				EnableFiltrosbusquedaEncargados(true);
			}
		}

		protected void dgvListaInactividades_RowDataBound(object sender, GridViewRowEventArgs e)
		{
			if (e.Row.RowType == DataControlRowType.DataRow)
			{
				e.Row.Attributes["onclick"] = ClientScript.GetPostBackClientHyperlink(dgvListaInactividades, "Select$" + e.Row.RowIndex);
			}
		}

		protected void cbTodos_CheckedChanged(object sender, EventArgs e)
		{
			GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

			foreach (GridViewRow row in gridViewRowCollection)
			{
				if (row.RowType == DataControlRowType.DataRow)
				{
					CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
					checkBox.Checked = true;
				}
			}
		}

		protected void cbSeleccionado_CheckedChanged(object sender, EventArgs e)
		{
			GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

			CheckBox cb = dgvDiasInactividad.HeaderRow.FindControl("cbTodos") as CheckBox;
			bool enableQuit = false, checkAll = true;
			foreach (GridViewRow row in gridViewRowCollection)
			{
				if (row.RowType == DataControlRowType.DataRow)
				{
					CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
					if (checkAll && !checkBox.Checked)
					{
						checkAll = false;
					}
					if (!enableQuit && checkBox.Checked)
					{
						enableQuit = true;
					}
				}
			}

			if (checkAll)
				cb.Checked = true;
			else
				cb.Checked = false;

			if (enableQuit)
				btnQuitarTodos.Enable();
			else
				btnQuitarTodos.Disable();
		}

		protected void dgvDiasInactividad_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "Quitar")
			{
				int i = Convert.ToInt32(e.CommandArgument);
				DateTime fecha = Convert.ToDateTime(dgvDiasInactividad.DataKeys[i].Value);
				VM.QuitarDia(UidPeriodoInactividad, fecha);
				ListarDias();
			}
		}

		protected void btnQuitarTodos_Click(object sender, EventArgs e)
		{
			if (dgvDiasInactividad.Rows.Count == 0)
				return;

			GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

			foreach (GridViewRow row in gridViewRowCollection)
			{
				if (row.RowType == DataControlRowType.DataRow)
				{
					CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
					if (checkBox.Checked)
					{
						DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);

						VM.QuitarDia(UidPeriodoInactividad, fecha);
					}
				}
			}

			VM.ObtenerDias(UidPeriodoInactividad, DtFiltroDiaFechaInicio, DtFiltroDiaFechaFin);
			dgvDiasInactividad.DataSource = VM.Dias;
			dgvDiasInactividad.DataBind();
		}

		protected void dgvEncargados_RowCommand(object sender, GridViewCommandEventArgs e)
		{
			if (e.CommandName == "Asignar")
			{
				int i = Convert.ToInt32(e.CommandArgument);
				Guid uid = new Guid(dgvEncargados.DataKeys[i].Value.ToString());
				string ErrorEstatus = string.Empty;
				int Response = 0;
				int SelectedRows = 0;
				GridViewRowCollection gridViewRowCollection = dgvDiasInactividad.Rows;

				foreach (GridViewRow row in gridViewRowCollection)
				{
					if (row.RowType == DataControlRowType.DataRow)
					{
						CheckBox checkBox = row.FindControl("cbSeleccionado") as CheckBox;
						if (checkBox.Checked)
						{
							//DateTime fecha = Convert.ToDateTime(row.Cells[1].Text);
							DateTime DtFecha = Convert.ToDateTime(DateTime.ParseExact(row.Cells[1].Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));

							Response = VM.AsignarDia(UidPeriodoInactividad, DtFecha, uid);
							if (string.IsNullOrEmpty(ErrorEstatus))
								ErrorEstatus = "Asignación: " + DtFecha.ToString("dd/MM/yyyy") + ReturnAssignmentError(Response);
							else
								ErrorEstatus += "</br>Asignación: " + DtFecha.ToString("dd/MM/yyyy") + ReturnAssignmentError(Response);

							SelectedRows++;
						}
					}
				}

				if (SelectedRows == 0)
					DisplayAlert("Error: Seleccione minimo (1) dia de inactividad.", "danger");
				else
					DisplayAlert(ErrorEstatus, "info");

				ListarDias();
			}
		}

		protected void OcultarFiltrosDiasInactividad_Click(object sender, EventArgs e)
		{
			if (lblOcultarFiltrosDiasInactividad.Text.Equals("Ocultar"))
			{
				lblOcultarFiltrosDiasInactividad.Text = "Mostrar";
				pnlFiltrosDiasInactividad.Visible = false;
			}
			else
			{
				lblOcultarFiltrosDiasInactividad.Text = "Ocultar";
				pnlFiltrosDiasInactividad.Visible = true;
			}
		}
		protected void btnOcultarFiltrosEncargado_Click(object sender, EventArgs e)
		{
			if (lblOcultarFiltrosEncargado.Text.Equals("Ocultar"))
			{
				pnlFiltrosEncargado.Visible = false;
				lblOcultarFiltrosEncargado.Text = "Mostrar";
			}
			else
			{
				pnlFiltrosEncargado.Visible = true;
				lblOcultarFiltrosEncargado.Text = "Ocultar";
			}
		}

		protected void btnBuscarDiasInactividad_Click(object sender, EventArgs e)
		{
			if (pnlAlertGeneral.Visible)
				pnlAlertGeneral.Visible = false;

			DateTime DtFechaInicio = DateTime.Today;
			DateTime DtFechaFin = DateTime.Today;

			try
			{
				DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(txtFiltroDiaFechaInicio.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				DisplayAlert("Formato de fecha de (inicio) invalido.", "danger");
				return;
			}

			try
			{
				DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(txtFiltroDiaFechaFin.Text, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			}
			catch (Exception)
			{
				DisplayAlert("Formato de fecha (fin) invalido.", "danger");
				return;
			}
			this.DtFiltroDiaFechaInicio = DtFechaInicio;
			this.DtFiltroDiaFechaFin = DtFechaFin;

			VM.ObtenerDias(UidPeriodoInactividad, DtFechaInicio, DtFechaFin);
			dgvDiasInactividad.DataSource = VM.Dias;
			dgvDiasInactividad.DataBind();
		}
		protected void btnBuscarEncargado_Click(object sender, EventArgs e)
		{
			VM.ObtenerEncargado(SesionActual.uidSucursalActual.Value, txtNombreEncargado.Text);

			ViewState["EncargadosPreviousRow"] = null;

			dgvEncargados.DataSource = VM.Encargados;
			dgvEncargados.DataBind();
		}

		protected void btnLimpiarFiltrosEncargado_Click(object sender, EventArgs e)
		{
			txtNombreEncargado.Text = string.Empty;
		}

		protected void btnCloseAlertDialogGeneral(object sender, EventArgs e)
		{
			pnlAlertGeneral.Visible = false;
		}

		private void DisplayAlert(string Message, string Type)
		{
			string AlertType = "alert alert-";
			pnlAlertGeneral.CssClass = AlertType + Type;
			lblAlertGeneral.Text = Message;
			pnlAlertGeneral.Visible = true;
		}

		private string ReturnAssignmentError(int ErrorLevel)
		{
			string Result = string.Empty;
			switch (ErrorLevel)
			{
				case 0:
					Result = " la asignación fue realizada correctamente.";
					break;
				case 1:
					Result = " la asignación se realizo correctamente con actualizaciondes de fechas finales indefinidas.";
					break;
				case 2:
					Result = " se actualizo correctamente la asignación.";
					break;
				case -1:
					Result = " existe una asignacion similar.";
					break;
				case -2:
					Result = " el usuario alcanzo el maximo número de asignaciones disponibles.";
					break;
				case -3:
					Result = " la fecha de inicio no es consecutiva a la ultima asignación";
					break;
				default:
					break;
			}
			return Result;
		}

		private void EnableFiltrosBusquedaDias(bool IsEnabled)
		{
			txtFiltroDiaFechaInicio.Enabled = IsEnabled;
			txtFiltroDiaFechaFin.Enabled = IsEnabled;
		}

		private void EnableFiltrosbusquedaEncargados(bool IsEnabled)
		{
			txtNombreEncargado.Enabled = IsEnabled;
		}
	}
}