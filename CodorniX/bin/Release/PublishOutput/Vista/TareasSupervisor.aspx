﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="TareasSupervisor.aspx.cs" Inherits="CodorniX.Vista.TareasSupervisor" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Busqueda
				</div>
				<div class="panel-body panel-pd">
					<div class="text-right">
						<div class="btn-group pd-0">
							<asp:LinkButton runat="server" ID="btnOcultarFiltros" CssClass="btn btn-sm btn-default" OnClick="btnOcultar_Click">
								<asp:Label CssClass="glyphicon glyphicon-eye-close" runat="server" ID="lblIconOcultarFiltros" />
								<asp:Label Text="Ocultar" runat="server" ID="lblTextoOcultarFiltros" />
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnLimpiar" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
								<span class="glyphicon glyphicon-trash"></span>
								Limpiar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-sm btn-default" OnClick="btnBuscar_Click">
								<span class="glyphicon  glyphicon-search"></span>
								Buscar
							</asp:LinkButton>
						</div>
					</div>
					<asp:Panel runat="server" ID="pnlAlertBusqueda">
						<div class="alert alert-danger">
							<asp:Label Text="Error" runat="server" ID="lblErrorBusqueda" />
							<asp:LinkButton ID="btnCloseAlerBusqueda" OnClick="CloseAlertDialogSearch" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlFiltrosBusqueda">
						<div class="col-md-4 pd-left-right-5">
							<h6>Folio</h6>
							<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFolio" />
						</div>
						<div class="col-sm-8 col-md-4 pd-left-right-5">
							<h6>Tarea</h6>
							<asp:TextBox runat="server" ID="txtFiltroTarea" CssClass="form-control" />
						</div>
						<div class="col-sm-6 col-md-4 pd-left-right-5">
							<h6>Fecha Inicio</h6>
							<asp:TextBox runat="server" ID="txtFiltroFechaInicio" CssClass="form-control" />
						</div>
						<div class="col-sm-6 col-md-4 pd-left-right-5">
							<h6>Fecha Fin</h6>
							<asp:TextBox runat="server" ID="txtFiltroFechaFin" CssClass="form-control" />
						</div>
						<div class="col-sm-12 col-md-4 pd-left-right-5">
							<h6>Departamento</h6>
							<asp:ListBox runat="server" ID="lbFiltroDepartamento" CssClass="form-control">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:ListBox>
						</div>
						<div class="col-sm-12 col-md-4 pd-left-right-5">
							<h6>Unidad de Medida</h6>
							<asp:ListBox runat="server" ID="lbFiltroUnidadMedida" CssClass="form-control">
								<asp:ListItem Text="text1" />
								<asp:ListItem Text="text2" />
							</asp:ListBox>
						</div>
						<div class="col-sm-12 col-md-4 pd-left-right-5">
							<h6>Folio Antecesor</h6>
							<asp:TextBox runat="server" ID="TxtFiltroFolioAntecesor" CssClass="form-control" />
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlListaTareas">
						<asp:HiddenField runat="server" ID="hfGvTareasSortDirection" Value="ASC" />
						<asp:GridView runat="server" ID="gvTareas" DataKeyNames="UidTarea" OnPreRender="gvTareas_PreRender" OnRowDataBound="gvTareas_RowDataBound" OnSelectedIndexChanging="gvTareas_SelectedIndexChanging" OnSelectedIndexChanged="gvTareas_SelectedIndexChanged" OnPageIndexChanging="gvTareas_PageIndexChanging" OnSorting="gvTareas_Sorting" AllowSorting="true" AllowPaging="true" PageSize="13" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm" SelectedRowStyle-BackColor="#dff0d8">
							<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
							<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
							<EmptyDataTemplate>
								<div class="alert alert-info">
									Sin resultados
								</div>
							</EmptyDataTemplate>
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
								<asp:BoundField DataField="IntFolio" HeaderText="Folio" DataFormatString="{0:0000}" SortExpression="Folio" />
								<asp:BoundField DataField="StrNombre" HeaderText="Nombre" SortExpression="Nombre" />
								<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodicidad" SortExpression="Periodicidad" />
								<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
								<asp:BoundField DataField="DtFechaInicio" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha Inicio" SortExpression="FechaInicio" />
							</Columns>
						</asp:GridView>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Datos Generales
				</div>
				<div class="panel-body panel-pd">
					<div class="btn-group pd-0">
						<asp:LinkButton runat="server" ID="btnNuevo" OnClick="btnNuevo_Click" CssClass="btn btn-sm btn-default">
							<span class="glyphicon glyphicon-plus"></span>
							Nuevo
						</asp:LinkButton>
						<asp:LinkButton runat="server" ID="btnEditar" OnClick="btnEditar_Click" CssClass="btn btn-sm btn-default disabled">
							<span class="glyphicon glyphicon-pencil"></span>
							Editar
						</asp:LinkButton>
						<asp:LinkButton runat="server" ID="btnGuardar" OnClick="btnGuardar_Click" CssClass="btn btn-sm btn-success">
							<span class="glyphicon glyphicon-ok"></span>
						</asp:LinkButton>
						<asp:LinkButton runat="server" ID="btnCancelar" OnClick="btnCancelar_Click" CssClass="btn btn-sm btn-danger">
							<span class="glyphicon glyphicon-remove"></span>
						</asp:LinkButton>
						<asp:HiddenField runat="server" ID="hfUidTarea" />
					</div>
					<asp:Panel runat="server" ID="pnlAlertGeneral">
						<div class="alert alert-danger">
							<asp:Label Text="Error" runat="server" ID="lblErrorGeneral" />
							<asp:LinkButton ID="btnCloseAlertGeneral" OnClick="CloseAlertDialogGeneral" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</div>
					</asp:Panel>
					<ul class="nav nav-tabs">
						<li id="liDatosGenerales" runat="server" class="active">
							<asp:LinkButton Text="Datos" runat="server" OnClick="NavDatosGenerales_Click" />
						</li>
						<li id="liOpcionesTarea" runat="server">
							<asp:LinkButton Text="Opciones" runat="server" OnClick="NavOpcionesTarea_Click" />
						</li>
						<li id="liPeriodos" runat="server">
							<asp:LinkButton Text="Periodos" runat="server" OnClick="NavPeriodos_Click" />
						</li>
						<li id="liAsignacion" runat="server">
							<asp:LinkButton Text="Asignacion" runat="server" OnClick="NavAsignacion_Click" />
						</li>
						<li id="liAntecesor" runat="server">
							<asp:LinkButton Text="Antecesor" runat="server" OnClick="NavAntecesor_Click" />
						</li>
						<li id="liNotificacion" runat="server">
							<asp:LinkButton Text="Notificación" runat="server" OnClick="NavNotificacion_Click" />
						</li>
					</ul>
					<asp:Panel runat="server" ID="pnlTabDatosGenerales">
						<div class="col-md-4 pd-left-right-5">
							<h6>Nombre</h6>
							<asp:TextBox runat="server" ID="txtNombreTarea" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Fecha inicio</h6>
							<div class="input-group date extra">
								<asp:TextBox runat="server" ID="txtFechaInicioTarea" CssClass="form-control" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Fecha fin</h6>
							<div class="input-group date extra">
								<asp:TextBox runat="server" ID="txtFechaFinTarea" CssClass="form-control" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Descripcion</h6>
							<asp:TextBox runat="server" ID="txtDescripcionTarea" TextMode="MultiLine" Rows="9" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Estatus</h6>
							<asp:DropDownList runat="server" ID="ddlEstatusTarea" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Requerida</h6>
							<asp:DropDownList runat="server" ID="ddlTareaRequerida" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Tipo de Medicion</h6>
							<asp:DropDownList runat="server" ID="ddlTipoMedicionTarea" CssClass="form-control" OnSelectedIndexChanged="ddlTipoMedicionTarea_SelectedIndexChanged" AutoPostBack="true" />
						</div>
						<asp:Panel runat="server" ID="pnlUnidadMedida">
							<div class="col-md-4 pd-left-right-5">
								<h6>Unidad de medida</h6>
								<asp:DropDownList runat="server" ID="ddlUnidadMedida" CssClass="form-control" />
							</div>
						</asp:Panel>
						<div class="col-md-8 pd-0">
							<div class="col-md-6 pd-left-right-5">
								<label class="checkbox-inline">
									<asp:CheckBox AutoPostBack="true" ID="CbFoto" runat="server" Checked="false" Visible="false" />

								</label>
							</div>
							<div class="col-md-6 pd-left-right-5">
								<label class="checkbox-inline">
									<asp:CheckBox AutoPostBack="true" ID="cbHabilitarHoraTarea" OnCheckedChanged="cbHabilitarHoraTarea_CheckedChanged" runat="server" />
									Habilitar Hora
								</label>
							</div>
						</div>
						<asp:Panel runat="server" ID="pnlHabilitarHoraTarea">
							<div class="col-md-4 pd-left-right-5">
								<h6>Hora</h6>
								<div class=" input-group clockpicker" data-placement="top" data-align="left" data-autoclose="true">
									<asp:TextBox runat="server" ID="txtHoraTarea" CssClass="form-control" />
									<span id="clock" runat="server" class="input-group-addon input-sm">
										<i class="glyphicon glyphicon-time"></i>
									</span>
								</div>
							</div>
							<div class="col-md-4 pd-left-right-5">
								<h6>Tolerancia</h6>
								<div class="input-group">
									<asp:TextBox runat="server" ID="txtToleranciaTarea" CssClass="form-control" />
									<span id="min" runat="server" class="input-group-addon input-sm"><i>min</i> </span>
								</div>
							</div>
						</asp:Panel>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlTabOpcionesTarea">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="btnNuevoOpcionTarea" OnClick="btnNuevoOpcionTarea_Click" CssClass="btn btn-sm btn-default">
								<span class="glyphicon glyphicon-plus"></span>
								Nuevo
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnEditarOpcionTarea" OnClick="btnEditarOpcionTarea_Click" CssClass="btn btn-sm btn-default disabled">
								<span class="glyphicon glyphicon-pencil"></span>
								Editar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnEliminarOpcionTarea" OnClick="btnEliminarOpcionTarea_Click" CssClass="btn btn-sm btn-default disabled">
								<span class="glyphicon glyphicon-trash"></span>
								Eliminar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnAceptarOpcionTarea" OnClick="btnAceptarOpcionTarea_Click" CssClass="btn btn-sm btn-default btn-success">
								<span class="glyphicon glyphicon-ok"></span>
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnCancelarOpcionTarea" OnClick="btnCancelarOpcionTarea_Click" CssClass="btn btn-sm btn-default btn-danger">
								<span class="glyphicon glyphicon-remove"></span>								
							</asp:LinkButton>
							<asp:HiddenField runat="server" ID="hfUidOpcionTarea" />
						</div>
						<asp:Panel runat="server" ID="pnlAlertOpcionTarea">
							<div class="alert alert-danger">
								<asp:Label Text="Error" runat="server" ID="lblErrorOpcionTarea" />
								<asp:LinkButton ID="LinkButton1" OnClick="CloseAlertDialogOpcionTarea" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
							</div>
						</asp:Panel>
						<div class="col-md-8 pd-left-right-5">
							<h6>Opción</h6>
							<asp:TextBox runat="server" ID="txtNombreOpcionTarea" CssClass="form-control" />
						</div>
						<div class="col-md-2 pd-left-right-5">
							<h6>Orden</h6>
							<asp:TextBox runat="server" ID="txtOrdenOpcionTarea" CssClass="form-control" />
						</div>
						<div class="col-md-2 pd-left-right-5">
							<h6>Activo</h6>
							<asp:CheckBox runat="server" ID="cbActivoOpcionTarea" />
						</div>

						<div class="col-md-12 pd-left-right-5">
							<asp:GridView runat="server" ID="gvOpcionesTarea" DataKeyNames="UidOpciones" OnRowDataBound="gvOpcionesTarea_RowDataBound" OnSelectedIndexChanged="gvOpcionesTarea_SelectedIndexChanged" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm" SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField HeaderText="Opción" DataField="StrOpciones" />
									<asp:BoundField HeaderText="Orden" DataField="IntOrden" DataFormatString="{0:00}" />
									<asp:TemplateField HeaderText="Activo">
										<ItemTemplate>
											<asp:Label runat="server" ID="lblActivo" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField HeaderText="Activo" DataField="BlVisible" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
								</Columns>
								<EmptyDataTemplate>
									<div class="alert alert-info">
										No hay registros
									</div>
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlTabPeriodos">
						<asp:Panel runat="server" ID="pnlMessagePeriodicity">
							<asp:Label Text="Ocurrion un error" runat="server" ID="lblMessagePeriodicity" />
						</asp:Panel>
						<asp:HiddenField runat="server" ID="hfTipoPeriodicidad" />
						<div class="col-md-3 pd-left-right-5">
							<div class="row">
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="chkSinPeriodicida" AutoPostBack="true" OnCheckedChanged="chkSinPeriodicida_CheckedChanged" />
									Sin Periodicidad
								</label>
							</div>
							<div class="row">
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="chkPeriodicidadDiaria" AutoPostBack="true" OnCheckedChanged="chkPeriodicidadDiaria_CheckedChanged" />
									Diaria
								</label>
							</div>
							<div class="row">
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="chkPeriodicidadSemanal" AutoPostBack="true" OnCheckedChanged="chkPeriodicidadSemanal_CheckedChanged" />
									Semanal
								</label>
							</div>
							<div class="row">
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="chkPeriodicidadMensual" AutoPostBack="true" OnCheckedChanged="chkPeriodicidadMensual_CheckedChanged" />
									Mensual
								</label>
							</div>
							<div class="row">
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="chkPeriodicidadAnual" AutoPostBack="true" OnCheckedChanged="chkPeriodicidadAnual_CheckedChanged" />
									Anual
								</label>
							</div>
						</div>
						<div class="col-md-9">
							<asp:Panel runat="server" ID="subpnlPeriodicidadDiaria">
								<h5>
									<asp:CheckBox runat="server" ID="chkPDOpcion1" AutoPostBack="true" OnCheckedChanged="chkPDOpcion1_CheckedChanged" />
									Repetir cada
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPDNumeroDias"></asp:TextBox>
									dias
								</h5>
								<asp:CheckBox Text="Todos los dias de la semana" runat="server" ID="chkPDOpcion2" AutoPostBack="true" OnCheckedChanged="chkPDOpcion2_CheckedChanged" />
							</asp:Panel>
							<asp:Panel runat="server" ID="subpnlPeriodicidadSemanal">
								<div>
									<h5>Repetir cada
                                        <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPSNumeroSemanas"></asp:TextBox>
										semanas el:
									</h5>
									<div class="col-md-12" style="padding-left: 25px;">
										<asp:CheckBoxList runat="server" TextAlign="Right" CssClass="checkbox" ID="cblPSListaDias">
											<asp:ListItem Text="Lunes" />
											<asp:ListItem Text="Martes" />
											<asp:ListItem Text="Miercoles" />
											<asp:ListItem Text="Jueves" />
											<asp:ListItem Text="Viernes" />
											<asp:ListItem Text="Sabado" />
											<asp:ListItem Text="Domingo" />
										</asp:CheckBoxList>
									</div>
								</div>
							</asp:Panel>
							<asp:Panel runat="server" ID="subpnlPeriodicidadMensual">
								<h5>
									<asp:CheckBox runat="server" ID="chkPMRepetirCadaNDiasCadaNMes" AutoPostBack="true" OnCheckedChanged="chkPMRepetirCadaNDiasCadaNMes_CheckedChanged" />
									El dia
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPMDiaDelMes"></asp:TextBox>
									de cada
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPMNumeroDeMesesOpc1"></asp:TextBox>
									meses.
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnAgregarFechaMensual" OnClick="btnAgregarFechaMensual_Click">
										<span class="glyphicon glyphicon-plus"></span>
									</asp:LinkButton>
								</h5>
								<h5>
									<asp:CheckBox runat="server" ID="chkPMRepetirCadaNMESConParametros" AutoPostBack="true" OnCheckedChanged="chkPMRepetirCadaNMESConParametros_CheckedChanged" />
									El
                                    <asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPMListaNumerales">
										<asp:ListItem Text="Primer" Value="1" />
										<asp:ListItem Text="Segundo" Value="2" />
										<asp:ListItem Text="Tercer" Value="3" />
										<asp:ListItem Text="Cuarto" Value="4" />
										<asp:ListItem Text="Ultimo" Value="0" />
									</asp:DropDownList>
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPMDiaSemanaOLapsoDias">
										<asp:ListItem Text="lunes" Value="Lunes" />
										<asp:ListItem Text="martes" Value="Martes" />
										<asp:ListItem Text="miércoles" Value="Miercoles" />
										<asp:ListItem Text="jueves" Value="Jueves" />
										<asp:ListItem Text="viernes" Value="Viernes" />
										<asp:ListItem Text="sábado" Value="Sabado" />
										<asp:ListItem Text="domingo" Value="Domingo" />
									</asp:DropDownList>
									de cada
                                    <asp:TextBox runat="server" CssClass="txtContadorPeriodicidad" Text="1" ID="txtPMNumeroDeMesesOpc2"></asp:TextBox>
									meses
								</h5>
								<p>
									<asp:CheckBox runat="server" ID="chkPMTipoC" AutoPostBack="true" OnCheckedChanged="chkPMTipoC_CheckedChanged" />
									Los
									<asp:TextBox runat="server" Text="2" CssClass="txtContadorPeriodicidad" ID="txtPMDiasOpc3"></asp:TextBox>
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPMNumeralesOpc3">
										<asp:ListItem Text="Primeros" Value="1" />
										<asp:ListItem Text="Primer y ultimo" Value="2" />
										<asp:ListItem Text="Ultimos" Value="3" />
									</asp:DropDownList>
									dias
									de cada
									<asp:TextBox runat="server" CssClass="txtContadorPeriodicidad" Text="1" ID="txtPMMesesOpc3"></asp:TextBox>
									meses.
								</p>
							</asp:Panel>
							<asp:Panel runat="server" ID="subpnlPeriodicidadAnual">
								<h5>Repetir cada 
                                    <asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPANumeroAnios"></asp:TextBox>
									años
								</h5>
								<h5>
									<asp:CheckBox runat="server" ID="chkPAOpcion1" AutoPostBack="true" OnCheckedChanged="chkPAOpcion1_CheckedChanged" />
									El:                                    
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaMesesOpc1">
										<asp:ListItem Text="Enero" />
										<asp:ListItem Text="Febrero" />
										<asp:ListItem Text="Marzo" />
										<asp:ListItem Text="Abril" />
										<asp:ListItem Text="Mayo" />
										<asp:ListItem Text="Junio" />
										<asp:ListItem Text="Julio" />
										<asp:ListItem Text="Agosto" />
										<asp:ListItem Text="Septiembre" />
										<asp:ListItem Text="Octubre" />
										<asp:ListItem Text="Noviembre" />
										<asp:ListItem Text="Diciembre" />
									</asp:DropDownList>
									<asp:TextBox runat="server" Text="1" CssClass="txtContadorPeriodicidad" ID="txtPADiaDelMesOpc1"></asp:TextBox>
									<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnAgregarFechaAnual" OnClick="btnAgregarFechaAnual_Click">
										<span class="glyphicon glyphicon-plus"></span>
									</asp:LinkButton>
								</h5>

								<h5>
									<asp:CheckBox runat="server" ID="chkPAOpcion2" AutoPostBack="true" OnCheckedChanged="chkPAOpcion2_CheckedChanged" />
									El:
                                    <asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaNumerales">
										<asp:ListItem Text="Primer" Value="1" />
										<asp:ListItem Text="Segundo" Value="2" />
										<asp:ListItem Text="Tercer" Value="3" />
										<asp:ListItem Text="Cuarto" Value="4" />
										<asp:ListItem Text="Ultimo" Value="0" />
									</asp:DropDownList>
									<asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaPeriodosyDias">
										<asp:ListItem Text="lunes" />
										<asp:ListItem Text="martes" />
										<asp:ListItem Text="miércoles" />
										<asp:ListItem Text="jueves" />
										<asp:ListItem Text="viernes" />
										<asp:ListItem Text="sábado" />
										<asp:ListItem Text="domingo" />
									</asp:DropDownList>
									de
                                    <asp:DropDownList runat="server" CssClass="itemComboBoxPeriodicidad" ID="ddlPAListaMesesOpc2">
										<asp:ListItem Text="Enero" />
										<asp:ListItem Text="Febrero" />
										<asp:ListItem Text="Marzo" />
										<asp:ListItem Text="Abril" />
										<asp:ListItem Text="Mayo" />
										<asp:ListItem Text="Junio" />
										<asp:ListItem Text="Julio" />
										<asp:ListItem Text="Agosto" />
										<asp:ListItem Text="Septiembre" />
										<asp:ListItem Text="Octubre" />
										<asp:ListItem Text="Noviembre" />
										<asp:ListItem Text="Diciembre" />
									</asp:DropDownList>
								</h5>
							</asp:Panel>
							<asp:GridView
								runat="server"
								ID="gvFechasPeriodicidad"
								OnRowDataBound="gvFechasPeriodicidad_RowDataBound"
								DataKeyNames="Identificador"
								OnRowCommand="gvFechasPeriodicidad_RowCommand"
								OnRowDeleting="gvFechasPeriodicidad_RowDeleting"
								CssClass="table table-bordered table-condensed input-sm"
								AutoGenerateColumns="false"
								SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:BoundField HeaderText="Fecha" DataField="Text" />
									<asp:TemplateField HeaderText="Opciones">
										<ItemTemplate>
											<asp:LinkButton runat="server"
												CssClass="btn btn-sm btn-default"
												CommandName="Delete"
												CommandArgument='<%#Eval("Identificador")%>'>
												<span class="glyphicon glyphicon-minus"></span>
											</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlTabAsignacion">
						<div class="col-md-12 pd-left-right-5">
							<h6>Asignar a:</h6>
							<div class="col-sm-12 col-md-12 mg-bottom-10">
								<div class="checkbox-inline">
									<asp:RadioButton runat="server" ID="rbAsignarADepartamento" AutoPostBack="true" OnCheckedChanged="rbAsignarADepartamento_CheckedChanged" />
									Departamento
								</div>
								<div class="checkbox-inline">
									<asp:RadioButton runat="server" ID="rbAsignarAArea" AutoPostBack="true" OnCheckedChanged="rbAsignarAArea_CheckedChanged" />
									Area
								</div>
							</div>
						</div>
						<div class="input-group">
							<asp:TextBox runat="server" CssClass="form-control" ID="txtNombreDepartamentoAsignacion" />
							<span class="input-group-btn pd-0">
								<asp:LinkButton Text="Buscar Departamento" runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscarDepartamentoAsignacion" OnClick="btnBuscarDepartamentoAsignacion_Click" />
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-danger" ID="btnCancelarBuscarDepartamentoAsignacion" OnClick="btnCancelarBuscarDepartamentoAsignacion_Click"> 
								<span class="glyphicon glyphicon-remove"></span>
								</asp:LinkButton>
							</span>
						</div>
						<asp:Panel runat="server" ID="pnlAsignacionDepartamentos">
							<div class="col-md-12 pd-0">
								<div class="col-md-6 pd-left-right-5">
									<h6 class="font-bold">Asignados</h6>
									<asp:GridView runat="server" ID="gvDepartamentosAsignados" DataKeyNames="UidDepartamento" AutoGenerateColumns="false" OnRowDataBound="gvDepartamentosAsignados_RowDataBound" OnSelectedIndexChanged="gvDepartamentosAsignados_SelectedIndexChanged" CssClass="table table-bordered table-condensed input-sm">
										<Columns>
											<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
											<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
											<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
										</Columns>
										<EmptyDataTemplate>
											<div class="alert alert-info">
												No se encontraron departamentos asignados
											</div>
										</EmptyDataTemplate>
									</asp:GridView>
								</div>
								<div class="col-md-6 pd-left-right-5">
									<h6 class="font-bold">Disponibles</h6>
									<asp:GridView runat="server" ID="gvDepartamentosAsignacionBusqueda" DataKeyNames="UidDepartamento" AutoGenerateColumns="false" OnRowDataBound="gvDepartamentosAsignacionBusqueda_RowDataBound" OnSelectedIndexChanged="gvDepartamentosAsignacionBusqueda_SelectedIndexChanged" CssClass="table table-bordered table-condensed input-sm">
										<Columns>
											<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
											<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
											<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
										</Columns>
										<EmptyDataTemplate>
											<div class="alert alert-info">
												No se encontraron departamentos
											</div>
										</EmptyDataTemplate>
									</asp:GridView>
								</div>
							</div>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlAsignacionAreas">
							<div class="col-md-12 pd-left-right-5">
								<div class="col-md-6 pd-left-right-5">
									<h6 class="font-bold">Asignados</h6>
									<asp:GridView runat="server" ID="gvAreasAsignadas" DataKeyNames="UidArea" AutoGenerateColumns="false" OnRowDataBound="gvAreasAsignadas_RowDataBound" OnSelectedIndexChanged="gvAreasAsignadas_SelectedIndexChanged" CssClass="table table-bordered table-condensed input-sm">
										<Columns>
											<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
											<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
											<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
										</Columns>
										<EmptyDataTemplate>
											<div class="alert alert-info">
												No se encontraron Areas
											</div>
										</EmptyDataTemplate>
									</asp:GridView>
								</div>
								<div class="col-md-6 pd-left-right-5">
									<h6 class="font-bold">Disponible</h6>
									<asp:GridView runat="server" ID="gvDepartamentosAsignacionArea" DataKeyNames="UidDepartamento" AutoGenerateColumns="false" OnRowDataBound="gvDepartamentosAsignacionArea_RowDataBound" OnSelectedIndexChanged="gvDepartamentosAsignacionArea_SelectedIndexChanged" CssClass="table table-bordered table-condensed input-sm">
										<Columns>
											<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
											<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
											<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
										</Columns>
										<EmptyDataTemplate>
											<div class="alert alert-info">
												No se encontraron departamentos
											</div>
										</EmptyDataTemplate>
									</asp:GridView>
									<asp:GridView runat="server" ID="gvAreasDisponiblesDepartamento" DataKeyNames="UidArea" AutoGenerateColumns="false" OnRowDataBound="gvAreasDisponiblesDepartamento_RowDataBound" OnSelectedIndexChanged="gvAreasDisponiblesDepartamento_SelectedIndexChanged" CssClass="table table-bordered table-condensed input-sm">
										<Columns>
											<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
											<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
											<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
										</Columns>
										<EmptyDataTemplate>
											<div class="alert alert-info">
												No se encontraron Areas
											</div>
										</EmptyDataTemplate>
									</asp:GridView>
									<div class="text-right">
										<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnMostrarDepartamentosAsignacion" OnClick="btnMostrarDepartamentosAsignacion_Click">
											<span class="glyphicon glyphicon-triangle-left"></span>
											Volver
										</asp:LinkButton>
									</div>
								</div>
							</div>
						</asp:Panel>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlTabAntecesor">
						<asp:CheckBox Text="Activar Antecesor" runat="server" ID="cbActivarAntecesor" Checked="false" AutoPostBack="true" OnCheckedChanged="cbActivarAntecesor_CheckedChanged" />
						<asp:Panel runat="server" ID="pnlCamposAntecesor">
							<div class="col-md-12 pd-left-right-5">
								<div class="input-group">
									<asp:TextBox runat="server" CssClass="form-control" ID="txtNombreAntecesor" />
									<span class="input-group-btn">
										<asp:Button Text="Buscar" ID="btnBuscarAntecesor" runat="server" CssClass="btn btn-sm btn-default" OnClick="btnBuscarAntecesor_Click" />
										<asp:LinkButton runat="server" ID="btnCancelarBuscarAntecesor" CssClass="btn btn-sm btn-danger" OnClick="btnCancelarBuscarAntecesor_Click">
											<span class="glyphicon glyphicon-remove"></span>
										</asp:LinkButton>
									</span>
								</div>
							</div>
							<div class="col-md-12 pd-left-right-5">
								<asp:HiddenField runat="server" ID="hfAntecesorSeleccionado" />
								<asp:GridView runat="server" ID="gvListaTareasAntecesor" DataKeyNames="UidTarea" OnRowDataBound="gvListaTareasAntecesor_RowDataBound" OnSelectedIndexChanged="gvListaTareasAntecesor_SelectedIndexChanged" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed input-sm">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="IntFolio" HeaderText="Folio" />
										<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
										<asp:BoundField DataField="StrDescripcion" HeaderText="Descripcion" />
									</Columns>
									<EmptyDataTemplate>
										<div class="alert alert-info">
											No existe ningun antecesor con ese nombre.
										</div>
									</EmptyDataTemplate>
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								</asp:GridView>
							</div>
							<div class="col-md-12 pd-left-right-5">
								<h6>Cumplimiento</h6>
								Ejecutar tantos días
                                <asp:TextBox ID="txtDiasDespues" runat="server" Text="0" />
								después.
							</div>
							<div class="col-md-12 pd-left-right-5">
								<label class="checkbox-inline">
									<asp:CheckBox ID="cbUsarNotificacionAntecesor" runat="server" AutoPostBack="true" OnCheckedChanged="cbUsarNotificacionAntecesor_CheckedChanged" />
									Usar notificación del antecesor.
								</label>
							</div>
							<asp:Panel runat="server" ID="pnlNotificacionAntecesor">
								<div class="col-md-12 pd-left-right-5">
									<h6>Número de repeticiones</h6>
									<div class="row">
										<div class="col-xs-12">
											<label class="radio-inline">
												<asp:RadioButton ID="rdNumeroLimitado" runat="server" AutoPostBack="true" GroupName="MaximoDias" />Máximo
                                                    <asp:TextBox ID="txtMaximo" runat="server" />
												veces
											</label>
										</div>
										<div class="col-xs-12">
											<label class="radio-inline">
												<asp:RadioButton ID="rdIlimitado" runat="server" AutoPostBack="true" GroupName="MaximoDias" />Siempre
											</label>
										</div>
									</div>
								</div>
							</asp:Panel>
						</asp:Panel>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlTabNotificacion">
						<asp:CheckBox Text="Activar Notificación" runat="server" AutoPostBack="true" ID="cbHabilitarNotificacionTarea" OnCheckedChanged="cbHabilitarNotificacionTarea_CheckedChanged" />
						<asp:Panel runat="server" ID="pnlNotificationSendTo" CssClass="col-md-12 pd-left-right-5">
							<h6>Notificar a:</h6>
							<label class="radio-inline">
								<asp:CheckBox runat="server" ID="cbSendToAdministrador" />
								Administrador
							</label>
							<label class="radio-inline">
								<asp:CheckBox runat="server" ID="cbSendToSupervisor" />
								Supervisor
							</label>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlNotificacionVerdaderoFalso">
							<div class="col-md-12 pd-left-right-5">
								<h6>Se lanzará la notificación cuando el valor sea:</h6>
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbVerdaderoNotificacion" />
									Sí
								</label>
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbFalsoNotificacion" />
									No
								</label>
							</div>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlNotificacionValor">
							<div class="col-md-6 pd-left-right-5">
								<div class="row">
									<div class="col-xs-12">
										<label class="radio-inline">
											<asp:CheckBox ID="rbMayor" runat="server" />
											Mayor
										</label>
										<label class="radio-inline">
											<asp:CheckBox ID="rbMayorIgual" runat="server" />
											Mayor o igual
										</label>
									</div>
									<div class="col-xs-12">
										<div class="input-group">
											<asp:TextBox runat="server" ID="txtMayorQue" CssClass="form-control" />
											<asp:Label ID="txtMedida2" runat="server" CssClass="input-group-addon input-group-sm" />
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6 pd-left-right-5">
								<div class="row">
									<div class="col-xs-12">
										<label class="radio-inline">
											<asp:CheckBox ID="rbMenor" runat="server" />
											Menor
										</label>
										<label class="radio-inline">
											<asp:CheckBox ID="rbMenorIgual" runat="server" />
											Menor o igual
										</label>
									</div>
									<div class="col-xs-12">
										<div class="input-group">
											<asp:TextBox ID="txtMenorQue" runat="server" CssClass="form-control" />
											<asp:Label ID="txtMedida1" runat="server" CssClass="input-group-addon input-group-sm" />
										</div>
									</div>
								</div>
							</div>
						</asp:Panel>
						<asp:Panel runat="server" ID="pnlNotificacionOpcionMultiple">
							<div class="col-md-12 pd-left-right-5">
								<h6>Se lanzará la notificación la cumplir con las opciones:</h6>
								<asp:ListBox runat="server" ID="lbOpcionesSeleccionadasNotificacion" CssClass="form-control" SelectionMode="Multiple" />
							</div>
						</asp:Panel>
					</asp:Panel>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		function hora() {
			$('.input-group.clockpicker').clockpicker();
		}
	</script>

	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>
	<script>
		//<![CDATA[
		function pageLoad() {

			prepareFechaAll();
			hora();
		}
        //]]>
	</script>
</asp:Content>
