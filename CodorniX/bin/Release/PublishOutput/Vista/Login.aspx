﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Login.aspx.cs" Inherits="CodorniX.Vista.Login" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title></title>
	<link href="../Content/bootstrap-theme.css" rel="stylesheet" />
	<link href="../Content/bootstrap.css" rel="stylesheet" />
	<script src="../Scripts/jquery-3.3.1.js"></script>
	<script src="../Scripts/bootstrap.js"></script>
	<style type="text/css">
		.loader {
			float: right;
			border: 4px solid #f3f3f3; /* Light grey */
			border-top: 4px solid #2796d3; /* Blue */
			border-bottom: 4px solid #2796d3; /* Blue */
			border-radius: 50%;
			width: 25px;
			height: 25px;
			animation: spin 2s linear infinite;
		}

		@keyframes spin {
			0% {
				transform: rotate(0deg);
			}

			100% {
				transform: rotate(360deg);
			}
		}
	</style>
</head>
<body>
	<form id="form1" runat="server">
		<asp:ScriptManager runat="server" />
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-4 col-lg-4 col-md-offset-4 col-lg-offset-4">
					<div class="panel-primary" style="margin-top: 15%;">
						<div class="panel-heading">
							<asp:Label runat="server" CssClass="control-label input-lg">Login</asp:Label>
						</div>
						<div class="panel-body">
							<div>
								<asp:Label ID="lblMensaje" runat="server" />
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<asp:Label ID="lblUsuario" runat="server" CssClass="control-label" Text="Usuario:" AssociatedControlID="lblUsuario"> </asp:Label>
								<asp:TextBox ID="txtUsuario" runat="server" CssClass="form-control" />
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
								<asp:Label ID="lblPassword" runat="server" CssClass="control-label" Text="Contraseña:" AssociatedControlID="lblPassword"></asp:Label>
								<asp:TextBox ID="txtPassword" CssClass="form-control" runat="server" TextMode="Password" />
							</div>
							<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="margin-top: 8px;">
								<table style="width: 100%;">
									<tr>
										<td valign="middle">
											<span class="toggle" data-toggle="pnlRecoverPassword" style="cursor: pointer;">Recuperar contraseña</span>
										</td>
										<td class="text-right">
											<asp:Button ID="btnLogin" OnClick="btnLogin_Click" CssClass="btn btn-sm btn-default pull-right" runat="server" Text="Iniciar Sessión" />
										</td>
									</tr>
								</table>
							</div>
						</div>
					</div>
					<hr style="margin-top: 5px; margin-bottom: 5px;" />
					<asp:Panel runat="server" CssClass="panel panel-default" Style="border-radius: 0px;" ID="pnlRecoverPassword">
						<div class="panel-heading">
							<table style="width: 100%;">
								<tr>
									<td>Recuperar Contraseña	
									</td>
									<td class="text-right">
										<asp:UpdateProgress runat="server">
											<ProgressTemplate>
												<div class="loader"></div>
											</ProgressTemplate>
										</asp:UpdateProgress>
										<%--<asp:Label ID="lblIconoCompletado" Text="<span class='glyphicon glyphicon-ok'></span>" runat="server" style="color:#4EB146;"/>--%>
									</td>
								</tr>
							</table>
						</div>
						<div class="panel-body" style="padding: 0px 5px 5px 5px;">
							<asp:UpdatePanel runat="server">
								<ContentTemplate>
									<h6>Nombre de usuario</h6>
									<div class="input-group">
										<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtRecoverUsername" />										
										<div class="input-group-btn">
											<asp:LinkButton runat="server" ID="btnRecoverPassword" OnClick="btnRecoverPassword_Click" CssClass="btn btn-sm btn-primary">
												<asp:Label runat="server" ID="lblIconoCompletado" CssClass="glyphicon glyphicon-chevron-right"></asp:Label>
											</asp:LinkButton>
										</div>
									</div>
									<asp:Label Text="" runat="server" ID="lblMessageRecovery" CssClass="text-danger small" />									
									<small>
										<em>Se le enviara un correo electronico para recuperar la contraseña</em>
									</small>
								</ContentTemplate>
							</asp:UpdatePanel>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
	</form>

	<script>
		$('[id^=pnlRecoverPassword]').hide();
		$(document).ready(function () {
			$('.toggle').click(function () {
				$input = $(this);
				$target = $('#' + $input.attr('data-toggle'));
				$target.slideToggle('fast', function () {
				});
			});
		});
	</script>
</body>
</html>
