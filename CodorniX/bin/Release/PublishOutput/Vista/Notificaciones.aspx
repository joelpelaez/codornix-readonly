﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Notificaciones.aspx.cs" Inherits="CodorniX.Vista.Notificaciones" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Notificaciones</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-8">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="text-center">
								Lista de Tareas del Día
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body panel-pd">
					<div class="text-right">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="btnMostrar" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click" Text="Mostrar" />
							<asp:LinkButton runat="server" ID="btnLimpiar" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
                            <span class="glyphicon glyphicon-trash"></span>
                            Limpiar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnBuscar" CssClass="btn btn-sm btn-default" OnClick="btnBuscar_Click">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Buscar
							</asp:LinkButton>
						</div>
					</div>
					<asp:PlaceHolder ID="panelBusqueda" runat="server">
						<div class="row">
							<%--<div class="col-md-6">
								<h6>Ordenamiento</h6>
								<asp:GridView ID="gvOrdenamiento" DataKeyNames="ID" OnRowDataBound="gvOrdenamiento_RowDataBound" OnRowCommand="gvOrdenamiento_RowCommand" runat="server" CssClass="table table-condensed table-bordered input-sm" AutoGenerateColumns="false">
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
										<asp:BoundField HeaderText="Columna" DataField="Parametro" />
										<asp:TemplateField HeaderText="Ordenar">
											<ItemTemplate>
												<asp:CheckBox runat="server" OnCheckedChanged="Ordenamiento_CheckedChanged" Checked='<%# Eval("BlOrdenar") %>' />
											</ItemTemplate>
										</asp:TemplateField>
										<asp:TemplateField HeaderText="Direccion">
											<ItemTemplate>
												<asp:LinkButton runat="server" ID="btnChangeDirection" CommandName="CambiarOrden" CommandArgument='<%# Eval("ID") %>' CssClass="btn btn-sm btn-default" >
													<asp:Label ID="lblIcon" CssClass="glyphicon glyphicon-arrow-up" runat="server" />
												</asp:LinkButton>
											</ItemTemplate>
										</asp:TemplateField>
										<asp:BoundField DataField="Direccion" HeaderText="Direccion" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
									</Columns>
									<EmptyDataTemplate>
										No hay criterios de ordenamiento
									</EmptyDataTemplate>
								</asp:GridView>
							</div>--%>
							<div class="col-xs-6">
								<h6>Fecha</h6>
								<div class="input-group date">
									<span class="input-group-addon">Desde:</span>
									<asp:TextBox ID="txtFechaInicio" runat="server" CssClass="form-control" />
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
								<div class="input-group date" style="padding-top: 10px">
									<span class="input-group-addon">Hasta:</span>
									<asp:TextBox ID="txtFechaFin" runat="server" CssClass="form-control" />
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-xs-6">
								<h6>Estado</h6>
								<asp:ListBox ID="lbEstados" runat="server" CssClass="form-control" SelectionMode="Multiple" />
							</div>
						</div>
					</asp:PlaceHolder>
					<asp:Panel runat="server" ID="pnlAlertBusqueda">
						<div class="row">
							<div class="col-md-12">
								<div class="alert alert-danger">
									<asp:LinkButton runat="server" CssClass="close" OnClick="btnCloseAlertSearch_Click">
										<span aria-hidden="true">&times;</span>
									</asp:LinkButton>
									<asp:Label runat="server" ID="lblErrorBusqueda"></asp:Label>
								</div>
							</div>
						</div>
					</asp:Panel>
					<asp:PlaceHolder ID="panelResultados" runat="server">
						<div class="row">
							<div class="col-xs-12">
								<asp:GridView runat="server" AllowPaging="true" PageSize="10" ID="dgvNotificaciones" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidMensajeNotificacion" OnSelectedIndexChanged="dgvNotificaciones_SelectedIndexChanged" OnRowDataBound="dgvNotificaciones_RowDataBound" OnPageIndexChanging="dgvNotificaciones_PageIndexChanging" OnSorting="dgvNotificaciones_Sorting" AllowSorting="true">
									<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
									<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
									<EmptyDataTemplate>
										No tienes ninguna tarea asignada para el día de hoy.
									</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
										<asp:BoundField DataField="FolioCumplimiento" DataFormatString="{0:0000}" HeaderText="Folio C." SortExpression="FolioC" />
										<asp:BoundField DataField="FolioTarea" DataFormatString="{0:0000}" HeaderText="Folio T." SortExpression="FolioT" />
										<asp:BoundField DataField="StrTarea" HeaderText="Tarea" SortExpression="Tarea" />
										<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
										<asp:BoundField DataField="StrEstadoNotificacion" HeaderText="Completado" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
										<asp:BoundField DataField="DtFechaCumplimiento" DataFormatString="{0:dd/MM/yyyy}" HeaderText="Fecha" FooterStyle-Wrap="False" HeaderStyle-HorizontalAlign="Center" />
										<asp:BoundField DataField="DtFechaCumplimiento" DataFormatString="{0:t}" HeaderText="Hora" HeaderStyle-Wrap="False" />
										<asp:TemplateField HeaderText="Estado" SortExpression="Estado">
											<ItemTemplate>
												<asp:Label runat="server" ID="lblCompleto" />
											</ItemTemplate>
										</asp:TemplateField>
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
		<div class="col-md-4">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="text-center">
						Detalles
					</div>
				</div>
				<div class="row">
					<div class="col-xs-6">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="btnLeido" CssClass="btn btn-sm btn-default" OnClick="btnLeido_Click" Text="Leido" />
						</div>
					</div>
				</div>
				<div class="panel-body">
					<asp:HiddenField runat="server" ID="fldUidMensajeNotificacion" />
					<div class="row">
						<asp:Panel runat="server" ID="pnlAlertDetail">
							<div class="col-md-12">
								<div class="alert alert-danger">
									<asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">
										<span aria-hidden="true">&times;</span>
									</asp:LinkButton>
									<asp:Label runat="server" ID="lblErrorDetail"></asp:Label>
								</div>
							</div>
						</asp:Panel>
						<div class="col-sm-2 col-md-4 text-right text-wrap">
							<b>Departamento:</b>
						</div>
						<div class="col-sm-8 col-md-8 text-wrap">
							<asp:Label runat="server" ID="lblDepto" Text="(ninguna)" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2 col-md-4 text-right text-wrap margin-top-10">
							<b>Area:</b>
						</div>
						<div class="col-sm-8 col-md-8 text-wrap margin-top-10">
							<asp:Label runat="server" ID="lblArea" Text="(ninguna)" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2 col-md-4 text-right text-wrap margin-top-10">
							<b>Tarea:</b>
						</div>
						<div class="col-sm-8 col-md-8 text-wrap margin-top-10">
							<asp:Label runat="server" ID="lblTarea" Text="(ninguna)" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2 col-md-4 text-right text-wrap margin-top-10">
							<b>Tipo de Tarea:</b>
						</div>
						<div class="col-sm-8 col-md-8 text-wrap margin-top-10">
							<asp:Label runat="server" ID="lblTipoTarea" Text="(ninguna)" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2 col-md-4 text-right text-wrap margin-top-10">
							<b>Valor Ingresado:</b>
						</div>
						<div class="col-sm-8 col-md-8 text-wrap margin-top-10">
							<asp:Label runat="server" ID="lblResultado" Text="(ninguno)" />
						</div>
					</div>
					<div class="row">
						<div class="col-sm-2 col-md-4 text-right text-wrap margin-top-10">
							<b>Observaciones:</b>
						</div>
						<div class="col-sm-8 col-md-8 text-wrap margin-top-10">
							<asp:Label runat="server" ID="lblObservaciones" Text="(ninguno)" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		//<![CDATA[
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
		}

		function setStartDateLimit(start, end) {
			$(".input-group.date").datepicker('remove');
			$(".input-group.date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: start,
				endDate: end
			})
			startDateReady = true;
		}


		function pageLoad() {
			enableDatapicker();
		}
        //]]>
	</script>
</asp:Content>
