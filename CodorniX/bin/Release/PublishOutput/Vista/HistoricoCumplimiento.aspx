﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="HistoricoCumplimiento.aspx.cs" Inherits="CodorniX.Vista.HistoricoCumplimiento" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-7">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="row">
						<div class="col-xs-12">
							<div class="text-center">
								Lista de Tareas del Día
							</div>
						</div>
					</div>
				</div>
				<div class="panel-body panel-pd">
					<table class="table" style="margin: 0px;">
						<tr>
							<td style="padding: 0px;">
								<asp:LinkButton runat="server" ID="btnOrdenamiento" CssClass="btn btn-sm btn-default" OnClick="btnOrdenamiento_Click" Text="Ordenamiento">														
								</asp:LinkButton>
							</td>
							<td style="padding: 0px;">
								<div class="text-right">
									<div class="btn-group">
										<asp:LinkButton runat="server" ID="btnMostrar" CssClass="btn btn-sm btn-default" OnClick="btnMostrar_Click" Text="Mostrar" />
										<asp:LinkButton runat="server" ID="btnLimpiar" CssClass="btn btn-sm btn-default" OnClick="btnLimpiar_Click">
											<span class="glyphicon glyphicon-trash"></span>
											Limpiar
										</asp:LinkButton>
										<asp:LinkButton runat="server" ID="btnActualizar" CssClass="btn btn-sm btn-default" OnClick="btnActualizar_Click">
											<span class="glyphicon glyphicon-refresh"></span>
											Buscar
										</asp:LinkButton>
									</div>
								</div>
							</td>
						</tr>
					</table>

					<div class="col-md-12 pd-0">
						<asp:Panel runat="server" ID="pnlAlertBusquedas">
							<div class="alert alert-danger">
								<asp:Label ID="lblError" Text="Error" CssClass="text-danger" runat="server" />
							</div>
						</asp:Panel>
					</div>
					<asp:Panel runat="server" ID="pnlOrdenamiento">
						<div class="col-md-12 table-responsive pd-left-right-5">
							<h6>Ordenamiento</h6>
							<asp:GridView ID="gvOrdenamiento" DataKeyNames="ID" OnRowCommand="gvOrdenamiento_RowCommand" OnRowDataBound="gvOrdenamiento_RowDataBound" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-responsive input-sm" SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
									<asp:BoundField HeaderText="Columna" DataField="Parametro" />
									<asp:TemplateField HeaderText="Ordenar" HeaderStyle-Width="40">
										<ItemTemplate>
											<asp:CheckBox runat="server" OnCheckedChanged="Ordenamiento_CheckedChanged" Checked='<%# Eval("BlOrdenar") %>' />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:TemplateField HeaderText="Direccion" HeaderStyle-Width="40">
										<ItemTemplate>
											<asp:LinkButton runat="server" ID="btnChangeDirection" CommandName="CambiarOrden" CommandArgument='<%# Eval("ID") %>' CssClass="btn btn-sm btn-default">
												<asp:Label ID="lblIcon" CssClass="glyphicon glyphicon-arrow-up" runat="server" />
											</asp:LinkButton>
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="Direccion" HeaderText="Direccion" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
									<asp:TemplateField HeaderText="Posicion" HeaderStyle-Width="40" HeaderStyle-HorizontalAlign="Center">
										<ItemTemplate>
											<table>
												<tr>
													<td>
														<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" CommandArgument='<%# Eval("ID") %>' CommandName="Up">
															<span class="glyphicon glyphicon-arrow-up"></span>
														</asp:LinkButton>
													</td>
													<td>
														<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" CommandArgument='<%# Eval("ID") %>' CommandName="Down">															
															<span class="glyphicon glyphicon-arrow-down"></span>
														</asp:LinkButton>
													</td>
												</tr>
											</table>
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
								<EmptyDataTemplate>
									No hay criterios de ordenamiento
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:Panel>
					<asp:PlaceHolder ID="panelBusqueda" runat="server">
						<div class="col-sm-6 col-md-4 pd-left-right-5">
							<h6>Folio Tarea</h6>
							<asp:TextBox runat="server" ID="txtFolioTareaFiltro" CssClass="form-control"></asp:TextBox>
						</div>
						<div class="col-sm-8 col-md-8 pd-left-right-5">
							<h6>Tarea</h6>
							<asp:TextBox ID="txtNombre" runat="server" CssClass="form-control" />
						</div>
						<div class="col-sm-4 col-md-4 pd-left-right-5">
							<h6>Folio Cumplimiento</h6>
							<asp:TextBox runat="server" ID="txtFolioCumplimientoFiltro" CssClass="form-control"></asp:TextBox>
						</div>
						<div class="col-sm-4 col-md-4 pd-left-right-5">
							<h6>Folio Turno</h6>
							<asp:TextBox runat="server" ID="txtFolioTurno" CssClass="form-control"></asp:TextBox>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Tipo Tarea</h6>
							<asp:DropDownList ID="ddTipo" runat="server" CssClass="form-control" />
						</div>
						<div class="col-sm-4 col-md-4 pd-left-right-5">
							<h6>Fecha Inicio</h6>
							<div class="input-group date start-date">
								<asp:TextBox ID="txtFechaInicio" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4 col-md-4 pd-left-right-5">
							<h6>Fecha Fin</h6>
							<div class="input-group date start-date">
								<asp:TextBox ID="txtFechaFin" CssClass="form-control" runat="server" />
								<span class="input-group-addon input-sm ">
									<i class="glyphicon glyphicon-calendar"></i>
								</span>
							</div>
						</div>
						<div class="col-sm-4 col-md-4 pd-left-right-5">
							<h6>Departamento</h6>
							<asp:DropDownList ID="ddDepartamentos" runat="server" CssClass="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddDepartamentos_SelectedIndexChanged" />
						</div>
						<div class=" col-sm-4 col-md-4 pd-left-right-5">
							<h6>Área</h6>
							<asp:DropDownList ID="ddAreas" runat="server" CssClass="form-control" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h6>Estado</h6>
							<asp:ListBox ID="lbEstados" runat="server" CssClass="form-control" SelectionMode="Multiple" />
						</div>
						<asp:Panel runat="server" ID="pnlFiltrosAdministrador">
							<div class="col-md-4 pd-left-right-5">
								<h6>Encargado</h6>
								<asp:DropDownList runat="server" ID="ddlFiltroEncargado" CssClass="form-control">
								</asp:DropDownList>
							</div>
						</asp:Panel>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="panelResultados" runat="server">
						<div class="col-md-12 pd-0">
							<asp:GridView runat="server" AllowPaging="true" PageSize="8" ID="dgvTareasPendientes" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidTarea,UidDepartamento,UidArea,UidCumplimiento,UidPeriodo" OnSelectedIndexChanged="dgvTareasPendientes_SelectedIndexChanged" OnRowDataBound="dgvTareasPendientes_RowDataBound" OnPageIndexChanging="dgvTareasPendientes_PageIndexChanging" OnSorting="dgvTareasPendientes_Sorting">
								<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
								<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								<EmptyDataTemplate>
									No existe cumplimiento con los parametros actuales
								</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
									<asp:BoundField DataField="IntFolioTurno" HeaderText="F. Turno" SortExpression="FolioTurno" />
									<asp:BoundField DataField="IntFolioTarea" HeaderText="Folio T." SortExpression="FolioTarea" />
									<asp:BoundField DataField="IntFolio" HeaderText="Folio C." SortExpression="FolioCumpl" />
									<asp:BoundField DataField="StrTarea" HeaderText="Tarea" SortExpression="Tarea" />
									<asp:BoundField DataField="StrDepartamento" HeaderText="Departamento" SortExpression="Departamento" />
									<asp:BoundField DataField="StrArea" HeaderText="Área" SortExpression="Area" />
									<asp:BoundField DataField="DtFechaHora" HeaderText="Hora" SortExpression="Hora" HtmlEncode="false" DataFormatString="{0:dd/MM/yy HH\:mm}" />
									<asp:BoundField DataField="StrEstadoCumplimiento" HeaderText="Completado" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Requerido" HeaderStyle-CssClass="hidden" FooterStyle-CssClass="hidden" ItemStyle-CssClass="hidden" />
									<asp:TemplateField HeaderText="Estado" SortExpression="Estado">
										<ItemTemplate>
											<asp:Label runat="server" ID="lblCompleto" />
										</ItemTemplate>
									</asp:TemplateField>
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
		<div class="col-md-5">
			<div class="panel panel-primary">
				<div class="panel-heading">
					<div class="text-center">
						Tarea
					</div>
				</div>
				<div class="panel-body panel-pd">
					<asp:HiddenField runat="server" ID="fldUidTarea" />
					<asp:HiddenField runat="server" ID="fldUidDepartamento" />
					<asp:HiddenField runat="server" ID="fldUidArea" />
					<asp:HiddenField runat="server" ID="fldUidCumplimiento" />
					<asp:HiddenField runat="server" ID="fldUidPerido" />
					<asp:HiddenField runat="server" ID="fldEditing" />
					<div class="row">
						<div class="col-md-6 pd-right-5">
							<h6>Departamento:</h6>
							<asp:TextBox runat="server" ID="lblDepto" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-md-6 pd-left-5">
							<h6>Área:</h6>
							<asp:TextBox runat="server" ID="lblArea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-md-12">
							<h6>Tarea:</h6>
							<asp:TextBox runat="server" ID="lblTarea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-md-6 pd-right-5">
							<h6>Tipo:</h6>
							<asp:TextBox runat="server" ID="lblTipoTarea" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-md-6 pd-left-5">
							<h6>Periodicidad:</h6>
							<asp:TextBox runat="server" ID="lblPeriodicidad" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
						<div class="col-md-6">
							<h6>Fecha de cumplimiento</h6>
							<asp:TextBox runat="server" ID="lblFechaCumplimiento" CssClass="form-control disabled" Text="(ninguna)" Enabled="false" />
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<div class="form-inline">
								<h6>Valor:</h6>
								<asp:PlaceHolder runat="server" ID="frmSiNo">
									<label class="radio-inline">
										<asp:RadioButton runat="server" ID="rbYes" GroupName="rbgSiNo" />
										Sí
									</label>
									<label class="radio-inline">
										<asp:RadioButton runat="server" ID="rbNo" GroupName="rbgSiNo" />
										No
									</label>
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="frmValue">
									<div class="form-group" id="valorUnico" runat="server">
										<div class="input-group">
											<asp:TextBox runat="server" ID="txtValor" CssClass="form-control" />
											<div class="input-group-addon">
												<asp:Label runat="server" ID="lblUnidad" />
											</div>
										</div>
									</div>
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="frmTwoValues">
									<div class="input-group">
										<div class="input-group-addon">
											Desde:
										</div>
										<asp:TextBox runat="server" ID="txtValor1" CssClass="form-control" />
										<div class="input-group-addon">
											<asp:Label runat="server" ID="lblUnidad1" />
										</div>
									</div>
									<div class="input-group">
										<div class="input-group-addon">
											Hasta:
										</div>
										<asp:TextBox runat="server" ID="txtValor2" CssClass="form-control" />
										<div class="input-group-addon">
											<asp:Label runat="server" ID="lblUnidad2" />
										</div>
									</div>
								</asp:PlaceHolder>
								<asp:PlaceHolder runat="server" ID="frmOptions">
									<asp:DropDownList runat="server" ID="ddOpciones" CssClass="form-control" />
								</asp:PlaceHolder>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<label>Observaciones:</label>
							<asp:TextBox TextMode="MultiLine" ID="txtObservaciones" runat="server" CssClass="form-control" />
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		//<![CDATA[
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date.start-date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
		}

		function setStartDateLimit(start, end) {
			$(".input-group.date.start-date").datepicker('remove');
			$(".input-group.date.start-date").datepicker({
				todayBtn: true,
				clearBtn: true,
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				startDate: start,
				endDate: end
			})
			startDateReady = true;
		}


		function pageLoad() {
			enableDatapicker();
		}
        //]]>
	</script>
</asp:Content>
