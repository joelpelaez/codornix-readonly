﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="HistoricoTareas.aspx.cs" Inherits="CodorniX.Vista.HistoricoTareas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					Tareas
				</div>
				<div class="panel-body panel-pd">
					<asp:Panel runat="server" ID="pnlBotonesIzquierda">
						<div class="text-right">
							<div class="btn-group">
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnOcultarFiltros" OnClick="btnOcultarFiltros_Click">
									<span class="glyphicon glyphicon-eye-open"></span>
									<asp:Label runat="server" ID="lblOcultarFiltros" Text="Mostrar" />
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnLimpiarFiltros" OnClick="btnLimpiarFiltros_Click">
								<span class="glyphicon glyphicon-trash"></span>
								Limpiar
								</asp:LinkButton>
								<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscar" OnClick="btnBuscar_Click">
								<span class="glyphicon glyphicon-search"></span>
								Buscar
								</asp:LinkButton>
							</div>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlAlertBusqueda">
						<div class="alert alert-danger">
							<asp:Label Text="Error" runat="server" ID="lblErrorBusqueda" />
							<asp:LinkButton ID="btnCloseAlerBusqueda" OnClick="CloseAlertDialogSearch" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlFiltros">
						<div class="col-md-4 pd-left-right-5">
							<h6>Folio</h6>
							<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroFolio" />
						</div>
						<div class="col-md-8 pd-left-right-5">
							<h6>Nombre</h6>
							<asp:TextBox runat="server" CssClass="form-control" ID="txtFiltroDescripcion" />
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlListaTareas">
						<div class="col-md-12 pd-left-right-5">
							<asp:GridView runat="server" OnRowDataBound="gvTareasDepartamento_RowDataBound" OnSelectedIndexChanged="gvTareasDepartamento_SelectedIndexChanged" AllowPaging="true" OnPageIndexChanging="gvTareasDepartamento_PageIndexChanging" AllowSorting="true" OnSorting="gvTareasDepartamento_Sorting" DataKeyNames="UidTarea,UidDepartamento,UidArea,UidPeriodicidad" ID="gvTareasDepartamento" CssClass="table table-bordered table-hover table-condensed input-sm" AutoGenerateColumns="false" SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
									<asp:BoundField HeaderText="Folio" DataField="IntFolio" SortExpression="Folio" />
									<asp:BoundField HeaderText="Nombre" DataField="StrNombre" SortExpression="Nombre" />
									<asp:BoundField HeaderText="Tipo" DataField="StrTipoTarea" SortExpression="Tipo" />
								</Columns>
								<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
								<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								<EmptyDataTemplate>
									<div class="info">
										No hay registros de tareas
									</div>
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlDetallesCumplimiento">
						<div class="text-right">
							<asp:LinkButton runat="server" ID="btnCerrarDetalleCumplimiento" CssClass="btn btn-sm btn-danger" OnClick="btnCerrarDetalleCumplimiento_Click">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
						</div>
						<div class="col-md-12 pd-left-right-5">
							<strong>Datos de cumplimiento.</strong>
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h6>Fecha
								<asp:Label CssClass="glyphicon glyphicon-time text-danger" Text="(Atrasada)" runat="server" ToolTip="La Tarea fue realizada despues del tiempo programado" ID="lblTareaAtrasada" /></h6>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtFechaCumplimiento" />
						</div>
						<div class="col-md-6 pd-left-right-5">
							<h6>Hora</h6>
							<asp:TextBox runat="server" CssClass="form-control" Enabled="false" ID="txtHoraCumplimiento" />
						</div>
						<div class="col-md-12 pd-left-right-5">
							<h6>Valor Ingresado</h6>
							<asp:Panel runat="server" ID="pnlCumplimientoSeleccion">
								<asp:DropDownList runat="server" CssClass="form-control" ID="ddlOpcionSeleccionada" Enabled="false">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlCumplimientoVerdaderoFalso">
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbYes" Enabled="false" />
									Sí
								</label>
								<label class="radio-inline">
									<asp:RadioButton runat="server" ID="rbNo" Enabled="false" />
									No
								</label>
							</asp:Panel>
							<asp:Panel runat="server" ID="pnlCumplimientoValor">
								<div class="input-group">
									<span class="input-group-addon">
										<asp:Label Text="$" runat="server" ID="lblUnidadMedida" />
									</span>
									<asp:TextBox runat="server" CssClass="form-control" ID="txtValorIngresado" Enabled="false" />
								</div>
							</asp:Panel>
						</div>
						<div class="col-md-12 pd-left-right-5">
							<h6>Observaciones</h6>
							<asp:TextBox runat="server" ID="txtObservaciones" TextMode="MultiLine" CssClass="form-control" Enabled="false" />
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">
					<asp:Label Text="Historico" runat="server" ID="lblTituloPanelDerecho" />
				</div>
				<div class="panel-body panel-pd">
					<asp:Panel runat="server" ID="pnlAlertGeneral">
						<div class="alert alert-danger">
							<asp:Label Text="Error" runat="server" ID="lblErrorGeneral" />
							<asp:LinkButton ID="LinkButton1" OnClick="CloseAlertDialogGeneral" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</div>
					</asp:Panel>
					<div class="col-md-12 pd-0">
						<strong>Detalles Tarea.</strong>
					</div>
					<div class="col-md-12 pd-left-right-5 mg-bottom-10">
						<div class="col-md-12 pd-0">
							<h5 class="mg-0-5">
								<small>Nombre:</small>
								<asp:Label Text="(ninguno)" runat="server" ID="lblNombreTarea" />
							</h5>
						</div>
						<div class="col-md-12 pd-0">
							<h5 class="mg-0-5">
								<small>Descripcion:</small>
								<asp:Label Text="(sin descripción)" runat="server" ID="lblDescripcion" />
							</h5>
						</div>
						<div class="col-md-6 pd-0">
							<h5 class="mg-0-5">
								<small>Departamento:</small>
								<asp:Label Text="(global)" runat="server" ID="lblDepartamento" />
							</h5>
						</div>
						<div class="col-md-6 pd-0">
							<h5 class="mg-0-5">
								<small>Area:</small>
								<asp:Label Text="(global)" runat="server" ID="lblArea" />
							</h5>
						</div>
						<div class="col-md-6 pd-0">
							<h5 class="mg-0-5">
								<small>Tipo:</small>
								<asp:Label Text="(ninguno)" runat="server" ID="lblTipoTarea" />
							</h5>
						</div>
						<div class="col-md-6 pd-0">
							<h5 class="mg-0-5">
								<small>Periodicidad</small>
								<asp:Label Text="(ninguno)" runat="server" ID="lblPeriodicidad" />
							</h5>
						</div>
					</div>
					<div class="col-md-12 pd-0 mg-bottom-10">
						<div class="col-md-6 pd-0">
							<strong>Busqueda.</strong>
						</div>
						<div class="col-md-6 pd-0">
							<div class="text-right">
								<div class="btn-group">
									<asp:LinkButton Text="text" runat="server" CssClass="btn btn-default btn-sm" ID="btnOcultarFiltrosCumplimiento" OnClick="btnOcultarFiltrosCumplimiento_Click">
										<span class="glyphicon glyphicon-eye-close"></span>
										<asp:Label Text="Ocultar" runat="server" ID="lblMostrarFiltrosCumplimiento" />
									</asp:LinkButton>
									<asp:LinkButton Text="text" runat="server" CssClass="btn btn-default btn-sm" ID="btnLimpiarFiltrosCumplimiento" OnClick="btnLimpiarFiltrosCumplimiento_Click">
										<span class="glyphicon glyphicon-trash"></span>
										Limpiar
									</asp:LinkButton>
									<asp:LinkButton Text="text" runat="server" CssClass="btn btn-default btn-sm" ID="btnBuscarCumplimiento" OnClick="btnBuscarCumplimiento_Click">
										<span class="glyphicon glyphicon-search"></span>
										Buscar
									</asp:LinkButton>
								</div>
							</div>
						</div>
						<asp:Panel runat="server" ID="pnlFiltrosCumplimiento">
							<div class="col-md-3 pd-left-right-5">
								<h6>Folio Cumplimiento</h6>
								<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFolioCumplimiento" />
							</div>
							<div class="col-md-3 pd-left-right-5">
								<h6>Desde:</h6>
								<div class="input-group date">
									<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFechaInicioCumplimiento" />
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-3 pd-left-right-5">
								<h6>Hasta:</h6>
								<div class="input-group date">
									<asp:TextBox runat="server" CssClass="form-control input-sm" ID="txtFiltroFechaFinCumplimiento" />
									<span class="input-group-addon">
										<i class="glyphicon glyphicon-calendar"></i>
									</span>
								</div>
							</div>
							<div class="col-md-3 pd-left-right-5">
								<h6>Estado</h6>
								<asp:DropDownList runat="server" CssClass="form-control input-sm" ID="ddlFiltroEstadoCumplimiento">
									<asp:ListItem Text="text1" />
									<asp:ListItem Text="text2" />
								</asp:DropDownList>
							</div>
						</asp:Panel>
					</div>
					<div class="col-md-12 pd-0">
						<strong>Resultados.</strong>
					</div>
					<div class="col-md-12  pd-left-right-5">
						<asp:HiddenField runat="server" ID="hfSortDirectionGvCumplimientos" Value="ASC" />
						<asp:GridView runat="server" AllowSorting="true" OnSorting="gvCumplimientos_Sorting" OnRowDataBound="gvCumplimientos_RowDataBound" OnSelectedIndexChanged="gvCumplimientos_SelectedIndexChanged" DataKeyNames="UidCumplimiento" ID="gvCumplimientos" AutoGenerateColumns="false" CssClass="table table-bordered table-condensed table-hover input-sm" SelectedRowStyle-BackColor="#dff0d8">
							<Columns>
								<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
								<asp:BoundField HeaderText="F. Turno" DataField="IntFolioTurno" SortExpression="FolioT" />
								<asp:BoundField HeaderText="F. Cumplimiento" DataField="IntFolio" SortExpression="FolioC" />
								<asp:BoundField HeaderText="Fecha" DataField="DtFechaHora" DataFormatString="{0:dd/MM/yyyy}" SortExpression="Fecha" />
								<asp:BoundField HeaderText="Estado" DataField="StrEstadoCumplimiento" SortExpression="Estado" />
								<asp:BoundField HeaderText="Atrasada" DataField="BlAtrasada" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
							</Columns>
							<EmptyDataTemplate>
								<div class="info">
									No hay cumplimientos
								</div>
							</EmptyDataTemplate>
						</asp:GridView>
					</div>
				</div>
			</div>
		</div>
	</div>
	<script>
		var startDateReady = false;
		var endDateReady = false;

		function enableDatapicker() {
			if (!startDateReady) {
				$(".input-group.date").datepicker({
					todayBtn: true,
					clearBtn: true,
					autoclose: true,
					todayHighlight: true,
					language: 'es',
				}).on('changeDate', function (e) {
					setEndDateLimit(e.format());
				});
			}
		}
		function pageLoad() {
			enableDatapicker();
		}
	</script>
</asp:Content>
