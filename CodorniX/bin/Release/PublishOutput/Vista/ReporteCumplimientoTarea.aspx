﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="ReporteCumplimientoTarea.aspx.cs" Inherits="CodorniX.Vista.ReporteCumplimientoTarea" %>
<%@ Register Assembly="Microsoft.ReportViewer.WebForms, Version=14.0.0.0, Culture=neutral, PublicKeyToken=89845DCD8080CC91" Namespace="Microsoft.Reporting.WebForms" TagPrefix="rsweb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="container-fluid">
		<div class="row">
			<div class="col-xs-12">
				<rsweb:ReportViewer ID="rvReport" runat="Server" ProcessingMode="Local" AsyncRendering="true"></rsweb:ReportViewer>
			</div>
		</div>
	</div>
</asp:Content>
