﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="Supervision.aspx.cs" Inherits="CodorniX.Vista.Supervision" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
	<title>Supervisión de la sucursal</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<asp:PlaceHolder runat="server" ID="PanelBusqueda">
				<div class="panel panel-primary">
					<div class="panel-heading text-center">Turnos de encargados</div>
					<div class="pull-right">
						<asp:LinkButton ID="btnActualizar" OnClick="btnActualizar_Click" CssClass="btn btn-sm btn-default" runat="server">
                            <span class="glyphicon glyphicon-refresh"></span>
                            Actualizar
						</asp:LinkButton>
					</div>
					<div class="panel-body">
						<div style="padding-top: 15px;" class="row">
							<div class="col-xs-12">
								<asp:Label runat="server" ID="lblmensaje" />
								<asp:GridView ID="dvgDepartamentos" runat="server" CssClass="table table-bordered table-sm table-condensed input-sm" OnRowDataBound="dvgDepartamentos_RowDataBound" OnSelectedIndexChanged="dvgDepartamentos_SelectedIndexChanged" AutoGenerateColumns="false" DataKeyNames="UidDepartamento,UidUsuario,DtFecha,UidInicioTurno,UidEstadoTurno">
									<EmptyDataTemplate>No hay Historial</EmptyDataTemplate>
									<Columns>
										<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
										<asp:BoundField DataField="IntFolio" HeaderText="Folio" />
										<asp:BoundField DataField="StrUsuario" HeaderText="Usuario" />
										<asp:BoundField DataField="StrTurno" HeaderText="Turno" />
										<asp:BoundField DataField="DtFecha" DataFormatString="{0:d}" HeaderText="Fecha" />
										<asp:BoundField DataField="StrDepartamento" HeaderText="Depto." />
										<asp:BoundField DataField="DtFechaHoraInicio" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Inicio" />
										<asp:BoundField DataField="DtFechaHoraFin" DataFormatString="{0:dd/MM/yyyy HH:mm}" HeaderText="Fin" />
										<asp:BoundField DataField="StrEstadoTurno" HeaderText="Estado" />
									</Columns>
								</asp:GridView>
							</div>
						</div>
					</div>
				</div>
			</asp:PlaceHolder>
		</div>
		<asp:Label ID="lblnoiniciarturno" runat="server" />

		<div id="divCerrarturno" runat="server" class="col-md-6">
			<asp:Label ID="lblAceptarCerrarTurno" runat="server" />

			<asp:LinkButton ID="btnAceptarCerrarTurno" CssClass="btn btn-sm btn-success" runat="server" OnClick="btnAceptarCerrarTurno_Click">
				<asp:Label ID="Label2" CssClass="glyphicon glyphicon-ok" runat="server" />
			</asp:LinkButton>

			<asp:LinkButton ID="btnCancelarCerrarTurno" CssClass="btn btn-sm btn-danger" runat="server" OnClick="btnCancelarCerrarTurno_Click">
                <span class="glyphicon glyphicon-remove"></span>
			</asp:LinkButton>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Detalle del Turno</div>
				<div class="panel-body panel-pd">
					<table style="width:100%;">
						<tr>
							<td>
								<div class="btn-group">
									<asp:LinkButton ID="btnReporte" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnReporte_Click" OnClientClick="document.forms[0].target ='_blank';">
										<span class="glyphicon glyphicon-file"></span>
										Reporte
									</asp:LinkButton>
									<asp:LinkButton ID="btnLista" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnLista_Click" OnClientClick="document.forms[0].target = '_blank';">
										<span class="glyphicon glyphicon-list"></span>
										Lista
									</asp:LinkButton>
								</div>
							</td>
							<td class="text-right">
								<label class="radio-inline">
									<asp:CheckBox runat="server" ID="cbCumplimientoPosterior" Enabled="false"/>
									<small>Cumplimiento posterior.</small>
								</label>
								<div class="btn-group">
									<asp:LinkButton ID="btnIniciarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnIniciarTurno_Click" Text="Iniciar" />
									<asp:LinkButton ID="btnInicioTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnInicioTurno_Click" Text="Tomar" />
									<asp:LinkButton ID="btnBloquearTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnBloquearTurno_Click" Text="Bloquear" />
									<asp:LinkButton ID="btnCerrarTurno" CssClass="btn btn-sm btn-default" runat="server" OnClick="btnCerrarTurno_Click" Text="Cerrar" />
								</div>
							</td>
						</tr>
					</table>
					<asp:TextBox CssClass="hide" ID="txtUidDepartamento" runat="server"></asp:TextBox>
					<asp:TextBox CssClass="hide" ID="txtUidArea" runat="server"></asp:TextBox>
					<asp:PlaceHolder ID="panelAlert" runat="server" Visible="false">
						<div class="alert alert-danger">
							<asp:LinkButton ID="btnCloseAlert" runat="server" CssClass="close" OnClick="btnCloseAlert_Click">x</asp:LinkButton>
							<strong>Error: </strong>
							<asp:Label ID="lblError" runat="server" />
						</div>
					</asp:PlaceHolder>
					<asp:HiddenField ID="fldUidPeriodo" runat="server" Value="" />
					<asp:HiddenField ID="fldUidUsuario" runat="server" Value="" />
					<asp:HiddenField ID="fldUidInicioTurno" runat="server" Value="" />
					<ul class="nav nav-tabs">
						<li class="active" id="activeResumen" runat="server">
							<asp:LinkButton ID="tabResumen" runat="server" Text="Resumen" OnClick="tabResumen_Click" />
						</li>
						<li id="activeCompletadas" runat="server">
							<asp:LinkButton ID="tabCompletadas" runat="server" OnClick="tabCompletadas_Click">
								Completadas
								<span class="badge">
									<asp:Label runat="server" ID="lblContadorC" Text="0"></asp:Label>
								</span>
							</asp:LinkButton>
						</li>
						<li id="activeNoCompletadas" runat="server">
							<asp:LinkButton ID="tabNoCompletadas" runat="server" OnClick="tabNoCompletadas_Click">
								No Completadas
								<span class="badge">
									<asp:Label runat="server" ID="lblContadorNC" Text="0"></asp:Label>
								</span>
							</asp:LinkButton>
						</li>
						<li id="activeRequeridas" runat="server">
							<asp:LinkButton ID="tabRequeridas" runat="server" OnClick="tabRequeridas_Click">
								Requeridas
								<span class="badge">
									<asp:Label runat="server" ID="lblContadorRNC" Text="0"></asp:Label>
								</span>
							</asp:LinkButton>
						</li>
					</ul>
					<asp:PlaceHolder ID="PanelResumen" runat="server">
						<div class="col-md-12 pd-left-right-5">
							<h5>Departamento</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblDepartamento" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Turno</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblTurno" />
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>No Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblNoCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Requeridas No Cumplidas</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblRequeridasNoCumplidas"></asp:TextBox>
						</div>
						<div class="col-md-4 pd-left-right-5">
							<h5>Estado</h5>
							<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblEstadoDelTurno"></asp:TextBox>
						</div>
						<div class="col-md-6 pd-0">
							<h6 class="text-center mg-0-5 font-bold">Apertura
							</h6>
							<div class="col-md-12 pd-0">
								<div class="col-md-6 col-sm-6 pd-left-right-5">
									<small>Fecha</small>
									<asp:TextBox runat="server" ID="lblFechaInicio" CssClass="form-control" Enabled="false" />
								</div>
								<div class="col-md-6 col-sm-6 pd-left-right-5">
									<small>Hora</small>
									<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraInicio"></asp:TextBox>
								</div>
							</div>
						</div>
						<div class="col-md-6 pd-0">
							<h6 class="text-center mg-0-5 font-bold">Cierre
							</h6>
							<div class="col-md-12 pd-0">
								<div class="col-md-6 col-sm-6 pd-left-right-5">
									<small>Fecha</small>
									<asp:TextBox runat="server" ID="lblFechaFin" CssClass="form-control" Enabled="false" />
								</div>
								<div class="col-md-6 col-sm-6 pd-left-right-5">
									<small>Hora</small>
									<asp:TextBox CssClass="form-control" Enabled="false" runat="server" ID="lblHoraFin"></asp:TextBox>
								</div>
							</div>
						</div>
						<div visible="false" id="divTareasRequeridasNoCumplidas" runat="server" style="padding-top: 25px">
							<asp:Label runat="server">Requeridas No cumplidas</asp:Label>
							<asp:GridView ID="DvgTareasRequeridasNoCumplidas" runat="server" CssClass="table table-bordered" AutoGenerateColumns="false" DataKeyNames="UidTareaRequeridaNoCumplida">
								<EmptyDataTemplate>No hay Departamentos Asignados</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrStatus" HeaderText="Estatus" />
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodo" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelCompletadas" runat="server">
						<div id="divTareasCumplidas" runat="server" class="margin-top-10">
							<asp:GridView ID="DVGTareasCumplidas" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidTareaCumplida">
								<EmptyDataTemplate>No hay Tareas Cumplidas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio C." />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrStatus" HeaderText="Estatus" />
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodo" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="PanelNoCompletadas" runat="server">
						<div id="divTareasNoCumplidas" runat="server" class="margin-top-10">
							<asp:GridView ID="DvgTareasNoCumplidas" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false" DataKeyNames="UidTareaNoCumplida">
								<EmptyDataTemplate>No hay Tareas No Cumplidas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrStatus" HeaderText="Estatus" />
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodo" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
					<asp:PlaceHolder ID="panelRequeridas" runat="server">
						<div class="margin-top-10">
							<asp:GridView ID="dgvTareasRequeridas" runat="server" CssClass="table table-bordered table-condensed input-sm" AutoGenerateColumns="false">
								<EmptyDataTemplate>No hay Tareas Requeridas</EmptyDataTemplate>
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hide" />
									<asp:BoundField DataField="IntFolio" DataFormatString="{0:0000}" HeaderText="Folio T." />
									<asp:BoundField DataField="StrNombre" HeaderText="Nombre" />
									<asp:BoundField DataField="StrStatus" HeaderText="Estatus" />
									<asp:BoundField DataField="StrTipoFrecuencia" HeaderText="Periodo" />
									<asp:BoundField DataField="StrTipoTarea" HeaderText="Tipo" />
								</Columns>
							</asp:GridView>
						</div>
					</asp:PlaceHolder>
				</div>
			</div>
		</div>
	</div>
	<script type="text/javascript">
		//<![CDATA[
		function prepareFechaAll() {
			$('.input-group.date.extra').datepicker({
				autoclose: true,
				todayHighlight: true,
				language: 'es',
				format: 'dd/mm/yyyy',
				clearBtn: true,
				todayBtn: true,
			});
		}//]]>
	</script>

	<script>
		//<![CDATA[
		function pageLoad() {
			prepareFechaAll();
		}
        //]]>
	</script>
</asp:Content>
