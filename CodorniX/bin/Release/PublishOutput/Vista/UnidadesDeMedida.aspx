﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Vista/Site1.Master" AutoEventWireup="true" CodeBehind="UnidadesDeMedida.aspx.cs" Inherits="CodorniX.Vista.UnidadesDeMedida" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContenidoDelSitio" runat="server">
	<div class="row">
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Unidades</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-left-right-5 text-right">
						<div class="btn-group">
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnMostrarFiltros" OnClick="btnMostrarFiltros_Click">
								<span class="glyphicon glyphicon-eye-closed"></span>
								<asp:Label Text="Mostrar" runat="server" ID="lblMostrarFiltros" />
							</asp:LinkButton>
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnLimpiarFiltros" OnClick="btnLimpiarFiltros_Click">
								<span class="glyphicon glyphicon-trash"></span>
								Limpiar
							</asp:LinkButton>
							<asp:LinkButton runat="server" CssClass="btn btn-sm btn-default" ID="btnBuscar" OnClick="btnBuscar_Click">
								<span class="glyphicon glyphicon-search"></span>
								Buscar
							</asp:LinkButton>
						</div>
					</div>
					<asp:Panel runat="server" ID="pnlFiltros">
						<div class="col-md-12 pd-0">
							<div class="col-md-8 pd-left-right-5">
								<small>Unidad Medida</small>
								<asp:TextBox runat="server" ID="txtFiltroUnidadMedida" CssClass="form-control" />
							</div>
							<div class="col-md-4 pd-left-right-5">
								<small>Estatus</small>
								<asp:DropDownList runat="server" ID="ddlFiltroEstatus" CssClass="form-control">
									<asp:ListItem Text="Todos" Value="Empty" />
									<asp:ListItem Text="Activo" Value="True" />
									<asp:ListItem Text="Inactivo" Value="False" />
								</asp:DropDownList>
							</div>
						</div>
					</asp:Panel>
					<asp:Panel runat="server" ID="pnlListaUnidades">
						<div class="col-md-12 pd-left-right-5">
							<asp:GridView runat="server"
								ID="gvUnidadesMedida"
								OnRowDataBound="gvUnidadesMedida_RowDataBound"
								OnSelectedIndexChanging="gvUnidadesMedida_SelectedIndexChanging"
								OnSelectedIndexChanged="gvUnidadesMedida_SelectedIndexChanged"
								OnPageIndexChanging="gvUnidadesMedida_PageIndexChanging"
								DataKeyNames="UidUnidadMedida"
								AllowPaging="true"
								PageSize="10"
								AutoGenerateColumns="false"
								CssClass="table table-bordered table-hover table-condensed input-sm"
								SelectedRowStyle-BackColor="#dff0d8">
								<Columns>
									<asp:ButtonField CommandName="Select" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden" />
									<asp:BoundField HeaderText="Unidad de medida" DataField="StrTipoUnidad" />
									<asp:TemplateField HeaderText="Estatus">
										<ItemTemplate>
											<asp:Label ID="lblEstatusIcon" runat="server" />
										</ItemTemplate>
									</asp:TemplateField>
									<asp:BoundField DataField="BlEstatus" HeaderStyle-CssClass="hide" FooterStyle-CssClass="hide" ItemStyle-CssClass="hidden"/>
								</Columns>
								<PagerSettings Mode="NumericFirstLast" Position="Top" PageButtonCount="4" />
								<PagerStyle CssClass="pagination-ys" HorizontalAlign="Center" />
								<EmptyDataTemplate>
									Sin resultados
								</EmptyDataTemplate>
							</asp:GridView>
						</div>
					</asp:Panel>
				</div>
			</div>
		</div>
		<div class="col-md-6">
			<div class="panel panel-primary">
				<div class="panel-heading text-center">Detalles</div>
				<div class="panel-body panel-pd">
					<div class="col-md-12 pd-left-right-5">
						<div class="btn-group">
							<asp:LinkButton runat="server" ID="btnNew" CssClass="btn btn-sm btn-default" OnClick="btnNew_Click">
								<span class="glyphicon glyphicon-plus"></span>
								Nuevo
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnEdit" CssClass="btn btn-sm btn-default" OnClick="btnEdit_Click">
								<span class="glyphicon glyphicon-pencil"></span>
								Editar
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnOk" CssClass="btn btn-sm btn-success" OnClick="btnOk_Click">
								<span class="glyphicon glyphicon-ok"></span>
							</asp:LinkButton>
							<asp:LinkButton runat="server" ID="btnCancel" CssClass="btn btn-sm btn-danger" OnClick="btnCancel_Click">
								<span class="glyphicon glyphicon-remove"></span>
							</asp:LinkButton>
							<asp:HiddenField ID="hfUidUnidadMedida" runat="server" />
						</div>
					</div>
					<div class="col-md-12 pd-left-right-5">
						<asp:Panel runat="server" ID="pnlAlertGeneral">
							<asp:Label Text="Error: " runat="server" ID="lblAlertGeneral" />
							<asp:LinkButton OnClick="HideAlertGeneral" CssClass="close" runat="server"><span aria-hidden="true">&times;</span></asp:LinkButton>
						</asp:Panel>
					</div>
					<div class="col-md-8 pd-left-right-5">
						<small>Unidad de medida</small>
						<asp:TextBox runat="server" CssClass="form-control" ID="txtUnidadMedida" />
					</div>
					<div class="col-md-4 pd-left-right-5">
						<small>Estatus</small>
						<asp:DropDownList runat="server" ID="ddlEstatus" CssClass="form-control">
							<asp:ListItem Text="Activo" Value="True" />
							<asp:ListItem Text="Inactivo" Value="False" />
						</asp:DropDownList>
					</div>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
