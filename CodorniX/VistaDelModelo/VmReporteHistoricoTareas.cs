﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VmReporteHistoricoTareas
	{
		#region Properties
		Cumplimiento.Repository CumplimientoRepositorio = new Cumplimiento.Repository();
		Departamento.Repository DepartamentoRepository = new Departamento.Repository();
		Area.Repository AreaRepository = new Area.Repository();
		Periodicidad.Repositorio PeriodicidadRepository = new Periodicidad.Repositorio();
		TipoFrecuencia.Repositorio TFRepository = new TipoFrecuencia.Repositorio();
		TareaOpcion.Repositorio TORepository = new TareaOpcion.Repositorio();
		private Tarea.Repositorio TareaRepository = new Tarea.Repositorio();
		public Empresa eEmpresa
		{
			get;
			set;
		}
		public Sucursal sSucursal
		{
			get;
			set;
		}
		public Tarea _Tarea
		{
			get;
			set;
		}
		public Departamento _Departamento
		{
			get;
			set;
		}
		public Area _Area
		{
			get;
			set;
		}
		public Periodicidad _Periodicidad
		{
			get;
			set;
		}
		public TipoFrecuencia _Frecuencia
		{
			get;
			set;
		}
		public Cumplimiento _Cumplimiento
		{
			get;
			set;
		}
		public List<Departamento> LsDepartamentos
		{
			get;
			set;
		}
		public List<Area> LsAreas
		{
			get;
			set;
		}
		public List<EstadoCumplimiento> LsEstadosCumplimiento
		{
			get;
			set;
		}
		public List<Tarea> LsTareas
		{
			get;
			set;
		}
		public List<Cumplimiento> LsCumplimientosTarea
		{
			get;
			set;
		}
		public List<TareaOpcion> LsOpcionesTarea
		{
			get;
			set;
		}
		#endregion

		public VmReporteHistoricoTareas()
		{
		}

		#region Methods
		/// <summary>
		/// Realizar busqueda de tareas por sucursal
		/// </summary>
		/// <param name="Nombre"></param>
		/// <param name="DtFechaInicio"></param>
		/// <param name="DtFechaFin"></param>
		/// <param name="UidSucursal"></param>
		/// <param name="Departamento"></param>
		/// <param name="Encargado"></param>
		/// <param name="Folio"></param>
		public void BusquedaTareas(string Nombre, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidSucursal, string Departamento, string Areas, int Folio)
		{
			this.LsTareas = TareaRepository.BuscarTareasReporte(UidSucursal, Folio, Nombre, DtFechaInicio, DtFechaFin, Departamento, Areas);
		}

		/// <summary>
		/// Obtener datos de tarea mediante su identificador
		/// </summary>
		/// <param name="UidTarea"></param>
		public void GetTarea(Guid UidTarea)
		{
			this._Tarea = TareaRepository.Encontrar(UidTarea);
		}

		/// <summary>
		/// Obtener departamento mediante su identificador
		/// </summary>
		/// <param name="UidDepartamento"></param>
		public void GetDepartamento(Guid UidDepartamento)
		{
			this._Departamento = DepartamentoRepository.Encontrar(UidDepartamento);
		}

		/// <summary>
		/// Obtener datos de area mediante su identificador
		/// </summary>
		/// <param name="UidArea"></param>
		public void GetArea(Guid UidArea)
		{
			this._Area = AreaRepository.Find(UidArea);
		}

		/// <summary>
		/// Obtener detalles de periodicidad mediante su identificador
		/// </summary>
		/// <param name="UidPeriodicidad"></param>
		public void GetPeriodicidad(Guid UidPeriodicidad)
		{
			this._Periodicidad = PeriodicidadRepository.ConsultarPeriodicidad(UidPeriodicidad);
		}

		/// <summary>
		/// Obtener tipo de frecuencia mediante el identificador
		/// </summary>
		/// <param name="UidTarea"></param>
		public void GetTipoFrecuenciaByTarea(Guid UidTarea)
		{
			this._Frecuencia = TFRepository.FindByTarea(UidTarea);
		}

		/// <summary>
		/// Obtener lista de cumplimientos de la tarea seleccionada
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <param name="UidUsuario"></param>
		public void GetCumplimientoTarea(Guid UidTarea, Guid UidUsuario, int? FolioCumplimiento, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidEstado)
		{
			this.LsCumplimientosTarea = CumplimientoRepositorio.GetCumplimientosTarea(UidTarea, UidUsuario, FolioCumplimiento, DtFechaInicio, DtFechaFin, UidEstado);
		}

		/// <summary>
		/// Obtener detalles del cumplimiento mediante el identificador
		/// </summary>
		/// <param name="UidCumplimiento"></param>
		public void GetCumplimientoById(Guid UidCumplimiento)
		{
			this._Cumplimiento = CumplimientoRepositorio.Find(UidCumplimiento); 
		}

		/// <summary>
		/// Obtener opciones tarea
		/// </summary>
		/// <param name="UidTarea"></param>
		public void GetOpcionesTarea(Guid UidTarea)
		{
			this.LsOpcionesTarea = TORepository.Buscar(UidTarea);
		}

		public void ObtenerEstadosCumplimiento()
		{
			EstadoCumplimiento.Repository ECRepository = new EstadoCumplimiento.Repository();
			this.LsEstadosCumplimiento = ECRepository.FindAll();
			this.LsEstadosCumplimiento.Insert(0, new EstadoCumplimiento() { UidEstadoCumplimiento = Guid.Empty, StrEstadoCumplimiento = "Todos" });
		}

		public void GetDepartamentosSucursal(Guid UidSucursal)
		{
			this.LsDepartamentos = DepartamentoRepository.TodosSucursal(UidSucursal);
			//if (LsDepartamentos == null)
			//	LsDepartamentos = new List<Departamento>();
			//this.LsDepartamentos.Insert(0, new Departamento() { UidDepartamento = Guid.Empty, StrNombre = "Todos" });
		}

		public void GetAreasSucursal(Guid UidSucursal)
		{
			this.LsAreas = AreaRepository.FindAllSucursal(UidSucursal);
			//if (LsAreas == null)
			//	LsAreas = new List<Area>();
			//LsAreas.Insert(0, new Area() { UidArea = Guid.Empty, StrNombre = "Todos" });
		}


		public void GetDatosEmpresa(Guid UidEmpresa)
		{
			Empresa.Repository EmpresaRepository = new Empresa.Repository();
			this.eEmpresa = EmpresaRepository.Find(UidEmpresa);
		}
		public void GetDatosSucursal(Guid UidSucursal)
		{
			Sucursal.Repository SucursalReporitory = new Sucursal.Repository();
			this.sSucursal = SucursalReporitory.Find(UidSucursal);
		}
		#endregion
	}
}