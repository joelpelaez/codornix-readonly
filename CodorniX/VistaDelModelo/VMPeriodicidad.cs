﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
    public class VMPeriodicidad
    {
        private TipoFrecuencia.Repositorio TipoFrecuenciaRepositorio = new TipoFrecuencia.Repositorio();
        private Periodicidad.Repositorio PeriodicidadRepositorio = new Periodicidad.Repositorio();
        private PeriodicidadMensual.Repositorio PeriodicidadMensualRepositorio = new PeriodicidadMensual.Repositorio();
        private Ordinal.Repositorio OrdinalRepositorio = new Ordinal.Repositorio();
        private Dias.Repositorio DiasRepositorio = new Dias.Repositorio();
        private PeriodicidadAnual.Repositorio PeriodicidadAnualrepositorio = new PeriodicidadAnual.Repositorio();
        private Meses.Repositorio MesesRepositorio = new Meses.Repositorio();
        private PeriodicidadSemanal.Repositorio PeriodicidadSemanalRepositorio = new PeriodicidadSemanal.Repositorio();

		private Cumplimiento.Repository CRepository = new Cumplimiento.Repository();

		private Periodicidad _CPeriodicidad;
        public Periodicidad Cperiodicidad
        {
            get { return _CPeriodicidad; }
            set { _CPeriodicidad = value; }
        }

        private TipoFrecuencia _CTipoFrecuencia;
        public TipoFrecuencia CTipoFrecuencia
        {
            get { return _CTipoFrecuencia; }
            set { _CTipoFrecuencia = value; }
        }

        private PeriodicidadMensual _CPeriodicidadMensual;
        public PeriodicidadMensual CPeriodicidadMensual
        {
            get { return _CPeriodicidadMensual; }
            set { _CPeriodicidadMensual = value; }
        }

        private PeriodicidadAnual _CPeriodicidadAnual;
        public PeriodicidadAnual CPeriodicidadAnual
        {
            get { return _CPeriodicidadAnual; }
            set { _CPeriodicidadAnual = value; }
        }

        private PeriodicidadSemanal _CPeriodicidadSemanal;
        public PeriodicidadSemanal CPeriodicidadSemanal
        {
            get { return _CPeriodicidadSemanal; }
            set { _CPeriodicidadSemanal = value; }
        }

        private Ordinal _COrdinal;
        public Ordinal COrdinal
        {
            get { return _COrdinal; }
            set { _COrdinal = value; }
        }

        private Dias _CDias;
        public Dias CDias
        {
            get { return _CDias; }
            set { _CDias = value; }
        }

        private Meses _CMeses;
        public Meses CMeses
        {
            get { return _CMeses; }
            set { _CMeses = value; }
        }

        public void ConsultarTipoFrecuencia(string TipoFrecuencia)
        {
            TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
            {
                TipoFrecuencia= TipoFrecuencia
            };

            _CTipoFrecuencia = TipoFrecuenciaRepositorio.Buscar(criterio);
        }

        public void ObtenerTipoFrecuencia(Guid UidTipoFrecuencia)
        {
            TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
            {
                UidTipoFrecuencia = UidTipoFrecuencia
            };

            _CTipoFrecuencia = TipoFrecuenciaRepositorio.Buscar(criterio);
        }
        public bool GuardarPeriodicidad(string Frecuencia, string UidTipoFrecuencia, string FechaInicio, string FechaFin)
        {
            _CPeriodicidad = new Periodicidad()
            {
                IntFrecuencia= Convert.ToInt32(Frecuencia),
                UidTipoFrecuencia=new Guid(UidTipoFrecuencia),
                DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(FechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture)),
                DtFechaFin = FechaFin.Length == 0 ? (DateTime?)null : Convert.ToDateTime(DateTime.ParseExact(FechaFin, "dd/MM/yyyy", CultureInfo.InvariantCulture)),
            };
            bool Resultado = false;
            try
            {
                Resultado = _CPeriodicidad.GuardarDatos();
            }
            catch (Exception)
            {

                throw;
            }
            return Resultado;
        }

        public bool ModificarPeriodicidad(string UidPeriodicidad, string FechaFin)
        {

            Cperiodicidad = new Periodicidad()
            {
                UidPeriodicidad =new Guid( UidPeriodicidad),
                DtFechaFin = FechaFin.Length == 0 ? (DateTime?)null : Convert.ToDateTime(DateTime.ParseExact(FechaFin, "dd/MM/yyyy", CultureInfo.InvariantCulture))
            };
            bool Resultado = false;
            try
            {
                Resultado = Cperiodicidad.ModificarDatos();
            }
            catch (Exception)
            {

                throw;
            }
            return Resultado;
        }

        public bool GuardarPeriodicidadMensual(Guid UidPeriodicidad, string DiasMes, string DiasSemanas)
        {
            CPeriodicidadMensual = new PeriodicidadMensual()
            {
                UidPeriodicidad =UidPeriodicidad,
                IntDiasMes =Convert.ToInt32(  DiasMes),
                IntDiasSemana= Convert.ToInt32(DiasSemanas),
            };
            bool Resultado = false;
            try
            {
                Resultado = CPeriodicidadMensual.GuardarDatos();
            }
            catch (Exception)
            {

                throw;
            }
            return Resultado;
        }

        public bool GuardarPeriodicidadAnual(Guid UidPeriodicidad, string DiasMes, string DiasSemanas, string Numero)
        {
            CPeriodicidadAnual = new PeriodicidadAnual()
            {
                UidPeriodicidad = UidPeriodicidad,
                IntDiasMes =Convert.ToInt32( DiasMes),
                IntDiasSemanas = Convert.ToInt32(DiasSemanas),
                IntNumero = Convert.ToInt32(Numero),
            };
            bool Resultado = false;
            try
            {
                Resultado = CPeriodicidadAnual.GuardarDatos();
            }
            catch (Exception)
            {

                throw;
            }
            return Resultado;
        }

        public bool GuardarPeriodicidadSemanal(Guid UidPeriodicidad, bool lunes, bool martes, bool miercoles, bool jueves, bool viernes, bool sabado, bool domingo)
        {
            CPeriodicidadSemanal = new PeriodicidadSemanal()
            {
                UidPeriodicidad = UidPeriodicidad,
                BlLunes = lunes,
                BlMartes = martes,
                BlMiercoles = miercoles,
                BlJueves= jueves,
                BlViernes = viernes,
                BlSabado = sabado,
                BlDomingo = domingo,

            };
            bool Resultado = false;
            try
            {
                Resultado = CPeriodicidadSemanal.GuardarDatos();
            }
            catch (Exception)
            {

                throw;
            }
            return Resultado;
        }

        public void ObtenerPeriodicidad(Guid Periodicidad)
        {
            _CPeriodicidad = PeriodicidadRepositorio.ConsultarPeriodicidad(Periodicidad);
        }

        public void ObtenerPeriodicidadMensual(Guid UidPeriodicidad)
        {
            _CPeriodicidadMensual = PeriodicidadMensualRepositorio.ConsultarPeriodicidadMensual(UidPeriodicidad);
        }

        public void ObtenerPeriodicidadAnual(Guid UidPeriodicidad)
        {
            _CPeriodicidadAnual = PeriodicidadAnualrepositorio.ConsultarPeriodicidadAnual(UidPeriodicidad);
        }

        public void ObtenerPeriodicidadSemanal(Guid UidPeriodicidad)
        {
            _CPeriodicidadSemanal = PeriodicidadSemanalRepositorio.ConsultarPeriodicidadSemanal(UidPeriodicidad);
        }

        public void ConsultarOrdinal(string Ordinal)
        {
            _COrdinal = OrdinalRepositorio.ObtenerOrdinal(Ordinal);
        }
        public void ObtenerDias(string dias)
        {
            _CDias = DiasRepositorio.ObtenerDias(dias);
        }

        public void ObtenerMeses(string mes)
        {
            _CMeses = MesesRepositorio.ObtenerMeses(mes);
        }


		public DateTime CalcularCumplimientoPeriodicidadSemanal(DateTime DtInicio, int Frecuencia, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
		{
			DateTime DtNewDate = DateTime.Today;
			DtNewDate = PeriodicidadSemanalRepositorio.CalculateNext(DtInicio, true, Frecuencia, BlLunes, BlMartes, BlMiercoles, BlJueves, BlViernes, BlSabado, BlDomingo);
			return DtNewDate;
		}
		public DateTime CalcularCumplimientoPeriodicidadMensual(string Tipo, DateTime DtInicio, List<FechaPeriodicidad> LsDias,int IntOrdinal,int DiaSemana,string DiasMes, int Frecuencia)
		{
			DateTime DtNewDate = DateTime.Today;
			if (Tipo.Equals("A"))
				DtNewDate = PeriodicidadMensualRepositorio.CalculateNextTypeA(DtInicio, true, DiasMes, Frecuencia);
			else if (Tipo.Equals("B"))
			{
				DtNewDate = PeriodicidadMensualRepositorio.CalculateNextTypeB(DtInicio, true, LsDias, Frecuencia);
			}
			else if (Tipo.Equals("C"))
				DtNewDate = PeriodicidadMensualRepositorio.CalculateNextTypeC(DtInicio, true, IntOrdinal, DiaSemana, Frecuencia);
				return DtNewDate;
		}
		public DateTime CalcularCumplimientoPeriodicidadAnual(string Tipo, DateTime DtInicio, List<DateTime> LsFechas, int IntMes, int IntOrdinal, int DiaSemana, int Frecuencia)
		{
			DateTime DtNewDate = DateTime.Today;
			if (Tipo.Equals("A"))
				DtNewDate = PeriodicidadAnualrepositorio.CalculateNextTypeA(DtInicio, true, LsFechas, Frecuencia);
			else
				DtNewDate = PeriodicidadAnualrepositorio.CalculateNextTypeB(DtInicio, true, IntOrdinal, DiaSemana, IntMes, Frecuencia);
			return DtNewDate;
		}

		public bool GuardarPrimerCumplimiento(Guid UidTarea, Guid UidDepartamento, Guid UidArea, DateTime DtFechaProgramada)
		{
			return CRepository.PrimerCumplimiento(UidTarea, UidDepartamento, UidArea, DtFechaProgramada);
		}
	}
}