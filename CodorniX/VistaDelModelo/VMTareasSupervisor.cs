﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMTareasSupervisor
	{
		#region Atributes
		private Tarea _Tarea;
		public Tarea TAREA
		{
			get { return _Tarea; }
			set { _Tarea = value; }
		}
		private Departamento _Departamento;
		public Departamento DEPARTAMENTO
		{
			get { return _Departamento; }
			set { _Departamento = value; }
		}
		private Area _Area;
		public Area AREA
		{
			get { return _Area; }
			set { _Area = value; }
		}
		private TipoFrecuencia _TipoFrecuencia;
		public TipoFrecuencia TIPOFRECUENCIA
		{
			get { return _TipoFrecuencia; }
			set { _TipoFrecuencia = value; }
		}

		private Periodicidad _Periodicidad;
		public Periodicidad PERIODICIDAD
		{
			get { return _Periodicidad; }
			set { _Periodicidad = value; }
		}

		private Antecesor _Antecesor;
		public Antecesor ANTECESOR
		{
			get { return _Antecesor; }
			set { _Antecesor = value; }
		}

		private Notificacion _Notificacion;
		public Notificacion NOTIFICACION
		{
			get { return _Notificacion; }
			set { _Notificacion = value; }
		}

		private PeriodicidadSemanal _PeriodicidadSemanal;
		public PeriodicidadSemanal PSemanal
		{
			get { return _PeriodicidadSemanal; }
			set { _PeriodicidadSemanal = value; }
		}
		private PeriodicidadMensual _PeriodicidadMensual;
		public PeriodicidadMensual PMensual
		{
			get { return _PeriodicidadMensual; }
			set { _PeriodicidadMensual = value; }
		}
		private PeriodicidadAnual _PeriodicidadAnual;
		public PeriodicidadAnual PAnual
		{
			get { return _PeriodicidadAnual; }
			set { _PeriodicidadAnual = value; }
		}


		/// <summary>
		/// Repositorios
		/// </summary>
		private Area.Repository AreaRepositorio = new Area.Repository();
		private AreaTarea.Repositorio AreaTareaRepositorio = new AreaTarea.Repositorio();
		private Tarea.Repositorio TareaRepositorio = new Tarea.Repositorio();
		private Departamento.Repository DeptoRepositorio = new Departamento.Repository();
		private DepartamentoTarea.Repositorio DeptoTareaRepositorio = new DepartamentoTarea.Repositorio();
		private UnidadMedida.Repositorio UMRepositorio = new UnidadMedida.Repositorio();
		private Medicion.Repositorio MedRepositorio = new Medicion.Repositorio();
		private TipoTarea.Repositorio TipoTRepositorio = new TipoTarea.Repositorio();
		private Status.Repository StatusRepositorio = new Status.Repository();
		private Dias.Repositorio DiasRepositorio = new Dias.Repositorio();
		private Meses.Repositorio MesesRepositorio = new Meses.Repositorio();
		private Ordinal.Repositorio OrdinalRepositorio = new Ordinal.Repositorio();
		private TipoFrecuencia.Repositorio TipoFrecRepositorio = new TipoFrecuencia.Repositorio();
		private TareaOpcion.Repositorio TOpcionRepositorio = new TareaOpcion.Repositorio();

		private Periodicidad.Repositorio PeriodicidadRepositorio = new Periodicidad.Repositorio();
		private PeriodicidadSemanal.Repositorio PSemanalRepositorio = new PeriodicidadSemanal.Repositorio();
		private PeriodicidadMensual.Repositorio PMensualRepositorio = new PeriodicidadMensual.Repositorio();
		private PeriodicidadAnual.Repositorio PAnualRepositorio = new PeriodicidadAnual.Repositorio();
		private FechaPeriodicidad.Repository FPeriodicidadRepositorio = new FechaPeriodicidad.Repository();

		private Notificacion.Repository NotificacionRepositorio = new Notificacion.Repository();
		private Antecesor.Repository AntecesorRepositorio = new Antecesor.Repository();

		private Cumplimiento.Repository CRepository = new Cumplimiento.Repository();

		/// <summary>
		/// Listas
		/// </summary>
		public List<Tarea> LsTareas
		{
			get;
			set;
		}
		public List<UnidadMedida> LsUnidadMedida
		{
			get;
			set;
		}
		public List<Departamento> LsFiltrosDepartamento
		{
			get;
			set;
		}
		public List<Departamento> LsDepartamentosAsignados
		{
			get;
			set;
		}
		public List<Area> LsAreas
		{
			get;
			set;
		}
		public List<Area> LsAreasAsignadas
		{
			get;
			set;
		}
		public List<Medicion> LsTipoMedicionTarea
		{
			get;
			set;
		}
		public List<Tarea> LsAntecesores
		{
			get;
			set;
		}
		public List<TipoTarea> LsTiposTarea
		{
			get;
			set;
		}
		public List<Status> LsEstadosTarea
		{
			get;
			set;
		}
		public List<TareaOpcion> LsOpcionesTarea
		{
			get;
			set;
		}
		/// <summary>
		/// Periodicidad
		/// </summary>
		public List<Dias> LsDias
		{
			get;
			set;
		}
		public List<Meses> LsMeses
		{
			get;
			set;
		}
		public List<Ordinal> LsOrdinales
		{
			get;
			set;
		}
		public List<FechaPeriodicidad> LsFechasPeriodicidad
		{
			get;
			set;
		}
		#endregion

		#region Constructor
		public VMTareasSupervisor()
		{

		}
		#endregion

		#region Methods
		public void BusquedaTareas(int FolioTarea, Guid UidSucursal, string UidDepartamento, string NombreTarea, DateTime? DtFechaInicio, DateTime? DtFechaFin, int FolioAntecesor)
		{
			this.LsTareas = TareaRepositorio.BuscarTarea(FolioTarea, NombreTarea, DtFechaInicio, DtFechaFin, UidSucursal, UidDepartamento, string.Empty, string.Empty, FolioAntecesor);
		}
		public void ObtenerUnidadesMedida(Guid UidEmpresa)
		{
			this.LsUnidadMedida = new List<UnidadMedida>();
			this.LsUnidadMedida = UMRepositorio.Busqueda(UidEmpresa, string.Empty, true);
		}
		public void ObtenerDepartamentos(List<Guid> LsDepartamentos)
		{
			string Uids = string.Empty;
			foreach (Guid item in LsDepartamentos)
			{
				if (Uids == string.Empty)
					Uids = item.ToString();
				else
					Uids += "," + item.ToString();
			}
			this.LsFiltrosDepartamento = DeptoRepositorio.EncontrarPorLista(Uids);
		}
		public void ObtenerDepartamento(Guid Uid)
		{
			this.DEPARTAMENTO = DeptoRepositorio.Encontrar(Uid);
		}
		public void ObtenerAreasDepartamento(Guid Uid)
		{
			LsAreas = AreaRepositorio.FindAll(Uid);
		}
		public void ObtenerArea(Guid UidArea)
		{
			this.AREA = AreaRepositorio.Find(UidArea);
		}
		public void ObtenerTipoMedicion()
		{
			this.LsTipoMedicionTarea = new List<Medicion>();
			this.LsTipoMedicionTarea = MedRepositorio.ConsultarMedicion();
		}
		public void ObtenerEstatusTarea()
		{
			this.LsEstadosTarea = StatusRepositorio.FindAll();
		}
		public void ObtenerTipoTarea()
		{
			LsTiposTarea = TipoTRepositorio.ConsultarTipoTarea();
		}

		public void ObtenerDias()
		{
			this.LsDias = DiasRepositorio.ConsultarDias();
		}
		public void ObtenerMeses()
		{
			this.LsMeses = MesesRepositorio.ConsultarMeses();
		}
		public void ObtenerOrdinales()
		{
			this.LsOrdinales = OrdinalRepositorio.ConsultarOrdinal();
		}
		public void BuscarAntecesor(Guid UidSucursal, List<Guid> LsDepartamentos, string Nombre)
		{
			string IdsDepartamentos = string.Empty;
			if (LsDepartamentos.Count > 0)
			{
				foreach (Guid item in LsDepartamentos)
				{
					if (IdsDepartamentos == string.Empty)
						IdsDepartamentos = item.ToString();
					else
						IdsDepartamentos += "," + item.ToString();
				}
			}
			LsAntecesores = TareaRepositorio.BuscarTarea(0, Nombre, null, null, UidSucursal, IdsDepartamentos, string.Empty, string.Empty, 0);
		}
		public void ObtenerDatosAntecesor(Guid UidTarea)
		{
			this.TAREA = TareaRepositorio.Encontrar(UidTarea);
		}

		public void ObtenerTarea(Guid UidTarea)
		{
			TAREA = TareaRepositorio.Encontrar(UidTarea);
		}
		public void ObtenerPeriodicidad(Guid UidPeriodicidad)
		{
			this.PERIODICIDAD = PeriodicidadRepositorio.ConsultarPeriodicidad(UidPeriodicidad);
		}
		public void ObtenerFrecuenciaPeriodicidad(Guid UidFrecuencia)
		{
			TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
			{
				UidTipoFrecuencia = UidFrecuencia
			};
			this.TIPOFRECUENCIA = TipoFrecRepositorio.Buscar(criterio);
		}
		public void ObtenerDepartamentosAsignadosTarea(Guid UidTarea)
		{
			this.LsDepartamentosAsignados = DeptoTareaRepositorio.FindAll(UidTarea);
		}
		public void ObtenerAreasAsignadasTarea(Guid UidTarea)
		{
			this.LsAreasAsignadas = AreaTareaRepositorio.FindAll(UidTarea);
		}
		public void ObtenerPeriodicidadSemanal(Guid UidPeriodicidad)
		{
			_PeriodicidadSemanal = PSemanalRepositorio.ConsultarPeriodicidadSemanal(UidPeriodicidad);
		}
		public void ObtenerPeriodicidadMensual(Guid UidPeriodicidad)
		{
			_PeriodicidadMensual = PMensualRepositorio.ConsultarPeriodicidadMensual(UidPeriodicidad);
		}
		public void ObtenerPeriodicidadAnual(Guid UidPeriodicidad)
		{
			_PeriodicidadAnual = PAnualRepositorio.ConsultarPeriodicidadAnual(UidPeriodicidad);
		}
		public void ObtenerFechasPerodicidad(Guid UidPeriodicidad)
		{
			this.LsFechasPeriodicidad = FPeriodicidadRepositorio.GetAll(UidPeriodicidad, "Anual");
		}
		public void ObtenerNotificacionTarea(Guid UidTarea)
		{
			this.NOTIFICACION = NotificacionRepositorio.Get(UidTarea);
		}
		public void ObtenerAntecesorTarea(Guid UidTarea)
		{
			this.ANTECESOR = AntecesorRepositorio.Get(UidTarea);
		}

		public bool GuardarTarea(string Nombre, string Descripcion, Guid UidAntecesor, Guid UidUnidadMedida, Guid UidPeriodicidad, Guid UidMedicion, string Hora, int Tolerancia, Guid UidTipoTarea, Guid UidEstadoTarea, bool foto, bool Caducado, Guid UidSucursal)
		{
			TAREA = new Tarea()
			{
				StrNombre = Nombre,
				StrDescripcion = Descripcion,
				UidAntecesor = UidAntecesor == Guid.Empty ? (Guid?)null : UidAntecesor,
				UidUnidadMedida = UidUnidadMedida == Guid.Empty ? (Guid?)null : UidUnidadMedida,
				UidPeriodicidad = UidPeriodicidad,
				UidMedicion = UidMedicion,
				TmHora = Hora.Length == 0 ? (TimeSpan?)null : TimeSpan.Parse(Hora),
				IntTolerancia = Tolerancia == -1 ? (int?)null : Tolerancia,
				UidTipoTarea = UidTipoTarea,
				UidStatus = UidEstadoTarea,
				BlFoto = foto,
				BlCaducado = Caducado,
				BlAutorizado = false,
				BlCreadoSupervisor = true
			};

			try
			{
				return TAREA.GuardarDatos(UidSucursal);
			}
			catch (Exception)
			{
				throw;
			}
		}
		public bool GuardarPeriodicidadSemanal(Guid UidPeriodicidad, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
		{
			PSemanal = new PeriodicidadSemanal()
			{
				UidPeriodicidad = UidPeriodicidad,
				BlLunes = BlLunes,
				BlMartes = BlMartes,
				BlMiercoles = BlMiercoles,
				BlJueves = BlJueves,
				BlViernes = BlViernes,
				BlSabado = BlSabado,
				BlDomingo = BlDomingo
			};
			bool Result = false;
			try
			{
				Result = PSemanal.GuardarDatos();
			}
			catch (Exception)
			{
				throw;
			}
			return Result;
		}
		public bool GuardarPeriodicidadMensual(Guid UidPeriodicidad, int DiasMes, int DiasSemanas,string StrDiasMes, string StrTipo)
		{
			PMensual = new PeriodicidadMensual()
			{
				UidPeriodicidad = UidPeriodicidad,
				IntDiasMes = DiasMes,
				IntDiasSemana = DiasSemanas,
				Tipo = StrTipo,
				DiasMes = StrDiasMes
			};

			bool Result = false;
			try
			{
				Result = PMensual.GuardarDatos();
			}
			catch (Exception)
			{
				throw;
			}
			return Result;
		}
		public bool GuardarPeriodicidadAnual(Guid UidPeriodicidad, int DiasMes, int DiasSemanas, int Numero, string StrTipo)
		{
			PAnual = new PeriodicidadAnual()
			{
				UidPeriodicidad = UidPeriodicidad,
				IntDiasMes = DiasMes,
				IntDiasSemanas = DiasSemanas,
				IntNumero = Numero,
				Tipo = StrTipo
			};

			bool Result = false;
			try
			{
				Result = PAnual.GuardarDatos();
			}
			catch (Exception)
			{
				throw;
			}
			return Result;
		}
		public void GuardarFechaPeriodicidad(Guid UidPeriodicidad, int IntNumeral, int IntDia, int IntMes, string Tipo)
		{
			FPeriodicidadRepositorio.Add(UidPeriodicidad, IntNumeral, IntDia, IntMes, Tipo);
		}
		public void GuardarDepartamentos(List<Modelo.Departamento> LsDepartamentos, Guid UidTarea)
		{
			foreach (Departamento item in LsDepartamentos)
			{
				DepartamentoTarea DTarea = new DepartamentoTarea();
				DTarea.UidTarea = UidTarea;
				DTarea.UidDepartamento = item.UidDepartamento;
				DeptoTareaRepositorio.Save(DTarea);
			}
		}
		public void GuardarAreas(List<Modelo.Area> LsAreas, Guid UidTarea)
		{
			foreach (Area item in LsAreas)
			{
				AreaTarea ATarea = new AreaTarea();
				ATarea.UidTarea = UidTarea;
				ATarea.UidArea = item.UidArea;
				AreaTareaRepositorio.Save(ATarea);
			}
		}
		public void GuardarOpcionesTarea(List<TareaOpcion> LsOpciones, Guid UidTarea)
		{
			foreach (TareaOpcion item in LsOpciones)
			{
				item.UidTarea = UidTarea;
				TOpcionRepositorio.Guardar(item);
			}
		}
		public void ObtenerOpcionesTarea(Guid UidOpcionTarea)
		{
			this.LsOpcionesTarea = TOpcionRepositorio.Buscar(UidOpcionTarea);
		}
		public void GuardarNotificacion(Notificacion notificacion)
		{
			NotificacionRepositorio.Set(notificacion);
		}
		public void DeshabilitarNotificacion(Guid UidTarea)
		{
			NotificacionRepositorio.Disable(UidTarea);
		}
		public void GuardaAntecesor(Antecesor antecesor)
		{
			AntecesorRepositorio.Set(antecesor);
		}

		///<summary>
		///Periodicidad
		///</summary>
		public void ObtenerTipoFrecuencia(string Frecuencia)
		{
			TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
			{
				TipoFrecuencia = Frecuencia
			};
			this.TIPOFRECUENCIA = TipoFrecRepositorio.Buscar(criterio);
		}
		public bool GuardarPeriodicidad(int Frecuencia, Guid UidTipoFrecuencia, DateTime DtFechaInicio, DateTime? DtFechaFin)
		{
			this.PERIODICIDAD = new Periodicidad()
			{
				IntFrecuencia = Frecuencia,
				UidTipoFrecuencia = UidTipoFrecuencia,
				DtFechaInicio = DtFechaInicio,
				DtFechaFin = DtFechaFin
			};
			bool Resultado = false;
			try
			{
				Resultado = PERIODICIDAD.GuardarDatos();
			}
			catch (Exception)
			{

				throw;
			}
			return Resultado;
		}

		public DateTime CalcularCumplimientoPeriodicidadSemanal(DateTime DtInicio, int Frecuencia, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
		{
			DateTime DtNewDate = DateTime.Today;
			DtNewDate = PSemanalRepositorio.CalculateNext(DtInicio, true, Frecuencia, BlLunes, BlMartes, BlMiercoles, BlJueves, BlViernes, BlSabado, BlDomingo);
			return DtNewDate;
		}
		public DateTime CalcularCumplimientoPeriodicidadMensual(string Tipo, DateTime DtInicio,List<FechaPeriodicidad> LsFechas, string DiasMes, int IntOrdinal, int DiaSemana, int Frecuencia)
		{
			DateTime DtNewDate = DateTime.Today;
			if (Tipo.Equals("A"))
				DtNewDate = PMensualRepositorio.CalculateNextTypeA(DtInicio, true, DiasMes, Frecuencia);
			else if (Tipo.Equals("B"))
				DtNewDate = PMensualRepositorio.CalculateNextTypeB(DtInicio, true, LsFechas, Frecuencia);
			else if (Tipo.Equals("C"))
				DtNewDate = PMensualRepositorio.CalculateNextTypeC(DtInicio, true, IntOrdinal, DiaSemana, Frecuencia);
			return DtNewDate;
		}
		public DateTime CalcularCumplimientoPeriodicidadAnual(string Tipo, DateTime DtInicio, List<DateTime> LsDates, int IntMes, int IntOrdinal, int DiaSemana, int Frecuencia)
		{
			DateTime DtNewDate = DateTime.Today;
			if (Tipo.Equals("A"))
				DtNewDate = PAnualRepositorio.CalculateNextTypeA(DtInicio, true, LsDates, Frecuencia);
			else
				DtNewDate = PAnualRepositorio.CalculateNextTypeB(DtInicio, true, IntOrdinal, DiaSemana, IntMes, Frecuencia);
			return DtNewDate;
		}

		public bool GuardarPrimerCumplimiento(Guid UidTarea, Guid UidDepartamento, Guid UidArea, DateTime DtFechaProgramada)
		{
			return CRepository.PrimerCumplimiento(UidTarea, UidDepartamento, UidArea, DtFechaProgramada);
		}
		#endregion
	}
}