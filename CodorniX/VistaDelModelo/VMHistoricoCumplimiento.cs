﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
	public class VMHistoricoCumplimiento
	{
		private Usuario.Repository UsuarioRepository = new Usuario.Repository();
		private Tarea.Repositorio tareaRepository = new Tarea.Repositorio();
		private Cumplimiento.Repository cumplimientoRepository = new Cumplimiento.Repository();
		private Departamento.Repository departamentoRepository = new Departamento.Repository();
		private Area.Repository areaRepository = new Area.Repository();
		private TareaOpcion.Repositorio opcionRepository = new TareaOpcion.Repositorio();
		private TipoTarea.Repositorio tipoRepository = new TipoTarea.Repositorio();
		private Periodo.Repository periodoRepository = new Periodo.Repository();
		private EstadoCumplimiento.Repository estadoRepository = new EstadoCumplimiento.Repository();
		private Periodicidad.Repositorio periodicidadRepository = new Periodicidad.Repositorio();
		private TipoFrecuencia.Repositorio frecuenciaRepository = new TipoFrecuencia.Repositorio();

		private List<Cumplimiento> _CumplimientosPendientes;
		public List<Cumplimiento> CumplimientosPendientes
		{
			get { return _CumplimientosPendientes; }
			set { _CumplimientosPendientes = value; }
		}

		private List<Cumplimiento> _ltsDepartamentos;
		public List<Cumplimiento> ltsDepartamentos
		{
			get { return _ltsDepartamentos; }
			set { _ltsDepartamentos = value; }
		}

		private List<TipoTarea> _tiposTarea;
		public List<TipoTarea> TiposTarea
		{
			get { return _tiposTarea; }
			set { _tiposTarea = value; }
		}

		private Tarea _Tarea;
		public Tarea Tarea
		{
			get { return _Tarea; }
			set { _Tarea = value; }
		}

		private Departamento _Departamento;
		public Departamento Departamento
		{
			get { return _Departamento; }
			set { _Departamento = value; }
		}

		private List<Departamento> _departamentos;
		public List<Departamento> Departamentos
		{
			get { return _departamentos; }
			set { _departamentos = value; }
		}

		private Area _Area;
		public Area Area
		{
			get { return _Area; }
			set { _Area = value; }
		}

		private List<Area> _areas;
		public List<Area> Areas
		{
			get { return _areas; }
			set { _areas = value; }
		}

		private List<TareaOpcion> _Opciones;
		public List<TareaOpcion> Opciones
		{
			get { return _Opciones; }
			set { _Opciones = value; }
		}

		private Cumplimiento _Cumplimiento;
		public Cumplimiento Cumplimiento
		{
			get { return _Cumplimiento; }
			set { _Cumplimiento = value; }
		}

		private Cumplimiento _CCumplimiento;
		public Cumplimiento CCumplimiento
		{
			get { return _CCumplimiento; }
			set { _CCumplimiento = value; }
		}

		private List<EstadoCumplimiento> _Estados;
		public List<EstadoCumplimiento> Estados
		{
			get { return _Estados; }
			set { _Estados = value; }
		}

		private Periodo _Periodo;
		public Periodo Periodo
		{
			get { return _Periodo; }
			set { _Periodo = value; }
		}

		private Periodicidad _periodicidad;
		public Periodicidad Periodicidad
		{
			get { return _periodicidad; }
			set { _periodicidad = value; }
		}

		private TipoFrecuencia _tipoFrecuencia;
		public TipoFrecuencia TipoFrecuencia
		{
			get { return _tipoFrecuencia; }
			set { _tipoFrecuencia = value; }
		}

		private List<ParametroOrdenamiento> _LsOrdenamiento;
		public List<ParametroOrdenamiento> LsOrdenamiento
		{
			get { return _LsOrdenamiento; }
			set { _LsOrdenamiento = value; }
		}

		public List<Usuario> LsEncargados
		{
			get;
			set;
		}

		public VMHistoricoCumplimiento()
		{
		}

		public void Busqueda(Guid UidSucursal, Guid UidUsuario, Guid UidDepartamento, Guid UidArea, Guid UidTipoTarea, int? FolioTarea, int? FolioCumplimiento, int? FolioTurno, string NombreTarea, string UidsEstados, DateTime? DtFechaInicio, DateTime? DtFechaFin, bool ExpFolioTurno, string DirFolioTurno, bool ExpFolioCumplimiento, string DirFolioCumplimiento, bool ExpFolioTarea, string DirFolioTarea, bool ExpTarea, string DirTarea, bool ExpDepartamento, string DirDepartamento, bool ExpArea, string DirArea, bool ExpHora, string DirHora, bool ExpEstado, string DirEstado, int PosFolioTurno, int PosFolioCumplimiento, int PosFolioTarea, int PosTarea, int PosDepartamento, int PosArea, int PosHora, int PosEstado)
		{
			_CumplimientosPendientes = cumplimientoRepository.BusquedaHistorico(UidSucursal, UidUsuario, UidDepartamento, UidArea, UidTipoTarea, FolioTarea, FolioCumplimiento, FolioTurno, NombreTarea, UidsEstados, DtFechaInicio, DtFechaFin, ExpFolioTurno, DirFolioTurno, ExpFolioCumplimiento, DirFolioCumplimiento, ExpFolioTarea, DirFolioTarea, ExpTarea, DirTarea, ExpDepartamento, DirDepartamento, ExpArea, DirArea, ExpHora, DirHora, ExpEstado, DirEstado, PosFolioTurno, PosFolioCumplimiento, PosFolioTarea, PosTarea, PosDepartamento, PosArea, PosHora, PosEstado);
		}

		public void ObtenerTareas(Guid? uidUsuario, Guid uidPeriodo, List<Guid> periodos, string nombre, string estados, Guid departamento, Guid area, Guid tipo, DateTime fechaInicio, DateTime fechaFin)
		{
			string lpr = null;
			if (periodos.Count > 0)
			{
				lpr = periodos[0].ToString();
				for (int i = 1; i < periodos.Count; i++)
				{
					lpr += "," + periodos[i].ToString();
				}
			}
			_CumplimientosPendientes = cumplimientoRepository.Search(uidUsuario, uidPeriodo, fechaInicio, fechaFin, lpr, nombre, estados, departamento, area, tipo);
		}

		public void ObtenerTarea(Guid uid)
		{
			_Tarea = tareaRepository.Encontrar(uid);
		}

		public void ObtenerTareaCumplimiento(Guid uid, Guid UidUsuario)
		{
			_CCumplimiento = cumplimientoRepository.ObtenerTareaCumplimiento(uid, UidUsuario);
		}

		public void ObtenerDepartamento(Guid uid)
		{
			_Departamento = departamentoRepository.Encontrar(uid);
		}

		public void ObtenerDepartamentosSucursal(Guid UidSucursal)
		{
			this.Departamentos = departamentoRepository.EncontrarTodos(UidSucursal);
			this.Departamentos.Insert(0, new Departamento() { UidDepartamento = Guid.Empty, StrNombre = "Todos" });
		}

		public void ObtenerEncargadosSucursal(Guid UidSucursal)
		{
			this.LsEncargados = UsuarioRepository.GetAllSucursal(UidSucursal);
			this.LsEncargados.Insert(0, new Usuario() { UIDUSUARIO = Guid.Empty, STRNOMBRE = "Todos" });
		}

		public void ObtenerDepartamentos(List<Guid> periodos)
		{
			string lpr = null;
			if (periodos.Count > 0)
			{
				lpr = periodos[0].ToString();
				for (int i = 1; i < periodos.Count; i++)
				{
					lpr += "," + periodos[i];
				}
			}
			_departamentos = departamentoRepository.EncontrarPorListaDePeriodos(lpr);
		}

		public void ObtenerArea(Guid uid)
		{
			_Area = areaRepository.Find(uid);
		}

		public void ObtenerAreas(Guid uid)
		{
			_areas = areaRepository.FindAll(uid);
		}

		public void ObtenerOpcionesDeTarea(Guid uidTarea)
		{
			_Opciones = opcionRepository.Buscar(uidTarea);
		}

		public void ObtenerCumplimiento(Guid uid)
		{
			_Cumplimiento = cumplimientoRepository.Find(uid);
		}

		public void ObtenerPeriodo(Guid uid)
		{
			_Periodo = periodoRepository.Find(uid);
		}
		public void Buscar(string Fecha, string Fecha2, Guid uidusurio)
		{
			_ltsDepartamentos = cumplimientoRepository.Buscar(Fecha, Fecha2, uidusurio);
		}

		public DateTime? ObtenerFechaSiguienteTarea(Guid uidTarea, DateTime fecha)
		{
			return cumplimientoRepository.ObtenerSiguienteFecha(uidTarea, fecha);
		}

		public void DeshacerCumplimiento(Guid uidCumplimiento)
		{
			cumplimientoRepository.Deshacer(uidCumplimiento);
		}

		public void ObtenerEstados()
		{
			_Estados = estadoRepository.FindAll();
		}

		public void ObtenerTiposTarea()
		{
			_tiposTarea = tipoRepository.ConsultarTipoTarea();
		}

		public void ObtenerPeriocidad(Guid uid)
		{
			_periodicidad = periodicidadRepository.ConsultarPeriodicidad(uid);
		}

		public void ObtenerTipoFrecuencia(Guid uid)
		{
			TipoFrecuencia.Criterio criterio = new TipoFrecuencia.Criterio()
			{
				UidTipoFrecuencia = uid
			};

			_tipoFrecuencia = frecuenciaRepository.Buscar(criterio);
		}

		public void ObtenerDepartamentoPeriodo(Guid UidPeriodo)
		{
			string IdPeriodo = UidPeriodo.ToString();
			_departamentos = departamentoRepository.EncontrarPorListaDePeriodos(IdPeriodo);
		}

		public void CargarParametrosOrdenamiento()
		{
			this.LsOrdenamiento = new List<ParametroOrdenamiento>();
			LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Folio de Turno",
				Direccion = "ASC",
				BlOrdenar = true,
				Posicion = 1
			});
			LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Folio de Tarea",
				Direccion = "ASC",
				BlOrdenar = false,
				Posicion = 2
			});

			LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Folio de Cumplimiento",
				Direccion = "ASC",
				BlOrdenar = false,
				Posicion = 3
			});
			LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Tarea",
				Direccion = "ASC",
				BlOrdenar = false,
				Posicion = 4
			});
			LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Departamento",
				Direccion = "ASC",
				BlOrdenar = false,
				Posicion = 5
			});
			LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Area",
				Direccion = "ASC",
				BlOrdenar = false,
				Posicion = 6
			});
			LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Hora",
				Direccion = "ASC",
				BlOrdenar = false,
				Posicion = 7
			}); LsOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Estado",
				Direccion = "ASC",
				BlOrdenar = false,
				Posicion = 8
			});
		}
	}
}