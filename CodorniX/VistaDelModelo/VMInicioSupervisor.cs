﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
	public class VMInicioSupervisor
	{
		private TurnoSupervisor.Repository turnoRepository = new TurnoSupervisor.Repository();
		private EstadoTurno.Repository estadoRepository = new EstadoTurno.Repository();
		private Revision.Repository RevisionRepositorio = new Revision.Repository();

		public Revision REVISION { get; set; }
		public List<TurnoSupervisor> LsTurnosSupervisor { get; set; }
		public List<Revision> LsRevisiones { get; set; }
		public List<Calificacion> LsCalificaciones { get; set; }
		public List<TareaOpcion> LsOpcionesTarea { get; set; }
		public TurnoSupervisor TurnoSupervisor { get; set; }
		public EstadoTurno EstadoTurno { get; set; }

		public void ObtenerTurnoDeHoy(Guid uidUsuario, Guid uidSucursal, DateTime fecha)
		{
			this.LsTurnosSupervisor = new List<TurnoSupervisor>();

			this.TurnoSupervisor = turnoRepository.VerificarUltimoTurno(uidUsuario, uidSucursal, fecha);
			if (this.TurnoSupervisor != null)
			{
				this.LsTurnosSupervisor.Add(this.TurnoSupervisor);
			}

			TurnoSupervisor = turnoRepository.ObtenerTurnoHoy(uidUsuario, uidSucursal, fecha);
			this.LsTurnosSupervisor.Add(this.TurnoSupervisor);
		}

		public void BusquedaTurnos(Guid UidUsuario, Guid UidSucural,DateTime Fecha)
		{			
			TurnoSupervisor = turnoRepository.ObtenerTurnoHoy(UidUsuario, UidSucural, Fecha);			
		}

		public void ObtenerTurno(Guid uid)
		{
			TurnoSupervisor = turnoRepository.Find(uid);
		}

		public void CrearTurno(Guid uidUsuario, Guid uidSucursal, DateTimeOffset fechaInicio)
		{
			turnoRepository.CrearTurno(uidUsuario, uidSucursal, fechaInicio);
		}

		public void ModificarFechaFin(Guid uidTurnoSupervisor, DateTimeOffset fechafin)
		{
			turnoRepository.ModificarFechaFin(uidTurnoSupervisor, fechafin);
		}

		public void IniciarTurno(Guid uidTurnoSupervisor)
		{
			turnoRepository.ModificarEstado(uidTurnoSupervisor, "Abierto");
		}

		public void CerrarTurno(Guid uidTurnoSupervisor)
		{
			turnoRepository.ModificarEstado(uidTurnoSupervisor, "Cerrado");
		}

		public void ObtenerEstadoTurno(Guid uid)
		{
			EstadoTurno = estadoRepository.Find(uid);
		}

		public bool TurnoPendienteCerrarUsuario(Guid UidUsuario, Guid UidSucursal, DateTime Fecha)
		{
			TurnoSupervisor Turno = this.turnoRepository.VerificarUltimoTurno(UidUsuario, UidSucursal, Fecha);
			if (Turno == null)
			{
				return false;
			}
			else
			{
				return true;
			}
		}

		public void ObtenerCalificaciones(Guid UidEmpresa)
		{
			Calificacion.Repository CalificacionRepositorio = new Calificacion.Repository();
			this.LsCalificaciones = CalificacionRepositorio.Search(UidEmpresa, string.Empty, true, -1);
		}

		public void ObtenerOpcionesTarea(Guid UidTarea)
		{
			TareaOpcion.Repositorio TARepositorio = new TareaOpcion.Repositorio();
			this.LsOpcionesTarea = TARepositorio.Buscar(UidTarea);
		}

		public void ObtenerRevisionesTurno(Guid UidTurno)
		{
			this.LsRevisiones = RevisionRepositorio.FindByTurno(UidTurno);
		}

		public void ObtenerDatosRevision(Guid UidRevision)
		{
			this.REVISION = RevisionRepositorio.GetRevisionDetailById(UidRevision);
		}

		public void SortList(string SortDirection, string SortExpression)
		{
			switch (SortExpression)
			{
				case "FC":
					if (SortDirection.Equals("ASC"))
						this.LsRevisiones = LsRevisiones.OrderBy(x => x.FolioCumplimiento).ToList();
					else
						this.LsRevisiones = LsRevisiones.OrderByDescending(x => x.FolioCumplimiento).ToList();
					break;
				case "FT":
					if (SortDirection.Equals("ASC"))
						this.LsRevisiones = LsRevisiones.OrderBy(x => x.FolioTarea).ToList();
					else
						this.LsRevisiones = LsRevisiones.OrderByDescending(x => x.FolioTarea).ToList();
					break;
				case "Tarea":
					if (SortDirection.Equals("ASC"))
						this.LsRevisiones = LsRevisiones.OrderBy(x => x.StrTarea).ToList();
					else
						this.LsRevisiones = LsRevisiones.OrderByDescending(x => x.StrTarea).ToList();
					break;
				case "Depto":
					if (SortDirection.Equals("ASC"))
						this.LsRevisiones = LsRevisiones.OrderBy(x => x.StrDepartamento).ToList();
					else
						this.LsRevisiones = LsRevisiones.OrderByDescending(x => x.StrDepartamento).ToList();
					break;
				case "Area":
					if (SortDirection.Equals("ASC"))
						this.LsRevisiones = LsRevisiones.OrderBy(x => x.StrArea).ToList();
					else
						this.LsRevisiones = LsRevisiones.OrderByDescending(x => x.StrArea).ToList();
					break;
				default:
					break;
			}
		}
	}
}