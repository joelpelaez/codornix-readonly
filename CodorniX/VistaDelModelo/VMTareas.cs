﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMTareas
	{
		#region properties
		private Periodicidad.Repositorio PeriodicidadRep = new Periodicidad.Repositorio();
		private PeriodicidadSemanal.Repositorio PSemanalRep = new PeriodicidadSemanal.Repositorio();
		private PeriodicidadMensual.Repositorio PMensualRep = new PeriodicidadMensual.Repositorio();
		private PeriodicidadAnual.Repositorio PAnualRep = new PeriodicidadAnual.Repositorio();
		private FechaPeriodicidad.Repository FechaPeriodicidadRep = new FechaPeriodicidad.Repository();
		private TipoFrecuencia.Repositorio FrecuenciaRep = new TipoFrecuencia.Repositorio();
		private TareaOpcion.Repositorio TareaOpcionRep = new TareaOpcion.Repositorio();
		private Notificacion.Repository NotificacionRep = new Notificacion.Repository();
		private Tarea.Repositorio TareaRep = new Tarea.Repositorio();
		private Antecesor.Repository AntecesorRep = new Antecesor.Repository();
		private Departamento.Repository DepartamentoRep = new Departamento.Repository();
		private DepartamentoTarea.Repositorio DeptoTareaRep = new DepartamentoTarea.Repositorio();
		private AreaTarea.Repositorio AreaTareaRep = new AreaTarea.Repositorio();
		private Area.Repository AreaRep = new Area.Repository();
		private Cumplimiento.Repository CumplimientoRep = new Cumplimiento.Repository();

		// Objectos
		public Tarea tTarea
		{
			get;
			set;
		}
		public Antecesor aAntecesor
		{
			get;
			set;
		}
		public Notificacion nNotificacion
		{
			get;
			set;
		}
		public Periodicidad pPeriodicidad
		{
			get;
			set;
		}
		public PeriodicidadSemanal pSemanal
		{
			get;
			set;
		}
		public PeriodicidadMensual pMensual
		{
			get;
			set;
		}
		public PeriodicidadAnual pAnual
		{
			get;
			set;
		}
		public TipoFrecuencia fFrecuencia
		{
			get;
			set;
		}
		public Departamento dDepartamento
		{
			get;
			set;
		}
		public Area aArea
		{
			get;
			set;
		}
		public Cumplimiento cCumplimiento
		{
			get;
			set;
		}

		// Listas
		public List<Tarea> LsTareas
		{
			get;
			set;
		}
		public List<TareaOpcion> LsOpcionesTarea
		{
			get;
			set;
		}
		public List<Tarea> LsAntecesores
		{
			get;
			set;
		}
		public List<Status> LsEstatusTarea
		{
			get;
			set;
		}
		public List<TipoTarea> LsTiposTarea
		{
			get;
			set;
		}
		public List<Departamento> LsDepartamentos
		{
			get;
			set;
		}
		public List<Area> LsAreas
		{
			get;
			set;
		}
		public List<UnidadMedida> LsUnidadesMedida
		{
			get;
			set;
		}
		public List<Medicion> LsMediciones
		{
			get;
			set;
		}
		public List<Ordinal> LsOrdinales
		{
			get;
			set;
		}

		public List<Dias> LsDias
		{
			get;
			set;
		}
		public List<Meses> LsMeses
		{
			get;
			set;
		}
		/// <summary>
		/// Almacenas las fechas de las periodicidades con frecuencia mensual y anual.
		/// </summary>
		public List<FechaPeriodicidad> LsFechasPeriodicidad
		{
			get;
			set;
		}
		#endregion

		public VMTareas()
		{

		}

		#region Methods
		public void Busqueda(Guid UidSucursal, int IntFolioTarea, string NombreTarea, DateTime? DtFechaInicio, DateTime? DtFechaFin, string Departamentos, string Areas, string UnidadesMedida, int IntFolioAntecesor)
		{
			this.LsTareas = TareaRep.BuscarTarea(IntFolioTarea, NombreTarea, DtFechaInicio, DtFechaFin, UidSucursal, Departamentos, Areas, UnidadesMedida, IntFolioAntecesor);
		}
		public void GetTareaById(Guid UidTarea)
		{
			this.tTarea = TareaRep.Encontrar(UidTarea);
		}
		public int GetTotalCumplimientosTarea(Guid UidTarea)
		{
			return TareaRep.ObtenerCantidadCumplimientosTarea(UidTarea);
		}
		public void GetOpcionesTarea(Guid UidTarea)
		{
			this.LsOpcionesTarea = TareaOpcionRep.Buscar(UidTarea);
		}
		public void GetPeriodicidad(Guid UidPeriodicidad)
		{
			this.pPeriodicidad = PeriodicidadRep.ConsultarPeriodicidad(UidPeriodicidad);
		}
		public void GetFrecuenciaTarea(Guid UidTarea)
		{
			this.fFrecuencia = FrecuenciaRep.FindByTarea(UidTarea);
		}
		public void GetFrecuenciaByName(string TipoFrecuencia)
		{
			TipoFrecuencia.Criterio cCriterio = new TipoFrecuencia.Criterio()
			{
				TipoFrecuencia = TipoFrecuencia
			};
			this.fFrecuencia = FrecuenciaRep.Buscar(cCriterio);
		}
		public void GetPeriodicidadSemanal(Guid UidPeriodicidad)
		{
			this.pSemanal = PSemanalRep.ConsultarPeriodicidadSemanal(UidPeriodicidad);
		}
		public void GetPeriodicidadMesual(Guid UidPeriodicidad)
		{
			this.pMensual = PMensualRep.ConsultarPeriodicidadMensual(UidPeriodicidad);
		}
		public void GetPeriodicidadAnual(Guid UidPeriodicidad)
		{
			this.pAnual = PAnualRep.ConsultarPeriodicidadAnual(UidPeriodicidad);
		}
		public void GetFechaPeriodicidad(Guid UidPeriodicidad)
		{
			this.LsFechasPeriodicidad = FechaPeriodicidadRep.GetAll(UidPeriodicidad, "Anual");
		}
		public void GetFechaPeriodicidadMensual(Guid UidPeriodicidad,string StrTipo)
		{
			this.LsFechasPeriodicidad = FechaPeriodicidadRep.GetAll(UidPeriodicidad, StrTipo);
		}
		public void GetEstadosTarea()
		{
			Status.Repository StatusRep = new Status.Repository();
			this.LsEstatusTarea = StatusRep.FindAll();
		}
		public void GetTiposTarea()
		{
			TipoTarea.Repositorio TipoRep = new TipoTarea.Repositorio();
			this.LsTiposTarea = TipoRep.ConsultarTipoTarea();
		}
		public void GetDepartamentosAsignadosTarea(Guid UidTarea)
		{
			DepartamentoTarea.Repositorio Rep = new DepartamentoTarea.Repositorio();
			this.LsDepartamentos = Rep.FindAll(UidTarea);
		}
		public void GetDepartamentosSucursal(Guid UidSucursal)
		{
			Departamento.Repository DeptoRep = new Departamento.Repository();
			this.LsDepartamentos = DeptoRep.EncontrarTodos(UidSucursal);
		}
		public void GetDepartamentoById(Guid UidDepartamento)
		{
			this.dDepartamento = DepartamentoRep.Encontrar(UidDepartamento);
		}
		public void GetAreasAsignadasTarea(Guid UidTarea)
		{
			AreaTarea.Repositorio Rep = new AreaTarea.Repositorio();
			this.LsAreas = Rep.FindAll(UidTarea);
		}
		public void GetAreasSucursal(Guid UidSucursal)
		{
			Area.Repository AreaRep = new Area.Repository();
			this.LsAreas = AreaRep.FindAllSucursal(UidSucursal);
		}
		public void GetAreaById(Guid UidArea)
		{
			this.aArea = AreaRep.Find(UidArea);
		}
		public void GetAreasDepartamento(Guid UidDepartamento)
		{
			this.LsAreas = AreaRep.FindAll(UidDepartamento);
		}
		public void GetUnidadesMedida(Guid UidEmpresa)
		{
			UnidadMedida.Repositorio UnidadMedidaRep = new UnidadMedida.Repositorio();
			this.LsUnidadesMedida = UnidadMedidaRep.Busqueda(UidEmpresa, string.Empty, true);
		}
		public void GetAntecesores(Guid UidSucursal, string Nombre)
		{

			this.LsAntecesores = TareaRep.BuscarTarea(0, Nombre, null, null, UidSucursal, string.Empty, string.Empty, string.Empty, 0);
		}
		public void GetAntecesorTarea(Guid UidTarea)
		{
			this.aAntecesor = AntecesorRep.Get(UidTarea
);
		}
		public void GetNotificacionTarea(Guid UidTarea)
		{
			this.nNotificacion = NotificacionRep.Get(UidTarea);
		}
		public void GetUltimoCumplimientoTarea(Guid UidTarea)
		{
			Cumplimiento.Repository CumplimientoRep = new Cumplimiento.Repository();
			this.cCumplimiento = CumplimientoRep.GetUltimoCumplimientoTarea(UidTarea);
		}

		public void GetTipoMedicion()
		{
			Medicion.Repositorio MedicionRep = new Medicion.Repositorio();
			this.LsMediciones = MedicionRep.ConsultarMedicion();
		}

		public void GetDias()
		{
			Dias.Repositorio DiaRep = new Dias.Repositorio();
			this.LsDias = DiaRep.ConsultarDias();
		}
		public void GetMeses()
		{
			Meses.Repositorio MesRep = new Meses.Repositorio();
			this.LsMeses = MesRep.ConsultarMeses();
		}
		public void GetOrdinales()
		{
			Ordinal.Repositorio OrdinalRep = new Ordinal.Repositorio();
			this.LsOrdinales = OrdinalRep.ConsultarOrdinal();
		}

		public void SortOrdenamientoTareas(string StrSortDirection, string StrSortExpression)
		{
			switch (StrSortExpression)
			{
				case "Folio":
					if (StrSortDirection.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.IntFolio).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.IntFolio).ToList();
					break;
				case "Departamento":
					if (StrSortDirection.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.StrDepartamento).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.StrDepartamento).ToList();
					break;
				case "Area":
					if (StrSortDirection.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.StrArea).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.StrArea).ToList();
					break;
				case "Orden":
					if (StrSortDirection.Equals("ASC"))
						this.LsTareas = this.LsTareas.OrderBy(t => t.IntOrden).ToList();
					else
						this.LsTareas = this.LsTareas.OrderByDescending(t => t.IntOrden).ToList();
					break;
				default:
					break;
			}
		}

		public bool SetOrdenTarea(Guid UidTarea, int Orden)
		{
			return TareaRep.EstablecerOrdenTarea(UidTarea, Orden);
		}
		public bool SaveTarea(string Nombre, string Descripcion, Guid UidAntecesor, Guid UidUnidadMedida, Guid UidPeriodicidad, Guid UidMedicion, string Hora, int Tolerancia, Guid UidTipoTarea, Guid UidEstadoTarea, bool foto, bool Caducado, Guid UidSucursal)
		{
			tTarea = new Tarea()
			{
				StrNombre = Nombre,
				StrDescripcion = Descripcion,
				UidAntecesor = UidAntecesor == Guid.Empty ? (Guid?)null : UidAntecesor,
				UidUnidadMedida = UidUnidadMedida == Guid.Empty ? (Guid?)null : UidUnidadMedida,
				UidPeriodicidad = UidPeriodicidad,
				UidMedicion = UidMedicion,
				TmHora = Hora.Length == 0 ? (TimeSpan?)null : TimeSpan.Parse(Hora),
				IntTolerancia = Tolerancia == -1 ? (int?)null : Tolerancia,
				UidTipoTarea = UidTipoTarea,
				UidStatus = UidEstadoTarea,
				BlFoto = foto,
				BlCaducado = Caducado,
				BlAutorizado = true,
				BlCreadoSupervisor = false
			};

			try
			{
				return tTarea.GuardarDatos(UidSucursal);
			}
			catch (Exception)
			{
				throw;
			}
		}
		public void SaveOpcionesTarea(List<TareaOpcion> LsOpciones, Guid UidTarea)
		{
			foreach (TareaOpcion item in LsOpciones)
			{
				item.UidTarea = UidTarea;
				TareaOpcionRep.Guardar(item);
			}
		}
		public bool SavePeriodicidad(int Frecuencia, Guid UidTipoFrecuencia, DateTime DtFechaInicio, DateTime? DtFechaFin)
		{
			this.pPeriodicidad = new Periodicidad()
			{
				IntFrecuencia = Frecuencia,
				UidTipoFrecuencia = UidTipoFrecuencia,
				DtFechaInicio = DtFechaInicio,
				DtFechaFin = DtFechaFin
			};
			bool Resultado = false;
			try
			{
				Resultado = pPeriodicidad.GuardarDatos();
			}
			catch (Exception ex)
			{
				throw;
			}
			return Resultado;
		}
		public bool SavePeriodicidadSemanal(Guid UidPeriodicidad, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
		{
			pSemanal = new PeriodicidadSemanal()
			{
				UidPeriodicidad = UidPeriodicidad,
				BlLunes = BlLunes,
				BlMartes = BlMartes,
				BlMiercoles = BlMiercoles,
				BlJueves = BlJueves,
				BlViernes = BlViernes,
				BlSabado = BlSabado,
				BlDomingo = BlDomingo
			};
			bool Result = false;
			try
			{
				Result = pSemanal.GuardarDatos();
			}
			catch (Exception ex)
			{
				throw;
			}
			return Result;
		}
		public bool SavePeriodicidadMensual(Guid UidPeriodicidad, int DiasMes, int DiasSemanas, string StrTipo, string StrDiasMes)
		{
			pMensual = new PeriodicidadMensual()
			{
				UidPeriodicidad = UidPeriodicidad,
				IntDiasMes = DiasMes,
				IntDiasSemana = DiasSemanas,
				Tipo = StrTipo,
				DiasMes = StrDiasMes
			};

			bool Result = false;
			try
			{
				Result = pMensual.GuardarDatos();
			}
			catch (Exception ex)
			{
				throw;
			}
			return Result;
		}
		public bool SavePeriodicidadAnual(Guid UidPeriodicidad, int DiasMes, int DiasSemanas, int Numero, string StrTipo)
		{
			pAnual = new PeriodicidadAnual()
			{
				UidPeriodicidad = UidPeriodicidad,
				IntDiasMes = DiasMes,
				IntDiasSemanas = DiasSemanas,
				IntNumero = Numero,
				Tipo = StrTipo
			};

			bool Result = false;
			try
			{
				Result = pAnual.GuardarDatos();
			}
			catch (Exception ex)
			{
				throw;
			}
			return Result;
		}
		public bool SaveFechaPeriodicidad(Guid UidPeriodicidad, int IntOrdinal, int IntDia, int IntMes, string StrTipo)
		{
			return FechaPeriodicidadRep.Add(UidPeriodicidad, IntOrdinal, IntDia, IntMes, StrTipo);
		}
		public void SaveDepartamentos(List<Departamento> LsDepartamentos, Guid UidTarea)
		{
			foreach (Departamento item in LsDepartamentos)
			{
				DepartamentoTarea DTarea = new DepartamentoTarea();
				DTarea.UidTarea = UidTarea;
				DTarea.UidDepartamento = item.UidDepartamento;
				DeptoTareaRep.Save(DTarea);
			}
		}
		public void SaveAreas(List<Area> LsAreas, Guid UidTarea)
		{
			foreach (Area item in LsAreas)
			{
				AreaTarea ATarea = new AreaTarea();
				ATarea.UidTarea = UidTarea;
				ATarea.UidArea = item.UidArea;
				AreaTareaRep.Save(ATarea);
			}
		}
		public void SaveNotificacion(Notificacion nNotificacion)
		{
			NotificacionRep.Set(nNotificacion);
		}
		public void SaveAntecesor(Antecesor aAntecesor)
		{
			AntecesorRep.Set(aAntecesor);
		}
		/// <summary>
		/// Guardar primero cumplimiento de la tarea
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <param name="UidDepartamento"></param>
		/// <param name="UidArea"></param>
		/// <param name="DtFechaProgramada"></param>
		/// <returns></returns>
		public bool GuardarPrimerCumplimiento(Guid UidTarea, Guid UidDepartamento, Guid UidArea, DateTime DtFechaProgramada)
		{
			return CumplimientoRep.PrimerCumplimiento(UidTarea, UidDepartamento, UidArea, DtFechaProgramada);
		}
		public bool HabilitarTareaCreadaSupervisor(Guid UidTarea, Guid UidSucursal)
		{
			return TareaRep.HabilitarTareaSupervisor(UidTarea, UidSucursal);
		}


		public bool UpdateTarea(Guid UidTarea, string Nombre, string Descripcion, int? Tolerancia, TimeSpan? Hora, Guid UidTipoTarea, Guid UidEstatus, bool BlFolo, DateTime? DtFechaInicio, Guid UidUnidadMedida)
		{
			Tarea uTarea = new Tarea()
			{
				UidTarea = UidTarea,
				StrNombre = Nombre,
				StrDescripcion = Descripcion,
				IntTolerancia = Tolerancia == null ? (int?)null : Convert.ToInt32(Tolerancia),
				TmHora = Hora == null ? (TimeSpan?)null : Hora,
				UidTipoTarea = UidTipoTarea,
				UidStatus = UidEstatus,
				BlFoto = BlFolo,
				DtNuevaFecha = DtFechaInicio,
				UidUnidadMedida = UidUnidadMedida
			};
			return uTarea.ModificarDatos();
		}
		public bool UpdatePeriodicidad(Guid UidPeriodicidad, DateTime? DtFechaFin)
		{
			pPeriodicidad = new Periodicidad()
			{
				UidPeriodicidad = UidPeriodicidad,
				DtFechaFin = DtFechaFin
			};
			return pPeriodicidad.ModificarDatos();
		}
		public bool UpdateIdentificadorFrecuenciaPeriodicidad(Guid UidPeriodicidad, Guid UidTipoFrecuencia)
		{
			return PeriodicidadRep.ActualizarUidFrecuencia(UidPeriodicidad, UidTipoFrecuencia);
		}
		public bool UpdateFrecuenciaPeriodicidad(Guid UidPeriodicidad, int Frecuencia)
		{
			return PeriodicidadRep.ActualizarFrecuencia(UidPeriodicidad, Frecuencia);
		}
		public bool UpdatePeriodicidadSemanal(Guid UidPeriodicidad, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
		{
			return PSemanalRep.Actualizar(UidPeriodicidad, BlLunes, BlMartes, BlMiercoles, BlJueves, BlViernes, BlSabado, BlDomingo);
		}
		public bool UpdatePeriodicidadMensual(Guid UidPeriodicidad, int DiaMes, int DiaSemana, string StrTipo, string DiasMes)
		{
			return PMensualRep.Actualizar(UidPeriodicidad, DiaMes, DiaSemana, StrTipo, DiasMes);
		}
		public bool UpdatePeriodicidadAnual(Guid UidPeriodicidad, int DiaMes, int DiaSemana, int Numero, string StrTipo)
		{
			return PAnualRep.Actualizar(UidPeriodicidad, DiaMes, DiaSemana, Numero, StrTipo);
		}
		public void UpdateAsignacionDepartamentos(Guid UidTarea, List<Departamento> LsDepartamentos, DateTime DtProximoCumplimiento)
		{
			DepartamentoTarea DT = new DepartamentoTarea();
			foreach (Departamento _Depto in LsDepartamentos)
			{
				if (_Depto.IsSaved && _Depto.ToDelete)
					DeptoTareaRep.DeleteAssignment(UidTarea, _Depto.UidDepartamento);
				else if (_Depto.IsSaved && !_Depto.ToDelete)
					PeriodicidadRep.ActualizarFechaInicioPrimerCumplimiento(UidTarea, DtProximoCumplimiento);
				else if (!_Depto.IsSaved)
				{
					DT = new DepartamentoTarea();
					DT.UidTarea = UidTarea;
					DT.UidDepartamento = _Depto.UidDepartamento;
					DeptoTareaRep.Save(DT);
					CumplimientoRep.PrimerCumplimiento(UidTarea, _Depto.UidDepartamento, Guid.Empty, DtProximoCumplimiento);
				}
			}
		}
		public void UpdateAsignacionAreas(Guid UidTarea, List<Area> LsAreas, DateTime DtProximoCumplimiento)
		{
			AreaTarea AT = new AreaTarea();
			foreach (Area _Area in LsAreas)
			{
				if (_Area.IsSaved && _Area.ToDelete)
					AreaTareaRep.DeleteAssignment(UidTarea, _Area.UidArea);
				else if (_Area.IsSaved && !_Area.ToDelete)
					PeriodicidadRep.ActualizarFechaInicioPrimerCumplimiento(UidTarea, DtProximoCumplimiento);
				else if (!_Area.IsSaved)
				{
					AT = new AreaTarea();
					AT.UidTarea = UidTarea;
					AT.UidArea = _Area.UidArea;
					AreaTareaRep.Save(AT);
					CumplimientoRep.PrimerCumplimiento(UidTarea, Guid.Empty, _Area.UidArea, DtProximoCumplimiento);
				}
			}
		}

		public void DeleteOpcionesTarea(Guid UidTarea)
		{
			// Sera muy necesario
		}
		public void DeletePeriodicidadSemanal(Guid UidPeriodicidad)
		{
			PSemanalRep.Delete(UidPeriodicidad);
		}
		public void DeletePeriodicidadMensual(Guid UidPeriodicidad)
		{
			PMensualRep.Delete(UidPeriodicidad);
		}
		public void DeletePeriodicidadAnual(Guid UidPeriodicidad)
		{
			PAnualRep.Delete(UidPeriodicidad);
		}
		public void DeleteAllFechaPeriodicidad(Guid UidPeriodicidad)
		{
			FechaPeriodicidadRep.DeleteAll(UidPeriodicidad);
		}
		public void DeleteFechaPeriodicidad(Guid UidFechaPeriodicidad)
		{
			FechaPeriodicidadRep.DeleteItem(UidFechaPeriodicidad);
		}
		public void DeleteAntecesor(Guid UidTarea)
		{
			AntecesorRep.Remove(UidTarea);
		}

		/// <summary>
		/// Deshabilita la notificacion de la tarea
		/// </summary>
		public void DisableNotificacion(Guid UidTarea)
		{
			NotificacionRep.Disable(UidTarea);
		}

		/// <summary>
		/// Validar si la tarea seleccionada tiene configurada una notificación
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <returns></returns>
		public bool ValidarNotificacionAntecesor(Guid UidTarea)
		{
			bool Result = false;
			Notificacion nTemp = NotificacionRep.Get(UidTarea);

			if (nTemp == null)
				Result = false;
			else
				Result = true;


			return Result;
		}
		/// <summary>
		/// Obtener proxima fecha de cumplimiento de la tarea (Semanal)
		/// </summary>
		/// <param name="DtInicio"></param>
		/// <param name="Frecuencia"></param>
		/// <param name="BlLunes"></param>
		/// <param name="BlMartes"></param>
		/// <param name="BlMiercoles"></param>
		/// <param name="BlJueves"></param>
		/// <param name="BlViernes"></param>
		/// <param name="BlSabado"></param>
		/// <param name="BlDomingo"></param>
		/// <returns></returns>
		public DateTime CalcularCumplimientoPeriodicidadSemanal(DateTime DtInicio, int Frecuencia, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
		{
			DateTime DtNewDate = DateTime.Today;
			DtNewDate = PSemanalRep.CalculateNext(DtInicio, true, Frecuencia, BlLunes, BlMartes, BlMiercoles, BlJueves, BlViernes, BlSabado, BlDomingo);
			return DtNewDate;
		}
		/// <summary>
		/// Obtener proxima fecha de cumplimiento de la tarea (Mensual)
		/// </summary>
		/// <param name="Tipo"></param>
		/// <param name="DtInicio"></param>
		/// <param name="DiaMes"></param>
		/// <param name="IntOrdinal"></param>
		/// <param name="DiaSemana"></param>
		/// <param name="Frecuencia"></param>
		/// <returns></returns>
		public DateTime CalcularCumplimientoPeriodicidadMensual(string Tipo, DateTime DtInicio, List<FechaPeriodicidad> LsFechas, string DiasMes, int IntOrdinal, int DiaSemana, int Frecuencia)
		{
			DateTime DtNewDate = DateTime.Today;
			if (Tipo.Equals("A"))
				DtNewDate = PMensualRep.CalculateNextTypeA(DtInicio, true, DiasMes, Frecuencia);
			else if (Tipo.Equals("B"))
				DtNewDate = PMensualRep.CalculateNextTypeB(DtInicio, true, LsFechas, Frecuencia);
			else if (Tipo.Equals("C"))
				DtNewDate = PMensualRep.CalculateNextTypeC(DtInicio, true, IntOrdinal, DiaSemana, Frecuencia);
			return DtNewDate;
		}
		/// <summary>
		/// Obtener proxima fecha de cumplimiento de la tarea (Anual)
		/// </summary>
		/// <param name="Tipo"></param>
		/// <param name="DtInicio"></param>
		/// <param name="DiaMes"></param>
		/// <param name="IntMes"></param>
		/// <param name="IntOrdinal"></param>
		/// <param name="DiaSemana"></param>
		/// <param name="Frecuencia"></param>
		/// <returns></returns>
		public DateTime CalcularCumplimientoPeriodicidadAnual(string Tipo, DateTime DtInicio, List<DateTime> LsDias, int IntMes, int IntOrdinal, int DiaSemana, int Frecuencia)
		{
			DateTime DtNewDate = DateTime.Today;
			if (Tipo.Equals("A"))
				DtNewDate = PAnualRep.CalculateNextTypeA(DtInicio, true, LsDias, Frecuencia);
			else
				DtNewDate = PAnualRep.CalculateNextTypeB(DtInicio, true, IntOrdinal, DiaSemana, IntMes, Frecuencia);
			return DtNewDate;
		}
		#endregion
	}
}