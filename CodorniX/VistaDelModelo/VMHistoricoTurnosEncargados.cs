﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMHistoricoTurnosEncargados
	{
		#region Properties
		public Tarea tTarea
		{
			get;
			set;
		}
		public Revision rRevision
		{
			get;
			set;
		}
		public Cumplimiento cCumplimiento
		{
			get;
			set;
		}
		public TurnoIniciado tiTurnoIniciado
		{
			get;
			set;
		}
		private Revision.Repository RevisionRepository = new Revision.Repository();
		private Cumplimiento.Repository CumplimientoRepository = new Cumplimiento.Repository();
		private TurnoIniciado.Repository TiRepository = new TurnoIniciado.Repository();
		private ResumenTarea.Repositorio ResumenTareaRepositorio = new ResumenTarea.Repositorio();
		public List<TareaOpcion> LsOpcionesTarea
		{
			get;
			set;
		}
		public List<TurnoIniciado> LsTurnos = new List<TurnoIniciado>();
		public List<Sucursal> LsSucursales
		{
			get;
			set;
		}
		public List<Encargado> LsEncargados
		{
			get;
			set;
		}
		public List<Calificacion> LsCalificaciones
		{
			get;
			set;
		}
		public List<ResumenTarea> LsTareasCompletadas
		{
			get;
			set;
		}
		public List<ResumenTarea> LsTareasRequeridasNoCompletadas
		{
			get;
			set;
		}
		public List<ResumenTarea> LsTareasNoCompletadas
		{
			get;
			set;
		}
		public List<ResumenTarea> LsTareasRequeridas
		{
			get;
			set;
		}
		public List<ResumenTarea> LsTareasCanceladas
		{
			get;
			set;
		}
		public List<ResumenTarea> LsTareasPospuestas
		{
			get;
			set;
		}
		#endregion

		public VMHistoricoTurnosEncargados()
		{
		}

		#region Methods
		public void Search(string UidDepartamentos, int? FolioTurno, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidSucursal, Guid UidEncargado, Guid UidEmpresa)
		{
			this.LsTurnos = TiRepository.Busqueda(UidDepartamentos, FolioTurno, DtFechaInicio, DtFechaFin, UidSucursal, UidEncargado, UidEmpresa);
		}
		public void EncontrarTurnoIniciado(Guid UidTurnoIniciado)
		{
			this.tiTurnoIniciado = TiRepository.FindById(UidTurnoIniciado);
		}
		public void ObtenerCumplimiento(Guid UidCumplmiento)
		{
			this.cCumplimiento = CumplimientoRepository.Find(UidCumplmiento);
		}
		public void ObtenerRevision(Guid UidCumplimiento)
		{
			this.rRevision = RevisionRepository.GetByCumplimientoId(UidCumplimiento);
		}
		public void ObtenerTarea(Guid UidTarea)
		{
			Tarea.Repositorio TareaRepositorio = new Tarea.Repositorio();
			tTarea = TareaRepositorio.Encontrar(UidTarea);
		}
		public void ObtenerOpcionesTarea(Guid UidTarea)
		{
			TareaOpcion.Repositorio TORepository = new TareaOpcion.Repositorio();

			this.LsOpcionesTarea = TORepository.Buscar(UidTarea);
		}
		public void ObtenerCalificaciones(Guid UidEmpresa)
		{
			Calificacion.Repository CalificacionRepository = new Calificacion.Repository();
			this.LsCalificaciones = CalificacionRepository.Search(UidEmpresa, string.Empty, true,-1);
		}

		public void ObtenerSucursalesEmpresa(Guid UidEmpresa)
		{
			this.LsSucursales = new List<Sucursal>();
			Sucursal.Repository SucRepository = new Sucursal.Repository();
			this.LsSucursales = SucRepository.FindAll(UidEmpresa);
			this.LsSucursales.Insert(0, new Sucursal() { UidSucursal = Guid.Empty, StrNombre = "Todos" });
		}
		public void ObtenerEncargadosEmpresa(Guid UidEmpresa)
		{
			this.LsEncargados = new List<Encargado>();
			Encargado.Repository UsrRepository = new Encargado.Repository();
			this.LsEncargados = UsrRepository.FindAll(UidEmpresa);
			this.LsEncargados.Insert(0, new Encargado() { UIDUSUARIO = Guid.Empty, STRNOMBRE = "Todos" });
		}

		public void ObtenerTareasCumplidasTurno(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, string Direction, string Expression)
		{
			this.LsTareasCompletadas = ResumenTareaRepositorio.TareasCumplidas(UidDepartamento, UidUsuario, Fecha);

			if (Direction != string.Empty && Expression != string.Empty)
			{
				switch (Expression)
				{
					case "FolioC":
						if (Direction.Equals("ASC"))
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderBy(t => t.IntFolioCumplimiento).ToList();
						else
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderByDescending(t => t.IntFolioCumplimiento).ToList();
						break;
					case "FolioT":
						if (Direction.Equals("ASC"))
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderBy(t => t.IntFolio).ToList();
						else
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderByDescending(t => t.IntFolio).ToList();
						break;
					case "Nombre":
						if (Direction.Equals("ASC"))
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderBy(t => t.StrNombre).ToList();
						else
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderByDescending(t => t.StrNombre).ToList();
						break;
					case "TipoT":
						if (Direction.Equals("ASC"))
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderBy(t => t.StrTipoTarea).ToList();
						else
							this.LsTareasCompletadas = this.LsTareasCompletadas.OrderByDescending(t => t.StrTipoTarea).ToList();
						break;
					default:
						break;
				}
			}
		}
		public void ObtenerTareasRequeridasNoCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid UidSucursal, string Direction, string Expression)
		{
			this.LsTareasRequeridasNoCompletadas = ResumenTareaRepositorio.TareasRequeridasNoCumplidas(UidDepartamento, UidUsuario, Fecha, UidSucursal);
		}
		public void ObtenerTareasNoCumplidasTurno(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid UidSucursal, string Direction, string Expression)
		{
			this.LsTareasNoCompletadas = ResumenTareaRepositorio.TareasNoCumplidas(UidDepartamento, UidUsuario, Fecha, UidSucursal);

			if (Direction != string.Empty && Expression != string.Empty)
			{
				switch (Expression)
				{
					case "FolioC":
						if (Direction.Equals("ASC"))
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.IntFolioCumplimiento).ToList();
						else
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.IntFolioCumplimiento).ToList();
						break;
					case "FolioT":
						if (Direction.Equals("ASC"))
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.IntFolio).ToList();
						else
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.IntFolio).ToList();
						break;
					case "Nombre":
						if (Direction.Equals("ASC"))
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.StrNombre).ToList();
						else
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.StrNombre).ToList();
						break;
					case "Estado":
						if (Direction.Equals("ASC"))
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.StrEstadoCumplimiento).ToList();
						else
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.StrEstadoCumplimiento).ToList();
						break;
					case "TipoT":
						if (Direction.Equals("ASC"))
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.StrTipoTarea).ToList();
						else
							this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.StrTipoTarea).ToList();
						break;
					default:
						break;
				}
			}
		}
		public void ObtenerTareasRequeridasTurno(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid UidSucursal, string Direction, string Expression)
		{
			this.LsTareasRequeridas = ResumenTareaRepositorio.TareasRequeridas(UidDepartamento, UidUsuario, Fecha, UidSucursal);

			if (Direction != string.Empty && Expression != string.Empty)
			{
				switch (Expression)
				{
					case "FolioC":
						if (Direction.Equals("ASC"))
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.IntFolioCumplimiento).ToList();
						else
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.IntFolioCumplimiento).ToList();
						break;
					case "FolioT":
						if (Direction.Equals("ASC"))
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.IntFolio).ToList();
						else
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.IntFolio).ToList();
						break;
					case "Nombre":
						if (Direction.Equals("ASC"))
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.StrNombre).ToList();
						else
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.StrNombre).ToList();
						break;
					case "Estado":
						if (Direction.Equals("ASC"))
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.StrEstadoCumplimiento).ToList();
						else
							this.LsTareasRequeridas = this.LsTareasRequeridas.OrderBy(t => t.StrEstadoCumplimiento).ToList();
						break;
					default:
						break;
				}
			}
		}
		public void ObtenerTareasCanceladasTurno(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid UidSucursal, string Direction, string Expression)
		{
			this.LsTareasCanceladas = ResumenTareaRepositorio.TareasCanceladas(UidDepartamento, UidUsuario, Fecha, UidSucursal);
			if (Direction != string.Empty && Expression != string.Empty)
			{
				switch (Expression)
				{
					case "FolioC":
						if (Direction.Equals("ASC"))
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderBy(t => t.IntFolioCumplimiento).ToList();
						else
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderByDescending(t => t.IntFolioCumplimiento).ToList();
						break;
					case "FolioT":
						if (Direction.Equals("ASC"))
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderBy(t => t.IntFolio).ToList();
						else
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderByDescending(t => t.IntFolio).ToList();
						break;
					case "Nombre":
						if (Direction.Equals("ASC"))
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderBy(t => t.StrNombre).ToList();
						else
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderByDescending(t => t.StrNombre).ToList();
						break;
					case "Tipo":
						if (Direction.Equals("ASC"))
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderBy(t => t.StrTipoTarea).ToList();
						else
							this.LsTareasCanceladas = this.LsTareasCanceladas.OrderByDescending(t => t.StrTipoTarea).ToList();
						break;
					default:
						break;
				}
			}
		}
		public void ObtenerTareasPospuestasTurno(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid UidSucursal, string Direction, string Expression)
		{
			this.LsTareasPospuestas = ResumenTareaRepositorio.TareasPospuestas(UidDepartamento, UidUsuario, Fecha, UidSucursal);
			if (Direction != string.Empty && Expression != string.Empty)
			{
				switch (Expression)
				{
					case "FolioC":
						if (Direction.Equals("ASC"))
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderBy(t => t.IntFolioCumplimiento).ToList();
						else
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderByDescending(t => t.IntFolioCumplimiento).ToList();
						break;
					case "FolioT":
						if (Direction.Equals("ASC"))
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderBy(t => t.IntFolio).ToList();
						else
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderByDescending(t => t.IntFolio).ToList();
						break;
					case "Nombre":
						if (Direction.Equals("ASC"))
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderBy(t => t.StrNombre).ToList();
						else
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderByDescending(t => t.StrNombre).ToList();
						break;
					case "Tipo":
						if (Direction.Equals("ASC"))
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderBy(t => t.StrTipoTarea).ToList();
						else
							this.LsTareasPospuestas = this.LsTareasPospuestas.OrderByDescending(t => t.StrTipoTarea).ToList();
						break;
					default:
						break;
				}
			}
		}
		#endregion
	}
}