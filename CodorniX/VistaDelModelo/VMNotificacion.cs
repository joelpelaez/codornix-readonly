﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
	public class VMNotificacion
	{
		EstadoNotificacion.Repository estadoRepository = new EstadoNotificacion.Repository();
		MensajeNotificacion.Repository notificacionRepository = new MensajeNotificacion.Repository();

		private List<EstadoNotificacion> _Estados;
		public List<EstadoNotificacion> Estados
		{
			get { return _Estados; }
			set { _Estados = value; }
		}

		private List<MensajeNotificacion> _Mensajes;
		public List<MensajeNotificacion> Mensajes
		{
			get { return _Mensajes; }
			set { _Mensajes = value; }
		}

		private MensajeNotificacion _Notificacion;
		public MensajeNotificacion Notificacion
		{
			get { return _Notificacion; }
			set { _Notificacion = value; }
		}

		private List<Departamento> _Departamentos;
		public List<Departamento> Departamentos
		{
			get { return _Departamentos; }
			set { _Departamentos = value; }
		}

		private List<ParametroOrdenamiento> _LsParametrosOrdenamiento;
		public List<ParametroOrdenamiento> LsParametrosOrdenamiento
		{
			get { return _LsParametrosOrdenamiento; }
			set { _LsParametrosOrdenamiento = value; }
		}


		public void ObtenerEstados()
		{
			_Estados = estadoRepository.FindAll();
		}

		public void ObtenerNotificaciones(Guid uidSucursal, string uidDepartamentos, string uidDepartamentosBusqueda,
			string estados, DateTime? fechaInicio, DateTime? fechaFin)
		{
			_Mensajes = notificacionRepository.Search(uidSucursal, uidDepartamentos, uidDepartamentosBusqueda, estados, fechaInicio, fechaFin);
		}

		public void ObtenerNotificacion(Guid uid)
		{
			_Notificacion = notificacionRepository.Find(uid);
		}

		public void ObtenerDepartamentos(Guid uidUsuario, DateTime fecha)
		{
			_Departamentos = notificacionRepository.GetDepartamentos(uidUsuario, fecha);
		}

		public void CambiarEstado(Guid uidNotificacion, string estado)
		{
			notificacionRepository.ChangeState(uidNotificacion, estado);
		}

		public void CargarParametrosOrdenamiento()
		{
			this._LsParametrosOrdenamiento = new List<ParametroOrdenamiento>();
			_LsParametrosOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Folio de Cumplimiento",
				BlOrdenar = true,
				Direccion = "ASC"
			});
			_LsParametrosOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Folio de Tarea",
				BlOrdenar = false,
				Direccion = "ASC"
			});
			_LsParametrosOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Tarea",
				BlOrdenar = false,
				Direccion = "ASC"
			});
			_LsParametrosOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Departamento",
				BlOrdenar = false,
				Direccion = "ASC"
			});
			_LsParametrosOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Fecha",
				BlOrdenar = false,
				Direccion = "ASC"
			});
			_LsParametrosOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Hora",
				BlOrdenar = false,
				Direccion = "ASC"
			});
			_LsParametrosOrdenamiento.Add(new ParametroOrdenamiento()
			{
				ID = Guid.NewGuid(),
				Parametro = "Estado",
				BlOrdenar = false,
				Direccion = "ASC"
			});
		}
	}
}