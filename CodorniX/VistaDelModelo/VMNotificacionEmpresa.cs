﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Collections.Generic;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMNotificacionEmpresa
	{
		#region Properties
		private MensajeNotificacion.Repository NRepositorio = new MensajeNotificacion.Repository();
		private NotificacionEmpresa.Repository NotificacionRepositorio = new NotificacionEmpresa.Repository();
		private Notificacion.Repository NotificacionRepository = new Notificacion.Repository();
		public Notificacion nNotificacion
		{
			get;
			set;
		}
		public NotificacionEmpresa NotificacionEmpresa
		{
			get;
			set;
		}
		public List<NotificacionEmpresa> LsNotificaciones
		{
			get;
			set;
		}
		public List<TareaOpcion> LsOpcionesTarea
		{
			get;
			set;
		}

		/*
		 * Filtros de busqueda
		 */
		public List<EstadoNotificacion> LsEstadoNotificacion
		{
			get;
			set;
		}
		public List<Sucursal> LsSucursales
		{
			get;
			set;
		}
		public List<Departamento> LsDepartamento
		{
			get;
			set;
		}
		public List<Area> LsArea
		{
			get;
			set;
		}
		#endregion

		public VMNotificacionEmpresa()
		{
		}

		#region Methods
		public void Search(Guid UidEmpresa, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidEstadoNotificacion, Guid UidSucursal, Guid UidDepartamento, Guid UidArea)
		{
			this.LsNotificaciones = NotificacionRepositorio.Busqueda(UidEmpresa, DtFechaInicio, DtFechaFin, UidEstadoNotificacion, UidSucursal, UidDepartamento, UidArea);
		}
		public void BuscarPorId(Guid UidMensajeNotificacion)
		{
			this.NotificacionEmpresa = NotificacionRepositorio.GetById(UidMensajeNotificacion);
		}
		public bool CambiarEstatus(Guid UidMensajeNotificacion, string Estado)
		{
			try
			{
				NRepositorio.ChangeState(UidMensajeNotificacion, Estado);
				return true;
			}
			catch (Exception)
			{
				return false;
			}
		}

		public void GetEstadosNotificacion()
		{
			EstadoNotificacion.Repository ENRepositorio = new EstadoNotificacion.Repository();
			this.LsEstadoNotificacion = new List<EstadoNotificacion>();
			this.LsEstadoNotificacion = ENRepositorio.FindAll();
			this.LsEstadoNotificacion.Insert(0, new EstadoNotificacion() { UidEstadoNotitifacion = Guid.Empty, StrEstadoNotificacion = "Todos" });
		}

		public void GetSucursalesEmpresa(Guid UidEmpresa)
		{
			Sucursal.Repository SRepositorio = new Sucursal.Repository();
			this.LsSucursales = SRepositorio.FindAll(UidEmpresa);
			this.LsSucursales.Insert(0, new Sucursal() { UidSucursal = Guid.Empty, StrNombre = "Todos" });
		}
		public void GetDepartamentosSucursal(Guid UidSucursal)
		{
			this.LsDepartamento = new List<Departamento>();
			Departamento.Repository DeptoRepository = new Departamento.Repository();
			this.LsDepartamento = DeptoRepository.EncontrarTodos(UidSucursal);
			this.LsDepartamento.Insert(0, new Departamento() { UidDepartamento = Guid.Empty, StrNombre = "Todos" });
		}
		public void GetAreasDepartamento(Guid UidDepartamento)
		{
			this.LsArea = new List<Area>();
			Area.Repository AreaRepository = new Area.Repository();
			this.LsArea = AreaRepository.FindAll(UidDepartamento);
			this.LsArea.Insert(0, new Area() { UidArea = Guid.Empty, StrNombre = "Todos" });
		}

		public void GetParametrosNotificacionTarea(Guid UidTarea)
		{
			this.nNotificacion = NotificacionRepository.Get(UidTarea);
		}
		public void GetOpcionesTarea(Guid UidTarea)
		{
			TareaOpcion.Repositorio TaRepository = new TareaOpcion.Repositorio();
			this.LsOpcionesTarea = TaRepository.Buscar(UidTarea);
		}
		#endregion
	}
}