﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMInformacionUsuario
	{
		#region Properties
		private Usuario.Repository uUsuarioRep = new Usuario.Repository();
		public Usuario uUsuario = new Usuario();
		#endregion

		public VMInformacionUsuario()
		{

		}

		#region Methods
		public void GetUsuario(Guid UidUsuario)
		{
			this.uUsuario = uUsuarioRep.Find(UidUsuario);
		}

		public bool ValidarNombreUsuario(string Name)
		{
			Usuario uUsuario = uUsuarioRep.FindByName(Name);

			if (uUsuario == null)
				return true;
			else
				return false;
		}
		public bool ValidarContraseñaActual(Guid UidUsuario, string Password)
		{
			Usuario uUsuario = uUsuarioRep.ValidatePassword(Password, UidUsuario);

			if (uUsuario == null)
				return false;
			else
				return true;
		}
		public bool UpdatePassword(Guid UidUsuario, string Password)
		{
			return uUsuarioRep.UpdatePassword(UidUsuario, Password);
		}

		public bool ActualizarDatos(Guid UidUsuario, string Nombre, string ApellidoPaterno, string ApellidoMaterno, DateTime DtFechaNacimiento, string CorreoElectronico)
		{
			return uUsuarioRep.Update(UidUsuario, Nombre, ApellidoPaterno, ApellidoMaterno, DtFechaNacimiento, CorreoElectronico);
		}
		#endregion
	}
}