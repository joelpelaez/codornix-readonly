﻿using CodorniX.Modelo;
using CodorniX.Modelo.Model;
using CodorniX.Modelo.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMReporteSupervisor
    {
        Usuario.Repository usuarioRepository = new Usuario.Repository();
        Empresa.Repository empresaRepository = new Empresa.Repository();
        Sucursal.Repository sucursalRepository = new Sucursal.Repository();
        TurnoSupervisor.Repository turnoRepository = new TurnoSupervisor.Repository();
        ReporteSupervisorRepository reporteSupervisionRepository = new ReporteSupervisorRepository();

        public Usuario Usuario { get; private set; }
        public Empresa Empresa { get; private set; }
        public Sucursal Sucursal { get; private set; }
        public TurnoSupervisor TurnoSupervisor { get; private set; }
        public List<ReporteSupervisor> ReportesCumplimiento { get; private set; }
        public List<ReporteSupervisor> ReportesRevision { get; private set; }

        public void ObtenerUsuario(Guid uid)
        {
            Usuario = usuarioRepository.Find(uid);
        }

        public void ObtenerEmpresa(Guid uid)
        {
            Empresa = empresaRepository.Find(uid);
        }

        public void ObtenerSucursal(Guid uid)
        {
            Sucursal = sucursalRepository.Find(uid);
        }

        public void ObtenerReportesCumplimiento(Guid UidTurnoSupervisor)
        {
            ReportesCumplimiento = reporteSupervisionRepository.ObtenerReporteCumplimiento(UidTurnoSupervisor);
        }

        public void ObtenerReportesRevision(Guid UidTurnoSupervisor)
        {
            ReportesRevision = reporteSupervisionRepository.ObtenerReporteRevision(UidTurnoSupervisor);
        }

        public void ObtenerTurnoSupervisor(Guid uid)
        {
            TurnoSupervisor = turnoRepository.Find(uid);
        }
    }
}