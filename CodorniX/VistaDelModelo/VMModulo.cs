﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.ConexionDB;
using CodorniX.Modelo;
using System.Data;
using System.Data.SqlClient;


namespace CodorniX.VistaDelModelo
{
    public class VMModulo
    {
        DBModulo Modulo = new DBModulo();

        Usuario _CUsuario = new Usuario();
        public Usuario CUsuario
        {
            get { return _CUsuario; }
            set { _CUsuario = value; }
        }
        public Guid Usuario_Modulo(Guid usuario)
        {
            Guid idModulo = new Guid();
            foreach (DataRow item in Modulo.obteneridModuloUsuario(usuario.ToString()).Rows)
            {
                idModulo = new Guid(item["MidModulo"].ToString());
            }
            return idModulo;
        }

        public string idUsuarioModulo(string idModulo)
        {
            string NombrePerfil = "";
            foreach (DataRow item in Modulo.obtenerNombreModulo(idModulo).Rows)
            {
                NombrePerfil = item["VchModulo"].ToString();
            }
            return NombrePerfil;
        }
    }
}