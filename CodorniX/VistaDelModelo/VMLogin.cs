﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using System.Net.Mail;
using CodorniX.Util;

namespace CodorniX.VistaDelModelo
{
	public class VMLogin
	{
		DBLogin login = new DBLogin();

		Permisos _CPermisos = new Permisos();
		public Permisos CPermisos
		{
			get { return _CPermisos; }
			set { _CPermisos = value; }
		}


		public Guid IniciarSesion(string usuario, string password)
		{
			Guid id = new Guid();
			foreach (DataRow item in login.obtenerUsuarioLogin(usuario, password).Rows)
			{
				id = new Guid(item["UidUsuario"].ToString());
			}
			return id;
		}
		public Guid Perfiles(Guid usuario)
		{
			Guid id = new Guid();
			foreach (DataRow item in login.obtenerperfiles(usuario).Rows)
			{
				id = new Guid(item["UidPerfil"].ToString());
			}
			return id;
		}

		//public Guid Permisos(Guid perfil)
		//{
		//    Guid id = new Guid();
		//    foreach (DataRow item in login.obtenerpermisos(perfil).Rows)
		//    {
		//        id = new Guid(item["MidModulo"].ToString());
		//    }
		//    return id;
		//}

		public string perfil(string idUsuario)
		{
			string NombrePerfil = "";
			foreach (DataRow item in login.obtenerPerfilLogin(idUsuario).Rows)
			{
				NombrePerfil = item["VchPerfil"].ToString();
			}
			return NombrePerfil;
		}

		public string empresa(string uidperfil)
		{
			string NombrePerfil = "";
			foreach (DataRow item in login.obtenerEmpresaPerfil(uidperfil).Rows)
			{
				NombrePerfil = item["VchNombreComercial"].ToString();
			}
			return NombrePerfil;
		}

		public Guid idperfil(string nombreperfil)
		{
			Guid NombrePerfil = Guid.Empty;
			foreach (DataRow item in login.obteneridperfil(nombreperfil).Rows)
			{
				NombrePerfil = new Guid(item["UidPerfil"].ToString());
			}
			return NombrePerfil;
		}

		public List<Permisos> LtsPermisos = new List<Permisos>();
		public void CargarListaDePermisos(Guid perfil)
		{
			foreach (DataRow item in login.obtenerpermisos(perfil).Rows)
			{
				CPermisos = new Permisos()
				{
					UIDPERFIL = new Guid(item["UidPerfil"].ToString()),
					UidModulo = new Guid(item["UidModulo"].ToString())

				};
				LtsPermisos.Add(CPermisos);
			}
		}

		public void SendNotificacionLogin(string StrUsuario, string StrEmpresa, string StrSucusal, DateTime DtFecha)
		{
			try
			{
				var Modelo = new
				{
					Asunto = "Notificacion de acceso Codornix",
					Usuario = StrUsuario,
					Empresa = StrEmpresa,
					Sucursal = StrSucusal == string.Empty ? "(na)" : StrSucusal,
					Fecha = DtFecha.ToString("dd/MM/yyyy"),
					Hora = DtFecha.ToString("t")
				};

				MailAddress mMail = new MailAddress("osbel_lugo@yahoo.com", "Osbel Lugo");
				//MailAddress mMail = new MailAddress("hectorb.peraza@gmail.com", "Osbel Lugo");
				MailAddressCollection mailAddresses = new MailAddressCollection();
				mailAddresses.Add(mMail);

				PageRender pageRender = PageRender.Instance();
				string content = pageRender.RenderViewToString("LoginNotification.cshtml", Modelo);

				Correo correo = new Correo();
				correo.SendEmail(mailAddresses, "Notificion de acceso CodorniX", content);
			}
			catch (Exception ex)
			{
				// No enviada
			}
		}

		public bool SendPasswordRecovery(string StrUsuario)
		{
			try
			{
				Usuario.Repository uUsuarioRep = new Usuario.Repository();
				Usuario uUsuario = uUsuarioRep.FindByName(StrUsuario);

				if (uUsuario == null)
				{
					return false;
				}
				else
				{
					string Email = uUsuario.STRCORREO;
					string FullName = uUsuario.STRNOMBRE + " " + uUsuario.STRAPELLIDOPATERNO + " " + uUsuario.STRAPELLIDOMATERNO;
					if (Email == string.Empty)
						return false;

					if (!IsValidEmail(Email))
					{
						return false;
					}

					MailAddress mMail = new MailAddress(Email, FullName);
					MailAddressCollection mailAddresses = new MailAddressCollection();
					mailAddresses.Add(mMail);

					Correo correo = new Correo();
					correo.SendEmail(mailAddresses, "Recuperacion de contraseña CodorniX", "Contraseña actual: " + uUsuario.STRPASSWORD);

					return true;
				}
			}
			catch (Exception ex)
			{
				//
				return false;
			}
		}

		private static bool IsValidEmail(string email)
		{
			try
			{
				var addr = new MailAddress(email);
				return addr.Address == email;
			}
			catch
			{
				return false;
			}
		}

	}
}