﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Web;
using CodorniX.Modelo;
using CodorniX.Util;

namespace CodorniX.VistaDelModelo
{
	public class VMCumplimientoAtrasado
	{
		#region Properties		
		public string CumplimientoCancelado
		{
			get
			{
				return "Cancelado";
			}
		}
		public string CumplimientoCompleto
		{
			get
			{
				return "Completo";
			}
		}

		private Usuario.Repository UsuarioRep = new Usuario.Repository();
		private Empresa.Repository EmpresaRep = new Empresa.Repository();
		private Sucursal.Repository SucursalRep = new Sucursal.Repository();
		private Departamento.Repository DepartamentoRep = new Departamento.Repository();
		private Area.Repository aAreaRep = new Area.Repository();
		private Tarea.Repositorio tTareaRep = new Tarea.Repositorio();
		private Cumplimiento.Repository cCumplimientoRep = new Cumplimiento.Repository();
		private TurnoIniciado.Repository InicioTurnoRep = new TurnoIniciado.Repository();
		private TurnoIniciado.Repository tiTurnoIniciadoRep = new TurnoIniciado.Repository();
		private MensajeNotificacion.Repository mMensajeRep = new MensajeNotificacion.Repository();
		public TurnoIniciado tiTurno
		{
			get;
			set;
		}
		public TurnoIniciado tiTurnoAbierto
		{
			get;
			set;
		}
		public Periodo pPeriodo
		{
			get;
			set;
		}
		public Usuario uUsuario
		{
			get;
			set;
		}
		public Cumplimiento cCumplimiento
		{
			get;
			set;
		}
		public Tarea tTarea
		{
			get;
			set;
		}
		public Departamento dDepartamento
		{
			get;
			set;
		}
		public Area aArea
		{
			get;
			set;
		}
		public Periodicidad pPeriodicidad
		{
			get;
			set;
		}
		public TipoFrecuencia tfFrecuencia
		{
			get;
			set;
		}
		public List<TurnoIniciado> LsTurnos
		{
			get;
			set;
		}
		public List<Usuario> LsEncargados
		{
			get;
			set;
		}
		public List<Cumplimiento> LsTareasNoCompletadas
		{
			get;
			set;
		}
		public List<TareaOpcion> LsOpciones
		{
			get;
			set;
		}
		public List<Departamento> LsDepartamentos
		{
			get;
			set;
		}
		public List<Area> LsAreas
		{
			get;
			set;
		}
		public List<TipoTarea> LsTiposTarea
		{
			get;
			set;
		}
		#endregion

		public VMCumplimientoAtrasado()
		{
		}

		#region Methods
		public void Search(Guid UidEmpresa, Guid UidSucursal, Guid UidEncargado, string UidDepartamentos, int? FolioTurno, DateTime? DtFechaInicio, DateTime? DtFechaFin)
		{
			this.LsTurnos = InicioTurnoRep.BusquedaTurnosCumplimientoPosterior(UidEmpresa, UidSucursal, UidEncargado, UidDepartamentos, FolioTurno, DtFechaInicio, DtFechaFin);
		}
		public void SortList(string SortDirection, string SortExpression)
		{
			switch (SortExpression)
			{
				case "Folio":
					if (SortDirection.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Folio).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Folio).ToList();
					break;
				case "Encargado":
					if (SortDirection.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Usuario).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Usuario).ToList();
					break;
				case "Departamento":
					if (SortDirection.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Departamento).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Departamento).ToList();
					break;
				case "FInicio":
					if (SortDirection.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.DtoFechaInicio).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.DtoFechaInicio).ToList();
					break;
				case "FFin":
					if (SortDirection.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.DtoFechaFin).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.DtoFechaFin).ToList();
					break;
				case "Turno":
					if (SortDirection.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.Turno).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.Turno).ToList();
					break;
				case "Estado":
					if (SortDirection.Equals("ASC"))
						this.LsTurnos = this.LsTurnos.OrderBy(t => t.EstadoTurno).ToList();
					else
						this.LsTurnos = this.LsTurnos.OrderByDescending(t => t.EstadoTurno).ToList();
					break;
				default:
					break;
			}
		}

		public void SortListCumplimiento(string SortDirection, string SortExpression)
		{
			switch (SortExpression)
			{
				case "FCumplimiento":
					if (SortDirection.Equals("ASC"))
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.IntFolio).ToList();
					else
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.IntFolio).ToList();
					break;
				case "FTarea":
					if (SortDirection.Equals("ASC"))
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.IntFolioTarea).ToList();
					else
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.IntFolioTarea).ToList();
					break;
				case "Tarea":
					if (SortDirection.Equals("ASC"))
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.StrTarea).ToList();
					else
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.StrTarea).ToList();
					break;
				case "Departamento":
					if (SortDirection.Equals("ASC"))
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.StrDepartamento).ToList();
					else
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.StrDepartamento).ToList();
					break;
				case "Area":
					if (SortDirection.Equals("ASC"))
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderBy(t => t.StrArea).ToList();
					else
						this.LsTareasNoCompletadas = this.LsTareasNoCompletadas.OrderByDescending(t => t.StrArea).ToList();
					break;
				default:
					break;
			}
		}

		public void GetEncargadosSupervisor()
		{
			LsEncargados = new List<Usuario>();
			LsEncargados.Insert(0, new Usuario() { UIDUSUARIO = Guid.Empty, STRNOMBRE = "Todos" });
		}
		public void GetEncargadosAdministrador(Guid UidSucursal)
		{
			LsEncargados = UsuarioRep.GetAllSucursal(UidSucursal);
			LsEncargados.Insert(0, new Usuario() { UIDUSUARIO = Guid.Empty, STRNOMBRE = "Todos" });
		}
		public void GetTurno(Guid UidInicioTurno)
		{
			this.tiTurno = tiTurnoIniciadoRep.FindById(UidInicioTurno);
		}
		public void GetUltimoTurnoAbiertoPeriodo(Guid UidPeriodo, int IntFolio)
		{
			this.tiTurnoAbierto = tiTurnoIniciadoRep.GetLastOpen(UidPeriodo, IntFolio);
		}
		public void GetPeriodo(Guid UidPeriodo)
		{
			Periodo.Repository pPeriodoRep = new Periodo.Repository();
			this.pPeriodo = pPeriodoRep.Find(UidPeriodo);
		}
		public void GetCumplimiento(Guid UidCumplimiento)
		{
			cCumplimiento = cCumplimientoRep.Find(UidCumplimiento);
		}
		public int GetTotalCumplimientosNuevosTurno(Guid UidPeriodo, DateTime DtFechaNuevoTurno, int IntFolioTurno)
		{
			List<Cumplimiento> LsCumplimientos = this.cCumplimientoRep.ReporteTareasNuevas(UidPeriodo, DtFechaNuevoTurno, IntFolioTurno);
			return LsCumplimientos.Count;
		}
		public void GetTarea(Guid UidTarea)
		{
			this.tTarea = tTareaRep.Encontrar(UidTarea);

		}
		public void GetDepartamento(Guid UidDepartamento)
		{
			Departamento.Repository dDepartamentoRep = new Departamento.Repository();
			this.dDepartamento = dDepartamentoRep.Encontrar(UidDepartamento);
		}
		public void GetDepartamentosSupervisor(List<Guid> LsPeriodos)
		{
			string UidPeriodos = string.Empty;
			foreach (Guid item in LsPeriodos)
			{
				if (UidPeriodos == string.Empty)
					UidPeriodos = item.ToString();
				else
					UidPeriodos += "," + item.ToString();
			}
			this.LsDepartamentos = DepartamentoRep.EncontrarPorLista(UidPeriodos);
			this.LsDepartamentos.Insert(0, new Departamento() { UidDepartamento = Guid.Empty, StrNombre = "Todos" });
		}
		public void GetAreasDepartamento(Guid UidDepartamento)
		{
			Area.Repository aAreaRep = new Area.Repository();
			this.LsAreas = aAreaRep.FindAll(UidDepartamento);

			if (this.LsAreas == null || this.LsAreas.Count == 0)
			{
				this.LsAreas = new List<Area>();
			}

			this.LsAreas.Insert(0, new Area() { UidArea = Guid.Empty, StrNombre = "Todos" });
		}
		public void GetDepartamentosAdministrador(Guid UidSucursal)
		{
			this.LsDepartamentos = DepartamentoRep.EncontrarTodos(UidSucursal);
			this.LsDepartamentos.Insert(0, new Departamento() { UidDepartamento = Guid.Empty, StrNombre = "TODOS" });
		}
		public void GetArea(Guid UidArea)
		{
			Area.Repository aAreaRep = new Area.Repository();
			this.aArea = aAreaRep.Find(UidArea);
		}
		public void GetPeriodicidad(Guid UidPeriodicidad)
		{
			Periodicidad.Repositorio pPeriodicidadRep = new Periodicidad.Repositorio();
			this.pPeriodicidad = pPeriodicidadRep.ConsultarPeriodicidad(UidPeriodicidad);
		}
		public void GetFrecuencia(Guid UidTarea)
		{
			TipoFrecuencia.Repositorio tfRep = new TipoFrecuencia.Repositorio();
			this.tfFrecuencia = tfRep.FindByTarea(UidTarea);
		}
		public void GetOpcionesTarea(Guid UidTarea)
		{
			TareaOpcion.Repositorio OpcionRep = new TareaOpcion.Repositorio();
			this.LsOpciones = OpcionRep.Buscar(UidTarea);
		}
		public void GetTareasNoCumplidas(Guid UidDepartamento, int IntFolioTurno, int? FolioTarea, string StrTarea, Guid UidDepartamentoFiltro, Guid UidArea, Guid UidTipoTarea)
		{
			this.LsTareasNoCompletadas = cCumplimientoRep.GetAtrasadasTurno(UidDepartamento, IntFolioTurno, FolioTarea, StrTarea, UidDepartamentoFiltro, UidArea, UidTipoTarea);
		}
		public void GetTiposTarea()
		{
			TipoTarea.Repositorio tTipoRep = new TipoTarea.Repositorio();
			this.LsTiposTarea = tTipoRep.ConsultarTipoTarea();

			this.LsTiposTarea.Insert(0, new TipoTarea() { UidTipoTarea = Guid.Empty, StrTipoTarea = "Todos" });
		}

		public bool SetCumplimiento(string EstadoCumplimiento, Guid UidCumplimiento, Guid UidUsuario, string StrObservaciones,
			bool? blValor, decimal? DcValor1, decimal? DcValor2, Guid UidOpcion, Guid UidTurno, Guid UidCreador, string StrRolCreador,
			Guid UidTipoMedicion, bool BlAtrasado, DateTimeOffset DtCumplimiento)
		{
			return cCumplimientoRep.RegistrarPosterior(UidCumplimiento, UidUsuario, StrObservaciones, EstadoCumplimiento, blValor, DcValor1, DcValor2, UidOpcion, UidTurno, UidCreador, StrRolCreador, UidTipoMedicion, BlAtrasado, DtCumplimiento);
		}

		/// <summary>
		/// Generar mensaje de notificacion del cumplimiento
		/// </summary>
		/// <param name="UidCumplimiento"></param>
		public bool SetNotification(Guid UidCumplimiento, string StrTipoMedicion)
		{
			bool BlNotificacionCreada = false;
			Cumplimiento cCumplimiento = new Cumplimiento();
			cCumplimiento = cCumplimientoRep.Find(UidCumplimiento);

			Notificacion.Repository nNotificacionRep = new Notificacion.Repository();
			Notificacion nNotificacion = new Notificacion();
			nNotificacion = nNotificacionRep.Get(cCumplimiento.UidTarea);

			mMensajeRep = new MensajeNotificacion.Repository();
			switch (StrTipoMedicion)
			{
				case "Verdadero/Falso":
					if (nNotificacion.BlValor != null)
					{
						if (nNotificacion.BlValor == cCumplimiento.BlValor)
						{
							mMensajeRep.GenerarMensaje(UidCumplimiento);
							BlNotificacionCreada = true;
						}
					}
					break;
				case "Numerico":
					try
					{
						if (nNotificacion.DcMayorQue == null || (nNotificacion.BlMayorIgual == true && cCumplimiento.DcValor1 >= nNotificacion.DcMayorQue) || cCumplimiento.DcValor1 > nNotificacion.DcMayorQue
							&& nNotificacion.DcMenorQue == null || (nNotificacion.BlMenorIgual == true && cCumplimiento.DcValor1 >= nNotificacion.DcMenorQue) || cCumplimiento.DcValor1 < nNotificacion.DcMenorQue)
						{
							mMensajeRep.GenerarMensaje(UidCumplimiento);
							BlNotificacionCreada = true;
						}
					}
					catch (Exception ex)
					{
						/// No hacer nada
					}
					break;
				case "Seleccionable":
					bool Match = false;
					if (nNotificacion.UidOpciones != null && cCumplimiento.UidOpcion != null)
					{
						foreach (Guid UidOpcion in nNotificacion.UidOpciones)
						{
							if (UidOpcion == cCumplimiento.UidOpcion)
							{
								Match = true;
								break;
							}
						}

						if (Match)
						{
							mMensajeRep.GenerarMensaje(UidCumplimiento);
							BlNotificacionCreada = true;
						}
					}
					break;
				default:
					break;
			}
			return BlNotificacionCreada;
		}

		public bool CrearSucesor(Guid UidCumplimiento, Guid UidTarea, bool BlNotificacionTarea, int FolioTurno)
		{
			bool IsCreated = false;

			Sucesor.Repository sSucesorRep = new Sucesor.Repository();
			List<Sucesor> LsSucesor = sSucesorRep.Get(UidTarea);

			foreach (Sucesor sSucesor in LsSucesor)
			{
				if (sSucesor.BlUsarNotificacion)
				{
					if (BlNotificacionTarea)
					{
						sSucesorRep.CrearSucesor(UidCumplimiento, sSucesor.UidTarea, FolioTurno);
					}
				}
				else
				{
					sSucesorRep.CrearSucesor(UidCumplimiento, sSucesor.UidTarea, FolioTurno);
				}
			}

			return IsCreated;
		}

		public void SendNotificacion(Guid uidCumplimiento)
		{
			MensajeNotificacion mensajeNotificacion = mMensajeRep.FindByCumplimiento(uidCumplimiento);
			if (mensajeNotificacion != null)
			{
				string areaNombre = "General";
				Cumplimiento cumplimiento = cCumplimientoRep.Find(mensajeNotificacion.UidCumplimiento);
				Tarea tarea = tTareaRep.Encontrar(cumplimiento.UidTarea);
				Departamento departamento = null;
				Area area = null;
				if (cumplimiento.UidArea.HasValue)
				{
					area = aAreaRep.Find(cumplimiento.UidArea.Value);
					areaNombre = area.StrNombre;
					departamento = DepartamentoRep.Encontrar(area.UidDepartamento);
				}
				else
				{
					departamento = DepartamentoRep.Encontrar(cumplimiento.UidDepartamento.Value);
				}
				Sucursal sucursal = SucursalRep.Find(departamento.UidSucursal);
				Empresa empresa = EmpresaRep.Find(sucursal.UidEmpresa);

				string asunto = "Notificacion de Cumplimiento - " + empresa.StrNombreComercial + " - " + sucursal.StrNombre;

				var modelo = new
				{
					Asunto = asunto,
					Empresa = empresa.StrNombreComercial,
					Sucursal = sucursal.StrNombre,
					Departamento = departamento.StrNombre,
					Area = areaNombre,
					Tarea = tarea.StrNombre,
					Folio = "Tarea: " + tarea.IntFolio.ToString("0000") + " - Cumplimiento: " + cumplimiento.IntFolio.ToString("0000"),
					FechaCumplimiento = cumplimiento.BlAtrasada.Value ? cumplimiento.DtFechaAtrasada.Value.ToString("dd/MM/yyyy HH:mm:ss") + " (Atrasada)" : cumplimiento.DtFechaHora.Value.ToString("dd/MM/yyyy HH:mm:ss"),
					Valor = mensajeNotificacion.StrResultado,
					Observaciones = cumplimiento.StrObservacion,
				};

				List<Usuario> usuarios = UsuarioRep.ObtenerCorreos(cumplimiento.UidCumplimiento);
				if (usuarios.Count == 0)
					return;

				List<MailAddress> mails = usuarios.Where(x => IsValidEmail(x.STRCORREO)).Select(x => new MailAddress(x.STRCORREO, x.StrNombreCompleto)).ToList();
				MailAddressCollection mailAddresses = new MailAddressCollection();
				foreach (var mail in mails)
				{
					mailAddresses.Add(mail);
				}

				PageRender pageRender = PageRender.Instance();
				string content = pageRender.RenderViewToString("Notificacion.cshtml", modelo);

				Correo correo = new Correo();

				correo.SendEmail(mailAddresses, asunto, content);
			}
		}

		public void GetSupervisorDepartamento(Guid UidDepartamento, DateTime DtToday)
		{
			Usuario.Repository UsuarioRep = new Usuario.Repository();
			uUsuario = UsuarioRep.GetSupervisorDepartamento(UidDepartamento, DtToday);
		}

		private static bool IsValidEmail(string email)
		{
			try
			{
				var addr = new MailAddress(email);
				return addr.Address == email;
			}
			catch
			{
				return false;
			}
		}
		#endregion
	}
}