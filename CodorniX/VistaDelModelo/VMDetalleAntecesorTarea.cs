﻿using System;
using CodorniX.Modelo;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
	public class VMDetalleAntecesorTarea
	{
		#region Properties
		private TareaAntecesora.Repository TareaARepository = new TareaAntecesora.Repository();
		Departamento.Repository DepartamentoRepository = new Departamento.Repository();
		Area.Repository AreaRepository = new Area.Repository();
		public Tarea tTarea
		{
			get;
			set;
		}
		public Departamento dDepartamento
		{
			get;
			set;
		}
		public Area aArea
		{
			get;
			set;
		}
		public Periodicidad pPeriodicidad
		{
			get;
			set;
		}
		public TipoFrecuencia tfFrecuencia
		{
			get;
			set;
		}
		public List<TareaAntecesora> LsTareas
		{
			get;
			set;
		}
		public List<TareaAntecesora> LsTareasRelacionadas
		{
			get;
			set;
		}
		public List<Departamento> LsDepartamentos
		{
			get;
			set;
		}
		public List<Area> LsAreas
		{
			get;
			set;
		}
		public List<TipoTarea> LsTipoTarea
		{
			get;
			set;
		}
		public List<Status> LsEstatus
		{
			get;
			set;
		}
		public List<Medicion> LsMediciones
		{
			get;
			set;
		}
		#endregion

		public VMDetalleAntecesorTarea()
		{

		}

		#region Methods
		public void BusquedaTareas(string TipoTarea, string Nombre, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidSucursal, string Departamentos, string Areas, int Folio)
		{
			this.LsTareas = TareaARepository.Busqueda(TipoTarea, Nombre, DtFechaInicio, DtFechaFin, UidSucursal, Departamentos, Areas, Folio);
		}

		public bool Actualizar(Guid UidAntecesor, int? Repeticion, bool UsarNotificacion, int DiasDespues)
		{
			return TareaARepository.Update(UidAntecesor, Repeticion, UsarNotificacion, DiasDespues);
		}

		public void GetDepartamentosSucursal(Guid UidSucursal)
		{
			this.LsDepartamentos = DepartamentoRepository.TodosSucursal(UidSucursal);
			//if (LsDepartamentos == null)
			//	LsDepartamentos = new List<Departamento>();
			//this.LsDepartamentos.Insert(0, new Departamento() { UidDepartamento = Guid.Empty, StrNombre = "Todos" });
		}

		public void GetAreasSucursal(Guid UidSucursal)
		{
			this.LsAreas = AreaRepository.FindAllSucursal(UidSucursal);
			//if (LsAreas == null)
			//	LsAreas = new List<Area>();
			//LsAreas.Insert(0, new Area() { UidArea = Guid.Empty, StrNombre = "Todos" });
		}

		public void GetTarea(Guid UidTarea)
		{
			Tarea.Repositorio tTareaRepositorio = new Tarea.Repositorio();
			this.tTarea = tTareaRepositorio.Encontrar(UidTarea);
		}

		public void GetDepartamento(Guid UidDepartamento)
		{
			this.dDepartamento = DepartamentoRepository.Encontrar(UidDepartamento);
		}

		public void GetArea(Guid UidArea)
		{
			this.aArea = AreaRepository.Find(UidArea);
		}

		public void GetPeriodicidad(Guid UidPeriodicidad)
		{
			Periodicidad.Repositorio PeriodicidadRep = new Periodicidad.Repositorio();
			this.pPeriodicidad = PeriodicidadRep.ConsultarPeriodicidad(UidPeriodicidad);
		}

		public void GetFrecuenciaPeriodicidad(Guid UidTarea)
		{
			TipoFrecuencia.Repositorio TFRepository = new TipoFrecuencia.Repositorio();
			this.tfFrecuencia = TFRepository.FindByTarea(UidTarea
);
		}

		public void GetTiposTarea()
		{
			TipoTarea.Repositorio TTRepository = new TipoTarea.Repositorio();
			this.LsTipoTarea = TTRepository.ConsultarTipoTarea();
		}

		public void GetEstadosTarea()
		{
			Status.Repository SRepository = new Status.Repository();
			this.LsEstatus = SRepository.FindAll();
		}

		public void GetMedicion()
		{
			Medicion.Repositorio MRepository = new Medicion.Repositorio();
			this.LsMediciones = MRepository.ConsultarMedicion();
		}

		/// <summary>
		/// Obtener tareas sucesoras mediante el Identificador de una tarea
		/// </summary>
		public void GetTareasSucesoras(Guid UidTarea)
		{
			this.LsTareasRelacionadas = TareaARepository.GetSucesorasTarea(UidTarea);
		}

		/// <summary>
		/// Obtener tareas antecesoras mediante el Identificador de una tarea
		/// </summary>
		public void GetTareasAntecesoras(Guid UidTarea)
		{
			this.LsTareasRelacionadas = TareaARepository.GetAntecesorasTarea(UidTarea);
		}
		#endregion
	}
}