﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMIniciarTurno
	{
		private IniciarTurno.Repositorio IniciarTunoRepositorio = new IniciarTurno.Repositorio();
		private TareasNoCumplidas.Repositorio TareasNoCumplidasRepositorio = new TareasNoCumplidas.Repositorio();
		private Periodo.Repository PeriodoRepositorio = new Periodo.Repository();
		private Cumplimiento.Repository cumplimientoRepository = new Cumplimiento.Repository();
		private Usuario.Repository usuarioRepository = new Usuario.Repository();
		private Perfil.Repositorio perfilRepository = new Perfil.Repositorio();

		private EstadoTurno.Repository estadoRepository = new EstadoTurno.Repository();

		private IniciarTurno _CIniciarTurno;
		public IniciarTurno CIniciarTurno
		{
			get { return _CIniciarTurno; }
			set { _CIniciarTurno = value; }
		}

		private IniciarTurno _CInicioTurno;
		public IniciarTurno CInicioTurno
		{
			get { return _CInicioTurno; }
			set { _CInicioTurno = value; }
		}

		private IniciarTurno _CIniciarTurno2;
		public IniciarTurno CIniciarTurno2
		{
			get { return _CIniciarTurno2; }
			set { _CIniciarTurno2 = value; }
		}

		private Periodo _CPeriodo;
		public Periodo CPeriodo
		{
			get { return _CPeriodo; }
			set { _CPeriodo = value; }
		}


		private TareasNoCumplidas _CTareasNoCumplidas;

		public TareasNoCumplidas CTareasNoCumplidas
		{
			get { return _CTareasNoCumplidas; }
			set { _CTareasNoCumplidas = value; }
		}

		private List<TareasNoCumplidas> _ltsDepartamento;

		public List<TareasNoCumplidas> ltsDepartamento
		{
			get { return _ltsDepartamento; }
			set { _ltsDepartamento = value; }
		}

		private List<Cumplimiento> _Cumplimientos;

		public List<Cumplimiento> Cumplimientos
		{
			get { return _Cumplimientos; }
			set { _Cumplimientos = value; }
		}

		private Usuario _Encargado;

		public Usuario Encargado
		{
			get { return _Encargado; }
			set { _Encargado = value; }
		}

		private Perfil _perfil;

		public Perfil Perfil
		{
			get { return _perfil; }
			set { _perfil = value; }
		}

		private EstadoTurno _EstadoTurno;

		public EstadoTurno EstadoTurno
		{
			get { return _EstadoTurno; }
			set { _EstadoTurno = value; }
		}


		public bool Guardar(Guid uidusuario, DateTimeOffset fecha, Guid UidPeriodo, bool isEncargado, bool isSupervisor)
		{
			CIniciarTurno = new IniciarTurno()
			{
				UidUsuario = uidusuario,
				DtFechaHoraInicio = fecha,
				UidPeriodo = UidPeriodo,
				UidCreador = uidusuario,
				BlCumplimientoPospterior = false
			};
			bool resultado = false;
			try
			{
				resultado = CIniciarTurno.GuardarDatos(isEncargado, isSupervisor);
			}
			catch (Exception)
			{

				throw;
			}
			return resultado;
		}

		public bool Modificar(Guid UidInicioTurno, DateTimeOffset? fechaFin, string nocompletado,bool BlCPosterior)
		{

			CIniciarTurno = new IniciarTurno()
			{
				UidInicioTurno = UidInicioTurno,
				IntNoCompleto = 0,
				DtFechaHoraFin = fechaFin.HasValue ? fechaFin.Value : (DateTimeOffset?)null,
				BlCumplimientoPospterior = BlCPosterior
			};
			bool Resultado = false;
			try
			{
				Resultado = CIniciarTurno.ModificarDatos();
			}
			catch (Exception)
			{

				throw;
			}
			return Resultado;
		}

		public void ObtenerInicioTurno(Guid inicioturno)
		{
			_CIniciarTurno = IniciarTunoRepositorio.ObtenerInicioTurno(inicioturno);
		}

		public void ObtenerNoCumplidos(Guid UidSucursal, Guid UidUsuario, DateTime DtFecha)
		{
			//DateTime DtFecha = Convert.ToDateTime(DateTime.ParseExact(Fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			_CTareasNoCumplidas = TareasNoCumplidasRepositorio.ObtenerNoCumplimiento(UidSucursal, UidUsuario, DtFecha);
		}

		public void ObtenerCumplimiento(Guid UidSucursal, Guid UidUsuario, DateTime Fecha, int? FolioTurno, Guid? UidEstadoTurno)
		{
			_ltsDepartamento = TareasNoCumplidasRepositorio.ConsultarCumplimiento(UidUsuario, UidSucursal, Fecha, FolioTurno, UidEstadoTurno);
		}

		public void ObtenerInicioPorPeriodo(Guid usuario, Guid periodo, DateTime fecha)
		{
			_CIniciarTurno2 = IniciarTunoRepositorio.ObtenerInicioPorPeriodo(usuario, periodo, fecha);
		}
		public void ObtenerAsignacion(Guid UidUsuario)
		{
			_CPeriodo = PeriodoRepositorio.BuscarPeriodo(UidUsuario);
		}

		public void ObtenerCumplimientos(Guid uidUsuario, Guid uidSucursal, DateTime fecha, string periodos)
		{
			_Cumplimientos = cumplimientoRepository.FindByUser(uidUsuario, uidSucursal, fecha, periodos,0, null, null, Guid.Empty, Guid.Empty, Guid.Empty);
		}

		public void ObtenerUsuario(Guid uid)
		{
			_Encargado = usuarioRepository.Find(uid);
		}
		public void ObtenerPeriodoTurno(Guid UidPeriodo)
		{
			_CPeriodo = PeriodoRepositorio.ObtenerPeriodoTurno(UidPeriodo);
		}

		public void ObtenerTurnoUsuario(Guid UidPeriodo, DateTime DtFecha)
		{
			//DateTime DtFecha = Convert.ToDateTime(DateTime.ParseExact(Fecha, "dd/MM/yyyy", CultureInfo.InvariantCulture));
			_CIniciarTurno = IniciarTunoRepositorio.ObtenerTurnoUsuario(UidPeriodo, DtFecha);
		}

		public void ModificarEstadoTurno(Guid uidTurno, string estado)
		{
			IniciarTunoRepositorio.ModificarEstado(uidTurno, estado);
		}

		public void ObtenerPerfil(Guid uidPerfil)
		{
			_perfil = perfilRepository.CargarDatos(uidPerfil);
		}


		public int ObtenerNumeroTareasSinEstado(Guid UidUsuario, Guid UidPeriodo, DateTime fecha)
		{
			return TareasNoCumplidasRepositorio.NumeroTareasSinEstado(UidPeriodo, UidUsuario, fecha);
		}

		public void ObtenerEstadoTurno(Guid uid)
		{
			_EstadoTurno = estadoRepository.Find(uid);
		}

		public bool ValidarCirreInicioTurnoAnteriorDepartamento(Guid? uidInicioTurno, DateTime date, Guid uidDepartamento)
		{
			IniciarTurno Aux = new IniciarTurno();
			Aux = IniciarTunoRepositorio.ObtenerUltimoPorDepartamento(uidInicioTurno, date, uidDepartamento);

			if (Aux != null)
			{
				if (Aux.DtFechaHoraFin == null)
				{
					return false;
				}
				else
				{
					return true;
				}
			}
			else
			{
				return true;
			}
		}

		public DateTime? ObtenerFechaUltimoTurnoPendienteCerrar(DateTime date, Guid uidDepartamento)
		{
			IniciarTurno Aux = new IniciarTurno();
			Aux = IniciarTunoRepositorio.ObtenerUltimoPorDepartamento(null, date, uidDepartamento);

			if (Aux != null)
			{
				if (Aux.DtFechaHoraFin == null)
				{
					return Aux.DtFechaHoraInicio.DateTime;
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}


		public int ObtenerTotalNotificacionesNoRevisadas(Guid UidSucursal, DateTime DtFecha)
		{
			int Result = 0;
			MensajeNotificacion.Repository repository = new MensajeNotificacion.Repository();
			Result = repository.ObtenerTotalNotificacionesNoRevisadas(UidSucursal, DtFecha);
			return Result;
		}
	}
}