﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMHistoricoTarea
	{
		#region Properties
		Cumplimiento.Repository CumplimientoRepositorio = new Cumplimiento.Repository();
		Tarea.Repositorio TareaRepository = new Tarea.Repositorio();
		Departamento.Repository DepartamentoRepository = new Departamento.Repository();
		Area.Repository AreaRepository = new Area.Repository();
		Periodicidad.Repositorio PeriodicidadRepository = new Periodicidad.Repositorio();
		TipoFrecuencia.Repositorio TFRepository = new TipoFrecuencia.Repositorio();
		TareaOpcion.Repositorio TORepository = new TareaOpcion.Repositorio();
		public Tarea _Tarea
		{
			get;
			set;
		}
		public Departamento _Departamento
		{
			get;
			set;
		}
		public Area _Area
		{
			get;
			set;
		}
		public Periodicidad _Periodicidad
		{
			get;
			set;
		}
		public TipoFrecuencia _Frecuencia
		{
			get;
			set;
		}
		public Cumplimiento _Cumplimiento
		{
			get;
			set;
		}
		public List<Tarea> LsTareas
		{
			get;
			set;
		}
		public List<Cumplimiento> LsCumplimientosTarea
		{
			get;
			set;
		}
		public List<TareaOpcion> LsOpcionesTarea
		{
			get;
			set;
		}
		public List<EstadoCumplimiento> LsEstadosCumplimiento
		{
			get;
			set;
		}
		#endregion

		public VMHistoricoTarea()
		{

		}

		#region Methods
		public void GetEstadosCumplimiento()
		{
			EstadoCumplimiento.Repository ECRepository = new EstadoCumplimiento.Repository();
			this.LsEstadosCumplimiento = ECRepository.FindAll();
			this.LsEstadosCumplimiento.Insert(0, new EstadoCumplimiento() { StrEstadoCumplimiento = "Todos", UidEstadoCumplimiento = Guid.Empty });
		}
		/// <summary>
		/// Obtener tareas asignardas al departamento del encargado
		/// </summary>
		/// <param name="UidDepartamentos"></param>
		/// <param name="Folio"></param>
		/// <param name="Descripcion"></param>
		public void GetTareasDepartamento(List<Guid> UidDepartamentos, int? Folio, string Descripcion)
		{
			string Ids = string.Empty;
			foreach (Guid item in UidDepartamentos)
			{
				if (Ids == string.Empty)
					Ids = item.ToString();
				else
					Ids = "," + item.ToString();
			}
			this.LsTareas = TareaRepository.FindByDepartamentos(Ids, Folio, Descripcion);
		}

		/// <summary>
		/// Obtener datos de tarea mediante su identificador
		/// </summary>
		/// <param name="UidTarea"></param>
		public void GetTarea(Guid UidTarea)
		{
			this._Tarea = TareaRepository.Encontrar(UidTarea);
		}
		/// <summary>
		/// Obtener departamento mediante su identificador
		/// </summary>
		/// <param name="UidDepartamento"></param>
		public void GetDepartamento(Guid UidDepartamento)
		{
			this._Departamento = DepartamentoRepository.Encontrar(UidDepartamento);
		}
		/// <summary>
		/// Obtener datos de area mediante su identificador
		/// </summary>
		/// <param name="UidArea"></param>
		public void GetArea(Guid UidArea)
		{
			this._Area = AreaRepository.Find(UidArea);
		}
		/// <summary>
		/// Obtener detalles de periodicidad mediante su identificador
		/// </summary>
		/// <param name="UidPeriodicidad"></param>
		public void GetPeriodicidad(Guid UidPeriodicidad)
		{
			this._Periodicidad = PeriodicidadRepository.ConsultarPeriodicidad(UidPeriodicidad);
		}
		/// <summary>
		/// Obtener tipo de frecuencia mediante el identificador
		/// </summary>
		/// <param name="UidTarea"></param>
		public void GetTipoFrecuenciaByTarea(Guid UidTarea)
		{
			this._Frecuencia = TFRepository.FindByTarea(UidTarea);
		}
		/// <summary>
		/// Obtener lista de cumplimientos de la tarea seleccionada
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <param name="UidUsuario"></param>
		public void GetCumplimientoTarea(Guid UidTarea, Guid UidUsuario, int? FolioCumplimiento, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidEstado)
		{
			this.LsCumplimientosTarea = CumplimientoRepositorio.GetCumplimientosTarea(UidTarea, UidUsuario, FolioCumplimiento, DtFechaInicio, DtFechaFin, UidEstado);
		}
		/// <summary>
		/// Obtener detalles del cumplimiento mediante el identificador
		/// </summary>
		/// <param name="UidCumplimiento"></param>
		public void GetCumplimientoById(Guid UidCumplimiento)
		{
			this._Cumplimiento = CumplimientoRepositorio.Find(UidCumplimiento);
		}

		/// <summary>
		/// Obtener opciones tarea
		/// </summary>
		/// <param name="UidTarea"></param>
		public void GetOpcionesTarea(Guid UidTarea)
		{
			this.LsOpcionesTarea = TORepository.Buscar(UidTarea);
		}
		#endregion
	}
}