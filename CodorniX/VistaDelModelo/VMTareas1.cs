﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.VistaDelModelo
{
	public class VMTareas1
	{
		#region Propiedades

		private UnidadMedida.Repositorio UnidadMedidaRepositorio = new UnidadMedida.Repositorio();
		private Departamento.Repository DepartamentoRepositorio = new Departamento.Repository();
		private Tarea.Repositorio TareaRepositorio = new Tarea.Repositorio();
		private Meses.Repositorio MesesRepositorio = new Meses.Repositorio();
		private Dias.Repositorio DiasRepositorio = new Dias.Repositorio();
		private Ordinal.Repositorio OrdinalRepositorio = new Ordinal.Repositorio();
		private DepartamentoTarea.Repositorio DepartamentoTareaRepositorio = new DepartamentoTarea.Repositorio();
		private Medicion.Repositorio MedicionRepositorio = new Medicion.Repositorio();
		private List<UnidadMedida> _ltsUnidadMedida;
		private TareaOpcion.Repositorio TareaOpcionRepositorio = new TareaOpcion.Repositorio();
		private Status.Repository StatusRepositorio = new Status.Repository();
		private TipoTarea.Repositorio TipoTareaRepositorio = new TipoTarea.Repositorio();
		private Encargado.Repository EncargadoRepositorio = new Encargado.Repository();
		private Area.Repository AreaRepositorio = new Area.Repository();
		private AreaTarea.Repositorio AreaTareaRepositorio = new AreaTarea.Repositorio();
		private Cumplimiento.Repository cumplimientorepositorio = new Cumplimiento.Repository();
		private Antecesor.Repository antecesorRepository = new Antecesor.Repository();
		private Notificacion.Repository notificacionRepository = new Notificacion.Repository();

		private List<Encargado> _ltsEncargaddo;
		public List<Encargado> ltsEncargado
		{
			get { return _ltsEncargaddo; }
			set { _ltsEncargaddo = value; }
		}


		private List<TareaOpcion> _Opciones;
		public List<TareaOpcion> Opciones
		{
			get { return _Opciones; }
			set { _Opciones = value; }
		}


		public List<UnidadMedida> ltsUnidadMedida
		{
			get { return _ltsUnidadMedida; }
			set { _ltsUnidadMedida = value; }
		}

		private List<Meses> _ltsMeses;
		public List<Meses> ltsMeses
		{
			get { return _ltsMeses; }
			set { _ltsMeses = value; }
		}

		private List<Medicion> _ltsMedicion;
		public List<Medicion> ltsMedicion
		{
			get { return _ltsMedicion; }
			set { _ltsMedicion = value; }
		}

		private List<Status> _ltsStatus;
		public List<Status> ltsStatus
		{
			get { return _ltsStatus; }
			set { _ltsStatus = value; }
		}


		private List<Departamento> _lstDepartamento;
		public List<Departamento> ltsDepartamento
		{
			get { return _lstDepartamento; }
			set { _lstDepartamento = value; }
		}

		public List<Area> LsAreas
		{
			get;
			set;
		}

		private List<Area> _ltsArea;
		public List<Area> ltsArea
		{
			get { return _ltsArea; }
			set { _ltsArea = value; }
		}

		private List<Departamento> _ltsDepartamento2;
		public List<Departamento> ltsDepartamento2
		{
			get { return _ltsDepartamento2; }
			set { _ltsDepartamento2 = value; }
		}

		private List<Departamento> _Departamentos;
		public List<Departamento> Departamentos
		{
			get { return _Departamentos; }
			set { _Departamentos = value; }
		}

		private List<Area> _Areas;
		public List<Area> Areas
		{
			get { return _Areas; }
			set { _Areas = value; }
		}


		private List<TipoTarea> _ltsTipoTarea;
		public List<TipoTarea> ltsTipoTarea
		{
			get { return _ltsTipoTarea; }
			set { _ltsTipoTarea = value; }
		}

		private Area _CArea;
		public Area CArea
		{
			get { return _CArea; }
			set { _CArea = value; }
		}


		private Departamento _CDepartamento;
		public Departamento CDepartamento
		{
			get { return _CDepartamento; }
			set { _CDepartamento = value; }
		}

		private List<Tarea> _ltsTarea;
		public List<Tarea> ltsTarea
		{
			get { return _ltsTarea; }
			set { _ltsTarea = value; }
		}
		private List<Dias> _ltsDias;
		public List<Dias> ltsDias
		{
			get { return _ltsDias; }
			set { _ltsDias = value; }
		}

		private List<Ordinal> _ltsOrdinal;
		public List<Ordinal> ltsOrdinal
		{
			get { return _ltsOrdinal; }
			set { _ltsOrdinal = value; }
		}


		private Tarea _CTarea;
		public Tarea CTarea
		{
			get { return _CTarea; }
			set { _CTarea = value; }
		}

		private DepartamentoTarea _CDepartamentoTarea;
		public DepartamentoTarea CDepartamentoTarea
		{
			get { return _CDepartamentoTarea; }
			set { _CDepartamentoTarea = value; }
		}

		private Tarea _CAntecesor;
		public Tarea CAntecesor
		{
			get { return _CAntecesor; }
			set { _CAntecesor = value; }
		}

		private Antecesor _Antecesor;
		public Antecesor Antecesor
		{
			get { return _Antecesor; }
			set { _Antecesor = value; }
		}

		private Notificacion _Notificacion;
		public Notificacion Notificacion
		{
			get { return _Notificacion; }
			set { _Notificacion = value; }
		}
		#endregion

		#region Consultas

		public void ConsultarUnidadMedida(Guid UidEmpresa)
		{
			_ltsUnidadMedida = UnidadMedidaRepositorio.Busqueda(UidEmpresa, string.Empty, true);
		}

		public void ConsultarMedicion()
		{
			_ltsMedicion = MedicionRepositorio.ConsultarMedicion();
		}

		public void ConsultarTipoTarea()
		{
			_ltsTipoTarea = TipoTareaRepositorio.ConsultarTipoTarea();
		}
		public void ConsultarStatus()
		{
			_ltsStatus = StatusRepositorio.FindAll();
		}
		public void ConsultarDepartamento(Guid sucursal)
		{
			Departamento.Criterio criterio = new Departamento.Criterio()
			{
				Sucursal = sucursal
			};
			ltsDepartamento = DepartamentoRepositorio.EncontrarPor(criterio);
		}
		public void GetAreasSucursal(Guid UidSucursal)
		{
			Area.Repository AreaRepository = new Area.Repository();
			this.LsAreas = AreaRepository.FindAllSucursal(UidSucursal);
			//if (LsAreas == null)
			//	LsAreas = new List<Area>();
			//LsAreas.Insert(0, new Area() { UidArea = Guid.Empty, StrNombre = "Todos" });
		}
		public void ConsultarEncargado(Guid sucursal)
		{
			ltsEncargado = EncargadoRepositorio.FindByName(string.Empty, string.Empty, string.Empty, sucursal.ToString());
		}

		public void ConsultarMeses()
		{
			_ltsMeses = MesesRepositorio.ConsultarMeses();
		}

		public void ConsultarDias()
		{
			ltsDias = DiasRepositorio.ConsultarDias();
		}

		public void ConsultarOrdinal()
		{
			ltsOrdinal = OrdinalRepositorio.ConsultarOrdinal();
		}

		public void BuscarDepartamento(string nombre, Guid sucursal)
		{
			Departamento.Criterio criterio = new Departamento.Criterio()
			{
				Nombre = nombre,
				Sucursal = sucursal
			};

			_lstDepartamento = DepartamentoRepositorio.EncontrarPor(criterio);
		}

		public void ObtenerDepartamentos()
		{
			_Departamentos = DepartamentoTareaRepositorio.FindAll(_CTarea.UidTarea);
		}

		public void ObtenerAreas()
		{
			_Areas = AreaTareaRepositorio.FindAll(_CTarea.UidTarea);
		}

		public void ObtenerDepartamento(Guid Departamento)
		{
			_CDepartamento = DepartamentoRepositorio.Encontrar(Departamento);
		}

		public void ObtenerArea(Guid Area)
		{
			_CArea = AreaRepositorio.Find(Area);
		}
		public void ObtenerListaArea(Guid UidDepartamento)
		{
			_ltsArea = AreaRepositorio.FindAll(UidDepartamento);
		}
		public void departamentoseleccionado(Guid Departamento)
		{
			_ltsDepartamento2 = DepartamentoRepositorio.Encontrardepartamento(Departamento);
		}

		public void BuscarAntecesor(Guid UidSuc, string nombre)
		{
			Tarea.Criterio criterio = new Tarea.Criterio()
			{
				Nombre = nombre,
				UidSucursal = UidSuc
			};
			_ltsTarea = TareaRepositorio.Buscar(criterio);
		}

		public void CargarAntecesor(string UidAntecesor)
		{
			_CAntecesor = TareaRepositorio.Encontrar(new Guid(UidAntecesor));
		}


		public void ObtenerTarea(Guid Tarea)
		{
			_CTarea = TareaRepositorio.Encontrar(Tarea);
		}

		public int CantidadCumplimientosTarea(Guid UidTarea)
		{
			return TareaRepositorio.ObtenerCantidadCumplimientosTarea(UidTarea);
		}

		public void ObtenerTareaDepartamento(string UidTarea)
		{
			_CDepartamentoTarea = DepartamentoTareaRepositorio.Buscar(new Guid(UidTarea));
		}

		public void CargarTarea(Guid uidsucursal)
		{
			_ltsTarea = TareaRepositorio.CargarTarea(uidsucursal);
		}

		public void BuscarTarea(int FolioTarea, string Nombre, DateTime? fecha1, DateTime? fecha2, Guid UidSucursal, string Departamento, string Areas, string Encargado, int folio)
		{
			_ltsTarea = TareaRepositorio.BuscarTarea(FolioTarea, Nombre, fecha1, fecha2, UidSucursal, Departamento, Areas, Encargado, folio);
		}


		#endregion

		public bool GuardarTarea(string Nombre, string Descripcion, string UidAntecesor,
			string UidUnidadMedida, Guid UidPeriodicidad, string UidMedicion,
			string Hora, string Tolerancia, string TipoTarea, string Status, bool foto, bool Caducado, Guid sucursal)
		{
			CTarea = new Tarea()
			{
				StrNombre = Nombre,
				StrDescripcion = Descripcion,
				UidAntecesor = UidAntecesor.Length == 0 ? (Guid?)null : new Guid(UidAntecesor),
				UidUnidadMedida = UidUnidadMedida.Length == 0 ? (Guid?)null : new Guid(UidUnidadMedida),
				UidPeriodicidad = UidPeriodicidad,
				UidMedicion = new Guid(UidMedicion),
				TmHora = Hora.Length == 0 ? (TimeSpan?)null : TimeSpan.Parse(Hora),
				IntTolerancia = Tolerancia.Length == 0 ? (int?)null : Convert.ToInt32(Tolerancia),
				UidTipoTarea = new Guid(TipoTarea),
				UidStatus = new Guid(Status),
				BlFoto = foto,
				BlCaducado = Caducado,
				BlAutorizado = true,
				BlCreadoSupervisor = false
			};
			bool Resultado = false;
			try
			{
				Resultado = CTarea.GuardarDatos(sucursal);
			}
			catch (Exception)
			{

				throw;
			}
			return Resultado;
		}
		public bool GuardarDepartamentoTarea(string Departamento, Guid Tarea)
		{
			CDepartamentoTarea = new DepartamentoTarea()
			{
				UidDepartamento = new Guid(Departamento),
				UidTarea = Tarea,
			};
			bool Resultado = false;
			try
			{
				//Resultado = CDepartamentoTarea.GuardarDatos();
			}
			catch (Exception)
			{

				throw;
			}
			return Resultado;
		}

		public void GuardarDepartamentosTareas(List<Departamento> departamentos, Guid uidTarea)
		{
			foreach (Departamento departamento in departamentos)
			{
				DepartamentoTarea departamentotarea = new DepartamentoTarea();
				departamentotarea.UidTarea = uidTarea;
				departamentotarea.UidDepartamento = departamento.UidDepartamento;
				DepartamentoTareaRepositorio.Save(departamentotarea);
			}
		}
		public void ActualizarCumplimiento(Guid uidTarea, Guid TareaAnterior)
		{
			cumplimientorepositorio.actualizarcumplimiento(uidTarea, TareaAnterior);
		}

		public void GuardarAreaTareas(List<Area> areas, Guid uidTarea)
		{
			foreach (Area area in areas)
			{
				AreaTarea areatarea = new AreaTarea();
				areatarea.UidTarea = uidTarea;
				areatarea.UidArea = area.UidArea;
				AreaTareaRepositorio.Save(areatarea);
			}
		}

		public bool ModificarTarea(string UidTarea, string Nombre, string Descripcion,
			string hora, string Tolerancia, string TipoTarea, string Status, bool foto, DateTime? DtNuevaFecha)
		{

			CTarea = new Tarea()
			{
				UidTarea = new Guid(UidTarea),

				StrNombre = Nombre,
				StrDescripcion = Descripcion,
				IntTolerancia = Tolerancia.Length == 0 ? (int?)null : Convert.ToInt32(Tolerancia),
				TmHora = hora.Length == 0 ? (TimeSpan?)null : TimeSpan.Parse(hora),
				UidTipoTarea = new Guid(TipoTarea),
				UidStatus = new Guid(Status),
				BlFoto = foto,
				DtNuevaFecha = DtNuevaFecha
			};
			bool Resultado = false;
			try
			{
				Resultado = CTarea.ModificarDatos();
			}
			catch (Exception)
			{

				throw;
			}
			return Resultado;
		}

		public bool ModificarCaducado(Guid UidTarea, bool caducado)
		{

			CTarea = new Tarea()
			{
				UidTarea = UidTarea,

				BlCaducado = caducado
			};
			bool Resultado = false;
			try
			{
				Resultado = CTarea.ModificarCaducado();
			}
			catch (Exception)
			{

				throw;
			}
			return Resultado;
		}

		public void EliminarDepartamentos(List<Departamento> departamentoTareas)
		{
			foreach (DepartamentoTarea departamentotarea in departamentoTareas)
			{
				DepartamentoTareaRepositorio.Remove(departamentotarea);
			}
		}

		public void ObtenerOpciones()
		{
			_Opciones = TareaOpcionRepositorio.Buscar(_CTarea.UidTarea);
		}

		public void ObtenerOpciones(Guid uidTarea)
		{
			_Opciones = TareaOpcionRepositorio.Buscar(uidTarea);
		}

		public void GuardarOpciones(List<TareaOpcion> opciones, Guid UidTarea)
		{
			foreach (TareaOpcion opcion in opciones)
			{
				opcion.UidTarea = UidTarea;
				TareaOpcionRepositorio.Guardar(opcion);
			}
		}

		public void ObtenerNotificacion(Guid uidTarea)
		{
			_Notificacion = notificacionRepository.Get(uidTarea);
		}

		public void GuardarNotificacion(Notificacion notificacion)
		{
			notificacionRepository.Set(notificacion);
		}

		public void DeshabilitarNotificacion(Guid UidTarea)
		{
			notificacionRepository.Disable(UidTarea);
		}

		public void ObtenerAntecesor(Guid uidTarea)
		{
			_Antecesor = antecesorRepository.Get(uidTarea);
		}

		public void GuardarAntecesor(Antecesor antecesor)
		{
			antecesorRepository.Set(antecesor);
		}

		public bool HabilitarTareaCreadaSupervisor(Guid UidTarea, Guid UidSucursal)
		{
			return TareaRepositorio.HabilitarTareaSupervisor(UidTarea, UidSucursal);
		}

		/// <summary>
		/// Actualizar las asignaciones correspondientes de la tarea (Departamentos)
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <param name="LsDepartamentos"></param>
		public void ActualizarAsignacionDepartamentos(Guid UidTarea, List<Departamento> LsDepartamentos)
		{
			DepartamentoTarea DT = new DepartamentoTarea();
			foreach (Departamento _Depto in LsDepartamentos)
			{
				if (_Depto.IsSaved && _Depto.ToDelete)
					DepartamentoTareaRepositorio.DeleteAssignment(UidTarea, _Depto.UidDepartamento);
				else if (!_Depto.IsSaved)
				{
					DT = new DepartamentoTarea();
					DT.UidTarea = UidTarea;
					DT.UidDepartamento = _Depto.UidDepartamento;
					DepartamentoTareaRepositorio.Save(DT);
				}
			}
		}
		/// <summary>
		/// Actualizar las asignaciones correspondientes de la tarea (Areas)
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <param name="LsAreas"></param>
		public void ActualizarAsignacionAreas(Guid UidTarea, List<Area> LsAreas)
		{
			AreaTarea AT = new AreaTarea();
			foreach (Area _Area in LsAreas)
			{
				if (_Area.IsSaved && _Area.ToDelete)
					AreaTareaRepositorio.DeleteAssignment(UidTarea, _Area.UidArea);
				else if (!_Area.IsSaved)
				{
					AT = new AreaTarea();
					AT.UidTarea = UidTarea;
					AT.UidArea = _Area.UidArea;
					AreaTareaRepositorio.Save(AT);
				}
			}
		}

		/// <summary>
		/// Actualiza la fecha del primer cumplimiento de la tarea seleccionada
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <param name="UidPeriodicidad"></param>
		/// <param name="DtActual"></param>
		public void ActualizarFechaPrimerCumplimientoTarea(Guid UidTarea, Guid UidPeriodicidad, DateTime DtActual)
		{
			DateTime DtNuevaFecha = DtActual;
			Periodicidad.Repositorio periodicidadRepository = new Periodicidad.Repositorio();
			Periodicidad Periodicity = periodicidadRepository.ConsultarPeriodicidad(UidPeriodicidad);

			if (Periodicity.Frecuencia.Equals("Semanal"))
			{
				PeriodicidadSemanal.Repositorio PSRepository = new PeriodicidadSemanal.Repositorio();
				PeriodicidadSemanal PS = PSRepository.ConsultarPeriodicidadSemanal(UidPeriodicidad);

				DtNuevaFecha = PSRepository.CalculateNext(DtActual, true, Periodicity.IntFrecuencia, PS.BlLunes, PS.BlMartes, PS.BlMiercoles, PS.BlJueves, PS.BlViernes, PS.BlSabado, PS.BlDomingo);
			}
			else if (Periodicity.Frecuencia.Equals("Mensual"))
			{
				PeriodicidadMensual.Repositorio PMRepository = new PeriodicidadMensual.Repositorio();
				PeriodicidadMensual PM = PMRepository.ConsultarPeriodicidadMensual(UidPeriodicidad);

				if (PM.Tipo.Equals("A"))
					DtNuevaFecha = PMRepository.CalculateNextTypeA(DtActual, true, PM.DiasMes, Periodicity.IntFrecuencia);
				else if (PM.Tipo.Equals("B"))
				{
					FechaPeriodicidad.Repository FPeriodicidad = new FechaPeriodicidad.Repository();
					List<FechaPeriodicidad> LsFechas = FPeriodicidad.GetAll(UidPeriodicidad,"Mensual");
					DtNuevaFecha = PMRepository.CalculateNextTypeB(DtActual, true, LsFechas, Periodicity.IntFrecuencia);
				}
				else if (PM.Tipo.Equals("C"))
					DtNuevaFecha = PMRepository.CalculateNextTypeC(DtActual, true, PM.IntDiasMes, PM.IntDiasSemana, Periodicity.IntFrecuencia);
			}
			else if (Periodicity.Frecuencia.Equals("Anual"))
			{
				PeriodicidadAnual.Repositorio PARepository = new PeriodicidadAnual.Repositorio();
				PeriodicidadAnual PA = PARepository.ConsultarPeriodicidadAnual(UidPeriodicidad);

				if (PA.Tipo == "A")
				{
					FechaPeriodicidad.Repository FPeriodicidad = new FechaPeriodicidad.Repository();
					List<FechaPeriodicidad> LsFPeriodicidad = FPeriodicidad.GetAll(UidPeriodicidad, "Anual");
					List<DateTime> LsDates = new List<DateTime>();
					foreach (FechaPeriodicidad item in LsFPeriodicidad)
					{
						LsDates.Add(DateTime.Parse(
							item.IntDia.ToString("00") + "/" +
							item.IntNumeroMes.ToString("00") + "/" +
							DtActual.Year.ToString("00")
							));
					}
					PARepository.CalculateNextTypeA(DtActual, false, LsDates, Periodicity.IntFrecuencia);
				}
				else if (PA.Tipo == "B")
					PARepository.CalculateNextTypeB(DtActual, false, PA.IntDiasMes, PA.IntDiasSemanas, PA.IntNumero, Periodicity.IntFrecuencia);
			}

			periodicidadRepository.ActualizarFechaInicioPrimerCumplimiento(UidTarea, DtNuevaFecha);
		}

		/// <summary>
		/// Validar si la tarea seleccionada tiene configurada una notificación
		/// </summary>
		/// <param name="UidTarea"></param>
		/// <returns></returns>
		public bool ValidarNotificacionAntecesor(Guid UidTarea)
		{
			bool Result = false;
			Notificacion nTemp = notificacionRepository.Get(UidTarea);

			if (nTemp == null)
				Result = false;
			else
				Result = true;


			return Result;
		}
	}
}