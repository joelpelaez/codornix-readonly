﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
    public class VMTareasAtrasadas
    {
        TareaAtrasada.Repository atrasadasRepository = new TareaAtrasada.Repository();
        Departamento.Repository departamentoRepository = new Departamento.Repository();
        Area.Repository areaRepository = new Area.Repository();
        Tarea.Repositorio tareaRepository = new Tarea.Repositorio();
        Cumplimiento.Repository cumplimientoRepository = new Cumplimiento.Repository();

        private List<TareaAtrasada> _TareasAtrasadas;

        public List<TareaAtrasada> TareasAtrasadas
        {
            get { return _TareasAtrasadas; }
            set { _TareasAtrasadas = value; }
        }

        private Tarea _Tarea;

        public Tarea Tarea
        {
            get { return _Tarea; }
            set { _Tarea = value; }
        }

        private Departamento _Departamento;

        public Departamento Departamento
        {
            get { return _Departamento; }
            set { _Departamento = value; }
        }

        private List<Departamento> _Departamentos;

        public List<Departamento> Departamentos
        {
            get { return _Departamentos; }
            set { _Departamentos = value; }
        }


        private Area _Area;

        public Area Area
        {
            get { return _Area; }
            set { _Area = value; }
        }

        private Cumplimiento _Cumplimiento;

        public Cumplimiento Cumplimiento
        {
            get { return _Cumplimiento; }
            set { _Cumplimiento = value; }
        }


        public void ObtenerDepartamentosDeSupervision(Guid uidSupervisor, Guid uidSucursal, DateTime fecha)
        {
            _Departamentos = atrasadasRepository.ObtenerDepartamentosDeSupervision(uidSupervisor, uidSucursal, fecha);
        }

        public void ObtenerTareasAtrasadas(Guid uidSupervisor, Guid uidSucursal, DateTime fecha, string departamentos)
        {
            _TareasAtrasadas = atrasadasRepository.Buscar(uidSupervisor, uidSucursal, fecha, departamentos);
        }

        public void ObtenerTarea(Guid uidTarea)
        {
            _Tarea = tareaRepository.Encontrar(uidTarea);
        }

        public void ObtenerDepartamento(Guid uidDepto)
        {
            _Departamento = departamentoRepository.Encontrar(uidDepto);
        }

        public void ObtenerArea(Guid uidArea)
        {
            _Area = areaRepository.Find(uidArea);
        }

        public int PosponerTarea(Guid uidTarea, Guid? uidDepartamento, Guid? uidArea, Guid? uidCumplimiento, DateTime fecha, DateTime nuevaFecha)
        {
            return atrasadasRepository.PosponerTarea(uidTarea, uidDepartamento, uidArea, uidCumplimiento, fecha, nuevaFecha);
        }

        public int CancelarTarea(Guid uidTarea, Guid? uidDepartamento, Guid? uidArea, Guid? uidCumplimiento, DateTime fecha)
        {
            return atrasadasRepository.CancelarTarea(uidTarea, uidDepartamento, uidArea, uidCumplimiento, fecha);
        }

        public DateTime? ObtenerFechaSiguienteTarea(Guid uidTarea, DateTime fecha)
        {
            return cumplimientoRepository.ObtenerSiguienteFecha(uidTarea, fecha);
        }

        public void ObtenerCumplimiento(Guid uidCumplimiento)
        {
            _Cumplimiento = cumplimientoRepository.Find(uidCumplimiento);
        }
    }
}