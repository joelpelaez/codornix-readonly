﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMCalificacion
	{
		#region Properties
		private Ajustes.Repository AjusteRep = new Ajustes.Repository();
		private Calificacion.Repository CalificacionRep = new Calificacion.Repository();
		public Calificacion cCalificacion
		{
			get;
			set;
		}
		public Ajustes aAjuste
		{
			get;
			set;
		}
		public List<Calificacion> LsCalificaciones
		{
			get;
			set;
		}
		#endregion

		public VMCalificacion()
		{

		}

		#region Methods
		public void Search(Guid UidEmpresa, string NombreCalificacion, bool? Estatus, float FlValor)
		{
			this.LsCalificaciones = CalificacionRep.Search(UidEmpresa, NombreCalificacion, Estatus, FlValor);
		}
		public void SortItemList(string SortDirection, string Expression)
		{
			if (Expression.Equals("Calificacion"))
			{
				if (SortDirection.Equals("ASC"))
					this.LsCalificaciones = LsCalificaciones.OrderBy(c => c.StrCalificacion).ToList();
				else
					this.LsCalificaciones = LsCalificaciones.OrderByDescending(c => c.StrCalificacion).ToList();
			}
			else if (Expression.Equals("Valor"))
			{
				if (SortDirection.Equals("ASC"))
					this.LsCalificaciones = LsCalificaciones.OrderBy(c => c.FlValor).ToList();
				else
					this.LsCalificaciones = LsCalificaciones.OrderByDescending(c => c.FlValor).ToList();
			}
			else if (Expression.Equals("Orden"))
			{
				if (SortDirection.Equals("ASC"))
					this.LsCalificaciones = LsCalificaciones.OrderBy(c => c.IntOrden).ToList();
				else
					this.LsCalificaciones = LsCalificaciones.OrderByDescending(c => c.IntOrden).ToList();
			}
			else if (Expression.Equals("Estatus"))
			{
				if (SortDirection.Equals("ASC"))
					this.LsCalificaciones = LsCalificaciones.OrderBy(c => c.BlEstatus).ToList();
				else
					this.LsCalificaciones = LsCalificaciones.OrderByDescending(c => c.BlEstatus).ToList();
			}
		}
		public void GetItemById(Guid UidCalificacion)
		{
			this.cCalificacion = CalificacionRep.FindById(UidCalificacion);
		}

		public bool AddNew(Guid UidEmpresa, string NombreCalificacion, int IntOrden, bool Estatus, float FlValor)
		{
			return CalificacionRep.Add(UidEmpresa, IntOrden, NombreCalificacion, Estatus, FlValor);
		}
		public bool UpdateItem(Guid UidCalificacion, string NombreCalificacion, int IntOrden, bool Estatus, float FlValor)
		{
			return CalificacionRep.Update(UidCalificacion, NombreCalificacion, IntOrden, Estatus, FlValor);
		}

		public void GetAjustes(Guid UidEmpresa)
		{
			this.aAjuste = AjusteRep.GetAjustesRevision(UidEmpresa);
		}
		public bool SetAjustes(Guid UidEmpresa, bool BlCualitativo)
		{
			return AjusteRep.SetAjustesRevision(UidEmpresa, BlCualitativo);
		}
		#endregion
	}
}