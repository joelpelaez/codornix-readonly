﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
	public class VMUnidadMedida
	{
		private UnidadMedida.Repositorio umUnidadRepositoy = new UnidadMedida.Repositorio();
		public UnidadMedida umUnidadMedida
		{
			get;
			set;
		}
		public List<UnidadMedida> LsUnidades
		{
			get;
			set;
		}

		public VMUnidadMedida()
		{

		}

		#region Methods
		public void Search(Guid UidEmpresa, string UnidadMedida, bool? BlEstatus)
		{
			this.LsUnidades = umUnidadRepositoy.Busqueda(UidEmpresa, UnidadMedida, BlEstatus);
		}

		public void Datos(Guid UidUnidadMedida)
		{
			this.umUnidadMedida = umUnidadRepositoy.Find(UidUnidadMedida);
		}

		public bool Agregar(Guid UidEmpresa, string UnidadMedida, bool BlEstatus)
		{
			return umUnidadRepositoy.Add(UnidadMedida, UidEmpresa, BlEstatus);
		}

		public bool Actualizar(Guid UidUnidadMedida, string UnidadMedida, bool BlEstatus)
		{
			return umUnidadRepositoy.Update(UidUnidadMedida, UnidadMedida, BlEstatus);
		}
		#endregion
	}
}