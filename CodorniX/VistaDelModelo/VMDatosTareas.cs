﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;

namespace CodorniX.VistaDelModelo
{
    public class VMDatosTareas
    {

        #region Propiedades

        private UnidadMedida.Repositorio UnidadMedidaRepositorio = new UnidadMedida.Repositorio();
        private Departamento.Repository DepartamentoRepositorio = new Departamento.Repository();
        private Tarea.Repositorio TareaRepositorio = new Tarea.Repositorio();
        private Meses.Repositorio MesesRepositorio = new Meses.Repositorio();
        private Dias.Repositorio DiasRepositorio = new Dias.Repositorio();
        private Ordinal.Repositorio OrdinalRepositorio = new Ordinal.Repositorio();
        private DepartamentoTarea.Repositorio DepartamentoTareaRepositorio = new DepartamentoTarea.Repositorio();
        private Medicion.Repositorio MedicionRepositorio = new Medicion.Repositorio();
        private List<UnidadMedida> _ltsUnidadMedida;
        private TareaOpcion.Repositorio TareaOpcionRepositorio = new TareaOpcion.Repositorio();
        private Status.Repository StatusRepositorio = new Status.Repository();
        private TipoTarea.Repositorio TipoTareaRepositorio = new TipoTarea.Repositorio();

        private List<TareaOpcion> _Opciones;

        public List<TareaOpcion> Opciones
        {
            get { return _Opciones; }
            set { _Opciones = value; }
        }


        public List<UnidadMedida> ltsUnidadMedida
        {
            get { return _ltsUnidadMedida; }
            set { _ltsUnidadMedida = value; }
        }

        private List<Meses> _ltsMeses;

        public List<Meses> ltsMeses
        {
            get { return _ltsMeses; }
            set { _ltsMeses = value; }
        }

        private List<Medicion> _ltsMedicion;

        public List<Medicion> ltsMedicion
        {
            get { return _ltsMedicion; }
            set { _ltsMedicion = value; }
        }

        private List<Status> _ltsStatus;

        public List<Status> ltsStatus
        {
            get { return _ltsStatus; }
            set { _ltsStatus = value; }
        }


        private List<Departamento> _lstDepartamento;

        public List<Departamento> ltsDepartamento
        {
            get { return _lstDepartamento; }
            set { _lstDepartamento = value; }
        }

        private List<Departamento> _ltsDepartamento2;

        public List<Departamento> ltsDepartamento2
        {
            get { return _ltsDepartamento2; }
            set { _ltsDepartamento2 = value; }
        }

        private List<Departamento> _Departamentos;

        public List<Departamento> Departamentos
        {
            get { return _Departamentos; }
            set { _Departamentos = value; }
        }

        private List<TipoTarea> _ltsTipoTarea;

        public List<TipoTarea> ltsTipoTarea
        {
            get { return _ltsTipoTarea; }
            set { _ltsTipoTarea = value; }
        }


        private Departamento _CDepartamento;

        public Departamento CDepartamento
        {
            get { return _CDepartamento; }
            set { _CDepartamento = value; }
        }

        private List<Tarea> _ltsTarea;

        public List<Tarea> ltsTarea
        {
            get { return _ltsTarea; }
            set { _ltsTarea = value; }
        }
        private List<Dias> _ltsDias;

        public List<Dias> ltsDias
        {
            get { return _ltsDias; }
            set { _ltsDias = value; }
        }

        private List<Ordinal> _ltsOrdinal;

        public List<Ordinal> ltsOrdinal
        {
            get { return _ltsOrdinal; }
            set { _ltsOrdinal = value; }
        }


        private Tarea _CTarea;

        public Tarea CTarea
        {
            get { return _CTarea; }
            set { _CTarea = value; }
        }

        private DepartamentoTarea _CDepartamentoTarea;

        public DepartamentoTarea CDepartamentoTarea
        {
            get { return _CDepartamentoTarea; }
            set { _CDepartamentoTarea = value; }
        }

        private Tarea _CAntecesor;

        public Tarea CAntecesor
        {
            get { return _CAntecesor; }
            set { _CAntecesor = value; }
        }



        #endregion

        #region Consultas

        public void ConsultarUnidadMedida(Guid UidEmpresa)
        {
            _ltsUnidadMedida = UnidadMedidaRepositorio.Busqueda(UidEmpresa,string.Empty,true);
        }

        public void ConsultarMedicion()
        {
            _ltsMedicion = MedicionRepositorio.ConsultarMedicion();
        }

        public void ConsultarTipoTarea()
        {
            _ltsTipoTarea = TipoTareaRepositorio.ConsultarTipoTarea();
        }
        public void ConsultarStatus()
        {
            _ltsStatus = StatusRepositorio.FindAll();
        }
        public void ConsultarDepartamento(Guid sucursal)
        {
            Departamento.Criterio criterio = new Departamento.Criterio()
            {
                Sucursal = sucursal
            };
            ltsDepartamento = DepartamentoRepositorio.EncontrarPor(criterio);
        }

        public void ConsultarMeses()
        {
            _ltsMeses = MesesRepositorio.ConsultarMeses();
        }

        public void ConsultarDias()
        {
            ltsDias = DiasRepositorio.ConsultarDias();
        }

        public void ConsultarOrdinal()
        {
            ltsOrdinal = OrdinalRepositorio.ConsultarOrdinal();
        }

        public void BuscarDepartamento(string nombre, Guid sucursal)
        {
            Departamento.Criterio criterio = new Departamento.Criterio()
            {
                Nombre = nombre,
                Sucursal = sucursal
            };

            _lstDepartamento = DepartamentoRepositorio.EncontrarPor(criterio);
        }

        public void ObtenerDepartamentos()
        {
            _Departamentos = DepartamentoTareaRepositorio.FindAll(_CTarea.UidTarea);
        }



        public void ObtenerDepartamento(Guid Departamento)
        {
            _CDepartamento = DepartamentoRepositorio.Encontrar(Departamento);
        }
        public void departamentoseleccionado(Guid Departamento)
        {
            _ltsDepartamento2 = DepartamentoRepositorio.Encontrardepartamento(Departamento);
        }

        public void BuscarAntecesor(string nombre)
        {
            Tarea.Criterio criterio = new Tarea.Criterio()
            {
                Nombre = nombre,
            };
            _ltsTarea = TareaRepositorio.Buscar(criterio);
        }

        public void CargarAntecesor(string UidAntecesor)
        {
            _CAntecesor = TareaRepositorio.Encontrar(new Guid(UidAntecesor));
        }


        public void ObtenerTarea(Guid Tarea)
        {
            _CTarea = TareaRepositorio.Encontrar(Tarea);
        }

        public void ObtenerTareaDepartamento(string UidTarea)
        {
            _CDepartamentoTarea = DepartamentoTareaRepositorio.Buscar(new Guid(UidTarea));
        }

        public void CargarTarea(Guid uidsucursal)
        {
            _ltsTarea = TareaRepositorio.CargarTarea(uidsucursal);
        }

        public void BuscarTarea(string Nombre, DateTime? fecha1, DateTime? fecha2, Guid UidSucursal, string Departamento, string Encargado)
        {
            _ltsTarea = TareaRepositorio.BuscarTarea(0,Nombre, fecha1, fecha2, UidSucursal, Departamento, string.Empty, Encargado, 0);
        }

        public void ObtenerOpciones()
        {
            _Opciones = TareaOpcionRepositorio.Buscar(_CTarea.UidTarea);
        }
        #endregion
    }
}