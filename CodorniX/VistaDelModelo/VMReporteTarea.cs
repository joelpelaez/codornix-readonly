﻿using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.VistaDelModelo
{
	public class VMReporteTarea
	{
		Usuario.Repository usuarioRepository = new Usuario.Repository();
		Empresa.Repository empresaRepository = new Empresa.Repository();
		Sucursal.Repository sucursalRepository = new Sucursal.Repository();
		Departamento.Repository departamentoRepository = new Departamento.Repository();
		Cumplimiento.Repository cumplimientoRepository = new Cumplimiento.Repository();
		Periodo.Repository periodoRepository = new Periodo.Repository();
		Turno.Repository turnoRepository = new Turno.Repository();
		TareaOpcion.Repositorio opcionRepository = new TareaOpcion.Repositorio();

		private Usuario _Encargado;
		public Usuario Encargado
		{
			get { return _Encargado; }
			set { _Encargado = value; }
		}

		private Empresa _Empresa;
		public Empresa Empresa
		{
			get { return _Empresa; }
			set { _Empresa = value; }
		}

		private Sucursal _Sucursal;
		public Sucursal Sucursal
		{
			get { return _Sucursal; }
			set { _Sucursal = value; }
		}

		private Departamento _Departamento;
		public Departamento Departamento
		{
			get { return _Departamento; }
			set { _Departamento = value; }
		}

		private Periodo _Periodo;
		public Periodo Periodo
		{
			get { return _Periodo; }
			set { _Periodo = value; }
		}

		private Turno _Turno;
		public Turno Turno
		{
			get { return _Turno; }
			set { _Turno = value; }
		}

		private List<TareaOpcion> _Opciones;
		public List<TareaOpcion> Opciones
		{
			get { return _Opciones; }
			set { _Opciones = value; }
		}

		private List<Cumplimiento> _Cumplimientos;
		public List<Cumplimiento> Cumplimientos
		{
			get { return _Cumplimientos; }
			set { _Cumplimientos = value; }
		}

		public List<Calificacion> LsCalificaciones
		{
			get;
			set;
		}

		public VMReporteTarea()
		{

		}

		public void ObtenerEncargado(Guid uid)
		{
			_Encargado = usuarioRepository.Find(uid);
		}

		public void ObtenerEmpresa(Guid uid)
		{
			_Empresa = empresaRepository.Find(uid);
		}

		public void ObtenerSucursal(Guid uid)
		{
			_Sucursal = sucursalRepository.Find(uid);
		}

		public void ObtenerDepartamento(Guid uid)
		{
			_Departamento = departamentoRepository.Encontrar(uid);
		}

		public void ObtenerPeriodo(Guid uid)
		{
			_Periodo = periodoRepository.Find(uid);
		}

		public void ObtenerTurno(Guid uid)
		{
			_Turno = turnoRepository.Find(uid);
		}

		public void ObtenerTareas(Guid uidUsuario, Guid uidDepartamento, DateTime fecha)
		{
			_Cumplimientos = cumplimientoRepository.ReporteTareas(uidUsuario, uidDepartamento, fecha);
		}

		public void ObtenerTareasNuevasTurno(Guid UidPeriodo, DateTime DtFechaTurno, int FolioTurno)
		{
			_Cumplimientos = cumplimientoRepository.ReporteTareasNuevas(UidPeriodo, DtFechaTurno, FolioTurno);
		}

		public void ObtenerOpciones(Guid uidTarea)
		{
			_Opciones = opcionRepository.Buscar(uidTarea);
		}

		public void ObtenerResumenCalificacion(Guid UidDepartamento, DateTime DtFechaTurno)
		{
			Calificacion.Repository CalificacionRep = new Calificacion.Repository();
			this.LsCalificaciones = CalificacionRep.ResumenTurno(UidDepartamento, DtFechaTurno);
		}
	}
}