﻿namespace CodorniX.Types
{
	using GraphQL.Types;
	using Modelo;
	using System.Collections.Generic;

	public class SucursalType : ObjectGraphType<Sucursal>
	{
		public SucursalType()
		{
			Field(x => x.Id).Name("id").Description("Identificador de la sucursal");
			Field(x => x.strIdEmpresa).Name("idEmpresa").Description("Identificador de la empresa");
			Field(x => x.strIdTipoSucursal).Name("idTipo").Description("Identificador del tipo");
			Field(x => x.StrNombre).Name("nombre").Description("Nombre");
			Field(x => x.strFechaRegistro).Name("fechaRegistro").Description("Fecha de registro de la sucursal");
			Field(x => x.StrTipoSucursal).Name("tipo").Description("Tipo");
			Field(x => x.RutaImagen).Name("img").Description("Ruta de la imagen");
			Field(x => x.ZonaHoraria).Name("zonaHoraria").Description("Zona Horaria");

			Field<ListGraphType<SucursalDireccionType>>("direcciones", null, resolve: context =>
			{
				try
				{
					var idSucursal = context.Source.UidSucursal;
					SucursalDireccion.Repository repository = new SucursalDireccion.Repository();

					return repository.FindAll(idSucursal);
				}
				catch (System.Exception)
				{
					List<SucursalDireccion> lsAux = new List<SucursalDireccion>();
					return lsAux;
				}

			});

			Field<ListGraphType<SucursalTelefonoType>>("telefonos", null, resolve: context =>
			{
				try
				{
					var idSucursal = context.Source.UidSucursal;
					SucursalTelefono.Repository repository = new SucursalTelefono.Repository();
					return repository.FindAll(idSucursal);
				}
				catch (System.Exception)
				{
					List<SucursalTelefono> lsAux = new List<SucursalTelefono>();
					return lsAux;
				}
			});
		}
	}
}