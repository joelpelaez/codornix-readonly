﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class CalificacionType : ObjectGraphType<Calificacion>
	{
		public CalificacionType()
		{
			Field(x => x.strIdCalificacion).Name("id").Description("Identificador de la calificacion");
			Field(x => x.StrCalificacion).Name("calificacion").Description("Descripcion de calificacion");
		}
	}
}