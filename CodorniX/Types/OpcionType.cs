﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
    public class OpcionType : ObjectGraphType<Opciones>
    {
        public OpcionType()
        {
            Field(x => x.strIdOpcion).Name("id").Description("Identificador de la Opción");
            Field(x => x.StrOpciones).Name("opcion").Description("Valor de la opción (texto)");
            Field(x => x.IntOrden).Name("orden").Description("Orden de las Opciones");
            Field(x => x.strBlVisible).Name("visible").Description("La Opción es visible");
        }
    }
}