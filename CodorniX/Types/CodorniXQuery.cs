﻿using CodorniX.Modelo;
using CodorniX.Types;
using CodorniX.VistaDelModelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace CodorniX.Types
{
	internal class CodorniXQuery : ObjectGraphType
	{
		/// <summary>
		/// Class to create query's to use un graphql
		/// </summary>
		public CodorniXQuery()
		{
			///<summary>
			/// Me (De uso general, se utiliza para llamar datos del usuario a travez del token)
			///</summary>
			Field<UsuarioType>("me", arguments: null, resolve: context =>
			 {
				 var name = context.UserContext as string;

				 var repo = new Usuario.Repository();
				 var result = repo.FindByName(name);

				 return result;
			 });

			///<summary>
			/// Datos de Sesion Requeridos para inicializar la Aplicación
			///</summary>
			Field<UserSessionType>("sessionData", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsuario" }
			), resolve: context =>
			{
				var idUsuario = context.GetArgument<string>("idUsuario");
				Sesion sesion = new Sesion();
				sesion.perfil = "";
				sesion.Messsage = "";
				sesion.appWeb = "";

				Status.Repository statusRepository = new Status.Repository();
				Perfil.Repositorio perfilRepository = new Perfil.Repositorio();
				Empresa.Repository empresaRepository = new Empresa.Repository();

				/* Obtener datos del usuario */
				Usuario usuario = new Usuario.Repository().Find(new Guid(idUsuario));
				sesion.uidUsuario = usuario.UIDUSUARIO;

				/*Obtener Estatus del Usuario */
				Status status = statusRepository.Find(usuario.UidStatus);
				if (!status.strStatus.Equals("Activo"))
				{
					sesion.Messsage = "Usuario inactivo";
					return sesion;
				}

				/* Obtener Empresas Pertenecientes al usuario */
				List<UsuarioPerfilEmpresa> ep = new UsuarioPerfilEmpresa.Repository().FindAll(new Guid(idUsuario));
				if (ep.Count > 0)
				{
					List<Guid> uidEmpresas = (from em in ep select em.UidEmpresa).ToList();
					List<Guid> uidPerfiles = (from pf in ep select pf.UidPerfil).ToList();
					sesion.uidEmpresasPerfiles = uidEmpresas;
					sesion.uidEmpresaActual = uidEmpresas[0];
					sesion.uidPerfilActual = uidPerfiles[0];
					sesion.uidNivelAccesoActual = perfilRepository.CargarDatos(sesion.uidPerfilActual.Value).UidNivelAcceso;
				}
				else
				{
					/* Obtener sucursales Pertenecientes al usuario */
					List<UsuarioPerfilSucursal> sp = new UsuarioPerfilSucursal.Repository().FindAll(new Guid(idUsuario));
					if (sp.Count > 0)
					{
						List<Guid> uidSucursales = (from su in sp select su.UidSucursal).ToList();
						List<Guid> uidPerfiles = (from pf in sp select pf.UidPerfil).ToList();
						sesion.uidSucursalesPerfiles = uidSucursales;
						sesion.uidSucursalActual = uidSucursales[0];
						sesion.uidEmpresaActual = new Sucursal.Repository().Find(uidSucursales[0]).UidEmpresa;
						sesion.uidPerfilActual = uidPerfiles[0];
					}
					else
					{
						sesion.Messsage = "El usuario no tiene empresa ni sucursal asignados";
						return sesion;
					}
				}

				Empresa empresa = empresaRepository.Find(sesion.uidEmpresaActual.Value);
				if (empresa != null && empresa.UidStatus != Guid.Empty)
				{
					status = statusRepository.Find(empresa.UidStatus);

					if (status.strStatus != "Activo")
					{
						sesion.Messsage = "La empresa a la que pertenece se encuentra desactivada";
						return sesion;
					}
				}

				Perfil perfil = perfilRepository.CargarDatos(sesion.uidPerfilActual.Value);
				sesion.perfil = perfil.strPerfil;
				if (sesion.perfil == "Supervisor")
				{
					/* Obtener listado Id departamentos */
					sesion.UidDepartamentos = AsignacionSupervision.Repository.ObtenerDepartamentosSupervisados(
						usuario.UIDUSUARIO,
						DateTime.Today);
				}

				string nivel = Acceso.ObtenerAppWeb(sesion.uidPerfilActual.Value);
				sesion.appWeb = nivel;
				if (nivel == "Frontend")
				{
					if (!Acceso.TienePeriodo(usuario.UIDUSUARIO, sesion.uidSucursalActual.Value, DateTime.Now))
					{
						sesion = new Sesion();
						sesion.perfil = "";
						sesion.Messsage = "No tiene ningun turno el día de hoy";
						sesion.appWeb = "";
						return sesion;
					}

					sesion.UidDepartamentos = Acceso.ObtenerDepartamentosSupervisor(usuario.UIDUSUARIO, DateTime.Today);
					sesion.Rol = "Supervisor";

					if (sesion.UidDepartamentos == null || sesion.UidDepartamentos.Count == 0)
					{
						sesion.UidDepartamentos = Acceso.ObtenerDepartamentosEncargado(usuario.UIDUSUARIO, DateTime.Today);
						sesion.Rol = "Encargado";
					}
				}

				return sesion;
			});

			///<summary>
			/// Metodos InicioSupervisor.aspx
			///</summary>
			Field<ListGraphType<TurnoSupervisorType>>("turnosSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "dtFechaInicio" },
				new QueryArgument<StringGraphType> { Name = "dtFechaFin" },
				new QueryArgument<StringGraphType> { Name = "folio" },
				new QueryArgument<StringGraphType> { Name = "uidSucursal" },
				new QueryArgument<StringGraphType> { Name = "uidUsuario" }
			), resolve: context =>
			{
				var vFechaInicio = context.GetArgument<string>("dtFechaInicio");
				var vFechaFin = context.GetArgument<string>("dtFechaFin");
				var vFolio = context.GetArgument<string>("folio");
				var vIdSucursal = context.GetArgument<string>("uidSucursal");
				var vIdUsuario = context.GetArgument<string>("uidUsuario");

				VMInicioSupervisor vMInicioSupervisor = new VMInicioSupervisor();
				try
				{
					Guid UidUsuario = new Guid(vIdUsuario);
					Guid UidSucursal = new Guid(vIdSucursal);
					DateTime? DtFechaInicio = DateTime.Today;
					DateTime? DtFechaFin = DateTime.Today;
					int? Folio = null;

					DateTimeOffset time = Hora.ObtenerHoraServidor();
					DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(UidSucursal));
					DateTime Today = horaLocal.DateTime;

					if (string.IsNullOrEmpty(vFechaInicio) && string.IsNullOrEmpty(vFechaFin) && string.IsNullOrEmpty(vFolio))
					{
						vMInicioSupervisor.ObtenerTurnoDeHoy(UidUsuario, UidSucursal, Today);
						return vMInicioSupervisor.LsTurnosSupervisor;
					}
					else
					{
						if (!string.IsNullOrEmpty(vFolio))
							Folio = int.Parse(vFolio);

						if (!string.IsNullOrEmpty(vFechaInicio))
							DtFechaInicio = Convert.ToDateTime(DateTime.ParseExact(vFechaInicio, "dd/MM/yyyy", CultureInfo.InvariantCulture));

						if (!string.IsNullOrEmpty(vFechaFin))
							DtFechaFin = Convert.ToDateTime(DateTime.ParseExact(vFechaFin, "dd/MM/yyyy", CultureInfo.InvariantCulture));

						vMInicioSupervisor.LsTurnosSupervisor = new List<TurnoSupervisor>();
						int IntDtAuxCompare = 0;
						for (; DtFechaInicio.Value <= DtFechaFin.Value; DtFechaInicio = DtFechaInicio.Value.AddDays(1))
						{
							vMInicioSupervisor.BusquedaTurnos(UidUsuario, UidSucursal, DtFechaInicio.Value);
							IntDtAuxCompare = DateTime.Compare(DtFechaInicio.Value, Today);
							if (IntDtAuxCompare < 0 && vMInicioSupervisor.TurnoSupervisor.DtFechaInicio == null)
							{
							}
							else
							{
								if (Folio == null)
									vMInicioSupervisor.LsTurnosSupervisor.Add(vMInicioSupervisor.TurnoSupervisor);
								else
								{
									if (Folio == vMInicioSupervisor.TurnoSupervisor.IntFolio)
										vMInicioSupervisor.LsTurnosSupervisor.Add(vMInicioSupervisor.TurnoSupervisor);
								}
							}
						}
					}
				}
				catch (Exception)
				{
					return new List<TurnoSupervisor>();
				}
				return vMInicioSupervisor.LsTurnosSupervisor;
			});

			Field<TextType>("turnoSupervisorAbierto", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "idUsr" },
				new QueryArgument<StringGraphType> { Name = "idSuc" },
				new QueryArgument<StringGraphType> { Name = "fecha" }
			), resolve: context =>
			{
				var vIdUsuario = context.GetArgument<string>("idUsr");
				var vIdSucursal = context.GetArgument<string>("idSuc");
				var vFecha = context.GetArgument<string>("fecha");
				ServerMessage SM = new ServerMessage();
				TurnoSupervisor.Repository TSRepository = new TurnoSupervisor.Repository();
				try
				{
					Guid UidUsuario = new Guid(vIdUsuario);
					Guid UidSucursal = new Guid(vIdSucursal);
					DateTime DtFecha;

					if (string.IsNullOrEmpty(vFecha))
					{
						DateTimeOffset time = Hora.ObtenerHoraServidor();
						DateTimeOffset horaLocal = TimeZoneInfo.ConvertTime(time, Hora.ObtenerZonaHoraria(UidSucursal));
						DtFecha = horaLocal.DateTime;
					}
					else
						DtFecha = Convert.ToDateTime(DateTime.ParseExact(vFecha, "dd/MM/yyyy", CultureInfo.InvariantCulture));

					var Turno = TSRepository.VerificarUltimoTurno(UidUsuario, UidSucursal, DtFecha);
					if (Turno != null)
						SM.Value = "True";
					else
						SM.Value = "False";
				}
				catch (Exception)
				{
					SM.Value = "failure";
				}

				return SM;
			});

			Field<TurnoSupervisorType>("turnoSupervisor", arguments: new QueryArguments(
				new QueryArgument<StringGraphType> { Name = "id" }
			), resolve: context =>
			{
				var vId = context.GetArgument<string>("id");
				TurnoSupervisor.Repository TRepository = new TurnoSupervisor.Repository();
				try
				{
					Guid Uid = new Guid(vId);
					var Result = TRepository.Find(Uid);
					if (Result == null)
					{
						TurnoSupervisor TS = new TurnoSupervisor();
						TS.UidTurnoSupervisor = Guid.Empty;
						TS.StrSucursal = "";
						TS.StrEstadoTurno = "(No creado)";
						return TS;
					}
					else
						return Result;
				}
				catch (Exception ex)
				{
					TurnoSupervisor TS = new TurnoSupervisor();
					TS.UidTurnoSupervisor = Guid.Empty;
					TS.StrSucursal = "";
					TS.StrEstadoTurno = "(No creado)";
					return TS;
				}
			});

			Field<ListGraphType<RevisionType>>("revisionesTurno", arguments: new QueryArguments() {
				new QueryArgument<StringGraphType> { Name="id"}
			}, resolve: context =>
			{
				var vUid = context.GetArgument<string>("id");

				Revision.Repository RevRepository = new Revision.Repository();
				try
				{
					Guid UidTurno = new Guid(vUid);
					var vResults = RevRepository.FindByTurno(UidTurno);
					return vResults;
				}
				catch (Exception)
				{
					return new List<Revision>();
				}
			});
		}
	}
}