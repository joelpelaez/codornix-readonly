﻿using CodorniX.Modelo;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class DepartamentoType : ObjectGraphType<Departamento>
	{
		public DepartamentoType()
		{
			Field(x => x.strIdDepartamento).Name("id").Description("Identificador del departamento");
			Field(x => x.StrNombre).Name("nombre").Description("Nombre del departamento");
			Field(x => x.StrDescripcion).Name("descripcion").Description("Descripcion del departamento");

			Field<ListGraphType<AreaType>>("areas", null, resolve: context =>
			{
				try
				{
					var idDepto = context.Source.UidDepartamento;
					Area.Repository repository = new Area.Repository();
					var result = repository.FindAll(idDepto);
					return result;
				}
				catch (Exception)
				{
					List<Area> lsAux = new List<Area>();
					return lsAux;
				}
			});
		}
	}
}