﻿using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class SucursalDireccionType : ObjectGraphType<SucursalDireccion>
	{
		public SucursalDireccionType()
		{
			Field(x => x.strIdDireccion).Name("id").Description("Identificador de la direccion");
			Field(x => x.strIdPais).Name("idPais").Description("Identificador del pais");
			Field(x => x.strIDEstado).Name("idEstado").Description("Identificador del estado");
			Field(x => x.StrMunicipio).Name("municipio").Description("Municipio");
			Field(x => x.StrCiudad).Name("ciudad").Description("Ciudad");
			Field(x => x.StrColonia).Name("colonia").Description("Colonia");
			Field(x => x.StrCalle).Name("calle").Description("Calle");
			Field(x => x.StrConCalle).Name("conCalle").Description("con Calle");
			Field(x => x.StrYCalle).Name("yCalle").Description("y calle");
			Field(x => x.StrNoInt).Name("noInt").Description("Numero Interior");
			Field(x => x.StrNoExt).Name("noExt").Description("Numero Exterior");
			Field(x => x.StrReferencia).Name("referencia").Description("Referencia");
			Field(x => x.LongDirection).Name("longDirection").Description("Direccion completa");
		}
	}
}