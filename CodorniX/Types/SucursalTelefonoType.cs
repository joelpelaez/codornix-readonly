﻿using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class SucursalTelefonoType : ObjectGraphType<SucursalTelefono>
	{
		public SucursalTelefonoType()
		{
			Field(x => x.strIdTelefono).Name("id").Description("Identficador del telefono");
			Field(x => x.strIdTipoTelefono).Name("idTipoTelefono").Description("Identificador del tipo de telefono");
			Field(x => x.StrTelefono).Name("telefono").Description("Telefono");
			Field(x => x.StrTipoTelefono).Name("tipoTelefono").Description("Tipo de telefono");
		}
	}
}