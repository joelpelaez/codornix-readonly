﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using CodorniX.Modelo;
using GraphQL.Types;

namespace CodorniX.Types
{
	public class TipoTareaType : ObjectGraphType<TipoTarea>
	{
		public TipoTareaType()
		{
			Field(x => x.strIdTarea).Name("id").Description("Identificador del tipo de tarea");
			Field(x => x.StrTipoTarea).Name("tipo").Description("Tipo de tarea");
		}
	}
}