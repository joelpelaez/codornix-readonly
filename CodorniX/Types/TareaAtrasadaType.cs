﻿using GraphQL.Types;
using CodorniX.Modelo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Types
{
	public class TareaAtrasadaType : ObjectGraphType<TareaAtrasada>
	{
		public TareaAtrasadaType()
		{
			Field(x => x.strIdTarea).Name("idTarea").Description("Identificador de la tarea");
			Field(x => x.strIdDepartamento).Name("idDepartamento").Description("Identificador del departamento");
			Field(x => x.strIdArea).Name("idArea").Description("Identificador del area");
			Field(x => x.strIdCumplimiento).Name("idCumplimiento").Description("Identificador del cumplimiento");
			Field(x => x.strIdTurno).Name("idTurno").Description("Identificador del turno");
			Field(x => x.StrTarea).Name("tarea").Description("Nombre de la tarea");
			Field(x => x.StrDepartamento).Name("departamento").Description("Departamento de la tarea");
			Field(x => x.StrArea).Name("area").Description("Area de la tarea");
			Field(x => x.StrTurno).Name("turno").Description("Turno de la tarea");

			Field(x => x.strTipo).Name("tipo").Description("Tipo de tarea");
			Field(x => x.strFechaProgramada).Name("fechaProgramada").Description("Fecha programada para la ejecucion de la tarea");
			Field(x => x.strFechaProxima).Name("fechaProxima").Description("Fecha proxima de ejecucion de la tarea");
			Field(x => x.strServerMessage).Name("serverMessage").Description("Mensajes del servidor");
		}
	}
}