﻿using CodorniX.Controllers;
using CodorniX.Types;
using GraphQL;
using GraphQL.Http;
using GraphQL.Types;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Unity;
using Unity.Lifetime;
using Unity.Registration;

namespace CodorniX.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            var container = new UnityContainer();
            container.RegisterInstance<IDocumentExecuter>(new DocumentExecuter());
            container.RegisterInstance<IDocumentWriter>(new DocumentWriter(true));
            container.RegisterType<UsuarioType>();
            container.RegisterSingleton<ISchema, CodorniXSchema>();
            container.RegisterType<CodorniXQuery>();
            container.RegisterType<CodorniXMutation>();
            config.DependencyResolver = new UnityResolver(container);

            config.Filters.Add(new AuthorizeAttribute());

            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute("DefaultApi", "api/{controller}/{id}", new { id = RouteParameter.Optional });

            config.EnsureInitialized();
        }
    }
}