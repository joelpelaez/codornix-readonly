﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CodorniX.Modelo
{
    public class Medicion
    {
        private Guid _UidTipoMedicion;
        public Guid UidTipoMedicion
        {
            get { return _UidTipoMedicion; }
            set { _UidTipoMedicion = value; }
        }
        private string _StrTipoMedicion;

        public string StrTipoMedicion
        {
            get { return _StrTipoMedicion; }
            set { _StrTipoMedicion = value; }
        }

        public class Repositorio
        {
            public List<Medicion> ConsultarMedicion()
            {
                List<Medicion> mediciones = new List<Medicion>();

                SqlCommand comando = new SqlCommand();

                try
                {
					// SP - usp_ConsultarMedicion
					comando.CommandText = "select * from Medicion order by orden";
                    comando.CommandType = CommandType.Text;

                    DataTable table = new Connection().ExecuteQuery(comando);

                    foreach (DataRow row in table.Rows)
                    {
                        Medicion medicion = new Medicion()
                        {
                            UidTipoMedicion = (Guid)row["UidTipoMedicion"],
                            StrTipoMedicion = (string)row["VchTipoMedicion"],
                        };
                        mediciones.Add(medicion);
                    }
                }
                catch (SqlException e)
                {
                    throw;
                }

                return mediciones;


            }
        }
    }
}
