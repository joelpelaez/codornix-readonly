﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	[Serializable]
	public class Revision
	{
		private Guid _UidRevision;
		public Guid UidRevision
		{
			get { return _UidRevision; }
			set { _UidRevision = value; }
		}
		public string strIdRevision { get { return _UidRevision.ToString(); } }

		private Guid _UidTarea;
		public Guid UidTarea
		{
			get { return _UidTarea; }
			set { _UidTarea = value; }
		}

		public int IntFolio
		{
			get; set;
		}
		public int FolioTarea
		{
			get;
			set;
		}

		private int IntFolioCumplimiento;
		public int FolioCumplimiento
		{
			get { return IntFolioCumplimiento; }
			set { IntFolioCumplimiento = value; }
		}

		private Guid _UidCumplimiento;
		/// <summary>
		/// Identificador único del revision.
		/// </summary>
		public Guid UidCumplimiento
		{
			get { return _UidCumplimiento; }
			set { _UidCumplimiento = value; }
		}
		public string strIdCumplimiento { get { return _UidCumplimiento.ToString(); } }

		private Guid? _UidUsuario;
		/// <summary>
		/// Identificador único del usuario (encargado) que realizó la tarea.
		/// </summary>
		public Guid? UidUsuario
		{
			get { return _UidUsuario; }
			set { _UidUsuario = value; }
		}
		public string strIdUsuario { get { return _UidUsuario.ToString(); } }

		private DateTimeOffset? _DtFechaHora;
		/// <summary>
		/// Fecha y hora de revision de la tarea. En teoría la fecha de revision y programación deben
		/// coincidir, pero debe tomarse en cuenta una fecha separada.
		/// </summary>
		// TODO: Revisar procedimeinto de cálculo con la fecha programada.
		public DateTimeOffset? DtFechaHora
		{
			get { return _DtFechaHora; }
			set { _DtFechaHora = value; }
		}
		public string strFechaHora { get { return _DtFechaHora.ToString(); } }

		private string _StrNotas;
		public string StrNotas
		{
			get { return _StrNotas; }
			set { _StrNotas = value; }
		}

		private bool _BlCorrecto;
		public bool BlCorrecto
		{
			get { return _BlCorrecto; }
			set { _BlCorrecto = value; }
		}

		private bool? _BlValor;
		/// <summary>
		/// Valor de resultado de la tarea: tipo booleano o verdadero/falso.
		/// </summary>
		public bool? BlValor
		{
			get { return _BlValor; }
			set { _BlValor = value; }
		}
		public string strBlValor { get { return _BlValor == null ? "empty" : _BlValor.ToString(); } }

		private decimal? _DcValor1;
		/// <summary>
		/// Valor de resultado de la tarea. tipo decimal, primer valor o único.
		/// </summary>
		public decimal? DcValor1
		{
			get { return _DcValor1; }
			set { _DcValor1 = value; }
		}
		public string strDcValor1 { get { return _DcValor1 == null ? "empty" : _DcValor1.ToString(); } }

		private decimal? _DcValor2;
		/// <summary>
		/// Valor de resultado de la tarea. Tipo decimal, segundo valor.
		/// </summary>
		public decimal? DcValor2
		{
			get { return _DcValor2; }
			set { _DcValor2 = value; }
		}
		public string strDcValor2 { get { return _DcValor2 == null ? "empty" : _DcValor2.ToString(); } }

		private Guid? _UidOpciones;
		/// <summary>
		/// Valor de resultado de la tarea. Tipo opcional, hace referencia a una tabla de opciones.
		/// </summary>
		public Guid? UidOpcion
		{
			get { return _UidOpciones; }
			set { _UidOpciones = value; }
		}
		public string strIdOpcion { get { return _UidOpciones == null ? Guid.Empty.ToString() : _UidOpciones.ToString(); } }

		private string _StrOpcion;
		/// <summary>
		/// Contenido textual de la opción seleccionada en <see cref="UidOpcion"/>
		/// </summary>
		public string StrOpcion
		{
			get { return _StrOpcion; }
			set { _StrOpcion = value; }
		}

		private Guid _UidCalificacion;
		public Guid UidCalificacion
		{
			get => _UidCalificacion;
			set => _UidCalificacion = value;
		}
		public string strIdCalificacion { get { return _UidCalificacion.ToString(); } }

		// EXTRA FIELDS

		private string _StrEstado;
		public string StrEstado
		{
			get { return _StrEstado; }
			set { _StrEstado = value; }
		}

		private string _StrNombreUsuario;
		/// <summary>
		/// Campo extra: Nombre del usuario referenciado por <see cref="UidUsuario"/>
		/// </summary>
		public string StrNombreUsuario
		{
			get { return _StrNombreUsuario; }
			set { _StrNombreUsuario = value; }
		}

		private string _StrApellidoPaterno;
		/// <summary>
		/// Campo extra: Apellido paterno del usuario referenciado por <see cref="UidUsuario"/>
		/// </summary>
		public string StrApellidoPaterno
		{
			get { return _StrApellidoPaterno; }
			set { _StrApellidoPaterno = value; }
		}

		private string _StrTarea;
		/// <summary>
		/// Campo extra: Nombre de la tarea referenciada por <see cref="UidTarea"/>
		/// </summary>
		public string StrTarea
		{
			get { return _StrTarea; }
			set { _StrTarea = value; }
		}

		private string _StrDepartamento;
		/// <summary>
		/// Campo extra: Nombre del departamento referenciado por <see cref="UidTarea"/>
		/// </summary>
		public string StrDepartamento
		{
			get { return _StrDepartamento; }
			set { _StrDepartamento = value; }
		}

		private string _StrArea;
		/// <summary>
		/// Campo extra: Nombre de la tarea
		/// </summary>
		public string StrArea
		{
			get { return _StrArea; }
			set { _StrArea = value; }
		}

		private TimeSpan? _TmHora;
		/// <summary>
		/// Campo extra: Hora programada para realizarse la tarea.
		/// </summary>
		public TimeSpan? TmHora
		{
			get { return _TmHora; }
			set { _TmHora = value; }
		}
		public string strHora { get { return _TmHora.ToString(); } }

		private string _StrValor;
		/// <summary>
		/// Campo de reporte: Representación textual del valor.
		/// </summary>
		public string StrValor
		{
			get { return _StrValor; }
			set { _StrValor = value; }
		}

		private string _StrTipoMedicion;
		public string StrTipoMedicion
		{
			get { return _StrTipoMedicion; }
			set { _StrTipoMedicion = value; }
		}

		private string _StrTipoUnidad;
		public string StrTipoUnidad
		{
			get { return _StrTipoUnidad; }
			set { _StrTipoUnidad = value; }
		}

		private bool _BlValorOriginal;
		public bool BlValorOriginal
		{
			get { return _BlValorOriginal; }
			set { _BlValorOriginal = value; }
		}

		private decimal _DcValor1Original;
		public decimal DcValor1Original
		{
			get { return _DcValor1Original; }
			set { _DcValor1Original = value; }
		}

		private decimal _DcValor2Original;
		public decimal DcValor2Original
		{
			get { return _DcValor2Original; }
			set { _DcValor2Original = value; }
		}

		private Guid _UidOpcionOriginal;
		public Guid UidOpcionOriginal
		{
			get { return _UidOpcionOriginal; }
			set { _UidOpcionOriginal = value; }
		}
		public string strIdOpcionOriginal { get { return _UidOpcionOriginal.ToString(); } }

		private string _StrOpcionOriginal;
		public string StrOpcionOriginal
		{
			get { return _StrOpcionOriginal; }
			set { _StrOpcionOriginal = value; }
		}

		private string _StrObservacion;
		/// <summary>
		/// Observaciones obtenidas durante el revision de la tarea, puede ser vacío.
		/// </summary>
		public string StrObservacion
		{
			get { return _StrObservacion; }
			set { _StrObservacion = value; }
		}

		///<summary>
		///Variable para detarminar si la tarea fue realizada en el tiempo (FALSE/NULL) o fue realizada despues (TRUE)
		///</summary>
		private bool _BlTareaAtrasada;
		public bool BlTareaAtrasada
		{
			get { return _BlTareaAtrasada; }
			set { _BlTareaAtrasada = value; }
		}

		private DateTimeOffset _DtoCumplimiento;
		/// <summary>
		/// Fecha y hora en la cual fue realizada la tarea
		/// </summary>
		public DateTimeOffset DtoCumplimiento
		{
			get { return _DtoCumplimiento; }
			set { _DtoCumplimiento = value; }
		}


		/// <summary>
		/// Clase repositorio que abstrae la persistencia de datos hacia la base de datos.
		/// </summary>
		public class Repository
		{
			Connection conn = new Connection();

			/// <summary>
			/// Obtiene un revision a partir de su identificador único.
			/// </summary>
			/// <param name="uid">Identificador único</param>
			/// <returns>Objeto <see cref="Revision"/></returns>
			public Revision Find(Guid uid)
			{
				Revision revision = null;

				SqlCommand command = new SqlCommand();

				try
				{
					// SP - usp_Revision_Find
					command.CommandText = "SELECT TOP(1) * FROM Revision WHERE UidRevision = '" + uid + "'";
					command.CommandType = CommandType.Text;

					//command.AddParameter("@UidRevision", uid, SqlDbType.UniqueIdentifier);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						revision = new Revision();
						revision._UidRevision = new Guid(row["UidRevision"].ToString());
						revision._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
						revision._UidUsuario = row.IsNull("UidUsuario") ? (Guid?)null : new Guid(row["UidUsuario"].ToString());
						revision._DtFechaHora = row.IsNull("DtFechaHora") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						revision._StrNotas = row["VchNotas"].ToString();
						revision._BlCorrecto = Convert.ToBoolean(row["BitCorrecto"].ToString());
						revision._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						revision._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						revision._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						revision._UidOpciones = row.IsNull("UidOpcion") ? (Guid?)null : new Guid(row["UidOpcion"].ToString());
						revision._UidCalificacion = new Guid(row["UidCalificacion"].ToString());
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching a Revision", e);
				}

				return revision;
			}

			/// <summary>
			/// Obtener detalle de revision mediante su identificador 
			/// </summary>
			/// <param name="Uid"></param>
			/// <returns></returns>
			public Revision GetRevisionDetailById(Guid Uid)
			{
				Revision _Revision = new Revision();
				SqlCommand Command = new SqlCommand();
				try
				{
					Command.CommandText = "usp_Revision_DetailById";
					Command.CommandType = CommandType.StoredProcedure;

					Command.AddParameter("@UidRevision", Uid, SqlDbType.UniqueIdentifier);

					DataTable Table = conn.ExecuteQuery(Command);
					foreach (DataRow item in Table.Rows)
					{
						_Revision = new Revision()
						{
							UidRevision = new Guid(item["UidRevision"].ToString()),
							BlValor = item.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(item["BitValor"]),
							DcValor1 = item.IsNull("DcValor1") ? (decimal?)null : (decimal)item["DcValor1"],
							UidOpcion = item.IsNull("UidOpcion") ? (Guid?)null : (Guid)item["UidOpcion"],
							StrNotas = item.IsNull("VchNotas") ? string.Empty : item["VchNotas"].ToString(),
							UidCalificacion = item.IsNull("UidCalificacion") ? Guid.Empty : new Guid(item["UidCalificacion"].ToString()),
							FolioCumplimiento = (int)item["FolioCumplimiento"],
							BlValorOriginal = item.IsNull("BitEvaluado") ? false : Convert.ToBoolean(item["BitEvaluado"]),
							DcValor1Original = item.IsNull("ValorEvaluado") ? 0 : (decimal)item["ValorEvaluado"],
							UidOpcionOriginal = item.IsNull("OpcionEvaluado") ? Guid.Empty : (Guid)item["OpcionEvaluado"],
							UidTarea = new Guid(item["UidTarea"].ToString()),
							FolioTarea = (int)item["FolioTarea"],
							StrTipoUnidad = item.IsNull("VchTipoUnidad") ? string.Empty : item["VchTipoUnidad"].ToString(),
							StrTipoMedicion = item["VchTipoMedicion"].ToString(),
							StrTarea = item["VchTarea"].ToString(),
							StrArea = item.IsNull("VchArea") ? "(global)" : item["VchArea"].ToString(),
							StrDepartamento = item["VchDepartamento"].ToString()
						};
					}
				}
				catch (Exception ex)
				{
					throw;
				}
				return _Revision;
			}

			/// <summary>
			/// Obtener detalles de revision por el Id del cumplimiento
			/// </summary>
			public Revision GetByCumplimientoId(Guid UidCumplimiento)
			{
				Revision rRevision = new Revision();
				try
				{
					SqlCommand Query = new SqlCommand();
					// SP - usp_Revision_FindByCumplimientoId
					Query.CommandText = "SELECT R.*,U.UidUsuario,CONCAT(U.VchNombre,' ',U.VchApellidoPaterno,' ',U.VchApellidoMaterno) AS VchUsuario,CL.VchCalificacion,	O.VchOpciones FROM Revision R INNER JOIN Cumplimiento C ON C.UidCumplimiento = R.UidCumplimiento INNER JOIN Calificacion CL ON CL.UidCalificacion = R.UidCalificacion INNER JOIN Usuario U ON U.UidUsuario = R.UidUsuario LEFT JOIN Opciones O ON O.UidOpciones = C.UidOpcion WHERE R.UidCumplimiento = '" + UidCumplimiento + "'";
					Query.CommandType = CommandType.Text;

					//Query.AddParameter("@UidCumplimiento", UidCumplimiento, SqlDbType.UniqueIdentifier);

					DataTable Table = conn.ExecuteQuery(Query);
					foreach (DataRow row in Table.Rows)
					{
						rRevision = new Revision();
						rRevision._UidRevision = new Guid(row["UidRevision"].ToString());
						rRevision._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
						rRevision._UidUsuario = row.IsNull("UidUsuario") ? (Guid?)null : new Guid(row["UidUsuario"].ToString());
						rRevision._DtFechaHora = row.IsNull("DtFechaHora") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						rRevision._StrNotas = row["VchNotas"].ToString();
						rRevision._BlCorrecto = Convert.ToBoolean(row["BitCorrecto"].ToString());
						rRevision._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						rRevision._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						rRevision._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						rRevision._UidOpciones = row.IsNull("UidOpcion") ? (Guid?)null : new Guid(row["UidOpcion"].ToString());
						rRevision._UidCalificacion = new Guid(row["UidCalificacion"].ToString());
						rRevision.StrNombreUsuario = row["VchUsuario"].ToString();
					}
				}
				catch (Exception ex)
				{
					throw;
				}
				return rRevision;
			}

			public List<Revision> FindByUser(Guid uidUsuario, Guid uidSucursal, Guid UidEncargado, int? FolioTarea, int? FolioCumplimiento, DateTime fecha, string periodos, string nombre, Guid departamento, Guid area, int estado)
			{
				List<Revision> cumplimientos = new List<Revision>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Revision_ListByUser";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFecha", fecha, SqlDbType.Date);
					command.AddParameter("@VchPeriodos", periodos, SqlDbType.NVarChar, 2000);

					if (!string.IsNullOrWhiteSpace(nombre))
						command.AddParameter("@VchNombre", nombre, SqlDbType.NVarChar, 50);

					if (departamento != Guid.Empty)
						command.AddParameter("@UidDepartamento", departamento, SqlDbType.UniqueIdentifier);

					if (UidEncargado != Guid.Empty)
						command.AddParameter("@UidEncargado", UidEncargado, SqlDbType.UniqueIdentifier);

					if (FolioTarea != null)
						command.AddParameter("@FolioTarea", FolioTarea, SqlDbType.Int);

					if (FolioCumplimiento != null)
						command.AddParameter("@FolioCumplimiento", FolioCumplimiento, SqlDbType.Int);

					if (area != Guid.Empty)
						command.AddParameter("@UidArea", area, SqlDbType.UniqueIdentifier);

					command.AddParameter("@UidEstado", estado, SqlDbType.Int);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Revision revision = new Revision();
						revision._UidRevision = row.IsNull("UidRevision") ? default(Guid) : new Guid(row["UidRevision"].ToString());
						revision._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						revision._StrTarea = row["VchTarea"].ToString();
						revision._StrDepartamento = row["VchDepartamento"].ToString();
						revision._StrArea = row.IsNull("VchArea") ? "(GLOBAL)" : row["VchArea"].ToString();
						revision._StrEstado = row["VchEstado"].ToString();
						revision.IntFolio = Convert.ToInt32(row["FolioCumplimiento"].ToString());
						revision.FolioTarea = Convert.ToInt32(row["FolioTarea"].ToString());
						revision.DtoCumplimiento = DateTimeOffset.Parse(row["DtFechaCumplimiento"].ToString());
						revision.BlTareaAtrasada = row.IsNull("BitAtrasado") ? false : Convert.ToBoolean(row["BitAtrasado"].ToString());
						if (revision.BlTareaAtrasada)
							revision.StrTarea += " <span data-toggle='tooltip' data-placement='top' title='(ATRASADA)'><span class='glyphicon glyphicon-time'/></span>";
						cumplimientos.Add(revision);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Revision", e);
				}
				return cumplimientos;
			}

			public List<Revision> FindByTurno(Guid UidturnoSupervisor)
			{
				List<Revision> LsRevisiones = new List<Revision>();
				SqlCommand Command = new SqlCommand();
				try
				{
					Command.CommandText = "usp_Revision_GetByTurno";
					Command.CommandType = CommandType.StoredProcedure;

					Command.AddParameter("@UidTurno", UidturnoSupervisor, SqlDbType.UniqueIdentifier);

					DataTable Table = conn.ExecuteQuery(Command);

					foreach (DataRow item in Table.Rows)
					{
						LsRevisiones.Add(new Revision()
						{
							UidRevision = new Guid(item["UidRevision"].ToString()),
							FolioCumplimiento = (int)item["FolioCumplimiento"],
							BlTareaAtrasada = Convert.ToBoolean(item["BitAtrasado"].ToString()),
							FolioTarea = (int)item["FolioTarea"],
							StrTarea = item["VchTarea"].ToString(),
							StrArea = item.IsNull("VchArea") ? "(global)" : item["VchArea"].ToString(),
							StrDepartamento = item["VchDepartamento"].ToString()
						});
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsRevisiones;
			}

			public void RegistrarRevision(Guid uidCumplimiento, Guid UidTurnoSupervisor, Guid uidUsuario, DateTime fechaCumplimiento,
				bool? estado, decimal? valor1, decimal? valor2, Guid? uidOpcion, string observaciones, bool correcto, Guid calificacion)
			{
				try
				{
					SqlCommand command = new SqlCommand();

					command.CommandText = "usp_Revision_Do";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidCumplimiento", uidCumplimiento, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidTurnoSupervisor", UidTurnoSupervisor, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFechaHora", fechaCumplimiento, SqlDbType.DateTimeOffset);
					command.AddParameter("@BitCorrecto", correcto, SqlDbType.Bit);
					command.AddParameter("@VchNotas", observaciones, SqlDbType.NVarChar, 200);
					command.AddParameter("@UidCalificacion", calificacion, SqlDbType.UniqueIdentifier);

					if (estado.HasValue)
						command.AddParameter("@BitValor", estado.Value, SqlDbType.Bit);

					if (valor1.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor1", valor1.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (valor2.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor1", valor2.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (uidOpcion.HasValue)
						command.AddParameter("@UidOpcion", uidOpcion.Value, SqlDbType.UniqueIdentifier);

					conn.ExecuteCommand(command);

				}
				catch (SqlException ex)
				{
					throw new DatabaseException("Error saving a Revision object", ex);
				}
			}

			public List<Revision> ReporteTareas(Guid uidUsuario, Guid uidPeriodo, DateTime fecha)
			{
				List<Revision> cumplimientos = new List<Revision>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Revision_Reporte";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidPeriodo", uidPeriodo, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFecha", fecha, SqlDbType.Date);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Revision revision = new Revision();
						revision._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						revision._StrTarea = row["VchTarea"].ToString();
						revision._StrDepartamento = row["VchDepartamento"].ToString();
						revision._StrArea = row["VchArea"].ToString();
						revision._TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString());
						revision._DtFechaHora = row.IsNull("DtFechaHora") ? default(DateTimeOffset) : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						revision._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						revision._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						revision._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						revision._StrOpcion = row["VchOpciones"].ToString();
						revision._StrTipoMedicion = row["VchTipoMedicion"].ToString();
						revision._StrTipoUnidad = row["VchTipoUnidad"].ToString();
						cumplimientos.Add(revision);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Revision", e);
				}
				return cumplimientos;
			}
		}
	}
}
