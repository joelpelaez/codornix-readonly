﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	[Serializable]
	public class EstadoTurno
	{
		private Guid _UidEstadoTurno;
		public Guid UidEstadoTurno
		{
			get { return _UidEstadoTurno; }
			set { _UidEstadoTurno = value; }
		}

		private string _StrEstadoTurno;
		public string StrEstadoTurno
		{
			get { return _StrEstadoTurno; }
			set { _StrEstadoTurno = value; }
		}

		public class Repository
		{
			private Connection conn = new Connection();

			public EstadoTurno Find(Guid uid)
			{
				EstadoTurno estado = null;
				SqlCommand command = new SqlCommand();

				// SP - usp_EstadoTurno_Find
				command.CommandText = "SELECT * FROM EstadoTurno WHERE UidEstadoTurno = '" + uid + "';";
				command.CommandType = CommandType.Text;

				//command.Parameters.Add("@UidEstadoTurno", SqlDbType.UniqueIdentifier);
				//command.Parameters["@UidEstadoTurno"].Value = uid;

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					estado = new EstadoTurno();
					estado._UidEstadoTurno = new Guid(row["UidEstadoTurno"].ToString());
					estado._StrEstadoTurno = row["VchEstadoTurno"].ToString();
					return estado;
				}

				return null;
			}

			public List<EstadoTurno> All()
			{
				List<EstadoTurno> LsEstadosTurno = new List<EstadoTurno>();
				LsEstadosTurno.Add(new EstadoTurno()
				{
					_UidEstadoTurno = Guid.Empty,
					_StrEstadoTurno = "Todos"

				});
				SqlCommand command = new SqlCommand();

				command.CommandText = "SELECT * FROM EstadoTurno";
				command.CommandType = CommandType.Text;

				DataTable table = conn.ExecuteQuery(command);
				foreach (DataRow item in table.Rows)
				{
					LsEstadosTurno.Add(new EstadoTurno()
					{
						_UidEstadoTurno = new Guid(item["UidEstadoTurno"].ToString()),
						_StrEstadoTurno = item["VchEstadoTurno"].ToString()
					});
				}
				return LsEstadosTurno;
			}
		}
	}
}
