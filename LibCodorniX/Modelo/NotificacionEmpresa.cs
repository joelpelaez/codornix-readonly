﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	/// <summary>
	/// Clase modelo del los mensajes de notificacion generados de forma general por empresa
	/// </summary>
	public class NotificacionEmpresa
	{
		#region Atributes
		private Guid _UidNotificacion;
		private Guid _UidCumplimiento;
		private Guid _UidEstadoNotificacion;
		private Guid _UidSucursal;
		private string _Sucursal;
		private int _FolioCumplimiento;
		private int _FolioTarea;
		private string _Tarea;
		private string _Departamento;
		private string _Area;
		private DateTimeOffset _DtFechaCumplimiento;
		private string _EstadoNotificacion;
		private TimeSpan _TsHoraCumplimiento;
		private string _TipoTarea;
		private string _TipoMedicion;
		private string _ValorIngresado;
		private string _UnidadMedida;
		private string _Observaciones;
		private Guid _UidTarea;

		public Guid UidTarea
		{
			get { return _UidTarea; }
			set { _UidTarea = value; }
		}
		public string Observaciones
		{
			get { return _Observaciones; }
			set { _Observaciones = value; }
		}
		public string UnidadMedida
		{
			get { return _UnidadMedida; }
			set { _UnidadMedida = value; }
		}
		public string ValorIngresado
		{
			get { return _ValorIngresado; }
			set { _ValorIngresado = value; }
		}
		public string TipoMedicion
		{
			get { return _TipoMedicion; }
			set { _TipoMedicion = value; }
		}
		public string TipoTarea
		{
			get { return _TipoTarea; }
			set { _TipoTarea = value; }
		}
		public TimeSpan TsHoraCumplimiento
		{
			get { return _TsHoraCumplimiento; }
			set { _TsHoraCumplimiento = value; }
		}
		public string EstadoNotificacion
		{
			get { return _EstadoNotificacion; }
			set { _EstadoNotificacion = value; }
		}
		public DateTimeOffset DtFechaCumplimiento
		{
			get { return _DtFechaCumplimiento; }
			set { _DtFechaCumplimiento = value; }
		}
		public string Area
		{
			get { return _Area; }
			set { _Area = value; }
		}
		public string Departamento
		{
			get { return _Departamento; }
			set { _Departamento = value; }
		}
		public string Tarea
		{
			get { return _Tarea; }
			set { _Tarea = value; }
		}
		public int FolioTarea
		{
			get { return _FolioTarea; }
			set { _FolioTarea = value; }
		}
		public int FolioCumplimiento
		{
			get { return _FolioCumplimiento; }
			set { _FolioCumplimiento = value; }
		}
		public string Sucursal
		{
			get { return _Sucursal; }
			set { _Sucursal = value; }
		}
		public Guid UidSucursal
		{
			get { return _UidSucursal; }
			set { _UidSucursal = value; }
		}
		public Guid UidEstadoNotificacion
		{
			get { return _UidEstadoNotificacion; }
			set { _UidEstadoNotificacion = value; }
		}
		public Guid UidCumplimiento
		{
			get { return _UidCumplimiento; }
			set { _UidCumplimiento = value; }
		}
		public Guid UidNotificacion
		{
			get { return _UidNotificacion; }
			set { _UidNotificacion = value; }
		}
		#endregion
		public NotificacionEmpresa()
		{
		}

		public class Repository
		{
			#region Properties
			Connection _Connection = new Connection();
			#endregion

			public Repository()
			{
			}

			#region Methods
			/// <summary>
			/// Obtener las notificaciones de la empresa a travez del Id de la empresa y parametros de busqueda
			/// </summary>
			/// <param name="UidEmpresa">Identificador de la empresa</param>
			/// <param name="DtFechaInicio">Rango de fecha s[INICIO]</param>
			/// <param name="DtFechaFin">Rango de fecha [FIN]</param>
			/// <param name="UidEstadoNotificacion">Identificador del estado de la Notificacion</param>
			/// <returns></returns>
			public List<NotificacionEmpresa> Busqueda(Guid UidEmpresa, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidEstadoNotificacion, Guid UidSucursal, Guid UidDepartamento, Guid UidArea)
			{
				List<NotificacionEmpresa> LsNotificaciones = new List<NotificacionEmpresa>();
				SqlCommand Query = new SqlCommand();
				try
				{
					Query.CommandText = "usp_NotificacionEmpresa_Search";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidEmpresa", UidEmpresa, SqlDbType.UniqueIdentifier);

					if (DtFechaInicio != null)
						Query.AddParameter("@DtFechaInicio", DtFechaInicio.Value, SqlDbType.Date);

					if (DtFechaFin != null)
						Query.AddParameter("@DtFechaFin", DtFechaFin.Value, SqlDbType.Date);

					if (UidEstadoNotificacion != Guid.Empty)
						Query.AddParameter("@UidEstadoNotificacion", UidEstadoNotificacion, SqlDbType.UniqueIdentifier);

					if (UidSucursal != Guid.Empty)
						Query.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);

					if (UidDepartamento != Guid.Empty)
						Query.AddParameter("@UidDepartamento", UidDepartamento, SqlDbType.UniqueIdentifier);

					if (UidArea != Guid.Empty)
						Query.AddParameter("@UidArea", UidArea, SqlDbType.UniqueIdentifier);

					DataTable Result = _Connection.ExecuteQuery(Query);
					NotificacionEmpresa NE;
					foreach (DataRow item in Result.Rows)
					{
						NE = new NotificacionEmpresa();
						NE.UidNotificacion = new Guid(item["UidMensajeNotificacion"].ToString());
						NE.UidCumplimiento = new Guid(item["UidCumplimiento"].ToString());
						NE.UidSucursal = new Guid(item["UidSucursal"].ToString());
						NE.UidEstadoNotificacion = new Guid(item["UidEstadoNotificacion"].ToString());
						NE.Sucursal = item["VchSucursal"].ToString();
						NE.Tarea = item["VchTarea"].ToString();
						NE.DtFechaCumplimiento = DateTimeOffset.Parse(item["FechaCumplimiento"].ToString());
						NE.Departamento = item["VchDepartamento"].ToString();
						NE.Area = item.IsNull("VchArea") ? "(global)" : item["VchArea"].ToString();
						NE.EstadoNotificacion = item["VchEstadoNotificacion"].ToString();
						NE.FolioCumplimiento = int.Parse(item["FolioCumplimiento"].ToString());
						NE.FolioTarea = int.Parse(item["FolioTarea"].ToString());

						LsNotificaciones.Add(NE);
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsNotificaciones;
			}

			/// <summary>
			/// Obtener datos del mensaje notificacion a travez del Id
			/// </summary>
			/// <param name="UidMensajeNotificacion">Identificador del Mesanje de la notificacion</param>
			/// <returns></returns>
			public NotificacionEmpresa GetById(Guid UidMensajeNotificacion)
			{
				NotificacionEmpresa NE = new NotificacionEmpresa();
				SqlCommand Query = new SqlCommand();
				try
				{
					Query.CommandText = "usp_MensajeNotificacionEmpresa_FindById";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidNotificacion", UidMensajeNotificacion, SqlDbType.UniqueIdentifier);

					DataTable Result = _Connection.ExecuteQuery(Query);
					foreach (DataRow row in Result.Rows)
					{
						NE.UidNotificacion = new Guid(row["UidMensajeNotificacion"].ToString());
						NE.EstadoNotificacion = row["VchEstadoNotificacion"].ToString();
						NE.DtFechaCumplimiento = DateTimeOffset.Parse(row["FechaEjecucion"].ToString());
						NE.TsHoraCumplimiento = TimeSpan.Parse(row["HoraEjecucion"].ToString());
						NE.Tarea = row["VchTarea"].ToString();
						NE.TipoTarea = row["VchTipoTarea"].ToString();
						NE.TipoMedicion = row["VchTipoMedicion"].ToString();
						NE.ValorIngresado = row["ValorIngresado"].ToString();
						NE.UnidadMedida = row.IsNull("VchTipoUnidad") ? string.Empty : row["VchTipoUnidad"].ToString();
						NE.Sucursal = row["VchSucursal"].ToString();
						NE.Departamento = row["VchDepartamento"].ToString();
						NE.Area = row.IsNull("VchArea") ? "(global)" : row["VchArea"].ToString();
						NE.Observaciones = row["VchObservacion"].ToString();
						NE.UidTarea = new Guid(row["UidTarea"].ToString());
					}
				}
				catch (Exception)
				{
					throw;
				}
				return NE;
			}
			#endregion
		}
	}
}
