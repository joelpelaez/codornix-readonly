﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using System.Globalization;

namespace CodorniX.Modelo
{
	[Serializable]
	public class PeriodicidadAnual
	{
		[NonSerialized]
		Conexion Conexion = new Conexion();
		private Guid _UidPeriodicidad;

		public Guid UidPeriodicidad
		{
			get { return _UidPeriodicidad; }
			set { _UidPeriodicidad = value; }
		}
		private int _IntDiasMes;

		public int IntDiasMes
		{
			get { return _IntDiasMes; }
			set { _IntDiasMes = value; }
		}
		private int _IntDiasSemanas;

		public int IntDiasSemanas
		{
			get { return _IntDiasSemanas; }
			set { _IntDiasSemanas = value; }
		}
		private int _IntNumero;

		private string _Tipo;
		public string Tipo
		{
			get { return _Tipo; }
			set { _Tipo = value; }
		}

		public int IntNumero
		{
			get { return _IntNumero; }
			set { _IntNumero = value; }
		}

		public bool GuardarDatos()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();
			if (Conexion == null)
				Conexion = new Conexion();
			try
			{
				Comando.CommandType = CommandType.StoredProcedure;

				Comando.CommandText = "usp_PeriodicidadAnual";

				Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

				Comando.Parameters.Add("@IntDiasMes", SqlDbType.Int);
				Comando.Parameters["@IntDiasMes"].Value = IntDiasMes;

				Comando.Parameters.Add("@IntDiasSemanas", SqlDbType.Int);
				Comando.Parameters["@IntDiasSemanas"].Value = IntDiasSemanas;

				Comando.Parameters.Add("@IntNumero", SqlDbType.Int);
				Comando.Parameters["@IntNumero"].Value = IntNumero;

				Comando.Parameters.Add("@VchTipo", SqlDbType.VarChar, 2);
				Comando.Parameters["@VchTipo"].Value = Tipo;

				Resultado = Conexion.ManipilacionDeDatos(Comando);
				Comando.Dispose();
			}
			catch (Exception)
			{
				throw;
			}
			return Resultado;
		}

		public class Repositorio
		{
			Conexion Conexion = new Conexion();
			public PeriodicidadAnual ConsultarPeriodicidadAnual(Guid Periodicidad)
			{
				PeriodicidadAnual periodicidadanual = null;

				DataTable table = null;

				SqlCommand comando = new SqlCommand();
				// SP-  usp_BuscarPeriodicidadAnual
				comando.CommandText = "select * from PeriodicidadAnual where UidPeriodicidad='" + Periodicidad + "'";
				comando.CommandType = CommandType.Text;

				//comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
				//comando.Parameters["@UidPeriodicidad"].Value = Periodicidad;

				table = Conexion.Busquedas(comando);

				foreach (DataRow row in table.Rows)
				{
					periodicidadanual = new PeriodicidadAnual()
					{
						UidPeriodicidad = (Guid)row["UidPeriodicidad"],
						IntDiasMes = (int)row["IntDiasMes"],
						IntDiasSemanas = (int)row["IntDiasSemanas"],
						IntNumero = (int)row["IntNumero"],
						Tipo = row["VchTipo"].ToString()
					};

				}

				return periodicidadanual;
			}

			public bool Delete(Guid UidPeriodicidad)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "DELETE FROM PeriodicidadAnual WHERE UidPeriodicidad ='" + UidPeriodicidad + "'";
					Query.CommandType = CommandType.Text;

					return this.Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}

			/// <summary>
			/// Actualizar periodicidad anual
			/// </summary>
			/// <param name="UidPeriodicidad"></param>
			/// <param name="DiaMes"></param>
			/// <param name="DiaSemana"></param>
			/// <param name="Numero"></param>
			/// <param name="Tipo"></param>
			/// <returns></returns>
			public bool Actualizar(Guid UidPeriodicidad, int DiaMes, int DiaSemana, int Numero, string Tipo)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_PeriodicidadAnual_Update";
					Query.CommandType = CommandType.StoredProcedure;

					Query.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

					Query.Parameters.Add("@DiaMes", SqlDbType.Int);
					Query.Parameters["@DiaMes"].Value = DiaMes;

					Query.Parameters.Add("@DiaSemana", SqlDbType.Int);
					Query.Parameters["@DiaSemana"].Value = DiaSemana;

					Query.Parameters.Add("@Numero", SqlDbType.Int);
					Query.Parameters["@Numero"].Value = Numero;

					Query.Parameters.Add("@VchTipo", SqlDbType.VarChar, 2);
					Query.Parameters["@VchTipo"].Value = Tipo;

					return this.Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}

			/// <summary>
			/// Calculate date from periodicity type A, Every (N) Years on (Month) (Day of month)
			/// </summary>
			/// <param name="DtToday"></param>
			/// <param name="BlFirstDate"></param>
			/// <param name="IntNumberOfDayOfMonth"></param>
			/// <param name="IntNumberOfMonth"></param>
			/// <param name="IntFrequency"></param>
			/// <returns></returns>
			public DateTime CalculateNextTypeA(DateTime DtToday, bool BlFirstDate, List<DateTime> LsDates, int IntFrequency)
			{
				DateTime DtNext = DtToday;
				LsDates.Sort();

				DateTime DtLast = LsDates[LsDates.Count - 1];
				if (DateTime.Compare(DtLast, DtToday) <= 0)
				{
					DtNext = Convert.ToDateTime(DateTime.ParseExact("01/" + "01" + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					if (BlFirstDate)
						DtNext = DtNext.AddYears(1);
					else
						DtNext = DtNext.AddYears(IntFrequency);
					int IntDay = LsDates[0].Day;
					int IntMonth = LsDates[0].Month;
					int IntYear = DtNext.Year;
					DtNext = Convert.ToDateTime(DateTime.ParseExact(IntDay.ToString("00") + "/" + IntMonth.ToString("00") + "/" + IntYear.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				else
				{
					DateTime AuxDate;
					foreach (DateTime DtDate in LsDates)
					{
						AuxDate = Convert.ToDateTime(DateTime.ParseExact(DtDate.Day.ToString("00") + "/" + DtDate.Month.ToString("00") + "/" + DtNext.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
						if (DateTime.Compare(AuxDate, DtToday) >= 0)
						{
							DtNext = AuxDate;
							break;
						}
					}
				}
				return DtNext;
			}
			/// <summary>
			/// Calculate date from periodicity type B, Every (N) Years on (Month) (Day of week) (Ordinal) 
			/// <para>[ES] Cada (N) años el (Ordinal) (Dia de la semana) del (Mes)</para>
			/// <para>01 Domingo,02 Lunes,03 Martes,04 Miercoles,05 Jueves,06 Viernes,07 Sabado</para>
			/// </summary>
			/// <param name="DtToday"></param>
			/// <param name="BlFirstDate"></param>
			/// <param name="Ordinal"></param>
			/// <param name="IntNumberOfDayOfWeek"></param>
			/// <param name="IntNumberOfMonth"></param>
			/// <param name="IntFrequency"></param>
			/// <returns></returns>
			public DateTime CalculateNextTypeB(DateTime DtToday, bool BlFirstDate, int Ordinal, int IntNumberOfDayOnWeek, int IntNumberOfMonth, int IntFrequency)
			{
				DateTime DtNext = DtToday;

				if (!BlFirstDate)
					DtNext.AddYears(IntFrequency);

				//Number of times the day is on the week, start on 1
				int IntTimesOfDayOnWeek = 1;
				bool IsLocated = false;
				DayOfWeek DOFDayPeriodicity = GetDayOfWeekByNumber(IntNumberOfDayOnWeek);

				if (Ordinal > 0)
				{
					DtNext = Convert.ToDateTime(DateTime.ParseExact("01/" + IntNumberOfMonth.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					int CompareDt = 0;
					while (!IsLocated)
					{
						if (DtNext.DayOfWeek == DOFDayPeriodicity)
						{
							if (IntTimesOfDayOnWeek == Ordinal)
							{
								CompareDt = DateTime.Compare(DtNext, DtToday);
								if (CompareDt < 0)
								{
									DtNext = Convert.ToDateTime(DateTime.ParseExact("01/" + IntNumberOfMonth.ToString("00") + "/" + DtToday.AddYears(1).Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
									IntTimesOfDayOnWeek = 0;
								}
								else
									IsLocated = true;
							}

							IntTimesOfDayOnWeek++;
						}

						if (!IsLocated)
							DtNext = DtNext.AddDays(1);
					}
				}
				else
				{
					DtNext = Convert.ToDateTime(DateTime.ParseExact("01/" + IntNumberOfMonth.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					DtNext = DtNext.AddMonths(1);
					DtNext = DtNext.AddDays(-1);

					while (!IsLocated)
					{
						if (DtNext.DayOfWeek == DOFDayPeriodicity)
						{
							break;
						}
						DtNext = DtNext.AddDays(-1);
					}
				}
				return DtNext;
			}

			public DayOfWeek GetDayOfWeekByNumber(int Day)
			{
				if (Day == 1)
					return DayOfWeek.Sunday;
				else if (Day == 2)
					return DayOfWeek.Monday;
				else if (Day == 3)
					return DayOfWeek.Tuesday;
				else if (Day == 4)
					return DayOfWeek.Wednesday;
				else if (Day == 5)
					return DayOfWeek.Thursday;
				else if (Day == 6)
					return DayOfWeek.Friday;
				else if (Day == 7)
					return DayOfWeek.Saturday;
				else
					return DayOfWeek.Monday;
			}
		}
	}
}
