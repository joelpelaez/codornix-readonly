﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using System.Globalization;

namespace CodorniX.Modelo
{
	[Serializable]
	public class PeriodicidadMensual
	{
		[NonSerialized]
		Conexion Conexion = new Conexion();
		private Guid _UidPeriodicidad;
		public Guid UidPeriodicidad
		{
			get { return _UidPeriodicidad; }
			set { _UidPeriodicidad = value; }
		}

		private int _IntDiasMes;
		public int IntDiasMes
		{
			get { return _IntDiasMes; }
			set { _IntDiasMes = value; }
		}

		private int _IntDiasSemana;
		public int IntDiasSemana
		{
			get { return _IntDiasSemana; }
			set { _IntDiasSemana = value; }
		}

		private string _Tipo;
		public string Tipo
		{
			get { return _Tipo; }
			set { _Tipo = value; }
		}

		private string _DiasMes;
		/// <summary>
		/// Almacena los dias del mes consecutivamente n,n,n...
		/// </summary>
		public string DiasMes
		{
			get { return _DiasMes; }
			set { _DiasMes = value; }
		}

		public bool GuardarDatos()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			if (Conexion == null)
				Conexion = new Conexion();

			try
			{
				Comando.CommandType = CommandType.StoredProcedure;

				Comando.CommandText = "usp_PeriodicidadMensual";

				Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

				Comando.Parameters.Add("@IntDiasMes", SqlDbType.Int);
				Comando.Parameters["@IntDiasMes"].Value = IntDiasMes;

				Comando.Parameters.Add("@IntDiasSemanas", SqlDbType.Int);
				Comando.Parameters["@IntDiasSemanas"].Value = IntDiasSemana;

				Comando.Parameters.Add("@VchTipo", SqlDbType.VarChar, 2);
				Comando.Parameters["@VchTipo"].Value = Tipo;

				Comando.Parameters.Add("@VchDiasMes", SqlDbType.VarChar, 60);
				Comando.Parameters["@VchDiasMes"].Value = DiasMes;

				Resultado = Conexion.ManipilacionDeDatos(Comando);
				Comando.Dispose();
			}
			catch (Exception)
			{
				throw;
			}
			return Resultado;
		}

		public class Repositorio
		{
			Conexion Conexion = new Conexion();
			public PeriodicidadMensual ConsultarPeriodicidadMensual(Guid Periodicidad)
			{
				PeriodicidadMensual periodicidadmensual = null;

				DataTable table = null;

				SqlCommand comando = new SqlCommand();
				// SP -usp_BuscarPeriodicidadMensual
				comando.CommandText = "select * from PeriodicidadMensual where UidPeriodicidad= '" + Periodicidad + "'";
				comando.CommandType = CommandType.Text;

				//comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
				//comando.Parameters["@UidPeriodicidad"].Value = Periodicidad;

				table = Conexion.Busquedas(comando);
				foreach (DataRow row in table.Rows)
				{
					periodicidadmensual = new PeriodicidadMensual()
					{
						UidPeriodicidad = (Guid)row["UidPeriodicidad"],
						IntDiasMes = (int)row["IntDiasMes"],
						IntDiasSemana = (int)row["IntDiasSemana"],
						Tipo = row["VchTipo"].ToString(),
						DiasMes = row.IsNull("VchDiasMes") ? string.Empty : row["VchDiasMes"].ToString()
					};
				}
				return periodicidadmensual;
			}

			public bool Delete(Guid UidPeriodicidad)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "DELETE FROM PeriodicidadMensual WHERE UidPeriodicidad ='" + UidPeriodicidad + "'";
					Query.CommandType = CommandType.Text;

					return this.Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}

			/// <summary>
			/// Actualizar datos de la periodicidad mensual
			/// </summary>
			/// <param name="UidPeriodicidad"></param>
			/// <param name="DiaMes"></param>
			/// <param name="DiaSemana"></param>
			/// <returns></returns>
			public bool Actualizar(Guid UidPeriodicidad, int DiaMes, int DiaSemana, string Tipo, string DiasMes)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_PeriodicidadMensual_Update";
					Query.CommandType = CommandType.StoredProcedure;

					Query.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

					Query.Parameters.Add("@DiaMes", SqlDbType.Int);
					Query.Parameters["@DiaMes"].Value = DiaMes;

					Query.Parameters.Add("@DiaSemana", SqlDbType.Int);
					Query.Parameters["@DiaSemana"].Value = DiaSemana;

					Query.Parameters.Add("@VchTipo", SqlDbType.VarChar, 2);
					Query.Parameters["@VchTipo"].Value = Tipo;

					Query.Parameters.Add("@VchDiasMes", SqlDbType.VarChar, 60);
					Query.Parameters["@VchDiasMes"].Value = DiasMes;

					return this.Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}

			/// <summary>
			/// Calcular la siguiente fecha de la periocidad: El dia (N) de cada (N) Mes.
			/// </summary>
			/// <param name="DtToday"></param>
			/// <param name="BlFirstDate"></param>
			/// <param name="IntFrequency"></param>
			/// <returns></returns>
			public DateTime CalculateNextTypeA(DateTime DtToday, bool BlFirstDate, string DaysOfMonth, int Frecuency)
			{
				DateTime DtNext = DtToday;
				bool IsLocated = false;
				List<int> LsDays = DaysOfMonth.Split(',').Select(int.Parse).ToList();
				LsDays.Sort();
				if (!BlFirstDate)
				{
					if (LsDays[LsDays.Count - 1] == DtToday.Day)
					{
						DtNext = Convert.ToDateTime(DateTime.ParseExact("01/" + DtToday.Month.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
						DtNext = DtNext.AddMonths(Frecuency);
						int IntDay = LsDays[0];
						int IntMonth = DtNext.Month;
						int IntYear = DtNext.Year;
						DtNext = Convert.ToDateTime(DateTime.ParseExact(IntDay.ToString("00") + "/" + IntMonth.ToString("00") + "/" + IntYear.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
					else
					{
						DtNext = DtNext.AddDays(1);
						int IntTotalDaysInMonth = DateTime.DaysInMonth(DtNext.Year, DtNext.Month);
						if (LsDays[LsDays.Count - 1] > IntTotalDaysInMonth)
						{
							DtNext = DtNext.AddMonths(1);
						}
						else
						{
							LsDays.RemoveAll(n => n < DtNext.Day);
							if (LsDays.Count == 0)
							{
								DtNext = DtNext.AddMonths(1);
								LsDays = DaysOfMonth.Split(',').Select(int.Parse).ToList();
								LsDays.Sort();
							}
						}
						int IntDay = LsDays[0];
						int IntMonth = DtNext.Month;
						int IntYear = DtNext.Year;
						DtNext = Convert.ToDateTime(DateTime.ParseExact(IntDay.ToString("00") + "/" + IntMonth.ToString("00") + "/" + IntYear.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
					}
				}
				else
				{
					int IntTotalDaysInMonth = DateTime.DaysInMonth(DtNext.Year, DtNext.Month);
					if (LsDays[LsDays.Count - 1] > IntTotalDaysInMonth)
					{
						DtNext = DtNext.AddMonths(1);
					}
					else
					{
						LsDays.RemoveAll(n => n < DtNext.Day);

						if (LsDays.Count == 0)
						{
							DtNext = DtNext.AddMonths(1);
							LsDays = DaysOfMonth.Split(',').Select(int.Parse).ToList();
							LsDays.Sort();
						}
					}
					int IntDay = LsDays[0];
					int IntMonth = DtNext.Month;
					int IntYear = DtNext.Year;
					DtNext = Convert.ToDateTime(DateTime.ParseExact(IntDay.ToString("00") + "/" + IntMonth.ToString("00") + "/" + IntYear.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
				}
				return DtNext;
			}

			/// <summary>
			/// Calcular siguiente fecha de la periodicidad: El (ORDINAL) (DIA) de cada (N) Mes.
			/// </summary>
			/// <param name="DtToday"></param>
			/// <param name="BlFirstDate"></param>
			/// <param name="IntFrequency"></param>
			/// <returns></returns>
			public DateTime CalculateNextTypeB(DateTime DtToday, bool BlFirstDate, List<FechaPeriodicidad> LsFechas, int IntFrequency)
			{
				DateTime DtNext = DtToday;
				if (!BlFirstDate)
				{
					DtNext = DtNext.AddDays(1);
					int CurrentMonthDays = DateTime.DaysInMonth(DtToday.Year, DtToday.Month);
					if (DtToday.Day >= (CurrentMonthDays - 7))
					{
						DtNext = new DateTime(DtToday.Year, DtToday.Month + IntFrequency, 1);
					}
				}

				foreach (FechaPeriodicidad fpFecha in LsFechas)
				{
					fpFecha.DtFecha = CalculateOrdinalDay(DtNext, fpFecha.IntNumeral, fpFecha.IntDia);
				}

				LsFechas = LsFechas.OrderBy(f => f.DtFecha).ToList();
				foreach (FechaPeriodicidad fpFecha in LsFechas)
				{
					if (DateTime.Compare(fpFecha.DtFecha, DtToday) == 0)
					{
						DtNext = fpFecha.DtFecha;
						break;
					}
					else if (DateTime.Compare(fpFecha.DtFecha, DtToday) > 0)
					{
						DtNext = fpFecha.DtFecha;
						break;
					}
				}
				return DtNext;
			}
			//public DateTime CalculateNextTypeB(DateTime DtToday, bool BlFirstDate, int Ordinal, int NumberDayOfWeek, int IntFrequency)
			//{
			//	///Assign the date from today
			//	DateTime DtNext = DtToday;
			//	///Number of days in the month
			//	///int DaysOnMonth = DateTime.DaysInMonth(DtToday.Year, DtToday.Month);
			//	/// Get the day from DayOfWeek date type, by the number of day
			//	DayOfWeek DayOfPeriodicidy = GetDayOfWeekByNumber(NumberDayOfWeek);
			//	///Boolean to validate if the date is founded
			//	bool IsLocated = false;
			//	///The number of times the day is founded,start on 1
			//	int TimesOfDayOfWeek = 1;

			//	/// Is the first date
			//	if (BlFirstDate)
			//	{
			//		if (Ordinal > 0)
			//		{
			//			DtNext = Convert.ToDateTime(DateTime.ParseExact("01/" + DtToday.Month.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
			//			int DateCompareResult = 0;
			//			DateTime DtAuxNextMont = DtNext.AddMonths(1);

			//			while (!IsLocated)
			//			{
			//				if (DtNext.DayOfWeek == DayOfPeriodicidy)
			//				{
			//					DateCompareResult = DateTime.Compare(DtNext, DtToday);
			//					if (DateCompareResult >= 0)
			//					{
			//						if (TimesOfDayOfWeek == Ordinal)
			//						{
			//							IsLocated = true;
			//							break;
			//						}
			//					}

			//					TimesOfDayOfWeek++;
			//				}
			//				DtNext = DtNext.AddDays(1);
			//				DateCompareResult = DateTime.Compare(DtNext, DtAuxNextMont);
			//				if (DateCompareResult == 0)
			//				{
			//					DtAuxNextMont = DtNext.AddMonths(1);
			//					TimesOfDayOfWeek = 1;
			//				}
			//			}
			//		}
			//		else
			//		{
			//			DateTime DtTemp = Convert.ToDateTime(DateTime.ParseExact("01/" + DtToday.Month.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
			//			DtNext = DtTemp.AddMonths(1);
			//			DtNext = DtNext.AddDays(-1);
			//			while (!IsLocated)
			//			{
			//				if (DtNext.DayOfWeek == DayOfPeriodicidy)
			//				{
			//					IsLocated = true;
			//					break;
			//				}
			//				DtNext = DtNext.AddDays(-1);
			//			}
			//		}
			//	}
			//	else
			//	{
			//		if (Ordinal > 0)
			//		{
			//			DateTime DtTemp = Convert.ToDateTime(DateTime.ParseExact("01/" + DtToday.Month.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
			//			DtNext = DtTemp.AddMonths(IntFrequency);
			//			while (!IsLocated)
			//			{
			//				if (DtNext.DayOfWeek == DayOfPeriodicidy)
			//				{
			//					if (TimesOfDayOfWeek == Ordinal)
			//					{
			//						IsLocated = true;
			//						break;
			//					}
			//					TimesOfDayOfWeek++;
			//				}
			//				DtNext = DtNext.AddDays(1);
			//			}
			//		}
			//		else
			//		{
			//			DateTime DtTemp = Convert.ToDateTime(DateTime.ParseExact("01/" + DtToday.Month.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
			//			DtNext = DtTemp.AddMonths(IntFrequency + 1);
			//			DtNext = DtNext.AddDays(-1);
			//			while (!IsLocated)
			//			{
			//				if (DtNext.DayOfWeek == DayOfPeriodicidy)
			//				{
			//					IsLocated = true;
			//					break;
			//				}
			//				DtNext = DtNext.AddDays(-1);
			//			}
			//		}
			//	}

			//	return DtNext;
			//}

			/// <summary>
			/// Calcular siguiente fecha de la periodicidad
			/// <para>Los N (primeros/ultimos/ambos) dias de N meses</para>
			///	<para>1 Primeros</para>
			///	<para>2 Primeros y ultimos</para>
			///	<para>3 Ultimos</para>
			/// </summary>
			/// <param name="DtToday"></param>
			/// <param name="BlFirstDate"></param>
			/// <param name="Ordinal"></param>
			/// <param name="NumberOfDays"></param>
			/// <param name="IntFrequency"></param>
			/// <returns></returns>
			public DateTime CalculateNextTypeC(DateTime DtToday, bool BlFirstDate, int Ordinal, int NumberOfDays, int IntFrequency)
			{
				DateTime DtNextDate = DtToday;
				bool IsLocated = false;
				if (Ordinal == 1)
				{
					if (BlFirstDate)
					{
						if (DtNextDate.Date.Day <= NumberOfDays)
							return DtNextDate;
						else
						{
							DtNextDate = Convert.ToDateTime(DateTime.ParseExact("01/" + DtToday.Month.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
							DtNextDate = DtNextDate.AddMonths(1);
						}
					}
				}
				else if (Ordinal == 3)
				{
					if (BlFirstDate)
					{
						int TotalDaysOfMonth = DateTime.DaysInMonth(DtNextDate.Year, DtNextDate.Month);
						if (DtNextDate.Date.Day > (TotalDaysOfMonth - NumberOfDays))
							return DtNextDate;
						else
						{
							int NewDay = (TotalDaysOfMonth - NumberOfDays) + 1;
							DtNextDate = Convert.ToDateTime(DateTime.ParseExact(NewDay.ToString("00") + "/" + DtNextDate.Month.ToString("00") + "/" + DtNextDate.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
						}
					}
					else
					{
						int TotalDaysOfMonth = DateTime.DaysInMonth(DtNextDate.Year, DtNextDate.Month);
						if (TotalDaysOfMonth == DtNextDate.Day)
						{
							DtNextDate = Convert.ToDateTime(DateTime.ParseExact("01/" + DtToday.Month.ToString("00") + "/" + DtToday.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
							DtNextDate = DtNextDate.AddMonths(IntFrequency);
							TotalDaysOfMonth = DateTime.DaysInMonth(DtNextDate.Year, DtNextDate.Month);
							int NewDay = (TotalDaysOfMonth - NumberOfDays) + 1;
							DtNextDate = Convert.ToDateTime(DateTime.ParseExact(NewDay.ToString("00") + "/" + DtNextDate.Month.ToString("00") + "/" + DtNextDate.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
						}
						else
						{
							DtNextDate = DtNextDate.AddDays(1);
							TotalDaysOfMonth = DateTime.DaysInMonth(DtNextDate.Year, DtNextDate.Month);
							if (DtNextDate.Date.Day > (TotalDaysOfMonth - NumberOfDays))
								return DtNextDate;
							else
							{
								int NewDay = (TotalDaysOfMonth - NumberOfDays) + 1;
								DtNextDate = Convert.ToDateTime(DateTime.ParseExact(NewDay.ToString("00") + "/" + DtNextDate.Month.ToString("00") + "/" + DtNextDate.Year.ToString("00"), "dd/MM/yyyy", CultureInfo.InvariantCulture));
							}
						}
					}
				}
				else
				{
					DtNextDate = DtNextDate.AddDays(1);
				}
				return DtNextDate;
			}

			public DateTime CalculateOrdinalDay(DateTime DtToday, int Ordinal, int DayNumber)
			{
				DateTime DtNext = new DateTime(DtToday.Year, DtToday.Month, 1);
				DayOfWeek DayOfWeek = GetDayOfWeekByNumber(DayNumber);
				bool IsLocated = false;
				int IntTimes = 1;
				if (Ordinal > 0)
				{
					while (!IsLocated)
					{
						if (DtNext.DayOfWeek == DayOfWeek)
						{
							if (IntTimes == Ordinal)
							{
								if (DateTime.Compare(DtNext, DtToday) < 0)
								{
									DtNext = new DateTime(DtToday.Year, DtToday.Month + 1, 1);
									IntTimes = 0;
								}
								else
								{
									IsLocated = true;
									break;
								}
							}
							IntTimes++;
						}

						DtNext = DtNext.AddDays(1);
					}
				}
				else
				{
					int DaysInMonth = DateTime.DaysInMonth(DtNext.Year, DtNext.Month);
					DtNext = new DateTime(DtToday.Year, DtToday.Month, DaysInMonth);
					while (!IsLocated)
					{
						if (DtNext.DayOfWeek == DayOfWeek)
						{
							if (DateTime.Compare(DtNext, DtToday) < 0)
							{
								DaysInMonth = DateTime.DaysInMonth(DtToday.Year, DtToday.Month + 1);
								DtNext = new DateTime(DtToday.Year, DtToday.Month + 1, DaysInMonth);
							}
							else
							{
								IsLocated = true;
								break;
							}
						}
						DtNext = DtNext.AddDays(-1);
					}
				}

				return DtNext;
			}

			private int GetNumberOfDay(DayOfWeek Day)
			{
				if (Day == DayOfWeek.Sunday)
				{
					return 1;
				}
				else if (Day == DayOfWeek.Monday)
				{
					return 2;
				}
				else if (Day == DayOfWeek.Tuesday)
				{
					return 3;
				}
				else if (Day == DayOfWeek.Wednesday)
				{
					return 4;
				}
				else if (Day == DayOfWeek.Thursday)
				{
					return 5;
				}
				else if (Day == DayOfWeek.Friday)
				{
					return 6;
				}
				else if (Day == DayOfWeek.Saturday)
				{
					return 7;
				}
				else
				{
					return 2;
				}
			}

			private DayOfWeek GetDayOfWeekByNumber(int Day)
			{
				if (Day == 1)
					return DayOfWeek.Sunday;
				else if (Day == 2)
					return DayOfWeek.Monday;
				else if (Day == 3)
					return DayOfWeek.Tuesday;
				else if (Day == 4)
					return DayOfWeek.Wednesday;
				else if (Day == 5)
					return DayOfWeek.Thursday;
				else if (Day == 6)
					return DayOfWeek.Friday;
				else if (Day == 7)
					return DayOfWeek.Saturday;
				else
					return DayOfWeek.Monday;
			}
		}
	}
}
