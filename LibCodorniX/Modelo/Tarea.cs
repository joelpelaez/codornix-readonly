﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodorniX.ConexionDB;
using System.Data;
using System.Data.SqlClient;
using CodorniX.Util;

namespace CodorniX.Modelo
{
	[Serializable]
	public class Tarea
	{
		#region Propiedades
		[NonSerialized]
		Conexion conexion = new Conexion();

		private Guid _UidTarea;
		public Guid UidTarea
		{
			get { return _UidTarea; }
			set { _UidTarea = value; }
		}
		public string Id { get { return _UidTarea.ToString(); } }

		private Guid _UidTareaCumplida;
		public Guid UidTareaCumplida
		{
			get { return _UidTareaCumplida; }
			set { _UidTareaCumplida = value; }
		}
		public string strIdTareaCumplida
		{
			get { return _UidTareaCumplida.ToString(); }
		}

		private string _StrNombre;
		public string StrNombre
		{
			get { return _StrNombre; }
			set { _StrNombre = value; }
		}

		private string _StrDescripcion;
		public string StrDescripcion
		{
			get { return _StrDescripcion; }
			set { _StrDescripcion = value; }
		}

		private Guid? _UidAntecesor;
		public Guid? UidAntecesor
		{
			get { return _UidAntecesor; }
			set { _UidAntecesor = value; }
		}
		public string strIdAntecesor
		{
			get { return _UidAntecesor.ToString(); }
		}

		private Guid? _UidUnidadMedida;
		public Guid? UidUnidadMedida
		{
			get { return _UidUnidadMedida; }
			set { _UidUnidadMedida = value; }
		}
		public string strIdUnidadMedida
		{
			get { return _UidUnidadMedida.ToString(); }
		}

		private Guid _UidPeriodicidad;
		public Guid UidPeriodicidad
		{
			get { return _UidPeriodicidad; }
			set { _UidPeriodicidad = value; }
		}
		public string strIdPeriodicidad
		{
			get { return _UidPeriodicidad.ToString(); }
		}

		private string _StrAntecesor;
		public string StrAntecesor
		{
			get { return _StrAntecesor; }
			set { _StrAntecesor = value; }
		}

		private string _StrTipoFrecuencia;
		public string StrTipoFrecuencia
		{
			get { return _StrTipoFrecuencia; }
			set { _StrTipoFrecuencia = value; }
		}

		private string _StrPeriodicidad;
		public string StrPeriodicidad
		{
			get { return _StrPeriodicidad; }
			set { _StrPeriodicidad = value; }
		}

		private int _IntFolio;
		public int IntFolio
		{
			get { return _IntFolio; }
			set { _IntFolio = value; }
		}

		private int _IntOrden;
		public int IntOrden
		{
			get { return _IntOrden; }
			set { _IntOrden = value; }
		}

		// Extra fields
		private Guid _UidDepartamento;
		public Guid UidDepartamento
		{
			get { return _UidDepartamento; }
			set { _UidDepartamento = value; }
		}
		public string strIdDepartamento
		{
			get { return _UidDepartamento.ToString(); }
		}

		private string _StrDepartamento;
		public string StrDepartamento
		{
			get { return _StrDepartamento; }
			set { _StrDepartamento = value; }
		}

		private Guid _UidArea;
		public Guid UidArea
		{
			get { return _UidArea; }
			set { _UidArea = value; }
		}

		private string _StrArea;
		public string StrArea
		{
			get { return _StrArea; }
			set { _StrArea = value; }
		}

		private Guid _UidTurno;
		public Guid UidTurno
		{
			get { return _UidTurno; }
			set { _UidTurno = value; }
		}
		public string strIdTurno
		{
			get { return _UidTurno.ToString(); }
		}

		private string _StrTurno;
		public string StrTurno
		{
			get { return _StrTurno; }
			set { _StrTurno = value; }
		}

		private Guid _UidMedicion;
		public Guid UidMedicion
		{
			get { return _UidMedicion; }
			set { _UidMedicion = value; }
		}
		public string strIdMedicion
		{
			get { return _UidMedicion.ToString(); }
		}

		private TimeSpan? _TmHora;
		public TimeSpan? TmHora
		{
			get { return _TmHora; }
			set { _TmHora = value; }
		}
		public string strTmHora
		{
			get { return _TmHora.ToString(); }
		}

		private int? _IntTolerancia;
		public int? IntTolerancia
		{
			get { return _IntTolerancia; }
			set { _IntTolerancia = value; }
		}
		public string strToleranciaInt { get { return _IntTolerancia.ToString(); } }

		private Guid _UidStatus;
		public Guid UidStatus
		{
			get { return _UidStatus; }
			set { _UidStatus = value; }
		}
		public string strIdStatus
		{
			get { return _UidStatus.ToString(); }
		}

		private Guid _UidTipoTarea;
		public Guid UidTipoTarea
		{
			get { return _UidTipoTarea; }
			set { _UidTipoTarea = value; }
		}
		public string strIdTipoTarea
		{
			get { return _UidTipoTarea.ToString(); }
		}

		private DateTime _DtFechaInicio;
		public DateTime DtFechaInicio
		{
			get { return _DtFechaInicio; }
			set { _DtFechaInicio = value; }
		}
		public string strFechaInicio
		{
			get { return _DtFechaInicio.ToString("dd-MM-yyyy"); }
		}

		private string _StrUsuario;
		public string StrUsuario
		{
			get { return _StrUsuario; }
			set { _StrUsuario = value; }
		}

		private bool _BlFoto;
		public bool BlFoto
		{
			get { return _BlFoto; }
			set { _BlFoto = value; }
		}
		public string strBlFoto { get { return _BlFoto.ToString(); } }

		private string _StrTipoTarea;
		public string StrTipoTarea
		{
			get { return _StrTipoTarea; }
			set { _StrTipoTarea = value; }
		}

		private string _StrStatus;
		public string StrStatus
		{
			get { return _StrStatus; }
			set { _StrStatus = value; }
		}

		private string _StrTipoMedicion;
		public string StrTipoMedicion
		{
			get { return _StrTipoMedicion; }
			set { _StrTipoMedicion = value; }
		}

		private string _StrUnidadMedida;
		public string StrUnidadMedida
		{
			get { return _StrUnidadMedida; }
			set { _StrUnidadMedida = value; }
		}

		private bool _BlCaducado;
		public bool BlCaducado
		{
			get { return _BlCaducado; }
			set { _BlCaducado = value; }
		}

		private bool _BlAutorizado;
		public bool BlAutorizado
		{
			get { return _BlAutorizado; }
			set { _BlAutorizado = value; }
		}

		private bool _BlCreadoSupervisor;
		public bool BlCreadoSupervisor
		{
			get { return _BlCreadoSupervisor; }
			set { _BlCreadoSupervisor = value; }
		}

		public DateTime? DtNuevaFecha
		{
			get;
			set;
		}
		#endregion

		#region Metodos.

		public bool GuardarDatos(Guid uidSucursal)
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			try
			{
				Comando.CommandType = CommandType.StoredProcedure;

				Comando.CommandText = "usp_Tarea_Add";

				Comando.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
				Comando.Parameters["@VchNombre"].Value = StrNombre;

				Comando.Parameters.Add("@VchDescripcion", SqlDbType.NVarChar, 200);
				Comando.Parameters["@VchDescripcion"].Value = StrDescripcion;

				Comando.Parameters.Add("@UidAntecesorTarea", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidAntecesorTarea"].Value = UidAntecesor;

				Comando.Parameters.Add("@UidUnidadMedida", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidUnidadMedida"].Value = UidUnidadMedida;

				Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

				Comando.Parameters.Add("@UidTipoMedicion", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidTipoMedicion"].Value = UidMedicion;

				Comando.Parameters.Add("@TmHora", SqlDbType.Time);
				Comando.Parameters["@TmHora"].Value = TmHora;

				Comando.Parameters.Add("@IntTolerancia", SqlDbType.Int);
				Comando.Parameters["@IntTolerancia"].Value = IntTolerancia;


				Comando.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidTarea"].Direction = ParameterDirection.Output;

				Comando.Parameters.Add("@UidTipoTarea", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidTipoTarea"].Value = UidTipoTarea;

				Comando.Parameters.Add("@UidStatus", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidStatus"].Value = UidStatus;

				Comando.Parameters.Add("@BitFoto", SqlDbType.Bit);
				Comando.Parameters["@BitFoto"].Value = BlFoto;

				Comando.Parameters.Add("@BitCaducado", SqlDbType.Bit);
				Comando.Parameters["@BitCaducado"].Value = BlCaducado;

				Comando.Parameters.Add("@BitAutorizado", SqlDbType.Bit);
				Comando.Parameters["@BitAutorizado"].Value = BlAutorizado;

				Comando.Parameters.Add("@BitCreadoSupervisor", SqlDbType.Bit);
				Comando.Parameters["@BitCreadoSupervisor"].Value = BlCreadoSupervisor;

				Comando.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);

				Resultado = conexion.ManipilacionDeDatos(Comando);
				_UidTarea = (Guid)Comando.Parameters["@UidTarea"].Value;
				Comando.Dispose();




			}
			catch (Exception)
			{
				throw;
			}
			return Resultado;
		}

		public bool ModificarDatos()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			Comando.CommandType = CommandType.StoredProcedure;
			Comando.CommandText = "usp_ModificarTarea";

			Comando.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
			Comando.Parameters["@UidTarea"].Value = _UidTarea;

			Comando.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
			Comando.Parameters["@VchNombre"].Value = StrNombre;

			Comando.Parameters.Add("@VchDescripcion", SqlDbType.NVarChar, 200);
			Comando.Parameters["@VchDescripcion"].Value = StrDescripcion;

			Comando.Parameters.Add("@TmHora", SqlDbType.Time);
			Comando.Parameters["@TmHora"].Value = TmHora;

			Comando.Parameters.Add("@IntTolerancia", SqlDbType.Int);
			Comando.Parameters["@IntTolerancia"].Value = IntTolerancia;

			Comando.Parameters.Add("@UidTipoTarea", SqlDbType.UniqueIdentifier);
			Comando.Parameters["@UidTipoTarea"].Value = UidTipoTarea;

			Comando.Parameters.Add("@UidStatus", SqlDbType.UniqueIdentifier);
			Comando.Parameters["@UidStatus"].Value = UidStatus;

			Comando.Parameters.Add("@BitFoto", SqlDbType.Bit);
			Comando.Parameters["@BitFoto"].Value = BlFoto;

			if (DtNuevaFecha != null)
			{
				Comando.Parameters.Add("@DtFechaInicio", SqlDbType.Date);
				Comando.Parameters["@DtFechaInicio"].Value = DtNuevaFecha;
			}

			if (UidUnidadMedida != Guid.Empty)
			{
				Comando.Parameters.Add("@UidUnidadMedida", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidUnidadMedida"].Value = UidUnidadMedida;
			}

			Resultado = conexion.ManipilacionDeDatos(Comando);

			return Resultado;
		}

		public bool ModificarCaducado()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			Comando.CommandType = CommandType.StoredProcedure;
			Comando.CommandText = "usp_ModificarCaducado";

			Comando.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
			Comando.Parameters["@UidTarea"].Value = _UidTarea;

			Comando.Parameters.Add("@BitCaducado", SqlDbType.Bit);
			Comando.Parameters["@BitCaducado"].Value = BlCaducado;

			Resultado = conexion.ManipilacionDeDatos(Comando);

			return Resultado;
		}

		#endregion
		public class Repositorio
		{
			Conexion Conexion = new Conexion();
			public List<Tarea> Buscar(Criterio criterio)
			{
				List<Tarea> tareas = new List<Tarea>();
				Tarea tarea = null;

				try
				{
					SqlCommand comando = new SqlCommand();
					comando.CommandText = "usp_BuscarTarea";
					comando.CommandType = CommandType.StoredProcedure;

					if (!string.IsNullOrWhiteSpace(criterio.Nombre))
					{
						comando.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
						comando.Parameters["@VchNombre"].Value = criterio.Nombre;
					}

					comando.Parameters.Add("@UidSucursal", SqlDbType.UniqueIdentifier);
					comando.Parameters["@UidSucursal"].Value = criterio.UidSucursal;

					DataTable table = Conexion.Busquedas(comando);

					foreach (DataRow row in table.Rows)
					{
						tarea = new Tarea()
						{
							_UidTarea = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchNombre"].ToString(),
							_StrDescripcion = row["VchDescripcion"].ToString(),
							_UidAntecesor = row.IsNull("UidAntecesorTarea") ? (Guid?)null : new Guid(row["UidAntecesorTarea"].ToString()),
							_UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
							_UidUnidadMedida = row.IsNull("UidUnidadMedida") ? (Guid?)null : new Guid(row["UidUnidadMedida"].ToString()),
							_StrTipoFrecuencia = row["VchTipoFrecuencia"].ToString(),
							_TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString()),
							_IntTolerancia = row.IsNull("IntTolerancia") ? (int?)null : Convert.ToInt32(row["IntTolerancia"].ToString()),
							_UidTipoTarea = new Guid(row["UidTipoTarea"].ToString()),
							_UidStatus = new Guid(row["UidStatus"].ToString()),
							_BlFoto = (bool)row["BitFoto"],
							_IntFolio = (int)row["IntFolio"],
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<Tarea> CargarTarea(Guid UidSucursal)
			{
				List<Tarea> tareas = new List<Tarea>();
				Tarea tarea = null;

				try
				{
					SqlCommand comando = new SqlCommand();
					comando.CommandText = "usp_ConsultarTarea";
					comando.CommandType = CommandType.StoredProcedure;
					comando.Parameters.Add("@UidSucursal", SqlDbType.UniqueIdentifier);
					comando.Parameters["@UidSucursal"].Value = UidSucursal;
					DataTable table = Conexion.Busquedas(comando);

					foreach (DataRow row in table.Rows)
					{
						tarea = new Tarea()
						{
							_UidTarea = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchNombre"].ToString(),
							_StrDescripcion = row["VchDescripcion"].ToString(),
							_UidAntecesor = row.IsNull("UidAntecesorTarea") ? (Guid?)null : new Guid(row["UidAntecesorTarea"].ToString()),
							_UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
							_UidUnidadMedida = row.IsNull("UidUnidadMedida") ? (Guid?)null : new Guid(row["UidUnidadMedida"].ToString()),
							_StrTipoFrecuencia = row["VchTipoFrecuencia"].ToString(),
							_TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString()),
							_IntTolerancia = row.IsNull("IntTolerancia") ? (int?)null : Convert.ToInt32(row["IntTolerancia"].ToString()),
							_UidTipoTarea = new Guid(row["UidTipoTarea"].ToString()),
							_UidStatus = new Guid(row["UidStatus"].ToString()),
							_StrUsuario = row["VchUsuario"].ToString(),
							_StrDepartamento = row["VchDepartamento"].ToString(),
							DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString()),
							_BlFoto = (bool)row["BitFoto"],
							_IntFolio = (int)row["IntFolio"],
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<Tarea> BuscarTarea(int FolioTarea, string Nombre, DateTime? Fecha1, DateTime? Fecha2, Guid UidSucursal, string Departamento, string Areas, string Encargado, int folio)
			{
				List<Tarea> tareas = new List<Tarea>();
				Tarea tarea = null;

				try
				{
					SqlCommand comando = new SqlCommand();
					comando.CommandText = "usp_ConsultarTarea";
					comando.CommandType = CommandType.StoredProcedure;

					if (Nombre != string.Empty)
					{
						comando.Parameters.Add("@VchNombre", SqlDbType.NVarChar, 50);
						comando.Parameters["@VchNombre"].Value = Nombre;
					}

					if (Fecha1 != null)
					{
						comando.Parameters.Add("@DtFechainicio", SqlDbType.Date);
						comando.Parameters["@DtFechainicio"].Value = Convert.ToDateTime(Fecha1);
					}

					if (Fecha2 != null)
					{
						comando.Parameters.Add("@DtFechainicio2", SqlDbType.Date);
						comando.Parameters["@DtFechainicio2"].Value = Convert.ToDateTime(Fecha2);
					}

					if (Departamento != string.Empty)
					{
						comando.Parameters.Add("@UidDepartamentos", SqlDbType.NVarChar, 4000);
						comando.Parameters["@UidDepartamentos"].Value = Departamento;
					}

					if (Encargado != string.Empty)
					{
						comando.Parameters.Add("@UidUsuarios", SqlDbType.NVarChar, 400);
						comando.Parameters["@UidUsuarios"].Value = Encargado;
					}
					if (folio != 0)
					{
						comando.AddParameter("@IntFolioAntecesor", folio, SqlDbType.Int);
					}

					if (Areas != string.Empty)
					{
						comando.Parameters.Add("@UidAreas", SqlDbType.NVarChar, 4000);
						comando.Parameters["@UidAreas"].Value = Areas;
					}

					if (FolioTarea > 0)
					{
						comando.Parameters.Add("@FolioTarea", SqlDbType.Int);
						comando.Parameters["@FolioTarea"].Value = FolioTarea;
					}

					comando.Parameters.Add("@UidSucursal", SqlDbType.UniqueIdentifier);
					comando.Parameters["@UidSucursal"].Value = UidSucursal;

					DataTable table = Conexion.Busquedas(comando);

					foreach (DataRow row in table.Rows)
					{
						tarea = new Tarea()
						{
							_UidTarea = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchNombre"].ToString(),
							_StrDescripcion = row["VchDescripcion"].ToString(),
							_UidAntecesor = row.IsNull("UidAntecesorTarea") ? (Guid?)null : new Guid(row["UidAntecesorTarea"].ToString()),
							_UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
							_UidUnidadMedida = row.IsNull("UidUnidadMedida") ? (Guid?)null : new Guid(row["UidUnidadMedida"].ToString()),
							_StrTipoFrecuencia = row["VchTipoFrecuencia"].ToString(),
							_TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString()),
							_IntTolerancia = row.IsNull("IntTolerancia") ? (int?)null : Convert.ToInt32(row["IntTolerancia"].ToString()),
							_UidTipoTarea = new Guid(row["UidTipoTarea"].ToString()),
							_UidStatus = new Guid(row["UidStatus"].ToString()),
							_StrDepartamento = row["VchDepartamento"].ToString(),
							_StrArea = row.IsNull("VchArea") ? "(global)" : row["VchArea"].ToString(),
							DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString()),
							_BlFoto = (bool)row["BitFoto"],
							_IntFolio = (int)row["IntFolio"],
							_BlAutorizado = (bool)row["BitAutorizado"],
							_IntOrden = row.IsNull("Orden") ? -1 : (int)row["Orden"]
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<Tarea> BuscarTareasReporte(Guid UidSucursal, int Folio, string Nombre, DateTime? DtFechaInicio, DateTime? DtFechaFin, string Departamentos, string Areas)
			{
				List<Tarea> LsTareas = new List<Tarea>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Tarea_SearchReporteHistoricoTarea";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);

					if (Folio >= 0)
						Query.AddParameter("@Folio", Folio, SqlDbType.Int);

					if (!string.IsNullOrEmpty(Nombre))
						Query.AddParameter("@Nombre", Nombre, SqlDbType.NVarChar, 50);

					if (DtFechaInicio != null)
						Query.AddParameter("@DtFechaInicio", DtFechaInicio, SqlDbType.Date);

					if (DtFechaFin != null)
						Query.AddParameter("@DtFechaFin", DtFechaFin, SqlDbType.Date);

					if (!string.IsNullOrEmpty(Departamentos))
						Query.AddParameter("@UidDepartamentos", Departamentos, SqlDbType.NVarChar, 4000);

					if (!string.IsNullOrEmpty(Areas))
						Query.AddParameter("@UidAreas", Areas, SqlDbType.NVarChar, 4000);

					DataTable Results = Conexion.Busquedas(Query);
					Tarea _Tarea;
					foreach (DataRow row in Results.Rows)
					{
						_Tarea = new Tarea();
						_Tarea.UidTarea = new Guid(row["UidTarea"].ToString());
						_Tarea.UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString());
						_Tarea.IntFolio = int.Parse(row["IntFolio"].ToString());
						_Tarea.StrNombre = row["VchNombre"].ToString();
						_Tarea.UidUnidadMedida = row.IsNull("UidUnidadMedida") ? Guid.Empty : new Guid(row["UidUnidadMedida"].ToString());
						_Tarea.UidDepartamento = new Guid(row["UidDepartamento"].ToString());
						_Tarea.StrDepartamento = row["VchDepartamento"].ToString();
						_Tarea.UidArea = row.IsNull("UidArea") ? Guid.Empty : new Guid(row["UidArea"].ToString());
						_Tarea.StrArea = row.IsNull("VchArea") ? "(global)" : row["VchArea"].ToString();
						_Tarea.StrTipoFrecuencia = row["VchTipoFrecuencia"].ToString();
						_Tarea.DtFechaInicio = DateTime.Parse(row["DtFechaInicio"].ToString());
						_Tarea._IntOrden = row.IsNull("Orden") ? 1 : (int)row["Orden"];
						LsTareas.Add(_Tarea);
					}
				}
				catch (Exception ex)
				{
					throw;
				}
				return LsTareas;
			}

			public Tarea Encontrar(Guid uid)
			{
				Tarea tarea = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_Tarea_Find";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidTarea"].Value = uid;
				table = Conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					tarea = new Tarea()
					{
						_UidTarea = new Guid(row["UidTarea"].ToString()),
						_StrNombre = row["VchNombre"].ToString(),
						_StrDescripcion = row["VchDescripcion"].ToString(),
						_UidAntecesor = row.IsNull("UidAntecesorTarea") ? (Guid?)null : new Guid(row["UidAntecesorTarea"].ToString()),
						_UidUnidadMedida = row.IsNull("UidUnidadMedida") ? (Guid?)null : new Guid(row["UidUnidadMedida"].ToString()),
						_UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
						_UidMedicion = new Guid(row["UidTipoMedicion"].ToString()),
						_TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString()),
						_IntTolerancia = row.IsNull("IntTolerancia") ? (int?)null : Convert.ToInt32(row["IntTolerancia"].ToString()),
						_UidTipoTarea = new Guid(row["UidTipoTarea"].ToString()),
						_UidStatus = new Guid(row["UidStatus"].ToString()),
						_BlFoto = (bool)row["BitFoto"],
						_StrTipoTarea = row["VchTipoTarea"].ToString(),
						_StrStatus = row["VchStatus"].ToString(),
						_StrTipoMedicion = row["VchTipoMedicion"].ToString(),
						_StrUnidadMedida = row["VchTipoUnidad"].ToString(),
						_IntFolio = (int)row["IntFolio"],
						_BlAutorizado = row.IsNull("BitAutorizado") ? false : (bool)row["BitAutorizado"],
						_BlCreadoSupervisor = row.IsNull("BitCreadoSupervisor") ? false : (bool)row["BitCreadoSupervisor"],
						_UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
						_UidArea = row.IsNull("UidArea") ? Guid.Empty : new Guid(row["UidArea"].ToString()),
						DtFechaInicio = DateTime.Parse(row["DtFechaInicio"].ToString())
					};
				}

				return tarea;
			}

			public List<Tarea> FindByUser(Guid uidUsuario, Guid? uidTurno, Guid uidSucursal, DateTime fecha)
			{
				List<Tarea> tareas = new List<Tarea>();
				Tarea tarea = null;

				try
				{
					SqlCommand comando = new SqlCommand();
					comando.CommandText = "usp_Tarea_FindByUser";
					comando.CommandType = CommandType.StoredProcedure;

					comando.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					comando.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
					comando.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

					if (uidTurno != null)
					{
						comando.AddParameter("@UidTurno", uidTurno, SqlDbType.UniqueIdentifier);
					}

					DataTable table = Conexion.Busquedas(comando);

					foreach (DataRow row in table.Rows)
					{
						tarea = new Tarea()
						{
							_UidTarea = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchNombre"].ToString(),
							_StrDescripcion = row["VchDescripcion"].ToString(),
							_UidAntecesor = row.IsNull("UidAntecesorTarea") ? (Guid?)null : new Guid(row["UidAntecesorTarea"].ToString()),
							_UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
							_UidUnidadMedida = row.IsNull("UidUnidadMedida") ? (Guid?)null : new Guid(row["UidUnidadMedida"].ToString()),
							_UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
							_UidTurno = new Guid(row["UidTurno"].ToString()),
							_StrDepartamento = row["VchDepartamento"].ToString(),
							_StrTurno = row["VchTurno"].ToString(),
							_IntFolio = (int)row["IntFolio"],
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			/// <summary>
			/// Habilitar tarea que fue creada por el supervisor
			/// Sele asigna un folio a la tarea y se actualiza a 'Activo'
			/// </summary>
			/// <returns></returns>
			public bool HabilitarTareaSupervisor(Guid UidTarea, Guid UidSucursal)
			{
				SqlCommand SqlCommand = new SqlCommand();

				try
				{
					SqlCommand.CommandText = "usp_Tarea_Autorizar";
					SqlCommand.CommandType = CommandType.StoredProcedure;

					SqlCommand.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);
					SqlCommand.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);

					return Conexion.ManipilacionDeDatos(SqlCommand);
				}
				catch (Exception)
				{
					return false;
				}
			}

			public List<Tarea> FindByDepartamentos(string UidsDepartamento, int? Folio, string Descripcion)
			{
				//187.188.15.20:2302
				List<Tarea> LsTareas = new List<Tarea>();
				SqlCommand Query = new SqlCommand();
				try
				{
					Query.CommandText = "usp_HistoricoTarea_FindTareas";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidDepartamento", UidsDepartamento, SqlDbType.VarChar, 999);

					if (Folio != null)
						Query.AddParameter("@Folio", Folio.Value, SqlDbType.Int);

					Query.AddParameter("@Nombre", Descripcion, SqlDbType.NVarChar);

					DataTable Results = Conexion.Busquedas(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsTareas.Add(new Tarea()
						{
							_UidTarea = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchNombre"].ToString(),
							_StrDescripcion = row["VchDescripcion"].ToString(),
							_UidTipoTarea = new Guid(row["UidTipoTarea"].ToString()),
							_StrTipoTarea = row["VchTipoTarea"].ToString(),
							_IntFolio = (int)row["IntFolio"],
							UidDepartamento = row.IsNull("UidDepartamento") ? Guid.Empty : new Guid(row["UidDepartamento"].ToString()),
							UidArea = row.IsNull("UidArea") ? Guid.Empty : new Guid(row["UidArea"].ToString()),
							UidPeriodicidad = row.IsNull("UidPeriodicidad") ? Guid.Empty : new Guid(row["UidPeriodicidad"].ToString())
						});
					}
				}
				catch (Exception e)
				{
					throw;
				}
				return LsTareas;
			}

			public int ObtenerCantidadCumplimientosTarea(Guid UidTarea)
			{
				int Total = 1;
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "SELECT COUNT(UidCumplimiento) as IntCumplimientos FROM Cumplimiento WHERE UidTarea='" + UidTarea + "'";
					Query.CommandType = CommandType.Text;

					Total = this.Conexion.Count(Query);

				}
				catch (Exception ex)
				{

					throw;
				}
				return Total;
			}

			public bool EstablecerOrdenTarea(Guid UidTarea, int Orden)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					// usp_Tarea_ChangeOrden
					Query.CommandText = "UPDATE Tarea SET Orden = '" + Orden + "' WHERE UidTarea = '" + UidTarea + "'";
					Query.CommandType = CommandType.Text;

					//Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);
					//Query.AddParameter("@Orden", Orden, SqlDbType.Int);

					return Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}
		}

		public class Criterio
		{
			public Guid UidSucursal { get; set; }
			public string Nombre { get; set; }
			public string Descripcion { get; set; }
			public Guid UidAntecesor { get; set; }
			public Guid UidUnidadMedida { get; set; }
			public Guid UidPeriodicidad { get; set; }
		}
	}
}
