﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	[Serializable]
	public class Calificacion
	{
		private Guid _UidCalificacion;
		public Guid UidCalificacion
		{
			get => _UidCalificacion;
			set => _UidCalificacion = value;
		}
		public string strIdCalificacion
		{
			get { return _UidCalificacion.ToString(); }
		}

		private string _StrCalificacion;
		public string StrCalificacion
		{
			get => _StrCalificacion;
			set => _StrCalificacion = value;
		}

		public int IntOrden
		{
			get;
			set;
		}

		public Guid UidEmpresa
		{
			get;
			set;
		}

		public bool BlEstatus
		{
			get;
			set;
		}

		public float FlValor
		{
			get;
			set;
		}

		public int Total
		{
			get;
			set;
		}

		public string Resumen
		{
			get
			{
				return this.StrCalificacion + "=" + this.Total;
			}
		}

		public class Repository
		{
			Connection conn = new Connection();

			public List<Calificacion> FindAll()
			{
				List<Calificacion> calificaciones = new List<Calificacion>();
				SqlCommand command = new SqlCommand();

				command.CommandText = "SELECT * FROM Calificacion ORDER BY IntOrden";
				command.CommandType = CommandType.Text;

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					Calificacion calificacion = new Calificacion();
					calificacion._UidCalificacion = new Guid(row["UidCalificacion"].ToString());
					calificacion._StrCalificacion = row["VchCalificacion"].ToString();
					calificaciones.Add(calificacion);
				}

				return calificaciones;
			}

			/// <summary>
			/// Hacer busqueda de calificaciones 			
			/// </summary>
			/// <param name="UidEmpresa"></param>
			/// <param name="Nombre"></param>
			/// <returns></returns>
			public List<Calificacion> Search(Guid UidEmpresa, string Nombre, bool? BlEstatus, float FlValor)
			{
				List<Calificacion> LsCalificaciones = new List<Calificacion>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Calificacion_Search";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidEmpresa", UidEmpresa, SqlDbType.UniqueIdentifier);

					if (Nombre != string.Empty)
						Query.AddParameter("@Nombre", Nombre, SqlDbType.NVarChar, 20);

					if (BlEstatus != null)
						Query.AddParameter("@Estatus", BlEstatus, SqlDbType.Bit);

					if (FlValor >= 0)
						Query.AddParameter("@FlValor", FlValor, SqlDbType.Float);

					DataTable Results = conn.ExecuteQuery(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsCalificaciones.Add(new Calificacion()
						{
							UidCalificacion = new Guid(row["UidCalificacion"].ToString()),
							_StrCalificacion = row["VchCalificacion"].ToString(),
							IntOrden = int.Parse(row["IntOrden"].ToString()),
							UidEmpresa = new Guid(row["UidEmpresa"].ToString()),
							BlEstatus = Convert.ToBoolean(row["Estatus"].ToString()),
							FlValor = row.IsNull("Valor") ? 0 : float.Parse(row["Valor"].ToString())
						});
					}
				}
				catch (Exception)
				{

					throw;
				}
				return LsCalificaciones;
			}

			public List<Calificacion> ResumenTurno(Guid UidDepartamento, DateTime DtFechaTurno)
			{
				List<Calificacion> LsCalificaciones = new List<Calificacion>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Calificacion_ResumenTurno";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidDepartamento", UidDepartamento, SqlDbType.UniqueIdentifier);

					Query.AddParameter("@DtFechaTurno", DtFechaTurno, SqlDbType.Date);

					DataTable Results = conn.ExecuteQuery(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsCalificaciones.Add(new Calificacion()
						{
							UidCalificacion = new Guid(row["UidCalificacion"].ToString()),
							_StrCalificacion = row["VchCalificacion"].ToString(),
							FlValor = row.IsNull("Valor") ? 0 : float.Parse(row["Valor"].ToString()),
							Total = (int)row["Total"]
						});
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsCalificaciones;
			}

			/// <summary>
			/// Obtener calificacion mediante su identificador
			/// </summary>
			/// <param name="UidCalificacion"></param>
			/// <returns></returns>
			public Calificacion FindById(Guid UidCalificacion)
			{
				Calificacion cCalificacion = new Calificacion();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "SELECT * FROM Calificacion WHERE UidCalificacion = '" + UidCalificacion + "'";
					Query.CommandType = CommandType.Text;

					DataTable Result = conn.ExecuteQuery(Query);
					foreach (DataRow row in Result.Rows)
					{
						cCalificacion = new Calificacion()
						{
							UidCalificacion = new Guid(row["UidCalificacion"].ToString()),
							_StrCalificacion = row["VchCalificacion"].ToString(),
							IntOrden = int.Parse(row["IntOrden"].ToString()),
							UidEmpresa = new Guid(row["UidEmpresa"].ToString()),
							BlEstatus = Convert.ToBoolean(row["Estatus"].ToString()),
							FlValor = row.IsNull("Valor") ? 0 : float.Parse(row["Valor"].ToString())
						};
					}
				}
				catch (Exception)
				{

					throw;
				}
				return cCalificacion;
			}

			/// <summary>
			/// Agregar nuevo registro de una calificacion
			/// </summary>
			/// <param name="UidEmpresa"></param>
			/// <param name="Orden"></param>
			/// <param name="Nombre"></param>
			/// <returns></returns>
			public bool Add(Guid UidEmpresa, int Orden, string Nombre, bool BlEstatus, float FlValor)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Calificacion_Add";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidEmpresa", UidEmpresa, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@Nombre", Nombre, SqlDbType.NVarChar, 20);
					Query.AddParameter("@Orden", Orden, SqlDbType.Int);
					Query.AddParameter("@Estatus", BlEstatus, SqlDbType.Bit);
					Query.AddParameter("@FlValor", FlValor, SqlDbType.Float);

					return conn.ExecuteCommand(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}

			/// <summary>
			/// Actualizar el registro de una califacacion
			/// </summary>
			/// <param name="UidCalificacion"></param>
			/// <param name="Nombre"></param>
			/// <param name="Orden"></param>
			/// <returns></returns>
			public bool Update(Guid UidCalificacion, string Nombre, int Orden, bool BlEstatus, float FlValor)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Calificacion_Update";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidCalificacion", UidCalificacion, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@Nombre", Nombre, SqlDbType.NVarChar, 20);
					Query.AddParameter("@Orden", Orden, SqlDbType.Int);
					Query.AddParameter("@Estatus", BlEstatus, SqlDbType.Bit);
					Query.AddParameter("@FlValor", FlValor, SqlDbType.Float);

					return conn.ExecuteCommand(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}
		}
	}
}
