﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace CodorniX.Modelo
{
	[Serializable]
	public class Encargado : Usuario
	{
		private int _IntMaxAsignaciones;

		public int IntMaxAsignaciones
		{
			get { return _IntMaxAsignaciones; }
			set { _IntMaxAsignaciones = value; }
		}

		public new class Repository : Usuario.Repository
		{

			public void ModificarEncargado(Guid uidUsuario, int maxAsignaciones)
			{
				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_Encargado_ModificarNumeroAsignaciones";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
				command.AddParameter("@IntMaxAsignaciones", maxAsignaciones, SqlDbType.Int);

				new Connection().ExecuteCommand(command);
			}

			public void AgregarEncargado(Guid uidUsuario, int maxAsignaciones)
			{
				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_Encargado_AgregarNumeroAsignaciones";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
				command.AddParameter("@IntMaxAsignaciones", maxAsignaciones, SqlDbType.Int);

				new Connection().ExecuteCommand(command);
			}

			/// <summary>
			/// Obtener nombre de todos los encargados de la empresa con un periodo
			/// </summary>
			/// <param name="UidEmpresa"></param>
			/// <returns></returns>
			public List<Encargado> FindAll(Guid UidEmpresa)
			{
				List<Encargado> LsEncargados = new List<Encargado>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "SELECT U.UidUsuario,CONCAT(U.VchNombre,' ',U.VchApellidoPaterno,' ',U.VchApellidoMaterno) AS VchUsuario FROM UsuarioPerfilSucursal US INNER JOIN Usuario U ON U.UidUsuario = US.UidUsuario	INNER JOIN Sucursal S ON S.UidSucursal = US.UidSucursal	INNER JOIN Periodo P ON P.UidUsuario = US.UidUsuario WHERE S.UidEmpresa = '" + UidEmpresa + "' GROUP BY U.UidUsuario,U.VchNombre,U.VchApellidoPaterno,U.VchApellidoMaterno";
					Query.CommandType = CommandType.Text;

					DataTable Results = new Connection().ExecuteQuery(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsEncargados.Add(new Encargado()
						{
							UIDUSUARIO = new Guid(row["UidUsuario"].ToString()),
							STRNOMBRE = row["VchUsuario"].ToString()
						});
					}
				}
				catch (Exception ex)
				{
					throw;
				}
				return LsEncargados;
			}

			public int ObtenerNumeroAsignaciones(Guid uidUsuario)
			{
				SqlCommand command = new SqlCommand();
				command.CommandText = "SELECT CASE WHEN UidUsuario IS NULL THEN 0 ELSE IntMaxAsignaciones END AS IntMaxAsignaciones FROM Encargado WHERE UidUsuario = @id";
				command.CommandType = CommandType.Text;

				command.AddParameter("@id", uidUsuario, SqlDbType.UniqueIdentifier);

				DataTable table = new Connection().ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					return (int)row["IntMaxAsignaciones"];
				}

				return 1;
			}

			public List<Encargado> FindByName(string nombre, string apellidoPaterno, string apellidoMaterno, string uidSucursal)
			{
				List<Encargado> usuarios = new List<Encargado>();
				Encargado usuario = null;
				SqlCommand command = new SqlCommand();

				command.CommandText = "sp_BuscarEncargado";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@VchNombre", nombre, SqlDbType.NVarChar, 50);
				command.AddParameter("@VchApellidoPaterno", apellidoPaterno, SqlDbType.NVarChar, 50);
				command.AddParameter("@VchApellidoMaterno", apellidoMaterno, SqlDbType.NVarChar, 50);
				command.AddParameter("@VchSucursal", uidSucursal, SqlDbType.NVarChar, 500);

				DataTable table = new Connection().ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					usuario = new Encargado()
					{
						_UidUsuario = (Guid)row["UidUsuario"],
						_strNombre = row["VchNombre"].ToString(),
						_strApellidoPaterno = row["VchApellidoPaterno"].ToString(),
						_strApellidoMaterno = row["VchApellidoMaterno"].ToString(),
						_DtFechaNacimiento = Convert.ToDateTime(row["DtFechaNacimiento"].ToString()),
						_strCorreo = row["VchCorreo"].ToString(),
						_DtFechaInicio = Convert.ToDateTime(row["DtFechaInicio"].ToString()),
						_DtFechaFin = row.IsNull("DtFechaFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaFin"].ToString()),
						_strUsuario = row["VchUsuario"].ToString(),
						_strPassword = row["VchPassword"].ToString(),
						// _UidPerfil = new Guid(row["UidPerfil"].ToString()),
						_UidStatus = new Guid(row["UidStatus"].ToString()),
						_IntMaxAsignaciones = row.IsNull("IntMaxAsignaciones") ? 1 : Convert.ToInt32(row["IntMaxAsignaciones"].ToString()),
						_StrPerfil = row["VchPerfil"].ToString()
					};

					usuarios.Add(usuario);
				}

				return usuarios;
			}
		}
	}
}