﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using CodorniX.Util;

namespace CodorniX.Modelo
{
	public class TipoFrecuencia
	{
		private Guid _UidTipoFrecuencia;
		public Guid UidTipoFrecuencia
		{
			get { return _UidTipoFrecuencia; }
			set { _UidTipoFrecuencia = value; }
		}
		public string strIdTipoFrecuencia { get { return _UidTipoFrecuencia.ToString(); } }

		private string _StrTipoFrecuencia;
		public string StrTipoFrecuencia
		{
			get { return _StrTipoFrecuencia; }
			set { _StrTipoFrecuencia = value; }
		}
		public class Repositorio
		{
			Conexion Conexion = new Conexion();
			public TipoFrecuencia Buscar(Criterio criterio)
			{
				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_ConsultarTipoFrecuencia";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@VchTipoFrecuencia", criterio.TipoFrecuencia, SqlDbType.NVarChar, 50);
					command.AddParameter("@UidTipoFrecuencia", criterio.UidTipoFrecuencia, SqlDbType.UniqueIdentifier);

					DataTable table = new Connection().ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						TipoFrecuencia tipofrecuencia = new TipoFrecuencia()
						{
							UidTipoFrecuencia = (Guid)row["UidTipoFrecuencia"],
							StrTipoFrecuencia = (string)row["VchTipoFrecuencia"]
						};
						return tipofrecuencia;
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching NivelAcceso", e);
				}

				return null;
			}
			public TipoFrecuencia FindByTarea(Guid UidTarea)
			{
				TipoFrecuencia Tipo = new TipoFrecuencia();
				SqlCommand Command = new SqlCommand();
				try
				{
					Command.CommandText = "SELECT T.UidTipoFrecuencia,T.VchTipoFrecuencia FROM Tarea S, TipoFrecuencia T INNER JOIN Periodicidad P ON P.UidTipoFrecuencia=T.UidTipoFrecuencia WHERE P.UidPeriodicidad=S.UidPeriodicidad AND S.UidTarea='"+UidTarea.ToString()+"'";
					Command.CommandType = CommandType.Text;

					DataTable Table = Conexion.Busquedas(Command);
					foreach (DataRow item in Table.Rows)
					{
						Tipo = new TipoFrecuencia() {
							UidTipoFrecuencia = new Guid(item["UidTipoFrecuencia"].ToString()),
							StrTipoFrecuencia = item["VchTipoFrecuencia"].ToString()
						};
					}
				}
				catch (Exception)
				{
					throw;
				}
				return Tipo;
			}
		}

		public class Criterio
		{
			public string TipoFrecuencia { get; set; }
			public Guid UidTipoFrecuencia { get; set; }
		}

	}
}
