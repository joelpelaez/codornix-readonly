﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CodorniX.Modelo
{
	[Serializable]
	public class UnidadMedida
	{
		private Guid _UidUnidadMedida;
		public Guid UidUnidadMedida
		{
			get { return _UidUnidadMedida; }
			set { _UidUnidadMedida = value; }
		}

		private string _StrTipoUnidad;
		public string StrTipoUnidad
		{
			get { return _StrTipoUnidad; }
			set { _StrTipoUnidad = value; }
		}

		private Guid _UidEmpresa;
		public Guid UidEmpresa
		{
			get { return _UidEmpresa; }
			set { _UidEmpresa = value; }
		}

		private bool _BlEstatus;
		public bool BlEstatus
		{
			get { return _BlEstatus; }
			set { _BlEstatus = value; }
		}

		public class Repositorio
		{			
			public List<UnidadMedida> ConsultarUnidadMedida()
			{
				List<UnidadMedida> unidades = new List<UnidadMedida>();

				SqlCommand comando = new SqlCommand();

				try
				{
					comando.CommandText = "usp_ConsultarUnidadMedida";
					comando.CommandType = CommandType.StoredProcedure;

					DataTable table = new Connection().ExecuteQuery(comando);

					foreach (DataRow row in table.Rows)
					{
						UnidadMedida unidad = new UnidadMedida()
						{
							UidUnidadMedida = (Guid)row["UidUnidadMedida"],
							StrTipoUnidad = (string)row["VchTipoUnidad"],
						};
						unidades.Add(unidad);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return unidades;


			}

			/// <summary>
			/// Realizar busqueda de las unidades de medida pertenecientes a la empresa
			/// </summary>
			/// <param name="UidEmpresa"></param>
			/// <param name="UnidadMedida"></param>
			/// <param name="BlEstatus"></param>
			/// <returns></returns>
			public List<UnidadMedida> Busqueda(Guid UidEmpresa, string UnidadMedida, bool? BlEstatus)
			{
				List<UnidadMedida> unidades = new List<UnidadMedida>();
				SqlCommand comando = new SqlCommand();

				try
				{
					comando.CommandText = "usp_UnidadMedida_Search";
					comando.CommandType = CommandType.StoredProcedure;

					comando.Parameters.Add("@UidEmpresa", SqlDbType.UniqueIdentifier);
					comando.Parameters["@UidEmpresa"].Value = UidEmpresa;

					if (UnidadMedida != string.Empty)
					{
						comando.Parameters.Add("@UnidadMedida", SqlDbType.NVarChar, 10);
						comando.Parameters["@UnidadMedida"].Value = UnidadMedida;
					}

					if (BlEstatus != null)
					{
						comando.Parameters.Add("@BlEstatus", SqlDbType.Bit);
						comando.Parameters["@BlEstatus"].Value = BlEstatus;
					}

					DataTable table = new Connection().ExecuteQuery(comando);

					foreach (DataRow row in table.Rows)
					{
						UnidadMedida unidad = new UnidadMedida()
						{
							UidUnidadMedida = (Guid)row["UidUnidadMedida"],
							StrTipoUnidad = (string)row["VchTipoUnidad"],
							BlEstatus = (bool)row["BitEstatus"]
						};
						unidades.Add(unidad);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return unidades;
			}

			/// <summary>
			/// Obtener unidad de medida mediante su identificador
			/// </summary>
			/// <param name="Uid"></param>
			/// <returns></returns>
			public UnidadMedida Find(Guid Uid)
			{
				UnidadMedida _UnidadMedida = new UnidadMedida();
				SqlCommand Command = new SqlCommand();
				try
				{
					Command.CommandText = "SELECT * FROM UnidadMedida WHERE UidUnidadMedida = '" + Uid.ToString() + "'";
					Command.CommandType = CommandType.Text;

					DataTable table = new Connection().ExecuteQuery(Command);
					foreach (DataRow item in table.Rows)
					{
						_UnidadMedida = new UnidadMedida()
						{
							UidUnidadMedida = new Guid(item["UidUnidadMedida"].ToString()),
							StrTipoUnidad = item["VchTipoUnidad"].ToString(),
							BlEstatus = (bool)item["BitEstatus"]
						};
					}
				}
				catch (Exception)
				{

					throw;
				}
				return _UnidadMedida;
			}

			/// <summary>
			/// Agregar nuevo registro
			/// </summary>
			/// <param name="UnidadMedida"></param>
			/// <param name="UidEmpresa"></param>
			/// <param name="BlEstatus"></param>
			/// <returns></returns>
			public bool Add(string UnidadMedida, Guid UidEmpresa, bool BlEstatus)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_UnidadMedida_Add";
					Query.CommandType = CommandType.StoredProcedure;

					Query.Parameters.Add("@UidEmpresa", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidEmpresa"].Value = UidEmpresa;

					Query.Parameters.Add("@Unidad", SqlDbType.NVarChar, 10);
					Query.Parameters["@Unidad"].Value = UnidadMedida;

					Query.Parameters.Add("@BlEstatus", SqlDbType.Bit);
					Query.Parameters["@BlEstatus"].Value = BlEstatus;

					return new Connection().ExecuteCommand(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}

			/// <summary>
			/// Actualizar registro existente
			/// </summary>
			/// <param name="UidUnidadMedida"></param>
			/// <param name="UnidadMedida"></param>
			/// <param name="BlEstatus"></param>
			/// <returns></returns>
			public bool Update(Guid UidUnidadMedida, string UnidadMedida, bool BlEstatus)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_UnidadMedida_Update";
					Query.CommandType = CommandType.StoredProcedure;

					Query.Parameters.Add("@UidUnidadMedida", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidUnidadMedida"].Value = UidUnidadMedida;

					Query.Parameters.Add("@Unidad", SqlDbType.NVarChar, 10);
					Query.Parameters["@Unidad"].Value = UnidadMedida;

					Query.Parameters.Add("@BlEstatus", SqlDbType.Bit);
					Query.Parameters["@BlEstatus"].Value = BlEstatus;

					return new Connection().ExecuteCommand(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}
		}
	}
}
