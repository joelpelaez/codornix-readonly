﻿using System;
using CodorniX.ConexionDB;
using CodorniX.Util;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;

namespace CodorniX.Modelo
{
	/// <summary>
	/// Contiene la definicion de atributos de la clase
	/// </summary>
	[Serializable]
	public class FechaPeriodicidad
	{
		/// <summary>
		/// Identificador de la fecha 
		/// </summary>
		public Guid Identificador
		{
			get;
			set;
		}
		/// <summary>
		/// Numero del dia lunes = 0 etc...
		/// </summary>
		public int IntDia
		{
			get;
			set;
		}
		/// <summary>
		/// Numero del mes Enero = 0 etc...
		/// </summary>
		public int IntNumeroMes
		{
			get;
			set;
		}
		/// <summary>
		/// Numeral Primero = 1, Ultimo = -1 etc..
		/// </summary>
		public int IntNumeral
		{
			get;
			set;
		}
		/// <summary>
		/// Nombre del mes
		/// </summary>
		public string Mes
		{
			get;
			set;
		}
		/// <summary>
		/// Texto del numeral
		/// </summary>
		public string Numeral
		{
			get;
			set;
		}
		/// <summary>
		/// Frecuencia Mensual, Anual etc..
		/// </summary>
		public string Frecuencia
		{
			get;
			set;
		}
		/// <summary>
		/// Tipo A, B, C
		/// </summary>
		public string Tipo
		{
			get;
			set;
		}
		/// <summary>
		/// Valor que se imprime el el gridview
		/// </summary>
		public string Text
		{
			get
			{
				return GetText();
			}
		}
		/// <summary>
		/// El registro esta guardado
		/// </summary>
		public bool IsSaved
		{
			get;
			set;
		}
		/// <summary>
		/// El registro debe elminarse
		/// </summary>
		public bool ToDelete
		{
			get;
			set;
		}
		/// <summary>
		/// Calculo de fecha para el (n) dia 
		/// </summary>
		public DateTime DtFecha
		{
			get;
			set;
		}

		public FechaPeriodicidad()
		{

		}

		public FechaPeriodicidad(Guid uidPeriodicidad, int intDia, int intNumeroMes, int intNumeral, string mes, string numeral, string frecuencia, string tipo, bool isSaved)
		{
			Identificador = uidPeriodicidad;
			IntDia = intDia;
			IntNumeroMes = intNumeroMes;
			IntNumeral = intNumeral;
			Mes = mes;
			Numeral = numeral;
			Frecuencia = frecuencia;
			Tipo = tipo;
			this.IsSaved = isSaved;
		}

		public string GetText()
		{
			string DisplayText = string.Empty;
			switch (this.Frecuencia)
			{
				case "Mensual":
					if (this.Tipo.Equals("A"))
						DisplayText = "El dia " + this.IntDia + " del mes";
					else if (this.Tipo.Equals("B"))
						DisplayText = "El " + this.Numeral + " " + NombreDia(this.IntDia) + ".";
					break;
				case "Anual":
					if (this.Tipo.Equals("A"))
						DisplayText = "El " + this.IntDia + " de " + this.Mes;
					else if (this.Tipo.Equals("B"))
						DisplayText = "El " + this.Numeral + " " + NombreDia(this.IntDia) + " de " + this.Mes;
					break;
				default:
					break;
			}
			return DisplayText;
		}

		public string NombreDia(int nDia)
		{
			if (nDia == 2)
				return "Lunes";
			if (nDia == 3)
				return "Martes";
			if (nDia == 4)
				return "Miercoles";
			if (nDia == 5)
				return "Jueves";
			if (nDia == 6)
				return "Viernes";
			if (nDia == 7)
				return "Sabado";
			else
				return "Domingo";
		}

		/// <summary>
		/// Contiene los metodos para Crear,Editar,Eliminar y obtener los datos.
		/// </summary>
		public class Repository
		{
			/// <summary>
			/// Objeto de la conexion a la base de datos
			/// </summary>
			protected Conexion cConexion = new Conexion();

			/// <summary>
			/// Obtiene todos los registros de fechas de la periodicidad
			/// </summary>
			/// <param name="UidPeriodicidad"></param>
			/// <param name="Frecuencia"></param>
			/// <returns></returns>
			public List<FechaPeriodicidad> GetAll(Guid UidPeriodicidad, string Frecuencia)
			{
				List<FechaPeriodicidad> LsFechas = new List<FechaPeriodicidad>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "SELECT * FROM FechaPeriodicidad WHERE UidPeriodicidad='" + UidPeriodicidad + "' ORDER BY IntMes ASC,IntDia ASC";
					Query.CommandType = CommandType.Text;

					DataTable Results = this.cConexion.Busquedas(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsFechas.Add(new FechaPeriodicidad(
								(Guid)row["UidFechaPeriodicidad"],
								(int)row["IntDia"],
								(int)row["IntMes"],
								(int)row["IntNumeral"],
								GetMonthNameByNumber((int)row["IntMes"]),
								GetOrdinalByNumber((int)row["IntNumeral"]),
								Frecuencia,
								row["VchTipo"].ToString(),
								true
							));
					}
				}
				catch (Exception)
				{

					throw;
				}
				return LsFechas;
			}

			/// <summary>
			/// Agregar un nuevo registro
			/// </summary>
			/// <param name="UidPeriodicidad"></param>
			/// <param name="IntNumeral"></param>
			/// <param name="IntDia"></param>
			/// <param name="IntMes"></param>
			/// <param name="Tipo"></param>
			/// <returns></returns>
			public bool Add(Guid UidPeriodicidad, int IntNumeral, int IntDia, int IntMes, string Tipo)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_FechaPeriodicidad_Add";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidPeriodicidad", UidPeriodicidad, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@IntNumeral", IntNumeral, SqlDbType.Int);
					Query.AddParameter("@IntDia", IntDia, SqlDbType.Int);
					Query.AddParameter("@IntMes", IntMes, SqlDbType.Int);
					Query.AddParameter("@VchTipo", Tipo, SqlDbType.VarChar, 2);

					return this.cConexion.ManipilacionDeDatos(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}

			/// <summary>
			/// Actualizar un registro existente
			/// </summary>
			/// <param name="Uid"></param>
			/// <param name="IntNumeral"></param>
			/// <param name="IntDia"></param>
			/// <param name="IntMes"></param>
			/// <param name="Tipo"></param>
			/// <returns></returns>
			public bool Update(Guid Uid, int IntNumeral, int IntDia, int IntMes, string Tipo)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_FechaPeriodicidad_Update";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidFechaPeriodicidad", Uid, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@IntNumeral", IntNumeral, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@IntDia", IntDia, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@IntMes", IntMes, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@VchTipo", Tipo, SqlDbType.UniqueIdentifier);

					return this.cConexion.ManipilacionDeDatos(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}

			/// <summary>
			/// Elimina todos los registros de la periodicidad
			/// </summary>
			/// <param name="UidPeriodicidad"></param>
			/// <returns></returns>
			public bool DeleteAll(Guid UidPeriodicidad)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "DELETE FROM FechaPeriodicidad WHERE UidPeriodicidad='" + UidPeriodicidad + "'";
					Query.CommandType = CommandType.Text;

					return this.cConexion.ManipilacionDeDatos(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}

			/// <summary>
			/// Elimina un registro mediante su identificador
			/// </summary>
			/// <param name="Uid"></param>
			/// <returns></returns>
			public bool DeleteItem(Guid Uid)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "DELETE FROM FechaPeriodicidad WHERE UidFechaPeriodicidad='" + Uid + "'";
					Query.CommandType = CommandType.Text;

					return this.cConexion.ManipilacionDeDatos(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}

			/// <summary>
			/// Regresa el nombre del mes de acuero al numero
			/// </summary>
			/// <param name="Number"></param>
			/// <returns></returns>
			private string GetMonthNameByNumber(int Number)
			{
				string Name = string.Empty;
				switch (Number)
				{
					case 1:
						Name = "Enero";
						break;
					case 2:
						Name = "Febrero";
						break;
					case 3:
						Name = "Marzo";
						break;
					case 4:
						Name = "Abril";
						break;
					case 5:
						Name = "Mayo";
						break;
					case 6:
						Name = "Junio";
						break;
					case 7:
						Name = "Julio";
						break;
					case 8:
						Name = "Agosto";
						break;
					case 9:
						Name = "Septiembre";
						break;
					case 10:
						Name = "Octubre";
						break;
					case 11:
						Name = "Noviembre";
						break;
					case 12:
						Name = "Diciembre";
						break;
					default:
						break;
				}
				return Name;
			}

			private string GetOrdinalByNumber(int Number)
			{
				string StrOrdinal = string.Empty;
				switch (Number)
				{
					case 1:
						StrOrdinal = "Primer";
						break;
					case 2:
						StrOrdinal = "Segundo";
						break;
					case 3:
						StrOrdinal = "Tercer";
						break;
					case 4:
						StrOrdinal = "Cuarto";
						break;
					case -1:
						StrOrdinal = "Ultimo";
						break;
					default:
						break;
				}

				return StrOrdinal;
			}
		}
	}
}
