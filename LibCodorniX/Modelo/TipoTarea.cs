﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
	public class TipoTarea
	{
		private Guid _UidTipoTarea;
		public Guid UidTipoTarea
		{
			get { return _UidTipoTarea; }
			set { _UidTipoTarea = value; }
		}
		public string strIdTarea
		{
			get
			{
				return this._UidTipoTarea.ToString();
			}
		}

		private string _StrTipoTarea;
		public string StrTipoTarea
		{
			get { return _StrTipoTarea; }
			set { _StrTipoTarea = value; }
		}


		public class Repositorio
		{
			public List<TipoTarea> ConsultarTipoTarea()
			{
				List<TipoTarea> TiposTareas = new List<TipoTarea>();

				SqlCommand comando = new SqlCommand();

				try
				{
					// usp_ConsultarTipoTarea
					comando.CommandText = "select * from TipoTarea";
					comando.CommandType = CommandType.Text;

					DataTable table = new Connection().ExecuteQuery(comando);

					foreach (DataRow row in table.Rows)
					{
						TipoTarea tipotarea = new TipoTarea()
						{
							UidTipoTarea = (Guid)row["UidTipoTarea"],
							StrTipoTarea = (string)row["VchTipoTarea"],
						};
						TiposTareas.Add(tipotarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return TiposTareas;


			}

			public TipoTarea FindByTarea(Guid UidTarea)
			{
				TipoTarea TipoTarea = new TipoTarea();
				SqlCommand Command = new SqlCommand();
				try
				{
					Command.CommandText = "SELECT O.* FROM TipoTarea O, Tarea T WHERE O.UidTipoTarea=T.UidTipoTarea AND T.UidTarea='" + UidTarea.ToString() + "'";
					Command.CommandType = CommandType.Text;
					DataTable Table = new Connection().ExecuteQuery(Command);
					foreach (DataRow item in Table.Rows)
					{
						TipoTarea = new TipoTarea()
						{
							_UidTipoTarea = new Guid(item["UidTipoTarea"].ToString()),
							_StrTipoTarea = item["VchTipoTarea"].ToString()
						};
					}
				}
				catch (Exception)
				{
					throw;
				}
				return TipoTarea;
			}
		}
	}
}
