﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
	public class Ajustes
	{
		public Guid UidEmpresa
		{
			get;
			set;
		}
		public bool BlRevisionCualitativa
		{
			get;
			set;
		}

		public class Repository
		{
			protected Conexion _Conexion = new Conexion();

			/// <summary>
			/// Obtener ajustes de realizar la revision de tareas
			/// </summary>
			/// <param name="UidEmpresa"></param>
			/// <returns></returns>
			public Ajustes GetAjustesRevision(Guid UidEmpresa)
			{
				Ajustes aAjustes = new Ajustes();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "SELECT * FROM AjusteRevision WHERE UidEmpresa='" + UidEmpresa + "'";
					Query.CommandType = CommandType.Text;

					DataTable Results = _Conexion.Busquedas(Query);
					foreach (DataRow row in Results.Rows)
					{
						aAjustes = new Ajustes()
						{
							UidEmpresa = new Guid(row["UidEmpresa"].ToString()),
							BlRevisionCualitativa = (bool)row["BitCualitativa"]
						};

					};
				}
				catch (Exception ex)
				{
					throw;
				}
				return aAjustes;
			}

			public bool SetAjustesRevision(Guid UidEmpresa, bool BlCualitativa)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_AjusteRevision_Set";
					Query.CommandType = CommandType.StoredProcedure;

					Query.Parameters.Add("@UidEmpresa", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidEmpresa"].Value = UidEmpresa;

					Query.Parameters.Add("@BlCualitativo", SqlDbType.Bit);
					Query.Parameters["@BlCualitativo"].Value = BlCualitativa;

					return _Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}
		}
	}
}
