﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using CodorniX.Util;

namespace CodorniX.Modelo
{
	/// <summary>
	/// Clase que representa la relación entre una tarea con un conjunto de departamentos.
	/// </summary>
	[Serializable]
	public class DepartamentoTarea : Departamento
	{
		private Guid _UidTarea;
		/// <summary>
		/// Identificador único de la tarea.
		/// </summary>
		public Guid UidTarea
		{
			get { return _UidTarea; }
			set { _UidTarea = value; }
		}

		private Guid _UidDepartamento;
		/// <summary>
		/// Identificador único del departamento.
		/// </summary>
		new public Guid UidDepartamento
		{
			get { return _UidDepartamento; }
			set { _UidDepartamento = value; }
		}

		//public bool GuardarDatos()
		//{

		//    bool Resultado = false;
		//    SqlCommand Comando = new SqlCommand();

		//    try
		//    {
		//        Comando.CommandType = CommandType.StoredProcedure;

		//        Comando.CommandText = "usp_DepartamentoTarea_Add";

		//        Comando.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
		//        Comando.Parameters["@UidDepartamento"].Value = UidDepartamento;

		//        Comando.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
		//        Comando.Parameters["@UidTarea"].Value = UidTarea;

		//        Resultado = Conexion.ManipilacionDeDatos(Comando);

		//        Comando.Dispose();


		//    }
		//    catch (Exception)
		//    {
		//        throw;
		//    }




		//    return Resultado;
		//}

		/// <summary>
		/// Clase repositorio de la relación.
		/// </summary>
		public class Repositorio : Departamento.Repository
		{
			private Connection _clsConexion = new Connection();

			/// <summary>
			/// Obtiene todos los departamentos de una tarea.
			/// </summary>
			/// <param name="uid">Identificador único de la tarea.</param>
			/// <returns>Una lista con los departamentos, puede ser nulo.</returns>
			public List<Departamento> FindAll(Guid uid)
			{
				DataTable table = new DataTable();
				List<Departamento> departamentos = new List<Departamento>();
				Departamento departamento = null;
				try
				{
					SqlCommand comando = new SqlCommand();
					comando.CommandText = "select Departamento.* from Departamento INNER JOIN DepartamentoTarea ON Departamento.UidDepartamento = DepartamentoTarea.UidDepartamento WHERE DepartamentoTarea.UidTarea = '" + uid + "'";
					comando.CommandType = CommandType.Text;

					//comando.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
					//comando.Parameters["@UidTarea"].Value = uid;

					table = _clsConexion.ExecuteQuery(comando);

					foreach (DataRow row in table.Rows)
					{
						departamento = new Departamento()
						{
							UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
							StrNombre = row["VchNombre"].ToString(),
							StrDescripcion = row["VchDescripcion"].ToString(),
							StrURL = row["VchURL"].ToString(),
							UidStatus = (Guid)row["UidStatus"],
							UidSucursal = (Guid)row["UidSucursal"],
							IsSaved = true,
							ToDelete = false
						};

						departamentos.Add(departamento);
					}
				}
				catch (Exception e)
				{
					throw new DatabaseException("Error populating", e);
				}

				return departamentos;
			}

			private bool InternalSave(DepartamentoTarea departamentotarea)
			{
				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_DepartamentoTarea_Add";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidDepartamento", departamentotarea.UidDepartamento, SqlDbType.UniqueIdentifier);

					command.AddParameter("@UidTarea", departamentotarea._UidTarea, SqlDbType.UniqueIdentifier);


					return _clsConexion.ExecuteCommand(command);
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Cannot save a Telefono from Sucursal", e);
				}
			}

			/// <summary>
			/// Guarda una relación Tarea-Departamento.
			/// </summary>
			/// <param name="departamentotarea">objeto relación</param>
			/// <returns>true si es insertado correctamente, en caso contrario false.</returns>
			public bool Save(DepartamentoTarea departamentotarea)
			{
				return InternalSave(departamentotarea);

			}

			/// <summary>
			/// Eliminar asignacion de departamento con la tarea
			/// </summary>
			/// <param name="UidTarea"></param>
			/// <param name="UidDepartamento"></param>
			/// <returns></returns>
			public bool DeleteAssignment(Guid UidTarea, Guid UidDepartamento)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_DepartamentoTarea_EliminarAsignacion";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@UidDepartamento", UidDepartamento, SqlDbType.UniqueIdentifier);

					return _clsConexion.ExecuteCommand(Query);
				}
				catch (Exception ex)
				{
					return false;
				}
			}

			/// <summary>
			/// Elimina un departamento de una tarea.
			/// </summary>
			/// <param name="departamentotarea">objeto relación</param>
			/// <returns>true si la operación fue realizada correctamente.</returns>
			public bool Remove(DepartamentoTarea departamentotarea)
			{
				try
				{
					SqlCommand comando = new SqlCommand();
					comando.CommandText = "usp_DepartamentoTarea_Eliminar";
					comando.CommandType = CommandType.StoredProcedure;

					comando.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
					comando.Parameters["@UidDepartamento"].Value = departamentotarea.UidDepartamento;

					return _clsConexion.ExecuteCommand(comando);
				}
				catch (Exception e)
				{
					throw new DatabaseException("Error removing a Telefono", e);
				}
			}

			/// <summary>
			/// No funcional
			/// </summary>
			/// <param name="UidTarea"></param>
			/// <returns></returns>
			[Obsolete]
			public DepartamentoTarea Buscar(Guid UidTarea)
			{
				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "select Departamento.* from Departamento INNER JOIN DepartamentoTarea ON Departamento.UidDepartamento = DepartamentoTarea.UidDepartamento WHERE DepartamentoTarea.UidTarea = '" + UidTarea + "'";
					command.CommandType = CommandType.Text;

					//command.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);

					DataTable table = new Connection().ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						DepartamentoTarea departamentotarea = new DepartamentoTarea()
						{
							UidTarea = (Guid)row["UidTarea"],
							UidDepartamento = (Guid)row["UidDepartamento"]
						};
						return departamentotarea;
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return null;
			}
		}
	}
}
