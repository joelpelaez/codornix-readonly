﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
	[Serializable]
	public class PeriodicidadSemanal
	{
		[NonSerialized]
		Conexion Conexion = new Conexion();
		private Guid _UidPeriodicidad;
		public Guid UidPeriodicidad
		{
			get { return _UidPeriodicidad; }
			set { _UidPeriodicidad = value; }
		}

		private bool _BlLunes;
		public bool BlLunes
		{
			get { return _BlLunes; }
			set { _BlLunes = value; }
		}

		private bool _BlMartes;
		public bool BlMartes
		{
			get { return _BlMartes; }
			set { _BlMartes = value; }
		}

		private bool _BlMiercoles;
		public bool BlMiercoles
		{
			get { return _BlMiercoles; }
			set { _BlMiercoles = value; }
		}

		private bool _BlJueves;
		public bool BlJueves
		{
			get { return _BlJueves; }
			set { _BlJueves = value; }
		}

		private bool _BlViernes;
		public bool BlViernes
		{
			get { return _BlViernes; }
			set { _BlViernes = value; }
		}

		private bool __BlSabado;
		public bool BlSabado
		{
			get { return __BlSabado; }
			set { __BlSabado = value; }
		}

		private bool _BlDomingo;
		public bool BlDomingo
		{
			get { return _BlDomingo; }
			set { _BlDomingo = value; }
		}

		public bool GuardarDatos()
		{

			bool Resultado = false;
			SqlCommand Comando = new SqlCommand();

			if (Conexion == null)
				Conexion = new Conexion();

			try
			{
				Comando.CommandType = CommandType.StoredProcedure;

				Comando.CommandText = "usp_PeriodicidadSemanal";

				Comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
				Comando.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

				Comando.Parameters.Add("@BitLunes", SqlDbType.Bit);
				Comando.Parameters["@BitLunes"].Value = BlLunes;

				Comando.Parameters.Add("@BitMartes", SqlDbType.Bit);
				Comando.Parameters["@BitMartes"].Value = BlMartes;

				Comando.Parameters.Add("@BitMiercoles", SqlDbType.Bit);
				Comando.Parameters["@BitMiercoles"].Value = BlMiercoles;

				Comando.Parameters.Add("@BitJueves", SqlDbType.Bit);
				Comando.Parameters["@BitJueves"].Value = BlJueves;

				Comando.Parameters.Add("@BitViernes", SqlDbType.Bit);
				Comando.Parameters["@BitViernes"].Value = BlViernes;

				Comando.Parameters.Add("@BitSabado", SqlDbType.Bit);
				Comando.Parameters["@BitSabado"].Value = BlSabado;

				Comando.Parameters.Add("@BitDomingo", SqlDbType.Bit);
				Comando.Parameters["@BitDomingo"].Value = BlDomingo;

				Resultado = Conexion.ManipilacionDeDatos(Comando);
				Comando.Dispose();




			}
			catch (Exception)
			{
				throw;
			}
			return Resultado;
		}

		public class Repositorio
		{
			Conexion Conexion = new Conexion();

			public PeriodicidadSemanal ConsultarPeriodicidadSemanal(Guid Periodicidad)
			{
				PeriodicidadSemanal periodicidadsemanal = null;

				DataTable table = null;

				SqlCommand comando = new SqlCommand();
				// SP - usp_BuscarPeriodicidadSemanal
				comando.CommandText = "select * from PeriodicidadSemanal where UidPeriodicidad='" + Periodicidad + "'";
				comando.CommandType = CommandType.Text;

				//comando.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
				//comando.Parameters["@UidPeriodicidad"].Value = Periodicidad;

				table = Conexion.Busquedas(comando);


				foreach (DataRow row in table.Rows)
				{
					periodicidadsemanal = new PeriodicidadSemanal()
					{
						UidPeriodicidad = (Guid)row["UidPeriodicidad"],
						BlLunes = (bool)row["BitLunes"],
						BlMartes = (bool)row["BitMartes"],
						BlMiercoles = (bool)row["BitMiercoles"],
						BlJueves = (bool)row["BitJueves"],
						BlViernes = (bool)row["BitViernes"],
						BlSabado = (bool)row["BitSabado"],
						BlDomingo = (bool)row["BitDomingo"],
					};

				}


				return periodicidadsemanal;


			}

			/// <summary>
			/// Actualizar datos de la periodicidad semanal
			/// </summary>
			/// <param name="UidPeriodicidad"></param>
			/// <param name="BlLunes"></param>
			/// <param name="BlMartes"></param>
			/// <param name="BlMiercoles"></param>
			/// <param name="BlJueves"></param>
			/// <param name="BlViernes"></param>
			/// <param name="BlSabado"></param>
			/// <param name="BlDomingo"></param>
			/// <returns></returns>
			public bool Actualizar(Guid UidPeriodicidad, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_PeriodicidadSemanal_Update";
					Query.CommandType = CommandType.StoredProcedure;

					Query.Parameters.Add("@UidPeriodicidad", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidPeriodicidad"].Value = UidPeriodicidad;

					Query.Parameters.Add("@BitLunes", SqlDbType.Bit);
					Query.Parameters["@BitLunes"].Value = BlLunes;

					Query.Parameters.Add("@BitMartes", SqlDbType.Bit);
					Query.Parameters["@BitMartes"].Value = BlMartes;

					Query.Parameters.Add("@BitMiercoles", SqlDbType.Bit);
					Query.Parameters["@BitMiercoles"].Value = BlMiercoles;

					Query.Parameters.Add("@BitJueves", SqlDbType.Bit);
					Query.Parameters["@BitJueves"].Value = BlJueves;

					Query.Parameters.Add("@BitViernes", SqlDbType.Bit);
					Query.Parameters["@BitViernes"].Value = BlViernes;

					Query.Parameters.Add("@BitSabado", SqlDbType.Bit);
					Query.Parameters["@BitSabado"].Value = BlSabado;

					Query.Parameters.Add("@BitDomingo", SqlDbType.Bit);
					Query.Parameters["@BitDomingo"].Value = BlDomingo;

					return this.Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}

			public bool Delete(Guid UidPeriodicidad)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "DELETE FROM PeriodicidadSemanal WHERE UidPeriodicidad ='" + UidPeriodicidad + "'";
					Query.CommandType = CommandType.Text;

					return this.Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}

			public DateTime CalculateNext(DateTime DtToday, bool BlFirstDate, int IntFrequency, bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
			{
				// Next Execution Date
				DateTime DtNext = DtToday;
				// Day Of Week From DtToday
				DayOfWeek DOFDay = DtToday.DayOfWeek;

				if (!BlFirstDate && IntFrequency > 1)
				{
					/// Last Day Of Week To Execute
					DayOfWeek DOFLastDay;
					DOFLastDay = GetLastDayFromPeriodicity(BlLunes, BlMartes, BlMiercoles, BlJueves, BlViernes, BlSabado, BlDomingo);
					// Number Of Day Of week

					if (DOFLastDay == DOFDay)
					{
						int NumberDayOfWeek = GetNumberDayOfWeekByDayOfWeek(DOFLastDay);
						DtNext = DtNext.AddDays((IntFrequency * 7) - (NumberDayOfWeek - 1));
					}
				}

				bool IsLocated = false;
				DayOfWeek DOFLastDayEx;
				DOFLastDayEx = GetLastDayFromPeriodicity(BlLunes, BlMartes, BlMiercoles, BlJueves, BlViernes, BlSabado, BlDomingo);
				if (DOFLastDayEx == DOFDay && !BlFirstDate)
				{
					DtNext = DtNext.AddDays(1);
				}
				else
				{
					if (!BlFirstDate)
					{
						DtNext = DtNext.AddDays(1);
					}
				}

				while (!IsLocated)
				{
					DOFDay = DtNext.DayOfWeek;

					if (DOFDay == DayOfWeek.Sunday && BlDomingo)
					{
						IsLocated = true;
						break;
					}
					else if (DOFDay == DayOfWeek.Monday && BlLunes)
					{
						IsLocated = true;
						break;
					}
					else if (DOFDay == DayOfWeek.Tuesday && BlMartes)
					{
						IsLocated = true;
						break;
					}
					else if (DOFDay == DayOfWeek.Wednesday && BlMiercoles)
					{
						IsLocated = true;
						break;
					}
					else if (DOFDay == DayOfWeek.Thursday && BlJueves)
					{
						IsLocated = true;
						break;
					}
					else if (DOFDay == DayOfWeek.Friday && BlViernes)
					{
						IsLocated = true;
						break;
					}
					else if (DOFDay == DayOfWeek.Saturday && BlSabado)
					{
						IsLocated = true;
						break;
					}

					DtNext = DtNext.AddDays(1);
				}

				return DtNext;
			}

			private int GetNumberDayOfWeekByDayOfWeek(DayOfWeek dayOfWeek)
			{
				// Monday = 1, Tuesday = 2, Wednesday = 3, Thursday = 4, Friday = 5, Saturday = 6, Sunday = 0
				int NumberDay = 1;
				switch (dayOfWeek)
				{
					case DayOfWeek.Sunday:
						NumberDay = 7;
						break;
					case DayOfWeek.Monday:
						NumberDay = 1;
						break;
					case DayOfWeek.Tuesday:
						NumberDay = 2;
						break;
					case DayOfWeek.Wednesday:
						NumberDay = 3;
						break;
					case DayOfWeek.Thursday:
						NumberDay = 4;
						break;
					case DayOfWeek.Friday:
						NumberDay = 5;
						break;
					case DayOfWeek.Saturday:
						NumberDay = 6;
						break;
					default:
						break;
				}

				return NumberDay;
			}

			private DayOfWeek GetLastDayFromPeriodicity(bool BlLunes, bool BlMartes, bool BlMiercoles, bool BlJueves, bool BlViernes, bool BlSabado, bool BlDomingo)
			{
				if (BlDomingo)
					return DayOfWeek.Sunday;
				else if (BlSabado)
					return DayOfWeek.Saturday;
				else if (BlViernes)
					return DayOfWeek.Friday;
				else if (BlJueves)
					return DayOfWeek.Thursday;
				else if (BlMiercoles)
					return DayOfWeek.Wednesday;
				else if (BlMartes)
					return DayOfWeek.Tuesday;
				else if (BlLunes)
					return DayOfWeek.Monday;
				else
					return DayOfWeek.Sunday;
			}
		}
	}
}
