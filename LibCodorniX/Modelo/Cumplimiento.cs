﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	/// <summary>
	/// Clase que representa un cumplimiento de una cierta tarea o una programación de la misma. Depende fuertemente
	/// de la tarea relacionada.
	/// </summary>
	[Serializable]
	public class Cumplimiento
	{
		private Guid _UidCumplimiento;
		/// <summary>
		/// Identificador único del cumplimiento.
		/// </summary>
		public Guid UidCumplimiento
		{
			get { return _UidCumplimiento; }
			set { _UidCumplimiento = value; }
		}
		public string strIdCumplimiento { get { return _UidCumplimiento.ToString(); } }

		private Guid _UidTarea;
		/// <summary>
		/// Identificador único de la tarea relacionada. Cambia únicamente en caso de cambios en la tarea.
		/// </summary>
		public Guid UidTarea
		{
			get { return _UidTarea; }
			set { _UidTarea = value; }
		}
		public string strIdTarea { get { return _UidTarea.ToString(); } }

		private Guid? _UidDepartamento;
		/// <summary>
		/// Identificador único del departamento al que se realizó dicho cumplimiento.
		/// </summary>
		/// <value>
		/// Puede estar relacionado
		/// directamente a un departamento (a través de este campo) o indirectamente por un área. Es mutualmente
		/// exclusivo del campo <see cref="UidArea"/>.
		/// </value>
		public Guid? UidDepartamento
		{
			get { return _UidDepartamento; }
			set { _UidDepartamento = value; }
		}
		public string strIdDepartamento { get { return _UidDepartamento == null ? Guid.Empty.ToString() : _UidDepartamento.ToString(); } }

		private Guid? _UidArea;
		/// <summary>
		/// Identificador único del área donde se realizó el cumplimiento, puede contener o no un valor.
		/// </summary>
		/// <value>
		/// Es mutualmente exclusivo del campo <see cref="UidDepartamento"/>. En caso de que ambos tengan un valor,
		/// se supone que el valor que contiene <see cref="UidDepartamento"/> es el departamento que pertenece el área
		/// especificada en este campo.
		/// </value>
		public Guid? UidArea
		{
			get { return _UidArea; }
			set { _UidArea = value; }
		}
		public string strIdArea { get { return _UidArea == null ? Guid.Empty.ToString() : _UidArea.ToString(); } }

		private Guid? _UidUsuario;
		/// <summary>
		/// Identificador único del usuario (encargado) que realizó la tarea.
		/// </summary>
		public Guid? UidUsuario
		{
			get { return _UidUsuario; }
			set { _UidUsuario = value; }
		}
		public string strIdUsuario { get { return _UidUsuario == null ? Guid.Empty.ToString() : _UidUsuario.ToString(); } }

		private DateTime _DtFechaProgramada;
		/// <summary>
		/// Fecha de programación del cumplimiento.
		/// </summary>
		/// <value>
		/// Se maneja como sustituto a la computación de las fechas
		/// de realización de las tareas de forma manual, ya que se genera la siguiente al momento de realizarse la
		/// actual. Especificación en el procedimiento almacenado: usp_Cumplimiento_Do
		/// </value>
		public DateTime DtFechaProgramada
		{
			get { return _DtFechaProgramada; }
			set { _DtFechaProgramada = value; }
		}
		public string strDtFechaProgramada { get { return DtFechaProgramada.ToString("dd-MM-yyyy"); } }

		private DateTimeOffset? _DtFechaHora;
		/// <summary>
		/// Fecha y hora de cumplimiento de la tarea. En teoría la fecha de cumplimiento y programación deben
		/// coincidir, pero debe tomarse en cuenta una fecha separada.
		/// </summary>
		// TODO: Revisar procedimeinto de cálculo con la fecha programada.
		public DateTimeOffset? DtFechaHora
		{
			get { return _DtFechaHora; }
			set { _DtFechaHora = value; }
		}
		public string strDtFechaHora { get { return _DtFechaHora == null ? "empty" : _DtFechaHora.ToString(); } }

		private string _UrlFoto;
		/// <summary>
		/// Dirección URL de la imagen de prueba del cumplimiento. No aplica en todas las tareas.
		/// </summary>
		public string UrlFoto
		{
			get { return _UrlFoto; }
			set { _UrlFoto = value; }
		}

		private string _StrObservacion;
		/// <summary>
		/// Observaciones obtenidas durante el cumplimiento de la tarea, puede ser vacío.
		/// </summary>
		public string StrObservacion
		{
			get { return _StrObservacion; }
			set { _StrObservacion = value; }
		}

		private Guid _UidEstadoCumplimiento;
		/// <summary>
		/// Identificador único del estado de cumplimiento.
		/// </summary>
		public Guid UidEstadoCumplimiento
		{
			get { return _UidEstadoCumplimiento; }
			set { _UidEstadoCumplimiento = value; }
		}

		private string _StrEstadoCumplimiento;
		/// <summary>
		/// Contenido textual del estado de cumplimiento. No debe modificarse con el fin de cambiar el estado.
		/// </summary>
		public string StrEstadoCumplimiento
		{
			get { return _StrEstadoCumplimiento; }
			set { _StrEstadoCumplimiento = value; }
		}

		private bool? _BlValor;
		/// <summary>
		/// Valor de resultado de la tarea: tipo booleano o verdadero/falso.
		/// </summary>
		public bool? BlValor
		{
			get { return _BlValor; }
			set { _BlValor = value; }
		}
		public string strBlValor { get { return _BlValor == null ? "empty" : _BlValor.ToString(); } }

		private decimal? _DcValor1;
		/// <summary>
		/// Valor de resultado de la tarea. tipo decimal, primer valor o único.
		/// </summary>
		public decimal? DcValor1
		{
			get { return _DcValor1; }
			set { _DcValor1 = value; }
		}
		public string strDcValor1 { get { return _DcValor1 == null ? "empty" : _DcValor1.ToString(); } }

		private decimal? _DcValor2;
		/// <summary>
		/// Valor de resultado de la tarea. Tipo decimal, segundo valor.
		/// </summary>
		public decimal? DcValor2
		{
			get { return _DcValor2; }
			set { _DcValor2 = value; }
		}
		public string strDcValor2 { get { return _DcValor2 == null ? "empty" : _DcValor2.ToString(); } }

		private Guid? _UidOpciones;
		/// <summary>
		/// Valor de resultado de la tarea. Tipo opcional, hace referencia a una tabla de opciones.
		/// </summary>
		public Guid? UidOpcion
		{
			get { return _UidOpciones; }
			set { _UidOpciones = value; }
		}
		public string strIdOpcion { get { return _UidOpciones == null ? Guid.Empty.ToString() : _UidOpciones.ToString(); } }

		private string _StrOpcion;
		/// <summary>
		/// Contenido textual de la opción seleccionada en <see cref="UidOpcion"/>
		/// </summary>
		public string StrOpcion
		{
			get { return _StrOpcion; }
			set { _StrOpcion = value; }
		}

		// EXTRA FIELDS

		private string _StrNombreUsuario;
		/// <summary>
		/// Campo extra: Nombre del usuario referenciado por <see cref="UidUsuario"/>
		/// </summary>
		public string StrNombreUsuario
		{
			get { return _StrNombreUsuario; }
			set { _StrNombreUsuario = value; }
		}

		private string _StrApellidoPaterno;
		/// <summary>
		/// Campo extra: Apellido paterno del usuario referenciado por <see cref="UidUsuario"/>
		/// </summary>
		public string StrApellidoPaterno
		{
			get { return _StrApellidoPaterno; }
			set { _StrApellidoPaterno = value; }
		}

		private string _StrUsuario;
		public string StrUsuario
		{
			get { return _StrUsuario; }
			set { _StrUsuario = value; }
		}

		private string _StrTarea;
		/// <summary>
		/// Campo extra: Nombre de la tarea referenciada por <see cref="UidTarea"/>
		/// </summary>
		public string StrTarea
		{
			get { return _StrTarea; }
			set { _StrTarea = value; }
		}

		private string _StrDepartamento;
		/// <summary>
		/// Campo extra: Nombre del departamento referenciado por <see cref="UidTarea"/>
		/// </summary>
		public string StrDepartamento
		{
			get { return _StrDepartamento; }
			set { _StrDepartamento = value; }
		}

		private string _StrArea;
		/// <summary>
		/// Campo extra: Nombre de la tarea
		/// </summary>
		public string StrArea
		{
			get { return _StrArea; }
			set { _StrArea = value; }
		}

		private TimeSpan? _TmHora;
		/// <summary>
		/// Campo extra: Hora programada para realizarse la tarea.
		/// </summary>
		public TimeSpan? TmHora
		{
			get { return _TmHora; }
			set { _TmHora = value; }
		}
		public string strTmHora { get { return _TmHora.ToString(); } }

		private string _StrValor;
		/// <summary>
		/// Campo de reporte: Representación textual del valor.
		/// </summary>
		public string StrValor
		{
			get { return _StrValor; }
			set { _StrValor = value; }
		}

		private string _StrTipoMedicion;
		public string StrTipoMedicion
		{
			get { return _StrTipoMedicion; }
			set { _StrTipoMedicion = value; }
		}

		private string _StrTipoUnidad;
		public string StrTipoUnidad
		{
			get { return _StrTipoUnidad; }
			set { _StrTipoUnidad = value; }
		}

		private string _StrTipoTarea;
		public string StrTipoTarea
		{
			get { return _StrTipoTarea; }
			set { _StrTipoTarea = value; }
		}

		private Guid _UidPeriodo;
		public Guid UidPeriodo
		{
			get { return _UidPeriodo; }
			set { _UidPeriodo = value; }
		}
		public string strIdPeriodo { get { return _UidPeriodo.ToString(); } }

		private int _IntFolio;
		public int IntFolio
		{
			get { return _IntFolio; }
			set { _IntFolio = value; }
		}

		private int _IntFolioTarea;
		public int IntFolioTarea
		{
			get { return _IntFolioTarea; }
			set { _IntFolioTarea = value; }
		}

		private int _IntFolioTurno;
		public int IntFolioTurno
		{
			get { return _IntFolioTurno; }
			set { _IntFolioTurno = value; }
		}

		private int _IntOrdenTarea;
		public int OrdenTarea
		{
			get { return _IntOrdenTarea; }
			set { _IntOrdenTarea = value; }
		}

		private Guid _UidCreador;
		/// <summary>
		/// Identificador del usuario que realizo el cumplimiento de la tarea
		/// </summary>
		public Guid UidCreador
		{
			get { return _UidCreador; }
			set { _UidCreador = value; }
		}

		/// <summary>
		/// Rol del usuario que cumplio la tarea Administrador/Supervisor/Encargado
		/// </summary>
		public string VchRolCreador
		{
			get;
			set;
		}

		/// <summary>
		/// Variable para determinar si el cumplimiento fue realizado fuera del tiempo establecido
		/// </summary>
		private bool? _BlAtrasada;
		public bool? BlAtrasada
		{
			get { return _BlAtrasada; }
			set { _BlAtrasada = value; }
		}

		/// <summary>
		/// Fecha en la que se realizo el cumplimiento Atrasado
		/// </summary>
		private DateTimeOffset? _DtFechaAtrasada;
		public DateTimeOffset? DtFechaAtrasada
		{
			get { return _DtFechaAtrasada; }
			set { _DtFechaAtrasada = value; }
		}
		public string strFechaAtrasada { get { return this._DtFechaAtrasada == null ? "empty" : this._DtFechaAtrasada.Value.ToString("dd/MM/yyyy"); } }

		public string ValorCumplimiento
		{
			get;
			set;
		}


		/* Android App */
		private string strDateProximo;
		public string strProximo
		{
			get { return strDateProximo; }
			set { strDateProximo = value; }
		}
		private double intIsDateToday;
		public double isDateToday
		{
			get { return intIsDateToday; }
			set { intIsDateToday = value; }
		}

		/// <summary>
		/// Clase repositorio que abstrae la persistencia de datos hacia la base de datos.
		/// </summary>
		public class Repository
		{
			Connection conn = new Connection();

			/// <summary>
			/// Crear el primero cumplimiento de la tarea
			/// </summary>
			/// <param name="UidTarea"></param>
			/// <param name="UidDepartamento"></param>
			/// <param name="UidArea"></param>
			/// <param name="DtFechaProgramada"></param>
			/// <returns></returns>
			public bool PrimerCumplimiento(Guid UidTarea, Guid UidDepartamento, Guid UidArea, DateTime DtFechaProgramada)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Cumplimiento_PrimerCumplimiento";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);

					if (UidDepartamento != Guid.Empty)
						Query.AddParameter("@UidDepartamento", UidDepartamento, SqlDbType.UniqueIdentifier);

					if (UidArea != Guid.Empty)
						Query.AddParameter("@UidArea", UidArea, SqlDbType.UniqueIdentifier);

					Query.AddParameter("@DtFechaProgramada", DtFechaProgramada, SqlDbType.Date);

					return conn.ExecuteCommand(Query);
				}
				catch (Exception)
				{

					throw;
				}
			}

			/// <summary>
			/// Obtener el ultimo cumplimiento de la tarea
			/// </summary>
			/// <param name="UidTarea"></param>
			/// <returns></returns>
			public Cumplimiento GetUltimoCumplimientoTarea(Guid UidTarea)
			{
				try
				{
					Cumplimiento cCumplimiento = null;

					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Cumplimiento_UltimoTarea";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);
					DataTable Result = conn.ExecuteQuery(Query);

					foreach (DataRow row in Result.Rows)
					{
						cCumplimiento = new Cumplimiento();
						cCumplimiento._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
						cCumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cCumplimiento._UidDepartamento = row.IsNull("UidDepartamento") ? (Guid?)null : new Guid(row["UidDepartamento"].ToString());
						cCumplimiento._UidArea = row.IsNull("UidArea") ? (Guid?)null : new Guid(row["UidArea"].ToString());
						cCumplimiento._UidUsuario = row.IsNull("UidUsuario") ? (Guid?)null : new Guid(row["UidUsuario"].ToString());
						cCumplimiento._IntFolio = row.IsNull("IntFolio") ? 0 : Convert.ToInt32(row["IntFolio"].ToString());
						cCumplimiento._DtFechaProgramada = Convert.ToDateTime(row["DtFechaProgramada"].ToString());
						cCumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cCumplimiento._UrlFoto = row.IsNull("UrlFoto") ? string.Empty : row["UrlFoto"].ToString();
						cCumplimiento._StrObservacion = row.IsNull("VchObservacion") ? string.Empty : row["VchObservacion"].ToString();
						cCumplimiento._UidEstadoCumplimiento = new Guid(row["UidEstadoCumplimiento"].ToString());
						cCumplimiento._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						cCumplimiento._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						cCumplimiento._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						cCumplimiento._UidOpciones = row.IsNull("UidOpcion") ? (Guid?)null : new Guid(row["UidOpcion"].ToString());

						cCumplimiento.BlAtrasada = row.IsNull("BitAtrasado") ? false : Convert.ToBoolean(row["BitAtrasado"].ToString());
						cCumplimiento.DtFechaAtrasada = row.IsNull("DtFechaHoraAtrasada") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHoraAtrasada"].ToString());
					}

					return cCumplimiento;
				}
				catch (Exception)
				{

					throw;
				}
			}

			/// <summary>
			/// Obtener el total de tareas generadas por el turno que cuenta con cumplimiento posterior
			/// </summary>
			/// <param name="FolioTurno"></param>
			/// <returns></returns>
			public int GetTotalCumplimientosGeneradosTurnoAtrasado(int IntFolioTurno)
			{
				int intTotal = 0;
				try
				{
					string Query = "SELECT COUNT(C.UidCumplimiento) AS TotalPendientes FROM Cumplimiento C 	INNER JOIN EstadoCumplimiento EC ON EC.UidEstadoCumplimiento = C.UidEstadoCumplimiento WHERE C.IntFolioTurno = " + IntFolioTurno + " AND EC.VchTipoCumplimiento = 'No Realizado'";

					intTotal = this.conn.EncontrarCoincidenciasSQL(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
				return intTotal;
			}

			/// <summary>
			/// Guarda un nuevo cumplimiento en la base de datos.
			/// </summary>
			/// <param name="cumplimiento">Nuevo objeto <see cref="Cumplimiento"/></param>
			/// <returns>true si se insertó currectamente, false en caso contrario.</returns>
			public bool Save(Cumplimiento cumplimiento)
			{
				try
				{
					SqlCommand command = new SqlCommand();

					command.CommandText = "usp_Cumplimiento_Add";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidTarea", cumplimiento._UidTarea, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidUsuario", cumplimiento._UidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFechaHora", cumplimiento._DtFechaHora, SqlDbType.DateTimeOffset);
					command.AddParameter("@UidEstadoCumplimiento", cumplimiento._UidEstadoCumplimiento, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UrlFoto", cumplimiento._UrlFoto, SqlDbType.NVarChar, 50);

					if (cumplimiento.BlAtrasada != null)
						command.AddParameter("@Atrasada", cumplimiento.BlAtrasada, SqlDbType.Bit);

					if (cumplimiento._DtFechaAtrasada != null)
						command.AddParameter("@FechaAtrasada", cumplimiento.BlAtrasada, SqlDbType.DateTimeOffset);

					if (cumplimiento._UidDepartamento.HasValue)
						command.AddParameter("@UidDepartamento", cumplimiento._UidDepartamento.Value, SqlDbType.UniqueIdentifier);
					if (cumplimiento._UidArea.HasValue)
						command.AddParameter("@UidArea", cumplimiento._UidArea.Value, SqlDbType.UniqueIdentifier);

					if (cumplimiento._BlValor.HasValue)
						command.AddParameter("@BitValor", cumplimiento._BlValor.Value, SqlDbType.Bit);

					if (cumplimiento._DcValor1.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor1", cumplimiento._DcValor1.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (cumplimiento._DcValor2.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor2", cumplimiento._DcValor2.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (cumplimiento._UidOpciones.HasValue)
						command.AddParameter("@UidOpcion", cumplimiento._UidOpciones.Value, SqlDbType.UniqueIdentifier);

					conn.ExecuteCommand(command);

				}
				catch (SqlException ex)
				{
					throw new DatabaseException("Error saving a Cumplimiento object", ex);
				}

				return true;
			}

			/// <summary>
			/// Actualiza el campo de <see cref="UidTarea"/> si hubo cambios notables en la tarea relacionada.
			/// Sólo aplica en cumplimientos no realizados o programados.
			/// </summary>
			/// <param name="UidTarea">Identificador único de la nueva tarea.</param>
			/// <param name="UidTareaAnterior">Identificaodr único de la tarea anterior.</param>
			public void actualizarcumplimiento(Guid UidTarea, Guid UidTareaAnterior)
			{
				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_ActualizarCumplimiento";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidTarea", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidTarea"].Value = UidTarea;

				command.Parameters.Add("@UidTareaAnterior", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidTareaAnterior"].Value = UidTareaAnterior;

				conn.ExecuteCommand(command);
			}

			/// <summary>
			/// Obtiene un cumplimiento a partir de su identificador único.
			/// </summary>
			/// <param name="uid">Identificador único</param>
			/// <returns>Objeto <see cref="Cumplimiento"/></returns>
			public Cumplimiento Find(Guid uid)
			{
				Cumplimiento cumplimiento = null;

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "SELECT TOP(1) c.*, ec.VchTipoCumplimiento FROM Cumplimiento c JOIN EstadoCumplimiento ec ON c.UidEstadoCumplimiento = ec.UidEstadoCumplimiento WHERE c.UidCumplimiento = '" + uid + "'";
					command.CommandType = CommandType.Text;

					//command.AddParameter("@UidCumplimiento", uid, SqlDbType.UniqueIdentifier);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = row.IsNull("UidDepartamento") ? (Guid?)null : new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row.IsNull("UidArea") ? (Guid?)null : new Guid(row["UidArea"].ToString());
						cumplimiento._UidUsuario = row.IsNull("UidUsuario") ? (Guid?)null : new Guid(row["UidUsuario"].ToString());
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimiento._DtFechaProgramada = Convert.ToDateTime(row["DtFechaProgramada"].ToString());
						cumplimiento._StrObservacion = row["VchObservacion"].ToString();
						cumplimiento._UidEstadoCumplimiento = new Guid(row["UidEstadoCumplimiento"].ToString());
						cumplimiento._UrlFoto = row["UrlFoto"].ToString();
						cumplimiento._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						cumplimiento._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						cumplimiento._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						cumplimiento._UidOpciones = row.IsNull("UidOpcion") ? (Guid?)null : new Guid(row["UidOpcion"].ToString());
						cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						cumplimiento._IntFolio = Convert.ToInt32(row["IntFolio"].ToString());

						cumplimiento.BlAtrasada = row.IsNull("BitAtrasado") ? false : Convert.ToBoolean(row["BitAtrasado"].ToString());
						cumplimiento.DtFechaAtrasada = row.IsNull("DtFechaHoraAtrasada") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHoraAtrasada"].ToString());

						cumplimiento.VchRolCreador = row.IsNull("VchRolCreador") ? string.Empty : row["VchRolCreador"].ToString();
						cumplimiento._UidCreador = row.IsNull("UidCreador") ? Guid.Empty : new Guid(row["UidCreador"].ToString());
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching a Cumplimiento", e);
				}

				return cumplimiento;
			}

			public Cumplimiento ObtenerTareaCumplimiento(Guid uid, Guid UidUsuario)
			{
				Cumplimiento cumplimiento = null;

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "Usp_obtenerCumplimiento";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidTarea", uid, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidUsuario", UidUsuario, SqlDbType.UniqueIdentifier);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = row.IsNull("UidDepartamento") ? (Guid?)null : new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row.IsNull("UidArea") ? (Guid?)null : new Guid(row["UidArea"].ToString());
						cumplimiento._UidUsuario = row.IsNull("UidUsuario") ? (Guid?)null : new Guid(row["UidUsuario"].ToString());
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? (DateTimeOffset?)null : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimiento._DtFechaProgramada = Convert.ToDateTime(row["DtFechaProgramada"].ToString());
						cumplimiento._StrObservacion = row["VchObservacion"].ToString();
						cumplimiento._UidEstadoCumplimiento = new Guid(row["UidEstadoCumplimiento"].ToString());
						cumplimiento._UrlFoto = row["UrlFoto"].ToString();
						cumplimiento._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						cumplimiento._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						cumplimiento._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						cumplimiento._UidOpciones = row.IsNull("UidOpcion") ? (Guid?)null : new Guid(row["UidOpcion"].ToString());
						cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching a Cumplimiento", e);
				}

				return cumplimiento;
			}

			/// <summary>
			/// Obtiene un conjunto de cumplimientos a partir de un criterio.
			/// </summary>
			/// <param name="criteria">objeto <see cref="Criteria"/></param>
			/// <returns>Una lista de cumplimientos que coincidan con la búsqueda, puede estar vacía.</returns>
			[Obsolete("No se hace uso de este método de forma activa")]
			public List<Cumplimiento> FindBy(Criteria criteria)
			{
				List<Cumplimiento> cumplimientos = new List<Cumplimiento>();

				SqlCommand command = new SqlCommand();

				try
				{

					command.CommandText = "usp_Cumplimiento_FindBy";
					command.CommandType = CommandType.StoredProcedure;

					InjectCriteria(criteria, command);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Cumplimiento cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._StrTarea = row["VchTarea"].ToString();
						cumplimiento._StrDepartamento = row["VchDepartamento"].ToString();
						cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						cumplimiento._StrNombreUsuario = row["VchNombreUsuario"].ToString();
						cumplimiento._StrApellidoPaterno = row["VchApellidoUsuario"].ToString();

						cumplimientos.Add(cumplimiento);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			/// <summary>
			/// Obtiene la lista de cumplimientos de una fecha exacta basado en el encargado y sucursal indicada.
			/// Puede estar vacía.
			/// </summary>
			/// <param name="uidUsuario">Identificador único del usuario.</param>
			/// <param name="uidPeriodo">Identificador único del periodo.</param>
			/// <param name="fecha">Fecha de los cumplimientos programados.</param>
			/// <returns>Lista de cumplimientos programados, puede estar vacía.</returns>
			public List<Cumplimiento> FindByUser(Guid uidUsuario, Guid uidPeriodo, DateTime fecha,
				string periodos, int Folio, string nombre, string estados, Guid departamento, Guid area, Guid tipo)
			{
				List<Cumplimiento> cumplimientos = new List<Cumplimiento>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Cumplimiento_ListByUser";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidPeriodo", uidPeriodo, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);
					command.AddParameter("@VchPeriodos", periodos, SqlDbType.NVarChar, 2000);
					if (!string.IsNullOrWhiteSpace(nombre))
						command.AddParameter("@VchNombre", nombre, SqlDbType.NVarChar, 50);
					if (!string.IsNullOrEmpty(estados))
						command.AddParameter("@VchEstados", estados, SqlDbType.NVarChar, 2000);
					command.AddParameter("@UidDepartamento", departamento, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidArea", area, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidTipoTarea", tipo, SqlDbType.UniqueIdentifier);

					if (Folio > 0)
						command.AddParameter("@FolioTarea", Folio, SqlDbType.Int);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Cumplimiento cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row["UidArea"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidArea"].ToString());
						cumplimiento._StrTarea = row["VchTarea"].ToString();
						cumplimiento._StrDepartamento = row["VchDepartamento"].ToString();
						cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						cumplimiento._StrTipoTarea = row["VchTipoTarea"].ToString();
						cumplimiento._StrArea = row["VchArea"].ToString();
						cumplimiento._TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString());
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? default(DateTimeOffset) : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimiento._UidPeriodo = new Guid(row["UidPeriodo"].ToString());
						cumplimiento._IntFolio = Convert.ToInt32(row["IntFolioCumpl"]);
						cumplimiento._IntFolioTarea = Convert.ToInt32(row["IntFolio"]);
						cumplimiento._IntOrdenTarea = row.IsNull("IntOrdenTarea") ? 0 : Convert.ToInt32(row["IntOrdenTarea"]);
						cumplimientos.Add(cumplimiento);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			public List<Cumplimiento> Search(Guid? uidUsuario, Guid uidPeriodo, DateTime fechaInicio, DateTime fechaFin,
				string periodos, string nombre, string estados, Guid departamento, Guid area, Guid tipo)
			{
				List<Cumplimiento> cumplimientos = new List<Cumplimiento>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Cumplimiento_Search";
					command.CommandType = CommandType.StoredProcedure;

					if (uidUsuario.HasValue)
						command.AddParameter("@UidUsuario", uidUsuario.Value, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidPeriodo", uidPeriodo, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFechaInicio", fechaInicio, SqlDbType.Date);
					command.AddParameter("@DtFechaFin", fechaFin, SqlDbType.Date);
					command.AddParameter("@VchPeriodos", periodos, SqlDbType.NVarChar, 2000);
					if (!string.IsNullOrWhiteSpace(nombre))
						command.AddParameter("@VchNombre", nombre, SqlDbType.NVarChar, 50);
					if (!string.IsNullOrEmpty(estados))
						command.AddParameter("@VchEstados", estados, SqlDbType.NVarChar, 2000);
					command.AddParameter("@UidDepartamento", departamento, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidArea", area, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidTipoTarea", tipo, SqlDbType.UniqueIdentifier);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Cumplimiento cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row["UidArea"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidArea"].ToString());
						cumplimiento._StrTarea = row["VchTarea"].ToString();
						cumplimiento._StrDepartamento = row["VchDepartamento"].ToString();
						cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						cumplimiento._StrTipoTarea = row["VchTipoTarea"].ToString();
						cumplimiento._StrArea = row["VchArea"].ToString();
						cumplimiento._TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString());
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? default(DateTimeOffset) : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimiento._UidPeriodo = new Guid(row["UidPeriodo"].ToString());
						cumplimiento._IntFolio = Convert.ToInt32(row["IntFolioCumpl"]);
						cumplimiento._IntFolioTarea = Convert.ToInt32(row["IntFolio"]);
						cumplimientos.Add(cumplimiento);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			public List<Cumplimiento> BusquedaHistorico(Guid UidSucursal, Guid UidUsuario, Guid UidDepartamento, Guid UidArea, Guid UidTipoTarea, int? FolioTarea, int? FolioCumplimiento, int? FolioTurno, string NombreTarea, string UidsEstadoCumplimiento, DateTime? DtFechaInicio, DateTime? DtFechaFin,
				bool ExpFolioTurno, string DirFolioTurno, bool ExpFolioCumplimiento, string DirFolioCumplimiento, bool ExpFolioTarea, string DirFolioTarea,
				bool ExpTarea, string DirTarea, bool ExpDepartamento, string DirDepartamento, bool ExpArea, string DirArea, bool ExpHora, string DirHora,
				bool ExpEstado, string DirEstado,
				int PosFolioTurno, int PosFolioCumplimiento, int PosFolioTarea, int PosTarea, int PosDepartamento, int PosArea, int PosHora, int PosEstado)
			{
				List<Cumplimiento> LsCumplimientos = new List<Cumplimiento>();
				SqlCommand Command = new SqlCommand();
				try
				{
					Command.CommandText = "usp_HistoricoCumplimiento_Search";
					Command.CommandType = CommandType.StoredProcedure;

					Command.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);

					if (UidUsuario != Guid.Empty)
						Command.AddParameter("@UidUsuario", UidUsuario, SqlDbType.UniqueIdentifier);

					if (UidDepartamento != Guid.Empty)
						Command.AddParameter("@UidDepartamento", UidDepartamento, SqlDbType.UniqueIdentifier);

					if (UidArea != Guid.Empty)
						Command.AddParameter("@UidArea", UidArea, SqlDbType.UniqueIdentifier);

					if (UidTipoTarea != Guid.Empty)
						Command.AddParameter("@UidTipoTarea", UidTipoTarea, SqlDbType.UniqueIdentifier);

					if (!string.IsNullOrEmpty(NombreTarea))
						Command.AddParameter("@Tarea", NombreTarea, SqlDbType.NVarChar, 50);

					if (!string.IsNullOrEmpty(UidsEstadoCumplimiento))
						Command.AddParameter("@VchUidEstado", UidsEstadoCumplimiento, SqlDbType.VarChar, 2000);

					if (FolioTarea != null)
						Command.AddParameter("@FolioTarea", FolioTarea.Value, SqlDbType.Int);

					if (FolioCumplimiento != null)
						Command.AddParameter("@FolioCumplimiento", FolioCumplimiento.Value, SqlDbType.Int);

					if (FolioTurno != null)
						Command.AddParameter("@FolioTurno", FolioTurno.Value, SqlDbType.Int);

					if (DtFechaInicio != null)
						Command.AddParameter("@DtFechaInicio", DtFechaInicio.Value, SqlDbType.DateTime);

					if (DtFechaFin != null)
						Command.AddParameter("@DtFechaFin", DtFechaFin.Value, SqlDbType.DateTime);

					Command.AddParameter("@ExpFolioTurno", ExpFolioTurno, SqlDbType.Bit);
					Command.AddParameter("@DirFolioTurno", DirFolioTurno, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosFolioTurno", PosFolioTurno, SqlDbType.Int);

					Command.AddParameter("@ExpFolioCumplimiento", ExpFolioCumplimiento, SqlDbType.Bit);
					Command.AddParameter("@DirFolioCumplimiento", DirFolioCumplimiento, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosFolioCumplimiento", PosFolioCumplimiento, SqlDbType.Int);

					Command.AddParameter("@ExpFolioTarea", ExpFolioTarea, SqlDbType.Bit);
					Command.AddParameter("@DirFolioTarea", DirFolioTarea, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosFolioTarea", PosFolioTarea, SqlDbType.Int);

					Command.AddParameter("@ExpTarea", ExpTarea, SqlDbType.Bit);
					Command.AddParameter("@DirTarea", DirTarea, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosTarea", PosTarea, SqlDbType.Int);

					Command.AddParameter("@ExpDepartamento", ExpDepartamento, SqlDbType.Bit);
					Command.AddParameter("@DirDepartamento", DirDepartamento, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosDepartamento", PosDepartamento, SqlDbType.Int);

					Command.AddParameter("@ExpArea", ExpArea, SqlDbType.Bit);
					Command.AddParameter("@DirArea", DirArea, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosArea", PosArea, SqlDbType.Int);

					Command.AddParameter("@ExpHora", ExpHora, SqlDbType.Bit);
					Command.AddParameter("@DirHora", DirHora, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosHora", PosHora, SqlDbType.Int);

					Command.AddParameter("@ExpEstado", ExpEstado, SqlDbType.Bit);
					Command.AddParameter("@DirEstado", DirEstado, SqlDbType.VarChar, 5);
					Command.AddParameter("@PosEstado", PosEstado, SqlDbType.Int);

					DataTable Table = conn.ExecuteQuery(Command);
					foreach (DataRow row in Table.Rows)
					{
						Cumplimiento cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row["UidArea"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidArea"].ToString());
						cumplimiento._StrTarea = row["VchTarea"].ToString();
						cumplimiento._StrDepartamento = row["VchDepartamento"].ToString();
						cumplimiento._StrEstadoCumplimiento = row["VchEstadoCumplimiento"].ToString();
						cumplimiento._StrTipoTarea = row["VchTipoTarea"].ToString();
						cumplimiento._StrArea = row["VchArea"].ToString();
						cumplimiento._TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString());
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? default(DateTimeOffset) : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimiento._UidPeriodo = new Guid(row["UidPeriodo"].ToString());
						cumplimiento._IntFolio = Convert.ToInt32(row["FolioCumplimiento"]);
						cumplimiento._IntFolioTarea = Convert.ToInt32(row["FolioTarea"]);
						cumplimiento._IntFolioTurno = Convert.ToInt32(row["FolioTurno"]);
						LsCumplimientos.Add(cumplimiento);
					}
				}
				catch (Exception e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return LsCumplimientos;
			}

			private void InjectCriteria(Criteria criteria, SqlCommand command)
			{
				if (!string.IsNullOrWhiteSpace(criteria.NombreUsuario))
				{
					command.AddParameter("@VchUsuario", criteria.NombreUsuario, SqlDbType.NVarChar, 50);
				}
				if (!string.IsNullOrWhiteSpace(criteria.Departamentos))
				{
					command.AddParameter("@VchDepartamentos", criteria.Departamentos, SqlDbType.NVarChar, 2000);
				}
				if (!string.IsNullOrWhiteSpace(criteria.Tareas))
				{
					command.AddParameter("@VchTareas", criteria.Tareas, SqlDbType.NVarChar, 2000);
				}
			}

			/// <summary>
			/// Registra un nuevo cumplimiento en la base de datos. 
			/// </summary>
			/// <remarks>
			/// Por defecto las nuevas tareas no poseen
			/// cumplimientos y al momento de llamar a este método se genera el primer cumplimiento correspondiente
			/// a un área o departamento junto con su siguiente cumplimiento programado basado en la
			/// periodicidad indicada en la tarea.
			/// 
			/// En las llamadas consecuentes solo se actualiza el estado del
			/// cumplimiento programado.
			/// </remarks>
			/// <param name="uidCumplimiento">Identificador único del cumplimiento, puede ser nulo.</param>
			/// <param name="uidTarea">Identificador único de la tarea.</param>
			/// <param name="uidDepartamento">Identificador único del departamento, puede ser nulo.</param>
			/// <param name="uidArea">Identificador único del área, puede ser nulo.</param>
			/// <param name="uidUsuario">Identificador único del usuario.</param>
			/// <param name="fechaCumplimiento">Fecha de cumplimiento de la tarea.</param>
			/// <param name="estado">Valor booleano de la tarea: Sí o No. Puede tener un valor dependiendo del tipo de tarea.</param>
			/// <param name="valor1">Valor decimal de la tarea. Puede tener un valor dependiendo del tipo de tarea.</param>
			/// <param name="valor2">Valor decimal de la tarea. Puede tener un valor dependiendo del tipo de tarea. Trabaja como complemento de <paramref name="valor1"/> para manejar rangos.</param>
			/// <param name="uidOpcion">Identificador único de la opción seleccionada. Puede tener un valor dependiendo del tipo de tarea.</param>
			/// <param name="observaciones">una cadena de texto con las observaciones capturadas durante la realización de la tarea, sí hay.</param>
			/// <param name="urlFoto">URL de la foto de prueba del cumplimiento.</param>
			public void RegistrarCumplimiento(ref Guid? uidCumplimiento, Guid uidTarea, Guid? uidDepartamento,
				Guid? uidArea, DateTime? DtProximo, Guid uidUsuario, DateTimeOffset fechaCumplimiento, bool? estado, decimal? valor1,
				decimal? valor2, Guid? uidOpcion, string observaciones, string urlFoto, Guid turno, bool? Atrasado, DateTimeOffset? dtFechaAtrasada, Guid UidCreador, string RolCreador, int FolioTurno)
			{
				try
				{
					SqlCommand command = new SqlCommand();

					command.CommandText = "usp_Cumplimiento_Do";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidTarea", uidTarea, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFechaHora", fechaCumplimiento, SqlDbType.DateTimeOffset);
					command.AddParameter("@UrlFoto", urlFoto, SqlDbType.NVarChar, 50);
					command.AddParameter("@VchObservacion", observaciones, SqlDbType.NVarChar, 200);
					command.AddParameter("@UidTurno", turno, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidCreadorC", UidCreador, SqlDbType.UniqueIdentifier);
					command.AddParameter("@VchRolCreadorC", RolCreador, SqlDbType.VarChar, 15);
					command.AddParameter("@IntFolioTurno", FolioTurno, SqlDbType.Int);

					if (DtProximo != null)
						command.AddParameter("@DtFechaNueva", DtProximo, SqlDbType.Date);

					command.Parameters.Add("@NuevoCumplimiento", SqlDbType.UniqueIdentifier);
					command.Parameters["@NuevoCumplimiento"].Direction = ParameterDirection.Output;

					if (Atrasado != null)
						command.AddParameter("@Atrasada", Atrasado, SqlDbType.Bit);

					if (dtFechaAtrasada != null)
						command.AddParameter("@FechaAtrasada", dtFechaAtrasada, SqlDbType.DateTimeOffset, 2);

					if (uidCumplimiento.HasValue)
						command.AddParameter("@UidCumplimiento", uidCumplimiento.Value, SqlDbType.UniqueIdentifier);
					if (uidDepartamento.HasValue)
						command.AddParameter("@UidDepartamento", uidDepartamento.Value, SqlDbType.UniqueIdentifier);
					if (uidArea.HasValue)
						command.AddParameter("@UidArea", uidArea.Value, SqlDbType.UniqueIdentifier);

					if (estado.HasValue)
						command.AddParameter("@BitValor", estado.Value, SqlDbType.Bit);

					if (valor1.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor1", valor1.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (valor2.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor1", valor2.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (uidOpcion.HasValue)
						command.AddParameter("@UidOpcion", uidOpcion.Value, SqlDbType.UniqueIdentifier);

					conn.ExecuteCommand(command, false);

					Guid id = default(Guid);
					if (command.Parameters["@NuevoCumplimiento"].Value != DBNull.Value)
						id = new Guid(command.Parameters["@NuevoCumplimiento"].Value.ToString());

					if (!uidCumplimiento.HasValue || uidCumplimiento.Value != id)
					{
						uidCumplimiento = id;
					}

					command.Dispose();
				}
				catch (SqlException ex)
				{
					throw new DatabaseException("Error saving a Cumplimiento object", ex);
				}
			}

			public void ActualizarCumplimiento(Guid uidCumplimiento, DateTimeOffset fechaCumplimiento, bool? estado, decimal? valor1,
				decimal? valor2, Guid? uidOpcion, string observaciones, string urlFoto)
			{
				try
				{
					SqlCommand command = new SqlCommand();

					command.CommandText = "usp_Cumplimiento_Update";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidCumplimiento", uidCumplimiento, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFechaHora", fechaCumplimiento, SqlDbType.DateTimeOffset);
					command.AddParameter("@UrlFoto", urlFoto, SqlDbType.NVarChar, 50);
					command.AddParameter("@VchObservacion", observaciones, SqlDbType.NVarChar, 200);

					if (estado.HasValue)
						command.AddParameter("@BitValor", estado.Value, SqlDbType.Bit);

					if (valor1.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor1", valor1.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (valor2.HasValue)
					{
						SqlParameter parm = command.AddParameter("@DcValor1", valor2.Value, SqlDbType.Decimal);
						parm.Precision = 18;
						parm.Scale = 4;
					}

					if (uidOpcion.HasValue)
						command.AddParameter("@UidOpcion", uidOpcion.Value, SqlDbType.UniqueIdentifier);

					conn.ExecuteCommand(command);

				}
				catch (SqlException ex)
				{
					throw new DatabaseException("Error saving a Cumplimiento object", ex);
				}
			}

			public int CambiarCumplimiento(Guid? uidCumplimiento, Guid uidTarea, Guid? uidDepartamento, Guid? uidArea, Guid? uidUsuario,
				DateTime? nuevaFecha, string operacion, DateTimeOffset fecha, string observaciones, bool? Atrasado, DateTimeOffset? dtFechaAtrasada, Guid UidCreador, string RolCreador, int FolioTurno)
			{
				try
				{
					SqlCommand command = new SqlCommand();

					command.CommandText = "usp_Cumplimiento_Do";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidTarea", uidTarea, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFechaHora", fecha, SqlDbType.DateTimeOffset);
					command.AddParameter("@DtFechaNueva", nuevaFecha, SqlDbType.Date);
					command.AddParameter("@vchEstado", operacion, SqlDbType.NVarChar, 50);
					command.AddParameter("@VchObservacion", observaciones, SqlDbType.NVarChar, 200);
					command.AddParameter("@UidCreadorC", UidCreador, SqlDbType.UniqueIdentifier);
					command.AddParameter("@VchRolCreadorC", RolCreador, SqlDbType.VarChar, 15);
					command.AddParameter("@IntFolioTurno", FolioTurno, SqlDbType.Int);

					if (Atrasado != null)
						command.AddParameter("@Atrasada", Atrasado, SqlDbType.Bit);

					if (dtFechaAtrasada != null)
						command.AddParameter("@FechaAtrasada", dtFechaAtrasada, SqlDbType.DateTimeOffset, 2);

					if (uidCumplimiento.HasValue)
						command.AddParameter("@UidCumplimiento", uidCumplimiento.Value, SqlDbType.UniqueIdentifier);
					if (uidDepartamento.HasValue)
						command.AddParameter("@UidDepartamento", uidDepartamento.Value, SqlDbType.UniqueIdentifier);
					if (uidArea.HasValue)
						command.AddParameter("@UidArea", uidArea.Value, SqlDbType.UniqueIdentifier);

					command.Parameters.Add("@Estado", SqlDbType.TinyInt);
					command.Parameters["@Estado"].Direction = ParameterDirection.Output;

					conn.ExecuteCommand(command, false);

					int value = (byte)command.Parameters["@Estado"].Value;

					command.Dispose();

					return value;
				}
				catch (SqlException ex)
				{
					throw new DatabaseException("Error saving a Cumplimiento object", ex);
				}
			}

			public List<Cumplimiento> Buscar(string fecha, string fecha2, Guid UidUsuario)
			{
				List<Cumplimiento> cumplimientos = new List<Cumplimiento>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_BuscarCumplimiento";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", UidUsuario, SqlDbType.UniqueIdentifier);

					if (fecha != string.Empty)
					{
						command.Parameters.Add("@DtFecha", SqlDbType.Date);
						command.Parameters["@DtFecha"].Value = Convert.ToDateTime(fecha);
					}

					if (fecha2 != string.Empty)
					{
						command.Parameters.Add("@DtFecha2", SqlDbType.Date);
						command.Parameters["@DtFecha2"].Value = Convert.ToDateTime(fecha2);
					}


					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Cumplimiento cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row["UidArea"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidArea"].ToString());
						//cumplimiento._StrTarea = row["VchTarea"].ToString();
						//cumplimiento._StrDepartamento = row["VchDepartamento"].ToString();
						//cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						//cumplimiento._StrArea = row["VchArea"].ToString();
						//cumplimiento._TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString());
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? default(DateTimeOffset) : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimientos.Add(cumplimiento);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			public List<Cumplimiento> ReporteTareas(Guid uidUsuario, Guid uidPeriodo, DateTime fecha)
			{
				List<Cumplimiento> cumplimientos = new List<Cumplimiento>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Cumplimiento_Reporte";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidPeriodo", uidPeriodo, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

					DataTable table = conn.ExecuteQuery(command);

					foreach (DataRow row in table.Rows)
					{
						Cumplimiento cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row["UidArea"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidArea"].ToString());
						cumplimiento._StrTarea = row["VchTarea"].ToString();
						cumplimiento._StrDepartamento = row["VchDepartamento"].ToString();
						cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						cumplimiento._StrArea = row["VchArea"].ToString();
						cumplimiento._TmHora = row.IsNull("TmHora") ? (TimeSpan?)null : TimeSpan.Parse(row["TmHora"].ToString());
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? default(DateTimeOffset) : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimiento._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						cumplimiento._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						cumplimiento._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						cumplimiento._StrOpcion = row["VchOpciones"].ToString();
						cumplimiento._StrTipoMedicion = row["VchTipoMedicion"].ToString();
						cumplimiento._StrTipoUnidad = row["VchTipoUnidad"].ToString();
						cumplimiento._StrTipoTarea = row["VchTipoTarea"].ToString();
						cumplimiento._StrObservacion = row["VchObservacion"].ToString();
						cumplimiento._IntFolio = Convert.ToInt32(row["IntFolioCumpl"]);
						cumplimiento._IntFolioTarea = Convert.ToInt32(row["IntFolio"]);
						cumplimiento._StrUsuario = row["VchUsuario"].ToString();
						cumplimiento.BlAtrasada = row.IsNull("BitPosterior") ? false : (bool)row["BitPosterior"];
						cumplimientos.Add(cumplimiento);
					}
				}
				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			/// <summary>
			/// Obtiene las tareas que se generaron en el cumplimiento atrasado del turno
			/// </summary>
			/// <returns></returns>
			public List<Cumplimiento> ReporteTareasNuevas(Guid UidPeriodo, DateTime DtFecha, int IntFolioTurno)
			{
				List<Cumplimiento> LsCumplimientos = new List<Cumplimiento>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Cumplimiento_ReporteTareasTurnoCAtrasado";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidPeriodo", UidPeriodo, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@DtFecha", DtFecha, SqlDbType.Date);
					Query.AddParameter("@IntFolioTurno", IntFolioTurno, SqlDbType.Int);

					DataTable dtResults = this.conn.ExecuteQuery(Query);
					foreach (DataRow row in dtResults.Rows)
					{
						Cumplimiento cumplimiento = new Cumplimiento();
						cumplimiento._UidCumplimiento = row["UidCumplimiento"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidCumplimiento"].ToString());
						cumplimiento._UidTarea = new Guid(row["UidTarea"].ToString());
						cumplimiento._UidDepartamento = new Guid(row["UidDepartamento"].ToString());
						cumplimiento._UidArea = row["UidArea"].ToString().Length == 0 ? Guid.Empty : new Guid(row["UidArea"].ToString());
						cumplimiento._StrTarea = row["VchTarea"].ToString();
						cumplimiento._StrDepartamento = row["VchDepartamento"].ToString();
						cumplimiento._StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						cumplimiento._StrArea = row["VchArea"].ToString();
						cumplimiento._DtFechaHora = row.IsNull("DtFechaHora") ? default(DateTimeOffset) : DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cumplimiento._BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						cumplimiento._DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : Convert.ToDecimal(row["DcValor1"].ToString());
						cumplimiento._DcValor2 = row.IsNull("DcValor2") ? (decimal?)null : Convert.ToDecimal(row["DcValor2"].ToString());
						cumplimiento._StrOpcion = row["VchOpciones"].ToString();
						cumplimiento._StrTipoMedicion = row["VchTipoMedicion"].ToString();
						cumplimiento._StrTipoUnidad = row["VchTipoUnidad"].ToString();
						cumplimiento._StrTipoTarea = row["VchTipoTarea"].ToString();
						cumplimiento._StrObservacion = row["VchObservacion"].ToString();
						cumplimiento._IntFolio = Convert.ToInt32(row["IntFolioCumpl"]);
						cumplimiento._IntFolioTarea = Convert.ToInt32(row["IntFolio"]);
						cumplimiento._StrUsuario = row["VchUsuario"].ToString();
						cumplimiento.BlAtrasada = row.IsNull("BitPosterior") ? false : (bool)row["BitPosterior"];

						LsCumplimientos.Add(cumplimiento);
					}
				}
				catch (Exception ex)
				{
					throw;
				}
				return LsCumplimientos;
			}

			public void Deshacer(Guid uidCumplimiento)
			{
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_Cumplimiento_Undo";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidCumplimiento", uidCumplimiento, SqlDbType.UniqueIdentifier);

				conn.ExecuteCommand(command);
			}

			public DateTime? ObtenerSiguienteFecha(Guid uidTarea, DateTime date)
			{
				SqlCommand command = new SqlCommand();

				command.CommandText = "usp_Cumplimiento_FechaProxima";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidTarea", uidTarea, SqlDbType.UniqueIdentifier);
				command.AddParameter("@DtFecha", date, SqlDbType.DateTime);

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					DateTime? fecha;

					fecha = row.IsNull("DtFecha") ? (DateTime?)null : Convert.ToDateTime(row["DtFecha"].ToString());
					return fecha;
				}
				return null;
			}

			public List<Cumplimiento> GetCumplimientosTarea(Guid UidTarea, Guid UidUsuario, int? FolioCumplimiento, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidEstado)
			{
				List<Cumplimiento> LsCumplimientos = new List<Cumplimiento>();
				SqlCommand Query = new SqlCommand();
				try
				{
					Query.CommandText = "usp_HistoricoTarea_FindCumplimientosTarea";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);

					if (UidUsuario != Guid.Empty)
						Query.AddParameter("@UidUsuario", UidUsuario, SqlDbType.UniqueIdentifier);

					if (FolioCumplimiento != null)
						Query.AddParameter("@FolioCumplimiento", FolioCumplimiento, SqlDbType.Int);

					if (DtFechaInicio != null)
						Query.AddParameter("@DtFechaInicio", DtFechaInicio, SqlDbType.Date);

					if (DtFechaFin != null)
						Query.AddParameter("@DtFechaFin", DtFechaFin, SqlDbType.Date);

					if (UidEstado != Guid.Empty)
						Query.AddParameter("@UidEstado", UidEstado, SqlDbType.UniqueIdentifier);

					DataTable Result = conn.ExecuteQuery(Query);
					Cumplimiento cCumplimiento;
					foreach (DataRow row in Result.Rows)
					{
						cCumplimiento = new Cumplimiento();
						cCumplimiento.UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
						cCumplimiento.IntFolio = int.Parse(row["IntFolio"].ToString());
						cCumplimiento.DtFechaHora = DateTimeOffset.Parse(row["DtFechaHora"].ToString());
						cCumplimiento.StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString();
						cCumplimiento.IntFolioTurno = int.Parse(row["FolioTurno"].ToString());
						cCumplimiento.BlAtrasada = row.IsNull("BitAtrasado") ? false : bool.Parse(row["BitAtrasado"].ToString());
						cCumplimiento.DcValor1 = row.IsNull("DcValor1") ? (decimal?)null : decimal.Parse(row["DcValor1"].ToString());
						cCumplimiento.BlValor = row.IsNull("BitValor") ? (bool?)null : Convert.ToBoolean(row["BitValor"].ToString());
						cCumplimiento.UidOpcion = row.IsNull("UidOpcion") ? (Guid?)null : new Guid(row["UidOpcion"].ToString());
						cCumplimiento.StrOpcion = row.IsNull("VchOpciones") ? string.Empty : row["VchOpciones"].ToString();
						cCumplimiento.StrTipoMedicion = row["VchTipoMedicion"].ToString();
						cCumplimiento.StrTipoUnidad = row.IsNull("VchUnidadMedida") ? string.Empty : row["VchUnidadMedida"].ToString();
						cCumplimiento.ValorCumplimiento = string.Empty;
						cCumplimiento.StrNombreUsuario = row["VchUsuario"].ToString();
						cCumplimiento.StrObservacion = row.IsNull("VchObservacion") ? string.Empty : row["VchObservacion"].ToString();

						if (cCumplimiento.StrTipoMedicion.Equals("Verdadero/Falso"))
						{
							cCumplimiento.StrTipoMedicion = "Si/No";
							if (cCumplimiento.BlValor != null)
								cCumplimiento.ValorCumplimiento = cCumplimiento.BlValor.Value ? "SI" : "NO";
						}
						else if (cCumplimiento.StrTipoMedicion.Equals("Numerico"))
						{
							if (cCumplimiento.DcValor1 != null)
								cCumplimiento.ValorCumplimiento = cCumplimiento.StrTipoUnidad + " " + cCumplimiento.DcValor1.Value.ToString();
						}
						else if (cCumplimiento.StrTipoMedicion.Equals("Seleccionable"))
						{
							cCumplimiento.StrTipoMedicion = "Selección";
							if (cCumplimiento.UidOpcion != null)
								cCumplimiento.ValorCumplimiento = cCumplimiento.StrOpcion;
						}

						LsCumplimientos.Add(cCumplimiento);
					}
				}
				catch (Exception e)
				{
					throw;
				}
				return LsCumplimientos;
			}

			public bool AgregarPosterior(Guid UidCumplimiento, Guid UidTarea, Guid UidDepartamento, Guid UidArea, DateTime DtSiguiente, DateTime DtFechaTurno, int IntFolioTurno, Guid UidTurno)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_Cumplimiento_AddPosterior";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidCumplimiento", UidCumplimiento, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@UidTurno", UidTurno, SqlDbType.UniqueIdentifier);

					if (UidDepartamento != Guid.Empty)
						Query.AddParameter("@UidDepartamento", UidDepartamento, SqlDbType.UniqueIdentifier);
					if (UidArea != Guid.Empty)
						Query.AddParameter("@UidArea", UidArea, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@DtSiguiente", DtSiguiente, SqlDbType.DateTime);
					Query.AddParameter("@DtFechaTurno", DtFechaTurno, SqlDbType.DateTime);
					Query.AddParameter("@IntFolioTurno", IntFolioTurno, SqlDbType.Int);

					return this.conn.ExecuteCommand(Query);
				}
				catch (Exception)
				{
					throw;
				}
			}

			public List<Cumplimiento> GetAtrasadasTurno(Guid UidDepartamento, int Folio, int? FolioTarea, string StrTarea, Guid UidDepartamentoFiltro, Guid UidArea, Guid UidTipoTarea)
			{
				List<Cumplimiento> LsCumplimientos = new List<Cumplimiento>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_CumplimientoAtrasado_GetTareas";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidDepartamento", UidDepartamento, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@IntFolioTurno", Folio, SqlDbType.Int);

					if (FolioTarea != null)
						Query.AddParameter("@IntFolioTarea", FolioTarea, SqlDbType.Int);

					if (StrTarea != string.Empty)
						Query.AddParameter("@VchTarea", StrTarea, SqlDbType.NVarChar, 50);

					if (UidDepartamentoFiltro != Guid.Empty)
						Query.AddParameter("@UidDepartamentoFiltro", UidDepartamentoFiltro, SqlDbType.UniqueIdentifier);

					if (UidArea != Guid.Empty)
						Query.AddParameter("@UidArea", UidArea, SqlDbType.UniqueIdentifier);

					if (UidTipoTarea != Guid.Empty)
						Query.AddParameter("@UidTipoTarea", UidTipoTarea, SqlDbType.UniqueIdentifier);

					DataTable dtResults = this.conn.ExecuteQuery(Query);
					foreach (DataRow row in dtResults.Rows)
					{
						LsCumplimientos.Add(new Cumplimiento()
						{
							UidCumplimiento = (Guid)row["UidCumplimiento"],
							IntFolio = row.IsNull("FolioCumplimiento") ? 0 : (int)row["FolioCumplimiento"],
							UidTarea = (Guid)row["UidTarea"],
							IntFolioTarea = (int)row["FolioTarea"],
							StrTarea = (string)row["VchTarea"],
							OrdenTarea = row.IsNull("Orden") ? 0 : (int)row["Orden"],
							UidArea = row.IsNull("UidArea") ? Guid.Empty : (Guid)row["UidArea"],
							StrArea = row.IsNull("VchArea") ? "(GLOBAL)" : (string)row["VchArea"],
							UidDepartamento = (Guid)row["UidDepartamento"],
							StrDepartamento = (string)row["VchDepartamento"]
						});
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsCumplimientos;
			}

			/// <summary>
			/// Registrar cumplimiento posterior
			/// </summary>
			/// <param name="UidCumplimiento"></param>
			/// <param name="UidUsuario"></param>
			/// <param name="StrObservaciones"></param>
			/// <param name="StrTipoCumplimiento"></param>
			/// <param name="BlValor"></param>
			/// <param name="dcValor1"></param>
			/// <param name="dcValor2"></param>
			/// <param name="UidOpcion"></param>
			/// <param name="UidTurno"></param>
			/// <param name="UidCreador"></param>
			/// <param name="StrRolCreador"></param>
			/// <param name="UidMedicion"></param>
			/// <param name="BlAtrasado"></param>
			/// <param name="DtFechaAtrasada"></param>
			/// <returns></returns>
			public bool RegistrarPosterior(Guid UidCumplimiento, Guid UidUsuario, string StrObservaciones, string StrTipoCumplimiento, bool? BlValor, decimal? dcValor1, decimal? dcValor2, Guid UidOpcion, Guid UidTurno, Guid UidCreador, string StrRolCreador, Guid UidMedicion, bool BlAtrasado, DateTimeOffset DtFechaAtrasada)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_CumplimientoAtrasado_Do";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidCumplimiento", UidCumplimiento, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@UidUsuario", UidUsuario, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@VchObservaciones", StrObservaciones, SqlDbType.NVarChar, 200);
					Query.AddParameter("@Estatus", StrTipoCumplimiento, SqlDbType.VarChar, 20);

					if (BlValor != null)
						Query.AddParameter("@BitValor", BlValor, SqlDbType.Bit);
					if (dcValor1 != null)
						Query.AddParameter("@DcValor1", dcValor1, SqlDbType.Decimal);
					if (dcValor2 != null)
						Query.AddParameter("@DcValor2", dcValor2, SqlDbType.Decimal);
					if (UidOpcion != Guid.Empty)
						Query.AddParameter("@UidOpcion", UidOpcion, SqlDbType.UniqueIdentifier);
					if (UidTurno != Guid.Empty)
						Query.AddParameter("@UidTurno", UidTurno, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@UidCreadorC", UidCreador, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@VchRolCreadorC", StrRolCreador, SqlDbType.VarChar, 15);
					Query.AddParameter("@UidTipoMedicion", UidMedicion, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@Atrasada", BlAtrasado, SqlDbType.Bit);
					Query.AddParameter("@FechaAtrasada", DtFechaAtrasada, SqlDbType.DateTimeOffset);

					return this.conn.ExecuteCommand(Query);
				}
				catch (Exception ex)
				{
					throw;
				}
			}
		}

		/// <summary>
		/// Clase de criterio de búsqueda.
		/// </summary>
		[Obsolete]
		public class Criteria
		{
			/// <summary>
			/// Nombre del usuario que realizó el cumplimiento.
			/// </summary>
			public string NombreUsuario { get; set; }

			/// <summary>
			/// Conjunto de <see cref="Guid"/> de los departamentos donde se realizaron los cumplimientos.
			/// </summary>
			public string Departamentos { get; set; }

			/// <summary>
			/// Conjutno de <see cref="Guid"/> de las tareas cumplidas.
			/// </summary>
			public string Tareas { get; set; }
		}
	}
}
