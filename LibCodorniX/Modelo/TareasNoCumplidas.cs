﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;
using CodorniX.Util;

namespace CodorniX.Modelo
{
	public class TareasNoCumplidas
	{
		private int _IntFolio;
		public int IntFolio
		{
			get { return _IntFolio; }
			set { _IntFolio = value; }
		}

		private Guid _UidDepartamento;
		public Guid UidDepartamento
		{
			get { return _UidDepartamento; }
			set { _UidDepartamento = value; }
		}
		public string strIdDepartamento { get { return _UidDepartamento.ToString(); } }

		private Guid? _UidArea;
		public Guid? UidArea
		{
			get { return _UidArea; }
			set { _UidArea = value; }
		}
		public string strIdArea { get { return _UidArea.ToString(); } }

		private Guid _UidUsuario;
		public Guid UidUsuario
		{
			get { return _UidUsuario; }
			set { _UidUsuario = value; }
		}
		public string strIdUsuario { get { return _UidUsuario.ToString(); } }

		private int _IntNumTareasRequeridasNoCumplidas;
		public int IntNumTareasRequeridasdNoCumplidas
		{
			get { return _IntNumTareasRequeridasNoCumplidas; }
			set { _IntNumTareasRequeridasNoCumplidas = value; }
		}

		private string _StrDepartamento;
		public string StrDepartamento
		{
			get { return _StrDepartamento; }
			set { _StrDepartamento = value; }
		}

		private string _StrArea;
		public string StrArea
		{
			get { return _StrArea; }
			set { _StrArea = value; }
		}

		private int _IntTareasCumplidas;
		public int IntTareasCumplidas
		{
			get { return _IntTareasCumplidas; }
			set { _IntTareasCumplidas = value; }
		}

		private int _IntTareasNoCumplidas;
		public int IntTareasNoCumplidas
		{
			get { return _IntTareasNoCumplidas; }
			set { _IntTareasNoCumplidas = value; }
		}

		private int _IntTareasCanceladas;
		public int IntTareasCanceladas
		{
			get { return _IntTareasCanceladas; }
			set { _IntTareasCanceladas = value; }
		}

		private int _IntTareasPospuestas;
		public int IntTareasPospuestas
		{
			get { return _IntTareasPospuestas; }
			set { _IntTareasPospuestas = value; }
		}


		private DateTime _DtFecha;
		public DateTime DtFecha
		{
			get { return _DtFecha; }
			set { _DtFecha = value; }
		}
		public string strDtFecha { get { return _DtFecha.ToString("dd-MM-yyyy"); } }

		private string _StrTurno;
		public string StrTurno
		{
			get { return _StrTurno; }
			set { _StrTurno = value; }
		}

		private Guid _UidPeriodo;
		public Guid UidPeriodo
		{
			get { return _UidPeriodo; }
			set { _UidPeriodo = value; }
		}
		public string strIdPeriodo { get { return _UidPeriodo.ToString(); } }

		private string _StrEstadoTurno;
		public string StrEstadoTurno
		{
			get { return _StrEstadoTurno; }
			set { _StrEstadoTurno = value; }
		}

		private DateTime? _DtFechaHoraInicio;
		public DateTime? DtFechaHoraInicio
		{
			get { return _DtFechaHoraInicio; }
			set { _DtFechaHoraInicio = value; }
		}
		public string strDtFechaHoraInicio { get { return _DtFechaHoraInicio == null ? "" : _DtFechaHoraInicio?.ToString("dd-MM-yyyy"); } }

		private DateTime? _DtFechaHoraFin;
		public DateTime? DtFechaHoraFin
		{
			get { return _DtFechaHoraFin; }
			set { _DtFechaHoraFin = value; }
		}
		public string strDtFechaHoraFin { get { return _DtFechaHoraFin == null ? "" : _DtFechaHoraFin?.ToString("dd-MM-yyyy"); } }

		private string _StrUsuario;
		public string StrUsuario
		{
			get { return _StrUsuario; }
			set { _StrUsuario = value; }
		}

		private Guid? _UidInicioTurno;
		public Guid? UidInicioTurno
		{
			get { return _UidInicioTurno; }
			set { _UidInicioTurno = value; }
		}
		public string strIdInicioTurno { get { return _UidInicioTurno.ToString(); } }

		private Guid? _UidEstadoTurno;
		public Guid? UidEstadoTurno
		{
			get { return _UidEstadoTurno; }
			set { _UidEstadoTurno = value; }
		}
		public string strIdEstadoTurno { get { return _UidEstadoTurno == null ? Guid.Empty.ToString() : _UidEstadoTurno.ToString(); } }

		/*Aux*/
		private string strIdSucursal;
		public string StrIdSucursal
		{
			get { return strIdSucursal; }
			set { strIdSucursal = value; }
		}

		/*2018 30 07*/
		//Campo para identificar si el Encargado o Supervisor a Iniciado el turno
		private bool blEncargadoInicio;
		public bool _BlEncargadoInicio
		{
			get { return blEncargadoInicio; }
			set { blEncargadoInicio = value; }
		}
		private bool blSupervisorInicio;
		public bool _BlSupervisorInicio
		{
			get { return blSupervisorInicio; }
			set { blSupervisorInicio = value; }
		}


		public class Repositorio
		{
			Conexion conexion = new Conexion();

			public int NumeroTareasSinEstado(Guid UidPeriodo, Guid UidUsuario, DateTime fecha)
			{
				int num = 0;
				TareasNoCumplidas tareanocumplida = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_Cumplimiento_NumeroTareasSinEstado";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidUsuario"].Value = UidUsuario;

				command.AddParameter("@UidPeriodo", UidPeriodo, SqlDbType.UniqueIdentifier);

				command.Parameters.Add("@DtFecha", SqlDbType.Date);
				command.Parameters["@DtFecha"].Value = fecha;

				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					num = Convert.ToInt32(row["NumTareasSinEstado"].ToString());
				}

				return num;
			}

			public TareasNoCumplidas ObtenerNoCumplimiento(Guid UidSucuarsal, Guid UidUsuario, DateTime fecha)
			{
				TareasNoCumplidas tareanocumplida = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_Cumplimiento_NumeroTareas";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidSucursal", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidSucursal"].Value = UidSucuarsal;

				command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidUsuario"].Value = UidUsuario;

				command.Parameters.Add("@DtFecha", SqlDbType.Date);
				command.Parameters["@DtFecha"].Value = fecha;

				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					tareanocumplida = new TareasNoCumplidas()
					{
						_IntTareasCumplidas = Convert.ToInt32(row["NumTareasCumplidas"].ToString()),
						IntTareasCanceladas = Convert.ToInt32(row["NumTareasCanceladas"].ToString()),
						_IntNumTareasRequeridasNoCumplidas = Convert.ToInt32(row["NumTareasRequeridasNoCumplidas"].ToString())
					};
				}

				return tareanocumplida;
			}

			public List<TareasNoCumplidas> ConsultarCumplimiento(Guid uidUsuario, Guid uidSucursal, DateTime fecha, int? FolioTurno, Guid? EstatusTurno)
			{
				List<TareasNoCumplidas> cumplimientos = new List<TareasNoCumplidas>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Cumplimiento_NumeroTareas";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

					if (FolioTurno != null)
						command.AddParameter("@IntFolioTurno", FolioTurno, SqlDbType.Int);

					if (EstatusTurno != null)
						command.AddParameter("@UidEstadoTurno", EstatusTurno, SqlDbType.UniqueIdentifier);

					DataTable table = conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						TareasNoCumplidas cumplimiento = new TareasNoCumplidas()
						{
							_DtFecha = Convert.ToDateTime(row["DtFecha"].ToString()),
							_UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
							_StrDepartamento = row["VchDepartamento"].ToString(),
							_IntTareasCumplidas = Convert.ToInt32(row["NumTareasCumplidas"].ToString()),
							_IntTareasNoCumplidas = Convert.ToInt32(row["NumTareasNoCumplidas"].ToString()),
							_IntTareasCanceladas = Convert.ToInt32(row["NumTareasCanceladas"].ToString()),
							_IntTareasPospuestas = Convert.ToInt32(row["NumTareasPospuestas"].ToString()),
							_IntNumTareasRequeridasNoCumplidas = Convert.ToInt32(row["NumTareasRequeridasNoCumplidas"].ToString()),
							_StrTurno = row["VchTurno"].ToString(),
							_StrEstadoTurno = row["VchEstadoTurno"].ToString(),
							_DtFechaHoraInicio = row.IsNull("DtFechaHoraInicio") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
							_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
							_BlEncargadoInicio = row.IsNull("iniEncargado") ? false : Convert.ToBoolean(row["iniEncargado"]),
							_BlSupervisorInicio = row.IsNull("iniSupervisor") ? false : Convert.ToBoolean(row["iniSupervisor"]),
							_IntFolio = row.IsNull("FolioTurno") ? 0 : int.Parse(row["FolioTurno"].ToString())
						};

						if (cumplimiento.StrEstadoTurno.Equals("Abierto") ||
							cumplimiento.StrEstadoTurno.Equals("Abierto (Controlado)"))
						{
							if (!cumplimiento._BlEncargadoInicio)
							{
								cumplimiento.StrEstadoTurno = "";
							}
						}

						cumplimientos.Add(cumplimiento);
					}

				}


				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			public List<TareasNoCumplidas> ConsultarSupervision(Guid uidUsuario, Guid uidSucursal, DateTime fecha)
			{
				List<TareasNoCumplidas> cumplimientos = new List<TareasNoCumplidas>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Supervision_NumeroTareas";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

					DataTable table = conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						TareasNoCumplidas cumplimiento = new TareasNoCumplidas()
						{
							_DtFecha = Convert.ToDateTime(row["DtFecha"].ToString()),
							_IntFolio = Convert.ToInt32(row["IntFolio"].ToString()),
							_StrUsuario = row["VchUsuario"].ToString(),
							_UidUsuario = new Guid(row["UidUsuario"].ToString()),
							_UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
							_StrDepartamento = row["VchDepartamento"].ToString(),
							_IntTareasCumplidas = Convert.ToInt32(row["NumTareasCumplidas"].ToString()),
							_IntTareasNoCumplidas = Convert.ToInt32(row["NumTareasNoCumplidas"].ToString()),
							_IntNumTareasRequeridasNoCumplidas = Convert.ToInt32(row["NumTareasRequeridasNoCumplidas"].ToString()),
							_StrTurno = row["VchTurno"].ToString(),
							_StrEstadoTurno = row["VchEstadoTurno"].ToString(),
							_DtFechaHoraInicio = row.IsNull("DtFechaHoraInicio") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
							_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
							_UidInicioTurno = row.IsNull("UidInicioTurno") ? (Guid?)null : new Guid(row["UidInicioTurno"].ToString()),
							_UidEstadoTurno = row.IsNull("UidEstadoTurno") ? (Guid?)null : new Guid(row["UidEstadoTurno"].ToString()),
							_UidPeriodo = new Guid(row["UidPeriodo"].ToString())
						};
						cumplimientos.Add(cumplimiento);
					}

				}


				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			public List<TareasNoCumplidas> ConsultarAdministrador(Guid uidUsuario, Guid uidSucursal, DateTime fecha, string departamentos)
			{
				List<TareasNoCumplidas> cumplimientos = new List<TareasNoCumplidas>();

				SqlCommand command = new SqlCommand();

				try
				{
					command.CommandText = "usp_Administrador_NumeroTareas";
					command.CommandType = CommandType.StoredProcedure;

					command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
					command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
					command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);
					command.AddParameter("@VchDepartamentos", departamentos, SqlDbType.NVarChar, 2000);

					DataTable table = conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						TareasNoCumplidas cumplimiento = new TareasNoCumplidas()
						{
							_DtFecha = Convert.ToDateTime(row["DtFecha"].ToString()),
							_IntFolio = Convert.ToInt32(row["IntFolio"].ToString()),
							_StrUsuario = row["VchUsuario"].ToString(),
							_UidUsuario = new Guid(row["UidUsuario"].ToString()),
							_UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
							_StrDepartamento = row["VchDepartamento"].ToString(),
							_IntTareasCumplidas = Convert.ToInt32(row["NumTareasCumplidas"].ToString()),
							_IntTareasNoCumplidas = Convert.ToInt32(row["NumTareasNoCumplidas"].ToString()),
							_IntNumTareasRequeridasNoCumplidas = Convert.ToInt32(row["NumTareasRequeridasNoCumplidas"].ToString()),
							_StrTurno = row["VchTurno"].ToString(),
							_StrEstadoTurno = row["VchEstadoTurno"].ToString(),
							_DtFechaHoraInicio = row.IsNull("DtFechaHoraInicio") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraInicio"].ToString()),
							_DtFechaHoraFin = row.IsNull("DtFechaHoraFin") ? (DateTime?)null : Convert.ToDateTime(row["DtFechaHoraFin"].ToString()),
							_UidInicioTurno = row.IsNull("UidInicioTurno") ? (Guid?)null : new Guid(row["UidInicioTurno"].ToString()),
							_UidEstadoTurno = row.IsNull("UidEstadoTurno") ? (Guid?)null : new Guid(row["UidEstadoTurno"].ToString()),
							_UidPeriodo = new Guid(row["UidPeriodo"].ToString())
						};
						cumplimientos.Add(cumplimiento);
					}

				}


				catch (SqlException e)
				{
					throw new DatabaseException("Error fetching Cumplimiento", e);
				}
				return cumplimientos;
			}

			public TareasNoCumplidas ObtenerTareas(Guid uiddepartamento, Guid uidUsuario, DateTime Fecha)
			{
				TareasNoCumplidas tarea = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_Cumplimiento_NumeroTareas";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidDepartamento"].Value = uiddepartamento;

				command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidUsuario"].Value = uidUsuario;

				command.Parameters.Add("@DtFecha", SqlDbType.Date);
				command.Parameters["@DtFecha"].Value = Fecha;


				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					tarea = new TareasNoCumplidas()
					{
						_DtFecha = Convert.ToDateTime(row["DtFecha"].ToString()),
						_UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
						_StrDepartamento = row["VchDepartamento"].ToString(),
						_IntTareasCumplidas = Convert.ToInt32(row["NumTareasCumplidas"].ToString()),
						_IntTareasNoCumplidas = Convert.ToInt32(row["NumTareasNoCumplidas"].ToString()),
						_IntNumTareasRequeridasNoCumplidas = Convert.ToInt32(row["NumTareasRequeridasNoCumplidas"].ToString()),
						_StrTurno = row["VchTurno"].ToString(),
						_UidArea = row.IsNull("UidArea") ? (Guid?)null : new Guid(row["UidArea"].ToString()),
						_StrArea = row.IsNull("VchArea") ? "N/A" : row["VchArea"].ToString(),
					};
				}

				return tarea;
			}


			public TareasNoCumplidas DepartamentoSeleccionado(Guid uiddepartamento, Guid uidUsuario, DateTime Fecha, Guid UidSucursal)
			{
				TareasNoCumplidas tarea = null;
				DataTable table = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_ContarTareas_Consultar";
				command.CommandType = CommandType.StoredProcedure;

				command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidDepartamento"].Value = uiddepartamento;

				command.Parameters.Add("@UidSucursal", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidSucursal"].Value = UidSucursal;

				command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
				command.Parameters["@UidUsuario"].Value = uidUsuario;

				command.Parameters.Add("@DtFecha", SqlDbType.Date);
				command.Parameters["@DtFecha"].Value = Fecha;



				table = conexion.Busquedas(command);

				foreach (DataRow row in table.Rows)
				{
					tarea = new TareasNoCumplidas()
					{
						_DtFecha = Convert.ToDateTime(row["DtFecha"].ToString()),
						_UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
						_StrDepartamento = row["VchDepartamento"].ToString(),
						_UidPeriodo = new Guid(row["UidPeriodo"].ToString()),
						_StrTurno = row["VchTurno"].ToString(),
						_IntTareasCumplidas = Convert.ToInt32(row["NumTareasCumplidas"].ToString()),
						_IntTareasNoCumplidas = Convert.ToInt32(row["NumTareasNoCumplidas"].ToString()),
						_IntTareasCanceladas = Convert.ToInt32(row["NumTareasCanceladas"].ToString()),
						_IntTareasPospuestas = Convert.ToInt32(row["NumTareasPospuestas"].ToString()),
						_IntNumTareasRequeridasNoCumplidas = Convert.ToInt32(row["NumTareasRequeridasNoCumplidas"].ToString()),
						//_BlEncargadoInicio = Convert.ToBoolean(row["blEncargadoInicio"].ToString()!=null ? row["blEncargadoInicio"].ToString() : "false"),
						//_BlSupervisorInicio = Convert.ToBoolean(row["blEncargadoInicio"].ToString() != null ? row["blEncargadoInicio"].ToString() : "false")
					};
				}

				return tarea;
			}

		}


	}
}
