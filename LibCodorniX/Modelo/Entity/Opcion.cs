﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo.Entity
{
    public class Opcion
    {
        private Guid _UidOpcion;

        public Guid UidOpcion
        {
            get { return _UidOpcion; }
            set { _UidOpcion = value; }
        }

        private string _StrOpcion;

        public string StrOpcion
        {
            get { return _StrOpcion; }
            set { _StrOpcion = value; }
        }

    }
}
