﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	[Serializable]
	public class ParametroOrdenamiento
	{
		#region Atributes
		private Guid _ID;
		private string _Parametro;
		private bool _BlOrdenar;
		private string _Direccion;
		private int _Posicion;

		public int Posicion
		{
			get { return _Posicion; }
			set { _Posicion = value; }
		}
		public string Direccion
		{
			get { return _Direccion; }
			set { _Direccion = value; }
		}
		public bool BlOrdenar
		{
			get { return _BlOrdenar; }
			set { _BlOrdenar = value; }
		}
		public string Parametro
		{
			get { return _Parametro; }
			set { _Parametro = value; }
		}
		public Guid ID
		{
			get { return _ID; }
			set { _ID = value; }
		}

		#endregion

		public ParametroOrdenamiento()
		{
		}
	}
}
