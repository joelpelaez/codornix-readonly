﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CodorniX.Modelo
{
    public class URL
    {
        private Guid _UidUrl;

        public Guid UidUrl
        {
            get { return _UidUrl; }
            set { _UidUrl = value; }
        }

        private string _StrUrl;

        public string StrUrl
        {
            get { return _StrUrl; }
            set { _StrUrl = value; }
        }

    }
}