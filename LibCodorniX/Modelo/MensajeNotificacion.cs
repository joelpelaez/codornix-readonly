﻿using CodorniX.Util;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	[Serializable]
	public class MensajeNotificacion
	{
		private Guid _UidMensajeNotificacion;
		public Guid UidMensajeNotificacion
		{
			get { return _UidMensajeNotificacion; }
			set { _UidMensajeNotificacion = value; }
		}
		public string strIdMensajeNotificacion { get { return _UidMensajeNotificacion.ToString(); } }

		private Guid _UidEstadoNotificacion;
		public Guid UidEstadoNotificacion
		{
			get { return _UidEstadoNotificacion; }
			set { _UidEstadoNotificacion = value; }
		}
		public string strIdEstadoNotificacion { get { return _UidEstadoNotificacion.ToString(); } }

		private Guid _UidCumplimiento;
		public Guid UidCumplimiento
		{
			get { return _UidCumplimiento; }
			set { _UidCumplimiento = value; }
		}
		public string strIdCumplimiento { get { return _UidCumplimiento.ToString(); } }

		// EXTRA:
		private string _StrTarea;
		public string StrTarea
		{
			get { return _StrTarea; }
			set { _StrTarea = value; }
		}

		private string _StrDepartamento;
		public string StrDepartamento
		{
			get { return _StrDepartamento; }
			set { _StrDepartamento = value; }
		}

		private string _StrEstadoNotificacion;
		public string StrEstadoNotificacion
		{
			get { return _StrEstadoNotificacion; }
			set { _StrEstadoNotificacion = value; }
		}

		private string _StrResultado;
		public string StrResultado
		{
			get { return _StrResultado; }
			set { _StrResultado = value; }
		}

		private string _StrArea;
		public string StrArea
		{
			get { return _StrArea; }
			set { _StrArea = value; }
		}

		/* 2018 Octubre 13 */
		private DateTimeOffset _DtFechaCumplimiento;
		public DateTimeOffset DtFechaCumplimiento
		{
			get { return _DtFechaCumplimiento; }
			set { _DtFechaCumplimiento = value; }
		}

		private bool _BlAtrasada;
		public bool BlAtrasada
		{
			get { return _BlAtrasada; }
			set { _BlAtrasada = value; }
		}

		private int _FolioTarea;
		public int FolioTarea
		{
			get { return _FolioTarea; }
			set { _FolioTarea = value; }
		}

		private int _FolioCumplimiento;
		public int FolioCumplimiento
		{
			get { return _FolioCumplimiento; }
			set { _FolioCumplimiento = value; }
		}

		private string _TipoTarea;
		public string TipoTarea
		{
			get { return _TipoTarea; }
			set { _TipoTarea = value; }
		}

		private string _Observaciones;
		public string Observaciones
		{
			get { return _Observaciones; }
			set { _Observaciones = value; }
		}

		private string _UnidadMedida;
		public string UnidadMedida
		{
			get { return _UnidadMedida; }
			set { _UnidadMedida = value; }
		}


		public class Repository
		{
			Connection conn = new Connection();

			public List<MensajeNotificacion> Search(Guid uidSucursal, string uidDepartamentos,
				string uidDepartamentosbusqueda, string uidEstados, DateTime? fechaInicio, DateTime? fechaFin)
			{
				List<MensajeNotificacion> notificaciones = new List<MensajeNotificacion>();
				MensajeNotificacion mensaje = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_MensajeNotificacion_Search";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidSucursal", uidSucursal, SqlDbType.UniqueIdentifier);
				command.AddParameter("@VchDepartamentos", uidDepartamentos, SqlDbType.NVarChar, 2000);
				command.AddParameter("@VchDepartamentosBusqueda", uidDepartamentosbusqueda, SqlDbType.NVarChar, 2000);
				command.AddParameter("@VchEstados", uidEstados, SqlDbType.NVarChar, 2000);
				if (fechaInicio.HasValue)
					command.AddParameter("@DtFechaInicio", fechaInicio.Value, SqlDbType.DateTime);
				if (fechaFin.HasValue)
					command.AddParameter("@DtFechaFin", fechaFin.Value, SqlDbType.DateTime);

				//command.AddParameter("@ExpFolioC", ExpFolioCumplimiento, SqlDbType.Bit);
				//command.AddParameter("@DirFolioC", DirFolioCumplimiento, SqlDbType.VarChar, 5);

				//command.AddParameter("@ExpFolioT", ExpFolioTarea, SqlDbType.Bit);
				//command.AddParameter("@DirFolioT", DirFolioTarea, SqlDbType.VarChar, 5);

				//command.AddParameter("@ExpTarea", ExpTarea, SqlDbType.Bit);
				//command.AddParameter("@DirTarea", DirTarea, SqlDbType.VarChar, 5);

				//command.AddParameter("@ExpDepartamento", ExpDepartamento, SqlDbType.Bit);
				//command.AddParameter("@DirDepartamento", DirDepartamento, SqlDbType.VarChar, 5);

				//command.AddParameter("@ExpFecha", ExpFecha, SqlDbType.Bit);
				//command.AddParameter("@DirFecha", DirFecha, SqlDbType.VarChar, 5);

				//command.AddParameter("@ExpHora", ExpHora, SqlDbType.Bit);
				//command.AddParameter("@DirHora", DirHora, SqlDbType.VarChar, 5);

				//command.AddParameter("@ExpEstado", ExpEstado, SqlDbType.Bit);
				//command.AddParameter("@DirEstado", DirEstado, SqlDbType.VarChar, 5);

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					mensaje = new MensajeNotificacion();
					mensaje._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
					mensaje._UidEstadoNotificacion = new Guid(row["UidEstadoNotificacion"].ToString());
					mensaje._UidMensajeNotificacion = new Guid(row["UidMensajeNotificacion"].ToString());
					mensaje._StrResultado = row["VchResultado"].ToString();
					mensaje._StrTarea = row["VchTarea"].ToString();
					mensaje._StrDepartamento = row["VchDepartamento"].ToString();
					mensaje._StrEstadoNotificacion = row["VchEstadoNotificacion"].ToString();

					mensaje.DtFechaCumplimiento = DateTimeOffset.Parse(row["FechaCumplida"].ToString());
					mensaje.BlAtrasada = row.IsNull("BitAtrasado") ? false : Convert.ToBoolean(row["BitAtrasado"].ToString());
					mensaje.FolioTarea = int.Parse(row["FolioTarea"].ToString());
					mensaje.FolioCumplimiento = int.Parse(row["FolioCumpl"].ToString());
					notificaciones.Add(mensaje);
				}

				return notificaciones;
			}

			public MensajeNotificacion Find(Guid uid)
			{
				MensajeNotificacion mensaje = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_MensajeNotificacion_Find";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidMensajeNotificacion", uid, SqlDbType.UniqueIdentifier);

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					mensaje = new MensajeNotificacion();
					mensaje._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
					mensaje._UidEstadoNotificacion = new Guid(row["UidEstadoNotificacion"].ToString());
					mensaje._UidMensajeNotificacion = new Guid(row["UidMensajeNotificacion"].ToString());
					mensaje._StrResultado = row["VchResultado"].ToString();
					mensaje._StrTarea = row["VchTarea"].ToString();
					mensaje._StrDepartamento = row["VchDepartamento"].ToString();
					mensaje._StrEstadoNotificacion = row["VchEstadoNotificacion"].ToString();
					mensaje._StrArea = row["VchArea"].ToString();
					mensaje.TipoTarea = row["VchTipoTarea"].ToString();
					mensaje.BlAtrasada = row.IsNull("BitAtrasado") ? false : Convert.ToBoolean(row["BitAtrasado"].ToString());
					mensaje.Observaciones = row.IsNull("VchObservacion") ? "(Sin observaciones)" : row["VchObservacion"].ToString();
					mensaje.UnidadMedida = row.IsNull("VchTipoUnidad") ? string.Empty : row["VchTipoUnidad"].ToString();
				}

				return mensaje;
			}

			public MensajeNotificacion FindByCumplimiento(Guid uid)
			{
				MensajeNotificacion mensaje = null;

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_MensajeNotificacion_FindByCumplimiento";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidCumplimiento", uid, SqlDbType.UniqueIdentifier);

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					mensaje = new MensajeNotificacion();
					mensaje._UidCumplimiento = new Guid(row["UidCumplimiento"].ToString());
					mensaje._UidEstadoNotificacion = new Guid(row["UidEstadoNotificacion"].ToString());
					mensaje._UidMensajeNotificacion = new Guid(row["UidMensajeNotificacion"].ToString());
					mensaje._StrResultado = row["VchResultado"].ToString();
					mensaje._StrTarea = row["VchTarea"].ToString();
					mensaje._StrDepartamento = row["VchDepartamento"].ToString();
					mensaje._StrEstadoNotificacion = row["VchEstadoNotificacion"].ToString();
					mensaje._StrArea = row["VchArea"].ToString();
				}

				return mensaje;
			}

			public void ChangeState(Guid uid, string state)
			{
				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_MensajeNotificacion_CambiarEstado";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidMensajeNotificacion", uid, SqlDbType.UniqueIdentifier);
				command.AddParameter("@VchEstado", state, SqlDbType.NVarChar, 50);

				conn.ExecuteCommand(command);
			}

			public List<Departamento> GetDepartamentos(Guid uidUsuario, DateTime fecha)
			{
				List<Departamento> deptos = new List<Departamento>();

				SqlCommand command = new SqlCommand();
				command.CommandText = "usp_Supervisor_Departamentos";
				command.CommandType = CommandType.StoredProcedure;

				command.AddParameter("@UidUsuario", uidUsuario, SqlDbType.UniqueIdentifier);
				command.AddParameter("@DtFecha", fecha, SqlDbType.DateTime);

				DataTable table = conn.ExecuteQuery(command);

				foreach (DataRow row in table.Rows)
				{
					Departamento depto = new Departamento();
					depto.UidDepartamento = new Guid(row["UidDepartamento"].ToString());
					depto.StrNombre = row["VchNombre"].ToString();
					deptos.Add(depto);
				}

				return deptos;
			}

			public int ObtenerTotalNotificacionesNoRevisadas(Guid UidSucursal, DateTime DtFecha)
			{
				SqlCommand Command = new SqlCommand();
				Command.CommandText = "usp_MensajeNotificacion_GetTotalToday";
				Command.CommandType = CommandType.StoredProcedure;

				Command.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);
				Command.AddParameter("@DtFecha", DtFecha, SqlDbType.DateTime);

				DataTable Table = conn.ExecuteQuery(Command);

				int Total = 0;

				foreach (DataRow row in Table.Rows)
				{
					Total = row.IsNull("Total") ? 0 : int.Parse(row["Total"].ToString());
				}

				return Total;
			}

			public bool GenerarMensaje(Guid UidCumplimiento)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_MensajeNotificacion_Generar";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidCumplimiento", UidCumplimiento, SqlDbType.UniqueIdentifier);

					return this.conn.ExecuteCommand(Query);
				}
				catch (Exception)
				{

					throw;
				}
			}
		}
	}
}
