﻿using System;
using CodorniX.Util;
using CodorniX.ConexionDB;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.SqlClient;
using System.Data;

namespace CodorniX.Modelo
{
	[Serializable]
	public class TareaAntecesora
	{
		public Guid UidAntecesor
		{
			get;
			set;
		}
		public int? Repeticion
		{
			get;
			set;
		}
		public int? DiasDespues
		{
			get;
			set;
		}
		public bool BlUsarNotificacion
		{
			get;
			set;
		}
		public Guid UidTarea
		{
			get;
			set;
		}
		public int Folio
		{
			get;
			set;
		}
		public string Nombre
		{
			get;
			set;
		}
		public Guid UidDepartamento
		{
			get;
			set;
		}
		public string Departamento
		{
			get;
			set;
		}
		public Guid UidArea
		{
			get;
			set;
		}
		public string Area
		{
			get;
			set;
		}
		public DateTime DtFechaInicio
		{
			get;
			set;
		}
		public Guid UidPeriodicidad
		{
			get;
			set;
		}
		public string TipoFrecuencia
		{
			get;
			set;
		}
		public string Type
		{
			get;
			set;
		}
		public bool HasNotificacion
		{
			get;
			set;
		}

		public class Repository
		{
			private Conexion _Conexion = new Conexion();

			public List<TareaAntecesora> Busqueda(string TipoTarea, string Nombre, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidSucursal, string Departamentos, string Areas, int Folio)
			{
				List<TareaAntecesora> LsTareas = new List<TareaAntecesora>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_TareaAntecesor_Search";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);
					Query.AddParameter("@TareaType", TipoTarea, SqlDbType.VarChar, 1);

					if (Folio >= 0)
						Query.AddParameter("@Folio", Folio, SqlDbType.Int);

					if (!string.IsNullOrEmpty(Nombre))
						Query.AddParameter("@Nombre", Nombre, SqlDbType.NVarChar, 50);

					if (DtFechaInicio != null)
						Query.AddParameter("@DtFechaInicio", DtFechaInicio, SqlDbType.Date);

					if (DtFechaFin != null)
						Query.AddParameter("@DtFechaFin", DtFechaFin, SqlDbType.Date);

					if (!string.IsNullOrEmpty(Departamentos))
						Query.AddParameter("@UidDepartamentos", Departamentos, SqlDbType.NVarChar, 4000);

					if (!string.IsNullOrEmpty(Areas))
						Query.AddParameter("@UidAreas", Areas, SqlDbType.NVarChar, 4000);

					DataTable Results = _Conexion.Busquedas(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsTareas.Add(new TareaAntecesora()
						{
							//UidAntecesor = new Guid(row["UidAntecesor"].ToString()),
							UidTarea = new Guid(row["UidTarea"].ToString()),
							Folio = int.Parse(row["IntFolio"].ToString()),
							Nombre = row["VchTarea"].ToString(),
							UidArea = row.IsNull("UidArea") ? Guid.Empty : new Guid(row["UidArea"].ToString()),
							Area = row.IsNull("VchArea") ? "(global)" : row["VchArea"].ToString(),
							UidDepartamento = row.IsNull("UidDepartamento") ? Guid.Empty : new Guid(row["UidDepartamento"].ToString()),
							Departamento = row["VchDepartamento"].ToString(),
							DtFechaInicio = DateTime.Parse(row["DtFechaInicio"].ToString()),
							UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
							TipoFrecuencia = row["VchTipoFrecuencia"].ToString(),
							Type = row["VchType"].ToString()
						});
					}
				}
				catch (Exception)
				{

					throw;
				}
				return LsTareas;
			}

			public List<TareaAntecesora> GetAntecesorasTarea(Guid UidTarea)
			{
				List<TareaAntecesora> LsTareas = new List<TareaAntecesora>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_TareaAntecesor_GetAntecesores";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);
					DataTable Results = _Conexion.Busquedas(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsTareas.Add(new TareaAntecesora()
						{
							UidAntecesor = new Guid(row["UidAntecesor"].ToString()),
							Repeticion = row.IsNull("IntRepeticion") ? (int?)null : int.Parse(row["IntRepeticion"].ToString()),
							DiasDespues = row.IsNull("IntDiasDespues") ? (int?)null : int.Parse(row["IntDiasDespues"].ToString()),
							BlUsarNotificacion = row.IsNull("BitUsarNotificacion") ? false : Convert.ToBoolean(row["BitUsarNotificacion"].ToString()),
							UidTarea = new Guid(row["UidTarea"].ToString()),
							Folio = int.Parse(row["IntFolio"].ToString()),
							Nombre = row["VchTarea"].ToString(),
							UidArea = row.IsNull("UidArea") ? Guid.Empty : new Guid(row["UidArea"].ToString()),
							Area = row.IsNull("VchArea") ? "(global)" : row["VchArea"].ToString(),
							UidDepartamento = row.IsNull("UidDepartamento") ? Guid.Empty : new Guid(row["UidDepartamento"].ToString()),
							Departamento = row["VchDepartamento"].ToString(),
							DtFechaInicio = DateTime.Parse(row["DtFechaInicio"].ToString()),
							UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
							TipoFrecuencia = row["VchTipoFrecuencia"].ToString(),
							HasNotificacion = row.IsNull("UidNotificacionTarea") ? false : true
						});
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsTareas;
			}

			public List<TareaAntecesora> GetSucesorasTarea(Guid UidTarea)
			{
				List<TareaAntecesora> LsTareas = new List<TareaAntecesora>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_TareaAntecesor_GetSucesores";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidTarea", UidTarea, SqlDbType.UniqueIdentifier);
					DataTable Results = _Conexion.Busquedas(Query);
					foreach (DataRow row in Results.Rows)
					{
						LsTareas.Add(new TareaAntecesora()
						{
							UidAntecesor = new Guid(row["UidAntecesor"].ToString()),
							Repeticion = row.IsNull("IntRepeticion") ? (int?)null : int.Parse(row["IntRepeticion"].ToString()),
							DiasDespues = row.IsNull("IntDiasDespues") ? (int?)null : int.Parse(row["IntDiasDespues"].ToString()),
							BlUsarNotificacion = row.IsNull("BitUsarNotificacion") ? false : Convert.ToBoolean(row["BitUsarNotificacion"].ToString()),
							UidTarea = new Guid(row["UidTarea"].ToString()),
							Folio = int.Parse(row["IntFolio"].ToString()),
							Nombre = row["VchTarea"].ToString(),
							UidArea = row.IsNull("UidArea") ? Guid.Empty : new Guid(row["UidArea"].ToString()),
							Area = row.IsNull("VchArea") ? "(global)" : row["VchArea"].ToString(),
							UidDepartamento = row.IsNull("UidDepartamento") ? Guid.Empty : new Guid(row["UidDepartamento"].ToString()),
							Departamento = row["VchDepartamento"].ToString(),
							DtFechaInicio = DateTime.Parse(row["DtFechaInicio"].ToString()),
							UidPeriodicidad = new Guid(row["UidPeriodicidad"].ToString()),
							TipoFrecuencia = row["VchTipoFrecuencia"].ToString(),
							HasNotificacion = row.IsNull("UidNotificacionTarea") ? false : true
						});
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsTareas;
			}

			public bool Update(Guid UidAntecesor, int? Repeticion, bool UsarNotificacion, int DiasDespues)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_TareaAntecesor_Update";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidAntecesor", UidAntecesor, SqlDbType.UniqueIdentifier);

					if (Repeticion != null)
						Query.AddParameter("@IntRepeticion", Repeticion, SqlDbType.Int);

					Query.AddParameter("@BitUsarNotificacion", UsarNotificacion, SqlDbType.Bit);
					Query.AddParameter("@IntDiasDespues", DiasDespues, SqlDbType.Int);

					return _Conexion.ManipilacionDeDatos(Query);
				}
				catch (Exception)
				{

					throw;
				}
			}
		}
	}
}
