﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.ConexionDB;

namespace CodorniX.Modelo
{
	public class ResumenTarea
	{
		public Guid UidTarea
		{
			get;
			set;
		}

		private Guid _UidTareaCumplida;
		public Guid UidTareaCumplida
		{
			get { return _UidTareaCumplida; }
			set { _UidTareaCumplida = value; }
		}
		public string strIdTareaCumplida { get { return _UidTareaCumplida.ToString(); } }

		private Guid _UidTareaNoCumplida;
		public Guid UidTareaNoCumplida
		{
			get { return _UidTareaNoCumplida; }
			set { _UidTareaNoCumplida = value; }
		}
		public string strIdTareaNoCumplida { get { return _UidTareaNoCumplida.ToString(); } }

		private Guid _UidTareaRequeridaNoCumplida;
		public Guid UidTareaRequeridaNoCumplida
		{
			get { return _UidTareaRequeridaNoCumplida; }
			set { _UidTareaRequeridaNoCumplida = value; }
		}
		public string strIdTareaRequeridaNoCumplida { get { return _UidTareaRequeridaNoCumplida.ToString(); } }

		private Guid _UidCumplimiento;
		public Guid UidCumplimiento
		{
			get { return _UidCumplimiento; }
			set { _UidCumplimiento = value; }
		}

		private string _StrNombre;
		public string StrNombre
		{
			get { return _StrNombre; }
			set { _StrNombre = value; }
		}

		private string _StrTipoFrecuencia;
		public string StrTipoFrecuencia
		{
			get { return _StrTipoFrecuencia; }
			set { _StrTipoFrecuencia = value; }
		}

		private string _StrTipoTarea;
		public string StrTipoTarea
		{
			get { return _StrTipoTarea; }
			set { _StrTipoTarea = value; }
		}

		private string _StrStatus;
		public string StrStatus
		{
			get { return _StrStatus; }
			set { _StrStatus = value; }
		}

		private int _IntFolio;
		public int IntFolio
		{
			get { return _IntFolio; }
			set { _IntFolio = value; }
		}

		private int _IntFolioCumplimiento;
		public int IntFolioCumplimiento
		{
			get { return _IntFolioCumplimiento; }
			set { _IntFolioCumplimiento = value; }
		}

		private string _StrEstadoCumplimiento;
		public string StrEstadoCumplimiento
		{
			get { return _StrEstadoCumplimiento; }
			set { _StrEstadoCumplimiento = value; }
		}

		public Guid UidPeriodicidad
		{
			get;
			set;
		}

		public Guid UidTipoMedicion
		{
			get;
			set;
		}

		public Guid UidDepartamento
		{
			get;
			set;
		}

		public Guid UidArea
		{
			get;
			set;
		}

		public int IntFrecuenciaPeriodicidad
		{
			get;
			set;
		}

		public class Repositorio
		{
			Conexion Conexion = new Conexion();
			public List<ResumenTarea> TareasCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha)
			{
				List<ResumenTarea> tareas = new List<ResumenTarea>();
				ResumenTarea tarea = null;

				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_TareasCumplidas";
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidDepartamento"].Value = UidDepartamento;

					command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidUsuario"].Value = UidUsuario;

					command.Parameters.Add("@DtFecha", SqlDbType.Date);
					command.Parameters["@DtFecha"].Value = Fecha;
					DataTable table = Conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						tarea = new ResumenTarea()
						{
							_UidTareaCumplida = new Guid(row["UidTarea"].ToString()),
							_UidCumplimiento = new Guid(row["UidCumplimiento"].ToString()),
							_StrNombre = row["VchTarea"].ToString(),
							_StrStatus = (row["VchStatus"].ToString()),
							_StrTipoFrecuencia = (row["VchTipoFrecuencia"].ToString()),
							_StrTipoTarea = (row["VchTipoTarea"].ToString()),
							_StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString(),
							_IntFolio = Convert.ToInt32(row["FolioTarea"].ToString()),
							_IntFolioCumplimiento = Convert.ToInt32(row["FolioCumplimiento"].ToString()),
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<ResumenTarea> TareasNoCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
			{
				List<ResumenTarea> tareas = new List<ResumenTarea>();
				ResumenTarea tarea = null;

				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_TareasNoCumplidas";
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidDepartamento"].Value = UidDepartamento;

					command.Parameters.Add("@UidSucursal", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidSucursal"].Value = uidsucursal;

					command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidUsuario"].Value = UidUsuario;

					command.Parameters.Add("@DtFecha", SqlDbType.Date);
					command.Parameters["@DtFecha"].Value = Fecha;
					DataTable table = Conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						tarea = new ResumenTarea()
						{
							_UidCumplimiento = (Guid)row["UidCumplimiento"],
							_UidTareaNoCumplida = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchTarea"].ToString(),
							_StrStatus = (row["VchStatus"].ToString()),
							_StrTipoFrecuencia = (row["VchTipoFrecuencia"].ToString()),
							_StrTipoTarea = (row["VchTipoTarea"].ToString()),
							_StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString(),
							_IntFolio = Convert.ToInt32(row["FolioTarea"].ToString()),
							_IntFolioCumplimiento = row.IsNull("FolioCumplimiento") ? 0 : Convert.ToInt32(row["FolioCumplimiento"].ToString()),
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<ResumenTarea> TareasRequeridasNoCumplidas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
			{
				List<ResumenTarea> tareas = new List<ResumenTarea>();
				ResumenTarea tarea = null;

				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_TareasRequeridasNoCumplidas";
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidDepartamento"].Value = UidDepartamento;

					command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidUsuario"].Value = UidUsuario;

					command.Parameters.Add("@UidSucursal", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidSucursal"].Value = uidsucursal;

					command.Parameters.Add("@DtFecha", SqlDbType.Date);
					command.Parameters["@DtFecha"].Value = Fecha;


					DataTable table = Conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						tarea = new ResumenTarea()
						{
							_UidTareaRequeridaNoCumplida = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchTarea"].ToString(),
							_StrStatus = (row["VchStatus"].ToString()),
							_StrTipoFrecuencia = (row["VchTipoFrecuencia"].ToString()),
							_IntFolio = Convert.ToInt32(row["FolioTarea"].ToString()),
							_IntFolioCumplimiento = Convert.ToInt32(row["FolioCumplimiento"].ToString()),
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<ResumenTarea> TareasRequeridas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
			{
				List<ResumenTarea> tareas = new List<ResumenTarea>();
				ResumenTarea tarea = null;

				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_TareasRequeridas";
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidDepartamento"].Value = UidDepartamento;

					command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidUsuario"].Value = UidUsuario;

					command.Parameters.Add("@DtFecha", SqlDbType.Date);
					command.Parameters["@DtFecha"].Value = Fecha;
					DataTable table = Conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						tarea = new ResumenTarea()
						{
							_UidTareaNoCumplida = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchTarea"].ToString(),
							_StrStatus = (row["VchStatus"].ToString()),
							_StrTipoFrecuencia = (row["VchTipoFrecuencia"].ToString()),
							_StrTipoTarea = (row["VchTipoTarea"].ToString()),
							_StrEstadoCumplimiento = row["VchTipoCumplimiento"].ToString(),
							_IntFolio = Convert.ToInt32(row["FolioTarea"].ToString()),
							_IntFolioCumplimiento = row.IsNull("FolioCumplimiento") ? 0 : Convert.ToInt32(row["FolioCumplimiento"].ToString()),
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<ResumenTarea> TareasPospuestas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
			{
				List<ResumenTarea> tareas = new List<ResumenTarea>();
				ResumenTarea tarea = null;

				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_TareasPospuestas";
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidDepartamento"].Value = UidDepartamento;

					command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidUsuario"].Value = UidUsuario;

					command.Parameters.Add("@DtFecha", SqlDbType.Date);
					command.Parameters["@DtFecha"].Value = Fecha;
					DataTable table = Conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						tarea = new ResumenTarea()
						{
							_UidTareaNoCumplida = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchTarea"].ToString(),
							_StrStatus = (row["VchStatus"].ToString()),
							_StrTipoFrecuencia = (row["VchTipoFrecuencia"].ToString()),
							_StrTipoTarea = (row["VchTipoTarea"].ToString()),
							IntFolio = Convert.ToInt32(row["FolioTarea"].ToString()),
							IntFolioCumplimiento = Convert.ToInt32(row["FolioCumplimiento"].ToString())

						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			public List<ResumenTarea> TareasCanceladas(Guid UidDepartamento, Guid UidUsuario, DateTime Fecha, Guid uidsucursal)
			{
				List<ResumenTarea> tareas = new List<ResumenTarea>();
				ResumenTarea tarea = null;

				try
				{
					SqlCommand command = new SqlCommand();
					command.CommandText = "usp_TareasCanceladas";
					command.CommandType = CommandType.StoredProcedure;

					command.Parameters.Add("@UidDepartamento", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidDepartamento"].Value = UidDepartamento;

					command.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
					command.Parameters["@UidUsuario"].Value = UidUsuario;

					command.Parameters.Add("@DtFecha", SqlDbType.Date);
					command.Parameters["@DtFecha"].Value = Fecha;
					DataTable table = Conexion.Busquedas(command);

					foreach (DataRow row in table.Rows)
					{
						tarea = new ResumenTarea()
						{
							_UidTareaNoCumplida = new Guid(row["UidTarea"].ToString()),
							_StrNombre = row["VchTarea"].ToString(),
							_StrStatus = (row["VchStatus"].ToString()),
							_StrTipoFrecuencia = (row["VchTipoFrecuencia"].ToString()),
							_StrTipoTarea = (row["VchTipoTarea"].ToString()),
							IntFolio = Convert.ToInt32(row["FolioTarea"].ToString()),
							IntFolioCumplimiento = row.IsNull("FolioCumplimiento") ? 0 : Convert.ToInt32(row["FolioCumplimiento"].ToString())
						};
						tareas.Add(tarea);
					}
				}
				catch (SqlException e)
				{
					throw;
				}

				return tareas;
			}

			/// <summary>
			/// Obtiene las tareas que aun no han sido completadas en el turno para generar cumplimiento posterior
			/// </summary>
			/// <param name="DtFecha"></param>
			/// <param name="UidPeriodo"></param>
			/// <param name="UidUsuario"></param>
			/// <returns></returns>
			public List<ResumenTarea> TareasNoCompletadasTurno(DateTime DtFecha, Guid UidPeriodo, Guid UidUsuario)
			{
				List<ResumenTarea> LsTareas = new List<ResumenTarea>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_TareasNoCompletadasTurno";
					Query.CommandType = CommandType.StoredProcedure;

					Query.Parameters.Add("@UidPeriodo", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidPeriodo"].Value = UidPeriodo;

					Query.Parameters.Add("@UidUsuario", SqlDbType.UniqueIdentifier);
					Query.Parameters["@UidUsuario"].Value = UidUsuario;

					Query.Parameters.Add("@DtFecha", SqlDbType.DateTime);
					Query.Parameters["@DtFecha"].Value = DtFecha;

					DataTable dtResults = this.Conexion.Busquedas(Query);
					foreach (DataRow dRow in dtResults.Rows)
					{
						LsTareas.Add(new ResumenTarea()
						{
							_UidCumplimiento = new Guid(dRow["UidCumplimiento"].ToString()),
							UidTarea = new Guid(dRow["UidTarea"].ToString()),
							UidPeriodicidad = new Guid(dRow["UidPeriodicidad"].ToString()),
							UidTipoMedicion = new Guid(dRow["UidTipoMedicion"].ToString()),
							UidDepartamento = dRow.IsNull("UidDepartamento") ? Guid.Empty : new Guid(dRow["UidDepartamento"].ToString()),
							UidArea = dRow.IsNull("UidArea") ? Guid.Empty : new Guid(dRow["UidArea"].ToString()),
							IntFrecuenciaPeriodicidad = (int)dRow["IntFrecuencia"],
							StrTipoFrecuencia = dRow["VchTipoFrecuencia"].ToString()
						});
					}
				}
				catch (Exception ex)
				{
					throw;
				}
				return LsTareas;
			}
		}
	}
}
