﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	public class PeriodoEncargadoSupervision
	{
		#region Properties
		private Guid _UidPeriodo;
		public Guid UidPeriodo
		{
			get { return _UidPeriodo; }
			set { _UidPeriodo = value; }
		}

		private Guid _UidDepartamento;
		public Guid UidDepartamento
		{
			get { return _UidDepartamento; }
			set { _UidDepartamento = value; }
		}
		
		private DateTime _DtFecha;
		public DateTime DtFecha
		{
			get { return _DtFecha; }
			set { _DtFecha = value; }
		}

		public int IntFolioTurno
		{
			get;
			set;
		}
		#endregion
	}
}
