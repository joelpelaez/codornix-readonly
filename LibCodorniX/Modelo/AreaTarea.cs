﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using CodorniX.Util;

namespace CodorniX.Modelo
{
    /// <summary>
    /// Tabla de union de <see cref="Area"/> y <see cref="Tarea"/>
    /// </summary>
    [Serializable]
    public class AreaTarea : Area
    {
        private Guid _UidTarea;

        /// <summary>
        /// Identificador único de la tarea.
        /// </summary>
        public Guid UidTarea
        {
            get { return _UidTarea; }
            set { _UidTarea = value; }
        }

        private Guid _UidArea;

        /// <summary>
        /// Identificador único del área,
        /// </summary>
        new public Guid UidArea
        {
            get { return _UidArea; }
            set { _UidArea = value; }
        }

        /// <summary>
        /// Clase repositorio de las áreas.
        /// </summary>
        public class Repositorio : Area.Repository
        {
            /// <summary>
            /// Obtiene todas las áreas correspondientes a una tarea.
            /// </summary>
            /// <param name="uid">identificador único de la tarea.</param>
            /// <returns>Lista de areas.</returns>
            new public List<Area> FindAll(Guid uid)
            {
                DataTable table = new DataTable();
                List<Area> areas = new List<Area>();
                Area area = null;
                try
                {
                    SqlCommand comando = new SqlCommand();
                    comando.CommandText = "select Area.* from Area INNER JOIN TareaArea ON Area.UidArea =TareaArea.UidArea WHERE TareaArea.UidTarea ='"+uid+"'";
                    comando.CommandType = CommandType.Text;

                    table = _clsConexion.ExecuteQuery(comando);

                    foreach (DataRow row in table.Rows)
                    {
                        area = new Area()
                        {
                            UidArea = new Guid(row["UidArea"].ToString()),
                            UidDepartamento = new Guid(row["UidDepartamento"].ToString()),
                            StrNombre = row["VchNombre"].ToString(),
                            StrDescripcion = row["VchDescripcion"].ToString(),
                            StrURL = row["VchURL"].ToString(),
                            UidStatus = (Guid)row["UidStatus"],
							IsSaved = true
                        };

                        areas.Add(area);
                    }
                }
                catch (Exception e)
                {
                    throw new DatabaseException("Error populating", e);
                }

                return areas;
            }

            /// <summary>
            /// Guarda una relación Tarea-Area.
            /// </summary>
            /// <param name="areatarea">objeto relación</param>
            /// <returns>true si se guardo correctamente, false en caso contrario.</returns>
            public bool Save(AreaTarea areatarea)
            {
                return InternalSave(areatarea);

            }

			/// <summary>
			/// Eliminar asignacion de departamento a tarea
			/// </summary>
			/// <param name="UidTarea"></param>
			/// <param name="UidArea"></param>
			/// <returns></returns>
			public bool DeleteAssignment(Guid UidTarea,Guid UidArea)
			{
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_TareaArea_EliminarAsignacion";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidTarea", UidTarea,SqlDbType.UniqueIdentifier);
					Query.AddParameter("@UidArea", UidArea, SqlDbType.UniqueIdentifier);

					return _clsConexion.ExecuteCommand(Query);
				}
				catch (Exception ex)
				{
					return false;
				}
			}

            private bool InternalSave(AreaTarea areatarea)
            {
                try
                {
                    SqlCommand command = new SqlCommand();
                    command.CommandText = "usp_TareaArea_Add";
                    command.CommandType = CommandType.StoredProcedure;

                    command.AddParameter("@UidArea", areatarea.UidArea, SqlDbType.UniqueIdentifier);

                    command.AddParameter("@UidTarea", areatarea._UidTarea, SqlDbType.UniqueIdentifier);


                    return _clsConexion.ExecuteCommand(command);
                }
                catch (Exception)
                {
                    throw;
                }
            }
        }
    }
}
