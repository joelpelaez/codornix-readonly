﻿using System;
using CodorniX.Util;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CodorniX.Modelo
{
	[Serializable]
	public class TurnoIniciado
	{
		#region Properties
		public Guid UidInicioTurno
		{
			get;
			set;
		}
		public Guid UidUsuario
		{
			get;
			set;
		}
		public Guid UidDepartamento
		{
			get;
			set;
		}
		public Guid UidSucursal
		{
			get;
			set;
		}
		public Guid UidPeriodo
		{
			get;
			set;
		}
		public int Folio
		{
			get;
			set;
		}
		public string Usuario
		{
			get;
			set;
		}
		public string Turno
		{
			get;
			set;
		}
		public DateTimeOffset? DtoFechaInicio
		{
			get;
			set;
		}
		public DateTimeOffset? DtoFechaFin
		{
			get;
			set;
		}
		public string Departamento
		{
			get;
			set;
		}
		public string EstadoTurno
		{
			get;
			set;
		}
		public string Sucursal
		{
			get;
			set;
		}
		#endregion

		public TurnoIniciado()
		{

		}

		public class Repository
		{
			protected Connection _Conexion = new Connection();

			public List<TurnoIniciado> Busqueda(string IdsDepartamentos, int? FolioTurno, DateTime? DtFechaInicio, DateTime? DtFechaFin, Guid UidSucursal, Guid UidUsuario, Guid UidEmpresa)
			{
				List<TurnoIniciado> LsTurnos = new List<TurnoIniciado>();
				try
				{

					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_InicioTurno_Search";
					Query.CommandType = CommandType.StoredProcedure;

					if (FolioTurno != null)
						Query.AddParameter("@FolioTurno", FolioTurno, SqlDbType.Int);

					if (DtFechaInicio != null)
						Query.AddParameter("@FechaInicio", DtFechaInicio, SqlDbType.Date);

					if (DtFechaFin != null)
						Query.AddParameter("@FechaFin", DtFechaFin, SqlDbType.Date);

					if (IdsDepartamentos != string.Empty)
						Query.AddParameter("@Departamentos", IdsDepartamentos, SqlDbType.VarChar, 999);

					if (UidSucursal != Guid.Empty)
						Query.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);

					if (UidUsuario != Guid.Empty)
						Query.AddParameter("@UidEncargado", UidUsuario, SqlDbType.UniqueIdentifier);

					Query.AddParameter("@UidEmpresa", UidEmpresa, SqlDbType.UniqueIdentifier);

					DataTable Results = _Conexion.ExecuteQuery(Query);
					TurnoIniciado TI;
					foreach (DataRow dRow in Results.Rows)
					{
						TI = new TurnoIniciado();
						TI.UidInicioTurno = new Guid(dRow["UidInicioTurno"].ToString());
						TI.Folio = int.Parse(dRow["IntFolio"].ToString());
						TI.UidUsuario = new Guid(dRow["UidUsuarioPeriodo"].ToString());
						TI.Usuario = dRow["VchEncargado"].ToString();
						TI.Turno = dRow["VchTurno"].ToString();
						TI.DtoFechaInicio = dRow.IsNull("DtFechaHoraInicio") ? (DateTimeOffset?)null : DateTimeOffset.Parse(dRow["DtFechaHoraInicio"].ToString());
						TI.DtoFechaFin = dRow.IsNull("DtFechaHoraFin") ? (DateTimeOffset?)null : DateTimeOffset.Parse(dRow["DtFechaHoraFin"].ToString());
						TI.UidDepartamento = new Guid(dRow["UidDepartamento"].ToString());
						TI.Departamento = dRow["VchDepartamento"].ToString();
						TI.EstadoTurno = dRow["VchEstadoTurno"].ToString();
						LsTurnos.Add(TI);
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsTurnos;
			}

			public TurnoIniciado FindById(Guid UidTurnoIniciado)
			{
				TurnoIniciado Turno = new TurnoIniciado();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_InicioTurno_FindById";
					Query.CommandType = CommandType.StoredProcedure;
					Query.AddParameter("@UidTurnoIniciado", UidTurnoIniciado, SqlDbType.UniqueIdentifier);

					DataTable Result = _Conexion.ExecuteQuery(Query);
					foreach (DataRow dRow in Result.Rows)
					{
						Turno = new TurnoIniciado();
						Turno.UidPeriodo = new Guid(dRow["UidPeriodo"].ToString());
						Turno.UidInicioTurno = new Guid(dRow["UidInicioTurno"].ToString());
						Turno.Folio = int.Parse(dRow["IntFolio"].ToString());
						Turno.UidUsuario = new Guid(dRow["UidUsuarioPeriodo"].ToString());
						Turno.Usuario = dRow["VchEncargado"].ToString();
						Turno.Turno = dRow["VchTurno"].ToString();
						Turno.DtoFechaInicio = dRow.IsNull("DtFechaHoraInicio") ? (DateTimeOffset?)null : DateTimeOffset.Parse(dRow["DtFechaHoraInicio"].ToString());
						Turno.DtoFechaFin = dRow.IsNull("DtFechaHoraFin") ? (DateTimeOffset?)null : DateTimeOffset.Parse(dRow["DtFechaHoraFin"].ToString());
						Turno.UidDepartamento = new Guid(dRow["UidDepartamento"].ToString());
						Turno.Departamento = dRow["VchDepartamento"].ToString();
						Turno.EstadoTurno = dRow["VchEstadoTurno"].ToString();
						Turno.UidSucursal = new Guid(dRow["UidSucursal"].ToString());
						Turno.Sucursal = dRow["VchSucursal"].ToString();
					}
				}
				catch (Exception)
				{

					throw;
				}
				return Turno;
			}

			/// <summary>
			/// Obtener ultimo turno abierto del periodo
			/// </summary>
			/// <param name="UidPeriodo"></param>
			/// <param name="IntFolio"></param>
			/// <returns></returns>
			public TurnoIniciado GetLastOpen(Guid UidPeriodo, int IntFolio)
			{
				TurnoIniciado tiTurno = null;
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "SELECT * FROM InicioTurno IT WHERE IT.DtFechaHoraFin IS NULL AND UidPeriodo='" + UidPeriodo + "' AND IT.IntFolio>" + IntFolio + "";
					Query.CommandType = CommandType.Text;

					DataTable dtResultado = this._Conexion.ExecuteQuery(Query);
					foreach (DataRow dRow in dtResultado.Rows)
					{
						tiTurno = new TurnoIniciado();
						tiTurno.UidInicioTurno = (Guid)dRow["UidInicioTurno"];
						tiTurno.DtoFechaInicio = dRow.IsNull("DtFechaHoraInicio") ? (DateTimeOffset?)null : (DateTimeOffset)dRow["DtFechaHoraInicio"];
						tiTurno.DtoFechaFin = dRow.IsNull("DtFechaHoraFin") ? (DateTimeOffset?)null : (DateTimeOffset)dRow["DtFechaHoraFin"];
						tiTurno.UidUsuario = (Guid)dRow["UidUsuario"];
						tiTurno.Folio = (int)dRow["IntFolio"];
					}
				}
				catch (Exception ex)
				{
					throw;
				}
				return tiTurno;
			}

			public List<TurnoIniciado> BusquedaTurnosCumplimientoPosterior(Guid UidEmpresa, Guid UidSucursal, Guid UidEncargado, string UidDepartamentos, int? FolioTurno, DateTime? DtFechaInicio, DateTime? DtFechaFin)
			{
				List<TurnoIniciado> LsTurnos = new List<TurnoIniciado>();
				try
				{
					SqlCommand Query = new SqlCommand();
					Query.CommandText = "usp_CumplimientoAtrasado_SearchTurnos";
					Query.CommandType = CommandType.StoredProcedure;

					Query.AddParameter("@UidEmpresa", UidEmpresa, SqlDbType.UniqueIdentifier);
					if (UidSucursal != Guid.Empty)
						Query.AddParameter("@UidSucursal", UidSucursal, SqlDbType.UniqueIdentifier);
					if (UidEncargado != Guid.Empty)
						Query.AddParameter("@UidEncargado", UidEncargado, SqlDbType.UniqueIdentifier);
					if (UidDepartamentos != string.Empty)
						Query.AddParameter("@Departamentos", UidDepartamentos, SqlDbType.VarChar, 999);
					if (FolioTurno != null)
						Query.AddParameter("@FolioTurno", FolioTurno.Value, SqlDbType.Int);
					if (DtFechaInicio != null)
						Query.AddParameter("@FechaInicio", DtFechaInicio.Value, SqlDbType.DateTime);
					if (DtFechaFin != null)
						Query.AddParameter("@FechaFin", DtFechaFin.Value, SqlDbType.DateTime);

					DataTable dtResults = this._Conexion.ExecuteQuery(Query);
					TurnoIniciado tiTurno;
					foreach (DataRow dRow in dtResults.Rows)
					{
						tiTurno = new TurnoIniciado();
						tiTurno.UidInicioTurno = new Guid(dRow["UidInicioTurno"].ToString());
						tiTurno.Folio = int.Parse(dRow["IntFolio"].ToString());
						tiTurno.UidUsuario = new Guid(dRow["UidUsuario"].ToString());
						tiTurno.Usuario = dRow["VchUsuario"].ToString();
						tiTurno.Turno = dRow["VchTurno"].ToString();
						tiTurno.DtoFechaInicio = dRow.IsNull("DtFechaHoraInicio") ? (DateTimeOffset?)null : DateTimeOffset.Parse(dRow["DtFechaHoraInicio"].ToString());
						tiTurno.DtoFechaFin = dRow.IsNull("DtFechaHoraFin") ? (DateTimeOffset?)null : DateTimeOffset.Parse(dRow["DtFechaHoraFin"].ToString());
						tiTurno.UidDepartamento = new Guid(dRow["UidDepartamento"].ToString());
						tiTurno.Departamento = dRow["VchDepartamento"].ToString();
						tiTurno.EstadoTurno = dRow["VchEstadoTurno"].ToString();
						tiTurno.Turno = (string)dRow["VchTurno"];
						LsTurnos.Add(tiTurno);
					}
				}
				catch (Exception)
				{
					throw;
				}
				return LsTurnos;
			}
		}
	}
}
